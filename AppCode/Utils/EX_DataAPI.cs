﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WFALawPdfApi;

/// <summary>
/// EX_DataAPI 的摘要描述
/// </summary>
public class EX_DataAPI
{
	public EX_DataAPI()
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
	}
    public string GetCmpyList(string InsType)
    {
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetCmpyList");
         //  string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetCmpyList";
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        // InsType|yyyyMMddHH|Channel|IP
            string no = string.Format("{0}|{1}|{2}|{3}", InsType
                               , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            ApiURL = string.Format("{0}?no={1}", ApiURL, no);
            string err = "";
            string ret=WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
            return ret;
    }

    public string GetInsItemListByCmpy(string CmpyNo, string iStatus)
    {
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsItemListByCmpy");
        //string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetInsItemListByCmpy";
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        //iStatus按保險公司性質找出所有符合的公司資料 0:壽險 1:產險
            //Channel，若為台名通路，則是「TABC」，否則為「null」值。
            // iStatus|CmpyNo|yyyyMMddHH|Channel|IP
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", iStatus, CmpyNo
                , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel.Length == 0 ? "null" : Channel, IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            ApiURL = string.Format("{0}?no={1}", ApiURL, no);
            string err = "";
            string ret=WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
            return ret;
    }
    public string GetInsItemList(string CmpyNo, string Mark)
    {
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsItemList");
        //string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetInsItemList";
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        // CmpyNo|Mark|yyyyMMddHH|Channel|IP 
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", CmpyNo, Mark
                               , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            ApiURL = string.Format("{0}?no={1}", ApiURL, no);
            string err = "";
            string ret=WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
            return ret;
    }
    public string GetLawPdfApi(string lawdisplay)
    {
      //  string ApiURL = "https://update.gouptech.com.tw/GoupCstmAgent/Laws/GetLawPdfApi";
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetLawPdfApi");
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        //  string lawdisplay = tBoxIN.Text; //lawdisplay = "pr/pr-086.pdf"
        string[] lawFile = lawdisplay.Replace('\\', '/').Split('/');
        // CompanyNo|PDFName|yyyyMMddHH|ID|Channel
        string no = string.Format("{0}|{1}|{2}|{3}|{4}", lawFile[0], lawFile[1]
                           , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
        no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
        ApiURL = string.Format("{0}?no={1}", ApiURL, no);
        return ApiURL;
    }
            /// <summary> 取得 簡介,費率,條款 </summary>
        /// <param name="no">CmpyNo|Mark|yyyyMMddHH|Channel|IP</param>
        /// <returns></returns>
    public string GetInsFullInfo(string CmpyNo, string Mark)
    {
    //    {
    //"UrlCmpy": "TW",
    //"UrlMark": "050",
    //"Intrude": "<p class=\"Title\">* 停售 *<br>台壽國民儲蓄保險</p>\n\n<span class=\"IntrudeItem\">●商品特色</span>\n<p></p>",
    //"Premiums": null,
    //"ItemUnit": "萬元",
    //"isPDFLaw": false,
    //"PDFUrl": null,
    //"KeyTitles": "在特定期間或期滿且被保險人仍生存時，可領取生存保險金。可領取的金額與時間因保單而異。",
    //"LawDisplay": "    \ufeff\n<p class=\"Title\">台灣人壽十年滿期國民儲蓄保險保險單條款\n</p>",
    //"keyword": "儲蓄/養老/還本",
    //"FacebookTitle": "* 停售 *<br>台壽國民儲蓄保險",
    //"FacebookDescription": "    * 停售 *台壽國民儲蓄保險  ●商品特色"
//}

      //  string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetInsFullInfo";
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsFullInfo");
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        string no = string.Format("{0}|{1}|{2}|{3}|{4}", CmpyNo, Mark 
            , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel.Length == 0 ? "null" : Channel, IP);
        no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
        ApiURL = string.Format("{0}?no={1}", ApiURL, no);
        string err = "";
        string ret = WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
        return ret;
    }

    /// <summary>
    /// 傳回指定的保險公司險種與年期資料檔(按指定保險公司代號 讀入險種輸入表單需要的險種資料表, 年期與投保單位表
    /// </summary>
    /// <param name="iStatus">0:現售  1:停售  2:全部</param>
    /// <param name="CmpyNo">保險公司的代碼</param>
    /// <param name="Channel"></param>
    /// <returns></returns>
    public string GetInsItemsByCompanyNo(string iStatus, string CmpyNo)
    {
       // string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetInsItemsByCompanyNo";
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsItemsByCompanyNo");
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        //cmpyNo：保險公司的代碼
//iStus：// status 0:現售  1:停售  2:全部
//yyyyMMddHH：國際標準UTC時間
//Channel：通路，例如(台名)：TABC
//IP：伺服器主機來源IP
        // cmpyNo|iStus|yyyyMMddHH|Channel|IP
        string no = string.Format("{0}|{1}|{2}|{3}|{4}", CmpyNo, iStatus
            , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel.Length == 0 ? "null" : Channel, IP);
        no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
        ApiURL = string.Format("{0}?no={1}", ApiURL, no);
        string err = "";
        string ret = WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
        return ret;
    }

    public string CompareInsItem(string InsItemCount, string isJsonFormat, string Json)
    {
        //string ApiURL = "https://cloudwinner.gouptech.com.tw/InsAPI/GetInsItemByCompanyNo";
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "CompareInsItem");
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
        //InsItemCount：1~4 數字，最多四個險種
//isJsonFormat：0或1，控制回傳的Json是否要格式化
//yyyyMMddHH：國際標準UTC時間
//Channel：通路，例如台壽、台名
//IP：伺服器主機來源IP
//Json：詳如範例

        //  InsItemCount|isJsonFormat|yyyyMMddHH|Channel|IP|Json
        string no = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", InsItemCount, isJsonFormat
            , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel.Length == 0 ? "null" : Channel, IP, Json);
        no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
        ApiURL = string.Format("{0}?no={1}", ApiURL, no);
        string err = "";
        string ret = WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
        return ret;
    }
    public string CalcItemPremium(string Sex, string Age, string Json)
    {
        string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "CalcItemPremium");
        string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
        string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];

         //Sex|Age|yyyyMMddHH|Channel|IP|Json
        string no = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", Sex, Age
            , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel.Length == 0 ? "null" : Channel, IP, Json);
        no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
        ApiURL = string.Format("{0}?no={1}", ApiURL, no);
        string err = "";
        string ret = WebTalk.WebClinetPost.WebClientGet(ApiURL, "UTF-8", out err);
        return ret;

    }
}