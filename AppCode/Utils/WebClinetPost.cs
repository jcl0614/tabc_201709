using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Specialized;

namespace WebTalk
{
    public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
    {
        public TrustAllCertificatePolicy()
        { }
        public bool CheckValidationResult(ServicePoint sp,
         X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }
    public class WebClinetPost
    {

        public static CookieContainer cookieContainer;
        public static string WebClientPost(string url, string postData, string encodeType, string Request_Header, out string err)
        {
            string uriString = url;
            byte[] byteArray;
            byte[] responseArray;

            //postData = "checkvalue=32&bbb=%CD%B6%C6%B1&ilc=0&kkk=22"; 
            Encoding encoding = Encoding.GetEncoding(encodeType);
            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                //   WebClient myWebClient = new WebClient();
                HttpClient myWebClient = new HttpClient();
                WebHeaderCollection myWebHeaderCollection;

                if (Request_Header.Length > 0)
                {
                    string[] Request_Headers = Request_Header.Replace("\n", "").Split('\r');
                    foreach (string item in Request_Headers)
                    {
                        string[] items = item.Split(':');
                        myWebClient.Headers.Add(items[0], items[1]);
                    //    myWebClient.Headers.Add("Content-Type", Content_Type);
                    }

                }
                //myWebClient.Headers.Add("Referer","http://xxxxx/xxxxxxxxxxxxxxxxxxxxxxxx");   
                //myWebClient.Headers.Add("Accept-Language","zh-cn");
                //     myWebClient.Encoding = System.Text.Encoding.GetEncoding(encodeType);
                myWebHeaderCollection = myWebClient.Headers;

                if (cookieContainer == null)
                    cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                    myWebClient.Cookies = cookieContainer;

                byteArray = encoding.GetBytes(postData);
                responseArray = myWebClient.UploadData(uriString, "POST", byteArray);

                err = string.Empty;
                return encoding.GetString(responseArray);
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }


        public static string WebClientPost_(string url, string postData, string encodeType,string Content_Type, out string err)
        {
            string uriString = url;
            byte[] byteArray;
            byte[] responseArray;

            //postData = "checkvalue=32&bbb=%CD%B6%C6%B1&ilc=0&kkk=22"; 
            Encoding encoding = Encoding.GetEncoding(encodeType);
            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
             //   WebClient myWebClient = new WebClient();
                HttpClient myWebClient = new HttpClient();
                WebHeaderCollection myWebHeaderCollection;
                if(Content_Type.Length ==0)
                    Content_Type = "application/x-www-form-urlencoded";

                myWebClient.Headers.Add("Content-Type", Content_Type);
                //myWebClient.Headers.Add("Referer","http://xxxxx/xxxxxxxxxxxxxxxxxxxxxxxx");   
                //myWebClient.Headers.Add("Accept-Language","zh-cn");
           //     myWebClient.Encoding = System.Text.Encoding.GetEncoding(encodeType);
                myWebHeaderCollection = myWebClient.Headers;

                if (cookieContainer == null)
                    cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                    myWebClient.Cookies = cookieContainer;

                byteArray = encoding.GetBytes(postData);
                responseArray = myWebClient.UploadData(uriString, "POST", byteArray);

                err = string.Empty;
                return encoding.GetString(responseArray);
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }

        public static string WebClientUploadValues(string url, string postData, string encodeType, out string err)
        {
            string resultXML = "";
            err = string.Empty;
            Encoding encoding = Encoding.GetEncoding(encodeType);
            using (WebClient wc = new WebClient())
            {
                try
                {
                    wc.Encoding = encoding;

                    NameValueCollection nc = new NameValueCollection();
                    string[] postDatas = postData.Split('&');
                    foreach (string items in postDatas)
                    {
                        if (items != "")
                        {
                            string item = items.Substring(0, items.IndexOf("=")).Trim();
                            string itemVul = items.Substring(items.IndexOf("=")+1).Trim();
                            nc[item] = itemVul;
                        }
                    }

                    byte[] bResult = wc.UploadValues(url, nc);

                    resultXML = encoding.GetString(bResult);
                }
                catch (WebException ex)
                {
                   err =ex.Message;//"無法連接遠端伺服器");
                }
            }
            return resultXML;
        }
        public static string WebClientGet(string url, string encodeType, out string err)
        {
            string uriString = url;
         //   byte[] byteArray;
         //   byte[] responseArray;
            //postData = "checkvalue=32&bbb=%CD%B6%C6%B1&ilc=0&kkk=22"; 
            Encoding encoding = Encoding.GetEncoding(encodeType);
            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                HttpClient myWebClient = new HttpClient();
                if (cookieContainer == null)
                    cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                myWebClient.Cookies = cookieContainer;
            //    myWebClient.Encoding = System.Text.Encoding.GetEncoding(encodeType);

                // Download home page data.
                //  Console.WriteLine("Downloading " + remoteUri);
                // Download the Web resource and save it into a data buffer.
                byte[] myDataBuffer = myWebClient.DownloadData(url);
              //  cookieContainer =  myWebClient.Cookies;

                err = string.Empty;
                return encoding.GetString(myDataBuffer);
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }

        public static string WebClientGetfile(string url, string encodeType, out string err)
        {
            string uriString = url;
            //   byte[] byteArray;
            //   byte[] responseArray;
            //postData = "checkvalue=32&bbb=%CD%B6%C6%B1&ilc=0&kkk=22"; 
            Encoding encoding = Encoding.GetEncoding(encodeType);
            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                HttpClient myWebClient = new HttpClient();
                   if (cookieContainer == null)
                       cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                    myWebClient.Cookies = cookieContainer;

                // Download home page data.
                //  Console.WriteLine("Downloading " + remoteUri);
                // Download the Web resource and save it into a data buffer.
                byte[] myDataBuffer = myWebClient.DownloadData(url);
                cookieContainer = myWebClient.Cookies;
                //先存起來Data
                string fname = url.Substring(url.IndexOf("/Data/") + 6);
                using (FileStream fs = File.Create(System.IO.Directory.GetCurrentDirectory() + "\\" + fname))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {

                        bw.Write(myDataBuffer);
                    }
                }

                err = string.Empty;
                return System.IO.Directory.GetCurrentDirectory() + "\\" + fname;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }


        public static string HttpWebResponsePost(string url, string postData, string encodeType, out string err)
        {
            return HttpWebResponsePost(url, postData, encodeType, out  err, 0);
        }

        public static string HttpWebResponsePost(string url, string sUser, string sPwd, string sDomain, string postData, string encodeType, out string err, int Timeout)
        {

            Stream outstream = null;

            HttpWebRequest request = null;

            Encoding encoding = Encoding.GetEncoding(encodeType);

            byte[] data = encoding.GetBytes(postData);

            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                // CookieContainer cookieContainer = new CookieContainer();
                //  if (cookieContainer == null)
                //  cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                    request.CookieContainer = cookieContainer;

                request.KeepAlive = false;
                if (Timeout > 0)
                    request.Timeout = Timeout;
                request.AllowAutoRedirect = true;

                request.Method = "POST";

                request.ContentType = "application/x-www-form-urlencoded";

                request.ContentLength = data.Length;
                if (sUser != String.Empty)
                {
                    NetworkCredential oCredential = new NetworkCredential(sUser, sPwd, sDomain);
                    request.Credentials = oCredential.GetCredential(new Uri(url), String.Empty);
                }
                else
                {
                    request.Credentials = CredentialCache.DefaultCredentials;
                }


                outstream = request.GetRequestStream();

                outstream.Write(data, 0, data.Length);

                outstream.Close();

                HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse();

                Stream responseStream = responseStream = WebResponse.GetResponseStream();
                if (WebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                else if (WebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, encoding);

                string content = Reader.ReadToEnd();
                cookieContainer = request.CookieContainer;
                WebResponse.Close();
                responseStream.Close();
                Reader.Close();



                //response = request.GetResponse() as HttpWebResponse;
                //instream = response.GetResponseStream();
                //sr = new StreamReader(instream, encoding);
                //string content = sr.ReadToEnd();

                //response.Close();
                //sr.Close();
                //instream.Close();

                if (content.IndexOf("Err:") >= 0) //傳回錯誤
                    throw new Exception(content);

                err = string.Empty;
                return content;

            }

            catch (Exception ex)
            {

                err = ex.Message;
                return string.Empty;

            }
        }




        public static string HttpWebResponsePost(string url, string postData, string encodeType, out string err, int Timeout)
        {

            Stream outstream = null;

            HttpWebRequest request = null;

            Encoding encoding = Encoding.GetEncoding(encodeType);

            byte[] data = encoding.GetBytes(postData);

            try
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();


                request = WebRequest.Create(url) as HttpWebRequest;
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

               // CookieContainer cookieContainer = new CookieContainer();
              //  if (cookieContainer == null)
              //  cookieContainer = new CookieContainer();

                if (cookieContainer != null)
                    request.CookieContainer = cookieContainer;

                request.KeepAlive = false;
                if (Timeout > 0)
                    request.Timeout = Timeout;
                request.AllowAutoRedirect = true;

                request.Method = "POST";
            //    request.Credentials = CredentialCache.DefaultCredentials;

                request.ContentType = "application/x-www-form-urlencoded";

                request.ContentLength = data.Length;

                outstream = request.GetRequestStream();

                outstream.Write(data, 0, data.Length);

                outstream.Close();

                HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse();

                Stream responseStream = responseStream = WebResponse.GetResponseStream();
                if (WebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                else if (WebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

                StreamReader Reader = new StreamReader(responseStream, encoding);

                string content = Reader.ReadToEnd();
               cookieContainer = request.CookieContainer;
                WebResponse.Close();
                responseStream.Close();
                Reader.Close();

                

                //response = request.GetResponse() as HttpWebResponse;
                //instream = response.GetResponseStream();
                //sr = new StreamReader(instream, encoding);
                //string content = sr.ReadToEnd();

                //response.Close();
                //sr.Close();
                //instream.Close();

                if (content.IndexOf("Err:") >= 0) //傳回錯誤
                    throw new Exception(content);

                err = string.Empty;
                return content;

            }

            catch (Exception ex)
            {

                err = ex.Message;
                return string.Empty;

            }
        }
        /// <summary>
        /// 傳送Url POST 資料 得到回應資料 用於欄位內是放圖形資料
        /// </summary>
        /// <param name="url">網址 可帶指令</param>
        /// <param name="postData">要POST 的資料 如SQL指令</param>
        /// <param name="encodeType">編碼方式 如 big5</param>
        /// <param name="err">錯誤訊息</param>
        /// <param name="outBitmap">取得Image</param>
        /// <returns>回應訊息 一般是空的 </returns>
        public static string HttpWebResponsePost(string url, string postData, string encodeType, out string err, out System.Drawing.Bitmap outBitmap)
        {

            //  Stream outstream = null;

            HttpWebRequest request = null;

            Encoding encoding = Encoding.GetEncoding(encodeType);

            byte[] data = encoding.GetBytes(postData);

            try
            {

                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                request = WebRequest.Create(url) as HttpWebRequest;
                //  request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                //  request.ContentType = "application/x-gzip";
                //  request.Accept = "application/x-gzip";
                request.Headers.Add("Accept-Encoding", "gzip,deflate");

                //   CookieContainer cookieContainer = new CookieContainer();

                //   request.CookieContainer = cookieContainer;
                if (cookieContainer == null)
                    cookieContainer = new CookieContainer();

                request.CookieContainer = cookieContainer;

                request.KeepAlive = false;
                request.AllowAutoRedirect = true;

                request.Method = "POST";

                //   request.ContentType = "application/x-www-form-urlencoded";

                request.ContentLength = data.Length;

                Stream outstream = request.GetRequestStream();
                // GZipStream outstream = new GZipStream(request.GetRequestStream(), CompressionMode.Compress); 

                outstream.Write(data, 0, data.Length);

                outstream.Close();
                string content = "";

                HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse();

                Stream responseStream = WebResponse.GetResponseStream();
                if (WebResponse.ContentEncoding.ToLower().IndexOf("gzip") >= 0)
                    responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                else if (WebResponse.ContentEncoding.ToLower().IndexOf("deflate") >= 0)
                    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

                try
                {
                    outBitmap = new System.Drawing.Bitmap(responseStream);
                    content = "";

                }
                catch (Exception ex)
                {
                    content = "Err:" + ex.Message;
                    outBitmap = null;
                    //  responseStream.Position = 0;
                }
                StreamReader Reader = new StreamReader(responseStream, Encoding.Default);
                try
                {
                    content = Reader.ReadToEnd();

                }
                catch (Exception ex)
                {
                    content = "Err:" + ex.Message;

                }

                WebResponse.Close();
                responseStream.Close();
                Reader.Close();



                //response = request.GetResponse() as HttpWebResponse;
                //instream = response.GetResponseStream();
                //sr = new StreamReader(instream, encoding);
                //string content = sr.ReadToEnd();

                //response.Close();
                //sr.Close();
                //instream.Close();

                if (content.IndexOf("Err:") >= 0) //傳回錯誤
                    throw new Exception(content);

                err = string.Empty;
                return content;

            }

            catch (Exception ex)
            {

                err = ex.Message;
                outBitmap = null;
                return string.Empty;

            }
        }
    }

}
