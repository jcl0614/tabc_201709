﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.ComponentModel;

/// <summary>
/// DataemDaily 的摘要描述
/// </summary>
public class DataemDaily
{
    //異動狀態
    public enum MaintainStatus
    {
        Insert,
        Update,
        Delete,
        Select
    }
    #region 取得員工維護記錄
    /// <summary>
    /// 員工維護記錄查詢
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="edDateBeg">維護日期起</param>
    /// <param name="edDateEnd">維護日期訖</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="key">關鍵值</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">每頁筆數</param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet emDailyReader(string edDateBeg, string edDateEnd, string emNo, string key, bool his, string sortExpression, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        string tablename = "sys_emDaily";
        if (his)
            tablename = string.Format("{0}{1}", tablename, "_temp");
        else
            tablename = string.Format("{0}{1}", tablename, "");

        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        //虛擬欄位
        StringBuilder sbVirtualColumn = new StringBuilder("edDate")
            .Append(", emNo")
            .Append(", emName")
            .Append(", muName")
            .Append(", edTableName")
            .Append(", edExecute")
            .Append(", edSource")
            .Append(", edTarget");
        //實體欄位
        StringBuilder sbColumn = new StringBuilder("edDate")
            .Append(", emNo")
            .AppendFormat(", (SELECT emName FROM sys_Employee WHERE emNo={0}.emNo) as emName",  tablename)
            .Append(", muName")
            .Append(", edTableName")
            .Append(", edExecute")
            .Append(", edSource")
            .Append(", edTarget");

        string where = sqlWhere(edDateBeg, edDateEnd, emNo, key, tablename, lSqlParas);

        //索引
        if (sortExpression.Trim().Length == 0)
            sortExpression = "edDate desc";
        sortExpression = sortExpression.Replace("emName", string.Format("(SELECT emName FROM sys_Employee WHERE emNo={0}.emNo)", tablename));


        string cmdtxt = MainControls.sqlSelect(sbVirtualColumn.ToString()
            , sbColumn.ToString()
            , tablename
            , where
            , sortExpression
            , startRowIndex
            , maximumRows);


        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }

    #region 取得總筆數
    /// <summary>
    /// 總筆數
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="edDateBeg">維護日期起</param>
    /// <param name="edDateEnd">維護日期訖</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="key">關鍵值</param>
    /// <returns></returns>
    public static int GetCount(string edDateBeg, string edDateEnd, string emNo, string key, bool his)
    {
        string tablename = "sys_emDaily";
        if (his)
            tablename = string.Format("{0}{1}", tablename, "_temp");
        else
            tablename = string.Format("{0}{1}", tablename, "");

        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*)")
            .Append(" FROM ").Append(tablename);

        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = sqlWhere(edDateBeg, edDateEnd, emNo, key, tablename, lSqlParas);
        sbSql.Append(where);
        //標題
        if( lSqlParas.Count == 0 )
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());
        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray<SqlParameter>());
    }
    #endregion

    /// <summary>
    /// 過濾條件
    /// </summary>
    /// <param name="edDateBeg">維護日期起</param>
    /// <param name="edDateEnd">維護日期訖</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="key">關鍵值</param>
    /// <param name="lSqlParas"></param>
    /// <returns></returns>
    private static string sqlWhere(string edDateBeg, string edDateEnd, string emNo, string key, string tablename, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        //維護日期起
        if (edDateBeg == null) edDateBeg = "";
        edDateBeg= edDateBeg.Trim();
        //維護日期訖
        if (edDateEnd == null) edDateEnd = "";
        edDateEnd = edDateEnd.Trim();
        //員工代碼
        if (emNo == null) emNo = "";
        emNo = emNo.Trim();
        //關鍵值
        if (key == null) key = "";
        key = key.Trim();

        //維護日期起
        if (edDateBeg.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateBeg, edDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateBeg", edDateBeg));
        }
        //維護日期訖
        if (edDateEnd.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateEnd, edDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateEnd", edDateEnd));
        }

        //員工代碼
        if (emNo.Length > 0)
        {
            sbWhere.AppendFormat("(emNo=@emNo OR (SELECT emName FROM sys_Employee WHERE emNo={0}.emNo) LIKE '%'+@emNo+'%') AND ", tablename);
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        }

        //關建值
        if (key.Length > 0)
        {
            sbWhere.Append("(edSource LIKE '%'+@key+'%' OR ")
                .Append("edTarget LIKE '%'+@key+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@key", key));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }

        return sbWhere.ToString();
    }

    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        foreach (DataRow dr in dt.Rows)
        {
            dr["edSource"] = dr["edSource"].ToString();
            dr["edTarget"] = dr["edTarget"].ToString();
        }
    }
    #endregion

    #region 功能表資料行
    public static void emDailyDataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("edDate", "維護日期", "{0:yyyy/MM/dd HH:mm:ss}", 70, true));
        mgv.Columns.Add(DataModel.CreateBoundField("emNo", "代碼", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("emName", "姓名", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("muName", "程式名稱", 100, "Center", true));
        mgv.Columns.Add(DataModel.CreateBoundField("edTableName", "資料表名稱", 100, "Center", true));
        mgv.Columns.Add(DataModel.CreateBoundField("edExecute", "執行", 50));
        mgv.Columns.Add(DataModel.CreateBoundField("edSource", "原始資料", 400, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("edTarget", "異動資料", 400, "Left"));


        mgv.AllowSorting = true;
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema(string connAddress)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, muName, edExecute, edTableName, edSource, edTarget FROM sys_emDaily WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema(connAddress, cmdtxt);
        emds.Tables[0].TableName = "sys_emDaily";
        return emds.Tables[0].Clone();
    }
    public static DataTable GetSchema(SqlTransaction sqlTrans)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, muName, edExecute, edTableName, edSource, edTarget FROM sys_emDaily WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema(sqlTrans, cmdtxt);
        emds.Tables[0].TableName = "sys_emDaily";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 員工維護記錄控制
    /// <summary>
    /// 員工維護記錄
    /// </summary>
    /// <param name="connAddress">平台代碼</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <param name="ms">維護狀態</param>
    public static void emDailyInsert(string connAddress, string emNo, string progId, DataSet ds, MaintainStatus ms)
    {
        switch (ms)
        {
            case MaintainStatus.Insert : //新增
                emDailyInsertAppend(connAddress, emNo, progId, ds);
                break;
            case MaintainStatus.Update: //修改
                emDailyInsertUpdate(connAddress, emNo, progId, ds);
                break;
            case MaintainStatus.Delete: //刪除
                emDailyInsertDelete(connAddress, emNo, progId, ds);
                break;
        }
    }

    public static void emDailyInsert(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds, MaintainStatus ms)
    {
        switch (ms)
        {
            case MaintainStatus.Insert: //新增
                emDailyInsertAppend(sqlTrans, emNo, progId, ds);
                break;
            case MaintainStatus.Update: //修改
                emDailyInsertUpdate(sqlTrans, emNo, progId, ds);
                break;
            case MaintainStatus.Delete: //刪除
                emDailyInsertDelete(sqlTrans, emNo, progId, ds);
                break;
        }
    }

    #endregion

    #region 新增資料庫 - 新增功能
    private static void emDailyInsertAppend(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("sys_emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbData = new StringBuilder();
                foreach (DataColumn col in dt.Columns)
                    sbData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);

                string sData = sbData.Remove(sbData.Length - 3, 3).ToString();
                //if (sData.Length > 4000)
                //    sData = sData.Substring(0, 4000);
                dtemDaily.Rows.Add(emNo, progId, "新增", dt.TableName, " ", sData);
            }
        }
        ds.Tables.Add(dtemDaily);
    }
    private static void emDailyInsertAppend(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("sys_emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbData = new StringBuilder();
                foreach (DataColumn col in dt.Columns)
                    sbData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);

                string sData = sbData.Remove(sbData.Length - 3, 3).ToString();
                //if (sData.Length > 4000)
                //    sData = sData.Substring(0, 4000);
                dtemDaily.Rows.Add(emNo, progId, "新增", dt.TableName, " ", sData);
            }
        }
        ds.Tables.Add(dtemDaily);
    }
    #endregion

    #region 新增資料庫 - 修改功能
    private static void emDailyInsertUpdate(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("sys_emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            #region 產生查詢原始資料語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk= new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.Columns)
            {
                if (dt.PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                {
                    sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                    ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));                    
                }
                sbField.Append(col.ColumnName).Append(",");
            }
            sbSql.Append("SELECT ")
                .Append(sbField.Remove(sbField.Length-1,1).ToString())
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length-5,5).ToString());
            #endregion
            
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                StringBuilder sbTargetData = new StringBuilder();
                StringBuilder sbPrimaryKeyData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {                  
                    if( row.RowState == DataRowState.Modified)
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName, DataRowVersion.Original];
                    else
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                    ////記錄primarykey
                    sbPrimaryKeyData.AppendFormat("{0}={1}　↔　", dc.ColumnName, row[dc.ColumnName]);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(connAddress, sbSql.ToString(), lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                    {
                        //比對資料,資料相同不記錄
                        if (dataSet.Tables[0].Rows[0][col.ColumnName].ToString().Trim().CompareTo(row[col.ColumnName].ToString().Trim()) == 0 ||
                            dataSet.Tables[0].PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                            continue;
                        //原始資料
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);
                        //異動資料
                        sbTargetData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);
                    }
                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbPrimaryKeyData.ToString() + sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        string sTargetData = sbPrimaryKeyData.ToString() + sbTargetData.Remove(sbTargetData.Length - 3, 3).ToString();
                        //if (sTargetData.Length > 4000)
                        //    sTargetData = sTargetData.Substring(0, 4000);
                        dtemDaily.Rows.Add(emNo, progId, "修改", dt.TableName, sSourceData, sTargetData);
                    }
                }
            }
        }
        if (dtemDaily.Rows.Count > 0)
            ds.Tables.Add(dtemDaily);
    }

    private static void emDailyInsertUpdate(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            #region 產生查詢原始資料語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.Columns)
            {
                if (dt.PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                {
                    sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                    ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
                }
                sbField.Append(col.ColumnName).Append(",");
            }
            sbSql.Append("SELECT ")
                .Append(sbField.Remove(sbField.Length - 1, 1).ToString())
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());
            #endregion

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                StringBuilder sbTargetData = new StringBuilder();
                StringBuilder sbPrimaryKeyData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    if( row.RowState == DataRowState.Modified)
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName, DataRowVersion.Original];
                    else
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                    ////記錄primarykey
                    sbPrimaryKeyData.AppendFormat("{0}={1}　↔　", dc.ColumnName, row[dc.ColumnName]);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(sbSql.ToString(), sqlTrans, lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                    {
                        //比對資料,資料相同不記錄
                        if (dataSet.Tables[0].Rows[0][col.ColumnName].ToString().Trim().CompareTo(row[col.ColumnName].ToString().Trim()) == 0 &&
                            !!dataSet.Tables[0].PrimaryKey.Contains<DataColumn>(col))
                            continue;
                        //原始資料
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);
                        //異動資料
                        sbTargetData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);
                    }
                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbPrimaryKeyData.ToString() + sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        string sTargetData = sbPrimaryKeyData.ToString() + sbTargetData.Remove(sbTargetData.Length - 3, 3).ToString();
                        //if (sTargetData.Length > 4000)
                        //    sTargetData = sTargetData.Substring(0, 4000);
                        dtemDaily.Rows.Add(emNo, progId, "修改", dt.TableName, sSourceData, sTargetData);
                    }
                }
            }
        }
        if (dtemDaily.Rows.Count > 0)
            ds.Tables.Add(dtemDaily);
    }
    #endregion

    #region 新增資料庫 - 刪除功能
    private static void emDailyInsertDelete(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("sys_emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            //產生查詢語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.PrimaryKey)
            {
                sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
            }
            sbSql.Append("SELECT * ")
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(connAddress, sbSql.ToString(), lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);

                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        dtemDaily.Rows.Add(emNo, progId, "刪除", dt.TableName, sSourceData, " ");
                    }
                }
            }
        }
        if( dtemDaily.Rows.Count > 0)
            ds.Tables.Add(dtemDaily);
    }
    private static void emDailyInsertDelete(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有emDaily資料表，則移除
        if (ds.Tables.Contains("sys_emDaily")) ds.Tables.Remove("sys_emDaily");
        //取得員工維護記錄檔資料結構
        DataTable dtemDaily = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            //產生查詢語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.PrimaryKey)
            {
                sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
            }
            sbSql.Append("SELECT * ")
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(sbSql.ToString(), sqlTrans, lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dt.Columns)
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);

                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        dtemDaily.Rows.Add(emNo, progId, "刪除", dt.TableName, sSourceData, " ");
                    }
                }
            }
        }
        if (dtemDaily.Rows.Count > 0)
            ds.Tables.Add(dtemDaily);
    }
    #endregion

    #region 手動新增
    public static RServiceProvider emDailyInsert(SqlTransaction sqlTrans, string emNo, string muName, string edTableName, string edSource, string edTarget, MaintainStatus ms)
    {
        string edExecute = "";
        switch (ms)
        {
            case MaintainStatus.Insert :
                edExecute = "新增";
                break;
            case MaintainStatus.Delete :
                edExecute = "刪除";
                break;
            case MaintainStatus.Update:
                edExecute = "修改";
                break;
        }
        string updatecmd = "INSERT INTO sys_emDaily (emNo, muName, edExecute, edTableName, edSource, edTarget) VALUES (@emNo, @muName, @edExecute, @edTableName, @edSource, @edTarget) ";
        List<SqlParameter> lParas = new List<SqlParameter>();
        lParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        lParas.Add(SqlAccess.CreateSqlParameter("@muName", muName));
        lParas.Add(SqlAccess.CreateSqlParameter("@edExecute", edExecute));
        lParas.Add(SqlAccess.CreateSqlParameter("@edTableName", edTableName));
        lParas.Add(SqlAccess.CreateSqlParameter("@edSource", edSource));
        lParas.Add(SqlAccess.CreateSqlParameter("@edTarget", edTarget));

        return SqlAccess.SqlcommandNonQueryExecute(sqlTrans.Connection, sqlTrans, updatecmd, lParas.ToArray());
    }
    #endregion

    #region 結轉資料至暫存 -- 保留30天內資料
    public static RServiceProvider emDailyDelete()
    {
        RServiceProvider rsp = new RServiceProvider();
        string cmdtxt = "INSERT INTO sys_emDaily{OS} SELECT emNo, muName, edExecute, edTableName, edSource, edTarget, edDate FROM sys_emDaily WHERE DATEDIFF( d, edDate, GETDATE()) > 30; "
                        + "DELETE FROM sys_emDaily WHERE DATEDIFF( d, edDate, GETDATE()) > 30";
        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdtxt.Replace("{OS}", "_temp"));
        if (rsp.Result== false)
        {
            rsp.ReturnMessage = string.Format("結轉資料錯誤:{0}", rsp.ReturnMessage);
                return rsp;
        }
        return rsp;
    }
    #endregion

    #region 匯出Excel
    #endregion

}
