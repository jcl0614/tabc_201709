﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataCustIndustPolm
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_IP_TransNo(string IP_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("IP_TransNo", SqlAccess.CreateSqlParameter("@IP_TransNo", IP_TransNo));

        string cmdtxt = "SELECT * FROM tblCustIndustPolm WHERE IP_TransNo=@IP_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_CU_TransNo_IP_Polno_IP_ComSname(string CU_TransNo, string IP_Polno, string IP_ComSname)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_TransNo", SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));
        dParas.Add("IP_Polno", SqlAccess.CreateSqlParameter("@IP_Polno", IP_Polno));
        dParas.Add("IP_ComSname", SqlAccess.CreateSqlParameter("@IP_ComSname", IP_ComSname));

        string cmdtxt = "SELECT * FROM tblCustIndustPolm WHERE CU_TransNo=@CU_TransNo AND IP_Polno=@IP_Polno AND IP_ComSname=@IP_ComSname";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string IP_ComSName,
        string IP_Polno,
        string IP_StartDate_s,
        string IP_StartDate_e,
        string IP_Memo,
        string AccID,
        int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(
            CU_TransNo,
             CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             IP_ComSName,
             IP_Polno,
             IP_StartDate_s,
             IP_StartDate_e,
             IP_Memo,
             AccID,
            lSqlParas);
        string sort = "IP_Polno";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblCustIndustPolm", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string IP_ComSName,
        string IP_Polno,
        string IP_StartDate_s,
        string IP_StartDate_e,
        string IP_Memo,
        string AccID
        )
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblCustIndustPolm");
        string where = whereString(
            CU_TransNo,
             CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             IP_ComSName,
             IP_Polno,
             IP_StartDate_s,
             IP_StartDate_e,
             IP_Memo,
             AccID,
            lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string IP_ComSName,
        string IP_Polno,
        string IP_StartDate_s,
        string IP_StartDate_e,
        string IP_Memo,
        string AccID,
        List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (CU_TransNo.Length > 0)
        {
            sbWhere.Append(" CU_TransNo=@CU_TransNo AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));
        }
        if (CU_CustName.Length > 0)
        {

        }
        if (CU_ID.Length > 0)
        {

        }
        if (CU_Sex.Length > 0)
        {

        }
        if (CU_Age.Length > 0)
        {

        }
        if (CU_Marry.Length > 0)
        {

        }
        if (CU_CustType.Length > 0)
        {

        }
        if (CU_CustSource.Length > 0)
        {

        }
        if (CU_ContactMode.Length > 0)
        {

        }
        if (CU_IsShow.Length > 0)
        {

        }
        if (Mobile.Length > 0)
        {

        }
        if (Address.Length > 0)
        {

        }
        if (IP_ComSName.Length > 0)
        {
            sbWhere.Append(" IP_ComSName LIKE '%'+@IP_ComSName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@IP_ComSName", IP_ComSName));
        }
        if (IP_Polno.Length > 0)
        {
            sbWhere.Append(" IP_Polno=@IP_Polno AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@IP_Polno", IP_Polno));
        }
        if (IP_StartDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @IP_StartDate_s, IP_StartDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@IP_StartDate_s", IP_StartDate_s));
        }
        if (IP_StartDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @IP_StartDate_e, IP_StartDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@IP_StartDate_e", IP_StartDate_e));
        }
        if (IP_Memo.Length > 0)
        {
            sbWhere.Append(" IP_Memo LIKE '%'+@IP_Memo+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@IP_Memo", IP_Memo));
        }
        if (AccID.Length > 0)
        {
            sbWhere.Append(" AccID=@AccID AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("IP_StartDate_str", typeof(string));
        dt.Columns.Add("IP_EndDate_str", typeof(string));
        dt.Columns.Add("IP_Insuterm_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["IP_StartDate"].ToString() != "")
                dr["IP_StartDate_str"] = DateTime.Parse(dr["IP_StartDate"].ToString()).ToString("yyyy/MM/dd");
            if (dr["IP_EndDate"].ToString() != "")
                dr["IP_EndDate_str"] = DateTime.Parse(dr["IP_EndDate"].ToString()).ToString("yyyy/MM/dd");
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("080", dr["IP_Insuterm"].ToString());
            if (dr_Phrase != null)
            {
                dr["IP_Insuterm_str"] = dr_Phrase["TypeName"].ToString();
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_ProdKind", "險種", 60, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_ComSName", "投保公司", 100, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_Polno", "保單號碼", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_StartDate_str", "投保日期", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_EndDate_str", "到期日期", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_Insuterm_str", "保險期間", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_CarOwner", "車主/屋主", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("IP_PlateNo", "牌照號碼", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider Append(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    public static RServiceProvider Update(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblCustIndustPolm where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblCustIndustPolm";
        return emds.Tables[0].Clone();
    }
    #endregion




}