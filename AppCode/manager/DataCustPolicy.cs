﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataCustPolicy
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_PO_TransNo(string PO_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("PO_TransNo", SqlAccess.CreateSqlParameter("@PO_TransNo", PO_TransNo));

        string cmdtxt = "SELECT * FROM tblCustPolicy WHERE PO_TransNo=@PO_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_PO_PKSKey(string PO_PKSKey)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("PO_PKSKey", SqlAccess.CreateSqlParameter("@PO_PKSKey", PO_PKSKey));

        string cmdtxt = "SELECT * FROM tblCustPolicy WHERE PO_PKSKey=@PO_PKSKey";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_CU_TransNo(string CU_TransNo)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_TransNo", SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));

        string cmdtxt = "SELECT * FROM tblCustPolicy WHERE CU_TransNo=@CU_TransNo and ModiState<>'D' order by PO_PolNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s,
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID,
        int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(
            CU_TransNo,
             CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             PO_ComSName,
             PO_PolNo,
             PO_AccureDate_s,
             PO_AccureDate_e,
             PO_Polmmemo,
             AccID,
            lSqlParas);
        string sort = "PO_PolNo";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblCustPolicy", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s,
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID
        )
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblCustPolicy");
        string where = whereString(
            CU_TransNo,
             CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             PO_ComSName,
             PO_PolNo,
             PO_AccureDate_s,
             PO_AccureDate_e,
             PO_Polmmemo,
             AccID,
            lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(
        string CU_TransNo,
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s,
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID,
        List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (CU_TransNo.Length > 0)
        {
            sbWhere.Append(" CU_TransNo=@CU_TransNo AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));
        }
        if (CU_CustName.Length > 0)
        {
            
        }
        if (CU_ID.Length > 0)
        {
            
        }
        if (CU_Sex.Length > 0)
        {
            
        }
        if (CU_Age.Length > 0)
        {
            
        }
        if (CU_Marry.Length > 0)
        {
            
        }
        if (CU_CustType.Length > 0)
        {
            
        }
        if (CU_CustSource.Length > 0)
        {
            
        }
        if (CU_ContactMode.Length > 0)
        {
            
        }
        if (CU_IsShow.Length > 0)
        {
            
        }
        if (Mobile.Length > 0)
        {

        }
        if (Address.Length > 0)
        {

        }
        if (PO_ComSName.Length > 0)
        {
            sbWhere.Append(" PO_ComSName LIKE '%'+@PO_ComSName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PO_ComSName", PO_ComSName));
        }
        if (PO_PolNo.Length > 0)
        {
            sbWhere.Append(" PO_PolNo=@PO_PolNo AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PO_PolNo", PO_PolNo));
        }
        if (PO_AccureDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @PO_AccureDate_s, PO_AccureDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PO_AccureDate_s", PO_AccureDate_s));
        }
        if (PO_AccureDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @PO_AccureDate_e, PO_AccureDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PO_AccureDate_e", PO_AccureDate_e));
        }
        if (PO_Polmmemo.Length > 0)
        {
            sbWhere.Append(" PO_Polmmemo LIKE '%'+@PO_Polmmemo+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PO_Polmmemo", PO_Polmmemo));
        }
        if (AccID.Length > 0)
        {
            sbWhere.Append(" AccID=@AccID AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("PO_AccureDate_str", typeof(string));
        dt.Columns.Add("PO_PaidToDate_str", typeof(string));
        dt.Columns.Add("PO_Mode_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if(dr["PO_AccureDate"].ToString() != "")
                dr["PO_AccureDate_str"] = DateTime.Parse(dr["PO_AccureDate"].ToString()).ToString("yyyy/MM/dd");
            if (dr["PO_PaidToDate"].ToString() != "")
                dr["PO_PaidToDate_str"] = DateTime.Parse(dr["PO_PaidToDate"].ToString()).ToString("yyyy/MM/dd");
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("028", dr["PO_Mode"].ToString());
            if (dr_Phrase != null)
            {
                dr["PO_Mode_str"] = dr_Phrase["TypeName"].ToString();
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_ComSName", "投保公司", 100, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_PolNo", "保單號碼", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_OwnerName", "要保人", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_AccureDate_str", "投保日期", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_InsureAge", "投保年齡", 40, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_Mode_str", "繳别", 70, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_PaidToDate_str", "繳費日期", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_ModePrem", "應繳保費", 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("PO_PolState", "保單狀態", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider Append(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    public static RServiceProvider Update(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblCustPolicy where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblCustPolicy";
        return emds.Tables[0].Clone();
    }
    #endregion




}