﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataETeacherFee
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_E6_TransNo(string E6_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("E6_TransNo", SqlAccess.CreateSqlParameter("@E6_TransNo", E6_TransNo));

        string cmdtxt = "SELECT * FROM tblETeacherFee WHERE E6_TransNo=@E6_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_ET_TransNo(string ET_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ET_TransNo", SqlAccess.CreateSqlParameter("@ET_TransNo", ET_TransNo));

        string cmdtxt = "SELECT * FROM tblETeacherFee WHERE ET_TransNo=@ET_TransNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string EC_CName, string EC_Code, string ET_TName, string EC_Mode, string EC_Type, string EC_Rating, string EC_SDate_s, string EC_SDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EC_CName, EC_Code, ET_TName, EC_Mode, EC_Type, EC_Rating, EC_SDate_s, EC_SDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblETeacherFee", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EC_CName, string EC_Code, string ET_TName, string EC_Mode, string EC_Type, string EC_Rating, string EC_SDate_s, string EC_SDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblETeacherFee");
        string where = whereString(EC_CName, EC_Code, ET_TName, EC_Mode, EC_Type, EC_Rating, EC_SDate_s, EC_SDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EC_CName, string EC_Code, string ET_TName, string EC_Mode, string EC_Type, string EC_Rating, string EC_SDate_s, string EC_SDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" (select EC_CName from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo) LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Code.Length > 0)
        {
            sbWhere.Append(" (select EC_Code from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo) LIKE '%'+@EC_Code+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        }
        if (ET_TName.Length > 0)
        {
            sbWhere.Append(" (select ET_TName from tblETeacher where tblETeacher.ET_TransNo=tblETeacherFee.ET_TransNo) LIKE '%'+@ET_TName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TName", ET_TName));
        }
        if (EC_Type != "")
        {
            sbWhere.Append(" (select EC_Type from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo)=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" (select EC_Rating from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo)=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_Mode != "")
        {
            sbWhere.Append(" (select EC_Mode from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo)=@EC_Mode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Mode", EC_Mode));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblETeacherFee.EC_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EC_SDate_Str", typeof(string));
        dt.Columns.Add("EC_CName", typeof(string));
        dt.Columns.Add("ET_Name", typeof(string));
        dt.Columns.Add("EC_Mode_Str", typeof(string));
        dt.Columns.Add("EC_Type", typeof(string));
        dt.Columns.Add("EC_Rating", typeof(string));
        dt.Columns.Add("print_link", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_ecourse = DataECourse.DataReader_EC_TransNo(dr["EC_TransNo"].ToString());
            if (dr_ecourse != null)
            {
                if (dr_ecourse["EC_Category"].ToString() == "2")
                {
                    dr["EC_CName"] = dr_ecourse["EC_CName"].ToString();
                    dr["EC_Type"] = dr_ecourse["EC_Type"].ToString();
                    dr["EC_Rating"] = dr_ecourse["EC_Rating"].ToString();
                    dr["EC_SDate_Str"] = string.Format("{0}&nbsp;&nbsp;{1}~{2}", DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_ecourse["EC_STime"].ToString(), dr_ecourse["EC_ETime"].ToString());
                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("130", dr_ecourse["EC_Mode"].ToString());
                    if (dr_Phrase != null)
                    {
                        dr["EC_Mode_Str"] = dr_Phrase["TypeName"].ToString();
                    }
                }
            }
            DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr["ET_TransNo"].ToString());
            if (dr_ET != null)
            {
                dr["ET_Name"] = dr_ET["ET_TName"].ToString();
            }

            dr["print_link"] = string.Format("<a href=\"/manager/ETeacherFee_print?id={0}\" target=\"_blank\">列印</a>", dr["E6_TransNo"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Name", "講師", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Mode_Str", "型態", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("print_link", "簽收表", false, 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblETeacherFee where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblETeacherFee";
        return emds.Tables[0].Clone();
    }
    #endregion





}