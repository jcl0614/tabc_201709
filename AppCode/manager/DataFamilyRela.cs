﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataFamilyRela
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_FA_TransNo(string FA_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("FA_TransNo", SqlAccess.CreateSqlParameter("@FA_TransNo", FA_TransNo));

        string cmdtxt = "SELECT * FROM tblFamilyRela WHERE FA_TransNo=@FA_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_CU_TransNo_FA_CUTransNo(string CU_TransNo, string FA_CUTransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_TransNo", SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));
        dParas.Add("FA_CUTransNo", SqlAccess.CreateSqlParameter("@FA_CUTransNo", FA_CUTransNo));

        string cmdtxt = "SELECT * FROM tblFamilyRela WHERE CU_TransNo=@CU_TransNo and FA_CUTransNo=@FA_CUTransNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_CU_TransNo(string CU_TransNo)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_TransNo", SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));

        StringBuilder columns = new StringBuilder();
        columns.Append("FA_TransNo ");
        columns.Append(", FA_UidNo ");
        columns.Append(", tblCustomer.CU_TransNo ");
        columns.Append(", tblCustomer.CU_UidNo ");
        columns.Append(", FA_CUTransNo ");
        columns.Append(", FA_CUUidNo ");
        columns.Append(", FA_RelaName ");
        columns.Append(", FA_RelaNo ");
        columns.Append(", tblCustomer.CU_CustName ");
        columns.Append(", tblCustomer.CU_ID ");
        columns.Append(", tblCustomer.CU_Sex ");
        columns.Append(", CONVERT(varchar(10), tblCustomer.CU_Birth, 111) CU_Birth ");
        columns.Append(", tblCustomer.CU_Age ");
        columns.Append(", tblCustomer.CU_JobPA ");
        columns.Append(",tblFamilyRela.AccID ");
        columns.Append(", tblFamilyRela.ComyCode ");
        columns.Append(", tblFamilyRela.ModiDate ");
        columns.Append(", tblFamilyRela.Server_ModiDate ");
        columns.Append(", tblFamilyRela.ModiState ");
        string cmdtxt = string.Format("SELECT {0} FROM tblFamilyRela left join tblCustomer on tblFamilyRela.FA_CUTransNo=tblCustomer.CU_TransNo WHERE tblFamilyRela.CU_TransNo=@CU_TransNo and tblFamilyRela.ModiState<>'D' order by FA_RelaNo", columns);

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblFamilyRela", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblFamilyRela");
        string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EN_Year.Length > 0)
        {
            sbWhere.Append(" EN_Year=@EN_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Year", EN_Year));
        }
        if (EN_Type.Length > 0)
        {
            sbWhere.Append(" EN_Type=@EN_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Type", EN_Type));
        }
        if (EN_AccID.Length > 0)
        {
            sbWhere.Append(" (EN_AccID LIKE '%'+@EN_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblFamilyRela.EN_AccID) LIKE '%'+@EN_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_AccID", EN_AccID));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    public static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {

        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "編輯", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("FA_RelaName", "關係", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_CustName", "客戶名稱/姓名", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_ID", "身分證號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_Sex", "性別", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_Birth", "生日", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_Age", "投保年齡", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_JobPA", "職級", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider Append(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    public static RServiceProvider Update(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblFamilyRela where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblFamilyRela";
        return emds.Tables[0].Clone();
    }
    #endregion




}