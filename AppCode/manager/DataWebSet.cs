﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// DataWebSet 的摘要描述
/// </summary>
public class DataWebSet
{
    #region 讀取網站基本資料
    //新增
    public static string dataReader(string name)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT value FROM sys_WebSet WHERE name=@name";
        DataTable dt = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@name", name)).Tables[0];

        if (dt.Rows.Count != 0)
            return dt.Rows[0][0].ToString().Trim();
        else
            throw new Exception(string.Format("參數[{0}]不存在於資料表[sys_WebSet]", name));
    }
    #endregion

    #region 更新網站基本資料
    public static RServiceProvider updateWebSet(String[] NameValue)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string[] s = { "", "" };
        StringBuilder cmdText = new StringBuilder();
        for (int i = 0; i < NameValue.Length ; i++)
        {
            s[0] = NameValue[i].Substring(0, NameValue[i].IndexOf(","));
            s[1] = NameValue[i].Substring(NameValue[i].IndexOf(",") + 1, NameValue[i].Length - NameValue[i].IndexOf(",") - 1);
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@value"+i.ToString(), s[1]));
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@name" + i.ToString(), s[0]));
            cmdText.AppendFormat(" UPDATE sys_WebSet SET value=@value{0} WHERE name=@name{1} ;", i.ToString(), i.ToString());
        }
            
        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdText.ToString(), lSqlParas.ToArray());

        return rsp;
    }
    public static RServiceProvider updateWebSet(string name, string value)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder cmdText = new StringBuilder();

        lSqlParas.Add(SqlAccess.CreateSqlParameter("@value", value));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@name", name));
        cmdText.Append(" UPDATE sys_WebSet SET value=@value WHERE name=@name ");

        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdText.ToString(), lSqlParas.ToArray());

        return rsp;
    }
    #endregion

}