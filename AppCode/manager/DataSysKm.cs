﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataSysKm
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string mulayer)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM sys_SysKm WHERE mulayer=@mulayer AND enable=1 ORDER BY  sort";
        else
            cmdtxt = "SELECT * FROM sys_SysKm WHERE mulayer=@mulayer ORDER BY  sort";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@mulayer", mulayer));
        return emds;
    }
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_SysKm WHERE enable=1 ORDER BY  sort";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataTable id_DataReader(string id)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT *, (select emName + '(' + emNo + ')' from sys_Employee where sys_Employee.emNo=sys_SysKm.emNo) AS emName, (SELECT CONVERT(varchar(10), updateDate, 120)) AS UpdateDateStr FROM sys_SysKm WHERE id=@id";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@id", id));
        return emds.Tables[0];
    }
    public static DataSet baseDataReader(string mulayer)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_SysKm WHERE mulayer=@mulayer AND enable=1 ORDER BY  sort";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@mulayer", mulayer));
        return emds;
    }
    public static DataSet DataReader(string name, string mulayer, string enable, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(name, mulayer, enable, lSqlParas);
        string sort = "mulayer,sort";
        string Vcolumns = "id, mulayer, mulayerName, name, description, sort, enable, emNo, emName, updateDate, UpdateDateStr";
        string columns = "id, mulayer, (SELECT name FROM sys_MenuLayer WHERE sys_MenuLayer.id = sys_SysKm.mulayer) as mulayerName, name, description, sort, enable, emNo, (select emName + '(' + emNo + ')' from sys_Employee where sys_Employee.emNo=sys_SysKm.emNo) AS emName, updateDate, (SELECT CONVERT(varchar(10), updateDate, 120)) AS UpdateDateStr";
        string cmdtxt = MainControls.sqlSelect(Vcolumns, columns, "sys_SysKm", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string name, string mulayer, string enable)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_SysKm");
        string where = whereString(name, mulayer, enable, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string name, string mulayer, string enable, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (name.Length > 0)
        {
            sbWhere.Append(" name LIKE '%'+@name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@name", name));
        }
        if (mulayer.Length > 0)
        {
            sbWhere.Append(" mulayer=@mulayer AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@mulayer", mulayer));
        }
        if (enable != "")
        {
            sbWhere.Append(" enable=@enable AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@enable", enable));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("Enable", typeof(string));    //啟用
        foreach (DataRow dr in dt.Rows)
        {
            dr["Enable"] = string.Format("{0}", dr["enable"].ToString().Trim() == "True" ? true : false);
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("id", "編號", 40, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("mulayerName", "類別", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("name", "功能選單", 380, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("sort", "順序", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("Enable", "啟用", 40, "center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("mulayer", "類別", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("description", "說明文字", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("emNo", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("emName", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("updateDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("enable", "啟用", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, mulayer, name, description, sort, enable, emNo, updateDate FROM sys_SysKm where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_SysKm";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 顯示順序調整異動資料表
    public static RServiceProvider SortUpdate(ListBox mListBox)
    {
        RServiceProvider rsp = new RServiceProvider();
        rsp.Result = false;
        int mSort = 1;

        try
        {
            foreach (ListItem lm in mListBox.Items)
            {
                SqlAccess.SqlcommandNonQueryExecute("0", string.Format("update sys_SysKm set sort='{0}'  where  id='{1}'", mSort.ToString(), lm.Value));
                mSort++;
            }
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.Result = false;
            rsp.ReturnMessage = ex.Message;
        }
        return rsp;
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="menuLayerDDL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void KmDropDownList(DropDownList DDL, bool all, string allString, string mulayer)
    {
        DDL.Items.Clear();
        if (all)
            DDL.Items.Add(new ListItem(allString, ""));
        DataSet emds = baseDataReader(false, mulayer);
        if (emds != null && emds.Tables.Count > 0)
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(dr["name"].ToString(), dr["id"].ToString()));
        }
    }
    #endregion   

}