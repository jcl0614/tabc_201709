﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Text;

/// <summary>
/// EmployeeLimit 的摘要描述
/// </summary>
public class DataMenuLayer
{
    #region  系統選單資料讀取讀取
    /// <summary>
    /// 系統選單資料讀取讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet DataMenuLayerReader(Boolean b)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM sys_MenuLayer WHERE enable=1 ORDER BY  sort";
        else
            cmdtxt = "SELECT * FROM sys_MenuLayer ORDER BY  sort";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static Boolean DataMenuLayerActive(string muHyperLink, string id)
    {
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muHyperLink", muHyperLink));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@id", id));

        DataSet emds = new DataSet();
        string cmdtxt = "SELECT (select count(*) from sys_Menu where sys_Menu.muLayer=sys_MenuLayer.id and sys_Menu.muHyperLink=@muHyperLink) AS Active FROM sys_MenuLayer WHERE id=@id";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        if (emds.Tables[0].Rows.Count == 1)
        {
            if (emds.Tables[0].Rows[0][0].ToString() != "0")
                return true;
            else
                return false;
        }
        else
            return false;
    }
    public static Boolean MenuLayerEnableCheck(string id)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_MenuLayer WHERE id=@id";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@id", id));
        if (emds.Tables[0].Rows.Count == 1)
            return Boolean.Parse(emds.Tables[0].Rows[0]["enable"].ToString());
        else
            return false;
    }
    /// <summary>
    /// 查詢員工資料
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet DataMenuLayerReader(string sortExpression, int startRowIndex, int maximumRows)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbVirtualColumn = new StringBuilder("id")
                .Append(", name")
                .Append(", sort")
                .Append(", enable");

        StringBuilder sbColumn = new StringBuilder("id")
                .Append(", name")
                .Append(", sort")
                .Append(", enable");

        if (sortExpression.Trim().Length == 0)
            sortExpression = "sort";

        string where = "";

        string cmdtxt = MainControls.sqlSelect(sbVirtualColumn.ToString()
            , sbColumn.ToString()
            , "sys_MenuLayer"
            , where
            , sortExpression
            , startRowIndex
            , maximumRows);

        if (lSqlParas.Count == 0)
            return SqlAccess.SqlcommandExecute("0", cmdtxt);

        return SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray<SqlParameter>());
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount()
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_MenuLayer");
        string where = "";
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 功能表資料行
    public static void MenuLayerDataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("id", "編號", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("name", "選單名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("sort", "順序", 100));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("enable", "啟用", 50));
        mgv.Columns.Add(DataModel.CreateCommandField(ButtonType.Button, false, true, false, 150));
        (mgv.Columns[0] as BoundField).ReadOnly = true;
    }
    #endregion

    #region 更新資料庫
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void MenuLayerUpdate(string id, string emNo, string progId, string name, string sort, bool enable)
    {
        DataTable dt = GetSchema();
        dt.Rows.Add(id, name, sort, enable);
        DataSet emds = new DataSet();
        emds.Tables.Add(dt);

        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, emds, DataemDaily.MaintainStatus.Update);
        SqlAccess.SqlCommandUpdate("0", emds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, name, sort, enable FROM sys_MenuLayer WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_MenuLayer";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="menuLayerDDL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void menuLayerReaderDropDownList(DropDownList DDL, bool all, string allString)
    {
        DDL.Items.Clear();
        if (all)
            DDL.Items.Add(new ListItem(allString, ""));
        DataSet emds = DataMenuLayerReader(true);
        if (emds != null && emds.Tables.Count > 0)
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0}) {1}", dr["id"].ToString(), dr["name"].ToString()), dr["id"].ToString()));
        }
    }
    #endregion   

    #region 顯示順序調整異動資料表
    public static RServiceProvider SortUpdate(ListBox mListBox)
    {
        RServiceProvider rsp = new RServiceProvider();
        rsp.Result = false;
        int mSort = 1;

        try
        {
            foreach (ListItem lm in mListBox.Items)
            {
                SqlAccess.SqlcommandNonQueryExecute("0", string.Format("update sys_MenuLayer set sort='{0}'  where  id='{1}'", mSort.ToString(), lm.Value));
                mSort++;
            }
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.Result = false;
            rsp.ReturnMessage = ex.Message;
        }
        return rsp;
    }
    #endregion
}
