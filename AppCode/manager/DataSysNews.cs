﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;

/// <summary>
/// DataLeftLinkClass 的摘要描述
/// </summary>
public class DataSysNews
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT TOP 5 *, (case class when '1' then '系統公告' else '行事曆' end) AS className, (SELECT CONVERT(varchar(10), addDate, 120)) AS DateStr FROM sys_SysNews ORDER BY addDate desc";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet DataReader(string Class, string title, string emNo, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(Class, title, emNo, lSqlParas);
        string sort = "addDate desc";
        string Vcolumns = "id, class, className, title, description, emNo, emName, addDate, DateStr";
        string columns = "id, class, (case class when '1' then '系統公告' else '行事曆' end) AS className, title, description, emNo, (select emName + '(' + emNo + ')' from sys_Employee where sys_Employee.emNo=sys_SysNews.emNo) AS emName, addDate, (SELECT CONVERT(varchar(10), addDate, 111)) AS DateStr";
        string cmdtxt = MainControls.sqlSelect(Vcolumns, columns, "sys_SysNews", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string Class, string title, string emNo)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_SysNews");
        string where = whereString(Class, title, emNo, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string Class, string title, string emNo, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (Class.Length > 0)
        {
            sbWhere.Append(" class=@Class AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@Class", Class));
        }
        if (title.Length > 0)
        {
            sbWhere.Append(" title LIKE '%'+@title+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@title", title));
        }
        if (emNo != "")
        {
            sbWhere.Append(" ((select emNo from sys_Employee where sys_Employee.emNo=sys_SysNews.emNo) LIKE '%'+@emNo+'%' OR (select emName from sys_Employee where sys_Employee.emNo=sys_SysNews.emNo) LIKE '%'+@emNo+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("className", "分類", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("title", "標題", false, 400, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("DateStr", "日期", 100));
        mgv.Columns.Add(DataModel.CreateBoundField("emName", "發佈人員", false, 150, "Center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "key", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("class", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("description", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("addDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("emNo", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  id, class, title, description, emNo FROM sys_SysNews where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_SysNews";
        return emds.Tables[0].Clone();
    }
    #endregion


}