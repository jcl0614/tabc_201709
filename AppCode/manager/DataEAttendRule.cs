﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEAttendRule
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EU_TransNo(string EU_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EU_TransNo", SqlAccess.CreateSqlParameter("@EU_TransNo", EU_TransNo));

        string cmdtxt = "SELECT * FROM tblEAttendRule WHERE EU_TransNo=@EU_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EU_Name, string EU_Mode, string EU_AttFreq, string EU_AttStatus, string EU_Staff, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EU_Name, EU_Mode, EU_AttFreq, EU_AttStatus, EU_Staff, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttendRule", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EU_Name, string EU_Mode, string EU_AttFreq, string EU_AttStatus, string EU_Staff)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttendRule");
        string where = whereString(EU_Name, EU_Mode, EU_AttFreq, EU_AttStatus, EU_Staff, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EU_Name, string EU_Mode, string EU_AttFreq, string EU_AttStatus, string EU_Staff, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EU_Name.Length > 0)
        {
            sbWhere.Append(" EU_Name LIKE '%'+@EU_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EU_Name", EU_Name));
        }
        if (EU_Mode.Length > 0)
        {
            sbWhere.Append(" EU_Mode=@EU_Mode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EU_Mode", EU_Mode));
        }
        if (EU_AttFreq.Length > 0)
        {
            sbWhere.Append(" EU_AttFreq=@EU_AttFreq AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EU_AttFreq", EU_AttFreq));
        }
        if (EU_AttStatus.Length > 0)
        {
            sbWhere.Append(" EU_AttStatus=@EU_AttStatus AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EU_AttStatus", EU_AttStatus));
        }
        if (EU_Staff.Length > 0)
        {
            if (EU_Staff == "0")
                sbWhere.Append(" EU_Staff=@EU_Staff AND ");
            else
                sbWhere.Append(" EU_Staff LIKE '%'+@EU_Staff+';%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EU_Staff", EU_Staff));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EU_Mode_str", typeof(string));
        dt.Columns.Add("EU_AttFreq_str", typeof(string));
        dt.Columns.Add("EU_AttStatus_str", typeof(string));
        dt.Columns.Add("EU_Staff_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("130", dr["EU_Mode"].ToString());
            if (dr_Phrase != null)
                dr["EU_Mode_str"] = dr_Phrase["TypeName"].ToString();
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("133", dr["EU_AttFreq"].ToString());
            if (dr_Phrase != null)
                dr["EU_AttFreq_str"] = dr_Phrase["TypeName"].ToString();
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("132", dr["EU_AttStatus"].ToString());
            if (dr_Phrase != null)
                dr["EU_AttStatus_str"] = dr_Phrase["TypeName"].ToString();
            if (dr["EU_Staff"].ToString().IndexOf(";") != -1)
            {
                string[] EU_Staff = dr["EU_Staff"].ToString().Split(';');
                StringBuilder sb_EU_Staff = new StringBuilder();
                foreach (string item in EU_Staff)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("E003", item);
                        if (dr_Phrase != null)
                            sb_EU_Staff.AppendFormat("{0}<br>", dr_Phrase["TypeName"].ToString());
                    }
                }
                dr["EU_Staff_str"] = sb_EU_Staff.ToString();
            }
            else
                dr["EU_Staff_str"] = "全體人員";
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_Name", "規則名稱", false, 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_Mode_str", "課程型態", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_AttFreq_str", "出席頻率", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_AttStatus_str", "出席狀態", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_Staff_str", "適用人員", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EU_Times", "次數", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEAttendRule where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEAttendRule";
        return emds.Tables[0].Clone();
    }
    #endregion




}