﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
///  的摘要描述
/// </summary>
public class DataScheduleM
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>

    public static DataRow DataReader_SC_TransNo(string SC_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("SC_TransNo", SqlAccess.CreateSqlParameter("@SC_TransNo", SC_TransNo));

        string cmdtxt = "SELECT * FROM tblScheduleM WHERE SC_TransNo=@SC_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_AccID(string AccID)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        string cmdtxt = "SELECT * FROM tblScheduleM WHERE AccID=@AccID and ModiState<>'D'";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_AccID_Date(string AccID, string SC_Date)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));
        dParas.Add("SC_StartTime", SqlAccess.CreateSqlParameter("@SC_StartTime", SC_Date));
        dParas.Add("SC_EndTime", SqlAccess.CreateSqlParameter("@SC_EndTime", SC_Date));

        StringBuilder cmdtxt = new StringBuilder("SELECT * FROM tblScheduleM WHERE AccID=@AccID and ModiState<>'D' ");
        cmdtxt.Append("AND (DATEDIFF(d, @SC_StartTime, SC_StartTime)>=0 OR DATEDIFF(d, @SC_StartTime, SC_EndTime)>=0) ");
        cmdtxt.Append("AND (DATEDIFF(d, @SC_EndTime, SC_StartTime)<=0 OR DATEDIFF(d, @SC_EndTime, SC_EndTime)<=0) ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string SC_CustName, string SC_CustType, string SC_Kind, string SC_Plan, string SC_State, string SC_Inreply, string SC_StartTime, string SC_EndTime, string AccID, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(SC_CustName, SC_CustType, SC_Kind, SC_Plan, SC_State, SC_Inreply, SC_StartTime, SC_EndTime, AccID, lSqlParas);
        string sort = "SC_StartTime desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblScheduleM", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string SC_CustName, string SC_CustType, string SC_Kind, string SC_Plan, string SC_State, string SC_Inreply, string SC_StartTime, string SC_EndTime, string AccID)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblScheduleM");
        string where = whereString(SC_CustName, SC_CustType, SC_Kind, SC_Plan, SC_State, SC_Inreply, SC_StartTime, SC_EndTime, AccID, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string SC_CustName, string SC_CustType, string SC_Kind, string SC_Plan, string SC_State, string SC_Inreply, string SC_StartTime, string SC_EndTime, string AccID, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (SC_CustName.Length > 0)
        {
            sbWhere.Append(" SC_CustName LIKE '%'+@SC_CustName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_CustName", SC_CustName));
        }
        if (SC_CustType.Length > 0)
        {
            sbWhere.Append(" SC_CustType=@SC_CustType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_CustType", SC_CustType));
        }
        if (SC_Kind.Length > 0)
        {
            sbWhere.Append(" SC_Kind=@SC_Kind AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_Kind", SC_Kind));
        }
        if (SC_Plan.Length > 0)
        {
            sbWhere.Append(" SC_Plan=@SC_Plan AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_Plan", SC_Plan));
        }
        if (SC_State.Length > 0)
        {
            sbWhere.Append(" SC_State=@SC_State AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_State", SC_State));
        }
        if (SC_Inreply.Length > 0)
        {
            sbWhere.Append(" SC_Inreply=@SC_Inreply AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_Inreply", SC_Inreply));
        }
        if (SC_StartTime.Length > 0)
        {
            sbWhere.Append("(DATEDIFF(d, @SC_StartTime, SC_StartTime)>=0 OR DATEDIFF(d, @SC_StartTime, SC_EndTime)>=0) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_StartTime", SC_StartTime));
        }
        if (SC_EndTime.Length > 0)
        {
            sbWhere.Append("(DATEDIFF(d, @SC_EndTime, SC_StartTime)<=0 OR DATEDIFF(d, @SC_EndTime, SC_EndTime)<=0) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@SC_EndTime", SC_EndTime));
        }
        if (AccID.Length > 0)
        {
            sbWhere.Append(" AccID=@AccID AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("SC_StartTime_str", typeof(string));
        dt.Columns.Add("SC_EndTime_str", typeof(string));
        dt.Columns.Add("SC_Kind_str", typeof(string));
        dt.Columns.Add("SC_Plan_str", typeof(string));
        dt.Columns.Add("SC_State_str", typeof(string));
        dt.Columns.Add("btn_Email", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["SC_StartTime"].ToString() != "")
                dr["SC_StartTime_str"] = DateTime.Parse(dr["SC_StartTime"].ToString()).ToString("yyyy/MM/dd HH:mm");
            if (dr["SC_EndTime"].ToString() != "")
                dr["SC_EndTime_str"] = DateTime.Parse(dr["SC_EndTime"].ToString()).ToString("yyyy/MM/dd HH:mm");
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "編輯", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_StartTime_str", "行程日期(起)", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_EndTime_str", "行程日期(迄)", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_CustName", "客戶姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_Kind", "種類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_Plan", "項目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_Remark", "商機", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("SC_State", "進度", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_Email", "E-Mail", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider AppendTransaction(string emNo, string progId, DataSet ds)
    {
        RServiceProvider rsp = SqlAccess.SqlCommandAppendTransaction("0", ds);
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert(rsp.ReturnSqlTransaction, emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return rsp;
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblScheduleM where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblScheduleM";
        return emds.Tables[0].Clone();
    }
    #endregion



}