﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEPayment
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EP_TransNo(string EP_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EP_TransNo", SqlAccess.CreateSqlParameter("@EP_TransNo", EP_TransNo));

        string cmdtxt = "SELECT * FROM tblEPayment WHERE EP_TransNo=@EP_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_ED_TransNo(string ED_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ED_TransNo", SqlAccess.CreateSqlParameter("@ED_TransNo", ED_TransNo));

        string cmdtxt = "SELECT * FROM tblEPayment WHERE ED_TransNo=@ED_TransNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EG_TransNo(string EG_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));

        string cmdtxt = "SELECT * FROM tblEPayment WHERE EG_TransNo=@EG_TransNo and ModiState<>'D'";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataRow DataReader_EP_AccID_EG_TransNo(string EP_AccID, string EG_TransNo)
    {
        DataRow dr = null;
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EP_AccID", SqlAccess.CreateSqlParameter("@EP_AccID", EP_AccID));
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));

        string cmdtxt = "SELECT * FROM tblEPayment WHERE EP_AccID=@EP_AccID and EG_TransNo=@EG_TransNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EC_CName, EC_Code, PS_NAME, EP_Bank, EC_SDate_s, EC_SDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEPayment", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_exam(string EG_Name, string PS_NAME, string EP_Bank, string EG_ExamDate_s, string EG_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_exam(EG_Name, PS_NAME, EP_Bank, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEPayment", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_upload(string EG_Name, string PS_NAME, string EG_ExamDate_s, string EG_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_upload(EG_Name, PS_NAME, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEPayment", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEPayment");
        string where = whereString(EC_CName, EC_Code, PS_NAME, EP_Bank, EC_SDate_s, EC_SDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_exam(string EG_Name, string PS_NAME, string EP_Bank, string EG_ExamDate_s, string EG_ExamDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEPayment");
        string where = whereString_exam(EG_Name, PS_NAME, EP_Bank, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_upload(string EG_Name, string PS_NAME, string EG_ExamDate_s, string EG_ExamDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEPayment");
        string where = whereString_upload(EG_Name, PS_NAME, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" (select EC_CName from tblECourse where tblECourse.EC_TransNo=tblEPayment.EC_TransNo) LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Code.Length > 0)
        {
            sbWhere.Append(" (select EC_Code from tblECourse where tblECourse.EC_TransNo=tblEPayment.EC_TransNo) LIKE '%'+@EC_Code+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblEPayment.EP_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EP_Bank.Length > 0)
        {
            sbWhere.Append(" EP_Bank LIKE '%'+@EP_Bank+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EP_Bank", EP_Bank));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEPayment.EC_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEPayment.EC_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }

        sbWhere.Append(" ED_TransNo is not null AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_exam(string EG_Name, string PS_NAME, string EP_Bank, string EG_ExamDate_s, string EG_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EG_Name.Length > 0)
        {
            sbWhere.Append(" (select EG_Name from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo) LIKE '%'+@EG_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Name", EG_Name));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblEPayment.EP_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EP_Bank.Length > 0)
        {
            sbWhere.Append(" EP_Bank LIKE '%'+@EP_Bank+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EP_Bank", EP_Bank));
        }
        if (EG_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_s, (select EG_ExamDate from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_s", EG_ExamDate_s));
        }
        if (EG_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_e, (select EG_ExamDate from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_e", EG_ExamDate_e));
        }

        sbWhere.Append(" EG_TransNo is not null AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_upload(string EG_Name, string PS_NAME, string EG_ExamDate_s, string EG_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EG_Name.Length > 0)
        {
            sbWhere.Append(" (select EG_Name from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo) LIKE '%'+@EG_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Name", EG_Name));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblEPayment.EP_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EG_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_s, (select EG_ExamDate from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_s", EG_ExamDate_s));
        }
        if (EG_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_e, (select EG_ExamDate from tblERegion where tblERegion.EG_TransNo=tblEPayment.EG_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_e", EG_ExamDate_e));
        }

        sbWhere.Append(" EP_Type in ('E011','E012','E013','E014') AND ");
        sbWhere.Append(" EG_TransNo is not null AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EC_SDate_Str", typeof(string));
        dt.Columns.Add("EC_CName", typeof(string));
        dt.Columns.Add("EG_ExamDate_Str", typeof(string));
        dt.Columns.Add("EG_Name", typeof(string));
        dt.Columns.Add("EP_Type_str", typeof(string));
        dt.Columns.Add("EP_EduLevel_str", typeof(string));
        dt.Columns.Add("EX_Score", typeof(string));
        dt.Columns.Add("EP_SignDate_str", typeof(string));
        dt.Columns.Add("notifyBtn", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_ecourse = DataECourse.DataReader_EC_TransNo(dr["EC_TransNo"].ToString());
            if (dr_ecourse != null)
            {
                if (dr_ecourse["EC_Category"].ToString() == "2")
                {
                    dr["EC_CName"] = dr_ecourse["EC_CName"].ToString();
                    dr["EC_SDate_Str"] = string.Format("{0}<br>{1}~{2}", DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_ecourse["EC_STime"].ToString(), dr_ecourse["EC_ETime"].ToString());
                }
            }
            DataRow dr_region = DataERegion.DataReader_EG_TransNo(dr["EG_TransNo"].ToString());
            if (dr_region != null)
            {
                dr["EG_Name"] = dr_region["EG_Name"].ToString();
                dr["EG_ExamDate_Str"] = DateTime.Parse(dr_region["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            }
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EP_Type"].ToString());
            if (dr_Phrase != null)
            {
                dr["EP_Type_str"] = dr_Phrase["TypeName"].ToString();
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("E006", dr["EP_EduLevel"].ToString());
            if (dr_Phrase != null)
            {
                dr["EP_EduLevel_str"] = dr_Phrase["TypeName"].ToString();
            }
            if (dr["EP_SignDate"].ToString() != "")
                dr["EP_SignDate_str"] = DateTime.Parse(dr["EP_SignDate"].ToString()).ToString("yyyy/MM/dd");
            DataTable dr_exam = DataExam.DataReader_EB_TransNo_AccID(dr["EB_TransNo"].ToString(), dr["EP_AccID"].ToString()).Tables[0];
            if (dr_exam != null && dr_exam.Rows.Count != 0)
            {
                dr["EX_Score"] = dr_exam.AsEnumerable().Max(row => row["EX_Score"]).ToString();
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Bank", "銀行", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_TransAmt", "金額", 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_exam(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_ExamDate_Str", "考試日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Name", "考試名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Bank", "銀行", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_TransAmt", "金額", 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("notifyBtn", "", 50));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_upload(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Type_str", "考試類別", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_ExamDate_Str", "考試日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Name", "考試名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_SignDate_str", "報名日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_Score", "分數", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EP_EduLevel_str", "學歷", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEPayment where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEPayment";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  驗證報名編號
    public static Boolean valid_ED_TransNo(string ED_TransNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEPayment WHERE ED_TransNo=@ED_TransNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@ED_TransNo", ED_TransNo));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    public static DataSet DataReader_examPay_statistics(string year, string month)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("Year", SqlAccess.CreateSqlParameter("@Year", year));
        dParas.Add("Month", SqlAccess.CreateSqlParameter("@Month", month));

        StringBuilder cmdtxt = new StringBuilder();
        cmdtxt.Append("DECLARE @TmpTable TABLE(考試項目 varchar(100),考試日期 varchar(10),報考人數 int,現金繳交 int,銀行轉帳 int,合計金額 int,備註 varchar(500)); ");
        cmdtxt.Append("insert into @TmpTable ");
        cmdtxt.Append("SELECT ");
        cmdtxt.Append("(select TypeName  from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=EG_Type) AS '考試項目', ");
        cmdtxt.Append("CONVERT(varchar(10), EG_ExamDate, 111) AS '考試日期', ");
        cmdtxt.Append("count(EP_AccID) AS '報考人數', ");
        cmdtxt.Append("SUM(CASE WHEN (tblEPayment.EP_Account is null or tblEPayment.EP_Account='') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '現金繳交', ");
        cmdtxt.Append("SUM(CASE WHEN (tblEPayment.EP_Account is not null and tblEPayment.EP_Account<>'') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '銀行轉帳', ");
        cmdtxt.Append("sum(isnull(EP_TransAmt, 0)) AS '合計金額', ");
        cmdtxt.Append("tblEPayment.EG_TransNo AS '備註' ");
        cmdtxt.Append("FROM tblEPayment ");
        cmdtxt.Append("left join tblERegion on tblERegion.EG_TransNo=tblEPayment.EG_TransNo and tblEPayment.EG_TransNo is not null and tblEPayment.ModiState<>'D' ");
        cmdtxt.Append("group by tblEPayment.EG_TransNo,EG_Type,EG_ExamDate ");
        cmdtxt.Append("having EG_Type is not null and YEAR(EG_ExamDate)=@Year and MONTH(EG_ExamDate)=@Month; ");

        cmdtxt.Append("select * from @TmpTable ");
        cmdtxt.Append("UNION ALL ");
        cmdtxt.Append("SELECT '', '合計', sum(報考人數), sum(現金繳交), sum(銀行轉帳), sum(合計金額), '' from @TmpTable ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        return emds;

//DECLARE @TmpTable TABLE(考試項目 varchar(100),考試日期 varchar(10),報考人數 int,現金繳交 int,銀行轉帳 int,合計金額 int,備註 varchar(500));
//insert into @TmpTable
//SELECT 
//(select TypeName  from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=EG_Type) AS '考試項目'
//,CONVERT(varchar(10), EG_ExamDate, 111) AS '考試日期'
//,count(EP_AccID) AS '報考人數'
//,SUM(CASE WHEN (tblEPayment.EP_Account is null or tblEPayment.EP_Account='') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '現金繳交'
//,SUM(CASE WHEN (tblEPayment.EP_Account is not null and tblEPayment.EP_Account<>'') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '銀行轉帳'
//,sum(EP_TransAmt) AS '合計金額'
//,tblEPayment.EG_TransNo AS '備註'
//  FROM tblEPayment
//  left join tblERegion on tblERegion.EG_TransNo=tblEPayment.EG_TransNo and tblEPayment.EG_TransNo is not null
//  group by tblEPayment.EG_TransNo,EG_Type,EG_ExamDate
//having EG_Type is not null and YEAR(EG_ExamDate)='2017' and MONTH(EG_ExamDate)='8';
//select * from @TmpTable
//UNION ALL
//SELECT '', '合計', sum(報考人數), sum(現金繳交), sum(銀行轉帳), sum(合計金額), ''
//from @TmpTable 
    }

    public static DataSet DataReader_examPay_statistics_details(string year, string month)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("Year", SqlAccess.CreateSqlParameter("@Year", year));
        dParas.Add("Month", SqlAccess.CreateSqlParameter("@Month", month));

        StringBuilder cmdtxt = new StringBuilder();
        cmdtxt.Append("DECLARE @TmpTable TABLE(費用類別 varchar(100),考試項目 varchar(100),考試日期 varchar(10),業務員編號 varchar(10),ID varchar(10),姓名 varchar(10),現金繳交 int,銀行轉帳 int); ");
        cmdtxt.Append("insert into @TmpTable ");
        cmdtxt.Append("SELECT ");
        cmdtxt.Append("(select TypeName  from tblPhrase where tblPhrase.TypeCode='139' and tblPhrase.TypeNo=tblEPayment.EP_FeeType) AS '費用類別' ");
        cmdtxt.Append(",(select TypeName  from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblERegion.EG_Type) AS '考試項目' ");
        cmdtxt.Append(",CONVERT(varchar(10), EG_ExamDate, 111) AS '考試日期' ");
        cmdtxt.Append(",EP_AccID AS '業務員編號' ");
        cmdtxt.Append(",(select PS_ID from tblPerson where tblPerson.AccID=EP_AccID) AS 'ID' ");
        cmdtxt.Append(",(select PS_NAME from tblPerson where tblPerson.AccID=EP_AccID) AS '姓名' ");
        cmdtxt.Append(",(CASE WHEN (tblEPayment.EP_Account is null or tblEPayment.EP_Account='') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '現金繳交' ");
        cmdtxt.Append(",(CASE WHEN (tblEPayment.EP_Account is not null and tblEPayment.EP_Account<>'') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '銀行轉帳' ");
        cmdtxt.Append("FROM tblEPayment ");
        cmdtxt.Append("left join tblERegion on tblERegion.EG_TransNo=tblEPayment.EG_TransNo and tblEPayment.EG_TransNo is not null and tblEPayment.ModiState<>'D' ");
        cmdtxt.Append("where EG_Type is not null and YEAR(EG_ExamDate)=@Year and MONTH(EG_ExamDate)=@Month; ");

        cmdtxt.Append("select * from @TmpTable ");
        cmdtxt.Append("UNION ALL ");
        cmdtxt.Append("SELECT '', '', '', '', '', '合計', sum(現金繳交), sum(銀行轉帳) from @TmpTable ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        return emds;

        //DECLARE @TmpTable TABLE(費用類別 varchar(100),考試項目 varchar(100),考試日期 varchar(10),業務員編號 varchar(10),ID varchar(10),姓名 varchar(10),現金繳交 int,銀行轉帳 int);
        //insert into @TmpTable
        //SELECT 
        //(select TypeName  from tblPhrase where tblPhrase.TypeCode='139' and tblPhrase.TypeNo=tblEPayment.EP_FeeType) AS '費用類別'
        //,(select TypeName  from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblERegion.EG_Type) AS '考試項目'
        //,CONVERT(varchar(10), EG_ExamDate, 111) AS '考試日期'
        //,EP_AccID AS '業務員編號'
        //,(select PS_ID from tblPerson where tblPerson.AccID=EP_AccID) AS 'ID'
        //,(select PS_NAME from tblPerson where tblPerson.AccID=EP_AccID) AS '姓名'
        //,(CASE WHEN (tblEPayment.EP_Account is null or tblEPayment.EP_Account='') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '現金繳交'
        //,(CASE WHEN (tblEPayment.EP_Account is not null and tblEPayment.EP_Account<>'') THEN isnull(EP_TransAmt, 0) ELSE 0 END) AS '銀行轉帳'
        //  FROM tblEPayment
        //  left join tblERegion on tblERegion.EG_TransNo=tblEPayment.EG_TransNo and tblEPayment.EG_TransNo is not null
        //where tblERegion.EG_Type is not null and YEAR(EG_ExamDate)='2017' and MONTH(EG_ExamDate)='8';
        //select * from @TmpTable
        //UNION ALL
        //SELECT '', '', '', '', '', '合計', sum(現金繳交), sum(銀行轉帳)
        //from @TmpTable 
    }

    public static DataSet DataReader_examPay_upload_details(string EP_Type, string year, string month)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EP_Type", SqlAccess.CreateSqlParameter("@EP_Type", EP_Type));
        dParas.Add("Year", SqlAccess.CreateSqlParameter("@Year", year));
        dParas.Add("Month", SqlAccess.CreateSqlParameter("@Month", month));

        StringBuilder cmdtxt = new StringBuilder();
        
        cmdtxt.Append("SELECT  ");
        cmdtxt.Append("ROW_NUMBER() OVER( ORDER BY tblERegion.EG_ExamDate desc) AS SerialNo, ");
        cmdtxt.Append("CONVERT(varchar(10), tblERegion.EG_ExamDate, 111) EG_ExamDate, ");
        cmdtxt.Append("tblEPayment.EP_TransNo, tblEPayment.EG_TransNo, tblEPayment.EP_Region,");
        cmdtxt.Append("tblExamSub.EB_Subject, ");
        cmdtxt.Append("tblEPayment.EP_Name, ");
        cmdtxt.Append("tabcSales.Gender, ");
        cmdtxt.Append("(case when SUBSTRING(tabcSales.cSAL_ID_S, 1, 1) Like '[A-Z]%' AND LEN(tabcSales.cSAL_ID_S)=10 then '1' else '2' end) type, ");
        cmdtxt.Append("tblEPayment.EP_ID, ");
        cmdtxt.Append("CONVERT(varchar(10), tabcSales.Birth, 111) Birth, ");
        cmdtxt.Append("tblEPayment.EP_EduLevel, ");
        cmdtxt.Append("tblEPayment.EB_TransNo, ");
        cmdtxt.Append("CONVERT(VARCHAR(4),CONVERT(VARCHAR(4),tblExam.EX_Date,20) - 1911) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),6,2) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),9,2) + '~' + CONVERT(VARCHAR(4),CONVERT(VARCHAR(4),tblExam.EX_Date,20) - 1911) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),6,2) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),9,2) EX_Date_range, ");
        cmdtxt.Append("CONVERT(varchar(10), tblExam.EX_Date, 111) EX_Date, ");
        cmdtxt.Append("tblExam.EX_Score, ");
        cmdtxt.Append("tblERegion.EG_RegistFee ");
        cmdtxt.Append("FROM tblEPayment ");
        cmdtxt.Append("left join  ");
        cmdtxt.Append("( ");
        cmdtxt.Append("SELECT   ");
        cmdtxt.Append("tblPerson.PS_Title, s2.Name DM_Name, ");
        cmdtxt.Append("tabcSales.Title, tblPhrase.TypeCode,  tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate, tabcSales.Gender, ");
        cmdtxt.Append("tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, tabcSales.Leave_Date,  ");
        cmdtxt.Append("tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education,  ");
        cmdtxt.Append("tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC, ");
        cmdtxt.Append("tabcSales_Regedit.REG_END_L, tabcSales_Regedit.REG_DATE_I, tabcSales_Regedit.REG_DATE_F, tabcSales_Regedit.REG_END_P, tabcSales_Regedit.REG_FINSH_L ");
        cmdtxt.Append("FROM tabcSales ");
        cmdtxt.Append("left Join tblPerson on tabcSales.cSAL_FK=tblPerson.AccID ");
        cmdtxt.Append("left Join tabcSales s2 on tabcSales.DM=s2.cSAL_FK ");
        cmdtxt.Append("inner Join tabcUnit on tabcSales.Zone= tabcUnit.cZON_PK ");
        cmdtxt.Append("inner Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0,6,7) ");
        cmdtxt.Append("inner Join tabcEducation ON tabcSales.Education=tabcEducation.iEDU_PK ");
        cmdtxt.Append("inner Join tabcSales_Regedit on tabcSales.cSAL_FK= tabcSales_Regedit.cSAL_FK ");
        cmdtxt.Append(") tabcSales on tabcSales.cSAL_FK=tblEPayment.EP_AccID ");
        cmdtxt.Append("Left Join tblExam on tblEPayment.EX_TransNo=tblExam.EX_TransNo and tblEPayment.EX_UidNo= tblExam.EX_UidNo ");
        cmdtxt.Append("Left Join tblExamSub on tblEPayment.EB_TransNo=tblExamSub.EB_TransNo ");
        cmdtxt.Append("Left Join tblERegion on tblEPayment.EG_TransNo=tblERegion.EG_TransNo ");
        cmdtxt.Append("Where tblEPayment.EP_Type in ('E011','E012','E013','E014') and tblEPayment.ModiState <>'D' ");
        cmdtxt.Append(" and EP_Type=@EP_Type and YEAR(tblERegion.EG_ExamDate)=@Year and MONTH(tblERegion.EG_ExamDate)=@Month ");
        cmdtxt.Append(" and tblExam.EX_Date is not null and tblExam.EX_Score>=70 ");
        cmdtxt.Append("order by tblERegion.EG_ExamDate desc ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        return emds;

//        SELECT 
//ROW_NUMBER() OVER( ORDER BY tblExam.EX_Date desc) AS SerialNo,
//tblEPayment.EP_Region,
//tblEPayment.EP_Name,
//tabcSales.Gender,
//(case when SUBSTRING(tabcSales.cSAL_ID_S, 1, 1) Like '[A-Z]%' AND LEN(tabcSales.cSAL_ID_S)=10 then '1' else '2' end) type,
//tblEPayment.EP_ID,
//CONVERT(varchar(10), tabcSales.Birth, 111) Birth,
//tblEPayment.EP_EduLevel,
//tblExamSub.EB_Subject,
//CONVERT(VARCHAR(4),CONVERT(VARCHAR(4),tblExam.EX_Date,20) - 1911) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),6,2) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),9,2) + '~' + CONVERT(VARCHAR(4),CONVERT(VARCHAR(4),tblExam.EX_Date,20) - 1911) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),6,2) + SUBSTRING(CONVERT(VARCHAR(10),tblExam.EX_Date,20),9,2) EX_Date_range,
//CONVERT(varchar(10), tblExam.EX_Date, 111) EX_Date,
//tblExam.EX_Score,
//tblERegion.EG_RegistFee
//  FROM tblEPayment
//  left join 
//  (
//  SELECT  
//tabcSales.Title, tblPhrase.TypeCode,  tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate, tabcSales.Gender,
//tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, tabcSales.Leave_Date, 
//tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education, 
//tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC
//FROM tabcSales
//left Join tblPerson on tabcSales.cSAL_FK=tblPerson.AccID
//inner Join tabcUnit on tabcSales.Zone= tabcUnit.cZON_PK
//inner Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0,6,7)
//inner Join tabcEducation ON tabcSales.Education=tabcEducation.iEDU_PK
//inner Join tabcSales_Regedit on tabcSales.cSAL_FK= tabcSales_Regedit.cSAL_FK
//) tabcSales on tabcSales.cSAL_FK=tblEPayment.EP_AccID
//Left Join tblExam on tblEPayment.EX_TransNo= tblExam.EX_TransNo and tblEPayment.EX_UidNo= tblExam.EX_UidNo
//Left Join tblExamSub on tblEPayment.EB_TransNo= tblExamSub.EB_TransNo and tblEPayment.EB_UidNo= tblExamSub.EB_UidNo
//Left Join tblERegion on tblEPayment.EG_TransNo= tblERegion.EG_TransNo and tblEPayment.EG_UidNo= tblERegion.EG_UidNo
//Where tblEPayment.EP_Type in ('E011','E012','E013','E014') and tblEPayment.ModiState <>'D'
// and EP_Type='E011' and YEAR(tblExam.EX_Date)=2017 and MONTH(tblExam.EX_Date)=6
// and tblExam.EX_Date is not null and tblExam.EX_Score>=70
//order by tblExam.EX_Date desc
    }


}