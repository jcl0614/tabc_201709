﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataETeacherTot
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EH_TransNo(string EH_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EH_TransNo", SqlAccess.CreateSqlParameter("@EH_TransNo", EH_TransNo));

        string cmdtxt = "SELECT * FROM tblETeacherTot WHERE EH_TransNo=@EH_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_ET_TransNo(string ET_TransNo, string EH_Year, string EH_Month)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ET_TransNo", SqlAccess.CreateSqlParameter("@ET_TransNo", ET_TransNo));
        dParas.Add("EH_Year", SqlAccess.CreateSqlParameter("@EH_Year", EH_Year));
        dParas.Add("EH_Month", SqlAccess.CreateSqlParameter("@EH_Month", EH_Month));

        string cmdtxt = "SELECT * FROM tblETeacherTot WHERE ET_TransNo=@ET_TransNo and EH_Year=@EH_Year and EH_Month=@EH_Month";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EH_Year, string EH_Month, string ET_Company, string ET_Department, string ET_TCode, string ET_TName, string EC_SDate_s, string EC_SDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EH_Year, EH_Month, ET_Company, ET_Department, ET_TCode, ET_TName, EC_SDate_s, EC_SDate_e, lSqlParas);
        string sort = "tblETeacherTot.Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("SerialNo, ");
        Vcolumns.Append("tblETeacherTot.*, ET_Company, ET_Department, ET_TCode, ET_TName, ET_Area, ET_Unit, ET_UnitName");
        //Vcolumns.Append("EC_CName, EC_SDate, EC_Mode, EC_ModeName, EC_Area, EC_Unit, EC_UnitName, EC_Place  ");

        StringBuilder columns = new StringBuilder();
        columns.Append("ROW_NUMBER() OVER( ORDER BY tblETeacherTot.Server_ModiDate desc) AS SerialNo, ");
        columns.Append("tblETeacherTot.*, tblETeacher.ET_Company, tblETeacher.ET_Department, tblETeacher.ET_TCode, tblETeacher.ET_TName, tblETeacher.ET_Area, tblETeacher.ET_Unit, (select TypeName  from tblPhrase where tblPhrase.TypeCode='E008' and tblPhrase.TypeSubCode=tblETeacher.ET_Unit) ET_UnitName ");
        //columns.Append("tblECourse.EC_CName, CONVERT(varchar(10), tblECourse.EC_SDate, 111) EC_SDate,  tblECourse.EC_Mode, (select TypeName  from tblPhrase where tblPhrase.TypeCode='130' and tblPhrase.TypeNo=tblECourse.EC_Mode) EC_ModeName, tblECourse.EC_Area, tblECourse.EC_Unit, (select TypeName  from tblPhrase where tblPhrase.TypeCode='E008' and tblPhrase.TypeSubCode=tblECourse.EC_Unit) EC_UnitName, tblECourse.EC_Place ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), 
            "tblETeacherTot left join tblETeacher on tblETeacherTot.ET_TransNo = tblETeacher.ET_TransNo and tblETeacherTot.ET_UidNo = tblETeacher.ET_UidNo",
            //" left join tblECourse on tblECourse.ET_TransNo = tblETeacherTot.ET_TransNo and tblECourse.ET_UidNo = tblETeacherTot.ET_UidNo", 
            "tblETeacherTot", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader2(string EH_Year, string EH_Month, string ET_Company, string ET_Department, string ET_TCode, string ET_TName, string EC_SDate_s, string EC_SDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EH_Year, EH_Month, ET_Company, ET_Department, ET_TCode, ET_TName, EC_SDate_s, EC_SDate_e, lSqlParas);
        string sort = "tblETeacherTot.Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("SerialNo, ");
        Vcolumns.Append("tblETeacherTot.*, ET_Company, ET_Department, ET_TCode, ET_TName, ET_Area, ET_Unit, ET_UnitName,");
        Vcolumns.Append("EC_CName, EC_SDate, EC_Mode, EC_ModeName, EC_Area, EC_Unit, EC_UnitName, EC_Place  ");

        StringBuilder columns = new StringBuilder();
        columns.Append("ROW_NUMBER() OVER( ORDER BY tblETeacherTot.Server_ModiDate desc) AS SerialNo, ");
        columns.Append("tblETeacherTot.*, tblETeacher.ET_Company, tblETeacher.ET_Department, tblETeacher.ET_TCode, tblETeacher.ET_TName, tblETeacher.ET_Area, tblETeacher.ET_Unit, (select TypeName  from tblPhrase where tblPhrase.TypeCode='E008' and tblPhrase.TypeSubCode=tblETeacher.ET_Unit) ET_UnitName, ");
        columns.Append("tblECourse.EC_CName, CONVERT(varchar(10), tblECourse.EC_SDate, 111) EC_SDate,  tblECourse.EC_Mode, (select TypeName  from tblPhrase where tblPhrase.TypeCode='130' and tblPhrase.TypeNo=tblECourse.EC_Mode) EC_ModeName, tblECourse.EC_Area, tblECourse.EC_Unit, (select TypeName  from tblPhrase where tblPhrase.TypeCode='E008' and tblPhrase.TypeSubCode=tblECourse.EC_Unit) EC_UnitName, tblECourse.EC_Place ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(),
            "tblETeacherTot left join tblETeacher on tblETeacherTot.ET_TransNo = tblETeacher.ET_TransNo and tblETeacherTot.ET_UidNo = tblETeacher.ET_UidNo" +
            " left join tblECourse on tblECourse.ET_TransNo = tblETeacherTot.ET_TransNo and tblECourse.ET_UidNo = tblETeacherTot.ET_UidNo",
            "tblETeacherTot", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EH_Year, string EH_Month, string ET_Company, string ET_Department, string ET_TCode, string ET_TName, string EC_SDate_s, string EC_SDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblETeacherTot"+
        " left join tblETeacher on tblETeacherTot.ET_TransNo = tblETeacher.ET_TransNo and tblETeacherTot.ET_UidNo = tblETeacher.ET_UidNo"
            );
        string where = whereString(EH_Year, EH_Month, ET_Company, ET_Department, ET_TCode, ET_TName, EC_SDate_s, EC_SDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EH_Year, string EH_Month, string ET_Company, string ET_Department, string ET_TCode, string ET_TName, string EC_SDate_s, string EC_SDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EH_Year.Length > 0)
        {
            sbWhere.Append(" EH_Year=@EH_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EH_Year", EH_Year));
        }
        if (EH_Month.Length > 0)
        {
            sbWhere.Append(" EH_Month=@EH_Month AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EH_Month", EH_Month));
        }
        if (ET_Company.Length > 0)
        {
            sbWhere.Append(" ET_Company LIKE '%'+@ET_Company+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Company", ET_Company));
        }
        if (ET_Department != "")
        {
            sbWhere.Append(" ET_Department LIKE '%'+@ET_Department+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Department", ET_Department));
        }
        if (ET_TCode != "")
        {
            sbWhere.Append(" ET_TCode LIKE '%'+@ET_TCode+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TCode", ET_TCode));
        }
        if (ET_TName != "")
        {
            sbWhere.Append(" ET_TName LIKE '%'+@ET_TName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TName", ET_TName));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("(select count(*) from tblECourse where tblECourse.ET_TransNo=tblETeacherTot.ET_TransNo and DATEDIFF(d, @EC_SDate_s, tblECourse.EC_SDate)>=0)>0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("(select count(*) from tblECourse where tblECourse.ET_TransNo=tblETeacherTot.ET_TransNo and DATEDIFF(d, @EC_SDate_e, tblECourse.EC_SDate)<=0)>0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }

        //sbWhere.Append(" (select count(*) from tblECourse where tblECourse.ET_TransNo=tblETeacherTot.ET_TransNo and tblECourse.EC_Category=@EC_Category)>0 AND ");
        //lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", "2"));

        sbWhere.Append(" tblETeacherTot.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("YearMonth", typeof(string));
        dt.Columns.Add("EH_ShouldPay_str", typeof(string));
        dt.Columns.Add("EH_RealPay_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["YearMonth"] = string.Format("{0}/{1}", dr["EH_Year"].ToString(), dr["EH_Month"].ToString());
            dr["EH_ShouldPay_str"] = string.Format("N:{0}", dr["EH_ShouldPay"].ToString());
            dr["EH_RealPay_str"] = string.Format("N:{0}", dr["EH_RealPay"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Company", "公司名稱", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Department", "部門名稱", 100, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_TCode", "講師編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_TName", "講師姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("YearMonth", "統計年月", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EH_Points", "點數", 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("EH_ShouldPay", "應付費", 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("EH_RealPay", "實付費", false, 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblETeacherTot where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblETeacherTot";
        return emds.Tables[0].Clone();
    }
    #endregion





}