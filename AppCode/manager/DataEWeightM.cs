﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEWeightM
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EW_WeightM(string EW_CUnit, string EW_TUnit)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EW_CUnit", SqlAccess.CreateSqlParameter("@EW_CUnit", EW_CUnit));
        dParas.Add("EW_TUnit", SqlAccess.CreateSqlParameter("@EW_TUnit", EW_TUnit));

        string cmdtxt = "SELECT * FROM tblEWeightM WHERE EW_CUnit=@EW_CUnit and EW_TUnit=@EW_TUnit";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EB_SubType, string EB_Subject, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EB_SubType, EB_Subject, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEWeightM", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EB_SubType, string EB_Subject)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEWeightM");
        string where = whereString(EB_SubType, EB_Subject, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EB_SubType, string EB_Subject, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EB_SubType.Length > 0)
        {
            sbWhere.Append(" (select EB_SubType from tblExamSub where tblExamSub.EB_TransNo=tblEWeightM.EB_TransNo)=@EB_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        }
        if (EB_Subject.Length > 0)
        {
            sbWhere.Append(" (select EB_Subject from tblExamSub where tblExamSub.EB_TransNo=tblEWeightM.EB_TransNo)=@EB_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EB_Explan", typeof(string));
        dt.Columns.Add("EB_SubType_str", typeof(string));
        dt.Columns.Add("EB_Subject_str", typeof(string));
        dt.Columns.Add("EE_TrueFalse_str", typeof(string));
        dt.Columns.Add("EE_OneChoice_str", typeof(string));
        dt.Columns.Add("EE_MultiChoice_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_ExamSub = DataExamSub.DataReader_EB_TransNo(dr["EB_TransNo"].ToString());
            if (dr_ExamSub != null)
            {
                dr["EB_Explan"] = dr_ExamSub["EB_Explan"].ToString();
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("144", dr_ExamSub["EB_SubType"].ToString());
                if (dr_Phrase != null)
                    dr["EB_SubType_str"] = dr_Phrase["TypeName"].ToString();
                string EB_SubjectTypeCode = "";
                switch (dr_ExamSub["EB_SubType"].ToString())
                {
                    case "1":
                        EB_SubjectTypeCode = "138";
                        break;
                    case "2":
                        EB_SubjectTypeCode = "158";
                        break;
                    case "3":
                        EB_SubjectTypeCode = "159";
                        break;
                    case "4":
                        EB_SubjectTypeCode = "160";
                        break;
                }
                dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EB_SubjectTypeCode, dr_ExamSub["EB_Subject"].ToString());
                if (dr_Phrase != null)
                    dr["EB_Subject_str"] = dr_Phrase["TypeName"].ToString();
            }
            dr["EE_TrueFalse_str"] = string.Format("{0}／{1}／{2}", dr["EE_TrueFalse"].ToString(), dr["EE_TrueFalseS"].ToString(), dr["EE_TrueFalseO"].ToString());
            dr["EE_OneChoice_str"] = string.Format("{0}／{1}／{2}", dr["EE_OneChoice"].ToString(), dr["EE_OneChoiceS"].ToString(), dr["EE_OneChoiceO"].ToString());
            dr["EE_MultiChoice_str"] = string.Format("{0}／{1}／{2}", dr["EE_MultiChoice"].ToString(), dr["EE_MultiChoiceS"].ToString(), dr["EE_MultiChoiceO"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Explan", "報考科目", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_SubType_str", "考試類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Subject_str", "科目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EE_TrueFalse_str", "是非題/分數/順序", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EE_OneChoice_str", "是非題/分數/順序", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EE_MultiChoice_str", "是非題/分數/順序", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EE_TimeLength", "測驗時間", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEWeightM where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEWeightM";
        return emds.Tables[0].Clone();
    }
    #endregion




}