﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataPerson
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblPerson WHERE (PS_NAME LIKE '%'+@searchString+'%' OR AccID LIKE '%'+@searchString+'%') AND tblPerson.ModiState<>'D'";
        else
            cmdtxt = "SELECT * FROM tblPerson";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_AccID(string AccID)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        string cmdtxt = "SELECT * FROM tblPerson WHERE AccID=@AccID";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EC_CName, string EC_Type, string EC_Rating, string EC_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EC_CName, EC_Type, EC_Rating, EC_Category, lSqlParas);
        string sort = "tblPerson.ModiDate";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("tblPerson.*, tblETeacher_ET_TransNo, tblETeacher_ET_UidNo, tblETeacher_E5_TransNo, tblETeacher_E5_UidNo, tblETeacher_ET_TName");

        StringBuilder columns = new StringBuilder();
        columns.Append("tblPerson.*, tblETeacher.ET_TransNo as tblETeacher_ET_TransNo,tblETeacher.ET_UidNo as tblETeacher_ET_UidNo,tblETeacher.E5_TransNo as tblETeacher_E5_TransNo,tblETeacher.E5_UidNo as tblETeacher_E5_UidNo,tblETeacher.ET_TName as tblETeacher_ET_TName ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblPerson left join tblETeacher on tblPerson.ET_TransNo=tblETeacher.ET_TransNo and tblPerson.ET_UidNo=tblETeacher.ET_UidNo", "tblPerson", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EC_CName, string EC_Type, string EC_Rating, string EC_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblPerson");
        string where = whereString(EC_CName, EC_Type, EC_Rating, EC_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EC_CName, string EC_Type, string EC_Rating, string EC_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" EC_CName LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Type != "")
        {
            sbWhere.Append(" EC_Type=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" EC_Rating=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_Category != "")
        {
            sbWhere.Append(" EC_Category=@EC_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        }

        sbWhere.Append(" tblPerson.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];

        foreach (DataRow dr in dt.Rows)
        {

        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv, string category)
    {
        if (category == "1")
        {
            mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Code", "課程代碼", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 200, "Left"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Summary", "課程簡介", 300, "Left"));
        }
        else if (category == "2")
        {

        }

        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
        BoundField bf_ = DataModel.CreateBoundField("tblETeacher_ET_TName", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_ET_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
         bf_ = DataModel.CreateBoundField("tblETeacher_ET_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblPerson where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblPerson";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_AccID(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} ({1})", dr["PS_NAME"].ToString(), dr["AccID"].ToString()), dr["AccID"].ToString()));
        }
    }
    #endregion

    #region  驗證編號
    public static Boolean valid_AccID(string AccID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblPerson WHERE AccID=@AccID";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@AccID", AccID));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

}