﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEFile
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EF_TransNo(string EF_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EF_TransNo", SqlAccess.CreateSqlParameter("@EF_TransNo", EF_TransNo));

        string cmdtxt = "SELECT * FROM tblEFile WHERE EF_TransNo=@EF_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EF_Type_EF_SubType(string EF_Type, string EF_SubType)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EF_Type", SqlAccess.CreateSqlParameter("@EF_Type", EF_Type));
        dParas.Add("EF_SubType", SqlAccess.CreateSqlParameter("@EF_SubType", EF_SubType));

        string cmdtxt = "SELECT * FROM tblEFile WHERE EF_Type=@EF_Type AND EF_SubType=@EF_SubType AND ModiState<>'D' order by tblEFile.EF_OrderNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string EF_Type, string EF_SubType, string EF_Name, string EF_Contents, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EF_Type, EF_SubType, EF_Name, EF_Contents, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEFile", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EF_Type, string EF_SubType, string EF_Name, string EF_Contents)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEFile");
        string where = whereString(EF_Type, EF_SubType, EF_Name, EF_Contents, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EF_Type, string EF_SubType, string EF_Name, string EF_Contents, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EF_Type.Length > 0)
        {
            sbWhere.Append(" EF_Type=@EF_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EF_Type", EF_Type));
        }
        if (EF_SubType.Length > 0)
        {
            sbWhere.Append(" EF_SubType=@EF_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EF_SubType", EF_SubType));
        }
        if (EF_Name.Length > 0)
        {
            sbWhere.Append(" EF_Name LIKE '%'+@EF_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EF_Name", EF_Name));
        }
        if (EF_Contents.Length > 0)
        {
            sbWhere.Append(" EF_Contents LIKE '%'+@EF_Contents+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EF_Contents", EF_Contents));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EF_SubType_str", typeof(string));
        dt.Columns.Add("EF_File_link", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("144", dr["EF_SubType"].ToString());
            if (dr_Phrase != null)
                dr["EF_SubType_str"] = dr_Phrase["TypeName"].ToString();
            dr["EF_File_link"] = string.Format("<a title=\"{0}\" href=\"/uploads/EFile/{0}\" target=\"_blank\"><img src=\"/Manager/images/download-icon.png\" border=\"0\" align=\"bottom\" /></a>", dr["EF_Name"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EF_SubType_str", "考試類別", 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EF_Name", "檔案名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EF_Contents", "說明", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EF_File_link", "下載", false, 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEFile where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEFile";
        return emds.Tables[0].Clone();
    }
    #endregion





}