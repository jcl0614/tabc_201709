﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEAttendTot
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EO_TransNo(string EO_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EO_TransNo", SqlAccess.CreateSqlParameter("@EO_TransNo", EO_TransNo));

        string cmdtxt = "SELECT * FROM tblEAttendTot WHERE EO_TransNo=@EO_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EO_DM(string EO_DM, string EO_Year, string EO_Month)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EO_DM", SqlAccess.CreateSqlParameter("@EO_DM", EO_DM));
        dParas.Add("EO_Year", SqlAccess.CreateSqlParameter("@EO_Year", EO_Year));
        dParas.Add("EO_Month", SqlAccess.CreateSqlParameter("@EO_Month", EO_Month));

        string cmdtxt = "SELECT * FROM tblEAttendTot WHERE EO_DM=@EO_DM and EO_Year=@EO_Year and EO_Month=@EO_Month";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EO_Year, EO_Month, EO_Unit, EO_AccID, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttendTot", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_my(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID, string DM, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_my(EO_Year, EO_Month, EO_Unit, EO_AccID, DM, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttendTot", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_my(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttendTot");
        string where = whereString(EO_Year, EO_Month, EO_Unit, EO_AccID, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_my(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID, string DM)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttendTot");
        string where = whereString_my(EO_Year, EO_Month, EO_Unit, EO_AccID, DM, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EO_Year.Length > 0)
        {
            sbWhere.Append(" EO_Year=@EO_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Year", EO_Year));
        }
        if (EO_Month.Length > 0)
        {
            sbWhere.Append(" EO_Month=@EO_Month AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Month", EO_Month));
        }
        if (EO_Unit.Length > 0)
        {
            sbWhere.Append(" EO_Unit=@EO_Unit AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Unit", EO_Unit));
        }
        if (EO_AccID.Length > 0)
        {
            sbWhere.Append(" (EO_AccID LIKE '%'+@EO_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblEAttendTot.EO_AccID) LIKE '%'+@EO_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_AccID", EO_AccID));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_my(string EO_Year, string EO_Month, string EO_Unit, string EO_AccID, string DM, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EO_Year.Length > 0)
        {
            sbWhere.Append(" EO_Year=@EO_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Year", EO_Year));
        }
        if (EO_Month.Length > 0)
        {
            sbWhere.Append(" EO_Month=@EO_Month AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Month", EO_Month));
        }
        if (EO_Unit.Length > 0)
        {
            sbWhere.Append(" EO_Unit=@EO_Unit AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_Unit", EO_Unit));
        }
        if (EO_AccID.Length > 0)
        {
            sbWhere.Append(" (EO_AccID LIKE '%'+@EO_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblEAttendTot.EO_AccID) LIKE '%'+@EO_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EO_AccID", EO_AccID));
        }
        if (DM.Length > 0)
        {
            sbWhere.Append(" ((select DM from tabcSales where tabcSales.cSAL_FK=tblEAttendTot.EO_AccID)=@DM) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@DM", DM));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("YearMonth", typeof(string));
        dt.Columns.Add("EO_Unit_str", typeof(string));
        dt.Columns.Add("EO_AccID_Name", typeof(string));
        dt.Columns.Add("DM_NAME", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["YearMonth"] = string.Format("{0}/{1}", dr["EO_Year"].ToString(), int.Parse(dr["EO_Month"].ToString()).ToString("00"));
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr["EO_Unit"].ToString());
            if (dr_Phrase != null)
                dr["EO_Unit_str"] = dr_Phrase["TypeName"].ToString();
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EO_AccID"].ToString());
            if (dr_sales != null)
            {
                dr["EO_AccID_Name"] = dr_sales["NAME"].ToString();
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    dr["DM_NAME"] = dr_sales2["NAME"].ToString();
                }
            }
        }
    }
    private static void DataProcess_my(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("YearMonth", typeof(string));
        dt.Columns.Add("EO_Unit_str", typeof(string));
        dt.Columns.Add("EO_AccID_Name", typeof(string));
        dt.Columns.Add("DM", typeof(string));
        dt.Columns.Add("DM_NAME", typeof(string));
        dt.Columns.Add("btn_member", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["YearMonth"] = string.Format("{0}/{1}", dr["EO_Year"].ToString(), int.Parse(dr["EO_Month"].ToString()).ToString("00"));
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr["EO_Unit"].ToString());
            if (dr_Phrase != null)
                dr["EO_Unit_str"] = dr_Phrase["TypeName"].ToString();
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EO_AccID"].ToString());
            if (dr_sales != null)
            {
                dr["EO_AccID_Name"] = dr_sales["NAME"].ToString();
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    dr["DM"] = dr_sales["DM"].ToString();
                    dr["DM_NAME"] = dr_sales2["NAME"].ToString();
                }
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("YearMonth", "年/月", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_Unit_str", "單位", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID", "人員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_ShouldAtt", "應出席次數", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_AttRate(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("YearMonth", "年/月", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_Unit_str", "部門", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("DM_NAME", "主管", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_ShouldAtt", "應出席", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_RealAtt", "實出席", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AttRate", "出席率(%)", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AttBonus", "加值(%)", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }

    }
    public static void DataColumn_my(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Link, "年度", "EO_Year", "", "Select", "", 50, false));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_Month", "月份", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_Unit_str", "單位", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_AccID_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("DM", "處經理編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("DM_NAME", "處經理姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EO_ShouldAtt", "應出席", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_member", "出席人員", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }

    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEAttendTot where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEAttendTot";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region
    public static bool valid_EO(string EO_AccID, string EO_Year, string EO_Month)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EO_AccID", SqlAccess.CreateSqlParameter("@EO_AccID", EO_AccID));
        dParas.Add("EO_Year", SqlAccess.CreateSqlParameter("@EO_Year", EO_Year));
        dParas.Add("EO_Month", SqlAccess.CreateSqlParameter("@EO_Month", EO_Month));

        string cmdtxt = "SELECT * FROM tblEAttendTot WHERE EO_AccID=@EO_AccID AND EO_Year=@EO_Year AND EO_Month=@EO_Month AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion


}