﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataECircular
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EI_TransNo(string EI_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EI_TransNo", SqlAccess.CreateSqlParameter("@EI_TransNo", EI_TransNo));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EI_TransNo=@EI_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EI_AccID(string EI_AccID)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EI_AccID", SqlAccess.CreateSqlParameter("@EI_AccID", EI_AccID));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EI_AccID=@EI_AccID and ModiState<>'D'";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataRow DataReader_EG_TransNo_EP_TransNo(string EG_TransNo, string EP_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));
        dParas.Add("EP_TransNo", SqlAccess.CreateSqlParameter("@EP_TransNo", EP_TransNo));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EG_TransNo=@EG_TransNo and EP_TransNo=@EP_TransNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_EG_TransNo_EI_AccID(string EG_TransNo, string EI_AccID)
    {
        DataRow dr = null;
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));
        dParas.Add("EI_AccID", SqlAccess.CreateSqlParameter("@EI_AccID", EI_AccID));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EG_TransNo=@EG_TransNo AND EI_AccID=@EI_AccID AND ModiState<>'D'";
        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EI_Type, string EI_Mode, string EI_Title, string EI_Name, string EI_AppDate_s, string EI_AppDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EI_Type, EI_Mode, EI_Title, EI_Name, EI_AppDate_s, EI_AppDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECircular", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_pass(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_pass(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECircular", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_pass(emds);
        return emds;
    }
    public static DataSet DataReader_ReviewScore(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_ReviewScore(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECircular", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_pass(emds);
        return emds;
    }
    public static DataSet DataReader_PassSend(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_PassSend(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECircular", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_pass(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EI_Type, string EI_Mode, string EI_Title, string EI_Name, string EI_AppDate_s, string EI_AppDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECircular");
        string where = whereString(EI_Type, EI_Mode, EI_Title, EI_Name, EI_AppDate_s, EI_AppDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_pass(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECircular");
        string where = whereString_pass(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_ReviewScore(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECircular");
        string where = whereString_ReviewScore(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_PassSend(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECircular");
        string where = whereString_PassSend(EI_ExamType, PS_NAME, EI_ExamDate_s, EI_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EI_Type, string EI_Mode, string EI_Title, string EI_Name, string EI_AppDate_s, string EI_AppDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EI_Type.Length > 0)
        {
            sbWhere.Append(" EI_Type=@EI_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_Type", EI_Type));
        }
        if (EI_Mode.Length > 0)
        {
            sbWhere.Append(" EI_Mode=@EI_Mode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_Mode", EI_Mode));
        }
        if (EI_Title.Length > 0)
        {
            sbWhere.Append(" (SELECT TypeName FROM tblPhrase WHERE TypeCode='E010' and TypeNo=EI_Title) LIKE '%'+@EI_Title+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_Title", EI_Title));
        }
        if (EI_Name.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblECircular.EI_AccID) LIKE '%'+@EI_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_Name", EI_Name));
        }
        if (EI_AppDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_AppDate_s, EI_AppDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_AppDate_s", EI_AppDate_s));
        }
        if (EI_AppDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_AppDate_e, EI_AppDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_AppDate_e", EI_AppDate_e));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_pass(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EI_ExamType.Length > 0)
        {
            sbWhere.Append(" EI_ExamType=@EI_ExamType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamType", EI_ExamType));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblECircular.EI_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EI_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_s, EI_ExamDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_s", EI_ExamDate_s));
        }
        if (EI_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_e, EI_ExamDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_e", EI_ExamDate_e));
        }

        sbWhere.Append(" EI_Type=2 AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_ReviewScore(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EI_ExamType.Length > 0)
        {
            sbWhere.Append(" EI_ExamType=@EI_ExamType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamType", EI_ExamType));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblECircular.EI_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EI_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_s, EI_ExamDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_s", EI_ExamDate_s));
        }
        if (EI_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_e, EI_ExamDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_e", EI_ExamDate_e));
        }

        sbWhere.Append(" EI_Type=2 AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_PassSend(string EI_ExamType, string PS_NAME, string EI_ExamDate_s, string EI_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EI_ExamType.Length > 0)
        {
            sbWhere.Append(" EI_ExamType=@EI_ExamType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamType", EI_ExamType));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblECircular.EI_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EI_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_s, EI_ExamDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_s", EI_ExamDate_s));
        }
        if (EI_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EI_ExamDate_e, EI_ExamDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EI_ExamDate_e", EI_ExamDate_e));
        }

        sbWhere.Append(" EI_Type=2 AND (tblECircular.EI_ScoreF>0 or tblECircular.EI_Score1>0 or tblECircular.EI_Score2>0 or tblECircular.EI_Score3>0 or tblECircular.EI_Score4>0) AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EI_Mode_str", typeof(string));
        dt.Columns.Add("EI_AppDate_Str", typeof(string));
        dt.Columns.Add("EI_CompDate_Str", typeof(string));
        dt.Columns.Add("EI_Title_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["EI_AppDate"].ToString() != "")
                dr["EI_AppDate_Str"] = DateTime.Parse(dr["EI_AppDate"].ToString()).ToString("yyyy/MM/dd");
            if (dr["EI_CompDate"].ToString() != "")
                dr["EI_CompDate_Str"] = DateTime.Parse(dr["EI_CompDate"].ToString()).ToString("yyyy/MM/dd");
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("136", dr["EI_Mode"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_Mode_str"] = dr_Phrase["TypeName"].ToString();
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("E010", dr["EI_Title"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_Title_str"] = dr_Phrase["TypeName"].ToString();
            }

        }
    }
    private static void DataProcess_pass(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EI_ExamType_str", typeof(string));
        dt.Columns.Add("EI_ExamDate_Str", typeof(string));
        dt.Columns.Add("EF_File_link", typeof(string));
        dt.Columns.Add("EI_ReviewProgress_str", typeof(string));
        dt.Columns.Add("EI_Benotified_str", typeof(string));
        dt.Columns.Add("EI_Progress_str", typeof(string));
        dt.Columns.Add("btn_ReviewScore", typeof(string));
        dt.Columns.Add("btn_mail", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["EI_ExamDate"].ToString() != "")
                dr["EI_ExamDate_Str"] = DateTime.Parse(dr["EI_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EI_ExamType"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_ExamType_str"] = dr_Phrase["TypeName"].ToString();
            }
            DataRow dr_EF1 = DataEFile.DataReader_EF_TransNo(dr["EF_TransNo"].ToString());
            if (dr_EF1 != null)
            {
                dr["EF_File_link"] = string.Format("<a href=\"/uploads/ECircular/{0}\" target=\"_blank\">{0}</a>", dr_EF1["EF_Name"].ToString());
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("144", dr["EI_ReviewProgress"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_ReviewProgress_str"] = dr_Phrase["TypeName"].ToString();
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("147", dr["EI_Benotified"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_Benotified_str"] = dr_Phrase["TypeName"].ToString();
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("146", dr["EI_Progress"].ToString());
            if (dr_Phrase != null)
            {
                dr["EI_Progress_str"] = dr_Phrase["TypeName"].ToString();
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Mode_str", "教育訓練類型", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_AppDate_Str", "申請日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_CompDate_Str", "完訓日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Score", "分數", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Title_str", "職稱", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_pass(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamType_str", "考試類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamDate_Str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EF_File_link", "檔案名稱", false, 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_ReviewScore(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamType_str", "考試類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamDate_Str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ReviewProgress_str", "複查進度", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ReviewScore", "複查分數", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_ReviewScore", "申請複查", 100, "Center"));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_PassSend(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamType_str", "考試類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_ExamDate_Str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Benotified_str", "通知", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EI_Progress_str", "申請進度", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_mail", "Email通知", 100, "Center"));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblECircular where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblECircular";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 更新網站基本資料
    public static RServiceProvider updateField(Dictionary<string, string> FieldData, Dictionary<string, string> QueryData)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder cmdText = new StringBuilder();
        StringBuilder sb_field = new StringBuilder();
        StringBuilder sb_field_q = new StringBuilder();
        foreach(var field in FieldData)
        {
            lSqlParas.Add(SqlAccess.CreateSqlParameter(string.Format("@{0}", field.Key), field.Value));
            sb_field.AppendFormat("{0}=@{0}, ", field.Key);
        }
        foreach (var field in QueryData)
        {
            lSqlParas.Add(SqlAccess.CreateSqlParameter(string.Format("@q_{0}", field.Key), field.Value));
            sb_field_q.AppendFormat("{0}=@q_{0} and ", field.Key);
        }
        cmdText.AppendFormat(" UPDATE tblECircular SET {0} WHERE {1} ", sb_field.Remove(sb_field.Length - 2, 2), sb_field_q.Remove(sb_field_q.Length - 5, 5));

        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdText.ToString(), lSqlParas.ToArray());

        return rsp;
    }
    #endregion

    #region
    public static bool valid_EI(string EG_TransNo, string EI_AccID)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));
        dParas.Add("EI_AccID", SqlAccess.CreateSqlParameter("@EI_AccID", EI_AccID));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EG_TransNo=@EG_TransNo AND EI_AccID=@EI_AccID AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    public static bool valid_import(string EG_TransNo, string EI_ID, string EI_ExamType)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));
        dParas.Add("EI_ID", SqlAccess.CreateSqlParameter("@EI_ID", EI_ID));
        dParas.Add("EI_ExamType", SqlAccess.CreateSqlParameter("@EI_ExamType", EI_ExamType));

        string cmdtxt = "SELECT * FROM tblECircular WHERE EG_TransNo=@EG_TransNo and EI_ID=@EI_ID and EI_ExamType=@EI_ExamType and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            return true;
        else
            return false;
    }

    public static DataSet DataReader_EG_TransNo(string EG_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));

        StringBuilder cmdtxt = new StringBuilder();

        cmdtxt.Append("Select ROW_NUMBER() OVER( ORDER BY tblECircular.EI_AccID) AS SerialNo, ");
        cmdtxt.Append("tblECircular.*, tblERegion.EG_Type, tabcSales.PS_Title, tabcSales.PS_EMAIL,");
        cmdtxt.Append("tabcSales.cZON_TYPE, tabcSales.cZON_NAME, tabcSales.cZON_PK+tabcSales.cZON_NAME cZON_PK_NAME, tabcSales.DM_Name, ");
        cmdtxt.Append("(select TypeName  from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblECircular.EI_ExamType) EI_ExamTypeName ");
        cmdtxt.Append("from tblECircular  ");
        cmdtxt.Append("Left Join tblERegion on tblECircular.EG_TransNo= tblERegion.EG_TransNo and tblECircular.EG_UidNo=tblERegion.EG_UidNo  ");
        cmdtxt.Append("left join  ");
        cmdtxt.Append("( ");
        cmdtxt.Append("SELECT   ");
        cmdtxt.Append("tblPerson.PS_Title, tblPerson.PS_EMAIL, s2.Name DM_Name, ");
        cmdtxt.Append("tabcSales.Title, tblPhrase.TypeCode,  tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate, tabcSales.Gender, ");
        cmdtxt.Append("tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, tabcSales.Leave_Date,  ");
        cmdtxt.Append("tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education,  ");
        cmdtxt.Append("tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_PK, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC, ");
        cmdtxt.Append("tabcSales_Regedit.REG_END_L, tabcSales_Regedit.REG_DATE_I, tabcSales_Regedit.REG_DATE_F, tabcSales_Regedit.REG_END_P, tabcSales_Regedit.REG_FINSH_L ");
        cmdtxt.Append("FROM tabcSales ");
        cmdtxt.Append("left Join tblPerson on tabcSales.cSAL_FK=tblPerson.AccID ");
        cmdtxt.Append("left Join tabcSales s2 on tabcSales.DM=s2.cSAL_FK ");
        cmdtxt.Append("inner Join tabcUnit on tabcSales.Zone= tabcUnit.cZON_PK ");
        cmdtxt.Append("inner Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0,6,7) ");
        cmdtxt.Append("inner Join tabcEducation ON tabcSales.Education=tabcEducation.iEDU_PK ");
        cmdtxt.Append("inner Join tabcSales_Regedit on tabcSales.cSAL_FK= tabcSales_Regedit.cSAL_FK ");
        cmdtxt.Append(") tabcSales on tabcSales.cSAL_FK=tblECircular.EI_AccID ");
        cmdtxt.Append("where tblECircular.ModiState<>'D' and tblECircular.EI_Type=2 and tblERegion.EG_TransNo=@EG_TransNo ");
        cmdtxt.Append("Order by tblECircular.EI_AccID ");


        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        return emds;


    }

}