﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataPhrase
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet DataReader_TypeCode(string TypeCode)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode order by TypeNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet DataReader_TypeSubCode(string TypeSubCode)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@TypeSubCode", TypeSubCode));
        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeSubCode=@TypeSubCode order by TypeNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet DataReader_TypeCode_TypeNoList(string TypeCode, string TypeNoList)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        string cmdtxt = string.Format("SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode and TypeNo in ({0}) order by TypeNo", TypeNoList);

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet DataReader(string TypeCode, string TypeName, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(TypeCode, TypeName, lSqlParas);
        string sort = "TypeCode,TypeNo";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("*, PK_TypeName ");

        StringBuilder columns = new StringBuilder();
        columns.Append("*, (SELECT PK_TypeName FROM tblPhraseKind where tblPhraseKind.PK_TypeCode=tblPhrase.TypeCode) PK_TypeName");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblPhrase", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string TypeCode, string TypeName)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblPhrase");
        string where = whereString(TypeCode, TypeName, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string TypeCode, string TypeName, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (TypeCode.Length > 0)
        {
            sbWhere.Append(" TypeCode=@TypeCode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        }
        else
        {
            sbWhere.Append(" TypeCode in (SELECT PK_TypeCode FROM tblPhraseKind) AND ");
        }
        if (TypeName.Length > 0)
        {
            sbWhere.Append(" TypeName LIKE '%'+@TypeName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@TypeName", TypeName));
        }
        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("PK_TypeCodeName", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["PK_TypeCodeName"] = string.Format("【{0}】{1}", dr["TypeCode"].ToString(), dr["PK_TypeName"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("PK_TypeCodeName", "類別", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("TypeName", "名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("TypeNo", "序號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("TypeSubCode", "代碼", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("TypeOrder", "排序", 100, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblPhrase where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblPhrase";
        return emds.Tables[0].Clone();
    }
    #endregion

    public static DataRow dataReader_TypeCode_TypeNo(string TypeCode, string TypeNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("TypeCode", SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        dParas.Add("TypeNo", SqlAccess.CreateSqlParameter("@TypeNo", TypeNo));

        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode and TypeNo=@TypeNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow dataReader_TypeMemo_TypeNo(string TypeMemo, string TypeNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("TypeMemo", SqlAccess.CreateSqlParameter("@TypeMemo", TypeMemo));
        dParas.Add("TypeNo", SqlAccess.CreateSqlParameter("@TypeNo", TypeNo));

        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeMemo=@TypeMemo and TypeNo=@TypeNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow dataReader_TypeCode_TypeName(string TypeCode, string TypeName)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("TypeCode", SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        dParas.Add("TypeName", SqlAccess.CreateSqlParameter("@TypeName", TypeName));

        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode and TypeName=@TypeName";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow dataReader_TypeCode_TypeSubCode(string TypeCode, string TypeSubCode)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("TypeCode", SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        dParas.Add("TypeSubCode", SqlAccess.CreateSqlParameter("@TypeSubCode", TypeSubCode));

        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode and TypeSubCode=@TypeSubCode";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static DataTable DDL_TypeName(DropDownList DDL, bool all, string allString, string TypeCode)
    {
        DDL.Items.Clear();
        DataSet emds = DataReader_TypeCode(TypeCode);
        DataTable dt = new DataTable();
        dt.Columns.Add("TypeCode", typeof(string));
        dt.Columns.Add("TypeNo", typeof(string));
        dt.Columns.Add("TypeName", typeof(string));
        dt.Columns.Add("TypeSubCode", typeof(string));
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
            {
                ListItem listItem = new ListItem(dr["TypeName"].ToString(), dr["TypeNo"].ToString());
                dt.Rows.Add(dr["TypeCode"].ToString(), dr["TypeNo"].ToString(), dr["TypeName"].ToString(), dr["TypeSubCode"].ToString());
                //listItem.Attributes.Add("TypeSubCode", dr["TypeSubCode"].ToString());
                DDL.Items.Add(listItem);
            }
        }
        return dt;
    }
    public static DataTable DDL_TypeName_TypeSubCode(DropDownList DDL, bool all, string allString, string TypeSubCode)
    {
        DDL.Items.Clear();
        DataSet emds = DataReader_TypeSubCode(TypeSubCode);
        DataTable dt = new DataTable();
        dt.Columns.Add("TypeCode", typeof(string));
        dt.Columns.Add("TypeNo", typeof(string));
        dt.Columns.Add("TypeName", typeof(string));
        dt.Columns.Add("TypeSubCode", typeof(string));
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
            {
                ListItem listItem = new ListItem(dr["TypeName"].ToString(), dr["TypeNo"].ToString());
                dt.Rows.Add(dr["TypeCode"].ToString(), dr["TypeNo"].ToString(), dr["TypeName"].ToString(), dr["TypeSubCode"].ToString());
                //listItem.Attributes.Add("TypeSubCode", dr["TypeSubCode"].ToString());
                DDL.Items.Add(listItem);
            }
        }
        return dt;
    }
    public static DataTable CBL_TypeName(CheckBoxList cbl, string TypeCode)
    {
        cbl.Items.Clear();
        DataSet emds = DataReader_TypeCode(TypeCode);
        DataTable dt = new DataTable();
        dt.Columns.Add("TypeCode", typeof(string));
        dt.Columns.Add("TypeNo", typeof(string));
        dt.Columns.Add("TypeName", typeof(string));
        dt.Columns.Add("TypeSubCode", typeof(string));
        if (MainControls.checkDataSet(emds))
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
            {
                ListItem listItem = new ListItem(dr["TypeName"].ToString(), dr["TypeNo"].ToString());
                dt.Rows.Add(dr["TypeCode"].ToString(), dr["TypeNo"].ToString(), dr["TypeName"].ToString(), dr["TypeSubCode"].ToString());
                //listItem.Attributes.Add("TypeSubCode", dr["TypeSubCode"].ToString());
                cbl.Items.Add(listItem);
            }
        }
        return dt;
    }
    public static DataTable CBL_TypeName(RadioButtonList cbl, string TypeCode)
    {
        cbl.Items.Clear();
        DataSet emds = DataReader_TypeCode(TypeCode);
        DataTable dt = new DataTable();
        dt.Columns.Add("TypeCode", typeof(string));
        dt.Columns.Add("TypeNo", typeof(string));
        dt.Columns.Add("TypeName", typeof(string));
        dt.Columns.Add("TypeSubCode", typeof(string));
        if (MainControls.checkDataSet(emds))
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
            {
                ListItem listItem = new ListItem(dr["TypeName"].ToString(), dr["TypeNo"].ToString());
                dt.Rows.Add(dr["TypeCode"].ToString(), dr["TypeNo"].ToString(), dr["TypeName"].ToString(), dr["TypeSubCode"].ToString());
                //listItem.Attributes.Add("TypeSubCode", dr["TypeSubCode"].ToString());
                cbl.Items.Add(listItem);
            }
        }
        return dt;
    }
    public static DataTable CBL_TypeName(RadioButtonList cbl, string TypeCode, string TypeNoList)
    {
        cbl.Items.Clear();
        DataSet emds = DataReader_TypeCode_TypeNoList(TypeCode, TypeNoList);
        DataTable dt = new DataTable();
        dt.Columns.Add("TypeCode", typeof(string));
        dt.Columns.Add("TypeNo", typeof(string));
        dt.Columns.Add("TypeName", typeof(string));
        dt.Columns.Add("TypeSubCode", typeof(string));
        if (MainControls.checkDataSet(emds))
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
            {
                ListItem listItem = new ListItem(dr["TypeName"].ToString(), dr["TypeNo"].ToString());
                dt.Rows.Add(dr["TypeCode"].ToString(), dr["TypeNo"].ToString(), dr["TypeName"].ToString(), dr["TypeSubCode"].ToString());
                //listItem.Attributes.Add("TypeSubCode", dr["TypeSubCode"].ToString());
                cbl.Items.Add(listItem);
            }
        }
        return dt;
    }
    #endregion

    #region
    public static bool valid(string TypeCode, string TypeNo, string TypeName)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("TypeCode", SqlAccess.CreateSqlParameter("@TypeCode", TypeCode));
        dParas.Add("TypeNo", SqlAccess.CreateSqlParameter("@TypeNo", TypeNo));
        dParas.Add("TypeName", SqlAccess.CreateSqlParameter("@TypeName", TypeName));

        string cmdtxt = "SELECT * FROM tblPhrase WHERE TypeCode=@TypeCode AND TypeNo=@TypeNo AND TypeName=@TypeName";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

}