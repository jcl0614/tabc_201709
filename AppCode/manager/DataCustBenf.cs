﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataCustBenf
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_BE_TransNo(string BE_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("BE_TransNo", SqlAccess.CreateSqlParameter("@BE_TransNo", BE_TransNo));

        string cmdtxt = "SELECT * FROM tblCustBenf WHERE BE_TransNo=@BE_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_PO_TransNo(string PO_TransNo)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("PO_TransNo", SqlAccess.CreateSqlParameter("@PO_TransNo", PO_TransNo));

        string cmdtxt = "SELECT * FROM tblCustBenf WHERE PO_TransNo=@PO_TransNo and ModiState<>'D' order by BE_Relation";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    //public static DataSet DataReader(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, int startRowIndex, int maximumRows)
    //{
    //    DataSet emds = new DataSet();
    //    List<SqlParameter> lSqlParas = new List<SqlParameter>();
    //    string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
    //    string sort = "Server_ModiDate desc";

    //    StringBuilder Vcolumns = new StringBuilder();
    //    Vcolumns.Append("* ");

    //    StringBuilder columns = new StringBuilder();
    //    columns.Append("* ");

    //    string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblCustBenf", where, sort, startRowIndex, maximumRows);
    //    if (lSqlParas.Count == 0)
    //        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
    //    else
    //        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
    //    DataProcess(emds);
    //    return emds;
    //}
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    //public static int GetCount(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e)
    //{
    //    //參數
    //    List<SqlParameter> lSqlParas = new List<SqlParameter>();
    //    StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblCustBenf");
    //    string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
    //    sbSql.Append(where);
    //    if (lSqlParas.Count == 0)
    //        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

    //    return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    //}
    #endregion

    #region 查詢條件處理
    //public static String whereString(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, List<SqlParameter> lSqlParas)
    //{
    //    StringBuilder sbWhere = new StringBuilder();

    //    if (EN_Year.Length > 0)
    //    {
    //        sbWhere.Append(" EN_Year=@EN_Year AND ");
    //        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Year", EN_Year));
    //    }
    //    if (EN_Type.Length > 0)
    //    {
    //        sbWhere.Append(" EN_Type=@EN_Type AND ");
    //        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Type", EN_Type));
    //    }
    //    if (EN_AccID.Length > 0)
    //    {
    //        sbWhere.Append(" (EN_AccID LIKE '%'+@EN_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblCustBenf.EN_AccID) LIKE '%'+@EN_AccID+'%') AND ");
    //        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_AccID", EN_AccID));
    //    }

    //    sbWhere.Append(" ModiState<>@ModiState AND ");
    //    lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

    //    if (sbWhere.Length > 0)
    //    {
    //        sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
    //        sbWhere.Insert(0, " WHERE ");
    //    }
    //    return sbWhere.ToString();
    //}
    #endregion

    #region 處理動態欄位
    public static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("CU_CustName", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_CU = DataCustomer.DataReader_CU_TransNo(dr["CU_TransNo"].ToString());
            if (dr_CU != null)
                dr["CU_CustName"] = dr_CU["CU_CustName"].ToString();
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "編輯", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_Kind", "種類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_CustName", "受益人", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_Relation", "關係", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_BenefSuccession", "順位", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_BenefPercent", "比例", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_BenefID", "身分證號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("BE_Age", "年齡", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider Append(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    public static RServiceProvider Update(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblCustBenf where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblCustBenf";
        return emds.Tables[0].Clone();
    }
    #endregion




}