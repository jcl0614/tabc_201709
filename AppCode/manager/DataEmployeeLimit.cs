﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Text;

/// <summary>
/// EmployeeLimit 的摘要描述
/// </summary>
public class DataEmployeeLimit
{
    #region  員工系統使用權限資料讀取讀取
    /// <summary>
    /// 員工系統使用權限資料讀取讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet EmployeeLimitReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, emNo, (SELECT emName FROM sys_Employee WHERE emNo=sys_EmployeeLimit.emNo) as emName, muNo, (SELECT muName FROM sys_Menu WHERE sys_Menu.muNo=sys_EmployeeLimit.muNo) as muName, emInsert, emUpdate, emSelect, emDelete FROM sys_EmployeeLimit ORDER BY emNo, muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }

    public static DataTable EmployeeLimitReader(string muNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, emNo, muNo FROM sys_EmployeeLimit WHERE muNo=@muNo ORDER BY emNo, muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@muNo", muNo.Trim()));
        emds.Tables[0].TableName = "sys_EmployeeLimit";
        return emds.Tables[0].Copy();
    }

    /// <summary>
    /// 查詢員工資料
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet EmployeeLimitReaderForEmployee(string emNo, string muLayer, string sortExpression, int startRowIndex, int maximumRows)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbVirtualColumn = new StringBuilder("id")
                .Append(", muNo")
                .Append(", muName")
                .Append(", emInsert")
                .Append(", emUpdate")
                .Append(", emSelect")
                .Append(", emDelete");

        StringBuilder sbColumn = new StringBuilder("id")
                .Append(", muNo")
                .Append(", (SELECT muName FROM sys_Menu WHERE sys_Menu.muNo=sys_EmployeeLimit.muNo) as muName")
                .Append(", emInsert")
                .Append(", emUpdate")
                .Append(", emSelect")
                .Append(", emDelete");

        if (sortExpression.Trim().Length == 0)
            sortExpression = "muNo";

        string where = whereString(emNo, muLayer, lSqlParas);
        string cmdtxt = MainControls.sqlSelect(sbVirtualColumn.ToString()
            , sbColumn.ToString()
            , "sys_EmployeeLimit"
            , where
            , sortExpression
            , startRowIndex
            , maximumRows);

        if (lSqlParas.Count == 0)
            return SqlAccess.SqlcommandExecute("0", cmdtxt);

        return SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray<SqlParameter>());
    }
    /// <summary>
    /// 筆數
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string emNo, string muLayer)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_EmployeeLimit");
        string where = whereString(emNo, muLayer, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    /// <summary>
    /// 查詢條件處理
    /// </summary>
    /// <returns></returns>
    public static String whereString(string emNo, string muLayer, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        sbWhere.Append(" emNo=@emNo AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));

        if (muLayer != "")
        {
            sbWhere.Append(" (select muLayer from sys_Menu where sys_Menu.muNo=sys_EmployeeLimit.muNo)=@muLayer AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLayer", muLayer));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }

    #endregion

    #region 取得系統程式使用權限
    public static MaintainAuthority EmployeeLimitReader(string emNo, string muHyperLink)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emInsert, emUpdate, emSelect, emDelete FROM sys_EmployeeLimit WHERE emNo=@emNo AND muNo=(SELECT muNo FROM sys_Menu WHERE muHyperLink=@muHyperLink)";
        List<SqlParameter> lParas = new List<SqlParameter>();
        lParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        lParas.Add(SqlAccess.CreateSqlParameter("@muHyperLink", muHyperLink));
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lParas.ToArray());

        bool elInster = false;
        bool emUpdate = false;
        bool emSelect = false;
        bool emDelete = false;

        if (emds != null && emds.Tables.Count > 0 && emds.Tables[0].Rows.Count > 0)
        {
            DataRow row = emds.Tables[0].Rows[0];
            elInster = Convert.ToBoolean(row["emInsert"]);
            emUpdate = Convert.ToBoolean(row["emUpdate"]);
            emSelect = Convert.ToBoolean(row["emSelect"]);
            emDelete = Convert.ToBoolean(row["emDelete"]);
        }
        return new MaintainAuthority(elInster, emUpdate, emSelect, emDelete);
    }
    #endregion

    #region 功能表資料行
    public static void EmployeeLimitDataColumn(GridView mgv)
    {
        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "Key", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);

        mgv.Columns.Add(DataModel.CreateBoundField("muNo", "編號", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("muName", "程式名稱", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("emInsert", "新增", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("emUpdate", "修改", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("emSelect", "查詢", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("emDelete", "刪除", 40));
        mgv.Columns.Add(DataModel.CreateCommandField(ButtonType.Button, false, true, true, 150));
        (mgv.Columns[0] as BoundField).ReadOnly = true;
        (mgv.Columns[1] as BoundField).ReadOnly = true;
        (mgv.Columns[2] as BoundField).ReadOnly = true;

        
    }
    #endregion

    #region 更新資料庫(員工)
    /// <summary>
    /// 更新資料庫(員工)
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="emNo">員工帳號</param>
    /// <param name="muNo">程式編號</param>
    /// <param name="emInsert">新增功能</param>
    /// <param name="emUpdate">修改功能</param>
    /// <param name="emSelect">查詢功能</param>
    /// <param name="emDelete">刪除功能</param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void EmployeeLimitUpdate(string id, string emNo, string progId, string muNo, bool emInsert, bool emUpdate, bool emSelect, bool emDelete)
    {
        DataTable dt = GetSchema();
        dt.Columns.Remove("emNo");
        dt.Columns.Remove("muNo");
        dt.Rows.Add(id, emInsert, emUpdate, emSelect, emDelete);
        DataSet emds = new DataSet();
        emds.Tables.Add(dt);

        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, emds, DataemDaily.MaintainStatus.Update);
        SqlAccess.SqlCommandUpdate("0", emds);
    }
    #endregion

    #region 刪除資料庫(員工)
    /// <summary>
    /// 刪除資料庫(員工)
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="emNo">員工帳號</param>
    /// <param name="muNo">程式編號</param>
    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static void EmployeeLimitDelete(string id, string emNo, string progId, string muNo)
    {
        DataTable dt = GetSchema();
        dt.Columns.Remove("emNo");
        dt.Columns.Remove("muNo");
        dt.Columns.Remove("emInsert");
        dt.Columns.Remove("emUpdate");
        dt.Columns.Remove("emSelect");
        dt.Columns.Remove("emDelete");
        dt.Rows.Add(id);
        DataSet emds = new DataSet();
        emds.Tables.Add(dt);

        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, emds, DataemDaily.MaintainStatus.Delete);
        SqlAccess.SqlCommandDelete("0", emds);

    }
    #endregion

    #region  刪除員工所有權限
    public static RServiceProvider emNoEmployeeLimitDelete(string emNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "DELETE FROM sys_EmployeeLimit WHERE emNo=@emNo";

        return SqlAccess.SqlcommandNonQueryExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emNo", emNo));
    }
    #endregion

    #region 新增資料庫(員工)
    /// <summary>
    /// 新增資料庫(員工)
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider EmployeeLimitAppend(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region  複製群組權限
    public static RServiceProvider CopyEmGroupLimit(string emNo, string gpId)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo.Trim()));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@gpId", gpId.Trim()));
        StringBuilder sb_cmdtxt = new StringBuilder();
        //刪除原本權限
        sb_cmdtxt.Append("DELETE FROM sys_EmployeeLimit WHERE emNo=@emNo;");
        //新增群組權限
        sb_cmdtxt.Append("INSERT INTO sys_EmployeeLimit (emNo,muNo,emInsert,emUpdate,emSelect,emDelete) SELECT @emNo,muNo,gpInsert,gpUpdate,gpSelect,gpDelete FROM sys_EmGroupLimit WHERE gpId=@gpId;");

        return SqlAccess.SqlcommandNonQueryExecute("0", sb_cmdtxt.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 功能啟用項讀取成ListItems
    public static void FunctionReaderCheckBoxList(CheckBoxList mossRCBL)
    {
        string[] func = new string[]{
            "emInsert,新增",
            "emUpdate,修改",
            "emSelect,查詢",
            "emDelete,刪除"};

        mossRCBL.Items.Clear();
        foreach (string str in func)
        {
            string[] mstr = str.Split(',');
            mossRCBL.Items.Add(new ListItem(mstr[1], mstr[0]));
        }
    }
    #endregion
    
    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, emNo, muNo, emInsert, emUpdate, emSelect, emDelete FROM sys_EmployeeLimit WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_EmployeeLimit";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  驗證權限是否已經加入
    public static Boolean emNoLimitValid(string emNo, string muNo)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string cmdtxt = "SELECT * FROM sys_EmployeeLimit WHERE emNo=@emNo AND muNo=@muNo";
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo.Trim()));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muNo", muNo.Trim()));
        int count = (int)SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray()).Tables[0].Rows.Count;
        if (count == 0)
            return true;
        else
            return false;
    }
    #endregion

}
