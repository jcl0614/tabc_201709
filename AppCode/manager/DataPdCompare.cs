﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// DataLeftLinkClass 的摘要描述
/// </summary>
public class DataPdCompare
{
    #region  資料讀取
    public static DataSet Itemds=null;
    public static DataTable dtCompare=null;
    public static string CompanyName = "";
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT TOP 5 *, (case class when '1' then '系統公告' else '行事曆' end) AS className, (SELECT CONVERT(varchar(10), addDate, 120)) AS DateStr FROM sys_SysNews ORDER BY addDate desc";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet DataReader(string iStatus, string CompanyNo, string Saled, string Channel, string DispMark, string Keyword, string DispClass, int startRowIndex, int maximumRows)
    {
        EX_DataAPI ed = new EX_DataAPI();
        DataTable dt = new DataTable();
 Itemds = new DataSet();
 string InsItem = ed.GetInsItemsByCompanyNo(Saled, CompanyNo);
 //cmpyNo|iStus|yyyyMMddHH|Channel|IP
 if (InsItem != "{}" && InsItem.Length > 0)
 {
     //ItemList險種清單欄位說明：
     //Mark險種代碼：
     //DispMark險種顯示代碼：
     //DispClass險種類別：
     //InsName險種名稱：

     //YearList險種年期清單：
     //Mark險種代碼：
     //Yearly年期值：
     //YName年期名稱：

     //UnitList險種單位資料：使用者輸入的保額需要受限於ItemList的「MinVal保額下限」與「MaxVal保額上限」，避免傳給昇華進行「商品比較」的資料錯誤，而無法回傳相關保障計算。
     //Mark險種代碼：
     //Val保額：
     //Project單位：顯示輸入「保額」欄位後方的單位名稱，若取得之值有數值，請拿掉。
     JObject jo = JObject.Parse(InsItem);
    //  Itemds = JsonConvert.DeserializeObject<DataSet>(InsItem);
     //  dt.TableName = "sys_PdInfo";
     string ItemList = jo["ItemList"].ToString();
     if (ItemList != "[]" && ItemList.Length > 0)
         dt = JsonConvert.DeserializeObject<DataTable>(ItemList);
     else
     {
         dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
         dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
         dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
         dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
     }
     dt.TableName = "ItemList";
     Itemds.Tables.Add(dt);
     dt = JsonConvert.DeserializeObject<DataTable>(jo["YearList"].ToString());
     dt.TableName = "YearList";
     Itemds.Tables.Add(dt);
     dt = JsonConvert.DeserializeObject<DataTable>(jo["UnitList"].ToString());
     dt.TableName = "UnitList";
     Itemds.Tables.Add(dt);
 }
 else
 {
     //    dt.Columns.Add(new DataColumn("CompanyNo", System.Type.GetType("System.String")));
     //   dt.Columns.Add(new DataColumn("CompanyName", System.Type.GetType("System.String")));
     dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
     dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
     dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
     dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
     //   dt.Columns.Add(new DataColumn("Unit", System.Type.GetType("System.String")));
     //   dt.Columns.Add(new DataColumn("Saled", System.Type.GetType("System.Boolean")));
     //   dt.Columns.Add(new DataColumn("Keyword", System.Type.GetType("System.String")));
     //   dt.Columns.Add(new DataColumn("Master", System.Type.GetType("System.Boolean")));
     //   dt.Columns.Add(new DataColumn("MinVal", System.Type.GetType("System.Int16")));
     //   dt.Columns.Add(new DataColumn("MaxVal", System.Type.GetType("System.Int16")));
     //   dt.Columns.Add(new DataColumn("Channel", System.Type.GetType("System.String")));
     dt.TableName = "ItemList";
     Itemds.Tables.Add(dt);
 }

        dt = Itemds.Tables["ItemList"];
        DataSet emds = new DataSet();
        DataView dv = new DataView(dt, " 1=1 "
           + (DispClass.Length == 0 ? "" : " and DispClass = '" + DispClass + "'")
           + (DispMark.Length == 0 ? "" : " and DispMark like '%" + DispMark + "%'")
           + (Keyword.Length == 0 ? "" : " and InsName like '%" + Keyword + "%'")
, "", DataViewRowState.CurrentRows);
        DataTable dtPdInfo = GetPagedTable(dv.ToTable(), startRowIndex, maximumRows);

        //string DispClassName = "";
        //string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='061' AND ModiState <> 'D' order by TypeOrder";
        //DataTable dtDispClass = SqlAccess.SqlcommandExecute("0", cmdtxt).Tables[0];
        //foreach (DataRow dr in dtDispClass.Rows)
        //{
        //    DispClassName += "IIF(DispClass = '" + dr["TypeSubCode"].ToString() + "', '" + dr["TypeName"].ToString() + "',";
        //}
        //DispClassName += "DispClass)";

        //for (int i = 0; i < dtDispClass.Rows.Count - 1; i++)
        //{
        //    DispClassName += ")";
        //}

        //DataColumn DispClassNameColumn = new DataColumn();
        //DispClassNameColumn.DataType = System.Type.GetType("System.String");
        //DispClassNameColumn.ColumnName = "DispClassName";
        //DispClassNameColumn.Expression = DispClassName;
        //dtPdInfo.Columns.Add(DispClassNameColumn);

        DataColumn CompanyNameColumn = new DataColumn();
        CompanyNameColumn.DataType = System.Type.GetType("System.String");
        CompanyNameColumn.ColumnName = "CompanyName";
        CompanyNameColumn.Expression = "IIF(1 = 1, '" + CompanyName + "', '')";// CompanyName;
        dtPdInfo.Columns.Add(CompanyNameColumn);

        DataColumn CompanyNoColumn = new DataColumn();
        CompanyNoColumn.DataType = System.Type.GetType("System.String");
        CompanyNoColumn.ColumnName = "CompanyNo";
        CompanyNoColumn.Expression ="IIF(1 = 1, '" + CompanyNo + "', '')";// CompanyNo;
        dtPdInfo.Columns.Add(CompanyNoColumn);


        dtPdInfo.TableName = "ItemList";
        emds.Tables.Add(dtPdInfo);

        //if (dtPdInfo.Rows.Count > 0)
        //{
        //  RowsRadioButtonList  Mark
        //}

        DataProcess(emds);
        return emds;
    }

    /// <summary> 將DataTable進行分頁 </summary>
    /// <param name="dt"> 原DataTable
    /// <param name="PageIndex"> 選擇第N頁
    /// <param name="PageSize"> 每頁的頁數 
    /// <returns> 新DataTable</returns>
    public static DataTable GetPagedTable(DataTable dt, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
        {
            return dt;
        }
        DataTable NewDt = dt.Copy();
        NewDt.Clear();
        //起始列
        int rowbegin = PageIndex;// (PageIndex - 1) * PageSize;
        //結束列
        int rowend = PageIndex + PageSize;// PageIndex* PageSize;
        if (rowbegin >= dt.Rows.Count)
        {
            return NewDt;
        }

        if (rowend > dt.Rows.Count)
        {
            rowend = dt.Rows.Count;
        }
        //產生新的DataTable
        for (int i = rowbegin; i <= rowend - 1; i++)
        {
            DataRow newdr = NewDt.NewRow();
            DataRow dr = dt.Rows[i];
            foreach (DataColumn column in dt.Columns)
            {
                newdr[column.ColumnName] = dr[column.ColumnName];
            }
            NewDt.Rows.Add(newdr);
        }
        return NewDt;
    }

    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string iStatus, string CompanyNo, string Saled, string Channel, string DispMark, string Keyword, string DispClass)
    {
        EX_DataAPI ed = new EX_DataAPI();
        DataTable dt = new DataTable();
        Itemds = new DataSet();

        string InsItem = ed.GetInsItemsByCompanyNo(Saled, CompanyNo);
        //cmpyNo|iStus|yyyyMMddHH|Channel|IP
        if (InsItem != "{}" && InsItem.Length > 0)
        {
            //ItemList險種清單欄位說明：
            //Mark險種代碼：
            //DispMark險種顯示代碼：
            //DispClass險種類別：
            //InsName險種名稱：

            //YearList險種年期清單：
            //Mark險種代碼：
            //Yearly年期值：
            //YName年期名稱：

            //UnitList險種單位資料：使用者輸入的保額需要受限於ItemList的「MinVal保額下限」與「MaxVal保額上限」，避免傳給昇華進行「商品比較」的資料錯誤，而無法回傳相關保障計算。
            //Mark險種代碼：
            //Val保額：
            //Project單位：顯示輸入「保額」欄位後方的單位名稱，若取得之值有數值，請拿掉。
            JObject jo = JObject.Parse(InsItem);
            //  Itemds = JsonConvert.DeserializeObject<DataSet>(InsItem);
            //  dt.TableName = "sys_PdInfo";
            string ItemList = jo["ItemList"].ToString();
            if (ItemList != "[]" && ItemList.Length > 0)
                dt = JsonConvert.DeserializeObject<DataTable>(ItemList);
            else
            {
                dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
            }
            dt.TableName = "ItemList";
            Itemds.Tables.Add(dt);
            dt = JsonConvert.DeserializeObject<DataTable>(jo["YearList"].ToString());
            dt.TableName = "YearList";
            Itemds.Tables.Add(dt);
            dt = JsonConvert.DeserializeObject<DataTable>(jo["UnitList"].ToString());
            dt.TableName = "UnitList";
            Itemds.Tables.Add(dt);
        }
        else
        {
            //    dt.Columns.Add(new DataColumn("CompanyNo", System.Type.GetType("System.String")));
            //   dt.Columns.Add(new DataColumn("CompanyName", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
            //   dt.Columns.Add(new DataColumn("Unit", System.Type.GetType("System.String")));
            //   dt.Columns.Add(new DataColumn("Saled", System.Type.GetType("System.Boolean")));
            //   dt.Columns.Add(new DataColumn("Keyword", System.Type.GetType("System.String")));
            //   dt.Columns.Add(new DataColumn("Master", System.Type.GetType("System.Boolean")));
            //   dt.Columns.Add(new DataColumn("MinVal", System.Type.GetType("System.Int16")));
            //   dt.Columns.Add(new DataColumn("MaxVal", System.Type.GetType("System.Int16")));
            //   dt.Columns.Add(new DataColumn("Channel", System.Type.GetType("System.String")));
            dt.TableName = "ItemList";
            Itemds.Tables.Add(dt);
        }

        dt = Itemds.Tables["ItemList"];

        //string DispClassName = "";
        //string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='061' AND ModiState <> 'D' order by TypeOrder";
        //DataTable dtDispClass = SqlAccess.SqlcommandExecute("0", cmdtxt).Tables[0];
        //foreach (DataRow dr in dtDispClass.Rows)
        //{
        //    DispClassName += "IIF(DispClass = '" + dr["TypeSubCode"].ToString() + "', '" + dr["TypeName"].ToString() + "',";
        //}
        //DispClassName += "DispClass)";

        //for (int i = 0; i < dtDispClass.Rows.Count - 1; i++)
        //{
        //    DispClassName += ")";
        //}


        DataSet emds = new DataSet();
        DataView dv = new DataView(dt, " 1=1 "
           + (DispClass.Length == 0 ? "" : " and DispClass = '" + DispClass + "'")
           + (DispMark.Length == 0 ? "" : " and DispMark like '%" + DispMark + "%'")
           + (Keyword.Length == 0 ? "" : " and InsName like '%" + Keyword + "%'")
, "", DataViewRowState.CurrentRows);


        return dv.Count;
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string Class, string title, string emNo, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (Class.Length > 0)
        {
            sbWhere.Append(" class=@Class AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@Class", Class));
        }
        if (title.Length > 0)
        {
            sbWhere.Append(" title LIKE '%'+@title+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@title", title));
        }
        if (emNo != "")
        {
            sbWhere.Append(" ((select emNo from sys_Employee where sys_Employee.emNo=sys_SysNews.emNo) LIKE '%'+@emNo+'%' OR (select emName from sys_Employee where sys_Employee.emNo=sys_SysNews.emNo) LIKE '%'+@emNo+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("CompanyName", "公司", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("DispMark", "代碼", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("InsName", "名稱", false, 400, "Left"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("Mark", "key", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("DispClass", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("CompanyNo", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
       // \"Unit\":\"萬元\",\"MinVal\":\"25\",\"MaxVal\":\"300\"

        bf = DataModel.CreateBoundField("Unit", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("MinVal", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("MaxVal", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        //bf = DataModel.CreateBoundField("addDate", "", 10);
        //bf.HeaderStyle.CssClass = "GridViewColHidden";
        //bf.ItemStyle.CssClass = "GridViewColHidden";
        //mgv.Columns.Add(bf);
        //bf = DataModel.CreateBoundField("emNo", "", 10);
        //bf.HeaderStyle.CssClass = "GridViewColHidden";
        //bf.ItemStyle.CssClass = "GridViewColHidden";
        //mgv.Columns.Add(bf);
    }
    public static void DataColumnPDT(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("CompanyName", "公司", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("DispMark", "險種代碼", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("InsName", "險種名稱", false, 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("YName", "年期", false, 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("Amtinput", "保額", false, 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("ModePrem", "保費", false, 80, "Right"));
   //     mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "刪除", "Delete", 40));
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Image, "刪除", "Delete", "images/icon08.gif", 40));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("Mark", "key", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("DispClass", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("CompanyNo", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        // \"Unit\":\"萬元\",\"MinVal\":\"25\",\"MaxVal\":\"300\"

        bf = DataModel.CreateBoundField("Unit", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("Yearly", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("GID", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);

    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  id, class, title, description, emNo FROM sys_SysNews where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_SysNews";
        return emds.Tables[0].Clone();
    }
    #endregion

    public static void CompanyDropDownList(DropDownList DDL, string insType)
    {
        DDL.Items.Clear();
        EX_DataAPI ed = new EX_DataAPI();
        string CmpyList = ed.GetCmpyList(insType);

        if (CmpyList.Length > 0 && CmpyList != "[]")
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(CmpyList);
            foreach (DataRow dr in dt.Rows)
            {
                DDL.Items.Add(new ListItem(string.Format("{0} {1}", dr["CompanyNo"].ToString(), dr["CompanyName"].ToString()), dr["CompanyNo"].ToString()));
            }
        }
        //{"CompanyNo":"AN","CompanyName":"ING安泰"}
    }
    public static void RowsRadioButtonList(RadioButtonList RBL,DataTable dt,String Mark)
    {
                    //YearList險種年期清單：
            //Mark險種代碼：
            //Yearly年期值：
            //YName年期名稱：

        RBL.Items.Clear();

        DataRow[] drs = dt.Select("Mark='" + Mark + "'");
        foreach (DataRow dr in drs)
        {
            RBL.Items.Add(new ListItem(dr["YName"].ToString(), dr["Yearly"].ToString()));
        }
        if (RBL.Items.Count > 0)
            RBL.Items[0].Selected = true;
    }

    public static void PhraseRadioButtonList(RadioButtonList RBL, String TYPECODE, String TYPEKey)
    {
        // (2)險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，以「投資險」取代「投聯險」的顯示。顯示險種類別的清單，取片語tblPhrase.TypeCode='061'，以tblPhrase.TypeOrder排序，顯示tblPhrase.TypeName，但對應tblPhrase.TypeSubCode的值給昇華。
        DataSet Phraseds = new DataSet();
        //    string cmdtxt = "select TypeNo,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeNo";
        string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeOrder";
        Phraseds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        Phraseds.Tables[0].TableName = "tblPhrase";
        RBL.Items.Clear();

        DataTable dt = Phraseds.Tables[0];
        foreach (DataRow dr in dt.Rows)
        {
            RBL.Items.Add(new ListItem(dr["TypeName"].ToString(), dr[TYPEKey].ToString()));
        }
        if(RBL.Items.Count>0)
           RBL.Items[0].Selected=true;
    }
    public static void PhraseDropDownList(DropDownList DDL, String TYPECODE, String TYPEKey)
    {
        // (2)險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，以「投資險」取代「投聯險」的顯示。顯示險種類別的清單，取片語tblPhrase.TypeCode='061'，以tblPhrase.TypeOrder排序，顯示tblPhrase.TypeName，但對應tblPhrase.TypeSubCode的值給昇華。
        DataSet Phraseds = new DataSet();
        //    string cmdtxt = "select TypeNo,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeNo";
        string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeOrder";
        Phraseds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        Phraseds.Tables[0].TableName = "tblPhrase";
        DDL.Items.Clear();

        DataTable dt = Phraseds.Tables[0];
        foreach (DataRow dr in dt.Rows)
        {
            DDL.Items.Add(new ListItem(dr["TypeName"].ToString(), dr[TYPEKey].ToString()));
        }
    }
    public static DataTable GetTable(string mSql)
    {
        DataSet ds = new DataSet();

        ds = SqlAccess.SqlcommandExecute("0", mSql);
        return ds.Tables.Count > 0 ? ds.Tables[0] : null;
    }
    public static int Cint(object o)
    {
        int ret = 0;
        try
        {
            ret = Convert.ToInt32(o);
        }
        catch (Exception)
        {
            
          //  throw;
        }
        return ret;
    }
    public static int InsAge(string o)
    {
        int ret = 0;
        try
        {
            string[] births = o.Split('/');
            DateTime birth = new DateTime(Cint(births[0]), Cint(births[1]), Cint(births[2]));
            ret = DateTime.Now.AddTicks(-1 * birth.Ticks).AddMonths(6).Year;
        }
        catch (Exception)
        {

            //  throw;
        }
        return ret;
    }

}