﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEQues
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblEQues WHERE EQ_Question LIKE '%'+@searchString+'%' AND tblEQues.ModiState<>'D'";
        else
            cmdtxt = "SELECT * FROM tblEQues";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_EQ_TransNo(string EQ_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EQ_TransNo", SqlAccess.CreateSqlParameter("@EQ_TransNo", EQ_TransNo));

        string cmdtxt = "SELECT * FROM tblEQues WHERE EQ_TransNo=@EQ_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EC_TransNo(string EC_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        string cmdtxt = "SELECT * FROM tblEQues WHERE EC_TransNo=@EC_TransNo AND tblEQues.ModiState<>'D'";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_EC_TransNo_random(string EC_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        string cmdtxt = "SELECT * FROM tblEQues WHERE EC_TransNo=@EC_TransNo AND tblEQues.ModiState<>'D' order by EQ_Type,newid()";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_EC_TransNo_random(string EC_TransNo, string EC_QuizNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        string cmdtxt = string.Format("SELECT TOP {0} * FROM tblEQues WHERE EC_TransNo=@EC_TransNo AND tblEQues.ModiState<>'D' order by EQ_Type,newid()", EC_QuizNo == "" ? "0" : EC_QuizNo);

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_EB_TransNo(string EB_TransNo, string EQ_Type, string count)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EB_TransNo", SqlAccess.CreateSqlParameter("@EB_TransNo", EB_TransNo));
        dParas.Add("EQ_Type", SqlAccess.CreateSqlParameter("@EQ_Type", EQ_Type));

        string cmdtxt = string.Format("SELECT TOP {0} * FROM tblEQues WHERE EB_TransNo=@EB_TransNo AND EQ_Type=@EQ_Type AND tblEQues.ModiState<>'D' order by newid()", count);

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_EB_TransNo(string EB_TransNo, string EQ_Item, string EQ_Type, string count)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EB_TransNo", SqlAccess.CreateSqlParameter("@EB_TransNo", EB_TransNo));
        dParas.Add("EQ_Item", SqlAccess.CreateSqlParameter("@EQ_Item", EQ_Item));
        dParas.Add("EQ_Type", SqlAccess.CreateSqlParameter("@EQ_Type", EQ_Type));

        string cmdtxt = string.Format("SELECT TOP {0} * FROM tblEQues WHERE EB_TransNo=@EB_TransNo AND EQ_Item=@EQ_Item AND EQ_Type=@EQ_Type AND tblEQues.ModiState<>'D' order by newid()", count);

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader(string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EQ_Type, EQ_Question, EA_ListItem, EQ_QCode, EQ_Category, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEQues", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_P(string EB_SubType, string EB_Subject, string EQ_Item, string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_P(EB_SubType, EB_Subject, EQ_Item, EQ_Type, EQ_Question, EA_ListItem, EQ_QCode, EQ_Category, lSqlParas);
        string sort = "EQ_Chapter,EQ_QNo,Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEQues", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEQues");
        string where = whereString(EQ_Type, EQ_Question, EA_ListItem, EQ_QCode, EQ_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_P(string EB_SubType, string EB_Subject, string EQ_Item, string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEQues");
        string where = whereString_P(EB_SubType, EB_Subject, EQ_Item, EQ_Type, EQ_Question, EA_ListItem, EQ_QCode, EQ_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EQ_Type.Length > 0)
        {
            sbWhere.Append(" EQ_Type LIKE '%'+@EQ_Type+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Type", EQ_Type));
        }
        if (EQ_Question.Length > 0)
        {
            sbWhere.Append(" EQ_Question LIKE '%'+@EQ_Question+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Question", EQ_Question));
        }
        if (EA_ListItem.Length > 0)
        {
            sbWhere.Append(" (select count(EA_TransNo) from tblEQans where tblEQans.EQ_TransNo=tblEQues.EQ_TransNo and tblEQans.EA_ListItem LIKE '%'+@EA_ListItem+'%')>0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EA_ListItem", EA_ListItem));
        }
        if (EQ_QCode.Length > 0)
        {
            sbWhere.Append(" EQ_QCode LIKE '%'+@EQ_QCode+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_QCode", EQ_QCode));
        }
        if (EQ_Category.Length > 0)
        {
            sbWhere.Append(" EQ_Category=@EQ_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Category", EQ_Category));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_P(string EB_SubType, string EB_Subject, string EQ_Item, string EQ_Type, string EQ_Question, string EA_ListItem, string EQ_QCode, string EQ_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EB_SubType.Length > 0)
        {
            sbWhere.Append(" (select EB_SubType from tblExamSub where tblExamSub.EB_TransNo=tblEQues.EB_TransNo)=@EB_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        }
        if (EB_Subject.Length > 0)
        {
            sbWhere.Append(" (select EB_Subject from tblExamSub where tblExamSub.EB_TransNo=tblEQues.EB_TransNo)=@EB_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));
        }
        if (EQ_Item.Length > 0)
        {
            sbWhere.Append(" EQ_Item=@EQ_Item AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Item", EQ_Item));
        }
        if (EQ_Type.Length > 0)
        {
            sbWhere.Append(" EQ_Type LIKE '%'+@EQ_Type+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Type", EQ_Type));
        }
        if (EQ_Question.Length > 0)
        {
            sbWhere.Append(" EQ_Question LIKE '%'+@EQ_Question+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Question", EQ_Question));
        }
        if (EA_ListItem.Length > 0)
        {
            sbWhere.Append(" (select count(EA_TransNo) from tblEQans where tblEQans.EQ_TransNo=tblEQues.EQ_TransNo and tblEQans.EA_ListItem LIKE '%'+@EA_ListItem+'%')>0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EA_ListItem", EA_ListItem));
        }
        if (EQ_QCode.Length > 0)
        {
            sbWhere.Append(" EQ_QCode LIKE '%'+@EQ_QCode+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_QCode", EQ_QCode));
        }
        if (EQ_Category.Length > 0)
        {
            sbWhere.Append(" EQ_Category=@EQ_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_Category", EQ_Category));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EA_ListItem", typeof(string));
        dt.Columns.Add("EB_Explan", typeof(string));
        dt.Columns.Add("EB_SubType_str", typeof(string));
        dt.Columns.Add("EB_Subject_str", typeof(string));
        dt.Columns.Add("EQ_Item_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, dr["EQ_TransNo"].ToString()).Tables[0];
            StringBuilder sb = new StringBuilder();
            foreach (DataRow item in dt_ans.Rows)
            {
                if (item["EA_IsCorrect"].ToString() == "Y")
                    sb.AppendFormat("<span style=\"color:#009900\">({0}) {1}</span><br>", item["EA_OrderNo"].ToString(), item["EA_ListItem"].ToString());
                else
                    sb.AppendFormat("<span>({0}) {1}</span><br>", item["EA_OrderNo"].ToString(), item["EA_ListItem"].ToString());
            }
            dr["EA_ListItem"] = sb.ToString();
            if (dr["EQ_Category"].ToString() == "3")
            {
                DataRow dr_ExamSub = DataExamSub.DataReader_EB_TransNo(dr["EB_TransNo"].ToString());
                if (dr_ExamSub != null)
                {
                    dr["EB_Explan"] = dr_ExamSub["EB_Explan"].ToString();
                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_ExamSub["EB_SubType"].ToString());
                    if (dr_Phrase != null)
                        dr["EB_SubType_str"] = dr_Phrase["TypeName"].ToString();
                    dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("163", dr["EQ_Item"].ToString());
                    if (dr_Phrase != null)
                        dr["EQ_Item_str"] = dr_Phrase["TypeName"].ToString();
                    string EB_SubjectTypeCode = "";
                    switch (dr_ExamSub["EB_SubType"].ToString())
                    {
                        case "E011":
                            EB_SubjectTypeCode = "138";
                            break;
                        case "E012":
                            EB_SubjectTypeCode = "158";
                            break;
                        case "E013":
                            EB_SubjectTypeCode = "159";
                            break;
                        case "E014":
                            EB_SubjectTypeCode = "160";
                            break;
                    }
                    dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EB_SubjectTypeCode, dr_ExamSub["EB_Subject"].ToString());
                    if (dr_Phrase != null)
                        dr["EB_Subject_str"] = dr_Phrase["TypeName"].ToString();
                }
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Type", "題型", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Question", "題目", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_ListItem", "答案", false, 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_QCode", "測驗代碼", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_P(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        //mgv.Columns.Add(DataModel.CreateBoundField("EB_Explan", "報考科目", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_SubType_str", "考試類別", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Subject_str", "科目", 100, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Item_str", "項目", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Type", "題型", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Chapter", "章節", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_QNo", "題號", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_Question", "題目", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_ListItem", "答案", false, 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EQ_QCode", "測驗代碼", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEQues where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEQues";
        return emds.Tables[0].Clone();
    }
    #endregion





}