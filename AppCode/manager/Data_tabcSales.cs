﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class Data_tabcSales
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tabcSales WHERE (NAME LIKE '%'+@searchString+'%' OR cSAL_FK LIKE '%'+@searchString+'%') AND tabcSales.Status in (0,6,7)";
        else
            cmdtxt = "SELECT * FROM tabcSales";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_AccID(string AccID)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        string cmdtxt = "SELECT tabcSales.*,tabcUnit.* FROM tabcSales LEFT JOIN tabcUnit on tabcSales.Zone=tabcUnit.cZON_PK WHERE tabcSales.cSAL_FK=@AccID";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_DM(string DM)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("DM", SqlAccess.CreateSqlParameter("@DM", DM));

        string cmdtxt = "SELECT * FROM tabcSales WHERE DM=@DM and DATEDIFF(d, '1900/01/01', Leave_Date)=0";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_DM(string DM, string AccID)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("DM", SqlAccess.CreateSqlParameter("@DM", DM));
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        string cmdtxt = "SELECT * FROM tabcSales WHERE DM=@DM AND (cSAL_FK=@AccID OR NAME LIKE '%'+@AccID+'%') and DATEDIFF(d, '1900/01/01', Leave_Date)=0";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_certificate(string Zone, string AccID, string DM, string UM, string PassDate_s, string PassDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_certificate(Zone, AccID, DM, UM, PassDate_s, PassDate_e, lSqlParas);
        string sort = "cSAL_FK";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tabcSales_new", where, sort, startRowIndex, maximumRows);

        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_certificate(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_certificate(string Zone, string AccID, string DM, string UM, string PassDate_s, string PassDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tabcSales_new ");

        string where = whereString_certificate(Zone, AccID, DM, UM, PassDate_s, PassDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString_certificate(string Zone, string AccID, string DM, string UM, string PassDate_s, string PassDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (Zone.Length > 0)
        {
            sbWhere.Append(" (Zone=@Zone OR cZON_NAME LIKE '%'+@Zone+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@Zone", Zone));
        }
        if (AccID != "")
        {
            sbWhere.Append(" (cSAL_FK=@AccID OR NAME LIKE '%'+@AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));
        }
        if (DM != "")
        {
            sbWhere.Append(" (DM=@DM OR DM_NAME LIKE '%'+@DM+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@DM", DM));
        }
        if (UM != "")
        {
            sbWhere.Append(" (UM=@UM OR UM_NAME LIKE '%'+@UM+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@UM", UM));
        }
        if (PassDate_s != "")
        {
            sbWhere.Append("((DATEDIFF(d, @PassDate_s, REG_DATE_L)>=0 AND DATEDIFF(d, @PassDate_s, REG_END_L)>=0) OR DATEDIFF(d, @PassDate_s, REG_DATE_F)>=0 OR DATEDIFF(d, @PassDate_s, REG_DATE_I)>=0 OR (DATEDIFF(d, @PassDate_s, REG_DATE_P)>=0 AND DATEDIFF(d, @PassDate_s, REG_END_P)>=0)) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PassDate_s", PassDate_s));
        }
        if (PassDate_e != "")
        {
            sbWhere.Append("((DATEDIFF(d, @PassDate_e, REG_DATE_L)<=0 AND DATEDIFF(d, @PassDate_s, REG_END_L)<=0) OR DATEDIFF(d, @PassDate_e, REG_DATE_F)<=0 OR DATEDIFF(d, @PassDate_e, REG_DATE_P)<=0 AND DATEDIFF(d, @PassDate_s, REG_END_P)<=0)) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PassDate_e", PassDate_e));
        }
        sbWhere.Append(" DATEDIFF(d, '1900/01/01', Leave_Date)=0 AND ");

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess_certificate(DataSet ds)
    {
        DateTime ServerDateTime = MainControls.ServerDateTime();
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("Unit_AccID_Name", typeof(string));
        dt.Columns.Add("REG_DATE_L_str", typeof(string));
        dt.Columns.Add("REG_DATE_F_str", typeof(string));
        dt.Columns.Add("REG_DATE_I_str", typeof(string));
        dt.Columns.Add("REG_DATE_P_str", typeof(string));
        dt.Columns.Add("REG_DATE__str", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["Unit_AccID_Name"] = string.Format("{0}{1}／{2}／{3}", dr["Zone"].ToString(), dr["cZON_NAME"].ToString(), dr["cSAL_FK"].ToString(), dr["NAME"].ToString());
            if (dr["REG_DATE_L"].ToString() != "" && dr["REG_END_L"].ToString() != "" && DateTime.Parse(dr["REG_DATE_L"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" && DateTime.Parse(dr["REG_END_L"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" &&
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_DATE_L"].ToString())) >= 0 && MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_END_L"].ToString())) <= 0)
                dr["REG_DATE_L_str"] = "V";
            if (dr["REG_DATE_F"].ToString() != "" && DateTime.Parse(dr["REG_DATE_F"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" && 
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_DATE_F"].ToString())) >= 0 &&
                MainControls.DateDiffDays(DateTime.Parse("1900/01/01"), DateTime.Parse(dr["REG_DATE_F"].ToString())) < 0 &&
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_END_L"].ToString())) <= 0)
                dr["REG_DATE_F_str"] = "V";
            if (dr["REG_DATE_I"].ToString() != "" && DateTime.Parse(dr["REG_DATE_I"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" && 
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_DATE_I"].ToString())) >= 0 &&
                MainControls.DateDiffDays(DateTime.Parse("1900/01/01"), DateTime.Parse(dr["REG_DATE_I"].ToString())) < 0 &&
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_END_L"].ToString())) <= 0)
                dr["REG_DATE_I_str"] = "V";
            if (dr["REG_DATE_P"].ToString() != "" && dr["REG_END_P"].ToString() != "" && DateTime.Parse(dr["REG_DATE_P"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" && DateTime.Parse(dr["REG_END_P"].ToString()).ToString("yyyy/MM/dd") != "1900/01/01" &&
                MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_DATE_P"].ToString())) >= 0 && MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["REG_END_P"].ToString())) <= 0)
                dr["REG_DATE_P_str"] = "V";
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn_certificate(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("Unit_AccID_Name", "單位／業務員編號／姓名", 300, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("REG_DATE_L_str", "人身保險證照", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("REG_DATE_F_str", "外幣證照", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("REG_DATE_I_str", "投資型證照", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("REG_DATE_P_str", "產物保險證照", 120, "Center"));
        //mgv.Columns.Add(DataModel.CreateBoundField("REG_DATE_P_str", "證照E<br>補發申請", 120, "Center"));

        //DataTable dt = GetSchema();
        //foreach (DataColumn dc in dt.Columns)
        //{
        //    BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
        //    bf.HeaderStyle.CssClass = "GridViewColHidden";
        //    bf.ItemStyle.CssClass = "GridViewColHidden";
        //    mgv.Columns.Add(bf);
        //}
        
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tabcSales where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tabcSales";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_ET_TransNo(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} ({1})", dr["PS_NAME"].ToString(), dr["AccID"].ToString()), dr["AccID"].ToString()));
        }
    }
    #endregion

    #region  驗證編號
    public static Boolean valid_AccID(string AccID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tabcSales WHERE AccID=@AccID";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@AccID", AccID));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region  判斷是否處經理
    public static Boolean isDM(string AccID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tabcSales WHERE DM=@AccID";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@AccID", AccID));
        if (emds.Tables[0].Rows.Count == 0)
            return false;
        else
            return true;
    }
    #endregion

    public static DataRow DataReader_Full_AccID(string AccID)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        StringBuilder cmdtxt = new StringBuilder();

        cmdtxt.Append("SELECT ");
        cmdtxt.Append("tblPerson.PS_Title, s2.Name DM_Name, ");
        cmdtxt.Append("tabcSales.Title, tblPhrase.TypeCode,  tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate,  ");
        cmdtxt.Append("tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, s2.NAME,tabcSales.UM, s3.NAME, tabcSales.Leave_Date,  ");
        cmdtxt.Append("tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education,  ");
        cmdtxt.Append("tabcEducation.iEDU_PK, tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC, ");
        cmdtxt.Append("tabcSales_Regedit.REG_END_L, tabcSales_Regedit.REG_DATE_I, tabcSales_Regedit.REG_DATE_F, tabcSales_Regedit.REG_END_P, tabcSales_Regedit.REG_FINSH_L ");
        cmdtxt.Append("FROM tabcSales ");
        cmdtxt.Append("left Join tblPerson on tabcSales.cSAL_FK=tblPerson.AccID ");
        cmdtxt.Append("left Join tabcUnit on tabcSales.Zone= tabcUnit.cZON_PK ");
        cmdtxt.Append("left Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0,6,7) ");
        cmdtxt.Append("left Join tabcEducation ON tabcSales.Education=tabcEducation.iEDU_PK ");
        cmdtxt.Append("left Join tabcSales s2 on tabcSales.DM=s2.cSAL_FK ");
        cmdtxt.Append("left Join tabcSales s3 on tabcSales.UM=s3.cSAL_FK ");
        cmdtxt.Append("left Join tabcSales_Regedit on tabcSales.cSAL_FK= tabcSales_Regedit.cSAL_FK ");
        cmdtxt.Append("WHERE tabcSales.cSAL_FK=@AccID ");

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }

    public static DataSet DataReader_Full_DM(string DM)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("DM", SqlAccess.CreateSqlParameter("@DM", DM));

        StringBuilder cmdtxt = new StringBuilder();

        cmdtxt.Append("SELECT ");
        cmdtxt.Append("tblPerson.PS_Title, s2.Name DM_Name, ");
        cmdtxt.Append("tabcSales.Title, tblPhrase.TypeCode,  tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate,  ");
        cmdtxt.Append("tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, s2.NAME,tabcSales.UM, s3.NAME, tabcSales.Leave_Date,  ");
        cmdtxt.Append("tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education,  ");
        cmdtxt.Append("tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC, ");
        cmdtxt.Append("tabcSales_Regedit.REG_END_L, tabcSales_Regedit.REG_DATE_L, tabcSales_Regedit.REG_DATE_P, tabcSales_Regedit.REG_DATE_I, tabcSales_Regedit.REG_DATE_F, tabcSales_Regedit.REG_END_P, tabcSales_Regedit.REG_FINSH_L ");
        cmdtxt.Append("FROM tabcSales ");
        cmdtxt.Append("left Join tblPerson on tabcSales.cSAL_FK=tblPerson.AccID ");
        cmdtxt.Append("left Join tabcUnit on tabcSales.Zone= tabcUnit.cZON_PK ");
        cmdtxt.Append("left Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0,6,7) ");
        cmdtxt.Append("left Join tabcEducation ON tabcSales.Education=tabcEducation.iEDU_PK ");
        cmdtxt.Append("left Join tabcSales s2 on tabcSales.DM=s2.cSAL_FK ");
        cmdtxt.Append("left Join tabcSales s3 on tabcSales.UM=s3.cSAL_FK ");
        cmdtxt.Append("left Join tabcSales_Regedit on tabcSales.cSAL_FK= tabcSales_Regedit.cSAL_FK ");
        cmdtxt.Append("WHERE tabcSales.DM=@DM and DATEDIFF(d, '1900/01/01', tabcSales.Leave_Date)=0 ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());

        return emds;
    }

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_AccID_DM(DropDownList DDL, bool all, string allString, string DM)
    {
        DDL.Items.Clear();
        DataSet emds = DataReader_Full_DM(DM);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} ({1})", dr["NAME"].ToString(), dr["cSAL_FK"].ToString()), dr["cSAL_FK"].ToString()));
        }
    }
    #endregion

}