﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Collections;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.ComponentModel;
using Newtonsoft.Json;

/// <summary>
/// DataPdInfo 的摘要描述
/// </summary>
public class DataPdInfo
{

    //異動狀態
    public enum MaintainStatus
    {
        Insert,
        Update,
        Delete,
        Select
    }
    #region 
    /// <summary>
    ///  商品百科險種列表
    /// </summary>
    /// <param name="iStatus">按保險公司性質找出所有符合的公司資料 0:壽險 1:產險</param>
    /// <param name="CompanyNo">公司代碼</param>
    /// <param name="Master">主附約：Master，其值為TRUE，表示險種為主約；反之，FALSE則為附約</param>
    /// <param name="DispClass">險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，請以「投資型」取代「投聯型」的顯示</param>
    /// <param name="Saled">狀態：Saled，其值為TRUE，表示現售；反之，FALSE則為停售</param>
    /// <param name="Channel">若為台名通路，則是「TABC」，否則為「null」值</param>
    /// <param name="DispMark">險種代號</param>
    /// <param name="InsName">險種名稱</param>
    /// <param name="Keyword">關鍵字查詢：以模糊查詢方式，同時查詢DispMark或InsName欄位值</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">每頁筆數</param>
    /// <returns></returns>       
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet PdInfoReader(string iStatus, string CompanyNo, string Master, string DispClass, string Saled, string Channel, string DispMark, string InsName, string Keyword, string sortExpression, int startRowIndex, int maximumRows)
    {
        //主附約  //險種類別  //
     //   ComKind CompanyNo  PdtState  TABCPdtKind  TABCPdtCode
                EX_DataAPI ed = new EX_DataAPI();
            DataTable dt = new DataTable();
            dt.TableName = "sys_PdInfo";

       //     string InsItem = ed.GetInsItemListByCmpy(CompanyNo, iStatus, Channel);
            string InsItem = ed.GetInsItemListByCmpy(CompanyNo, iStatus);
            //iStatus|CmpyNo|yyyyMMddHH|Channel|IP
            if (InsItem != "[]" && InsItem.Length >0)
            {
                //   "CompanyNo": "TW",  
                //"CompanyName": "台壽",
                //"Mark": "010",
                //"DispMark": "010",
                //"InsName": "國民養老保險",
                //"DispClass": "A",                  //
                //"Unit": "萬元",
                //"Saled": true,
                //"Keyword": "養老;",
                //"Master": true,
                //"MinVal": 10,
                //"MaxVal": 2000,
                //"Channel": null
                //   1.3.2.1商品百科   的最下方有寫，畫面中文名稱對照昇華欄位說明

                //6.取得險種清單後，再依使用的查詢條件進行篩選。
                //(1)主附約：Master，其值為TRUE，表示險種為主約；反之，FALSE則為附約。
                //(2)險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，請以「投資型」取代「投聯型」的顯示。
                //string[] iType = { "壽險主約", "壽險附約", "意外險", "醫療險", "防癌險", "其他", "投聯險", "年金險" };
                //string[] iChar = { "A", "D", "G", "J", "M", "P", "V", "S" };
                //(3)狀態：Saled，其值為TRUE，表示現售；反之，FALSE則為停售。
                //(4)台名銷售：Channel，若為台名通路，則是「TABC」，否則為「null」值。
                //(5)險種代號：以模糊查詢方式，比對DispMark欄位值。
                //(6)險種名稱：以模糊查詢方式，比對InsName欄位值。
                //(7)關鍵字查詢：以模糊查詢方式，同時查詢DispMark或InsName欄位值
                dt = JsonConvert.DeserializeObject<DataTable>(InsItem);
                dt.TableName = "sys_PdInfo";
            }
            else
            {
                dt.Columns.Add(new DataColumn("CompanyNo", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("CompanyName", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Unit", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Saled", System.Type.GetType("System.Boolean")));
                dt.Columns.Add(new DataColumn("Keyword", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Master", System.Type.GetType("System.Boolean")));
                dt.Columns.Add(new DataColumn("MinVal", System.Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("MaxVal", System.Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("Channel", System.Type.GetType("System.String")));
            }
        DataSet emds = new DataSet();

        DataView dv = new DataView(dt, 
            " DispClass='" + DispClass + "'"
           + (Master.Length == 0 ? "" : " and Master='" + (Master == "1" ? "True" : "False") + "'")
            //  + (Saled.Length == 0 ? "" : " and Saled='" + Saled  + "'")
              + (Channel.Length == 0 ? "" : " and Channel='" + Channel + "'")
           + (DispMark.Length == 0 ? "" : " and DispMark like '%" + DispMark + "%'")
           + (InsName.Length == 0 ? "" : " and InsName like '%" + InsName + "%'")
           + (Keyword.Length == 0 ? "" : " and Keyword like '%" + Keyword + "%'")
, "", DataViewRowState.CurrentRows);
        DataTable dtPdInfo = GetPagedTable(dv.ToTable(),startRowIndex, maximumRows);

                dtPdInfo.TableName = "sys_PdInfo";

                DataColumn MasterNameColumn = new DataColumn();
                MasterNameColumn.DataType = System.Type.GetType("System.String");
                MasterNameColumn.ColumnName = "MasterName";
                MasterNameColumn.Expression = "IIF(Master = 'True', '壽險主約', '壽險副約')";
                dtPdInfo.Columns.Add(MasterNameColumn);
                string DispClassName = "";       
        string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='061' AND ModiState <> 'D' order by TypeOrder";
        DataTable dtDispClass = SqlAccess.SqlcommandExecute("0", cmdtxt).Tables[0];
        foreach (DataRow dr in dtDispClass.Rows)
        {
           DispClassName+="IIF(DispClass = '"+dr["TypeSubCode"].ToString()+"', '"+dr["TypeName"].ToString()+"',";
        }
        			 DispClassName+="DispClass)";

for (int i = 0; i < dtDispClass.Rows.Count-1; i++)
			{
			 DispClassName+=")";
}

        DataColumn DispClassNameColumn = new DataColumn(); 
	    DispClassNameColumn.DataType = System.Type.GetType("System.String"); 
	    DispClassNameColumn.ColumnName = "DispClassName";
        DispClassNameColumn.Expression = DispClassName;
        dtPdInfo.Columns.Add(DispClassNameColumn);

        DataColumn SaledNameColumn = new DataColumn();
        SaledNameColumn.DataType = System.Type.GetType("System.String");
        SaledNameColumn.ColumnName = "SaledName";
        SaledNameColumn.Expression = "IIF(Saled = 'True', '停售', '現售')";
        dtPdInfo.Columns.Add(SaledNameColumn);
        DataColumn ChannelNameColumn = new DataColumn();
        ChannelNameColumn.DataType = System.Type.GetType("System.String");
        ChannelNameColumn.ColumnName = "ChannelName";
        ChannelNameColumn.Expression = "IIF(Channel = 'TABC', '<img src=\"images/Y.jpg\" height=\"24\">', '<img src=\"images/X.jpg\" height=\"24\">')";
        dtPdInfo.Columns.Add(ChannelNameColumn);

        emds.Tables.Add(dtPdInfo);

        DataProcess(emds);
        return emds;
    }
    /// <summary> 將DataTable進行分頁 </summary>
    /// <param name="dt"> 原DataTable
    /// <param name="PageIndex"> 選擇第N頁
    /// <param name="PageSize"> 每頁的頁數 
    /// <returns> 新DataTable</returns>
    public static DataTable GetPagedTable(DataTable dt, int PageIndex, int PageSize)
    {
        if (PageIndex == 0)
        {
            return dt;
        }
        DataTable NewDt = dt.Copy();
        NewDt.Clear();
        //起始列
        int rowbegin = PageIndex;// (PageIndex - 1) * PageSize;
        //結束列
        int rowend = PageIndex + PageSize;// PageIndex* PageSize;
        if (rowbegin >= dt.Rows.Count)
        {
            return NewDt;
        }

        if (rowend > dt.Rows.Count)
        {
            rowend = dt.Rows.Count;
        }
        //產生新的DataTable
        for (int i = rowbegin; i <= rowend - 1; i++)
        {
            DataRow newdr = NewDt.NewRow();
            DataRow dr = dt.Rows[i];
            foreach (DataColumn column in dt.Columns)
            {
                newdr[column.ColumnName] = dr[column.ColumnName];
            }
            NewDt.Rows.Add(newdr);
        }
        return NewDt;
    }
    #region 取得總筆數
    /// <summary>
    /// 總筆數
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="edDateBeg">維護日期起</param>
    /// <param name="edDateEnd">維護日期訖</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="key">關鍵值</param>
    /// <returns></returns>
    public static int GetCount(string iStatus, string CompanyNo, string Master, string DispClass, string Saled, string Channel, string DispMark, string InsName, string Keyword)
    {
        EX_DataAPI ed = new EX_DataAPI();
        DataTable dt = new DataTable();
        dt.TableName = "sys_PdInfo";

      //  string InsItem = ed.GetInsItemListByCmpy(CompanyNo, iStatus, Channel);
        string InsItem = ed.GetInsItemListByCmpy(CompanyNo, iStatus);
        if (InsItem != "[]" && InsItem.Length > 0)
        {
             dt = JsonConvert.DeserializeObject<DataTable>(InsItem);
        dt.TableName = "sys_PdInfo";
            }
            else
            {
                dt.Columns.Add(new DataColumn("CompanyNo", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("CompanyName", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Unit", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Saled", System.Type.GetType("System.Boolean")));
                dt.Columns.Add(new DataColumn("Keyword", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Master", System.Type.GetType("System.Boolean")));
                dt.Columns.Add(new DataColumn("MinVal", System.Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("MaxVal", System.Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("Channel", System.Type.GetType("System.String")));
            }

//        DataSet emds = new DataSet();

//        DataView dv = new DataView(dt, "Master=" + (Master == "1" ? "True" : "False") +
//            " and DispClass='" + DispClass +"'"
//           + (Saled.Length == 0 ? "" : " and Saled=" + Saled + "")
//           + (DispMark.Length == 0 ? "" : " and DispMark like '%" + DispMark + "%'")
//           + (InsName.Length == 0 ? "" : " and InsName like '%" + InsName + "%'")
//           + (Keyword.Length == 0 ? "" : " and Keyword like '%" + Keyword + "%'")
//, "", DataViewRowState.CurrentRows);
//        DataTable dtPdInfo = dv.ToTable();

//        return dtPdInfo.Rows.Count;

        return dt.Select(" DispClass='" + DispClass + "'"
           + (Master.Length == 0 ? "" : " and Master='" + (Master == "1" ? "True" : "False") + "'")
       //    + (Saled.Length == 0 ? "" : " and Saled='" + Saled + "'")
               + (Channel.Length == 0 ? "" : " and Channel='" + Channel + "'")
          + (DispMark.Length == 0 ? "" : " and DispMark like '%" + DispMark + "%'")
           + (InsName.Length == 0 ? "" : " and InsName like '%" + InsName + "%'")
           + (Keyword.Length == 0 ? "" : " and Keyword like '%" + Keyword + "%'")).Length;
    }
    #endregion

    /// <summary>
    /// 過濾條件
    /// </summary>
    /// <param name="edDateBeg">維護日期起</param>
    /// <param name="edDateEnd">維護日期訖</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="key">關鍵值</param>
    /// <param name="lSqlParas"></param>
    /// <returns></returns>
    private static string sqlWhere(string edDateBeg, string edDateEnd, string emNo, string key, string tablename, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        //維護日期起
        if (edDateBeg == null) edDateBeg = "";
        edDateBeg= edDateBeg.Trim();
        //維護日期訖
        if (edDateEnd == null) edDateEnd = "";
        edDateEnd = edDateEnd.Trim();
        //員工代碼
        if (emNo == null) emNo = "";
        emNo = emNo.Trim();
        //關鍵值
        if (key == null) key = "";
        key = key.Trim();

        //維護日期起
        if (edDateBeg.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateBeg, edDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateBeg", edDateBeg));
        }
        //維護日期訖
        if (edDateEnd.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateEnd, edDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateEnd", edDateEnd));
        }

        //員工代碼
        if (emNo.Length > 0)
        {
            sbWhere.AppendFormat("(emNo=@emNo OR (SELECT emName FROM sys_Employee WHERE emNo={0}.emNo) LIKE '%'+@emNo+'%') AND ", tablename);
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        }

        //關建值
        if (key.Length > 0)
        {
            sbWhere.Append("(edSource LIKE '%'+@key+'%' OR ")
                .Append("edTarget LIKE '%'+@key+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@key", key));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }

        return sbWhere.ToString();
    }

    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        //foreach (DataRow dr in dt.Rows)
        //{
        //    dr["edSource"] = dr["edSource"].ToString();
        //    dr["edTarget"] = dr["edTarget"].ToString();
        //}
    }
    #endregion

    #region 功能表資料行
    public static void PdInfoDataColumn(GridView mgv)
    {
        //mgv.Columns.Add(DataModel.CreateBoundField("edDate", "維護日期", "{0:yyyy/MM/dd HH:mm:ss}", 70, true));
        //mgv.Columns.Add(DataModel.CreateBoundField("emNo", "代碼", 100, true));
        //mgv.Columns.Add(DataModel.CreateBoundField("emName", "姓名", 100, true));
        //mgv.Columns.Add(DataModel.CreateBoundField("muName", "程式名稱", 100, "Center", true));
        //mgv.Columns.Add(DataModel.CreateBoundField("edTableName", "資料表名稱", 100, "Center", true));
        //mgv.Columns.Add(DataModel.CreateBoundField("edExecute", "執行", 50));
        //mgv.Columns.Add(DataModel.CreateBoundField("edSource", "原始資料", 400, "Left"));
        //mgv.Columns.Add(DataModel.CreateBoundField("edTarget", "異動資料", 400, "Left"));
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("CompanyName", "公司", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("MasterName", "約別", 50, true));
        mgv.Columns.Add(DataModel.CreateBoundField("DispMark", "險種代號", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("InsName", "險種名稱", 200, true));
      //  mgv.Columns.Add(DataModel.CreateBoundField("InsName", "年期", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("DispClassName", "險種類別", 100, true));
        mgv.Columns.Add(DataModel.CreateBoundField("SaledName", "狀態", 50, true));
     //   mgv.Columns.Add(DataModel.CreateBoundField("ChannelName", "台名銷售", false, 50));
        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("CompanyNo", "公司代碼", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("Mark", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("DispClass", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("Channel", "台名銷售", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        mgv.AllowSorting = true;
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema(string connAddress)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, muName, edExecute, edTableName, edSource, edTarget FROM sys_PdInfo WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema(connAddress, cmdtxt);
        emds.Tables[0].TableName = "sys_PdInfo";
        return emds.Tables[0].Clone();
    }
    public static DataTable GetSchema(SqlTransaction sqlTrans)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, muName, edExecute, edTableName, edSource, edTarget FROM sys_PdInfo WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema(sqlTrans, cmdtxt);
        emds.Tables[0].TableName = "sys_PdInfo";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 員工維護記錄控制
    /// <summary>
    /// 員工維護記錄
    /// </summary>
    /// <param name="connAddress">平台代碼</param>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <param name="ms">維護狀態</param>
    public static void PdInfoInsert(string connAddress, string emNo, string progId, DataSet ds, MaintainStatus ms)
    {
        switch (ms)
        {
            case MaintainStatus.Insert : //新增
                PdInfoInsertAppend(connAddress, emNo, progId, ds);
                break;
            case MaintainStatus.Update: //修改
                PdInfoInsertUpdate(connAddress, emNo, progId, ds);
                break;
            case MaintainStatus.Delete: //刪除
                PdInfoInsertDelete(connAddress, emNo, progId, ds);
                break;
        }
    }

    public static void PdInfoInsert(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds, MaintainStatus ms)
    {
        switch (ms)
        {
            case MaintainStatus.Insert: //新增
                PdInfoInsertAppend(sqlTrans, emNo, progId, ds);
                break;
            case MaintainStatus.Update: //修改
                PdInfoInsertUpdate(sqlTrans, emNo, progId, ds);
                break;
            case MaintainStatus.Delete: //刪除
                PdInfoInsertDelete(sqlTrans, emNo, progId, ds);
                break;
        }
    }

    #endregion

    #region 新增資料庫 - 新增功能
    private static void PdInfoInsertAppend(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("sys_PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbData = new StringBuilder();
                foreach (DataColumn col in dt.Columns)
                    sbData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);

                string sData = sbData.Remove(sbData.Length - 3, 3).ToString();
                //if (sData.Length > 4000)
                //    sData = sData.Substring(0, 4000);
                dtPdInfo.Rows.Add(emNo, progId, "新增", dt.TableName, " ", sData);
            }
        }
        ds.Tables.Add(dtPdInfo);
    }
    private static void PdInfoInsertAppend(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("sys_PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbData = new StringBuilder();
                foreach (DataColumn col in dt.Columns)
                    sbData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);

                string sData = sbData.Remove(sbData.Length - 3, 3).ToString();
                //if (sData.Length > 4000)
                //    sData = sData.Substring(0, 4000);
                dtPdInfo.Rows.Add(emNo, progId, "新增", dt.TableName, " ", sData);
            }
        }
        ds.Tables.Add(dtPdInfo);
    }
    #endregion

    #region 新增資料庫 - 修改功能
    private static void PdInfoInsertUpdate(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("sys_PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            #region 產生查詢原始資料語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk= new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.Columns)
            {
                if (dt.PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                {
                    sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                    ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));                    
                }
                sbField.Append(col.ColumnName).Append(",");
            }
            sbSql.Append("SELECT ")
                .Append(sbField.Remove(sbField.Length-1,1).ToString())
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length-5,5).ToString());
            #endregion
            
            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                StringBuilder sbTargetData = new StringBuilder();
                StringBuilder sbPrimaryKeyData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {                  
                    if( row.RowState == DataRowState.Modified)
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName, DataRowVersion.Original];
                    else
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                    ////記錄primarykey
                    sbPrimaryKeyData.AppendFormat("{0}={1}　↔　", dc.ColumnName, row[dc.ColumnName]);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(connAddress, sbSql.ToString(), lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                    {
                        //比對資料,資料相同不記錄
                        if (dataSet.Tables[0].Rows[0][col.ColumnName].ToString().Trim().CompareTo(row[col.ColumnName].ToString().Trim()) == 0 ||
                            dataSet.Tables[0].PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                            continue;
                        //原始資料
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);
                        //異動資料
                        sbTargetData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);
                    }
                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbPrimaryKeyData.ToString() + sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        string sTargetData = sbPrimaryKeyData.ToString() + sbTargetData.Remove(sbTargetData.Length - 3, 3).ToString();
                        //if (sTargetData.Length > 4000)
                        //    sTargetData = sTargetData.Substring(0, 4000);
                        dtPdInfo.Rows.Add(emNo, progId, "修改", dt.TableName, sSourceData, sTargetData);
                    }
                }
            }
        }
        if (dtPdInfo.Rows.Count > 0)
            ds.Tables.Add(dtPdInfo);
    }

    private static void PdInfoInsertUpdate(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            #region 產生查詢原始資料語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.Columns)
            {
                if (dt.PrimaryKey.Contains<DataColumn>(col)) //是否為Primary Key
                {
                    sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                    ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
                }
                sbField.Append(col.ColumnName).Append(",");
            }
            sbSql.Append("SELECT ")
                .Append(sbField.Remove(sbField.Length - 1, 1).ToString())
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());
            #endregion

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                StringBuilder sbTargetData = new StringBuilder();
                StringBuilder sbPrimaryKeyData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    if( row.RowState == DataRowState.Modified)
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName, DataRowVersion.Original];
                    else
                        (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                    ////記錄primarykey
                    sbPrimaryKeyData.AppendFormat("{0}={1}　↔　", dc.ColumnName, row[dc.ColumnName]);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(sbSql.ToString(), sqlTrans, lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                    {
                        //比對資料,資料相同不記錄
                        if (dataSet.Tables[0].Rows[0][col.ColumnName].ToString().Trim().CompareTo(row[col.ColumnName].ToString().Trim()) == 0 &&
                            !!dataSet.Tables[0].PrimaryKey.Contains<DataColumn>(col))
                            continue;
                        //原始資料
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);
                        //異動資料
                        sbTargetData.AppendFormat("{0}={1}　↔　", col.ColumnName, row[col.ColumnName]);
                    }
                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbPrimaryKeyData.ToString() + sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        string sTargetData = sbPrimaryKeyData.ToString() + sbTargetData.Remove(sbTargetData.Length - 3, 3).ToString();
                        //if (sTargetData.Length > 4000)
                        //    sTargetData = sTargetData.Substring(0, 4000);
                        dtPdInfo.Rows.Add(emNo, progId, "修改", dt.TableName, sSourceData, sTargetData);
                    }
                }
            }
        }
        if (dtPdInfo.Rows.Count > 0)
            ds.Tables.Add(dtPdInfo);
    }
    #endregion

    #region 新增資料庫 - 刪除功能
    private static void PdInfoInsertDelete(string connAddress, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("sys_PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(connAddress);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            //產生查詢語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.PrimaryKey)
            {
                sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
            }
            sbSql.Append("SELECT * ")
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(connAddress, sbSql.ToString(), lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dataSet.Tables[0].Columns)
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);

                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        dtPdInfo.Rows.Add(emNo, progId, "刪除", dt.TableName, sSourceData, " ");
                    }
                }
            }
        }
        if( dtPdInfo.Rows.Count > 0)
            ds.Tables.Add(dtPdInfo);
    }
    private static void PdInfoInsertDelete(SqlTransaction sqlTrans, string emNo, string progId, DataSet ds)
    {
        //已有PdInfo資料表，則移除
        if (ds.Tables.Contains("sys_PdInfo")) ds.Tables.Remove("sys_PdInfo");
        //取得員工維護記錄檔資料結構
        DataTable dtPdInfo = GetSchema(sqlTrans);
        //取出資料
        foreach (DataTable dt in ds.Tables)
        {
            //產生查詢語法
            StringBuilder sbSql = new StringBuilder();
            StringBuilder sbField = new StringBuilder();
            StringBuilder sbPk = new StringBuilder();
            Hashtable ht = new Hashtable();
            foreach (DataColumn col in dt.PrimaryKey)
            {
                sbPk.Append(col.ColumnName).Append("=").Append("@").AppendFormat(col.ColumnName).Append(" AND ");
                ht.Add(col.ColumnName, new SqlParameter("@" + col.ColumnName, DBNull.Value));
            }
            sbSql.Append("SELECT * ")
                .Append(" FROM ")
                .Append(dt.TableName)
                .Append(" WHERE ")
                .Append(sbPk.Remove(sbPk.Length - 5, 5).ToString());

            foreach (DataRow row in dt.Rows)
            {
                StringBuilder sbSourceData = new StringBuilder();
                List<SqlParameter> lPara = new List<SqlParameter>();
                foreach (DataColumn dc in dt.PrimaryKey)
                {
                    (ht[dc.ColumnName] as SqlParameter).Value = row[dc.ColumnName];
                    lPara.Add(ht[dc.ColumnName] as SqlParameter);
                }
                //原始資料
                DataSet dataSet = SqlAccess.SqlcommandExecute(sbSql.ToString(), sqlTrans, lPara.ToArray());
                if (dataSet != null && dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    lPara.Clear();
                    foreach (DataColumn col in dt.Columns)
                        sbSourceData.AppendFormat("{0}={1}　↔　", col.ColumnName, dataSet.Tables[0].Rows[0][col.ColumnName]);

                    if (sbSourceData.Length > 0)
                    {
                        string sSourceData = sbSourceData.Remove(sbSourceData.Length - 3, 3).ToString();
                        //if (sSourceData.Length > 4000)
                        //    sSourceData = sSourceData.Substring(0, 4000);
                        dtPdInfo.Rows.Add(emNo, progId, "刪除", dt.TableName, sSourceData, " ");
                    }
                }
            }
        }
        if (dtPdInfo.Rows.Count > 0)
            ds.Tables.Add(dtPdInfo);
    }
    #endregion

    #region 手動新增
    public static RServiceProvider PdInfoInsert(SqlTransaction sqlTrans, string emNo, string muName, string edTableName, string edSource, string edTarget, MaintainStatus ms)
    {
        string edExecute = "";
        switch (ms)
        {
            case MaintainStatus.Insert :
                edExecute = "新增";
                break;
            case MaintainStatus.Delete :
                edExecute = "刪除";
                break;
            case MaintainStatus.Update:
                edExecute = "修改";
                break;
        }
        string updatecmd = "INSERT INTO sys_PdInfo (emNo, muName, edExecute, edTableName, edSource, edTarget) VALUES (@emNo, @muName, @edExecute, @edTableName, @edSource, @edTarget) ";
        List<SqlParameter> lParas = new List<SqlParameter>();
        lParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        lParas.Add(SqlAccess.CreateSqlParameter("@muName", muName));
        lParas.Add(SqlAccess.CreateSqlParameter("@edExecute", edExecute));
        lParas.Add(SqlAccess.CreateSqlParameter("@edTableName", edTableName));
        lParas.Add(SqlAccess.CreateSqlParameter("@edSource", edSource));
        lParas.Add(SqlAccess.CreateSqlParameter("@edTarget", edTarget));

        return SqlAccess.SqlcommandNonQueryExecute(sqlTrans.Connection, sqlTrans, updatecmd, lParas.ToArray());
    }
    #endregion

    #region 結轉資料至暫存 -- 保留30天內資料
    public static RServiceProvider PdInfoDelete()
    {
        RServiceProvider rsp = new RServiceProvider();
        string cmdtxt = "INSERT INTO sys_PdInfo{OS} SELECT emNo, muName, edExecute, edTableName, edSource, edTarget, edDate FROM sys_emDaily WHERE DATEDIFF( d, edDate, GETDATE()) > 30; "
                        + "DELETE FROM sys_emDaily WHERE DATEDIFF( d, edDate, GETDATE()) > 30";
        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdtxt.Replace("{OS}", "_temp"));
        if (rsp.Result== false)
        {
            rsp.ReturnMessage = string.Format("結轉資料錯誤:{0}", rsp.ReturnMessage);
                return rsp;
        }
        return rsp;
    }
    #endregion

    public static void CompanyDropDownList(DropDownList DDL ,string insType)
    {
        DDL.Items.Clear();
        EX_DataAPI ed = new EX_DataAPI();
        string CmpyList = ed.GetCmpyList(insType);

        if (CmpyList.Length > 0 && CmpyList != "[]")
        {
            DataTable dt = JsonConvert.DeserializeObject<DataTable>(CmpyList);
            foreach (DataRow dr in dt.Rows)
            {
                DDL.Items.Add(new ListItem(string.Format("{0} {1}", dr["CompanyNo"].ToString(), dr["CompanyName"].ToString()), dr["CompanyNo"].ToString()));
            }
        }
        //{"CompanyNo":"AN","CompanyName":"ING安泰"}
    }
    public static void DispClassDropDownList(DropDownList DDL)
    {
        //(2)險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，請以「投資型」取代「投聯型」的顯示。
        string[] iType = { "壽險主約", "壽險附約", "意外險", "醫療險", "防癌險", "其他", "投資型", "年金險" };
        string[] iChar = { "A", "D", "G", "J", "M", "P", "V", "S" };
            for (int i = 0; i < iType.Length; i++)
            {
                DDL.Items.Add(new ListItem(iType[i], iChar[i]));
                
            }
    }
    public static void PhraseDropDownList(DropDownList DDL, String TYPECODE, String TYPEKey)
    {
       // (2)險種類別：DispClass的代碼對應如下，投聯險即為「投資型」險種，以「投資險」取代「投聯險」的顯示。顯示險種類別的清單，取片語tblPhrase.TypeCode='061'，以tblPhrase.TypeOrder排序，顯示tblPhrase.TypeName，但對應tblPhrase.TypeSubCode的值給昇華。
        DataSet Phraseds = new DataSet();
    //    string cmdtxt = "select TypeNo,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeNo";
        string cmdtxt = "select TypeNo,TypeSubCode,TypeName from tblPhrase tblPhrase with(nolock) WHERE COMYCODE='GM' AND TYPECODE='" + TYPECODE + "' AND ModiState <> 'D' order by TypeOrder";
        Phraseds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        Phraseds.Tables[0].TableName = "tblPhrase";
        DDL.Items.Clear();

        DataTable dt = Phraseds.Tables[0];
        foreach (DataRow dr in dt.Rows)
        {
            DDL.Items.Add(new ListItem(dr["TypeName"].ToString(), dr[TYPEKey].ToString()));
        }
    }
    public static DataTable GetTable(string mSql)
    {
        DataSet ds = new DataSet();

        ds = SqlAccess.SqlcommandExecute("0", mSql);
        return ds.Tables.Count >0 ? ds.Tables[0] : null;
    }

    #region 匯出Excel
    #endregion

}
