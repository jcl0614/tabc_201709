﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataERegion
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblERegion WHERE (EG_Name LIKE '%'+@searchString+'%' OR EG_TransNo LIKE '%'+@searchString+'%') AND ModiState<>'D'";
        else
            cmdtxt = "SELECT * FROM tblERegion WHERE (EG_Name LIKE '%'+@searchString+'%' OR EG_TransNo LIKE '%'+@searchString+'%') ";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_EG_TransNo(string EG_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EG_TransNo", SqlAccess.CreateSqlParameter("@EG_TransNo", EG_TransNo));

        StringBuilder cmdtxt = new StringBuilder();
        cmdtxt.Append("SELECT ");
        cmdtxt.Append("tblERegion.*, ");
        cmdtxt.Append("tblExamSub.EB_TransNo, tblExamSub.EB_Regist, tblExamSub.EF_TransNo1, tblExamSub.EF_TransNo2,  ");
        cmdtxt.Append("tblECircular.EI_Type, tblECircular.EF_TransNo, tblECircular.EI_ScoreF, tblECircular.EI_Score1, tblECircular.EI_Score2, tblECircular.EI_Score3, tblECircular.EI_Score4 ");
        cmdtxt.Append("FROM tblERegion ");
        cmdtxt.Append("left join tblExamSub on tblExamSub.EB_SubType=tblERegion.EG_Type and tblExamSub.EB_Subject=tblERegion.EG_Subject and tblExamSub.ModiState<>'D' ");
        cmdtxt.Append("left join tblECircular on tblECircular.EG_TransNo=tblERegion.EG_TransNo and tblECircular.ModiState<>'D' ");
        cmdtxt.Append("WHERE tblERegion.EG_TransNo=@EG_TransNo ");

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EG_Year, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_SDate_s, string EG_SDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EG_Year, EG_Type, EG_Subject, EG_Region, EG_Name, EG_SDate_s, EG_SDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblERegion", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_exam(string Status, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_ExamDate_s, string AccID, string EG_ExamDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_exam(Status, EG_Type, EG_Subject, EG_Region, EG_Name, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        string sort = "tblERegion.EG_ExamDate";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("tblERegion.*,  ");
        Vcolumns.Append("EB_TransNo, EB_Regist, EF_TransNo1, EF_TransNo2, ");
        Vcolumns.Append("EI_Type, EF_TransNo, EI_ScoreF, EI_Score1, EI_Score2, EI_Score3, EI_Score4 ");

        StringBuilder columns = new StringBuilder();
        columns.Append("tblERegion.*, ");
        columns.Append("tblExamSub.EB_TransNo, tblExamSub.EB_Regist, tblExamSub.EF_TransNo1, tblExamSub.EF_TransNo2,  ");
        columns.Append("tblECircular.EI_Type, tblECircular.EF_TransNo, tblECircular.EI_ScoreF, tblECircular.EI_Score1, tblECircular.EI_Score2, tblECircular.EI_Score3, tblECircular.EI_Score4 ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(),
            "tblERegion left join tblExamSub on tblExamSub.EB_SubType=tblERegion.EG_Type and tblExamSub.EB_Subject=tblERegion.EG_Subject and tblExamSub.ModiState<>'D' " +
            string.Format("left join tblECircular on tblECircular.EG_TransNo=tblERegion.EG_TransNo and tblECircular.ModiState<>'D' and tblECircular.EI_AccID='{0}' ", AccID), 
            "tblERegion", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_exam(emds, AccID);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EG_Year, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_SDate_s, string EG_SDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblERegion");
        string where = whereString(EG_Year, EG_Type, EG_Subject, EG_Region, EG_Name, EG_SDate_s, EG_SDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_exam(string Status, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_ExamDate_s, string EG_ExamDate_e, string AccID)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblERegion " +
        "left join tblExamSub on tblExamSub.EB_SubType=tblERegion.EG_Type and tblExamSub.EB_Subject=tblERegion.EG_Subject and tblExamSub.ModiState<>'D' " +
        "left join tblECircular on tblECircular.EG_TransNo=tblERegion.EG_TransNo and tblECircular.ModiState<>'D'"
        );
        string where = whereString_exam(Status, EG_Type, EG_Subject, EG_Region, EG_Name, EG_ExamDate_s, EG_ExamDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EG_Year, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_SDate_s, string EG_SDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EG_Year.Length > 0)
        {
            sbWhere.Append(" EG_Year=@EG_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Year", EG_Year));
        }
        if (EG_Type.Length > 0)
        {
            sbWhere.Append(" EG_Type=@EG_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Type", EG_Type));
        }
        if (EG_Subject.Length > 0)
        {
            sbWhere.Append(" EG_Subject=@EG_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Subject", EG_Subject));
        }
        if (EG_Region.Length > 0)
        {
            sbWhere.Append(" EG_Region LIKE '%'+@EG_Region+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Region", EG_Region));
        }
        if (EG_Name.Length > 0)
        {
            sbWhere.Append(" EG_Name LIKE '%'+@EG_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Name", EG_Name));
        }
        if (EG_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_SDate_s, EG_StartDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_SDate_s", EG_SDate_s));
        }
        if (EG_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_SDate_e, EG_Deadline)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_SDate_e", EG_SDate_e));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_exam(string Status, string EG_Type, string EG_Subject, string EG_Region, string EG_Name, string EG_ExamDate_s, string EG_ExamDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (Status.Length > 0)
        {
            switch(Status)
            {
                case "1":
                    sbWhere.Append("DATEDIFF(d, getdate(), EG_StartDate)<=0 AND DATEDIFF(d, getdate(), EG_Deadline)>=0 AND ");
                    break;
                case "2":
                    sbWhere.Append("DATEDIFF(d, getdate(), EG_Deadline)<0 AND ");
                    break;
                case "3":
                    sbWhere.Append("DATEDIFF(d, getdate(), EG_ExamDate)<0 AND ");
                    break;
                case "4":
                    sbWhere.Append("DATEDIFF(d, getdate(), EG_StartDate)>0 AND ");
                    break;
                case "5":
                    sbWhere.Append("DATEDIFF(d, getdate(), EG_DownLoad)<=0 AND DATEDIFF(d, getdate(), EG_ExamDate)>0 AND ");
                    break;
            }
        }
        if (EG_Type.Length > 0)
        {
            sbWhere.Append(" EG_Type=@EG_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Type", EG_Type));
        }
        if (EG_Subject.Length > 0)
        {
            sbWhere.Append(" EG_Subject=@EG_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Subject", EG_Subject));
        }
        if (EG_Region.Length > 0)
        {
            sbWhere.Append(" EG_Region LIKE '%'+@EG_Region+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Region", EG_Region));
        }
        if (EG_Name.Length > 0)
        {
            sbWhere.Append(" EG_Name LIKE '%'+@EG_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Name", EG_Name));
        }
        if (EG_ExamDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_s, EG_ExamDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_s", EG_ExamDate_s));
        }
        if (EG_ExamDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EG_ExamDate_e, EG_ExamDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_ExamDate_e", EG_ExamDate_e));
        }

        sbWhere.Append(" tblERegion.ModiState<>@ModiState AND tblExamSub.EB_Subject is not null AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EG_Type_str", typeof(string));
        dt.Columns.Add("EG_Subject_str", typeof(string));
        dt.Columns.Add("EG_StartDate_str", typeof(string));
        dt.Columns.Add("EG_Region_str", typeof(string));
        dt.Columns.Add("EG_RegistDate_str", typeof(string));
        dt.Columns.Add("EG_ExamDate_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        dt.Columns.Add("btn_member", typeof(string));
        dt.Columns.Add("btn_notify", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
            if (dr_Phrase != null)
                dr["EG_Type_str"] = dr_Phrase["TypeName"].ToString();
            string EG_SubjectTypeCode = "";
            switch (dr["EG_Type"].ToString())
            {
                case "E011":
                    EG_SubjectTypeCode = "138";
                    break;
                case "E012":
                    EG_SubjectTypeCode = "158";
                    break;
                case "E013":
                    EG_SubjectTypeCode = "159";
                    break;
                case "E014":
                    EG_SubjectTypeCode = "160";
                    break;
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EG_SubjectTypeCode, dr["EG_Subject"].ToString());
            if (dr_Phrase != null)
                dr["EG_Subject_str"] = dr_Phrase["TypeName"].ToString();
            dr["EG_StartDate_str"] = string.Format("{0}<br>至<br>{1}", DateTime.Parse(dr["EG_StartDate"].ToString()).ToString("yyyy/MM/dd"), DateTime.Parse(dr["EG_Deadline"].ToString()).ToString("yyyy/MM/dd"));
            dr["EG_RegistDate_str"] = DateTime.Parse(dr["EG_RegistDate"].ToString()).ToString("yyyy/MM/dd");
            dr["EG_ExamDate_str"] = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            string EG_Region = dr["EG_Region"].ToString();
            StringBuilder sb_EG_Region = new StringBuilder();
            if (EG_Region.IndexOf(";") != -1)
            {
                string[] EG_Region_item = EG_Region.Split(';');
                foreach (string item in EG_Region_item)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] value = item.Split(',');
                        if (value[0] == "Y")
                            sb_EG_Region.AppendFormat("{0}、", value[3]);
                    }
                }
            }
            dr["EG_Region_str"] = sb_EG_Region.ToString().TrimEnd('、');
        }
    }
    private static void DataProcess_exam(DataSet ds, string AccID)
    {
        DateTime ServerDateTime = MainControls.ServerDateTime();
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EG_Type_str", typeof(string));
        dt.Columns.Add("EG_Subject_str", typeof(string));
        dt.Columns.Add("EG_StartDate_str", typeof(string));
        dt.Columns.Add("EG_Region_str", typeof(string));
        dt.Columns.Add("EG_RegistDate_str", typeof(string));
        dt.Columns.Add("EG_ExamDate_str", typeof(string));
        dt.Columns.Add("link_1", typeof(string));
        dt.Columns.Add("link_2", typeof(string));
        dt.Columns.Add("link_3", typeof(string));
        dt.Columns.Add("Status", typeof(string));
        dt.Columns.Add("ExamScore", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
            if (dr_Phrase != null)
            {
                dr["EG_Type_str"] = dr_Phrase["TypeName"].ToString();
                dr["link_1"] = string.Format("<a href=\"/manager/ExamEregion?id={1}&c=1&open=1\">{0}</a>", dr_Phrase["TypeName"].ToString(), dr["EG_TransNo"].ToString());
            }
            DataTable dt_EI = DataECircular.DataReader_EI_AccID(AccID).Tables[0];
            var list_EI = dt_EI.AsEnumerable().Where(
                x => x.Field<int>("EI_Type").ToString() == "2" && x.Field<string>("EI_ExamType").ToString() == dr["EG_Type"].ToString() && x.Field<DateTime>("EI_ExamDate").ToString("yyyy/MM/dd") != DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd")).OrderByDescending(o => o.Field<DateTime>("EI_ExamDate"));
            if (list_EI != null && list_EI.Count() != 0)
            {
                DataRow dr_EI = list_EI.ToList()[0];
                if (dr_EI["EI_ScoreF"].ToString() == "" && dr_EI["EI_Score1"].ToString() == "" && dr_EI["EI_Score2"].ToString() == "" && dr_EI["EI_Score3"].ToString() == "" && dr_EI["EI_Score4"].ToString() == "")
                {
                    dr["link_2"] = string.Format("<a title=\"報名\" href=\"javascript:\" onclick=\"{1}\">{0}</a>", "<img src=\"/images/signup.png\" border=\"0\" />", "alert('台端前次測驗成績尚未放榜，請等待測驗成績公布後，即可開放報名！');");
                    dr["Status"] = "前次測驗成績尚未放榜";
                }
            }
            else
            {
                if (dr["EI_Type"].ToString() == "" || dr["EI_Type"].ToString() == "2")
                {
                    DataTable dr_exam = DataExam.DataReader_EB_TransNo_AccID(dr["EB_TransNo"].ToString(), AccID).Tables[0];
                    if (dr_exam == null || dr_exam.Rows.Count == 0) //尚未完成模擬測驗
                    {
                        dr["link_2"] = string.Format("<a title=\"報名\" href=\"javascript:\" onclick=\"{1}\">{0}</a>", "<img src=\"/images/signup.png\" border=\"0\" />", "alert('您尚未完成模擬測驗，請點選【進入模擬測驗】進行測驗，待測驗完成後即可開放報名。');");
                        dr["Status"] = "未完成模擬測驗";
                    }
                    else
                    {
                        DataRow dr_ePayment = DataEPayment.DataReader_EP_AccID_EG_TransNo(AccID, dr["EG_TransNo"].ToString());
                        if (dr_ePayment != null) //已報名
                        {
                            if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["EG_ExamDate"].ToString())) < 0)
                            {
                                DataRow dr_EF1 = DataEFile.DataReader_EF_TransNo(dr["EF_TransNo"].ToString());
                                if (dr_EF1 != null) //入場證已上傳
                                {
                                    dr["link_2"] = string.Format("<a title=\"入場證下載\" href=\"/manager/ExamEregion?id={1}&c=3&open=1\">{0}</a>", "<img src=\"/images/pdf.png\" border=\"0\" />", dr["EG_TransNo"].ToString());
                                    dr["Status"] = "入場證下載";
                                }
                                else
                                {
                                    dr["link_2"] = string.Format("<a title=\"報名\" href=\"/manager/ExamEregion?id={1}&c=2&open=1\">{0}</a>", "<img src=\"/images/signup.png\" border=\"0\" />", dr["EG_TransNo"].ToString());
                                    dr["Status"] = "已報名";
                                }
                            }
                            else //考試結束
                            {
                                if (dr["EI_ScoreF"].ToString() == "" && dr["EI_Score1"].ToString() == "" && dr["EI_Score2"].ToString() == "" && dr["EI_Score3"].ToString() == "" && dr["EI_Score4"].ToString() == "")
                                {
                                    dr["link_2"] = string.Format("<a title=\"報名\" href=\"javascript:\" onclick=\"{1}\">{0}</a>", "<img src=\"/images/signup.png\" border=\"0\" />", "alert('測驗成績尚未放榜，請等待測驗成績公布後，即可開放報名！');");
                                    dr["ExamScore"] = "未放榜";
                                    dr["Status"] = "未放榜";
                                }
                                else //已放榜
                                {
                                    dr["Status"] = "已放榜";
                                    if (dr["EI_ScoreF"].ToString() != "")
                                        dr["ExamScore"] = dr["EI_ScoreF"].ToString();
                                    else if (dr["EI_Score1"].ToString() != "")
                                        dr["ExamScore"] = dr["EI_Score1"].ToString();
                                    else if (dr["EI_Score2"].ToString() != "")
                                        dr["ExamScore"] = dr["EI_Score2"].ToString();
                                    else if (dr["EI_Score3"].ToString() != "")
                                        dr["ExamScore"] = dr["EI_Score3"].ToString();
                                    else if (dr["EI_Score4"].ToString() != "")
                                        dr["ExamScore"] = dr["EI_Score4"].ToString();
                                }
                            }
                        }
                        else //尚未報名
                        {
                            if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr["EG_Deadline"].ToString())) <= 0)
                            {
                                dr["link_2"] = string.Format("<a title=\"報名\" href=\"/manager/ExamEregion?id={1}&c=1&open=1\">{0}</a>", "<img src=\"/images/signup.png\" border=\"0\" />", dr["EG_TransNo"].ToString());
                                dr["Status"] = "報名中";
                            }
                            else //報名截止
                                dr["Status"] = "報名截止";

                        }
                    }
                }
            }
            dr["link_3"] = string.Format("<a href=\"/manager/ExamEregion?id={1}&c=4&open=1\">{0}</a>", "<img src=\"/images/download.png\" border=\"0\" />", dr["EG_TransNo"].ToString());
            
            string EG_SubjectTypeCode = "";
            switch (dr["EG_Type"].ToString())
            {
                case "E011":
                    EG_SubjectTypeCode = "138";
                    break;
                case "E012":
                    EG_SubjectTypeCode = "158";
                    break;
                case "E013":
                    EG_SubjectTypeCode = "159";
                    break;
                case "E014":
                    EG_SubjectTypeCode = "160";
                    break;
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EG_SubjectTypeCode, dr["EG_Subject"].ToString());
            if (dr_Phrase != null)
                dr["EG_Subject_str"] = dr_Phrase["TypeName"].ToString();
            dr["EG_StartDate_str"] = string.Format("{0}<br>至<br>{1}", DateTime.Parse(dr["EG_StartDate"].ToString()).ToString("yyyy/MM/dd"), DateTime.Parse(dr["EG_Deadline"].ToString()).ToString("yyyy/MM/dd"));
            dr["EG_RegistDate_str"] = DateTime.Parse(dr["EG_RegistDate"].ToString()).ToString("yyyy/MM/dd");
            dr["EG_ExamDate_str"] = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            string EG_Region = dr["EG_Region"].ToString();
            StringBuilder sb_EG_Region = new StringBuilder();
            if (EG_Region.IndexOf(";") != -1)
            {
                string[] EG_Region_item = EG_Region.Split(';');
                foreach (string item in EG_Region_item)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        string[] value = item.Split(',');
                        if (value[0] == "Y")
                            sb_EG_Region.AppendFormat("{0}、", value[3]);
                    }
                }
            }
            dr["EG_Region_str"] = sb_EG_Region.ToString().TrimEnd('、');
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Year", "年度", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Type_str", "考試類別", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Subject_str", "科目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Name", "考試名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_StartDate_str", "報名日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_RegistDate_str", "公會報名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_ExamDate_str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Region_str", "地區", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_ExamResultsImport(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Year", "年度", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Type_str", "考試類別", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Name", "考試名稱", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_ExamDate_str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_member", "名單", 100));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_notify", "通知", 100));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_exam(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("link_1", "證照分類", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Subject_str", "證照科目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("Status", "最新狀態", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_StartDate_str", "報名日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_ExamDate_str", "考試日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EG_Region_str", "地區", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ExamScore", "證照考試成績", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_2", "報名", false, 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_3", "題庫下載", false, 80, "Center"));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblERegion where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblERegion";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_EG_TransNo(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(dr["EG_Name"].ToString(), dr["EG_TransNo"].ToString()));
        }
    }
    #endregion


}