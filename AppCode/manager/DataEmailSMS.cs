﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
///  的摘要描述
/// </summary>
public class DataEmailSMS
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>

    public static DataRow DataReader_E1_TransNo(string E1_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("E1_TransNo", SqlAccess.CreateSqlParameter("@E1_TransNo", E1_TransNo));

        string cmdtxt = "SELECT * FROM tblEmailSMS WHERE E1_TransNo=@E1_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EB_Type, EB_SubType, EB_Subject, EB_Explan, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEmailSMS", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEmailSMS");
        string where = whereString(EB_Type, EB_SubType, EB_Subject, EB_Explan, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EB_Type.Length > 0)
        {
            sbWhere.Append(" EB_Type=@EB_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Type", EB_Type));
        }
        if (EB_SubType.Length > 0)
        {
            sbWhere.Append(" EB_SubType=@EB_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        }
        if (EB_Subject.Length > 0)
        {
            sbWhere.Append(" EB_Subject=@EB_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));
        }
        if (EB_Explan.Length > 0)
        {
            sbWhere.Append(" EB_Explan LIKE '%'+@EB_Explan+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Explan", EB_Explan));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EB_Type_str", typeof(string));
        dt.Columns.Add("EB_SubType_str", typeof(string));
        dt.Columns.Add("EB_Subject_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("137", dr["EB_Type"].ToString());
            if (dr_Phrase != null)
                dr["EB_Type_str"] = dr_Phrase["TypeName"].ToString();
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("144", dr["EB_SubType"].ToString());
            if (dr_Phrase != null)
                dr["EB_SubType_str"] = dr_Phrase["TypeName"].ToString();
            string EB_SubjectTypeCode = "";
            switch (dr["EB_SubType"].ToString())
            {
                case "1":
                    EB_SubjectTypeCode = "138";
                    break;
                case "2":
                    EB_SubjectTypeCode = "158";
                    break;
                case "3":
                    EB_SubjectTypeCode = "159";
                    break;
                case "4":
                    EB_SubjectTypeCode = "160";
                    break;
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EB_SubjectTypeCode, dr["EB_Subject"].ToString());
            if (dr_Phrase != null)
                dr["EB_Subject_str"] = dr_Phrase["TypeName"].ToString();
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Type_str", "測驗大分類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_SubType_str", "測驗次分類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Subject_str", "科目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Explan", "說明", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Regist", "報名費", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_TimeLength", "考試時間", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Reviewfee", "複查費", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider AppendTransaction(string emNo, string progId, DataSet ds)
    {
        RServiceProvider rsp = SqlAccess.SqlCommandAppendTransaction("0", ds);
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert(rsp.ReturnSqlTransaction, emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return rsp;
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEmailSMS where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEmailSMS";
        return emds.Tables[0].Clone();
    }
    #endregion



}