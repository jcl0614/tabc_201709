﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataExamSub
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT *, "+
                "(select TypeName from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblExamSub.EB_SubType) EB_SubTypeName, "+
                "(select TypeName from tblPhrase where tblPhrase.TypeCode=(case EB_SubType when 'E011' then '138' when 'E012' then '158' when 'E013' then '159' when 'E014' then '160' else '' end) and tblPhrase.TypeNo=tblExamSub.EB_Subject) EB_SubjectName " +
                "FROM tblExamSub WHERE ((select TypeName from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblExamSub.EB_SubType) LIKE '%'+@searchString+'%' OR EB_TransNo LIKE '%'+@searchString+'%') AND ModiState<>'D'";
        else
            cmdtxt = "SELECT *, (select TypeName from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblExamSub.EB_SubType) EB_SubTypeName FROM tblExamSub WHERE ((select TypeName from tblPhrase where tblPhrase.TypeCode='144' and tblPhrase.TypeSubCode=tblExamSub.EB_SubType) LIKE '%'+@searchString+'%' OR EB_TransNo LIKE '%'+@searchString+'%')";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_EB_TransNo(string EB_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EB_TransNo", SqlAccess.CreateSqlParameter("@EB_TransNo", EB_TransNo));

        string cmdtxt = "SELECT * FROM tblExamSub WHERE EB_TransNo=@EB_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_EB_SubType(string EB_SubType, string EB_Subject)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EB_SubType", SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        dParas.Add("EB_Subject", SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));

        string cmdtxt = "SELECT * FROM tblExamSub WHERE EB_SubType=@EB_SubType and EB_Subject=@EB_Subject ";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }

    public static DataSet DataReader(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EB_Type, EB_SubType, EB_Subject, EB_Explan, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblExamSub", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_test(string EB_SubType, string EB_Subject, string EG_Name, string EX_Date_s, string EX_Date_e, string AccID, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_test(EB_SubType, EB_Subject, EG_Name, EX_Date_s, EX_Date_e, AccID, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblExamSub", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_test(emds, AccID);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblExamSub");
        string where = whereString(EB_Type, EB_SubType, EB_Subject, EB_Explan, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    public static int GetCount_test(string EB_SubType, string EB_Subject, string EG_Name, string EX_Date_s, string EX_Date_e, string AccID)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblExamSub");
        string where = whereString_test(EB_SubType, EB_Subject, EG_Name, EX_Date_s, EX_Date_e, AccID, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EB_Type, string EB_SubType, string EB_Subject, string EB_Explan, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EB_Type.Length > 0)
        {
            sbWhere.Append(" EB_Type=@EB_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Type", EB_Type));
        }
        if (EB_SubType.Length > 0)
        {
            sbWhere.Append(" EB_SubType=@EB_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        }
        if (EB_Subject.Length > 0)
        {
            sbWhere.Append(" EB_Subject=@EB_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));
        }
        if (EB_Explan.Length > 0)
        {
            sbWhere.Append(" EB_Explan LIKE '%'+@EB_Explan+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Explan", EB_Explan));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_test(string EB_SubType, string EB_Subject, string EG_Name, string EX_Date_s, string EX_Date_e, string AccID, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EB_SubType.Length > 0)
        {
            sbWhere.Append(" EB_SubType=@EB_SubType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        }
        if (EB_Subject.Length > 0)
        {
            sbWhere.Append(" EB_Subject=@EB_Subject AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));
        }
        if (EG_Name.Length > 0)
        {
            sbWhere.Append(" (select count(*) from tblERegion where tblERegion.EG_Type=tblExamSub.EB_SubType and tblERegion.EG_Subject=tblExamSub.EB_Subject and EB_Explan LIKE '%'+@EG_Name+'%' and tblERegion.ModiState<>'D')>0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EG_Name", EG_Name));
        }
        if (EX_Date_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EX_Date_s, (select top 1 EX_Date from tblExam where tblExamSub.EB_TransNo=tblExam.EB_TransNo and tblExam.ModiState<>'D' and tblExam.AccID=@AccID order by tblExam.EX_Score desc))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EX_Date_s", EX_Date_s));
        }
        if (EX_Date_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EX_Date_e, (select top 1 EX_Date from tblExam where tblExamSub.EB_TransNo=tblExam.EB_TransNo and tblExam.ModiState<>'D' and tblExam.AccID=@AccID order by tblExam.EX_Score desc))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EX_Date_e", EX_Date_e));
        }
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));
        //sbWhere.Append(" (select count(*) from tblExam where tblExamSub.EB_TransNo=tblExam.EB_TransNo and tblExamSub.EB_UidNo=tblExam.EB_UidNo and tblExam.AccID=@AccID)>0 AND ");
        //lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));


        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EB_Type_str", typeof(string));
        dt.Columns.Add("EB_SubType_str", typeof(string));
        dt.Columns.Add("EB_Subject_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("137", dr["EB_Type"].ToString());
            if (dr_Phrase != null)
                dr["EB_Type_str"] = dr_Phrase["TypeName"].ToString();
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EB_SubType"].ToString());
            if (dr_Phrase != null)
                dr["EB_SubType_str"] = dr_Phrase["TypeName"].ToString();
            string EB_SubjectTypeCode = "";
            switch (dr["EB_SubType"].ToString())
            {
                case "E011":
                    EB_SubjectTypeCode = "138";
                    break;
                case "E012":
                    EB_SubjectTypeCode = "158";
                    break;
                case "E013":
                    EB_SubjectTypeCode = "159";
                    break;
                case "E014":
                    EB_SubjectTypeCode = "160";
                    break;
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EB_SubjectTypeCode, dr["EB_Subject"].ToString());
            if (dr_Phrase != null)
                dr["EB_Subject_str"] = dr_Phrase["TypeName"].ToString();
        }
    }
    private static void DataProcess_test(DataSet ds, string AccID)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EB_Type_str", typeof(string));
        dt.Columns.Add("EB_SubType_str", typeof(string));
        dt.Columns.Add("EB_Subject_str", typeof(string));
        dt.Columns.Add("EX_Date", typeof(string));
        dt.Columns.Add("EX_AccID", typeof(string));
        dt.Columns.Add("EX_Name", typeof(string));
        dt.Columns.Add("EX_Score", typeof(string));
        dt.Columns.Add("link_1", typeof(string));
        dt.Columns.Add("link_2", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            //dr["link_1"] = string.Format("<a href=\"/manager/ExamTest?id={1}&c=4&open=1\">{0}</a>", "<img src=\"/images/class.png\" border=\"0\" />", dr["EB_TransNo"].ToString());
            DataTable dt_EE = DataEQRule.DataReader(dr["EB_SubType"].ToString(), dr["EB_Subject"].ToString(), "", 0, 99999).Tables[0];
            StringBuilder sb_EE_Item = new StringBuilder();
            foreach (DataRow dr_EE in dt_EE.Rows)
            {
                string EE_ItemName = "";
                DataRow dr_Phrase_ = DataPhrase.dataReader_TypeCode_TypeNo("163", dr_EE["EE_Item"].ToString());
                if (dr_Phrase_ != null)
                    EE_ItemName = dr_Phrase_["TypeName"].ToString();
                sb_EE_Item.AppendFormat("<a title=\"進入測驗\" href=\"/manager/ExamTest?id={1}&i={2}&c=1&open=1\"><img src=\"/images/edit.png\" border=\"0\" />{0}</a><br>", EE_ItemName, dr_EE["EB_TransNo"].ToString(), dr_EE["EE_Item"].ToString());
            }
            dr["link_2"] = sb_EE_Item.ToString();
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("137", dr["EB_Type"].ToString());
            if (dr_Phrase != null)
                dr["EB_Type_str"] = dr_Phrase["TypeName"].ToString();
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EB_SubType"].ToString());
            if (dr_Phrase != null)
                dr["EB_SubType_str"] = dr_Phrase["TypeName"].ToString();
            string EB_SubjectTypeCode = "";
            switch (dr["EB_SubType"].ToString())
            {
                case "E011":
                    EB_SubjectTypeCode = "138";
                    break;
                case "E012":
                    EB_SubjectTypeCode = "158";
                    break;
                case "E013":
                    EB_SubjectTypeCode = "159";
                    break;
                case "E014":
                    EB_SubjectTypeCode = "160";
                    break;
            }
            dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo(EB_SubjectTypeCode, dr["EB_Subject"].ToString());
            if (dr_Phrase != null)
                dr["EB_Subject_str"] = dr_Phrase["TypeName"].ToString();
            DataRow dr_EX = DataExam.DataReader_EB_TransNo_AccID_MAX(dr["EB_TransNo"].ToString(), AccID);
            if (dr_EX != null)
            {
                dr["EX_Date"] = DateTime.Parse(dr_EX["EX_Date"].ToString()).ToString("yyyy/MM/dd");
                dr["EX_Score"] = dr_EX["EX_Score"].ToString();
                DataRow dr_person = DataPerson.DataReader_AccID(dr_EX["AccID"].ToString());
                if (dr_person != null)
                {
                    dr["EX_AccID"] = dr_person["AccID"].ToString();
                    dr["EX_Name"] = dr_person["PS_NAME"].ToString();
                }
                dr["link_1"] = string.Format("<a href=\"/manager/ExamTest?id={1}&c=4&open=1\">{0}</a>", "<img src=\"/images/class.png\" border=\"0\" />", dr["EB_TransNo"].ToString());
            }
            else
                dr["link_1"] = string.Format("<a href=\"/manager/ExamTest?id={1}&c=1&open=1\">{0}</a>", "<img src=\"/images/class.png\" border=\"0\" />", dr["EB_TransNo"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Type_str", "測驗大分類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_SubType_str", "測驗次分類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Subject_str", "科目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Explan", "說明", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Regist", "報名費", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_TimeLength", "考試時間", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Reviewfee", "複查費", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_test(GridView mgv)
    {
        //mgv.Columns.Add(DataModel.CreateBoundField("link_1", "瀏覽", false, 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_SubType_str", "證照分類", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EB_Subject_str", "證照科目", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_Date", "測驗日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_Score", "最佳分數", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_2", "進入測驗", false, 300, "Left"));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblExamSub where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblExamSub";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region
    public static bool valid_EB(string EB_SubType, string EB_Subject)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EB_SubType", SqlAccess.CreateSqlParameter("@EB_SubType", EB_SubType));
        dParas.Add("EB_Subject", SqlAccess.CreateSqlParameter("@EB_Subject", EB_Subject));

        string cmdtxt = "SELECT * FROM tblExamSub WHERE  EB_SubType=@EB_SubType and EB_Subject=@EB_Subject AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_EB_TransNo(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0}-{1}", dr["EB_SubTypeName"].ToString(), dr["EB_SubjectName"].ToString()), dr["EB_TransNo"].ToString()));
        }
    }
    #endregion


}