﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataUnit
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tabcUnit order by cZON_PK";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    //public static DataSet baseDataReader(string ZON_PK)
    //{
    //    DataSet emds = new DataSet();
    //    List<SqlParameter> lSqlParas = new List<SqlParameter>();
    //    lSqlParas.Add(SqlAccess.CreateSqlParameter("@ZON_PK", ZON_PK));
    //    string cmdtxt = "SELECT * FROM tabcUnit WHERE ZON_PK=@ZON_PK";

    //    emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
    //    return emds;
    //}
    public static DataSet DataReader(string title, string type, string planType, string customerType, string sDate, string eDate, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(title, type, planType, customerType, sDate, eDate, lSqlParas);
        string sort = "";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tabcUnit", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string title, string type, string planType, string customerType, string sDate, string eDate)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tabcUnit");
        string where = whereString(title, type, planType, customerType, sDate, eDate, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string title, string type, string planType, string customerType, string sDate, string eDate, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (title.Length > 0)
        {
            sbWhere.Append(" title LIKE '%'+@title+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@name", title));
        }
        if (type.Length > 0)
        {
            sbWhere.Append(" type=@type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@type", type));
        }
        if (planType != "")
        {
            sbWhere.Append(" planType=@planType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@planType", planType));
        }
        if (customerType != "")
        {
            sbWhere.Append(" customerType=@customerType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@customerType", customerType));
        }
        if (sDate.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @sDate, sDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@sDate", sDate));
        }
        if (eDate.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @eDate, eDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@eDate", eDate));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("sDateStr", typeof(string));
        dt.Columns.Add("eDateStr", typeof(string));
        dt.Columns.Add("mailStr", typeof(string));
        dt.Columns.Add("customerTypeStr", typeof(string));
        dt.Columns.Add("typeStr", typeof(string));
        dt.Columns.Add("planTypeStr", typeof(string));
        dt.Columns.Add("status", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["sDateStr"] = DateTime.Parse(dr["sDate"].ToString()).ToString("yyyy/MM/dd");
            dr["eDateStr"] = DateTime.Parse(dr["eDate"].ToString()).ToString("yyyy/MM/dd");
            dr["mailStr"] = string.Format("<a href=\"mailto:{0}\">{0}</a>", dr["mail"].ToString().Trim());
            switch(dr["customerType"].ToString())
            {
                case "1":
                    dr["customerTypeStr"] = "保戶";
                    break;
                case "2":
                    dr["customerTypeStr"] = "準客戶";
                    break;
                case "3":
                    dr["customerTypeStr"] = "兩者皆是";
                    break;
                case "4":
                    dr["customerTypeStr"] = "潛在客戶";
                    break;
            }
            switch (dr["type"].ToString())
            {
                case "1":
                    dr["typeStr"] = "招攬活動";
                    break;
                case "2":
                    dr["typeStr"] = "增員活動";
                    break;
                case "3":
                    dr["typeStr"] = "保戶服務";
                    break;
                case "4":
                    dr["typeStr"] = "客戶開發";
                    break;
                case "5":
                    dr["typeStr"] = "績效管理";
                    break;
                case "6":
                    dr["typeStr"] = "業務活動/訓練";
                    break;
                case "7":
                    dr["typeStr"] = "行政作業";
                    break;
                case "8":
                    dr["typeStr"] = "私人行程";
                    break;
                case "9":
                    dr["typeStr"] = "其他";
                    break;
            }
            switch (dr["planType"].ToString())
            {
                case "1":
                    dr["planTypeStr"] = "工作計畫";
                    break;
                case "2":
                    dr["planTypeStr"] = "(預定客戶數)約訪";
                    break;
                case "3":
                    dr["planTypeStr"] = "(約訪成功客戶數)初次面";
                    break;
                case "4":
                    dr["planTypeStr"] = "需求確認";
                    break;
                case "5":
                    dr["planTypeStr"] = "遞送建議書";
                    break;
                case "6":
                    dr["planTypeStr"] = "促成簽約";
                    break;
                case "7":
                    dr["planTypeStr"] = "遞送保單";
                    break;
                case "8":
                    dr["planTypeStr"] = "轉介紹";
                    break;

            }
            dr["status"] = dr["show"].ToString() == "1" ? "未完成" : "已完成";
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("sDateStr", "行程日期(起)", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("eDateStr", "行程日期(迄)", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("name", "客戶姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("SexName", "性別", 40, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("mailStr", "電子信箱", false, 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("customerTypeStr", "客戶類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("typeStr", "種類", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("planTypeStr", "項目", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("status", "狀態", 100, "Center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "編號", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("title", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("type", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("planType", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("customerType", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("sex", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("old", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("phone", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("cellphone", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("mail", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("address", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("note", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("sDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("eDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("applyDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("updateDate", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, title, type, planType, customerType, name, sex, old, phone, cellphone, mail, address, note sDate, eDate, updateDate FROM tabcUnit where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tabcUnit";
        return emds.Tables[0].Clone();
    }
    #endregion

    public static DataRow dataReader_cZON_PK(string cZON_PK)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("cZON_PK", SqlAccess.CreateSqlParameter("@cZON_PK", cZON_PK));

        string cmdtxt = "SELECT * FROM tabcUnit WHERE cZON_PK=@cZON_PK";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }


    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_cZON_PK(DropDownList DDL, bool all, string allString)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader();
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0}", dr["cZON_PK"].ToString()), dr["cZON_PK"].ToString()));
        }
    }
    public static void DDL_cZON_PK_cZON_NAME(DropDownList DDL, bool all, string allString)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader();
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} {1}", dr["cZON_PK"].ToString(), dr["cZON_NAME"].ToString()), dr["cZON_PK"].ToString()));
        }
    }
    #endregion



}