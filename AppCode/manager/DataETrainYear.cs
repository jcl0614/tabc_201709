﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataETrainYear
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EY_TransNo(string EY_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EY_TransNo", SqlAccess.CreateSqlParameter("@EY_TransNo", EY_TransNo));

        string cmdtxt = "SELECT * FROM tblETrainYear WHERE EY_TransNo=@EY_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_EY_AccID_EY_Year(string EY_AccID, string EY_Year)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EY_AccID", SqlAccess.CreateSqlParameter("@EY_AccID", EY_AccID));
        dParas.Add("EY_Year", SqlAccess.CreateSqlParameter("@EY_Year", EY_Year));

        string cmdtxt = "SELECT * FROM tblETrainYear WHERE EY_AccID=@EY_AccID AND EY_Year=@EY_Year AND ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_my(string EY_Year, string EY_Unit, string EY_AccID, string DM, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_my(EY_Year, EY_Unit, EY_AccID, DM, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblETrainYear", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_my(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_my(string EY_Year, string EY_Unit, string EY_AccID, string DM)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblETrainYear");
        string where = whereString_my(EY_Year, EY_Unit, EY_AccID, DM, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString_my(string EY_Year, string EY_Unit, string EY_AccID, string DM, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EY_Year.Length > 0)
        {
            sbWhere.Append(" EY_Year=@EY_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EY_Year", EY_Year));
        }
        if (EY_Unit.Length > 0)
        {
            sbWhere.Append(" EY_Unit=@EY_Unit AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EY_Unit", EY_Unit));
        }
        if (EY_AccID.Length > 0)
        {
            sbWhere.Append(" (EY_AccID LIKE '%'+@EY_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblETrainYear.EY_AccID) LIKE '%'+@EY_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EY_AccID", EY_AccID));
        }
        if (DM.Length > 0)
        {
            sbWhere.Append(" ((select DM from tabcSales where tabcSales.cSAL_FK=tblETrainYear.EY_AccID)=@DM) AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@DM", DM));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess_my(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EY_Unit_str", typeof(string));
        dt.Columns.Add("EY_AccID_Name", typeof(string));
        dt.Columns.Add("EY_Hours", typeof(string));
        dt.Columns.Add("EY_AttHours", typeof(string));
        dt.Columns.Add("LackHour", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr["EY_Unit"].ToString());
            if (dr_Phrase != null)
                dr["EY_Unit_str"] = dr_Phrase["TypeName"].ToString();
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EY_AccID"].ToString());
            if (dr_sales != null)
            {
                dr["EY_AccID_Name"] = dr_sales["NAME"].ToString();
            }
            int EY_Hours1 = 0, EY_Hours2 = 0, EY_AttHours1 = 0, EY_AttHours2 = 0;
            int.TryParse(dr["EY_Hours1"].ToString(), out EY_Hours1);
            int.TryParse(dr["EY_Hours2"].ToString(), out EY_Hours2);
            int.TryParse(dr["EY_AttHours1"].ToString(), out EY_AttHours1);
            int.TryParse(dr["EY_AttHours2"].ToString(), out EY_AttHours2);
            dr["EY_Hours"] = (EY_Hours1 + EY_Hours2).ToString();
            dr["EY_AttHours"] = (EY_AttHours1 + EY_AttHours2).ToString();
            dr["LackHour"] = (EY_Hours1 + EY_Hours2 - EY_AttHours1 - EY_AttHours2).ToString();
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn_my(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Link, "年度", "EY_Year", "", "Select", "", 50, false));
        mgv.Columns.Add(DataModel.CreateBoundField("EY_Unit_str", "單位", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EY_AccID", "受訓人員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EY_AccID_Name", "受訓人員姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EY_Hours", "應訓時數", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EY_AttHours", "實訓時數", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("LackHour", "尚缺時數", 80, "Center"));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }

    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblETrainYear where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblETrainYear";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region
    public static bool valid_EY(string EY_AccID, string EY_Year)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EY_AccID", SqlAccess.CreateSqlParameter("@EY_AccID", EY_AccID));
        dParas.Add("EY_Year", SqlAccess.CreateSqlParameter("@EY_Year", EY_Year));

        string cmdtxt = "SELECT * FROM tblETrainYear WHERE EY_AccID=@EY_AccID AND EY_Year=@EY_Year AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion


}