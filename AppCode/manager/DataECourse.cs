﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataECourse
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString, string EC_Category, string EC_Mode)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        if (EC_Category != "")
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        if (EC_Mode != "")
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Mode", EC_Mode));
        string cmdtxt = "";
        if (b)
            cmdtxt = string.Format("SELECT * FROM tblECourse WHERE (EC_CName LIKE '%'+@searchString+'%' OR EC_TransNo LIKE '%'+@searchString+'%') {0} {1} AND tblECourse.ModiState<>'D'", EC_Category != "" ? "AND EC_Category=@EC_Category" : "", EC_Mode != "" ? (EC_Mode.IndexOf(",") == -1 ? "AND EC_Mode=@EC_Mode" : string.Format("AND EC_Mode in ({0})", EC_Mode)) : "");
        else
            cmdtxt = string.Format("SELECT * FROM tblECourse WHERE (EC_CName LIKE '%'+@searchString+'%' OR EC_TransNo LIKE '%'+@searchString+'%') {0} {1}", EC_Category != "" ? "AND EC_Category=@EC_Category" : "", EC_Mode != "" ? (EC_Mode.IndexOf(",") == -1 ? "AND EC_Mode=@EC_Mode" : string.Format("AND EC_Mode in ({0})", EC_Mode)) : "");
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_EC_TransNo(string EC_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        string cmdtxt = "SELECT * FROM tblECourse WHERE EC_TransNo=@EC_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_ET_TransNo(string ET_TransNo, string EC_Category)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ET_TransNo", SqlAccess.CreateSqlParameter("@ET_TransNo", ET_TransNo));
        dParas.Add("EC_Category", SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));

        string cmdtxt = "SELECT * FROM tblECourse WHERE ET_TransNo=@ET_TransNo and EC_Category=@EC_Category";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_EC_Year_IsForce(string EC_Year)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_Year", SqlAccess.CreateSqlParameter("@EC_Year", EC_Year));
        dParas.Add("EC_Mode", SqlAccess.CreateSqlParameter("@EC_Mode", "2"));

        string cmdtxt = "SELECT * FROM tblECourse WHERE EC_IsForce='Y' and EC_Year=@EC_Year and EC_Mode=@EC_Mode";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_DepartmentCalendar(string EC_Unit, string Year, string Month)
    {
        DataSet emds = new DataSet();

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        if (EC_Unit != "")
            dParas.Add("EC_Unit", SqlAccess.CreateSqlParameter("@EC_Unit", EC_Unit));
        dParas.Add("Year", SqlAccess.CreateSqlParameter("@Year", Year));
        dParas.Add("Month", SqlAccess.CreateSqlParameter("@Month", Month));

        string cmdtxt = string.Format("SELECT * FROM tblECourse WHERE ModiState<>'D' AND Year(EC_SDate)=@Year AND Month(EC_SDate)=@Month and EC_Mode in (1,3) {0}", EC_Unit != "" ? " AND EC_Unit=@EC_Unit" : "");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        return emds;
    }
    public static DataSet DataReader(string EC_CName, string EC_Type, string EC_Rating, string EC_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EC_CName, EC_Type, EC_Rating, EC_Category, lSqlParas);
        string sort = "tblECourse.Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("tblECourse.*, ETM_count, tblETeacher_ET_TransNo, tblETeacher_ET_UidNo, tblETeacher_E5_TransNo, tblETeacher_E5_UidNo, tblETeacher_ET_TName");

        StringBuilder columns = new StringBuilder();
        columns.Append("tblECourse.*, (select count(EM_TransNo) from tblETm where tblETm.EC_TransNo=tblECourse.EC_TransNo and tblETm.ModiState<>'D') as ETM_count, tblETeacher.ET_TransNo as tblETeacher_ET_TransNo,tblETeacher.ET_UidNo as tblETeacher_ET_UidNo,tblETeacher.E5_TransNo as tblETeacher_E5_TransNo,tblETeacher.E5_UidNo as tblETeacher_E5_UidNo,tblETeacher.ET_TName as tblETeacher_ET_TName ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECourse left join tblETeacher on tblECourse.ET_TransNo=tblETeacher.ET_TransNo and tblECourse.ET_UidNo=tblETeacher.ET_UidNo", "tblECourse", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_P(string EC_CName, string EC_Code, string EC_Summary, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Mode, string EC_Staff, string EC_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_P(EC_CName, EC_Code, EC_Summary, EC_SDate_s, EC_SDate_e, EC_Type, EC_Rating, EC_Mode, EC_Staff, EC_Category, lSqlParas);
        string sort = "tblECourse.EC_SDate,tblECourse.EC_CName";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("tblECourse.*, ETM_count, tblETeacher_ET_TransNo, tblETeacher_ET_UidNo, tblETeacher_E5_TransNo, tblETeacher_E5_UidNo, tblETeacher_ET_TName");

        StringBuilder columns = new StringBuilder();
        columns.Append("tblECourse.*, (select count(EM_TransNo) from tblETm where tblETm.EC_TransNo=tblECourse.EC_TransNo and tblETm.ModiState<>'D') as ETM_count, tblETeacher.ET_TransNo as tblETeacher_ET_TransNo,tblETeacher.ET_UidNo as tblETeacher_ET_UidNo,tblETeacher.E5_TransNo as tblETeacher_E5_TransNo,tblETeacher.E5_UidNo as tblETeacher_E5_UidNo,tblETeacher.ET_TName as tblETeacher_ET_TName ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECourse left join tblETeacher on tblECourse.ET_TransNo=tblETeacher.ET_TransNo and tblECourse.ET_UidNo=tblETeacher.ET_UidNo", "tblECourse", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_clerk(string EC_CName, string ET_Name, string EC_Place, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Category, string EC_Mode, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_clerk(EC_CName, ET_Name, EC_Place, EC_SDate_s, EC_SDate_e, EC_Type, EC_Rating, EC_Category, EC_Mode, lSqlParas);
        string sort = "tblECourse.EC_SDate,tblECourse.EC_CName";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("tblECourse.*, ETM_count, tblETeacher_ET_TransNo, tblETeacher_ET_UidNo, tblETeacher_E5_TransNo, tblETeacher_E5_UidNo, tblETeacher_ET_TName");

        StringBuilder columns = new StringBuilder();
        columns.Append("tblECourse.*, (select count(EM_TransNo) from tblETm where tblETm.EC_TransNo=tblECourse.EC_TransNo and tblETm.ModiState<>'D') as ETM_count, tblETeacher.ET_TransNo as tblETeacher_ET_TransNo,tblETeacher.ET_UidNo as tblETeacher_ET_UidNo,tblETeacher.E5_TransNo as tblETeacher_E5_TransNo,tblETeacher.E5_UidNo as tblETeacher_E5_UidNo,tblETeacher.ET_TName as tblETeacher_ET_TName ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblECourse left join tblETeacher on tblECourse.ET_TransNo=tblETeacher.ET_TransNo and tblECourse.ET_UidNo=tblETeacher.ET_UidNo", "tblECourse", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_hit(string EC_CName, string EL_Hits, string EC_SDate_s, string EC_SDate_e, string EC_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_hit(EC_CName, EL_Hits, EC_SDate_s, EC_SDate_e, EC_Category, lSqlParas);
        string sort = "tblECourse.Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append(" tblECourse.EC_TransNo, tblECourse.EC_CName, EL_Hits");

        StringBuilder columns = new StringBuilder();
        columns.Append(" tblECourse.EC_TransNo, tblECourse.EC_CName, sum(tblEClick.EL_Hits) as EL_Hits ");

        string cmdtxt = MainControls.sqlSelectJoinGroup(Vcolumns.ToString(), columns.ToString(), "tblECourse left Join tblEClick on tblECourse.EC_TransNo=tblEClick.EC_TransNo and tblECourse.EC_UidNo=tblEClick.EC_UidNo", "tblECourse", "tblECourse.EC_TransNo,tblECourse.EC_CName,tblECourse.Server_ModiDate", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_hit(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EC_CName, string EC_Type, string EC_Rating, string EC_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECourse");
        string where = whereString(EC_CName, EC_Type, EC_Rating, EC_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_P(string EC_CName, string EC_Code, string EC_Summary, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Mode, string EC_Staff, string EC_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECourse");
        string where = whereString_P(EC_CName, EC_Code, EC_Summary, EC_SDate_s, EC_SDate_e, EC_Type, EC_Rating, EC_Mode, EC_Staff, EC_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    public static int GetCount_clerk(string EC_CName, string ET_Name, string EC_Place, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Category, string EC_Mode)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECourse");
        string where = whereString_clerk(EC_CName, ET_Name, EC_Place, EC_SDate_s, EC_SDate_e, EC_Type, EC_Rating, EC_Category, EC_Mode, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    public static int GetCount_hit(string EC_CName, string EL_Hits, string EC_SDate_s, string EC_SDate_e, string EC_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblECourse");
        string where = whereString_hit(EC_CName, EL_Hits, EC_SDate_s, EC_SDate_e, EC_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EC_CName, string EC_Type, string EC_Rating, string EC_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" EC_CName LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Type != "")
        {
            sbWhere.Append(" EC_Type=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" EC_Rating=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_Category != "")
        {
            sbWhere.Append(" EC_Category=@EC_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        }

        sbWhere.Append(" tblECourse.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_P(string EC_CName, string EC_Code, string EC_Summary, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Mode, string EC_Staff, string EC_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" EC_CName LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Code.Length > 0)
        {
            sbWhere.Append(" EC_Code LIKE '%'+@EC_Code+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        }
        if (EC_Summary.Length > 0)
        {
            sbWhere.Append(" EC_Summary LIKE '%'+@EC_Summary+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Summary", EC_Summary));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, EC_SDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, EC_SDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (EC_Type != "")
        {
            sbWhere.Append(" EC_Type=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" EC_Rating=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_Mode != "")
        {
            sbWhere.Append(" EC_Mode=@EC_Mode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Mode", EC_Mode));
        }
        if (EC_Staff.Length > 0)
        {
            sbWhere.Append(" EC_Staff LIKE '%'+@EC_Staff+';%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Staff", EC_Staff));
        }
        if (EC_Category != "")
        {
            sbWhere.Append(" EC_Category=@EC_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        }

        sbWhere.Append(" tblECourse.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_clerk(string EC_CName, string ET_Name, string EC_Place, string EC_SDate_s, string EC_SDate_e, string EC_Type, string EC_Rating, string EC_Category, string EC_Mode, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" EC_CName LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (ET_Name.Length > 0)
        {
            sbWhere.Append(" tblETeacher_ET_TName LIKE '%'+@ET_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Name", ET_Name));
        }
        if (EC_Place.Length > 0)
        {
            sbWhere.Append(" EC_Place LIKE '%'+@EC_Place+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Place", EC_Place));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, EC_SDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, EC_SDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (EC_Type != "")
        {
            sbWhere.Append(" EC_Type=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" EC_Rating=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_Category != "")
        {
            sbWhere.Append(" EC_Category=@EC_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        }
        if (EC_Mode != "")
        {
            sbWhere.Append(" EC_Mode=@EC_Mode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Mode", EC_Mode));
        }

        sbWhere.Append("(DATEDIFF(d, getdate(), EC_SDate)>=0 OR EC_SDate is null) AND ");

        sbWhere.Append(" tblECourse.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_hit(string EC_CName, string EL_Hits, string EC_SDate_s, string EC_SDate_e, string EC_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" EC_CName LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EL_Hits.Length > 0)
        {
            sbWhere.Append(" (select sum(tblEClick.EL_Hits) from tblEClick where tblECourse.EC_TransNo=tblEClick.EC_TransNo and tblECourse.EC_UidNo=tblEClick.EC_UidNo) >= @EL_Hits AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EL_Hits", EL_Hits));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, EC_SDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, EC_SDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (EC_Category != "")
        {
            sbWhere.Append(" EC_Category=@EC_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Category", EC_Category));
        }

        sbWhere.Append(" tblECourse.ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("NO", typeof(string));
        dt.Columns.Add("EC_Category_str", typeof(string));
        dt.Columns.Add("EC_SDate_Str", typeof(string));
        dt.Columns.Add("EC_Mode_Str", typeof(string));
        dt.Columns.Add("ETM_link", typeof(string));
        dt.Columns.Add("print_link", typeof(string));
        dt.Columns.Add("link_1", typeof(string));
        dt.Columns.Add("link_2", typeof(string));
        dt.Columns.Add("link_3", typeof(string));
        dt.Columns.Add("link_4", typeof(string));
        dt.Columns.Add("link_5", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        dt.Columns.Add("btn_member_hit", typeof(string));
        dt.Columns.Add("btn_member_finish", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["NO"] = (dt.Rows.IndexOf(dr) + 1).ToString();
            
            
            dr["link_5"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=5&open=1\">{0}</a>", "<img src=\"/images/groupenroll-1.png\" border=\"0\" />", dr["EC_TransNo"].ToString());
            switch(dr["EC_Category"].ToString())
            {
                case "1":
                    dr["EC_Category_str"] = "數位";
                    break;
                case "2":
                    dr["EC_Category_str"] = "實體";
                    break;
            }
            if (dr["EC_Category"].ToString() == "1")
            {
                dr["link_2"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=1&open=1\">{0}</a>", dr["EC_CName"].ToString(), dr["EC_TransNo"].ToString());
                dr["link_3"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=3&open=1\">{0}</a>", "<img src=\"/images/gotoclass.png\" border=\"0\" />", dr["EC_TransNo"].ToString());
            }
            if (dr["EC_Category"].ToString() == "2")
            {
                dr["link_1"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=2&open=1\">{0}</a>", string.Format("{0}<br>{1}~{2}", DateTime.Parse(dr["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr["EC_STime"].ToString(), dr["EC_ETime"].ToString()), dr["EC_TransNo"].ToString());
                dr["link_2"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=2&open=1\">{0}</a>", dr["EC_CName"].ToString(), dr["EC_TransNo"].ToString());
                dr["link_4"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=4&open=1\">{0}</a>", "<img src=\"/images/enroll-1.png\" border=\"0\" />", dr["EC_TransNo"].ToString());
                dr["EC_SDate_Str"] = string.Format("{0}<br>{1}~{2}", DateTime.Parse(dr["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr["EC_STime"].ToString(), dr["EC_ETime"].ToString());
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("130", dr["EC_Mode"].ToString());
                if (dr_Phrase != null)
                {
                    dr["EC_Mode_Str"] = dr_Phrase["TypeName"].ToString();
                }
                dr["print_link"] = string.Format("<a href=\"/manager/ECoursePrint_print?id={0}\" target=\"_blank\">列印</a>", dr["EC_TransNo"].ToString());
            }
            dr["ETM_link"] = string.Format("<a href=\"/manager/ETM?t={0}&ec_transNo={1}&open=1\" target=\"_blank\">{2} 筆</a>", dr["EC_Category"].ToString(), dr["EC_TransNo"].ToString(), dr["ETM_count"].ToString());
        }
    }
    private static void DataProcess_hit(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("btn_member_hit", typeof(string));
        dt.Columns.Add("btn_member_finish", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv, string category)
    {
        if (category == "1")
        {
            mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Code", "課程代碼", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 200, "Left"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Summary", "課程簡介", 300, "Left"));
            mgv.Columns.Add(DataModel.CreateBoundField("ETM_link", "相關教材", false, 80, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        }
        else if (category == "2")
        {
            mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 200, "Left"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Mode_Str", "型態", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 100, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("EC_Summary", "課程簡介", 300, "Left"));
            mgv.Columns.Add(DataModel.CreateBoundField("ETM_link", "相關教材", false, 80, "Center"));
            mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        }

        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
        BoundField bf_ = DataModel.CreateBoundField("tblETeacher_ET_TName", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_ET_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
         bf_ = DataModel.CreateBoundField("tblETeacher_ET_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        
    }
    public static void DataColumn_Print(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Mode_Str", "型態", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Summary", "課程簡介", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("print_link", "簽到表", false, 80, "Center"));
    }
    public static void DataColumn_clerk(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("NO", "NO", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_1", "開課日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_2", "課程名稱", false, 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Category_str", "數位/實體", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Mode_Str", "型態", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("tblETeacher_ET_TName", "授課講師", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Place", "上課地點", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Summary", "課程簡介", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_3", "進入課程", false, 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_4", "個人報名", false, 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_5", "團體報名", false, 50, "Center"));

        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
        BoundField bf_ = DataModel.CreateBoundField("tblETeacher_ET_TName", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_ET_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_ET_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
        bf_ = DataModel.CreateBoundField("tblETeacher_E5_UidNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);

    }
    public static void DataColumn_hit(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 400, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EL_Hits", "點閱次數", 100, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_member_hit", "點閱人員名單", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_member_finish", "完訓人員名單", 150, "Center"));

        BoundField bf_ = DataModel.CreateBoundField("EC_TransNo", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblECourse where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblECourse";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  驗證課程代碼
    public static Boolean valid_EC_Code(string EC_Code)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblECourse WHERE EC_Code=@EC_Code";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region  
    public static DataRow valid_EC(string EC_Code, string EC_CName, string EC_SDate)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_Code", SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        dParas.Add("EC_CName", SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        dParas.Add("EC_SDate", SqlAccess.CreateSqlParameter("@EC_SDate", EC_SDate));

        string cmdtxt = "SELECT * FROM tblECourse WHERE EC_Code=@EC_Code AND EC_CName=@EC_CName AND EC_SDate=@EC_SDate";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count != 0)
            return emds.Tables[0].Rows[0];
        else
            return null;
    }
    public static DataRow valid_EC2(string EC_Year, string EC_TrainType, string EC_Type, string EC_Mode, string EC_Kind, string EC_CName, string EC_SDate)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_Year", SqlAccess.CreateSqlParameter("@EC_Year", EC_Year));
        dParas.Add("EC_TrainType", SqlAccess.CreateSqlParameter("@EC_TrainType", EC_TrainType));
        dParas.Add("EC_Type", SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        dParas.Add("EC_Mode", SqlAccess.CreateSqlParameter("@EC_Mode", EC_Mode));
        dParas.Add("EC_Kind", SqlAccess.CreateSqlParameter("@EC_Kind", EC_Kind));
        dParas.Add("EC_CName", SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        dParas.Add("EC_SDate", SqlAccess.CreateSqlParameter("@EC_SDate", EC_SDate));

        string cmdtxt = "SELECT * FROM tblECourse WHERE YEAR(EC_SDate)=@EC_Year AND EC_TrainType=@EC_TrainType AND EC_Type=@EC_Type AND EC_Mode=@EC_Mode AND EC_Kind=@EC_Kind AND EC_CName=@EC_CName AND EC_SDate=@EC_SDate";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count != 0)
            return emds.Tables[0].Rows[0];
        else
            return null;
    }
    #endregion

    public static DataSet DataReader_hitEC_details(string EC_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        StringBuilder cmdtxt = new StringBuilder();


        cmdtxt.Append("Select ROW_NUMBER() OVER(ORDER BY tabcSales.cSAL_FK) AS SerialNo, ");
        cmdtxt.Append("tblECourse.EC_TransNo, ");
        cmdtxt.Append("tabcSales.cSAL_FK AccID, tabcSales.Name, tabcSales.Title,  ");
        cmdtxt.Append("(select Max(tblExam.EX_Score) from tblExam where tblExam.EC_TransNo = tblECourse.EC_TransNo and tblExam.EC_UidNo = tblECourse.EC_UidNo and tblExam.AccID=tabcSales.cSAL_FK) as Score,  ");
        cmdtxt.Append("(select top 1 tblExam.EX_Date from tblExam where tblExam.EC_TransNo = tblECourse.EC_TransNo and tblExam.EC_UidNo = tblECourse.EC_UidNo and tblExam.AccID=tabcSales.cSAL_FK order by tblExam.EX_Date desc) EX_Date,  ");
        cmdtxt.Append("tabcSales.cZON_PK, tabcSales.cZON_TYPE, tabcSales.cZON_NAME, tabcSales.cZON_PK + tabcSales.cZON_NAME cZON_PK_NAME, tabcSales.DM_Name, tabcSales.UM_Name, tabcSales.Edu_NAME,  ");
        cmdtxt.Append("(select sum(tblEClick.EL_Hits) from tblEClick where tblEClick.AccID = tabcSales.cSAL_FK and tblECourse.EC_TransNo = tblEClick.EC_TransNo and tblECourse.EC_UidNo = tblEClick.EC_UidNo) EL_Hits,  ");
        cmdtxt.Append("(select  max(tblEClick.ModiDate) from tblEClick where tblEClick.AccID = tabcSales.cSAL_FK and tblECourse.EC_TransNo = tblEClick.EC_TransNo and tblECourse.EC_UidNo = tblEClick.EC_UidNo) EL_HitsDate ");
        cmdtxt.Append("From  tblECourse ");
        cmdtxt.Append("left join tabcSales_new as tabcSales on tabcSales.cSAL_FK = tabcSales.cSAL_FK  ");
        //cmdtxt.Append("left join ");
        //cmdtxt.Append("( ");
        //cmdtxt.Append("SELECT ");
        //cmdtxt.Append("tblPerson.PS_Name, tblPerson.PS_Title, tblPerson.PS_EMAIL, s2.Name DM_Name, s3.Name UM_Name, s4.Name cSAL_ID_S_Name, ");
        //cmdtxt.Append("tabcSales.Title, tblPhrase.TypeCode, tabcSales.cSAL_FK, tabcSales.cSAL_ID_S, tabcSales.NAME, tabcSales.Zone, tblPhrase.TypeNo, tblPhrase.TypeName, tabcSales.Startdate, tabcSales.Gender, ");
        //cmdtxt.Append("tabcSales.Birth, tabcSales.Email, tabcSales.Status, tabcSales.Type, tabcSales.Org_Type, tabcSales.DM, tabcSales.Leave_Date, ");
        //cmdtxt.Append("tabcSales.MOBILE, tabcSales.cSAL_FK_H, tabcSales.cSAL_FK_I, tabcSales.Promotion_Date, tabcSales.CHANGE_DATE, tabcSales.Education, ");
        //cmdtxt.Append("tabcEducation.cEDU_DESC, tabcSales.OAdress, tabcSales.NAdress, tabcUnit.cZON_PK, tabcUnit.cZON_NAME, tabcUnit.cZON_TYPE, tabcUnit.iORG_TYPE, tabcUnit.cORG_DESC, ");
        //cmdtxt.Append("tabcSales_Regedit.REG_END_L, tabcSales_Regedit.REG_DATE_I, tabcSales_Regedit.REG_DATE_F, tabcSales_Regedit.REG_END_P, tabcSales_Regedit.REG_FINSH_L ");
        //cmdtxt.Append("FROM tabcSales ");
        //cmdtxt.Append("left Join tblPerson on tabcSales.cSAL_FK= tblPerson.AccID ");
        //cmdtxt.Append("left Join tabcSales s2 on tabcSales.DM= s2.cSAL_FK ");
        //cmdtxt.Append("left Join tabcSales s3 on tabcSales.UM= s3.cSAL_FK ");
        //cmdtxt.Append("left Join tabcSales_EduRelation ON tabcSales.cSAL_FK= tabcSales_EduRelation.cSAL_FK ");
        //cmdtxt.Append("left Join tabcSales s4 on tabcSales_EduRelation.[1st]= s4.cSAL_ID_S ");
        //cmdtxt.Append("left Join tabcUnit on tabcSales.Zone = tabcUnit.cZON_PK ");
        //cmdtxt.Append("left Join tblPhrase ON tabcSales.Title = tblPhrase.TypeSubCode and tblPhrase.TypeCode = 'E010' and tabcSales.Status in (0, 6, 7) ");
        //cmdtxt.Append("left Join tabcEducation ON tabcSales.Education = tabcEducation.iEDU_PK ");
        //cmdtxt.Append("left Join tabcSales_Regedit on tabcSales.cSAL_FK = tabcSales_Regedit.cSAL_FK ");
        //cmdtxt.Append(") tabcSales on tabcSales.cSAL_FK = tabcSales.cSAL_FK ");
        cmdtxt.Append("where tblECourse.EC_Category = 1 and tblECourse.ModiState <> 'D' and tblECourse.EC_TransNo = @EC_TransNo ");
        cmdtxt.Append("Group by tabcSales.cSAL_FK, tabcSales.Name, tabcSales.Title, tabcSales.cZON_PK, tabcSales.cZON_TYPE, tabcSales.cZON_NAME, tabcSales.DM_Name, tabcSales.UM_Name, tabcSales.Edu_NAME, tblECourse.EC_TransNo, tblECourse.EC_UidNo ");
        cmdtxt.Append("order by tabcSales.cSAL_FK ");

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), dParas.Values.ToArray());
        return emds;


    }

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_EC_TransNo(DropDownList DDL, bool all, string allString, string key, string EC_Category, string EC_Mode)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key, EC_Category, EC_Mode);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} ({1})【{2}】", dr["EC_CName"].ToString(), dr["EC_Code"].ToString(), dr["EC_Category"].ToString() == "1" ? "數位課程" : "實體課程"), dr["EC_TransNo"].ToString()));
        }
    }
    #endregion



}