﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;
using System.Xml;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.IO;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataInsWork_PolicyRecord
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet DataReader(
        string P_iINS_TYPE, //保單類別
        string P_cINS_MAN_A, //要保人
        string P_cINS_MAN_B, //被保人
        string P_cSAL_ID_S, //轄下業務員
        string P_cRES_STATUS, //照會狀態
        string P_cSAL_FK, //AccID
        string field,
        string keyword,
        int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        DataTable dt = getAPI_Policy(
             P_iINS_TYPE,
             P_cINS_MAN_A,
             P_cINS_MAN_B,
             P_cSAL_ID_S,
             P_cRES_STATUS,
             P_cSAL_FK,
             field,
             keyword);
        if(dt.Rows.Count != 0)
            dt = dt.AsEnumerable().OrderByDescending(x => x.Field<string>("照會日期")).Skip(startRowIndex).Take(maximumRows).CopyToDataTable();
        emds.Tables.Add(dt);
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(
        string P_iINS_TYPE, //保單類別
        string P_cINS_MAN_A, //要保人
        string P_cINS_MAN_B, //被保人
        string P_cSAL_ID_S, //轄下業務員
        string P_cRES_STATUS, //照會狀態
        string P_cSAL_FK, //AccID
        string field,
        string keyword
        )
    {
        DataTable dt = getAPI_Policy(
            P_iINS_TYPE,
             P_cINS_MAN_A,
             P_cINS_MAN_B,
             P_cSAL_ID_S,
             P_cRES_STATUS,
             P_cSAL_FK,
             field,
             keyword
            );

        return dt.Rows.Count;
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];

        foreach (DataRow dr in dt.Rows)
        {

        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("保單號碼", "保單號碼", 150, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("保單類別", "保單類別", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("照會代號", "照會代號", 80, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("照會類別", "照會類別", 150, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("要保人", "要保人", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("被保人", "被保人", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("保險公司", "保險公司", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("業務員", "業務員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("照會日期", "照會日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("應回覆日期", "應回覆日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("照會內容", "照會內容", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("狀態", "狀態", 80, "Center"));
    }
    #endregion

    /// <summary>
    /// 智宇 - TM_查詢_0003_業查_保單查詢
    /// </summary>
    /// <param name="P_iINS_TYPE">保單類別</param>
    /// <param name="AccID">業務員編號</param>
    /// <returns>

    /// </returns>
    private static DataTable getAPI_Policy(
        string P_iINS_TYPE, //保單類別
        string P_cINS_MAN_A, //要保人
        string P_cINS_MAN_B, //被保人
        string P_cSAL_ID_S, //轄下業務員
        string P_cRES_STATUS, //照會狀態
        string P_cSAL_FK, //AccID
        string field,
        string keyword
        )
    {
        DataTable dtDisPlay = new DataTable();
        string setSearch = " <BIS Request=\"SWP(N)\">" +
                            "  <Requests Name=\"TM_查詢_0005_業查_照會記錄\" Text=\"Request\" RB=\"RB(N)\">" +
                            "   <Request Name=\"TM_查詢_0005_業查_照會記錄\" Program=\"TM_005_Request\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                            "    <Parameters>" +
                            "     <Parameter Name=\"@P_iINS_TYPE\"   Value=\"" + P_iINS_TYPE + "\"/>" +//'{COB_保單類別}'
                            "     <Parameter Name=\"@P_cINS_MAN_A\"  Value=\"'" + P_cINS_MAN_A + "'\"/>" +//'{TXI_要保人}'
                            "     <Parameter Name=\"@P_cINS_MAN_B\"  Value=\"'" + P_cINS_MAN_B + "'\"/>" +//'{TXI_被保人}'
                            "     <Parameter Name=\"@P_cSAL_ID_S\"   Value=\"'" + P_cSAL_ID_S + "'\"/>" +//'{COB_轄下業務員}'
                            "     <Parameter Name=\"@P_cRES_STATUS\" Value=\"'" + P_cRES_STATUS + "'\"/>" +//'{RBT_01}'
                            "     <Parameter Name=\"@P_cSAL_FK\"     Value=\"'" + P_cSAL_FK + "'\"/>" +//'{USR_PK}'
                            "    </Parameters>" +
                            "    <ReturnCols/>" +
                            "   </Request>" +
                            "  </Requests>" +
                            " </BIS>";
        string err = "";
        string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
        if (err.Length == 0)
        {
            dtDisPlay = XMLtoDataTable(ret, "BIS/Responds/Respond/N");
            if (dtDisPlay.Rows.Count != 0 && field != "" && keyword != "")
            {
                DataView dvDisPlay = dtDisPlay.AsDataView();
                dvDisPlay.RowFilter = field + " like '%" + keyword + "%'";
                dtDisPlay = dvDisPlay.ToTable();
            }
        }
        return dtDisPlay;
    }

    private static string WebClientPost(string url, string postData, string encodeType, out string err)
    {
        try
        {
            Uri uri = new Uri(url);
            if (uri.Scheme == "https" || uri.Scheme == "http")
            {
                ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = WebRequestMethods.Http.Post;

            string received = "";
            byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
            StreamReader reader = null;
            request.ContentLength = bs.Length;
            request.ContentType = "application/x-www-form-urlencoded";
            using (Stream reqStream = request.GetRequestStream())
            {
                reqStream.Write(bs, 0, bs.Length);
            }
            using (WebResponse wr = request.GetResponse())
            {
                reader = new StreamReader(wr.GetResponseStream());
                received = reader.ReadToEnd();
                err = string.Empty;
                return received;
            }


        }
        catch (Exception ex)
        {
            err = ex.Message;
            return string.Empty;
        }
    }
    private class TrustAllCertificatePolicy : ICertificatePolicy
    {
        public void New() { }
        public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }
    private static DataTable XMLtoDataTable(string xml, string SelectNodes)
    {
        DataTable dtDisPlay = new DataTable();
        try
        {
            #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(
                xml
                .Replace("&", HttpUtility.HtmlEncode("&"))
                .Replace("\u0002", "")
                );
            XmlNodeList NodeList = doc.SelectNodes(SelectNodes);
            foreach (XmlNode COB in NodeList)
            {
                if (dtDisPlay.Columns.Count == 0)
                {
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        dtDisPlay.Columns.Add(Attr.Name.ToString());
                    }
                }

                DataRow drnew = dtDisPlay.NewRow();
                foreach (XmlAttribute Attr in COB.Attributes)
                {
                    if (dtDisPlay.Columns.Contains(Attr.Name.ToString()))
                    {
                        drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value;
                    }
                }
                drnew.EndEdit();
                dtDisPlay.Rows.Add(drnew);
            }
            #endregion
        }
        catch (Exception ex)
        {
            string s = ex.Message;
        }
        return dtDisPlay;
    }

}