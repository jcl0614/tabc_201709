﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataCustomer
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString, string CU_IsShow)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        if(CU_IsShow != "")
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_IsShow", CU_IsShow));
        string cmdtxt = "";
        if (b)
            cmdtxt = string.Format("SELECT * FROM tblCustomer WHERE (CU_CustName LIKE '%'+@searchString+'%' OR CU_TransNo=@searchString) AND ModiState<>'D'", CU_IsShow != "" ? " AND CU_IsShow=@CU_IsShow" : "");
        else
            cmdtxt = "SELECT * FROM tblCustomer";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_CU_TransNo(string CU_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_TransNo", SqlAccess.CreateSqlParameter("@CU_TransNo", CU_TransNo));

        string cmdtxt = "SELECT * FROM tblCustomer WHERE CU_TransNo=@CU_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_CU_ID_CU_CustName(string CU_ID, string CU_CustName, string CU_IsShow)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("CU_ID", SqlAccess.CreateSqlParameter("@CU_ID", CU_ID));
        dParas.Add("CU_CustName", SqlAccess.CreateSqlParameter("@CU_CustName", CU_CustName));
        dParas.Add("CU_IsShow", SqlAccess.CreateSqlParameter("@CU_IsShow", CU_IsShow));

        string cmdtxt = "SELECT * FROM tblCustomer WHERE CU_ID=@CU_ID AND CU_CustName=@CU_CustName AND CU_IsShow=@CU_IsShow";
        if(CU_ID == "")
            cmdtxt = "SELECT * FROM tblCustomer WHERE (CU_ID=@CU_ID or CU_ID is null) AND CU_CustName=@CU_CustName AND CU_IsShow=@CU_IsShow";
        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(
        string CU_CustName, 
        string CU_ID, 
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s, 
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID,
        int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(
             CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             PO_ComSName,
             PO_PolNo,
             PO_AccureDate_s,
             PO_AccureDate_e,
             PO_Polmmemo,
             AccID,
            lSqlParas);
        string sort = "CU_CustName";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");
        Vcolumns.Append(", PO_count ");
        Vcolumns.Append(", IP_count ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");
        columns.Append(", (select count(*) from tblCustPolicy where tblCustPolicy.CU_TransNo=tblCustomer.CU_TransNo) AS PO_count ");
        columns.Append(", (select count(*) from tblCustIndustPolm where tblCustIndustPolm.CU_TransNo=tblCustomer.CU_TransNo) AS IP_count ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblCustomer", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s,
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblCustomer");
        string where = whereString(CU_CustName,
             CU_ID,
             CU_Sex,
             CU_Age,
             CU_Marry,
             CU_CustType,
             CU_CustSource,
             CU_ContactMode,
             CU_IsShow,
             Mobile,
             Address,
             PO_ComSName,
             PO_PolNo,
             PO_AccureDate_s,
             PO_AccureDate_e,
             PO_Polmmemo,
             AccID,
             lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(
        string CU_CustName,
        string CU_ID,
        string CU_Sex,
        string CU_Age,
        string CU_Marry,
        string CU_CustType,
        string CU_CustSource,
        string CU_ContactMode,
        string CU_IsShow,
        string Mobile,
        string Address,
        string PO_ComSName,
        string PO_PolNo,
        string PO_AccureDate_s,
        string PO_AccureDate_e,
        string PO_Polmmemo,
        string AccID,
        List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (CU_CustName.Length > 0)
        {
            sbWhere.Append(" CU_CustName LIKE '%'+@CU_CustName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_CustName", CU_CustName));
        }
        if (CU_ID.Length > 0)
        {
            sbWhere.Append(" CU_ID LIKE '%'+@CU_ID+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_ID", CU_ID));
        }
        if (CU_Sex.Length > 0)
        {
            sbWhere.Append(" CU_Sex=@CU_Sex AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_Sex", CU_Sex));
        }
        if (CU_Age.Length > 0)
        {
            sbWhere.Append(" CU_Age=@CU_Age AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_Age", CU_Age));
        }
        if (CU_Marry.Length > 0)
        {
            sbWhere.Append(" CU_Marry=@CU_Marry AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_Marry", CU_Marry));
        }
        if (CU_CustType.Length > 0)
        {
            sbWhere.Append(" CU_CustType=@CU_CustType AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_CustType", CU_CustType));
        }
        if (CU_CustSource.Length > 0)
        {
            sbWhere.Append(" CU_CustSource=@CU_CustSource AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_CustSource", CU_CustSource));
        }
        if (CU_ContactMode.Length > 0)
        {
            sbWhere.Append(" CU_ContactMode=@CU_ContactMode AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_ContactMode", CU_ContactMode));
        }
        if (CU_IsShow.Length > 0)
        {
            sbWhere.Append(" CU_IsShow=@CU_IsShow AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@CU_IsShow", CU_IsShow));
        }
        if (Mobile.Length > 0)
        {
            
        }
        if (Address.Length > 0)
        {
            
        }
        if (PO_ComSName.Length > 0)
        {

        }
        if (PO_PolNo.Length > 0)
        {

        }
        if (PO_AccureDate_s.Length > 0)
        {

        }
        if (PO_AccureDate_e.Length > 0)
        {

        }
        if (PO_Polmmemo.Length > 0)
        {
            
        }
        sbWhere.Append(" AccID=@AccID AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@AccID", AccID));

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("btn_mail", typeof(string));
        dt.Columns.Add("btn_addSchedule", typeof(string));
        dt.Columns.Add("currentPlane", typeof(string));
        dt.Columns.Add("notConnDays", typeof(string));
        dt.Columns.Add("link_1", typeof(string));
        dt.Columns.Add("link_2", typeof(string));
        dt.Columns.Add("link_3", typeof(string));
        dt.Columns.Add("link_4", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["link_1"] = string.Format("<a href=\"/manager/CustPolicy?id={0}&open=1\" target=\"_blank\">{1} 筆</a>", dr["CU_TransNo"].ToString(), dr["PO_count"].ToString());
            dr["link_2"] = string.Format("<a href=\"/manager/CustPolicy_I?id={0}&open=1\" target=\"_blank\">{1} 筆</a>", dr["CU_TransNo"].ToString(), dr["IP_count"].ToString());
            dr["link_3"] = string.Format("<a href=\"javascript:void(0)\" onclick=\"showDetail('客戶：{0}','{1}');\">檢視</a>", dr["CU_CustName"].ToString(), string.Format("/manager/ProtectAnalsis?id={0}", dr["CU_TransNo"].ToString()));
            dr["link_4"] = string.Format("<a href=\"javascript:void(0)\" onclick=\"showDetail('客戶：{0}','{1}');\">列印</a>", dr["CU_CustName"].ToString(), string.Format("/manager/PolicyPrint?id={0}", dr["CU_TransNo"].ToString()));
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Link, "客戶名稱/姓名", "CU_CustName", "", "Select", "", 150, false, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("CU_CustType", "客戶類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_1", "壽險保單", false, 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_2", "產險保單", false, 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_3", "分析保障", false, 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_4", "保單列印", false, 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_mail", "發送EMAIL", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("btn_addSchedule", "新增行程", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("currentPlane", "最近行程項目", 120, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("notConnDays", "未聯繫(天)", 80, "Right"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            

            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    public static RServiceProvider Append(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    public static RServiceProvider Update(string emNo, string progId, DataSet ds, bool emDaily)
    {
        //員工維護系統資料記錄
        if (emDaily)
            DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblCustomer where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblCustomer";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_TransNo(DropDownList DDL, bool all, string allString, string key, string CU_IsShow)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key, CU_IsShow);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(dr["CU_CustName"].ToString(), dr["CU_TransNo"].ToString()));
        }
    }
    #endregion


}