﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEAttend
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_ED_TransNo(string ED_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ED_TransNo", SqlAccess.CreateSqlParameter("@ED_TransNo", ED_TransNo));

        string cmdtxt = "SELECT * FROM tblEAttend WHERE ED_TransNo=@ED_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataRow DataReader_ED_AccID_ED_SignDate(string ED_AccID, string ED_Mode, string ED_SignDate)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ED_AccID", SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));
        dParas.Add("ED_Mode", SqlAccess.CreateSqlParameter("@ED_Mode", ED_Mode));
        dParas.Add("ED_SignDate", SqlAccess.CreateSqlParameter("@ED_SignDate", ED_SignDate));

        string cmdtxt = "SELECT * FROM tblEAttend WHERE ED_AccID=@ED_AccID and ED_Mode=@ED_Mode and DATEDIFF(d, @ED_SignDate, ED_SignDate)=0 AND ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_EC_TransNo(string EC_TransNo)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));

        string cmdtxt = "SELECT * FROM tblEAttend WHERE EC_TransNo=@EC_TransNo";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_finish(string finalDay)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("finalDay", SqlAccess.CreateSqlParameter("@finalDay", finalDay));

        string cmdtxt = "SELECT *, (select EC_TrainType from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) EC_TrainType " +
            " , ISNULL((case when ISNULL((select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo), 0)=0 then 0 else  (case when ED_AttHours>=(select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) then (select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) else ED_AttHours end) end), 0) EC_PassHours " +
            " FROM tblEAttend WHERE Year(ED_SignDate)=Year(@finalDay) and DATEDIFF(d, ED_SignDate, @finalDay)>=0 and ED_Type=1";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataSet DataReader_finish( string finalDay, string ED_AccID)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("finalDay", SqlAccess.CreateSqlParameter("@finalDay", finalDay));
        dParas.Add("ED_AccID", SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));

        string cmdtxt = "SELECT *, (select EC_TrainType from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) EC_TrainType " +
            " , ISNULL((case when ISNULL((select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo), 0)=0 then 0 else  (case when ED_AttHours>=(select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) then (select EC_PassHours from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) else ED_AttHours end) end), 0) EC_PassHours " +
            " FROM tblEAttend WHERE ED_AccID=@ED_AccID and Year(ED_SignDate)=Year(@finalDay) and DATEDIFF(d, ED_SignDate, @finalDay)>=0 and ED_Type=1";

        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return emds;
    }
    public static DataRow DataReader_EC_TransNo_ED_AccID(string EC_TransNo, string ED_AccID)
    {
        DataRow dr = null;
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));
        dParas.Add("ED_AccID", SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));
        string cmdtxt = "SELECT * FROM tblEAttend WHERE EC_TransNo=@EC_TransNo AND ED_AccID=@ED_AccID";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e, string ED_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EC_CName, EC_Code, PS_NAME, EP_Bank, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        string sort = "(select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttend", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_AttHours(string EC_CName, string EC_Code, string PS_NAME, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_AttHours(EC_CName, EC_Code, PS_NAME, ED_Type, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        string sort = "(select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttend", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_AttBonus(string ED_Year, string ED_Month, string EC_CName, string ED_AccID, string ED_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_AttBonus(ED_Year, ED_Month, EC_CName, ED_AccID, ED_Category, lSqlParas);
        string sort = "(select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttend", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    public static DataSet DataReader_my(string EC_Type, string EC_Rating, string EC_CName, string EC_Place, string ED_AccID, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString_my(EC_Type, EC_Rating, EC_CName, EC_Place, ED_AccID, ED_Type, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        string sort = "(select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEAttend", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess_my(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e, string ED_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttend");
        string where = whereString(EC_CName, EC_Code, PS_NAME, EP_Bank, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_AttHours(string EC_CName, string EC_Code, string PS_NAME, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttend");
        string where = whereString_AttHours(EC_CName, EC_Code, PS_NAME, ED_Type, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_AttBonus(string ED_Year, string ED_Month, string EC_CName, string ED_AccID, string ED_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttend");
        string where = whereString_AttBonus(ED_Year, ED_Month, EC_CName, ED_AccID, ED_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount_my(string EC_Type, string EC_Rating, string EC_CName, string EC_Place, string ED_AccID, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEAttend");
        string where = whereString_my(EC_Type, EC_Rating, EC_CName, EC_Place, ED_AccID, ED_Type, EC_SDate_s, EC_SDate_e, ED_Category, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EC_CName, string EC_Code, string PS_NAME, string EP_Bank, string EC_SDate_s, string EC_SDate_e, string ED_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" (select EC_CName from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Code.Length > 0)
        {
            sbWhere.Append(" (select EC_Code from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_Code+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblEAttend.ED_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (EP_Bank.Length > 0)
        {
            sbWhere.Append(" (select EP_Bank FROM tblEPayment where tblEPayment.ED_TransNo=tblEAttend.ED_TransNo) LIKE '%'+@EP_Bank+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EP_Bank", EP_Bank));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (ED_Category != "")
        {
            sbWhere.Append(" ED_Category=@ED_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Category", ED_Category));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_AttHours(string EC_CName, string EC_Code, string PS_NAME, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" (select EC_CName from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Code.Length > 0)
        {
            sbWhere.Append(" (select EC_Code from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_Code+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Code", EC_Code));
        }
        if (PS_NAME.Length > 0)
        {
            sbWhere.Append(" (select tblPerson.PS_NAME from tblPerson where tblPerson.AccID=tblEAttend.ED_AccID) LIKE '%'+@PS_NAME+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@PS_NAME", PS_NAME));
        }
        if (ED_Type.Length > 0)
        {
            sbWhere.Append(" ED_Type=@ED_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Type", ED_Type));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (ED_Category != "")
        {
            sbWhere.Append(" ED_Category=@ED_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Category", ED_Category));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_AttBonus(string ED_Year, string ED_Month, string EC_CName, string ED_AccID, string ED_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (ED_Year.Length > 0)
        {
            sbWhere.Append(" YEAR(ED_AttendDate)=@ED_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Year", ED_Year));
        }
        if (ED_Month.Length > 0)
        {
            sbWhere.Append(" MONTH(ED_AttendDate)=@ED_Month AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Month", ED_Month));
        }
        if (EC_CName.Length > 0)
        {
            sbWhere.Append(" (select EC_CName from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_CName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (ED_AccID.Length > 0)
        {
            sbWhere.Append(" (ED_AccID LIKE '%'+@ED_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblEAttend.ED_AccID) LIKE '%'+@ED_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));
        }
        if (ED_Category != "")
        {
            sbWhere.Append(" ED_Category=@ED_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Category", ED_Category));
        }
        sbWhere.Append(" (select EC_Mode from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) in (3,4) AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    public static String whereString_my(string EC_Type, string EC_Rating, string EC_CName, string EC_Place, string ED_AccID, string ED_Type, string EC_SDate_s, string EC_SDate_e, string ED_Category, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EC_Type != "")
        {
            sbWhere.Append(" (select EC_Type from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo)=@EC_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Type", EC_Type));
        }
        if (EC_Rating != "")
        {
            sbWhere.Append(" (select EC_Rating from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo)=@EC_Rating AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Rating", EC_Rating));
        }
        if (EC_CName.Length > 0)
        {
            sbWhere.AppendFormat(" (select count(*) from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo and ({0} or {1} or {2}))>0 AND "
                , " EC_Code LIKE '%'+@EC_CName+'%'"
                , " EC_CName LIKE '%'+@EC_CName+'%'"
                , " (select ET_TName from tblETeacher where tblETeacher.ET_TransNo=tblECourse.ET_TransNo) LIKE '%'+@EC_CName+'%'");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_CName", EC_CName));
        }
        if (EC_Place.Length > 0)
        {
            sbWhere.Append(" (select EC_Place from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo) LIKE '%'+@EC_Place+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_Place", EC_Place));
        }
        if (ED_AccID.Length > 0)
        {
            sbWhere.AppendFormat(" ED_AccID in ({0}) AND ", ED_AccID);
        }
        if (ED_Type.Length > 0)
        {
            sbWhere.Append(" ED_Type=@ED_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Type", ED_Type));
        }
        if (EC_SDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_s, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_s", EC_SDate_s));
        }
        if (EC_SDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EC_SDate_e, (select EC_SDate from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo))<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EC_SDate_e", EC_SDate_e));
        }
        if (ED_Category != "")
        {
            sbWhere.Append(" (select EC_Category from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo)=@ED_Category AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Category", ED_Category));
        }

        sbWhere.Append(" (select count(*) from tblECourse where tblECourse.EC_TransNo=tblEAttend.EC_TransNo)>0 AND ");

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EC_SDate_Str", typeof(string));
        dt.Columns.Add("EC_SDate", typeof(string));
        dt.Columns.Add("EC_CName", typeof(string));
        dt.Columns.Add("EC_YearMonth", typeof(string));
        dt.Columns.Add("ED_YearMonth", typeof(string));
        dt.Columns.Add("ED_TypeName", typeof(string));
        dt.Columns.Add("ED_AttHours_str", typeof(string));
        dt.Columns.Add("ED_EatHabits_str", typeof(string));
        dt.Columns.Add("ED_Transport_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if (dr["ED_AttendDate"].ToString() != "")
                dr["ED_YearMonth"] = string.Format("{0}/{1}", DateTime.Parse(dr["ED_AttendDate"].ToString()).Year.ToString(), DateTime.Parse(dr["ED_AttendDate"].ToString()).Month.ToString("00"));
            DataRow dr_ecourse = DataECourse.DataReader_EC_TransNo(dr["EC_TransNo"].ToString());
            if (dr_ecourse != null)
            {
                if (dr_ecourse["EC_Category"].ToString() == "2")
                {
                    dr["EC_CName"] = dr_ecourse["EC_CName"].ToString();
                    dr["EC_SDate"] = dr_ecourse["EC_SDate"].ToString();
                    dr["EC_YearMonth"] = string.Format("{0}/{1}", DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).Year.ToString(), DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).Month.ToString("00"));
                    dr["EC_SDate_Str"] = string.Format("{0}&nbsp;&nbsp;{1}~{2}", DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_ecourse["EC_STime"].ToString(), dr_ecourse["EC_ETime"].ToString());
                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("131", dr["ED_Type"].ToString());
                    if (dr_Phrase != null)
                    {
                        dr["ED_TypeName"] = dr_Phrase["TypeName"].ToString();
                    }
                    dr["ED_AttHours_str"] = string.Format("{0} 小時", dr["ED_AttHours"].ToString());
                    dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("124", dr["ED_EatHabits"].ToString());
                    if (dr_Phrase != null)
                    {
                        dr["ED_EatHabits_str"] = dr_Phrase["TypeName"].ToString();
                    }
                    dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("125", dr["ED_Transport"].ToString());
                    if (dr_Phrase != null)
                    {
                        dr["ED_Transport_str"] = dr_Phrase["TypeName"].ToString();
                    }
                    if (dr_ecourse["EC_Mode"].ToString() == "4")
                    {

                    }
                }
            }
        }
    }
    private static void DataProcess_my(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("NO", typeof(string));
        dt.Columns.Add("link_1", typeof(string));
        dt.Columns.Add("link_2", typeof(string));
        dt.Columns.Add("link_3", typeof(string));
        dt.Columns.Add("EC_Category_str", typeof(string));
        dt.Columns.Add("EC_Type", typeof(string));
        dt.Columns.Add("EC_Rating", typeof(string));
        dt.Columns.Add("EC_Place", typeof(string));
        dt.Columns.Add("ET_TName", typeof(string));
        dt.Columns.Add("ED_TypeName", typeof(string));
        dt.Columns.Add("EX_Score", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["NO"] = dt.Rows.IndexOf(dr) + 1;
            DataRow dr_ecourse = DataECourse.DataReader_EC_TransNo(dr["EC_TransNo"].ToString());
            if (dr_ecourse != null)
            {
                dr["EC_Type"] = dr_ecourse["EC_Type"].ToString();
                dr["EC_Rating"] = dr_ecourse["EC_Rating"].ToString();
                dr["EC_Place"] = dr_ecourse["EC_Place"].ToString();
                switch (dr_ecourse["EC_Category"].ToString())
                {
                    case "1":
                        dr["EC_Category_str"] = "數位";
                        break;
                    case "2":
                        dr["EC_Category_str"] = "實體";
                        break;
                }
                DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr_ecourse["ET_TransNo"].ToString());
                if (dr_ET != null)
                {
                    dr["ET_TName"] = dr_ET["ET_TName"].ToString();
                }
                if (dr_ecourse["EC_Category"].ToString() == "1")
                {
                    dr["link_2"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=1&open=1\" target=\"_blank\">{0}</a>", dr_ecourse["EC_CName"].ToString(), dr_ecourse["EC_TransNo"].ToString());
                    dr["link_3"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=3&open=1\" target=\"_blank\">{0}</a>", "<img src=\"/images/gotoclass.png\" border=\"0\" />", dr_ecourse["EC_TransNo"].ToString());
                }
                if (dr_ecourse["EC_Category"].ToString() == "2")
                {
                    dr["link_1"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=2&open=1\" target=\"_blank\">{0}</a>", string.Format("{0}<br>{1}~{2}", DateTime.Parse(dr_ecourse["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_ecourse["EC_STime"].ToString(), dr_ecourse["EC_ETime"].ToString()), dr_ecourse["EC_TransNo"].ToString());
                    dr["link_2"] = string.Format("<a href=\"/manager/ClerkECourse?id={1}&c=2&open=1\" target=\"_blank\">{0}</a>", dr_ecourse["EC_CName"].ToString(), dr_ecourse["EC_TransNo"].ToString());
                }
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("131", dr["ED_Type"].ToString());
                if (dr_Phrase != null)
                {
                    dr["ED_TypeName"] = dr_Phrase["TypeName"].ToString();
                }
                DataRow dr_exam = DataExam.DataReader_EC_TransNo_AccID_MAX(dr["EC_TransNo"].ToString(), dr["ED_AccID"].ToString());
                if (dr_exam != null)
                    dr["EX_Score"] = dr_exam["EX_Score"].ToString();
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_EatHabits_str", "飲食習慣", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Transport_str", "交通方式", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_AttHours(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_TypeName", "上課狀況", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_AttHours_str", "訓練時數", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_AttStatus(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_SDate_Str", "開課日期", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "課程名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Name", "人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_TypeName", "上課狀況", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_SigninTime", "上課簽到", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_SignoffTime", "下課簽退", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    public static void DataColumn_AttBonus(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_YearMonth", "年月", false, 200, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_CName", "活動名稱", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_AccID", "業務員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_AttBonus", "出席加分", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
        BoundField bf_ = DataModel.CreateBoundField("EC_SDate", "", 10);
        bf_.HeaderStyle.CssClass = "GridViewColHidden";
        bf_.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf_);
    }
    public static void DataColumn_my(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateBoundField("NO", "NO", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_1", "開課日期", false, 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_2", "課程名稱", false, 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Type", "學院", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Rating", "分級", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Category_str", "數位/實體", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_TName", "授課講師", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EC_Place", "上課地點", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_Name", "上課人員", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("link_3", "進入課程", false, 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EX_Score", "分數", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ED_TypeName", "狀態", 100, "Center"));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEAttend where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEAttend";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region
    public static bool valid_ED(string EC_TransNo, string ED_AccID)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EC_TransNo", SqlAccess.CreateSqlParameter("@EC_TransNo", EC_TransNo));
        dParas.Add("ED_AccID", SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));

        string cmdtxt = "SELECT * FROM tblEAttend WHERE EC_TransNo=@EC_TransNo AND ED_AccID=@ED_AccID AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    public static bool valid_ED_Mode(string ED_AccID, string ED_Mode, string ED_AttendDate)
    {
        DataSet emds = new DataSet();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ED_AccID", SqlAccess.CreateSqlParameter("@ED_AccID", ED_AccID));
        dParas.Add("ED_Mode", SqlAccess.CreateSqlParameter("@ED_Mode", ED_Mode));
        dParas.Add("ED_AttendDate", SqlAccess.CreateSqlParameter("@ED_AttendDate", ED_AttendDate));

        string cmdtxt = "SELECT * FROM tblEAttend WHERE EC_TransNo=@EC_TransNo AND ED_AccID=@ED_AccID AND DATEDIFF(d, @ED_AttendDate, ED_AttendDate)=0 AND ModiState<>'D'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    public static RServiceProvider updateED_AttHours_ED_Score(string ED_TransNo, string EX_TransNo, string EX_UidNo, string ED_AttHours, string ED_Score)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder cmdText = new StringBuilder();

        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_TransNo", ED_TransNo));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EX_TransNo", EX_TransNo));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EX_UidNo", EX_UidNo));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_AttHours", ED_AttHours));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Score", ED_Score));
        cmdText.Append(" UPDATE tblEAttend SET EX_TransNo=@EX_TransNo,EX_UidNo=@EX_UidNo,ED_AttHours=@ED_AttHours,ED_Score=@ED_Score,ED_Type='1',ED_FinDate=GETDATE() WHERE ED_TransNo=@ED_TransNo ");

        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdText.ToString(), lSqlParas.ToArray());

        return rsp;
    }

    public static RServiceProvider updateED_ED_Type(string ED_TransNo, string ED_Type)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder cmdText = new StringBuilder();

        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_TransNo", ED_TransNo));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ED_Type", ED_Type));
        cmdText.Append(" UPDATE tblEAttend SET ED_Type=@ED_Type WHERE ED_TransNo=@ED_TransNo ");

        rsp = SqlAccess.SqlcommandNonQueryExecute("0", cmdText.ToString(), lSqlParas.ToArray());

        return rsp;
    }



}