﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.ComponentModel;

/// <summary>
/// DataAdMenuLayer 的摘要描述
/// </summary>
public class DataEmployee
{
    #region  資料讀取
    /// <summary>
    /// 公司資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Employee ORDER BY  emDate";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet baseDataReader(string emID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Employee WHERE emID=@emID";
        return SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emID", emID));
    }
    public static DataSet DataReader(string emNoName, string emEnable, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(emNoName, emEnable, lSqlParas);
        string sort = "emNo";
        //string virtualColumns = "";
        string columns = "emNo, emID, emPWD, emName, emMail, emPhone, emSex, specialLimit, emEnable, emDate, ipLimit, EmGroup";
        string cmdtxt = MainControls.sqlSelect(columns, "sys_Employee", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string emNoName, string emEnable)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_Employee");
        string where = whereString(emNoName, emEnable, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string emNoName, string enable, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (emNoName.Length > 0)
        {
            sbWhere.Append(" (emNo LIKE '%'+@emNo+'%' OR ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNoName));
            sbWhere.Append(" emName LIKE '%'+@emName+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emName", emNoName));
        }
        if (enable != "")
        {
            sbWhere.Append(" emEnable=@emEnable AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@emEnable", enable));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("Sex", typeof(string));    //性別
        dt.Columns.Add("Enable", typeof(string));    //啟用
        foreach (DataRow dr in dt.Rows)
        {
            dr["Sex"] = string.Format("{0}", dr["emSex"].ToString().Trim() == "True" ? "男" : "女");
            dr["Enable"] = string.Format("{0}", dr["emEnable"].ToString().Trim() == "True" ? true : false);
        }
    }
    #endregion
   
    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("emNo", "員工代碼", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("emID", "帳號", 100));
        mgv.Columns.Add(DataModel.CreateBoundField("emName", "姓名", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("Sex", "性別", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("emMail", "信箱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("emPhone", "電話", 80));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("Enable", "啟用", 40, "center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("emSex", "性別", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("enable", "啟用", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("emDate", "建立日期", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("ipLimit", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("EmGroup", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema(Boolean b)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, emID, emPWD, emName, emMail, emPhone, emSex, specialLimit, emEnable, ipLimit, EmGroup FROM sys_Employee where 1=0";
        if(b == false)
            cmdtxt = "SELECT  emNo, emName, emMail, emPhone, emSex, specialLimit, emEnable, ipLimit, EmGroup FROM sys_Employee where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_Employee";
        return emds.Tables[0].Clone();
    }
    public static DataTable GetSchema2(Boolean b)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  emNo, emID, emPWD, emName, emMail, emPhone, emSex, specialLimit, emEnable, ipLimit, EmGroup, AccID FROM sys_Employee where 1=0";
        if (b == false)
            cmdtxt = "SELECT  emNo, emID, emPWD, emName, emMail, emPhone, emSex, specialLimit, emEnable, ipLimit, AccID FROM sys_Employee where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_Employee";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  員工資料讀取for 'NO'
    public static DataRow emDataReader(string emNo)
    {
        DataRow dr = null;

        string cmdtxt = "SELECT * FROM sys_Employee WHERE emNo=@emNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emNo", emNo));
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    #endregion

    #region  員工資料讀取for 'AccID'
    public static DataRow personDataReader(string AccID)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("AccID", SqlAccess.CreateSqlParameter("@AccID", AccID));

        string cmdtxt = "SELECT * FROM tblPerson WHERE AccID=@AccID";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    #endregion

    #region  員工登入驗證
    /// <summary>
    /// 員工登入驗證
    /// </summary>
    /// <param name="m_emID">帳號</param>
    /// <param name="m_emPwd">密碼</param>
    /// <param name="userip">ip</param>
    /// <returns></returns>
    public static DataSet employeeLogin(string m_emID, string m_emPwd, string userip, int mode)
    {
        DataSet emds = new DataSet();
        StringBuilder sb = new StringBuilder();
        List<SqlParameter> lParas = new List<SqlParameter>();
        switch (mode)
        {
            case 0:
                sb.Append(" SELECT  * ")
            .Append(" FROM  sys_Employee  ")
            //.Append(" where emID=@emID  COLLATE Chinese_PRC_Stroke_CS_AS and emPwd=@emPwd  COLLATE Chinese_PRC_Stroke_CS_AS and emEnable=1 ");
                .Append(" where emID=@emID and emPwd=@emPwd  COLLATE Chinese_PRC_Stroke_CS_AS and emEnable=1 ");
                lParas.Add(SqlAccess.CreateSqlParameter("@emID", m_emID));
                lParas.Add(SqlAccess.CreateSqlParameter("@emPwd", m_emPwd));
                break;
            case 1:
                
                break;
        }
        emds = SqlAccess.SqlcommandExecute("0", sb.ToString(), lParas.ToArray());

        return emds;
    }
    #endregion

    #region  員工編號
    public static string emNoReader(string emID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Employee WHERE emID=@emID";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emID", emID));
        if (emds.Tables[0].Rows.Count == 1)
            return emds.Tables[0].Rows[0]["emNo"].ToString();
        else
            return "";
    }
    #endregion

    #region  驗證員工編號
    public static Boolean emNoValid(string emNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Employee WHERE emNo=@emNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emNo", emNo));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region  驗證員工帳號
    public static Boolean emIDValid(string emID)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Employee WHERE emID=@emID";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@emID", emID));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region 變更密碼
    public static RServiceProvider changePwd(string emID, string emPWD, string emNewPWD)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("emID", SqlAccess.CreateSqlParameter("@emID", emID));
        dParas.Add("emPwd", SqlAccess.CreateSqlParameter("@emPWD", emPWD));
        if (Convert.ToInt16(SqlAccess.SqlcommandExecuteScalar("0", " Select count(*) FROM sys_Employee where emID=@emID and emPWD=@emPWD ", dParas.Values.ToArray())) == 0)
            rsp.ReturnMessage = "原密碼錯誤 !";
        else
        {
            dParas["emPwd"].Value = emNewPWD;
            rsp = SqlAccess.SqlcommandNonQueryExecute("0", " UPDATE sys_Employee SET emPWD =@emPWD WHERE emID=@emID ", dParas.Values.ToArray());
        }

        return rsp;
    }
    #endregion
}
