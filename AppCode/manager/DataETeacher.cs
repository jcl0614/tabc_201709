﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataETeacher
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblETeacher WHERE ET_TName LIKE '%'+@searchString+'%' AND tblETeacher.ModiState<>'D'";
        else
            cmdtxt = "SELECT * FROM tblETeacher WHERE ET_TName LIKE '%'+@searchString+'%'";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_ET_TransNo(string ET_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ET_TransNo", SqlAccess.CreateSqlParameter("@ET_TransNo", ET_TransNo));

        string cmdtxt = "SELECT * FROM tblETeacher WHERE ET_TransNo=@ET_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string ET_Company, string ET_TCode, string ET_TName, string ET_Phone, string ET_Specialty, string ET_TSummary, string ET_TeacherType, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(ET_Company, ET_TCode, ET_TName, ET_Phone, ET_Specialty, ET_TSummary, ET_TeacherType, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblETeacher", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string ET_Company, string ET_TCode, string ET_TName, string ET_Phone, string ET_Specialty, string ET_TSummary, string ET_TeacherType)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblETeacher");
        string where = whereString(ET_Company, ET_TCode, ET_TName, ET_Phone, ET_Specialty, ET_TSummary, ET_TeacherType, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string ET_Company, string ET_TCode, string ET_TName, string ET_Phone, string ET_Specialty, string ET_TSummary, string ET_TeacherType, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (ET_Company.Length > 0)
        {
            sbWhere.Append(" ET_Company LIKE '%'+@ET_Company+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Company", ET_Company));
        }
        if (ET_TCode.Length > 0)
        {
            sbWhere.Append(" ET_TCode LIKE '%'+@ET_TCode+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TCode", ET_TCode));
        }
        if (ET_TName.Length > 0)
        {
            sbWhere.Append(" ET_TName LIKE '%'+@ET_TName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TName", ET_TName));
        }
        if (ET_Phone.Length > 0)
        {
            sbWhere.Append(" ET_Phone LIKE '%'+@ET_Phone+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Phone", ET_Phone));
        }
        if (ET_Specialty.Length > 0)
        {
            sbWhere.Append(" ET_Specialty LIKE '%'+@ET_Specialty+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_Specialty", ET_Specialty));
        }
        if (ET_TSummary.Length > 0)
        {
            sbWhere.Append(" ET_TSummary LIKE '%'+@ET_TSummary+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TSummary", ET_TSummary));
        }
        if (ET_TeacherType.Length > 0)
        {
            sbWhere.Append(" ET_TeacherType LIKE '%'+@ET_TeacherType+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ET_TeacherType", ET_TeacherType));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Company", "公司名稱", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_TName", "授課講師", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Phone", "聯絡電話", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_Specialty", "專長領域", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("ET_TSummary", "講師簡介", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblETeacher where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblETeacher";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_ET_TransNo(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0} ({1})", dr["ET_TName"].ToString(), dr["ET_Company"].ToString()), dr["ET_TransNo"].ToString()));
        }
    }
    #endregion


    #region  驗證講師代碼
    public static Boolean valid_ET_TCode(string ET_TCode)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblETeacher WHERE ET_TCode=@ET_TCode";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@ET_TCode", ET_TCode));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion


}