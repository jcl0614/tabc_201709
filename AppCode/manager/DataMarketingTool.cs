﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;

/// <summary>
/// DataLeftLinkClass 的摘要描述
/// </summary>
public class DataMarketingTool
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
     //   string cmdtxt = "SELECT TOP 5 *, (case class when '1' then '系統公告' else '行事曆' end) AS className, (SELECT CONVERT(varchar(10), addDate, 120)) AS DateStr FROM TABC_Sell_DOC ORDER BY addDate desc";
        string cmdtxt = "SELECT TABC_sno,TABC_FILE_KIND,TABC_FILE_No,TABC_FILE_PATH,TABC_FILE_NAME,ModiDate FROM TABC_Sell_DOC ORDER BY TABC_FILE_No desc";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet DataReader(string kind, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(kind, lSqlParas);
        string sort = "TABC_FILE_No";
        string Vcolumns = "TABC_sno,TABC_FILE_KIND,TABC_FILE_No,TABC_FILE_PATH,TABC_FILE_NAME,ModiDate,Replace(TABC_FILE_PATH,'/',';') as FILE_PATH";
     //   string columns = "id, class, (case class when '1' then '系統公告' else '行事曆' end) AS className, title, description, emNo, (select emName + '(' + emNo + ')' from sys_Employee where sys_Employee.emNo=TABC_Sell_DOC.emNo) AS emName, addDate, (SELECT CONVERT(varchar(10), addDate, 111)) AS DateStr";
        string columns = "TABC_sno,TABC_FILE_KIND,TABC_FILE_No,TABC_FILE_PATH,TABC_FILE_NAME,ModiDate,Replace(TABC_FILE_PATH,'/',';') as FILE_PATH";
        string cmdtxt = MainControls.sqlSelect(Vcolumns, columns, "TABC_Sell_DOC", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string kind)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM TABC_Sell_DOC ");
        string where = whereString(kind, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string kind, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (kind.Length > 0)
        {
            sbWhere.Append(" TABC_FILE_KIND=@TABC_FILE_KIND AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@TABC_FILE_KIND", kind));
        }
        //if (title.Length > 0)
        //{
        //    sbWhere.Append(" title LIKE '%'+@title+'%' AND ");
        //    lSqlParas.Add(SqlAccess.CreateSqlParameter("@title", title));
        //}
        //if (emNo != "")
        //{
        //    sbWhere.Append(" ((select emNo from sys_Employee where sys_Employee.emNo=TABC_Sell_DOC.emNo) LIKE '%'+@emNo+'%' OR (select emName from sys_Employee where sys_Employee.emNo=TABC_Sell_DOC.emNo) LIKE '%'+@emNo+'%') AND ");
        //    lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
        //}

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        DataColumn ChannelNameColumn = new DataColumn();
        ChannelNameColumn.DataType = System.Type.GetType("System.String");
        ChannelNameColumn.ColumnName = "TABC_FILE_PATH_Btn";
        ChannelNameColumn.Expression = "IIF(TABC_FILE_PATH = '', '', '<a href=\"javascript:void(0);\" onclick=\"getcycloByItem1('''+FILE_PATH+TABC_FILE_NAME+''')\" class=\"btn btn-warning  btn-xs\" style=\"font-size:14px;\" title=\"另開視窗\"><img src=\"images/download.jpg\" alt=\"DownLoad Me\" height=\"24\">&nbsp;點我下載</a>')";
        dt.Columns.Add(ChannelNameColumn);
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
      //  mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("TABC_FILE_No", "編號", 80, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("TABC_FILE_NAME", "檔案名稱", false, 400, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("TABC_FILE_PATH_Btn", "文件下載", false, 150, "Center"));

        //隱藏欄位
        //BoundField bf = DataModel.CreateBoundField("id", "key", 10);
        //bf.HeaderStyle.CssClass = "GridViewColHidden";
        //bf.ItemStyle.CssClass = "GridViewColHidden";
        //mgv.Columns.Add(bf);

    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        //"TABC_sno":7,"TABC_FILE_KIND":"1101","TABC_FILE_No":"1","TABC_FILE_PATH":"C:\\inetpub\\wwwroot\\gmTABC2\\TABCfolder\\SellDoc\\","TABC_FILE_NAME":"1030408_晚美人生大樂退_莊介博副總.pdf","ModiDate"
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  TABC_sno,TABC_FILE_KIND,TABC_FILE_No,TABC_FILE_PATH,TABC_FILE_NAME,ModiDate FROM TABC_Sell_DOC where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "TABC_Sell_DOC";
        return emds.Tables[0].Clone();
    }
    #endregion


}