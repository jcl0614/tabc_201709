﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataENotice
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_EN_TransNo(string EN_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EN_TransNo", SqlAccess.CreateSqlParameter("@EN_TransNo", EN_TransNo));

        string cmdtxt = "SELECT * FROM tblENotice WHERE EN_TransNo=@EN_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblENotice", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblENotice");
        string where = whereString(EN_Year, EN_Type, EN_AccID, EN_InfoDate_s, EN_InfoDate_e, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EN_Year, string EN_Type, string EN_AccID, string EN_InfoDate_s, string EN_InfoDate_e, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EN_Year.Length > 0)
        {
            sbWhere.Append(" EN_Year=@EN_Year AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Year", EN_Year));
        }
        if (EN_Type.Length > 0)
        {
            sbWhere.Append(" EN_Type=@EN_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_Type", EN_Type));
        }
        if (EN_AccID.Length > 0)
        {
            sbWhere.Append(" (EN_AccID LIKE '%'+@EN_AccID+'%' OR (select NAME from tabcSales where tabcSales.cSAL_FK=tblENotice.EN_AccID) LIKE '%'+@EN_AccID+'%') AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_AccID", EN_AccID));
        }
        if (EN_InfoDate_s.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EN_InfoDate_s, EN_InfoDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_InfoDate_s", EN_InfoDate_s));
        }
        if (EN_InfoDate_e.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @EN_InfoDate_e, EN_InfoDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EN_InfoDate_e", EN_InfoDate_e));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EN_InfoDate_str", typeof(string));
        dt.Columns.Add("EN_AccID_Name", typeof(string));
        dt.Columns.Add("EN_TypeNAME", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            if(dr["EN_InfoDate"].ToString() != "")
                dr["EN_InfoDate_str"] = DateTime.Parse(dr["EN_InfoDate"].ToString()).ToString("yyyy/MM/dd");
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("E004", dr["EN_Type"].ToString());
            if (dr_Phrase != null)
                dr["EN_TypeNAME"] = dr_Phrase["TypeName"].ToString();
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EN_AccID"].ToString());
            if (dr_sales != null)
            {
                dr["EN_AccID_Name"] = dr_sales["NAME"].ToString();

            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EN_Year", "通知年度", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EN_InfoDate_str", "通知日期", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EN_AccID", "人員編號", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EN_AccID_Name", "姓名", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EN_TypeNAME", "通知狀態", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblENotice where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblENotice";
        return emds.Tables[0].Clone();
    }
    #endregion




}