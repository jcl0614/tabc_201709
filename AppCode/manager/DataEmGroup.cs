﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.ComponentModel;

/// <summary>
/// DataAdMenuLayer 的摘要描述
/// </summary>
public class DataEmGroup
{
    #region  資料讀取
    /// <summary>
    /// 公司資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_EmGroup ORDER BY id";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet baseDataReader(Boolean b)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM sys_EmGroup WHERE enable=1 ORDER BY  id";
        else
            cmdtxt = "SELECT * FROM sys_EmGroup ORDER BY  sort";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }
    public static DataSet baseDataReader(string id)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_EmGroup WHERE id=@id";
        return SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@id", id));
    }
    public static DataSet DataReader(string name, string enable, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(name, enable, lSqlParas);
        string sort = "id";
        //string virtualColumns = "";
        string columns = "id, name, enable, ipLimit";
        string cmdtxt = MainControls.sqlSelect(columns, "sys_EmGroup", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string name, string enable)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_EmGroup");
        string where = whereString(name, enable, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string name, string enable, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (name.Length > 0)
        {
            sbWhere.Append(" name LIKE '%'+@name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@name", name));
        }
        if (enable != "")
        {
            sbWhere.Append(" enable=@enable AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@enable", enable));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {

    }
    #endregion
   
    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("name", "群組名稱", 600, "Left"));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("Enable", "啟用", 40, "center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "性別", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("enable", "啟用", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("ipLimit", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, name, enable, ipLimit FROM sys_EmGroup where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_EmGroup";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  群組資料讀取for 'id'
    public static DataRow emGroupDataReader(string id)
    {
        DataRow dr = null;

        string cmdtxt = "SELECT * FROM sys_EmGroup WHERE id=@id";

        if (SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@id", id)).Tables[0].Rows.Count == 1)
            dr = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@id", id)).Tables[0].Rows[0];

        return dr;
    }
    #endregion

    #region 取的最大id
    public static string MAX_id()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT MAX(id) FROM sys_EmGroup";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds.Tables[0].Rows[0][0].ToString();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="menuLayerDDL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DropDownList(DropDownList DDL, bool all, string allString)
    {
        DDL.Items.Clear();
        if (all)
            DDL.Items.Add(new ListItem(allString, ""));
        DataSet emds = baseDataReader(true);
        if (emds != null && emds.Tables.Count > 0)
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0}) {1}", dr["id"].ToString(), dr["name"].ToString()), dr["id"].ToString()));
        }
    }
    #endregion   

}
