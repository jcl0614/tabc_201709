﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataERule
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataRow DataReader_ER_TransNo(string ER_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ER_TransNo", SqlAccess.CreateSqlParameter("@ER_TransNo", ER_TransNo));

        string cmdtxt = "SELECT * FROM tblERule WHERE ER_TransNo=@ER_TransNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet DataReader_ER_YearNo(string ER_YearNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ER_YearNo", SqlAccess.CreateSqlParameter("@ER_YearNo", ER_YearNo));

        string cmdtxt = "SELECT * FROM tblERule WHERE ER_YearNo=@ER_YearNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return ds;
    }
    public static DataSet DataReader_ER_Type(string ER_Type)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ER_Type", SqlAccess.CreateSqlParameter("@ER_Type", ER_Type));

        string cmdtxt = "SELECT * FROM tblERule WHERE ER_Type=@ER_Type and ModiState<>'D' order by ER_YearNo desc";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());

        return ds;
    }
    public static DataRow DataReader_ER_YearNo_ER_Type(string ER_YearNo, string ER_Type)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ER_YearNo", SqlAccess.CreateSqlParameter("@ER_YearNo", ER_YearNo));
        dParas.Add("ER_Type", SqlAccess.CreateSqlParameter("@ER_Type", ER_Type));

        string cmdtxt = "SELECT * FROM tblERule WHERE ER_YearNo=@ER_YearNo and ER_Type=@ER_Type and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static int DataReader_ER_YearNo_ER_Hours(string ER_YearNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("ER_YearNo", SqlAccess.CreateSqlParameter("@ER_YearNo", ER_YearNo));

        string cmdtxt = "SELECT sum(ER_Hours) ER_Hours FROM tblERule WHERE ER_YearNo=@ER_YearNo and ModiState<>'D'";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count != 0)
            dr = ds.Tables[0].Rows[0];

        return int.Parse(dr["ER_Hours"].ToString());
    }
    public static DataSet DataReader(string ER_Type, string ER_Continue, string ER_YearName, string ER_Hours, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(ER_Type, ER_Continue, ER_YearName, ER_Hours, lSqlParas);
        string sort = "ER_Type,ER_YearNo";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblERule", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string ER_Type, string ER_Continue, string ER_YearName, string ER_Hours)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblERule");
        string where = whereString(ER_Type, ER_Continue, ER_YearName, ER_Hours, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string ER_Type, string ER_Continue, string ER_YearName, string ER_Hours, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (ER_Type.Length > 0)
        {
            sbWhere.Append(" ER_Type=@ER_Type AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ER_Type", ER_Type));
        }
        if (ER_Continue.Length > 0)
        {
            sbWhere.Append(" ER_Continue=@ER_Continue AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ER_Continue", ER_Continue));
        }
        if (ER_YearName.Length > 0)
        {
            sbWhere.Append(" ER_YearName LIKE '%'+@ER_YearName+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ER_YearName", ER_YearName));
        }
        if (ER_Hours.Length > 0)
        {
            sbWhere.Append(" ER_Hours=@ER_Hours AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@ER_Hours", ER_Hours));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("ER_Type_str", typeof(string));
        dt.Columns.Add("ER_Continue_str", typeof(string));
        dt.Columns.Add("delBtn", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("128", dr["ER_Type"].ToString());
            if (dr_Phrase != null)
                dr["ER_Type_str"] = dr_Phrase["TypeName"].ToString();
            dr["ER_Continue_str"] = dr["ER_Continue"].ToString() == "True" ? "是" : "否";
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_Type_str", "類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_YearName", "訓練年度", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_Hours", "規定時數", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_YearNo", "年度數值", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_Continue_str", "下一年度沿用", 60, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("ER_Memo", "說明", 300, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("delBtn", "", 50));
        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblERule where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblERule";
        return emds.Tables[0].Clone();
    }
    #endregion




}