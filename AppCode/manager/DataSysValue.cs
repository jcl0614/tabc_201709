﻿
/// <summary>
/// DataSysValue 的摘要描述
/// </summary>
public class DataSysValue
{
    //密碼
    public static string Password
    {
        get { return EncryptionKey("p"); }
    }
    //地址
    public static string Address
    {
        get { return EncryptionKey("d"); }
    }
    //Email
    public static string eMail
    {
        get { return EncryptionKey("e"); }
    }
    //電話
    public static string Tel
    {
        get { return EncryptionKey("t"); }
    }
    //連線字串
    public static string Connection
    {
        get { return EncryptionKey("c"); }
    }
    //member
    public static string Member
    {
        get { return EncryptionKey("m"); }
    }
    /// <summary>
    /// 加密Key
    /// </summary>
    /// <param name="mode">p:password, d:address, e:email, t:tel</param>
    /// <returns></returns>
    public static string EncryptionKey(string mode)
	{
        string rmode = string.Empty;
        switch (mode)
        {
            case "p":   //password
                rmode = "PWD915PWD828";
                break;
            case "d":   //address
                rmode = "Address915Address828";
                break;
            case "e":   //Email
                rmode = "Mail915Mail828";
                break;
            case "t":   //Tel
                rmode = "Tel915Tel828";
                break;
            case "c" : //連線字串加密
                rmode = "conn915conn828";
                break;
            case "m": //member
                rmode = "mem915men828";
                break;
        }
        return rmode;
    }
}
