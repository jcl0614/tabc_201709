﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Web;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataErrorLog
{
    

    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet DataReader(string edDateBeg, string edDateEnd, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(edDateBeg, edDateEnd, lSqlParas);
        string sort = "ApplyDate desc";
        string Vcolumns = "Id, ApplyDate, Message";
        string columns = "Id, ApplyDate, Message";
        string cmdtxt = MainControls.sqlSelect(Vcolumns, columns, "sys_ErrorLog", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string edDateBeg, string edDateEnd)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_ErrorLog");
        string where = whereString(edDateBeg, edDateEnd, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string edDateBeg, string edDateEnd, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        //日期起
        if (edDateBeg.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateBeg, ApplyDate)>=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateBeg", edDateBeg));
        }
        //日期訖
        if (edDateEnd.Length > 0)
        {
            sbWhere.Append("DATEDIFF(d, @edDateEnd, ApplyDate)<=0 AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@edDateEnd", edDateEnd));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("Message_Line1", typeof(string));    //啟用
        foreach (DataRow dr in dt.Rows)
        {
            dr["Message_Line1"] = string.Format("<span onclick=\"javascript:alert('{0}');\" style=\"cursor:pointer;\">{1}</span>", dr["Message"].ToString().Replace("\r\n", "\\n").Replace(HttpUtility.UrlDecode("%0A"), "\\n").Replace("'", "’"), dr["Message"].ToString().Split('\r')[0]);
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("ApplyDate", "日期", "{0:yyyy-MM-dd HH:mm:ss}", 150));
        mgv.Columns.Add(DataModel.CreateBoundField("Message_Line1", "訊息", false, 1000, "Left"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "類別", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("Message", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    #region 新增資料
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 刪除資料
    public static RServiceProvider Delete(string Id)
    {
        RServiceProvider rsp = new RServiceProvider();

        rsp = SqlAccess.SqlcommandNonQueryExecute("0", " DELETE FROM sys_ErrorLog WHERE Id=@Id ", SqlAccess.CreateSqlParameter("@Id", Id));

        return rsp;
    }
    #endregion

    #region 刪除資料
    public static RServiceProvider DeleteBatch(string sDate, string eDate)
    {
        RServiceProvider rsp = new RServiceProvider();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@sDate", sDate));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@eDate", eDate));
        rsp = SqlAccess.SqlcommandNonQueryExecute("0", " DELETE FROM sys_ErrorLog WHERE DATEDIFF(d, @sDate, ApplyDate)>=0 AND DATEDIFF(d, @eDate, ApplyDate)<=0 ", lSqlParas.ToArray());

        return rsp;
    }
    #endregion

    #region  資料匯出
    public static DataTable DataExport(string sDate, string eDate)
    {
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@sDate", sDate));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@eDate", eDate));

        string cmdtxt = "SELECT Id AS '編號', ApplyDate AS '日期時間', Message AS '訊息' FROM sys_ErrorLog WHERE  DATEDIFF(d, @sDate, ApplyDate)>=0 AND DATEDIFF(d, @eDate, ApplyDate)<=0 ORDER BY ApplyDate desc";

        return SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray()).Tables[0];

    }
    #endregion
}