﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections;
using System.ComponentModel;

/// <summary>
/// menu 的摘要描述
/// </summary>
public class DataMenu
{
    #region  系統功能表選單讀取
    /// <summary>
    /// 系統功能表選單讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet menuReader(string muLayer , string muEnable, string muLocation, string  sortExpression ,int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(muLayer, muEnable, muLocation, lSqlParas);
        if (sortExpression.Trim().Length == 0)
            sortExpression = "muLayer, muSort, muNo";
        string virtualColumns = "muNo, muLayer, muLayerName, muSort, muName, muHyperLink, muTarget, muEnable, muLocation, muKm, muIsSub";
        string columns = "muNo, muLayer, (select name from sys_MenuLayer where sys_MenuLayer.id = muLayer) as muLayerName, muSort, muName, muHyperLink, muTarget, muEnable, muLocation, muKm, muIsSub";
        string cmdtxt = MainControls.sqlSelect(virtualColumns, columns, "sys_Menu", where, sortExpression, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }

    public static DataSet menuReader(string muLayer, string emNo)
    {
        emNo = emNo.Trim();
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLayer", muLayer));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));

        string cmdtxt = "SELECT muLayer, muSort, muName, muHyperLink, muTarget, muEnable, muLocation, muKm, muIsSub FROM sys_EmployeeLimit INNER JOIN sys_Menu ON sys_EmployeeLimit.muNo = sys_Menu.muNo " +
                         "WHERE muLayer = @muLayer AND emNo = @emNo AND muEnable=1 ORDER BY muLayer, muSort, sys_Menu.muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet menuReader_group(string muLayer, string gpId)
    {
        gpId = gpId.Trim();
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLayer", muLayer));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@gpId", gpId));

        string cmdtxt = "SELECT muLayer, muSort, muName, muHyperLink, muTarget, muEnable, muLocation, muKm, muIsSub  FROM sys_EmGroupLimit INNER JOIN sys_Menu ON sys_EmGroupLimit.muNo = sys_Menu.muNo " +
                         "WHERE muLayer = @muLayer AND gpId = @gpId AND muEnable=1 ORDER BY muLayer, muSort, sys_Menu.muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }

    public static DataTable menuReader(string muHyperLink)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muHyperLink", muHyperLink));

        string cmdtxt = "SELECT *  FROM sys_Menu WHERE muHyperLink=@muHyperLink";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds.Tables[0];
    }

    public static DataTable menuReader_Km(string muKm)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muKm", muKm));

        string cmdtxt = "SELECT *  FROM sys_Menu WHERE muKm=@muKm";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds.Tables[0];
    }

    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet menuddlSelectReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  muNo, muName FROM sys_Menu";
        return SqlAccess.SqlcommandExecute("0", cmdtxt);
    }

    public static DataSet menuddlSelectReader(string muLayer)
    {
        string cmdtxt = "SELECT  muNo, muName FROM sys_Menu WHERE muLayer=@muLayer ORDER BY muLayer, muSort, muNo";
        return SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@muLayer", muLayer.Trim()));
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string muLayer, string muEnable, string muLocation, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (muLayer.Length > 0)
        {
            sbWhere.Append(" muLayer=@muLayer AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLayer", muLayer));
        }
        if (muEnable != "")
        {
            sbWhere.Append(" muEnable=@muEnable AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muEnable", muEnable));
        }
        if (muLocation != "")
        {
            sbWhere.Append(" muLocation=@muLocation AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLocation", muLocation));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string muLayer, string muEnable, string muLocation)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_Menu");
        string where = whereString(muLayer, muEnable, muLocation, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 處理動態欄位
    private static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("Enable", typeof(string));    //啟用
        dt.Columns.Add("muLocationStr", typeof(string));    //啟用
        foreach (DataRow dr in dt.Rows)
        {
            dr["Enable"] = string.Format("{0}", dr["muEnable"].ToString().Trim() == "True" ? true : false);
            switch (dr["muLocation"].ToString())
            {
                case "0":
                    dr["muLocationStr"] = string.Format("<span style=\"color:{1}\">{0}</span>", "內外網", "#000000");
                    break;
                case "1":
                    dr["muLocationStr"] = string.Format("<span style=\"color:{1}\">{0}</span>", "內部網", "#009933");
                    break;
                case "2":
                    dr["muLocationStr"] = string.Format("<span style=\"color:{1}\">{0}</span>", "外部網", "#CC0000");
                    break;
            }
        }
    }
    #endregion

    #region 功能表資料行
    public static void menuDataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 70));
        mgv.Columns.Add(DataModel.CreateBoundField("muNo", "功能編號", 40, true));
        mgv.Columns.Add(DataModel.CreateBoundField("muLayer", "類別編號", 40, true));
        mgv.Columns.Add(DataModel.CreateBoundField("muSort", "順序", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("muName", "選單名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("muHyperLink", "選單連結", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("muTarget", "連結目標", 40));
        mgv.Columns.Add(DataModel.CreateBoundField("muLocationStr", "連線區域", false, 70));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("Enable", "啟用", 40, "center"));

        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("muEnable", "啟用", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("muLocation", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("muKm", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
        bf = DataModel.CreateBoundField("muIsSub", "", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);
    }
    #endregion

    /// <summary>
    /// DataMenu.DataTextField
    /// </summary>
    /// <returns></returns>
    public static string muDTF
    {
        get { return "muName"; }
    }

    /// <summary>
    /// DataMenu.DataValueField
    /// </summary>
    /// <returns></returns>
    public static string muDVF
    {
        get { return "muNo"; }
    }

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  muNo, muLayer, muSort, muName, muHyperLink, muTarget, muEnable, muLocation, muKm, muIsSub FROM sys_Menu";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_Menu";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void menuReaderDropDownList(DropDownList menuLayerDDL, bool all, string muLayer)
    {
        menuLayerDDL.Items.Clear();
        if (all)
            menuLayerDDL.Items.Add(new ListItem("全部", ""));
        DataSet emds = menuddlSelectReader(muLayer);
        if (emds != null && emds.Tables.Count > 0)
        {
            foreach (DataRow dr in emds.Tables[0].Rows)
                menuLayerDDL.Items.Add(new ListItem(dr["muName"].ToString(), dr["muNo"].ToString()));
        }
    }
    #endregion   

    #region  驗證選單代碼
    public static Boolean muNoValid(string muNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Menu WHERE muNo=@muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@muNo", muNo));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region  驗證選單連結
    public static Boolean hyperLinkValid(string muHyperLink)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM sys_Menu WHERE muHyperLink=@muHyperLink";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@muHyperLink", muHyperLink));
        if (emds.Tables[0].Rows.Count == 0)
            return true;
        else
            return false;
    }
    #endregion

    #region 判斷內外網
    public static Boolean LocationValid(string url, string emNo)
    {
        bool b = true;
        string type = "0";
        Uri uri = new Uri(url);
        for (int i = uri.Segments.Length - 1; i >= 0; i--)
        {
            DataTable dtMenu = DataMenu.menuReader(uri.Segments[i].Replace("/", string.Empty) + uri.Query.Split('&')[0]);
            if (dtMenu.Rows.Count != 0)
            {
                type = dtMenu.Rows[0]["muLocation"].ToString();
                break;
            }
        }

        switch (type)
        {
            case "0":
                b = true;
                break;
            case "1":
                if (url.IndexOf("http://192.168") != -1 || url.IndexOf("https://192.168") != -1 || url.IndexOf("http://localhost") != -1)
                    b = true;
                else
                    b = false;
                    
                break;
            case "2":
                if (url.Replace("https://", "http://").IndexOf(new WebSetValue().WebUrl) != -1)
                    b = true;
                else
                    b = false;
                break;

        }

        #region 判斷IP權限
        if (!string.IsNullOrEmpty(DataWebSet.dataReader("WebMapIP")) && DataWebSet.dataReader("WebMapIP").IndexOf(MainControls.GetUserIPAddress()) == -1)
            b = false;
        else
        {
            if (!string.IsNullOrEmpty(emNo))
            {
                DataRow dr_employee = DataEmployee.emDataReader(emNo);
                if (dr_employee != null)
                {
                    if (!string.IsNullOrEmpty(dr_employee["ipLimit"].ToString()) && dr_employee["ipLimit"].ToString().IndexOf(MainControls.GetUserIPAddress()) == -1)
                        b = false;
                    else
                    {
                        if (!string.IsNullOrEmpty(dr_employee["EmGroup"].ToString()))
                        {
                            DataRow dr_group = DataEmGroup.emGroupDataReader(dr_employee["EmGroup"].ToString());
                            if (dr_group != null)
                            {
                                if (!string.IsNullOrEmpty(dr_group["ipLimit"].ToString()) && dr_group["ipLimit"].ToString().IndexOf(MainControls.GetUserIPAddress()) == -1)
                                    b = false;
                                else
                                    b = true;
                            }
                            else
                                b = false;
                        }
                        else
                            b = true;
                    }
                }
                else
                    b = false;
            }
            else
                b = true;
        }
        #endregion

        return b;
    }
    #endregion

    #region 檢查此頁面權限

    public static bool PageLimitCheck(string PageName, string emNo)
    {
        DataRow dr = DataEmployee.emDataReader(emNo);
        if (dr != null)
        {
            DataSet emds = new DataSet();
            List<SqlParameter> lSqlParas = new List<SqlParameter>();
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muHyperLink", PageName));

            StringBuilder sbSQL = new StringBuilder("");
            if (string.IsNullOrEmpty(dr["EmGroup"].ToString()))
            {
                lSqlParas.Add(SqlAccess.CreateSqlParameter("@emNo", emNo));
                sbSQL = new StringBuilder("")
                .Append(" SELECT COUNT(muNo) ")
                .Append(" FROM [sys_Menu]  ")
                .Append(" where muHyperLink=@muHyperLink ")
                .Append(" and muNo in(SELECT [muNo]  ")
                .Append("             FROM [sys_EmployeeLimit]  ")
                .Append("             Where emNo=@emNo And (emInsert=1 OR emUpdate=1 OR emSelect=1 OR emDelete=1)) ");
            }
            else
            {
                lSqlParas.Add(SqlAccess.CreateSqlParameter("@EmGroup", dr["EmGroup"].ToString()));
                sbSQL = new StringBuilder("")
                .Append(" SELECT COUNT(muNo) ")
                .Append(" FROM [sys_Menu]  ")
                .Append(" where muHyperLink=@muHyperLink ")
                .Append(" and muNo in(SELECT [muNo]  ")
                .Append("             FROM [sys_EmGroupLimit]  ")
                .Append("             Where gpId=@EmGroup And (gpInsert=1 OR gpUpdate=1 OR gpSelect=1 OR gpDelete=1)) ");
            }

            object obj = SqlAccess.SqlcommandExecuteScalar("0", sbSQL.ToString(), lSqlParas.ToArray());

            if (obj == null || obj.ToString() == "0")
                return false;
            else
                return true;
        }
        else
            return false;
    }

    #endregion
}
