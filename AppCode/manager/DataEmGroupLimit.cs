﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Text;

/// <summary>
/// EmGroupLimit 的摘要描述
/// </summary>
public class DataEmGroupLimit
{
    #region  群組系統使用權限資料讀取讀取
    /// <summary>
    /// 群組系統使用權限資料讀取讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet EmGroupLimitReader()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, gpId, (SELECT name FROM sys_EmGroup WHERE gpId=sys_EmGroupLimit.gpId) as name, muNo, (SELECT muName FROM sys_Menu WHERE sys_Menu.muNo=sys_EmGroupLimit.muNo) as muName, gpInsert, gpUpdate, gpSelect, gpDelete FROM sys_EmGroupLimit ORDER BY gpId, muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        return emds;
    }

    public static DataTable EmGroupLimitReader(string muNo)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, gpId, muNo FROM sys_EmGroupLimit WHERE muNo=@muNo ORDER BY gpId, muNo";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@muNo", muNo.Trim()));
        emds.Tables[0].TableName = "sys_EmGroupLimit";
        return emds.Tables[0].Copy();
    }

    /// <summary>
    /// 查詢群組資料
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataSet EmGroupLimitReaderForEmGroup(string gpId, string muLayer, string sortExpression, int startRowIndex, int maximumRows)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbVirtualColumn = new StringBuilder("id")
                .Append(", muNo")
                .Append(", muName")
                .Append(", gpInsert")
                .Append(", gpUpdate")
                .Append(", gpSelect")
                .Append(", gpDelete");

        StringBuilder sbColumn = new StringBuilder("id")
                .Append(", muNo")
                .Append(", (SELECT muName FROM sys_Menu WHERE sys_Menu.muNo=sys_EmGroupLimit.muNo) as muName")
                .Append(", gpInsert")
                .Append(", gpUpdate")
                .Append(", gpSelect")
                .Append(", gpDelete");

        if (sortExpression.Trim().Length == 0)
            sortExpression = "muNo";

        string where = whereString(gpId, muLayer, lSqlParas);
        string cmdtxt = MainControls.sqlSelect(sbVirtualColumn.ToString()
            , sbColumn.ToString()
            , "sys_EmGroupLimit"
            , where
            , sortExpression
            , startRowIndex
            , maximumRows);

        if (lSqlParas.Count == 0)
            return SqlAccess.SqlcommandExecute("0", cmdtxt);

        return SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray<SqlParameter>());
    }
    /// <summary>
    /// 筆數
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string gpId, string muLayer)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM sys_EmGroupLimit");
        string where = whereString(gpId, muLayer, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    /// <summary>
    /// 查詢條件處理
    /// </summary>
    /// <returns></returns>
    public static String whereString(string gpId, string muLayer, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        sbWhere.Append(" gpId=@gpId AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@gpId", gpId));

        if (muLayer != "")
        {
            sbWhere.Append(" (select muLayer from sys_Menu where sys_Menu.muNo=sys_EmGroupLimit.muNo)=@muLayer AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@muLayer", muLayer));
        }

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }

    #endregion

    #region 取得系統程式使用權限
    public static MaintainAuthority EmGroupLimitReader(string gpId, string muHyperLink)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT  gpInsert, gpUpdate, gpSelect, gpDelete FROM sys_EmGroupLimit WHERE gpId=@gpId AND muNo=(SELECT muNo FROM sys_Menu WHERE muHyperLink=@muHyperLink)";
        List<SqlParameter> lParas = new List<SqlParameter>();
        lParas.Add(SqlAccess.CreateSqlParameter("@gpId", gpId));
        lParas.Add(SqlAccess.CreateSqlParameter("@muHyperLink", muHyperLink));
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lParas.ToArray());

        bool elInster = false;
        bool gpUpdate = false;
        bool gpSelect = false;
        bool gpDelete = false;

        if (emds != null && emds.Tables.Count > 0 && emds.Tables[0].Rows.Count > 0)
        {
            DataRow row = emds.Tables[0].Rows[0];
            elInster = Convert.ToBoolean(row["gpInsert"]);
            gpUpdate = Convert.ToBoolean(row["gpUpdate"]);
            gpSelect = Convert.ToBoolean(row["gpSelect"]);
            gpDelete = Convert.ToBoolean(row["gpDelete"]);
        }
        return new MaintainAuthority(elInster, gpUpdate, gpSelect, gpDelete);
    }
    #endregion

    #region 功能表資料行
    public static void EmGroupLimitDataColumn(GridView mgv)
    {
        //隱藏欄位
        BoundField bf = DataModel.CreateBoundField("id", "Key", 10);
        bf.HeaderStyle.CssClass = "GridViewColHidden";
        bf.ItemStyle.CssClass = "GridViewColHidden";
        mgv.Columns.Add(bf);

        mgv.Columns.Add(DataModel.CreateBoundField("muNo", "編號", 50, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("muName", "程式名稱", 250, "Left"));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("gpInsert", "新增", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("gpUpdate", "修改", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("gpSelect", "查詢", 40));
        mgv.Columns.Add(DataModel.CreateCheckBoxField("gpDelete", "刪除", 40));
        mgv.Columns.Add(DataModel.CreateCommandField(ButtonType.Button, false, true, true, 150));
        (mgv.Columns[0] as BoundField).ReadOnly = true;
        (mgv.Columns[1] as BoundField).ReadOnly = true;
        (mgv.Columns[2] as BoundField).ReadOnly = true;

        
    }
    #endregion

    #region 更新資料庫(群組)
    /// <summary>
    /// 更新資料庫(群組)
    /// </summary>
    /// <param name="gpId">群組代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="emNo">使用者帳號</param>
    /// <param name="muNo">程式編號</param>
    /// <param name="gpInsert">新增功能</param>
    /// <param name="gpUpdate">修改功能</param>
    /// <param name="gpSelect">查詢功能</param>
    /// <param name="gpDelete">刪除功能</param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void EmGroupLimitUpdate(string id, string emNo, string progId, string muNo, bool gpInsert, bool gpUpdate, bool gpSelect, bool gpDelete)
    {
        DataTable dt = GetSchema();
        dt.Columns.Remove("gpId");
        dt.Columns.Remove("muNo");
        dt.Rows.Add(id, gpInsert, gpUpdate, gpSelect, gpDelete);
        DataSet emds = new DataSet();
        emds.Tables.Add(dt);

        //群組維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, emds, DataemDaily.MaintainStatus.Update);
        SqlAccess.SqlCommandUpdate("0", emds);
    }
    #endregion

    #region 刪除資料庫(群組)
    /// <summary>
    /// 刪除資料庫(群組)
    /// </summary>
    /// <param name="gpId">群組代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="emNo">使用者帳號</param>
    /// <param name="muNo">程式編號</param>
    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static void EmGroupLimitDelete(string id, string emNo, string progId, string muNo)
    {
        DataTable dt = GetSchema();
        dt.Columns.Remove("gpId");
        dt.Columns.Remove("muNo");
        dt.Columns.Remove("gpInsert");
        dt.Columns.Remove("gpUpdate");
        dt.Columns.Remove("gpSelect");
        dt.Columns.Remove("gpDelete");
        dt.Rows.Add(id);
        DataSet emds = new DataSet();
        emds.Tables.Add(dt);

        //群組維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, emds, DataemDaily.MaintainStatus.Delete);
        SqlAccess.SqlCommandDelete("0", emds);

    }
    #endregion

    #region  刪除群組所有權限
    public static RServiceProvider gpIdEmGroupLimitDelete(string gpId)
    {
        DataSet emds = new DataSet();
        string cmdtxt = "DELETE FROM sys_EmGroupLimit WHERE gpId=@gpId";

        return SqlAccess.SqlcommandNonQueryExecute("0", cmdtxt, SqlAccess.CreateSqlParameter("@gpId", gpId));
    }
    #endregion

    #region 新增資料庫(群組)
    /// <summary>
    /// 新增資料庫(群組)
    /// </summary>
    /// <param name="gpId">群組代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider EmGroupLimitAppend(string gpId, string progId, DataSet ds)
    {
        //群組維護系統資料記錄
        DataemDaily.emDailyInsert("0", gpId, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 功能啟用項讀取成ListItems
    public static void FunctionReaderCheckBoxList(CheckBoxList mossRCBL)
    {
        string[] func = new string[]{
            "gpInsert,新增",
            "gpUpdate,修改",
            "gpSelect,查詢",
            "gpDelete,刪除"};

        mossRCBL.Items.Clear();
        foreach (string str in func)
        {
            string[] mstr = str.Split(',');
            mossRCBL.Items.Add(new ListItem(mstr[1], mstr[0]));
        }
    }
    #endregion
    
    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT id, gpId, muNo, gpInsert, gpUpdate, gpSelect, gpDelete FROM sys_EmGroupLimit WHERE 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "sys_EmGroupLimit";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region  驗證權限是否已經加入
    public static Boolean gpIdLimitValid(string gpId, string muNo)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string cmdtxt = "SELECT * FROM sys_EmGroupLimit WHERE gpId=@gpId AND muNo=@muNo";
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@gpId", gpId.Trim()));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@muNo", muNo.Trim()));
        int count = (int)SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray()).Tables[0].Rows.Count;
        if (count == 0)
            return true;
        else
            return false;
    }
    #endregion

}
