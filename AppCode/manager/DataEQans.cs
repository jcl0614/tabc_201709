﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;
using System.ComponentModel;
using System.Linq;

/// <summary>
/// DataTopLinkString 的摘要描述
/// </summary>
public class DataEQans
{
    #region  資料讀取
    /// <summary>
    /// 資料讀取
    /// </summary>
    /// <returns></returns>
    public static DataSet baseDataReader(Boolean b, string searchString)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@searchString", searchString));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblEQans WHERE EA_ListItem LIKE '%'+@searchString+'%' AND tblEQans.ModiState<>'D' ordr by EA_OrderNo";
        else
            cmdtxt = "SELECT * FROM tblEQans";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataRow DataReader_EA_TransNo(string EA_TransNo)
    {
        DataRow dr = null;

        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        dParas.Add("EA_TransNo", SqlAccess.CreateSqlParameter("@EA_TransNo", EA_TransNo));

        string cmdtxt = "SELECT * FROM tblEQans WHERE EA_TransNo=@EA_TransNo";

        DataSet ds = SqlAccess.SqlcommandExecute("0", cmdtxt, dParas.Values.ToArray());
        if (ds != null && ds.Tables.Count != 0 && ds.Tables[0].Rows.Count == 1)
            dr = ds.Tables[0].Rows[0];

        return dr;
    }
    public static DataSet dataReader_EQ_TransNo(Boolean b, string EQ_TransNo)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_TransNo", EQ_TransNo));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblEQans WHERE EQ_TransNo=@EQ_TransNo AND tblEQans.ModiState<>'D' order by EA_OrderNo";
        else
            cmdtxt = "SELECT * FROM tblEQans";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet dataReader_EQ_TransNo_random(Boolean b, string EQ_TransNo)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_TransNo", EQ_TransNo));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblEQans WHERE EQ_TransNo=@EQ_TransNo AND tblEQans.ModiState<>'D' order by newid()";
        else
            cmdtxt = "SELECT * FROM tblEQans";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }
    public static DataSet dataReader_EQ_TransNo(Boolean b, string EQ_TransNo, string EA_IsCorrect)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EQ_TransNo", EQ_TransNo));
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@EA_IsCorrect", EA_IsCorrect));
        string cmdtxt = "";
        if (b)
            cmdtxt = "SELECT * FROM tblEQans WHERE EQ_TransNo=@EQ_TransNo AND EA_IsCorrect=@EA_IsCorrect AND tblEQans.ModiState<>'D' order by EA_OrderNo";
        else
            cmdtxt = "SELECT * FROM tblEQans";
        emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        return emds;
    }

    public static DataSet DataReader(string EM_Type, string EM_Name, string EM_Explan, string EM_File, int startRowIndex, int maximumRows)
    {
        DataSet emds = new DataSet();
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        string where = whereString(EM_Type, EM_Name, EM_Explan, EM_File, lSqlParas);
        string sort = "Server_ModiDate desc";

        StringBuilder Vcolumns = new StringBuilder();
        Vcolumns.Append("* ");

        StringBuilder columns = new StringBuilder();
        columns.Append("* ");

        string cmdtxt = MainControls.sqlSelect(Vcolumns.ToString(), columns.ToString(), "tblEQans", where, sort, startRowIndex, maximumRows);
        if (lSqlParas.Count == 0)
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt);
        else
            emds = SqlAccess.SqlcommandExecute("0", cmdtxt, lSqlParas.ToArray());
        DataProcess(emds);
        return emds;
    }
    #endregion

    #region 讀取資料筆數
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCount(string EM_Type, string EM_Name, string EM_Explan, string EM_File)
    {
        //參數
        List<SqlParameter> lSqlParas = new List<SqlParameter>();
        StringBuilder sbSql = new StringBuilder("SELECT COUNT(*) FROM tblEQans");
        string where = whereString(EM_Type, EM_Name, EM_Explan, EM_File, lSqlParas);
        sbSql.Append(where);
        if (lSqlParas.Count == 0)
            return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString());

        return (int)SqlAccess.SqlcommandExecuteScalar("0", sbSql.ToString(), lSqlParas.ToArray());
    }
    #endregion

    #region 查詢條件處理
    public static String whereString(string EM_Type, string EM_Name, string EM_Explan, string EM_File, List<SqlParameter> lSqlParas)
    {
        StringBuilder sbWhere = new StringBuilder();

        if (EM_Type.Length > 0)
        {
            sbWhere.Append(" EM_Type LIKE '%'+@EM_Type+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EM_Type", EM_Type));
        }
        if (EM_Name.Length > 0)
        {
            sbWhere.Append(" EM_Name LIKE '%'+@EM_Name+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EM_Name", EM_Name));
        }
        if (EM_Explan.Length > 0)
        {
            sbWhere.Append(" EM_Explan LIKE '%'+@EM_Explan+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EM_Explan", EM_Explan));
        }
        if (EM_File.Length > 0)
        {
            sbWhere.Append(" EM_File LIKE '%'+@EM_File+'%' AND ");
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@EM_File", EM_File));
        }

        sbWhere.Append(" ModiState<>@ModiState AND ");
        lSqlParas.Add(SqlAccess.CreateSqlParameter("@ModiState", "D"));

        if (sbWhere.Length > 0)
        {
            sbWhere = sbWhere.Remove(sbWhere.Length - 5, 5);
            sbWhere.Insert(0, " WHERE ");
        }
        return sbWhere.ToString();
    }
    #endregion

    #region 處理動態欄位
    public static void DataProcess(DataSet ds)
    {
        DataTable dt = ds.Tables[0];
        dt.Columns.Add("EA_File_link", typeof(string));
        foreach (DataRow dr in dt.Rows)
        {
            dr["EA_File_link"] = string.Format("<a href=\"/uploads/ETM/{0}\" target=\"_blank\">{0}</a>", dr["EA_File"].ToString());
        }
    }
    #endregion

    #region 功能表資料行
    public static void DataColumn(GridView mgv)
    {
        mgv.Columns.Add(DataModel.CreateButtonField(ButtonType.Button, "選取", "Select", 80));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_Type", "教材類別", 100, "Center"));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_Name", "教材名稱", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_Explan", "說明", 200, "Left"));
        mgv.Columns.Add(DataModel.CreateBoundField("EA_File_link", "檔案名稱", false, 200, "Left"));

        //隱藏欄位
        DataTable dt = GetSchema();
        foreach (DataColumn dc in dt.Columns)
        {
            BoundField bf = DataModel.CreateBoundField(dc.ColumnName, "", 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 新增資料
    /// <summary>
    /// 新增資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Append(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Insert);
        return SqlAccess.SqlCommandAppend("0", ds);
    }
    #endregion

    #region 修改資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Update(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Update);
        return SqlAccess.SqlCommandUpdate("0", ds);
    }
    #endregion

    #region 刪除資料
    /// <summary>
    /// 修改資料
    /// </summary>
    /// <param name="emNo">員工代碼</param>
    /// <param name="progId">程式名稱</param>
    /// <param name="ds">異動資料</param>
    /// <returns></returns>
    public static RServiceProvider Delete(string emNo, string progId, DataSet ds)
    {
        //員工維護系統資料記錄
        DataemDaily.emDailyInsert("0", emNo, progId, ds, DataemDaily.MaintainStatus.Delete);
        return SqlAccess.SqlCommandDelete("0", ds);
    }
    #endregion

    #region 取得資料結構
    public static DataTable GetSchema()
    {
        DataSet emds = new DataSet();
        string cmdtxt = "SELECT * FROM tblEQans where 1=0";
        emds = SqlAccess.SqlcommandFillSchema("0", cmdtxt);
        emds.Tables[0].TableName = "tblEQans";
        return emds.Tables[0].Clone();
    }
    #endregion

    #region 選單資料讀取成ListItems
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mossRCBL">DropDownList</param>
    /// <param name="all">增加全部選項</param>
    public static void DDL_ET_TransNo(DropDownList DDL, bool all, string allString, string key)
    {
        DDL.Items.Clear();
        DataSet emds = baseDataReader(true, key);
        if (MainControls.checkDataSet(emds))
        {
            if (all)
                DDL.Items.Add(new ListItem(allString, ""));
            foreach (DataRow dr in emds.Tables[0].Rows)
                DDL.Items.Add(new ListItem(string.Format("{0}", dr["EM_Name"].ToString()), dr["EM_TransNo"].ToString()));
        }
    }
    #endregion





}