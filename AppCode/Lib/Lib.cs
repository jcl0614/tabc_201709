﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;
using CuteWebUI;
using System.Text.RegularExpressions;

/// <summary>
/// Lib 的摘要描述
/// </summary>
public class Lib
{
    #region 欄位內容處理 FdVP(string fvp)
    /// <summary>
    /// 欄位內容過濾'%()javascript/大小於符號...等
    /// </summary>
    /// <param name="fvp">欄位內容</param>
    /// <returns>string</returns>
    public static string FdVP(string fvp)
    {
        fvp = fvp.Replace("'", "").Replace("&#39;", "").Replace("&#173;", "").Replace("javascript", "").Replace("%", "").Replace("<", "").Replace(">", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace("&", "");
        return (fvp.Length == 0) ? "" : fvp;
    }

    /// <summary>
    /// 去除 HTML 標籤，可自訂合法標籤加以保留
    /// </summary>
    /// <param name="html">來源字串</param>
    /// <param name="reservedTagPool">合法標籤集</param>
    /// <returns></returns>
    public static string HtmlTags(string html, string[] reservedTagPool)
    {
        string[] reservedTagPool_ = {
            "section","nav","article","aside","h1","h2","h3","h4","h5","h6","hgroup","header","footer","address",
            "p","hr","br","pre","blockquote","ol","ul","li","dl","dt","dd","figure","figcaption","div",
            "a","em","strong","small","cite","q","dfn","abbr","time","code","var","samp","kbd","sub","sup","i","s","u","b","mark","ruby","rt","rp","bdo","span",
            "ins","del",
            "img","param","video","audio","source","canvas","map","area",
            "table","caption","colgroup","col","tbody","thread","tfoot","tr","td","th",
            "details","summary","command","menu",
            //"form","fieldset","legend","label","button","select","datalist","optgroup","option","textarea","keygen","output","progress","meter",
            "iframe",
            "style"
                                    };

        if (reservedTagPool == null || reservedTagPool.Length == 0)
            reservedTagPool = reservedTagPool_;

        return Regex.Replace(
            html,
            String.Format("<(?!({0}\\s*)|/({1}\\s*)).*?>", string.Join("\\s*)|(", reservedTagPool), string.Join("\\s*)|/(", reservedTagPool)),
            String.Empty);
    }
    public static string HtmlTags(string html)
    {
        string[] reservedTagPool_ = {
            "section","nav","article","aside","h1","h2","h3","h4","h5","h6","hgroup","header","footer","address",
            "p","hr","br","pre","blockquote","ol","ul","li","dl","dt","dd","figure","figcaption","div",
            "a","em","strong","small","cite","q","dfn","abbr","time","code","var","samp","kbd","sub","sup","i","s","u","b","mark","ruby","rt","rp","bdo","span",
            "ins","del",
            "img","param","video","audio","source","canvas","map","area",
            "table","caption","colgroup","col","tbody","thread","tfoot","tr","td","th",
            "details","summary","command","menu",
            //"form","fieldset","legend","label","button","select","datalist","optgroup","option","textarea","keygen","output","progress","meter",
            "iframe",
            "style"
                                    };
        string[] reservedTagPool = reservedTagPool_;

        return Regex.Replace(
            html,
            String.Format("<(?!({0}\\s*)|/({1}\\s*)).*?>", string.Join("\\s*)|(", reservedTagPool), string.Join("\\s*)|/(", reservedTagPool)),
            String.Empty);
    }

    /// <summary>
    /// 欄位內容過濾'%()javascript/大小於符號...等
    /// </summary>
    /// <param name="fvp">欄位內容</param>
    /// <param name="ext">不過濾內容(/、&、%、大小於符號、()、/大小於符號</param>
    /// <returns></returns>
    public static string FdVP(string fvp, string ext)
    {
        switch (ext)
        {
            case "/":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("<", "").Replace(">", "").Replace("(", "").Replace(")", "").Replace("&", "");
                break;
            case "&":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("<", "").Replace(">", "").Replace("(", "").Replace(")", "").Replace("/", "");
                break;
            case "%":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("<", "").Replace(">", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace("&", "");
                break;
            case "<>":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("(", "").Replace(")", "").Replace("/", "").Replace("&", "");
                break;
            case "()":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("<", "").Replace(">", "").Replace("/", "").Replace("&", "");
                break;
            case "/<>":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("(", "").Replace(")", "").Replace("&", "");
                break;
            case "/%&":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("(", "").Replace(")", "");
                break;
            case "/&":
                fvp = fvp.Replace("'", "").Replace("javascript", "").Replace("%", "").Replace("<", "").Replace(">", "").Replace("(", "").Replace(")", "");
                break;
        }
        return (fvp.Length == 0) ? "" : fvp;
    }

    public static string RequestValueChange(string fvp)
    {
        fvp.Replace("'", "").Replace("javascript'", "").Replace("script", "").Replace("<", "").Replace(">", "").Replace("&lt;", "").Replace("&gt;", "").Replace("&", "").Replace("src=", "").Replace(";", "").Replace("http", "").Replace("/", "");
        return (fvp.Length == 0) ? "" : fvp;
    }


    public static string AsciiChangeCode(string ACC)
    {
        string mCode = "TvMbcdOFGtPzHIyhRUwiXBoJKxEuVfgZjklDmnLYSsAaQeNpqrCW";
        string mNewCode = "";
        char[] mACC = ACC.ToCharArray();
        int btoi = 0;
        foreach (char mChar in mACC)
        {
            btoi = Convert.ToByte(mChar);
            if (btoi >= 65)
                mNewCode += mCode.Substring(btoi <= 90 ? (btoi - 65) : (btoi - 97) + 26, 1);
            else
                mNewCode += mChar;

        }
        return mNewCode;
    }

    public static string UnAsciiChangeCode(string ACC)
    {
        string mCode = "TvMbcdOFGtPzHIyhRUwiXBoJKxEuVfgZjklDmnLYSsAaQeNpqrCW";
        string mNewCode = "";
        char[] mACC = ACC.ToCharArray();
        int btoi = 0;
        foreach (char mChar in mACC)
        {
            btoi = mCode.IndexOf(mChar);
            if (btoi >= 0)
            {
                btoi = btoi <= 25 ? btoi + 65 : btoi - 26 + 97;
                mNewCode += Convert.ToChar(btoi);
            }
            else
                mNewCode += mChar;
        }
        return mNewCode;
    }
    #endregion

    #region 使用者權限 UserLimit(string limitRange , string limitString)
    public static bool UserLimit(string limitRange, string limitString)
    {
        bool Limit_bool = false;
        if (limitRange.LastIndexOf(limitString.ToString()) >= 0)
            Limit_bool = true;
        return Limit_bool;
    }
    #endregion

    #region 全形與半形轉換
    public static string changeBToS(string m_bs)
    {
        m_bs = m_bs.Replace("０", "0");
        m_bs = m_bs.Replace("１", "1");
        m_bs = m_bs.Replace("２", "2");
        m_bs = m_bs.Replace("３", "3");
        m_bs = m_bs.Replace("４", "4");
        m_bs = m_bs.Replace("５", "5");
        m_bs = m_bs.Replace("６", "6");
        m_bs = m_bs.Replace("７", "7");
        m_bs = m_bs.Replace("８", "8");
        m_bs = m_bs.Replace("９", "9");
        m_bs = m_bs.Replace("ａ", "a");
        m_bs = m_bs.Replace("ｂ", "b");
        m_bs = m_bs.Replace("ｃ", "c");
        m_bs = m_bs.Replace("ｄ", "d");
        m_bs = m_bs.Replace("ｅ", "e");
        m_bs = m_bs.Replace("ｆ", "f");
        m_bs = m_bs.Replace("ｇ", "g");
        m_bs = m_bs.Replace("ｈ", "h");
        m_bs = m_bs.Replace("ｉ", "i");
        m_bs = m_bs.Replace("ｊ", "j");
        m_bs = m_bs.Replace("ｋ", "k");
        m_bs = m_bs.Replace("ｌ", "l");
        m_bs = m_bs.Replace("ｍ", "m");
        m_bs = m_bs.Replace("ｎ", "n");
        m_bs = m_bs.Replace("ｏ", "o");
        m_bs = m_bs.Replace("ｐ", "p");
        m_bs = m_bs.Replace("ｑ", "q");
        m_bs = m_bs.Replace("ｒ", "r");
        m_bs = m_bs.Replace("ｓ", "s");
        m_bs = m_bs.Replace("ｔ", "t");
        m_bs = m_bs.Replace("ｕ", "u");
        m_bs = m_bs.Replace("ｖ", "v");
        m_bs = m_bs.Replace("ｗ", "w");
        m_bs = m_bs.Replace("ｘ", "x");
        m_bs = m_bs.Replace("ｙ", "y");
        m_bs = m_bs.Replace("ｚ", "z");
        m_bs = m_bs.Replace("Ａ", "A");
        m_bs = m_bs.Replace("Ｂ", "B");
        m_bs = m_bs.Replace("Ｃ", "C");
        m_bs = m_bs.Replace("Ｄ", "D");
        m_bs = m_bs.Replace("Ｅ", "E");
        m_bs = m_bs.Replace("Ｆ", "F");
        m_bs = m_bs.Replace("Ｇ", "G");
        m_bs = m_bs.Replace("Ｈ", "H");
        m_bs = m_bs.Replace("Ｉ", "I");
        m_bs = m_bs.Replace("Ｊ", "J");
        m_bs = m_bs.Replace("Ｋ", "K");
        m_bs = m_bs.Replace("Ｌ", "L");
        m_bs = m_bs.Replace("Ｍ", "M");
        m_bs = m_bs.Replace("Ｎ", "N");
        m_bs = m_bs.Replace("Ｏ", "O");
        m_bs = m_bs.Replace("Ｐ", "P");
        m_bs = m_bs.Replace("Ｑ", "Q");
        m_bs = m_bs.Replace("Ｒ", "R");
        m_bs = m_bs.Replace("Ｓ", "S");
        m_bs = m_bs.Replace("Ｔ", "T");
        m_bs = m_bs.Replace("Ｕ", "U");
        m_bs = m_bs.Replace("Ｖ", "V");
        m_bs = m_bs.Replace("Ｗ", "W");
        m_bs = m_bs.Replace("Ｘ", "X");
        m_bs = m_bs.Replace("Ｙ", "Y");
        m_bs = m_bs.Replace("Ｚ", "Z");

        m_bs = m_bs.Replace("　", "");
        m_bs = m_bs.Replace("－", "");
        m_bs = m_bs.Replace("-", "");
        m_bs = m_bs.Replace(" ", "");
        return m_bs;
    }
    #endregion

    #region 自動產生縮圖
    /// <summary>
    /// 產生縮略圖
    /// </summary>
    /// <param name="originalImagePath">源圖路徑（物理路徑）</param>
    /// <param name="thumbnailPath">縮略圖路徑（物理路徑）</param>
    /// <param name="width">縮略圖寬度</param>
    /// <param name="height">縮略圖高度</param>
    /// <param name="mode">產生縮略圖的方式HW指定高寬縮放,W指定寬，寬按比例,H指定高，寬按比例,Cut指定高寬裁減,再放大到原圖,空白自訂剪裁</param>    
    public static string MakeImageWH(Stream originalImagePath, string thumbnailPath, int? width, int? height, string mode)
    {
        System.Drawing.Image originalImage = System.Drawing.Image.FromStream(originalImagePath);

        int towidth = 0;
        int toheight = 0;

        int.TryParse(width.ToString(), out towidth);
        int.TryParse(height.ToString(), out toheight);

        int x = 0;
        int y = 0;
        int ow = originalImage.Width;
        int oh = originalImage.Height;

        switch (mode)
        {
            case "HW"://指定高寬縮放（可能變形）                
                break;
            case "W"://指定寬，高按比例                    
                toheight = originalImage.Height * (int)width / originalImage.Width;
                break;
            case "H"://指定高，寬按比例
                towidth = originalImage.Width * (int)height / originalImage.Height;
                break;
            case "Cut"://指定高寬裁減,再放大到原圖（不變形）                
                if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                {
                    oh = originalImage.Height;
                    ow = originalImage.Height * towidth / toheight;
                    y = 0;
                    x = (originalImage.Width - ow) / 2;
                }
                else
                {
                    ow = originalImage.Width;
                    oh = originalImage.Width * (int)height / towidth;
                    x = 0;
                    y = (originalImage.Height - oh) / 2;
                }
                break;
            default:
                toheight = oh;
                towidth = ow;
                break;
        }

        //新建一個bmp圖片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

        //新建一個畫板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

        //設定高品質插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

        //設定高品質,低速度呈現平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //清空畫布並以透明背景色填充
        g.Clear(System.Drawing.Color.Transparent);

        //在指定位置並且按指定大小繪制原圖片的指定部分
        g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
            new System.Drawing.Rectangle(x, y, ow, oh),
            System.Drawing.GraphicsUnit.Pixel);
        originalImage.Dispose();
        try
        {

            //圖片壓縮
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L); //設定100品質
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            if (originalImagePath.ToString().EndsWith(".jpg") == true || originalImagePath.ToString().EndsWith(".JPG") == true || originalImagePath.ToString().EndsWith(".jpeg") == true || originalImagePath.ToString().EndsWith(".JPEG") == true)
                ici = GetEncoderInfo("image/jpeg");
            if (originalImagePath.ToString().EndsWith(".png") == true || originalImagePath.ToString().EndsWith(".PNG") == true) // 解析度受影響
                ici = GetEncoderInfo("image/png");
            if (originalImagePath.ToString().EndsWith(".gif") == true || originalImagePath.ToString().EndsWith(".GIF") == true) // 解析度受影響
                ici = GetEncoderInfo("image/gif");

            //以Png格式保存縮略圖
            bitmap.Save(thumbnailPath, ici, eps);

            return "上傳成功!";
        }
        catch
        {
            return "上傳失敗!";
        }
        finally
        {
            bitmap.Dispose();
            g.Dispose();
        }
    }
    public static string MakeImageWH(string originalImagePath, string thumbnailPath, int? width, int? height, string mode, long compress)
    {
        System.Drawing.Image originalImage = System.Drawing.Image.FromFile(originalImagePath);

        int towidth = 0;
        int toheight = 0;

        int.TryParse(width.ToString(), out towidth);
        int.TryParse(height.ToString(), out toheight);

        int x = 0;
        int y = 0;
        int ow = originalImage.Width;
        int oh = originalImage.Height;

        switch (mode)
        {
            case "HW"://指定高寬縮放（可能變形）                
                break;
            case "W"://指定寬，高按比例                    
                toheight = originalImage.Height * (int)width / originalImage.Width;
                break;
            case "H"://指定高，寬按比例
                towidth = originalImage.Width * (int)height / originalImage.Height;
                break;
            case "Cut"://指定高寬裁減,再放大到原圖（不變形）                
                if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                {
                    oh = originalImage.Height;
                    ow = originalImage.Height * towidth / toheight;
                    y = 0;
                    x = (originalImage.Width - ow) / 2;
                }
                else
                {
                    ow = originalImage.Width;
                    oh = originalImage.Width * (int)height / towidth;
                    x = 0;
                    y = (originalImage.Height - oh) / 2;
                }
                break;
            default:
                toheight = oh;
                towidth = ow;
                break;
        }

        //新建一個bmp圖片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

        //新建一個畫板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

        //設定高品質插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

        //設定高品質,低速度呈現平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //清空畫布並以透明背景色填充
        g.Clear(System.Drawing.Color.Transparent);

        //在指定位置並且按指定大小繪制原圖片的指定部分
        g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
            new System.Drawing.Rectangle(x, y, ow, oh),
            System.Drawing.GraphicsUnit.Pixel);
        originalImage.Dispose();
        try
        {

            //圖片壓縮
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compress); //設定100品質
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            if (originalImagePath.ToString().EndsWith(".jpg") == true || originalImagePath.ToString().EndsWith(".JPG") == true || originalImagePath.ToString().EndsWith(".jpeg") == true || originalImagePath.ToString().EndsWith(".JPEG") == true)
                ici = GetEncoderInfo("image/jpeg");
            if (originalImagePath.ToString().EndsWith(".png") == true || originalImagePath.ToString().EndsWith(".PNG") == true) // 解析度受影響
                ici = GetEncoderInfo("image/png");
            if (originalImagePath.ToString().EndsWith(".gif") == true || originalImagePath.ToString().EndsWith(".GIF") == true) // 解析度受影響
                ici = GetEncoderInfo("image/gif");

            //以Png格式保存縮略圖
            bitmap.Save(thumbnailPath, ici, eps);

            return "上傳成功!";
        }
        catch (Exception ex)
        {
            return "上傳失敗!";
        }
        finally
        {
            bitmap.Dispose();
            g.Dispose();
        }
    }
    #endregion

    #region 自動產生縮圖
    /// <summary>
    /// 產生縮略圖
    /// </summary>
    /// <param name="originalImagePath">源圖路徑（物理路徑）</param>
    /// <param name="thumbnailPath">縮略圖路徑（物理路徑）</param>
    /// <param name="width">縮略圖寬度邊界</param>
    /// <param name="height">縮略圖高度邊界</param>
    /// <param name="title">印字</param>
    /// <param name="LOGO">LOGO</param>
    /// <param name="Example"> MakeImageWH(FileUpload1.PostedFile.FileName, Server.MapPath("~/testImg/" + FileUpload1.FileName).ToString(), 100, 100, "印字"); </param>
    public static string MakeImageTitleWH(Stream originalImagePath, string thumbnailPath, int width, int height, string title, string logo)
    {
        System.Drawing.Image Img = System.Drawing.Image.FromStream(originalImagePath);

        int towidth = Img.Width;
        int toheight = Img.Height;

        int x = 0;
        int y = 0;

        int w = Img.Width;
        int h = Img.Height;

        if (width >= w && height >= h)
        {
            towidth = w;
            toheight = h;
        }
        else
        {
            if (w == h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w > h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    if ((float)width / height <= (float)w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if ((float)width / height > (float)w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w < h)
            {
                if (width == height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    if ((float)width / height <= (float)w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if ((float)width / height > (float)w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
            }
        }

        //新建一個bmp圖片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

        //新建一個畫板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

        //設定高品質插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

        //設定高品質,低速度呈現平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //設定畫布的底色~透明或白色之類
        g.Clear(System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255))));

        //在指定位置並且按指定大小繪制原圖片的指定部分
        g.DrawImage(Img, new System.Drawing.Rectangle(0, 0, towidth, toheight),
            new System.Drawing.Rectangle(x, y, w, h),
            System.Drawing.GraphicsUnit.Pixel);
        Img.Dispose();
        try
        {
            //印字
            if (title != "")
                g.DrawString(title, new Font("Courier New", 10), new SolidBrush(Color.FromArgb(128, 0, 0, 0)), 0, toheight - 20); //半透明字體
            //LOGO
            if (logo != "")
            {
                int logo_Max = 0;
                if (towidth < toheight)
                    logo_Max = int.Parse((toheight / 3).ToString());
                if (towidth >= toheight)
                    logo_Max = int.Parse((towidth / 3).ToString());
                System.Drawing.Image logo_Img = System.Drawing.Image.FromFile(logo);

                int logo_towidth = logo_Max;
                int logo_toheight = logo_Max;

                int logo_w = logo_Img.Width;
                int logo_h = logo_Img.Height;

                if (logo_Max >= logo_w && logo_Max >= logo_h)
                {
                    logo_towidth = logo_w;
                    logo_toheight = logo_h;
                }
                else
                {
                    if (logo_w >= logo_h)
                    {
                        logo_towidth = logo_Max;
                        logo_toheight = int.Parse((logo_Max * logo_h / logo_w).ToString());
                    }
                    if (logo_w < logo_h)
                    {
                        logo_toheight = logo_Max;
                        logo_towidth = int.Parse((height * logo_w / logo_h).ToString());
                    }
                }
                g.DrawImage(logo_Img, new System.Drawing.Rectangle(towidth - logo_towidth, toheight - logo_toheight, logo_towidth, logo_toheight),
                new System.Drawing.Rectangle(0, 0, logo_w, logo_h),
                System.Drawing.GraphicsUnit.Pixel);
            }

            //圖片壓縮
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L); //設定100品質
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            if (originalImagePath.ToString().EndsWith(".jpg") == true || originalImagePath.ToString().EndsWith(".JPG") == true || originalImagePath.ToString().EndsWith(".jpeg") == true || originalImagePath.ToString().EndsWith(".JPEG") == true)
                ici = GetEncoderInfo("image/jpeg"); //若是JPEG檔再進行壓縮
            if (originalImagePath.ToString().EndsWith(".png") == true || originalImagePath.ToString().EndsWith(".PNG") == true) // 解析度受影響
                ici = GetEncoderInfo("image/png");
            if (originalImagePath.ToString().EndsWith(".gif") == true || originalImagePath.ToString().EndsWith(".GIF") == true) // 解析度受影響
                ici = GetEncoderInfo("image/gif");

            //以Png格式保存縮略圖
            bitmap.Save(thumbnailPath, ici, eps);
            return "上傳成功!";
        }
        catch (Exception ex)
        {
            return "上傳失敗!";
        }
        finally
        {
            bitmap.Dispose();
            g.Dispose();
        }
    }
    public static string MakeImageTitleWH(Stream originalImagePath, string thumbnailPath, int width, int height, string title, string logo, long compress)
    {
        System.Drawing.Image Img = System.Drawing.Image.FromStream(originalImagePath);

        int towidth = width;
        int toheight = height;

        int x = 0;
        int y = 0;

        int w = Img.Width;
        int h = Img.Height;

        if (width >= w && height >= h)
        {
            towidth = w;
            toheight = h;
        }
        else
        {
            if (w == h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w > h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    if ((float)width / height <= (float)w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if ((float)width / height > (float)w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w < h)
            {
                if (width == height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    if ((float)width / height <= (float)w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if ((float)width / height > (float)w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
            }
        }

        //新建一個bmp圖片
        System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

        //新建一個畫板
        System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

        //設定高品質插值法
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

        //設定高品質,低速度呈現平滑程度
        g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

        //設定畫布的底色~透明或白色之類
        g.Clear(System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(255))));

        //在指定位置並且按指定大小繪制原圖片的指定部分
        g.DrawImage(Img, new System.Drawing.Rectangle(0, 0, towidth, toheight),
            new System.Drawing.Rectangle(x, y, w, h),
            System.Drawing.GraphicsUnit.Pixel);
        Img.Dispose();
        try
        {
            //印字
            if (title != "")
                g.DrawString(title, new Font("Courier New", 10), new SolidBrush(Color.FromArgb(128, 0, 0, 0)), 0, toheight - 20); //半透明字體
            //LOGO
            if (logo != "")
            {
                int logo_Max = 0;
                if (towidth < toheight)
                    logo_Max = int.Parse((toheight / 3).ToString());
                if (towidth >= toheight)
                    logo_Max = int.Parse((towidth / 3).ToString());
                System.Drawing.Image logo_Img = System.Drawing.Image.FromFile(logo);

                int logo_towidth = logo_Max;
                int logo_toheight = logo_Max;

                int logo_w = logo_Img.Width;
                int logo_h = logo_Img.Height;

                if (logo_Max >= logo_w && logo_Max >= logo_h)
                {
                    logo_towidth = logo_w;
                    logo_toheight = logo_h;
                }
                else
                {
                    if (logo_w >= logo_h)
                    {
                        logo_towidth = logo_Max;
                        logo_toheight = int.Parse((logo_Max * logo_h / logo_w).ToString());
                    }
                    if (logo_w < logo_h)
                    {
                        logo_toheight = logo_Max;
                        logo_towidth = int.Parse((height * logo_w / logo_h).ToString());
                    }
                }
                g.DrawImage(logo_Img, new System.Drawing.Rectangle(towidth - logo_towidth, toheight - logo_toheight, logo_towidth, logo_toheight),
                new System.Drawing.Rectangle(0, 0, logo_w, logo_h),
                System.Drawing.GraphicsUnit.Pixel);
            }

            //圖片壓縮
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compress); //設定100品質
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");

            if (originalImagePath.ToString().EndsWith(".jpg") == true || originalImagePath.ToString().EndsWith(".JPG") == true || originalImagePath.ToString().EndsWith(".jpeg") == true || originalImagePath.ToString().EndsWith(".JPEG") == true)
                ici = GetEncoderInfo("image/jpeg"); //若是JPEG檔再進行壓縮
            if (originalImagePath.ToString().EndsWith(".png") == true || originalImagePath.ToString().EndsWith(".PNG") == true) // 解析度受影響
                ici = GetEncoderInfo("image/png");
            if (originalImagePath.ToString().EndsWith(".gif") == true || originalImagePath.ToString().EndsWith(".GIF") == true) // 解析度受影響
                ici = GetEncoderInfo("image/gif");

            //以Png格式保存縮略圖
            bitmap.Save(thumbnailPath, ici, eps);
            return "上傳成功!";
        }
        catch (Exception ex)
        {
            return "上傳失敗!";
        }
        finally
        {
            bitmap.Dispose();
            g.Dispose();
        }
    }
    public static string ReSize(UploaderEventArgs args, int MaxWidth, int MaxHeight)
    {
        try
        {
            System.Drawing.Bitmap img;
            using (Stream stream = args.OpenStream())
            {
                img = new System.Drawing.Bitmap(stream);
            }
            using (img)
            {
                if (img.Width > MaxWidth || img.Height > MaxHeight)
                {
                    int towidth = MaxWidth;
                    int toheight = MaxHeight;

                    int w = img.Width;
                    int h = img.Height;

                    if (MaxWidth >= w && MaxHeight >= h)
                    {
                        towidth = w;
                        toheight = h;
                    }
                    else
                    {
                        if (w == h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w > h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w < h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                        }
                    }

                    try
                    {
                        using (System.Drawing.Image thumb = new System.Drawing.Bitmap(towidth, toheight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                        {
                            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(thumb))
                            {
                                //設定高品質插值法
                                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                                //設定高品質,低速度呈現平滑程度
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                //設定畫布的底色~透明或白色之類
                                if (args.FileName.Split('.')[1] == "png")
                                    g.Clear(System.Drawing.Color.Transparent);
                                else
                                    g.Clear(System.Drawing.Color.White);

                                g.DrawImage(img
                                    , new System.Drawing.Rectangle(0, 0, towidth, toheight)
                                    , new System.Drawing.Rectangle(0, 0, w, h)
                                    , System.Drawing.GraphicsUnit.Pixel);
                            }
                            EncoderParameters eps = new EncoderParameters(1);
                            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L); //設定100品質
                            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "jpg" || args.FileName.Split('.')[1] == "jpeg")
                                ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "png")
                                ici = GetEncoderInfo("image/png");
                            if (args.FileName.Split('.')[1] == "gif")
                                ici = GetEncoderInfo("image/gif");
                            thumb.Save(args.GetTempFilePath(), ici, eps);
                            return string.Format("圖片已壓縮({0}*{1})並上載 (尚未儲存)", towidth, toheight);
                        }

                    }
                    catch (Exception x)
                    {
                        args.Delete();
                        return w + ":" + h + ":" + x.ToString();
                    }
                }
                else
                    return "圖片已上載 (尚未儲存)";
            }

        }
        catch (Exception x)
        {
            args.Delete();
            return x.ToString();

        }
    }
    public static string ReSize(UploaderEventArgs args, int MaxWidth, int MaxHeight, long compress)
    {
        try
        {
            System.Drawing.Bitmap img;
            using (Stream stream = args.OpenStream())
            {
                img = new System.Drawing.Bitmap(stream);
            }
            using (img)
            {
                if (img.Width > MaxWidth || img.Height > MaxHeight)
                {
                    int towidth = MaxWidth;
                    int toheight = MaxHeight;

                    int w = img.Width;
                    int h = img.Height;

                    if (MaxWidth >= w && MaxHeight >= h)
                    {
                        towidth = w;
                        toheight = h;
                    }
                    else
                    {
                        if (w == h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w > h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w < h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                        }
                    }

                    try
                    {
                        using (System.Drawing.Image thumb = new System.Drawing.Bitmap(towidth, toheight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                        {
                            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(thumb))
                            {
                                //設定高品質插值法
                                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                                //設定高品質,低速度呈現平滑程度
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                //設定畫布的底色~透明或白色之類
                                if (args.FileName.Split('.')[1] == "png")
                                    g.Clear(System.Drawing.Color.Transparent);
                                else
                                    g.Clear(System.Drawing.Color.White);

                                g.DrawImage(img
                                    , new System.Drawing.Rectangle(0, 0, towidth, toheight)
                                    , new System.Drawing.Rectangle(0, 0, w, h)
                                    , System.Drawing.GraphicsUnit.Pixel);
                            }
                            EncoderParameters eps = new EncoderParameters(1);
                            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compress); //設定100品質
                            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "jpg" || args.FileName.Split('.')[1] == "jpeg")
                                ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "png")
                                ici = GetEncoderInfo("image/png");
                            if (args.FileName.Split('.')[1] == "gif")
                                ici = GetEncoderInfo("image/gif");
                            thumb.Save(args.GetTempFilePath(), ici, eps);
                            return string.Format("圖片已壓縮({0}*{1})並上載 (尚未儲存)", towidth, toheight);
                        }

                    }
                    catch (Exception x)
                    {
                        args.Delete();
                        return w + ":" + h + ":" + x.ToString();
                    }
                }
                else
                    return "圖片已上載 (尚未儲存)";
            }

        }
        catch (Exception x)
        {
            args.Delete();
            return x.ToString();

        }
    }
    public static string ReSize(AttachmentItem args, int MaxWidth, int MaxHeight)
    {
        try
        {
            System.Drawing.Bitmap img;
            using (Stream stream = args.OpenStream())
            {
                img = new System.Drawing.Bitmap(stream);
            }
            using (img)
            {
                if (img.Width > MaxWidth || img.Height > MaxHeight)
                {
                    int towidth = MaxWidth;
                    int toheight = MaxHeight;

                    int w = img.Width;
                    int h = img.Height;

                    if (MaxWidth >= w && MaxHeight >= h)
                    {
                        towidth = w;
                        toheight = h;
                    }
                    else
                    {
                        if (w == h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w > h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w < h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                        }
                    }

                    try
                    {
                        using (System.Drawing.Image thumb = new System.Drawing.Bitmap(towidth, toheight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                        {
                            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(thumb))
                            {
                                //設定高品質插值法
                                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                                //設定高品質,低速度呈現平滑程度
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                //設定畫布的底色~透明或白色之類
                                if (args.FileName.Split('.')[1] == "png")
                                    g.Clear(System.Drawing.Color.Transparent);
                                else
                                    g.Clear(System.Drawing.Color.White);

                                g.DrawImage(img
                                    , new System.Drawing.Rectangle(0, 0, towidth, toheight)
                                    , new System.Drawing.Rectangle(0, 0, w, h)
                                    , System.Drawing.GraphicsUnit.Pixel);
                            }
                            EncoderParameters eps = new EncoderParameters(1);
                            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L); //設定100品質
                            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "jpg" || args.FileName.Split('.')[1] == "jpeg")
                                ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "png")
                                ici = GetEncoderInfo("image/png");
                            if (args.FileName.Split('.')[1] == "gif")
                                ici = GetEncoderInfo("image/gif");
                            thumb.Save(args.GetTempFilePath(), ici, eps);
                            return string.Format("圖片已壓縮({0}*{1})並上載 (尚未儲存)", towidth, toheight);
                        }

                    }
                    catch (Exception x)
                    {
                        args.Delete();
                        return w + ":" + h + ":" + x.ToString();
                    }
                }
                else
                    return "圖片已上載 (尚未儲存)";
            }

        }
        catch (Exception x)
        {
            args.Delete();
            return x.ToString();

        }
    }
    public static string ReSize(AttachmentItem args, int MaxWidth, int MaxHeight, long compress)
    {
        try
        {
            System.Drawing.Bitmap img;
            using (Stream stream = args.OpenStream())
            {
                img = new System.Drawing.Bitmap(stream);
            }
            using (img)
            {
                if (img.Width > MaxWidth || img.Height > MaxHeight)
                {
                    int towidth = MaxWidth;
                    int toheight = MaxHeight;

                    int w = img.Width;
                    int h = img.Height;

                    if (MaxWidth >= w && MaxHeight >= h)
                    {
                        towidth = w;
                        toheight = h;
                    }
                    else
                    {
                        if (w == h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w > h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                towidth = MaxWidth;
                                toheight = int.Parse((MaxWidth * h / w).ToString());
                            }
                        }
                        if (w < h)
                        {
                            if (MaxWidth == MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth > MaxHeight)
                            {
                                toheight = MaxHeight;
                                towidth = int.Parse((MaxHeight * w / h).ToString());
                            }
                            if (MaxWidth < MaxHeight)
                            {
                                if ((float)MaxWidth / MaxHeight <= (float)w / h)
                                {
                                    towidth = MaxWidth;
                                    toheight = int.Parse((MaxWidth * h / w).ToString());
                                }
                                if ((float)MaxWidth / MaxHeight > (float)w / h)
                                {
                                    toheight = MaxHeight;
                                    towidth = int.Parse((MaxHeight * w / h).ToString());
                                }
                            }
                        }
                    }

                    try
                    {
                        using (System.Drawing.Image thumb = new System.Drawing.Bitmap(towidth, toheight, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                        {
                            using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(thumb))
                            {
                                //設定高品質插值法
                                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                                //設定高品質,低速度呈現平滑程度
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                //設定畫布的底色~透明或白色之類
                                if (args.FileName.Split('.')[1] == "png")
                                    g.Clear(System.Drawing.Color.Transparent);
                                else
                                    g.Clear(System.Drawing.Color.White);

                                g.DrawImage(img
                                    , new System.Drawing.Rectangle(0, 0, towidth, toheight)
                                    , new System.Drawing.Rectangle(0, 0, w, h)
                                    , System.Drawing.GraphicsUnit.Pixel);
                            }
                            EncoderParameters eps = new EncoderParameters(1);
                            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compress); //設定100品質
                            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "jpg" || args.FileName.Split('.')[1] == "jpeg")
                                ici = GetEncoderInfo("image/jpeg");
                            if (args.FileName.Split('.')[1] == "png")
                                ici = GetEncoderInfo("image/png");
                            if (args.FileName.Split('.')[1] == "gif")
                                ici = GetEncoderInfo("image/gif");
                            thumb.Save(args.GetTempFilePath(), ici, eps);
                            return string.Format("圖片已壓縮({0}*{1})並上載 (尚未儲存)", towidth, toheight);
                        }

                    }
                    catch (Exception x)
                    {
                        args.Delete();
                        return w + ":" + h + ":" + x.ToString();
                    }
                }
                else
                    return "圖片已上載 (尚未儲存)";
            }

        }
        catch (Exception x)
        {
            args.Delete();
            return x.ToString();

        }
    }
    private static ImageCodecInfo GetEncoderInfo(String mimeType)
    {
        ImageCodecInfo[] encoders;
        encoders = ImageCodecInfo.GetImageEncoders();
        for (int j = 0; j < encoders.Length; ++j)
        {
            if (encoders[j].MimeType == mimeType)
                return encoders[j];
        }
        return null;
    }
    #endregion

    #region 變更照片長寬
    public static void ImgWH(object obj, string originalImagePath, int width, int height)
    {
        System.Drawing.Image Img = System.Drawing.Image.FromFile(originalImagePath);

        int towidth = width;
        int toheight = height;

        int w = Img.Width;
        int h = Img.Height;

        if (width >= w && height >= h)
        {
            towidth = w;
            toheight = h;
        }
        else
        {
            if (w == h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w > h)
            {
                if (width == height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
                if (width > height)
                {
                    if (width / height <= w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if (width / height > w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
                if (width < height)
                {
                    towidth = width;
                    toheight = int.Parse((width * h / w).ToString());
                }
            }
            if (w < h)
            {
                if (width == height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width > height)
                {
                    toheight = height;
                    towidth = int.Parse((height * w / h).ToString());
                }
                if (width < height)
                {
                    if (width / height <= w / h)
                    {
                        towidth = width;
                        toheight = int.Parse((width * h / w).ToString());
                    }
                    if (width / height > w / h)
                    {
                        toheight = height;
                        towidth = int.Parse((height * w / h).ToString());
                    }
                }
            }
        }

        if (obj is System.Web.UI.WebControls.Image)
        {
            ((System.Web.UI.WebControls.Image)obj).Width = towidth;
            ((System.Web.UI.WebControls.Image)obj).Height = toheight;
        }
        else if (obj is ImageButton)
        {
            ((ImageButton)obj).Width = towidth;
            ((ImageButton)obj).Height = toheight;
        }
        Img.Dispose();
    }
    #endregion

    #region MD加密器
    /// <summary>
    /// MD加密器
    /// </summary>
    /// <returns></returns>
    public static string MdEnCode(string cNo)
    {
        string codeno = "flVesWdBMcvxUzAJinZpCFSwKoIgLjyNOhqGXPrEabHQTktuRDmY";
        string codenum = "3296074581";
        string CodeNo = "";

        char[] emc_char = cNo.ToCharArray();
        int md5c = 0;
        int m_mc = 0;
        for (int mc = 0; mc < emc_char.Length; mc++)
        {
            if (mc >= 10)
                m_mc = mc - 10;
            else
                m_mc = mc;
            if (codenum.IndexOf(emc_char[mc].ToString()) >= 0)
            {
                md5c = Convert.ToInt16(emc_char[mc].ToString()) + codenum.IndexOf(m_mc.ToString()) + 1;
                CodeNo += codeno.Substring(md5c, 1);
            }
            else
                CodeNo += emc_char[mc].ToString();
        }
        return CodeNo;

    }
    #endregion

    #region MD解密器
    /// <summary>
    /// MD解密器
    /// </summary>
    /// <returns></returns>
    public static string MdDeCode(string cNo)
    {
        string codeno = "flVesWdBMcvxUzAJinZpCFSwKoIgLjyNOhqGXPrEabHQTktuRDmY";
        string codenum = "3296074581";
        string CodeNo = "";

        char[] emc_char = cNo.ToCharArray();
        int md5c = 0;
        int m_mc = 0;
        for (int mc = 0; mc < emc_char.Length; mc++)
        {
            if (mc >= 10)
                m_mc = mc - 10;
            else
                m_mc = mc;
            if (codeno.IndexOf(emc_char[mc].ToString()) >= 0)
            {
                md5c = codeno.IndexOf(emc_char[mc].ToString()) - (codenum.IndexOf(m_mc.ToString()) + 1);
                CodeNo += md5c;
            }
            else
                CodeNo += emc_char[mc].ToString();
        }
        return CodeNo;
    }
    #endregion

    #region md5加密處理
    public static string GetMD5(string original)
    {
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(original));
        return BitConverter.ToString(b).Replace("-", string.Empty);
    }
    #endregion

    #region  可回復式加密處理器方式一
    /// <summary>
    /// AES 加密器
    /// </summary>
    /// <param name="itstring">待加密內容</param>
    /// <param name="keyString">加密key</param>
    /// <returns></returns>
    public static string GetAesChinese(string itstring, string keyString)
    {
        byte[] binputString = Encoding.UTF8.GetBytes(itstring);
        byte[] saltBytes = Encoding.UTF8.GetBytes(keyString);

        RijndaelManaged aes = new RijndaelManaged();
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(keyString, saltBytes);

        aes.KeySize = 128;
        aes.Key = key.GetBytes(16);

        aes.BlockSize = 128;
        aes.IV = key.GetBytes(16);

        ICryptoTransform encryptor = aes.CreateEncryptor();

        MemoryStream memoryStream = new MemoryStream();
        CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);

        cryptoStream.Write(binputString, 0, binputString.Length);
        cryptoStream.FlushFinalBlock();
        cryptoStream.Close();

        byte[] encryptBytes = memoryStream.ToArray();
        string encryptedData = Convert.ToBase64String(encryptBytes);
        return encryptedData;
    }

    /// <summary>
    /// AES 解密器
    /// </summary>
    /// <param name="itstring">待解密內容</param>
    /// <param name="keyString">解密key</param>
    /// <returns></returns>
    public static string PuchAesChinese(string itstring, string keyString)
    {
        if (itstring.Trim().Length == 0) return "";

        byte[] saltBytes = Encoding.UTF8.GetBytes(keyString);

        RijndaelManaged aes = new RijndaelManaged();
        Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(keyString, saltBytes);

        aes.KeySize = 128;
        aes.Key = key.GetBytes(16);

        aes.BlockSize = 128;
        aes.IV = key.GetBytes(16);


        MemoryStream memoryStream = new MemoryStream();
        ICryptoTransform decryptor = aes.CreateDecryptor();
        byte[] encryptBytes = memoryStream.ToArray();

        encryptBytes = Convert.FromBase64String(itstring);
        CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write);

        cryptoStream.Write(encryptBytes, 0, encryptBytes.Length);
        cryptoStream.Flush();
        cryptoStream.Close();

        byte[] decryptBytes = memoryStream.ToArray();
        string decryptedData = Encoding.UTF8.GetString(decryptBytes);

        return decryptedData;
    }
    #endregion

    #region 取得本日星期
    /// <summary>
    /// 取得本日星期
    /// </summary>
    /// <returns>傳回星期數字0:日、1:一、2:二、3:三、4:四、5:五、6:六</returns>
    public static int ChiDayWeek()
    {
        int cdw = 0;
        switch (System.DateTime.Now.DayOfWeek)
        {
            case DayOfWeek.Sunday:
                cdw = 0;
                break;
            case DayOfWeek.Monday:
                cdw = 1;
                break;
            case DayOfWeek.Tuesday:
                cdw = 2;
                break;
            case DayOfWeek.Wednesday:
                cdw = 3;
                break;
            case DayOfWeek.Thursday:
                cdw = 4;
                break;
            case DayOfWeek.Friday:
                cdw = 5;
                break;
            case DayOfWeek.Saturday:
                cdw = 6;
                break;
        }
        return cdw;
    }
    #endregion

    #region 文字ABC...Z組合 轉換或逆轉 數字(以2進位方式轉換)
    /// <summary>
    /// 文字組合(ABC...Z) 轉成 數值(以2進位方式轉換)
    /// </summary>
    /// <param name="source">文字組合</param>
    /// <returns></returns>
    public static int TextToDigital(string source)
    {
        string ranges = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int result = 0;
        foreach (char c in source)
        {
            int i = ranges.IndexOf(c);
            if (i == -1) continue;
            //以2進位轉十進位
            result += (int)Math.Pow((double)2, (double)i);
        }

        return result;
    }
    /// <summary>
    /// 數值 轉成 文字組合(ABC...Z)
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static string DigitalToText(int source)
    {
        //轉二進位
        string binary = Convert.ToString(source, 2);
        string ranges = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sbresult = new StringBuilder();
        for (int i = binary.Length - 1; i >= 0; i--)
        {
            if (binary[i].ToString() == "1")
                sbresult.Append(ranges[binary.Length - i - 1]);
        }
        return sbresult.ToString();
    }
    #endregion

    #region 判斷是否為行動版瀏覽器
    /// <summary>    
    /// 判斷是否為行動版瀏覽器    
    /// </summary>    
    /// <param name="UserAnget"></param>    
    /// <returns></returns>    
    public static bool isMobile(string UserAnget)
    {
        string[] mobiles = new[]    
        {        
            "midp", "j2me", "avant", "docomo", "novarra", "palmos", "palmsource",        
            "240x320", "opwv", "chtml","pda", "windows ce", "mmp/",        
            "blackberry", "mib/", "symbian", "wireless", "nokia", "hand", "mobi",        
            "phone", "cdm", "up.b", "audio", "sie-", "sec-", "samsung", "htc",        
            "mot-", "mitsu", "sagem", "sony", "alcatel", "lg", "eric", "vx",        
            "NEC", "philips", "mmm", "xx", "panasonic", "sharp", "wap", "sch",        
            "rover", "pocket", "benq", "java", "pt", "pg", "vox", "amoi",        
            "bird", "compal", "kg", "voda","sany", "kdd", "dbt", "sendo",        
            "sgh", "gradi", "jb", "dddi", "moto", "iphone", "android",        
            "iPod", "incognito", "webmate", "dream", "cupcake", "webos",        
            "s8000", "bada", "googlebot-mobile"    
        };
        if (string.IsNullOrEmpty(UserAnget))
            return false;
        foreach (var item in mobiles)
        {
            if (UserAnget.ToLower().IndexOf(item) != -1)
                return true;
        }
        return false;
    }
    #endregion

    #region 金額轉換大寫
    public static string AmountToUpper(string Amount)
    {
        switch (Amount)
        {
            case "0": return "零";
            case "1": return "壹";
            case "2": return "貳";
            case "3": return "參";
            case "4": return "肆";
            case "5": return "伍";
            case "6": return "陸";
            case "7": return "柒";
            case "8": return "捌";
            case "9": return "玖";
            default: return "";
        }
    }
    public static string AmountToUpper(int Amount)
    {
        switch (Amount)
        {
            case 0: return "零";
            case 1: return "壹";
            case 2: return "貳";
            case 3: return "參";
            case 4: return "肆";
            case 5: return "伍";
            case 6: return "陸";
            case 7: return "柒";
            case 8: return "捌";
            case 9: return "玖";
            default: return "";
        }
    }
    #endregion
}
