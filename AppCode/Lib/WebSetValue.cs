﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// WebSetValue 的摘要描述
/// </summary>
[Serializable]
public class WebSetValue
{
	public WebSetValue()
	{
        WebTitle = DataWebSet.dataReader("WebTitle");
        WebUrl = DataWebSet.dataReader("WebUrl");
        WebMapIP = DataWebSet.dataReader("WebMapIP");
        WebMailServer = DataWebSet.dataReader("WebMailServer");
        WebSendMail = DataWebSet.dataReader("WebSendMail");
        WebSendMailID = DataWebSet.dataReader("WebSendMailID");
        WebSendMailPWD = DataWebSet.dataReader("WebSendMailPWD");
        WebAcceptMail = DataWebSet.dataReader("WebAcceptMail");
        WebMessage = DataWebSet.dataReader("WebMessage");
        WebKeywords = DataWebSet.dataReader("WebKeywords");
        WebDescription = DataWebSet.dataReader("WebDescription");
        WebEnable = DataWebSet.dataReader("WebEnable") == "True" ? true : false;
        WebCopyEnable = DataWebSet.dataReader("WebCopyEnable") == "True" ? true : false;
	}

    /// <summary>
    /// 網站標題
    /// </summary>
    public string WebTitle { get; set; }

    /// <summary>
    /// 網站網址
    /// </summary>
    public string WebUrl { get; set; }

    /// <summary>
    /// 網站對應IP
    /// </summary>
    public string WebMapIP { get; set; }

    /// <summary>
    /// 網站郵件伺服器
    /// </summary>
    public string WebMailServer { get; set; }

    /// <summary>
    /// 網站發信MAIL
    /// </summary>
    public string WebSendMail { get; set; }

    /// <summary>
    /// 網站發信MAIL帳號
    /// </summary>
    public string WebSendMailID { get; set; }

    /// <summary>
    /// 網站發信MAIL密碼
    /// </summary>
    public string WebSendMailPWD { get; set; }

    /// <summary>
    /// 網站收信MAIL
    /// </summary>
    public string WebAcceptMail { get; set; }

    /// <summary>
    /// 網站提示訊息
    /// </summary>
    public string WebMessage { get; set; }

    /// <summary>
    /// 網站關鍵字
    /// </summary>
    public string WebKeywords { get; set; }

    /// <summary>
    /// 網站說明文字
    /// </summary>
    public string WebDescription { get; set; }

    /// <summary>
    /// 網站啟用
    /// </summary>
    public bool WebEnable { get; set; }

    /// <summary>
    /// 網站複製
    /// </summary>
    public bool WebCopyEnable { get; set; }

}