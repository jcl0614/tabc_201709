﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// SysValue 系統參數
/// </summary>
[Serializable]
public class SysValue
{
	public SysValue(string progID, System.Web.SessionState.HttpSessionState Session) : this( progID, progID, Session)
	{
	}
    public SysValue(string progID, string refProgIDAuthority, System.Web.SessionState.HttpSessionState Session)
    {
        //程式編號
        this.ProgId = progID;
        emNo = Session["emNo"].ToString().Trim();
        //平台代碼(取得連線字串)
        osID = "0";
        //管理平台名稱
        osName = string.Format("《{0}》管理平台", DataWebSet.dataReader("WebTitle"));
        //設定維護按鈕權限
        DataRow dr = DataEmployee.emDataReader(Session["emNo"].ToString());
        if (dr != null)
        {
            if (string.IsNullOrEmpty(dr["EmGroup"].ToString()))
                ma = DataEmployeeLimit.EmployeeLimitReader(Session["emNo"].ToString(), refProgIDAuthority);
            else
                ma = DataEmGroupLimit.EmGroupLimitReader(dr["EmGroup"].ToString(), refProgIDAuthority);
        }
    }

    private MaintainAuthority ma;

    /// <summary>
    /// 程式編號
    /// </summary>
    public string ProgId
    {
        get;
        set;
    }
    /// <summary>
    /// 員工代碼
    /// </summary>
    public string emNo
    {
        get;
        set;
    }
    /// <summary>
    /// 平台
    /// </summary>
    public string osName
    {
        get;
        set;
    }
    /// <summary>
    /// 平台
    /// </summary>
    public string osID
    {
        get;
        set;
    }
    /// <summary>
    /// 維護權限
    /// </summary>
    public MaintainAuthority Authority
    {
        get { return ma; }
    }
}
