using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;


/// <summary>
/// Remoting 服務提供函式
/// </summary>
[Serializable]
public class RServiceProvider
{
    public RServiceProvider()
    {
    }

    /// <summary>
    /// 回傳結果 true-執行成功, false-執行失敗
    /// </summary>
    bool result = false;
    public bool Result
    {
        get { return result; }
        set { result = value; }
    }
    /// <summary>
    /// 回傳訊息
    /// </summary>
    string returnMessage = "";
    public string ReturnMessage
    {
        get { return returnMessage; }
        set { returnMessage = value; }
    }
    /// <summary>
    /// 回傳資料
    /// </summary>
    Object returnData;
    public Object ReturnData
    {
        get { return returnData; }
        set { returnData = value; }
    }
    /// <summary>
    /// 回傳交易
    /// </summary>
    SqlTransaction returnSqlTransaction = null;
    public SqlTransaction ReturnSqlTransaction
    {
        get { return returnSqlTransaction; }
        set { returnSqlTransaction = value; }
    }
}