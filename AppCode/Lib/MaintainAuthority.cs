﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// MaintainAuthority 的摘要描述
/// 維護按鈕權限控管
/// </summary>
[Serializable]
public class MaintainAuthority
{
	public MaintainAuthority(bool append, bool update, bool select, bool delete)
	{
		//
		// TODO: 在此加入建構函式的程式碼
		//
        this.Append = append;
        this.Update = update;
        this.Select = select;
        this.Delete = delete;
	}

    public bool Append {
        get;
        set;
    }
    public bool Update {
        get;
        set;
    }
    public bool Select
    {
        get;
        set;
    }
    public bool Delete
    {
        get;
        set;
    }
}
