﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;

/// <summary>
/// FontSet 的摘要描述
/// </summary>
public class HtmlMarkSet
{
    #region 粗體格式控制
    /// <summary>
    /// 粗體字型
    /// </summary>
    public static string fontbold
    {
        get{ return "style='font-weight: bold'"; }
    }
    #endregion

    #region 控制尺吋
    /// <summary>
    /// 控制字體大小
    /// </summary>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontS(int ms, string mdoc)
    {
        return string.Format("<font size='{0}'> {1} </font>", ms.ToString(), mdoc);
    }

    /// <summary>
    /// 控制字體大小
    /// </summary>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontS(string ms, int mdoc)
    {
        return string.Format("<font size='{0}'> {1} </font>", ms, mdoc.ToString());
    }

    /// <summary>
    /// 控制字體大小
    /// </summary>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontS(int ms, int mdoc)
    {
        return string.Format("<font size='{0}'> {1} </font>", ms.ToString(), mdoc.ToString());
    }

    /// <summary>
    /// 控制字體大小
    /// </summary>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontS(string ms, string mdoc)
    {
        return string.Format("<font size='{0}'> {1} </font>", ms, mdoc);
    }
    #endregion

    #region 控制顏色尺吋
    /// <summary>
    /// 控制顏色尺吋
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCS(string mc, int ms, int mdoc)
    {
        return string.Format("<font color='{0}' size='{1}'> {2} </font>", mc, ms.ToString(), mdoc.ToString());
    }

    /// <summary>
    /// 控制顏色尺吋
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCS(string mc, string ms, int mdoc)
    {
        return string.Format("<font color='{0}' size='{1}'> {2} </font>", mc, ms, mdoc.ToString());
    }

    /// <summary>
    /// 控制顏色尺吋
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCS(string mc, int ms, string mdoc)
    {
        return string.Format("<font color='{0}' size='{1}'> {2} </font>", mc, ms.ToString(), mdoc);
    }
    
    /// <summary>
    /// 控制顏色尺吋
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCS(string mc ,string ms , string mdoc)
	{
        return string.Format("<font color='{0}' size='{1}'> {2} </font>", mc , ms, mdoc);
    }
    #endregion

    #region 控制顏色尺吋粗體
    /// <summary>
    /// 控制顏色尺吋粗體
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCSB(string mc, int ms, int mdoc)
    {
        return string.Format("<font color='{0}' size='{1}' {2}> {3} </font>", mc, ms.ToString(), fontbold, mdoc.ToString());
    }

    /// <summary>
    /// 控制顏色尺吋粗體
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCSB(string mc, string ms, int mdoc)
    {
        return string.Format("<font color='{0}' size='{1}' {2}> {3} </font>", mc, ms, fontbold, mdoc.ToString());
    }

    /// <summary>
    /// 控制顏色尺吋粗體
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCSB(string mc, int ms, string mdoc)
    {
        return string.Format("<font color='{0}' size='{1}' {2}> {3} </font>", mc, ms.ToString(), fontbold, mdoc);
    }

    /// <summary>
    /// 控制顏色尺吋粗體
    /// </summary>
    /// <param name="mc">顏色</param>
    /// <param name="ms">大小</param>
    /// <param name="mdoc">內容</param>
    /// <returns></returns>
    public static string FontCSB(string mc, string ms, string mdoc)
    {
        return string.Format("<font color='{0}' size='{1}' {2}> {3} </font>", mc, ms, fontbold, mdoc);
    }
    #endregion

    #region 顏色樣版控制
    /// <summary>
    /// 顏色樣版控制
    /// </summary>
    /// <param name="mCss">CSS樣版</param>
    /// <param name="linkBefore">連結前顏色</param>
    /// <param name="linkAfter">連結後顏色</param>
    /// <param name="colorStyle">超連結．文字大小</param>
    /// <returns></returns>
    public static string ColorStyleCtrl(string mCss, string linkBefore, string linkAfter , string size)
    {
        return string.Format(" class=\"{0}\"  style=\"COLOR: {1};font-size: {3}px\" onmouseover=\"this.style.color='{2}'\" onmouseout=\"this.style.color='{1}'\" ", mCss, linkBefore, linkAfter, size);
    }
    #endregion

    #region 顏色樣版控制
    /// <summary>
    /// 顏色樣版控制
    /// </summary>
    /// <param name="mCss">CSS樣版</param>
    /// <param name="linkBefore">連結前顏色</param>
    /// <param name="linkAfter">連結後顏色</param>
    /// <returns></returns>
    public static string ColorStyleCtrl( string linkBefore, string linkAfter)
    {
        return string.Format(" style=\"COLOR: {0}\" onmouseover=\"this.style.color='{1}'\" onmouseout=\"this.style.color='{1}'\" ",  linkBefore, linkAfter);
    }
    #endregion

    #region 變換圖片樣版控制
    /// <summary>
    /// 變換圖片樣版控制
    /// </summary>
    /// <param name="imgHttp">CSS樣版</param>
    /// <param name="MouseOut">滑鼠離開圖片</param>
    /// <param name="mouseover">滑鼠覆蓋圖片</param>
    /// <returns></returns>
    public static string imgStyleCtrl(string imgId , string imgHttp , string MouseOut, string mouseover )
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendFormat(" onmouseout=\"{0}.src = '{1}{2}'\" ", imgId , imgHttp, MouseOut);
        if ( mouseover != "" )
            sb.AppendFormat(" onmouseover=\"{0}.src = '{1}{2}'\" ", imgId , imgHttp, mouseover);
        return sb.ToString();
    }
    #endregion

    #region 產生Image語法
    public static string MarkImageCtrl(string title , string imgHttp , string imgFileName , int border )
    {
        return string.Format("<img title=\"{0}\" src=\"{1}{2}\" border=\"{3}\">", title, imgHttp, imgFileName, border);
    }
    #endregion

    #region 產生Image語法 (圖片可變換)
    public static string MarkImageCtrl(string title, string imgHttp, string imgFileName, int border, string StyleCtrl)
    {
        return string.Format("<img title=\"{0}\" src=\"{1}{2}\" border=\"{3}\" {4}>", title, imgHttp, imgFileName, border, StyleCtrl);
    }
    #endregion

    #region 產生超連結語法

    #region 超連結連結前後變色控制
    /// <summary>
    /// 超連結連結前後變色控制
    /// </summary>
    /// <param name="mHttp">超連結網址</param>
    /// <param name="mHttpDoc">超連結文字內容</param>
    /// <param name="colorStyle">超連結．連結前後顏色控制</param>
    /// <returns></returns>
    public static string HyperLinkCtrl(string mHttp , string mHttpDoc , string StyleCtrl )
    {
        return string.Format(" <a href=\"{0}\" {2} >{1}</a>", mHttp, mHttpDoc, StyleCtrl);
    }
    #endregion

    #region 超連結連結前後變色控制
    /// <summary>
    /// 超連結連結前後變色控制
    /// </summary>
    /// <param name="mHttpDoc">超連結文字內容</param>
    /// <param name="title">連結描述</param>
    /// <param name="mHttp">超連結網址</param>
    /// <param name="Target">連結目標</param>
    /// <param name="colorStyle">超連結．連結前後顏色控制</param>
    /// <returns></returns>
    public static string HyperLinkCtrl(string mHttpDoc, string title, string mHttp, string Target, string StyleCtrl)
    {
        if (mHttp != "")
            return string.Format(" <a title=\"{1}\" href=\"{2}\" {4} target=\"{3}\">{0}</a>", mHttpDoc, title, mHttp, Target, StyleCtrl);
        else
            return string.Format(" <a title=\"{1}\" {3} target=\"{2}\">{0}</a>", mHttpDoc, title, Target, StyleCtrl);
    }
    #endregion

    #endregion

    #region markTable
    /// <summary>
    /// 建立表格內容
    /// </summary>
    /// <param name="mA">align</param>
    /// <param name="mVA">valign</param>
    /// <param name="mW">width</param>
    /// <param name="mH">height</param>
    /// <param name="mbgimg">background</param>
    /// <param name="mTdDoc">TD儲存格內容</param>
    /// <returns></returns>
    public static string markTable(string mAlign, string mValign, int mWidth, int mHeight, string mBackground, string mTdDoc)
    {
        if (mWidth == 0 && mHeight == 0)
            return string.Format("<table cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"{0}\" valign=\"{1}\" background=\"{2}\">{3}</td></tr></table>", mAlign, mValign, mBackground, mTdDoc);
        else if (mWidth == 0 && mHeight != 0)
            return string.Format("<table cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"{0}\" valign=\"{1}\" height=\"{2}\" background=\"{3}\">{4}</td></tr></table>", mAlign, mValign, mHeight, mBackground, mTdDoc);
        else if (mWidth != 0 && mHeight == 0)
            return string.Format("<table cellpadding=\"0\" cellspacing=\"0\"><tr><td align=\"{0}\" valign=\"{1}\" width=\"{2}\" background=\"{3}\">{4}</td></tr></table>", mAlign, mValign, mWidth, mBackground, mTdDoc);
        else
            return "";
    }
    #endregion

}
