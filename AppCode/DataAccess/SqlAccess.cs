﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

/// <summary>
/// SqlAccess 的摘要描述
/// </summary>
public class SqlAccess
{
    private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "System"));
    private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

    #region SqlConnectionstring
    /// <summary>
    /// 主系統連線字串
    /// </summary>
    public static readonly string CONN_Main = Lib.PuchAesChinese(System.Configuration.ConfigurationManager.ConnectionStrings["MainDBConnectionString"].ConnectionString, DataSysValue.Connection);
    #endregion

    #region 使用的連線字串
    /// <summary>
    /// 使用的連線字串
    /// </summary>
    /// <param name="setcn">平台</param>
    /// <returns></returns>
    public static string setConnectionString(string setcn)
    {
        return setConnectionString(setcn, false);
    }

    public static string setConnectionString(string setcn, bool backup)
    {
        string connectionString = "";
        string[] accountPwd = getAccountPwd();
        switch (setcn)
        {
            case "1":
                
                break;
            default:
                connectionString = CONN_Main;
                break;
        }
        return connectionString;
    }


    /// <summary>
    /// 產生連線
    /// </summary>
    /// <param name="setcn">平台</param>
    /// <returns></returns>
    public static SqlConnection setSqlConnection(string setcn)
    {
        return setSqlConnection(setcn, false);
    }
    /// <summary>
    /// 產生連線
    /// </summary>
    /// <param name="setcn">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <returns></returns>
    public static SqlConnection setSqlConnection(string setcn, bool backup)
    {
        return new SqlConnection(setConnectionString(setcn, backup));
    }
    #endregion

    #region 資料庫連線
    /// <summary>
    /// 資料庫連線開啟
    /// </summary>
    /// <param name="conn">連線物件</param>
    public static void SqlConnectionOpen(SqlConnection conn)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
                conn.Open();
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {

        }
    }
    /// <summary>
    /// 資料庫連線開啟
    /// </summary>
    /// <param name="cmd">命令物件</param>
    /// <param name="conn">連線物件</param>
    /// <param name="cmdText">命令陳述</param>
    private static void SqlConnectionOpen(SqlCommand cmd, SqlConnection conn, string cmdText)
    {
        try
        {
            //判斷連接的狀態。如果是關閉狀態，則打開
            if (conn.State != ConnectionState.Open)
                conn.Open();
            //cmd屬性賦值
            cmd.Connection = conn;
            cmd.CommandText = cmdText;
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
    }
    #endregion

    #region 資料庫中繼連線
    /// <summary>
    /// 資料庫中繼連線
    /// </summary>
    /// <param name="conn">連線物件</param>
    public static void SqlConnectionClose(SqlConnection conn)
    {
        try
        {
            //判斷連接的狀態。如果是關閉狀態，則打開
            if (conn != null)
            {
                if (conn.State != ConnectionState.Closed)
                    conn.Close();
                conn.Dispose();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
    }
    #endregion

    #region 資料庫查詢
    /// <summary>
    /// 資料庫查詢回傳DataSet(Schema)
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <returns></returns>
    public static DataSet SqlcommandFillSchema(string connAddress, string commandText)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnectionOpen(cmd, setSqlConnection(connAddress), commandText);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.FillSchema(ds, SchemaType.Source);
            SqlConnectionClose(cmd.Connection);

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet(Schema)
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">查詢語法</param>
    /// <returns></returns>
    public static DataSet SqlcommandFillSchema(SqlTransaction sqlTrans, string commandText)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlTrans.Connection;
            cmd.CommandText = commandText;
            cmd.Transaction = sqlTrans;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.FillSchema(ds, SchemaType.Source);

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText)
    {
        return SqlcommandExecute(connAddress, false, commandText);
    }


    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandExecute(connAddress, false, commandText, sqlParas);
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText, SqlParameter sqlPara)
    {
        return SqlcommandExecute(connAddress, false, commandText, sqlPara);
    }
    //-----------
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText, CommandType commandType)
    {
        return SqlcommandExecute(connAddress, false, commandText, commandType);
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        return SqlcommandExecute(connAddress, false, commandText, sqlParas, commandType);
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        return SqlcommandExecute(connAddress, false, commandText, sqlPara, commandType);
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlTransaction sqlTrans)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlTrans.Connection;
            cmd.CommandText = commandText;
            cmd.Transaction = sqlTrans;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlTransaction sqlTrans, SqlParameter[] sqlParas)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddRange(sqlParas);
            cmd.Connection = sqlTrans.Connection;
            cmd.CommandText = commandText;
            cmd.Transaction = sqlTrans;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlTransaction sqlTrans, SqlParameter sqlPara)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add(sqlPara);
            cmd.Connection = sqlTrans.Connection;
            cmd.CommandText = commandText;
            cmd.Transaction = sqlTrans;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlConn">資料庫連線</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlConnection sqlConn)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = sqlConn;
            cmd.CommandText = commandText;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlConn">資料庫連線</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlConnection sqlConn, SqlParameter[] sqlParas)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddRange(sqlParas);
            cmd.Connection = sqlConn;
            cmd.CommandText = commandText;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlConn">資料連線</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string commandText, SqlConnection sqlConn, SqlParameter sqlPara)
    {
        DataSet ds = new DataSet();
        try
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.Add(sqlPara);
            cmd.Connection = sqlConn;
            cmd.CommandText = commandText;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return ds;
    }


    ///---------/////////////////////////////////
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }


    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText, SqlParameter sqlPara)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.Add(sqlPara);
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }
    //-----------
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText, CommandType commandType)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }

    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }
    /// <summary>
    /// 資料庫查詢回傳DataSet
    /// </summary>
    /// <param name="connAddress">使用的資料庫連結編號</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">查詢語法</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="commandType">SQL命令型態</param>
    /// <returns></returns>
    public static DataSet SqlcommandExecute(string connAddress, bool backup, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        DataSet ds = new DataSet();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.Add(sqlPara);
        try
        {
            SqlConnectionOpen(cmd, setSqlConnection(connAddress, backup), commandText);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //加入主索引鍵
            da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
            da.Fill(ds);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
            SqlConnectionClose(cmd.Connection);
        }
        return ds;
    }
    #endregion

    #region 產生交易及交易處理
    /// <summary>
    /// 產生交易
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <returns></returns>
    public static SqlTransaction SetSqlTransaction(SqlConnection sqlConn)
    {
        if (sqlConn.State == ConnectionState.Closed)
            SqlConnectionOpen(sqlConn);
        return sqlConn.BeginTransaction();
    }
    /// <summary>
    /// 產生交易
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="isolationLevel">指定連接的交易鎖定行為</param>
    /// <returns></returns>
    public static SqlTransaction SetSqlTransaction(SqlConnection sqlConn, IsolationLevel isolationLevel)
    {
        if (sqlConn.State == ConnectionState.Closed)
            SqlConnectionOpen(sqlConn);
        return sqlConn.BeginTransaction(isolationLevel);
    }
    /// <summary>
    /// 完成交易
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    public static void SqlTransactionCommit(SqlTransaction sqlTrans)
    {
        if (sqlTrans != null && sqlTrans.Connection != null)
            sqlTrans.Commit();
    }
    /// <summary>
    /// 取消交易
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    public static void SqlTransactionRollback(SqlTransaction sqlTrans)
    {
        if (sqlTrans != null && sqlTrans.Connection != null)
            sqlTrans.Rollback();
    }
    #endregion

    #region 資料庫執行命令

    #region ExecuteScalar
    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, string commandText)
    {
        return SqlcommandExecuteScalar(connAddress, false, commandText);
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, string commandText, SqlParameter sqlPara)
    {
        return SqlcommandExecuteScalar(connAddress, false, commandText, sqlPara);
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandExecuteScalar(connAddress, false, commandText, sqlParas);
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandExecuteScalarTransaction(string connAddress, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandExecuteScalarTransaction(connAddress, false, commandText, sqlParas);
    }

    //////////////////////////////////////////////////////
    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, bool backup, string commandText)
    {
        object value = null;
        try
        {

            SqlConnection sqlConn = null;
            SqlCommand cmd = new SqlCommand();
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            value = cmd.ExecuteScalar();
            SqlConnectionClose(sqlConn);

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, bool backup, string commandText, SqlParameter sqlPara)
    {
        object value = null;
        SqlConnection sqlConn = null;
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.Add(sqlPara);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            value = cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas)
    {
        object value = null;
        SqlConnection sqlConn = null;
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            value = cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return value;
    }
    /// <summary>
    /// 回傳第一筆資料第一個欄位資料-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandExecuteScalarTransaction(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            if (sqlTrans == null)
                sqlTrans = SetSqlTransaction(sqlConn);
            //設定交易處理
            cmd.Transaction = sqlTrans;
            rsp.ReturnSqlTransaction = sqlTrans;
            rsp.ReturnData = cmd.ExecuteScalar();
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.Result = false;
            rsp.ReturnMessage = ex.Message;
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
            //cmd.Parameters.Clear();
        }
        return rsp;
    }

    ////////////////////////////////////////////////////

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlTransaction sqlTrans)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Connection = sqlTrans.Connection;
            cmd.Transaction = sqlTrans;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlParameter sqlPara, SqlTransaction sqlTrans)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.Add(sqlPara);
            cmd.Connection = sqlTrans.Connection;
            cmd.Transaction = sqlTrans;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlParameter[] sqlParas, SqlTransaction sqlTrans)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddRange(sqlParas);
            cmd.Connection = sqlTrans.Connection;
            cmd.Transaction = sqlTrans;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlConn">連線物件</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlConnection sqlConn)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Connection = sqlConn;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 回傳第一筆資料第一個欄位資料
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="sqlConn">連線物件</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlParameter sqlPara, SqlConnection sqlConn)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.Add(sqlPara);
            cmd.Connection = sqlConn;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="sqlConn">連線物件</param>
    /// <returns></returns>
    public static object SqlcommandExecuteScalar(string commandText, SqlParameter[] sqlParas, SqlConnection sqlConn)
    {
        object value = null;
        try
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = commandText;
            cmd.Parameters.AddRange(sqlParas);
            cmd.Connection = sqlConn;
            value = cmd.ExecuteScalar();
            cmd.Parameters.Clear();

        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
        }
        return value;
    }
    #endregion

    #region ExecuteNonQuery
    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText);
    }
    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, CommandType commandType)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, commandType);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, SqlParameter sqlPara)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, sqlPara);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, sqlPara, commandType);
    }


    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, sqlParas);
    }
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, SqlParameter[] sqlParas, SqlTransaction sqlTrans)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, sqlParas, sqlTrans);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        return SqlcommandNonQueryExecute(connAddress, false, commandText, sqlParas, commandType);
    }


    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText)
    {
        return SqlcommandNonQueryExecute(connAddress, backup, commandText, CommandType.Text);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, CommandType commandType, SqlTransaction sqlTrans)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫交易(交易階段,未執行)
    /// </summary>
    /// <param name="connAddress"></param>
    /// <param name="backup"></param>
    /// <param name="commandText"></param>
    /// <param name="commandType"></param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryTransaction(string connAddress, bool backup, string commandText, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                rsp.ReturnSqlTransaction = sqlTrans;
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter sqlPara)
    {
        return SqlcommandNonQueryExecute(connAddress, backup, commandText, sqlPara, CommandType.Text);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.Add(sqlPara);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫(交易階段,未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryTransaction(string connAddress, bool backup, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.Add(sqlPara);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnSqlTransaction = sqlTrans;
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandNonQueryExecute(connAddress, backup, commandText, sqlParas, CommandType.Text);
    }
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas, SqlTransaction sqlTrans)
    {
        return SqlcommandNonQueryExecute(connAddress, backup, commandText, sqlParas, CommandType.Text, sqlTrans);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }
    public static RServiceProvider SqlcommandNonQueryExecute(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas, CommandType commandType, SqlTransaction sqlTrans)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            if (sqlTrans == null)
                sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫(交易階段,未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="backup">歷史資料庫</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryTransaction(string connAddress, bool backup, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlConnection sqlConn = null;
        SqlTransaction sqlTrans = null;
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            sqlConn = setSqlConnection(connAddress, backup);
            SqlConnectionOpen(cmd, sqlConn, commandText);
            sqlTrans = SetSqlTransaction(sqlConn);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                SqlTransactionCommit(sqlTrans);
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
            cmd.Parameters.Clear();
        }
        return rsp;
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText)
    {
        return SqlcommandNonQueryExecute(sqlConn, sqlTrans, commandText, CommandType.Text);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        try
        {
            SqlConnectionOpen(cmd, sqlConn, commandText);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
        }
        return rsp;
    }
    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText, SqlParameter[] sqlParas)
    {
        return SqlcommandNonQueryExecute(sqlConn, sqlTrans, commandText, sqlParas, CommandType.Text);
    }
    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlParas">參數</param>
    /// <param name="commandType">指定解譯命令字串</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText, SqlParameter[] sqlParas, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.AddRange(sqlParas);
        try
        {
            SqlConnectionOpen(cmd, sqlConn, commandText);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
        }
        return rsp;
    }
    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText, SqlParameter sqlPara)
    {
        return SqlcommandNonQueryExecute(sqlConn, sqlTrans, commandText, sqlPara, CommandType.Text);
    }

    /// <summary>
    /// 異動資料庫
    /// </summary>
    /// <param name="sqlConn">連線物件</param>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="commandText">SQL命令</param>
    /// <param name="sqlPara">參數</param>
    /// <returns></returns>
    public static RServiceProvider SqlcommandNonQueryExecute(SqlConnection sqlConn, SqlTransaction sqlTrans, string commandText, SqlParameter sqlPara, CommandType commandType)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandType = commandType;
        cmd.Parameters.Add(sqlPara);
        try
        {
            SqlConnectionOpen(cmd, sqlConn, commandText);
            cmd.Transaction = sqlTrans;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                rsp.ReturnMessage = "未更新任何資料!!!";
                rsp.ReturnData = count;
                rsp.Result = true;
            }
            else
            {
                rsp.ReturnData = count;
                rsp.Result = true;
            }
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            cmd.Parameters.Clear();
        }
        return rsp;
    }
    #endregion

    #endregion

    #region 建置參數
    /// <summary>
    /// 建置參數
    /// </summary>
    /// <param name="paraName">參數名稱</param>
    /// <param name="value">參數值</param>
    /// <returns></returns>
    public static SqlParameter CreateSqlParameter(string paraName, object value)
    {
        return CreateSqlParameter(paraName, value, ParameterDirection.Input);
    }
    /// <summary>
    /// 建置參數
    /// </summary>
    /// <param name="paraName">參數名稱</param>
    /// <param name="value">參數值</param>
    /// <param name="direct">參數型別</param>
    /// <returns></returns>
    public static SqlParameter CreateSqlParameter(string paraName, object value, ParameterDirection direct)
    {
        SqlParameter sqlPara = new SqlParameter(paraName, value);
        sqlPara.Direction = direct;
        return sqlPara;
    }
    #endregion

    #region 資料庫新增
    /// <summary>
    /// 資料庫新增
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandAppend(SqlTransaction sqlTrans, DataSet ds)
    {
        string identityPK = "";
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlCommand[] sqlCmdsQuery = new SqlCommand[ds.Tables.Count];

        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPara = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                sqlCmdsQuery[i] = new SqlCommand();

                DataRow row = ds.Tables[i].Rows[0];

                //自動編號
                if (ds.Tables[i].TableName == "sys_emDaily" && identityPK.Trim().Length > 0)
                    row["edTarget"] = string.Format("{0}={1},{2}", identityPK, rsp.ReturnData, row["edTarget"]);

                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    if (dc.AutoIncrement) //自動編號欄位
                        identityPK = dc.ColumnName;
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmdsQuery[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號欄位不新增,sql server 系統自動新增
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append(",");
                    //加作SQL參數
                    sbPara.Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" (")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" ) VALUES (")
                    .Append(sbPara.Remove(sbPara.Length - 1, 1))
                    .Append(" )");
                if (i == 0) //只取第一個Table的IdentityPK
                {
                    sbSql.Append(" SET @IdentityPK = SCOPE_IDENTITY()");
                    //加作Cmd參數取IdentityPkParameter
                    SqlParameter IdentityPkParameter = new SqlParameter("@IdentityPK", SqlDbType.Int);
                    IdentityPkParameter.Direction = ParameterDirection.Output;
                    sqlCmds[i].Parameters.Add(IdentityPkParameter);
                }

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());

                //檢核資料是否重覆
                if (ds.Tables[i].TableName != "sys_emDaily" && identityPK.Trim().Length == 0)
                {
                    StringBuilder sbSqlQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmdsQuery[i], sqlConn, sbSqlQuery.ToString());
                    //設定交易處理
                    sqlCmdsQuery[i].Transaction = sqlTrans;
                    //執行新增資料命令
                    if (Convert.ToInt32(sqlCmdsQuery[i].ExecuteScalar()) > 0)
                    {
                        rsp.ReturnMessage = "資料重覆，請檢核!!!";
                        return rsp;
                    }
                }

                //設定交易處理
                sqlCmds[i].Transaction = sqlTrans;
                //執行新增資料命令
                sqlCmds[i].ExecuteNonQuery();

                if (i == 0) //IdentityPK
                    rsp.ReturnData = sqlCmds[i].Parameters["@IdentityPK"].Value;

            }
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫新增
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="ds">資料</param>
    /// <param name="identityPK">自動編號欄位</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandAppend(SqlTransaction sqlTrans, DataSet ds, string identityPK)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlCommand[] sqlCmdsQuery = new SqlCommand[ds.Tables.Count];

        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPara = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                sqlCmdsQuery[i] = new SqlCommand();

                DataRow row = ds.Tables[i].Rows[0];

                //自動編號
                if (ds.Tables[i].TableName == "sys_emDaily" && identityPK.Trim().Length > 0)
                    row["edTarget"] = string.Format("{0}={1},{2}", identityPK, rsp.ReturnData, row["edTarget"]);

                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmdsQuery[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append(",");
                    //加作SQL參數
                    sbPara.Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" (")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" ) VALUES (")
                    .Append(sbPara.Remove(sbPara.Length - 1, 1))
                    .Append(" )");
                if (i == 0) //只取第一個Table的IdentityPK
                {
                    sbSql.Append(" SET @IdentityPK = SCOPE_IDENTITY()");
                    //加作Cmd參數取IdentityPkParameter
                    SqlParameter IdentityPkParameter = new SqlParameter("@IdentityPK", SqlDbType.Int);
                    IdentityPkParameter.Direction = ParameterDirection.Output;
                    sqlCmds[i].Parameters.Add(IdentityPkParameter);
                }

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());

                //檢核資料是否重覆
                if (ds.Tables[i].TableName != "sys_emDaily" && identityPK.Trim().Length == 0)
                {
                    StringBuilder sbSqlQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmdsQuery[i], sqlConn, sbSqlQuery.ToString());
                    //設定交易處理
                    sqlCmdsQuery[i].Transaction = sqlTrans;
                    //執行新增資料命令
                    if (Convert.ToInt32(sqlCmdsQuery[i].ExecuteScalar()) > 0)
                    {
                        rsp.ReturnMessage = "資料重覆，請檢核!!!";
                        return rsp;
                    }
                }

                //設定交易處理
                sqlCmds[i].Transaction = sqlTrans;
                //執行新增資料命令
                sqlCmds[i].ExecuteNonQuery();

                if (i == 0) //IdentityPK
                    rsp.ReturnData = sqlCmds[i].Parameters["@IdentityPK"].Value;

            }
            SqlTransactionCommit(sqlTrans);
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }

    /// <summary>
    /// 資料庫新增
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandAppend(string connAddress, DataSet ds)
    {
        string identityPK = "";
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlCommand[] sqlCmdsQuery = new SqlCommand[ds.Tables.Count];

        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            sqlConn = setSqlConnection(connAddress);

            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPara = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                sqlCmdsQuery[i] = new SqlCommand();

                DataRow row = ds.Tables[i].Rows[0];

                //自動編號
                if (ds.Tables[i].TableName == "sys_emDaily" && identityPK.Trim().Length > 0)
                    row["edTarget"] = string.Format("{0}={1},{2}", identityPK, rsp.ReturnData, row["edTarget"]);

                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    if (dc.AutoIncrement) //自動編號欄位
                        identityPK = dc.ColumnName;
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmdsQuery[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號欄位不新增,sql server 系統自動新增
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append(",");
                    //加作SQL參數
                    sbPara.Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" (")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" ) VALUES (")
                    .Append(sbPara.Remove(sbPara.Length - 1, 1))
                    .Append(" )");
                if (i == 0) //只取第一個Table的IdentityPK
                {
                    sbSql.Append(" SET @IdentityPK = SCOPE_IDENTITY()");
                    //加作Cmd參數取IdentityPkParameter
                    SqlParameter IdentityPkParameter = new SqlParameter("@IdentityPK", SqlDbType.Int);
                    IdentityPkParameter.Direction = ParameterDirection.Output;
                    sqlCmds[i].Parameters.Add(IdentityPkParameter);
                }

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);

                //檢核資料是否重覆
                if (ds.Tables[i].TableName != "sys_emDaily" && identityPK.Trim().Length == 0)
                {
                    StringBuilder sbSqlQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmdsQuery[i], sqlConn, sbSqlQuery.ToString());
                    //設定交易處理
                    sqlCmdsQuery[i].Transaction = sqlTrans;
                    //執行新增資料命令
                    if (Convert.ToInt32(sqlCmdsQuery[i].ExecuteScalar()) > 0)
                    {
                        rsp.ReturnMessage = "資料重覆，請檢核!!!";
                        return rsp;
                    }
                }

                //設定交易處理
                sqlCmds[i].Transaction = sqlTrans;
                //執行新增資料命令
                sqlCmds[i].ExecuteNonQuery();

                if (i == 0) //IdentityPK
                    rsp.ReturnData = sqlCmds[i].Parameters["@IdentityPK"].Value;

            }
            SqlTransactionCommit(sqlTrans);
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫新增-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandAppendTransaction(string connAddress, DataSet ds)
    {
        string identityPK = "";
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlCommand[] sqlCmdsQuery = new SqlCommand[ds.Tables.Count];

        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            sqlConn = setSqlConnection(connAddress);

            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPara = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                sqlCmdsQuery[i] = new SqlCommand();

                DataRow row = ds.Tables[i].Rows[0];

                //自動編號
                if (ds.Tables[i].TableName == "sys_emDaily" && identityPK.Trim().Length > 0)
                    row["edTarget"] = string.Format("{0}={1},{2}", identityPK, rsp.ReturnData, row["edTarget"]);

                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    if (dc.AutoIncrement) //自動編號欄位
                        identityPK = dc.ColumnName;
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmdsQuery[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號欄位不新增,sql server 系統自動新增
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append(",");
                    //加作SQL參數
                    sbPara.Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" (")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" ) VALUES (")
                    .Append(sbPara.Remove(sbPara.Length - 1, 1))
                    .Append(" )");
                if (i == 0) //只取第一個Table的IdentityPK
                {
                    sbSql.Append(" SET @IdentityPK = SCOPE_IDENTITY()");
                    //加作Cmd參數取IdentityPkParameter
                    SqlParameter IdentityPkParameter = new SqlParameter("@IdentityPK", SqlDbType.Int);
                    IdentityPkParameter.Direction = ParameterDirection.Output;
                    sqlCmds[i].Parameters.Add(IdentityPkParameter);
                }

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);

                //檢核資料是否重覆
                if (ds.Tables[i].TableName != "sys_emDaily" && identityPK.Trim().Length == 0)
                {
                    StringBuilder sbSqlQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmdsQuery[i], sqlConn, sbSqlQuery.ToString());
                    //設定交易處理
                    sqlCmdsQuery[i].Transaction = sqlTrans;
                    //執行新增資料命令
                    if (Convert.ToInt32(sqlCmdsQuery[i].ExecuteScalar()) > 0)
                    {
                        rsp.ReturnMessage = "資料重覆，請檢核!!!";
                        return rsp;
                    }
                }

                //設定交易處理
                sqlCmds[i].Transaction = sqlTrans;
                //執行新增資料命令
                sqlCmds[i].ExecuteNonQuery();

                if (i == 0) //IdentityPK
                    rsp.ReturnData = sqlCmds[i].Parameters["@IdentityPK"].Value;

            }
            rsp.ReturnSqlTransaction = sqlTrans;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫新增
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <param name="identityPK">自動編號欄位</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandAppend(string connAddress, DataSet ds, string identityPK)
    {

        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlCommand[] sqlCmdsQuery = new SqlCommand[ds.Tables.Count];

        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            sqlConn = setSqlConnection(connAddress);

            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPara = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                sqlCmdsQuery[i] = new SqlCommand();

                DataRow row = ds.Tables[i].Rows[0];

                //自動編號
                if (ds.Tables[i].TableName == "sys_emDaily" && identityPK.Trim().Length > 0)
                    row["edTarget"] = string.Format("{0}={1},{2}", identityPK, rsp.ReturnData, row["edTarget"]);

                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmdsQuery[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append(",");
                    //加作SQL參數
                    sbPara.Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" (")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" ) VALUES (")
                    .Append(sbPara.Remove(sbPara.Length - 1, 1))
                    .Append(" )");
                if (i == 0) //只取第一個Table的IdentityPK
                {
                    sbSql.Append(" SET @IdentityPK = SCOPE_IDENTITY()");
                    //加作Cmd參數取IdentityPkParameter
                    SqlParameter IdentityPkParameter = new SqlParameter("@IdentityPK", SqlDbType.Int);
                    IdentityPkParameter.Direction = ParameterDirection.Output;
                    sqlCmds[i].Parameters.Add(IdentityPkParameter);
                }

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);

                //檢核資料是否重覆
                if (ds.Tables[i].TableName != "sys_emDaily" && identityPK.Trim().Length == 0)
                {
                    StringBuilder sbSqlQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmdsQuery[i], sqlConn, sbSqlQuery.ToString());
                    //設定交易處理
                    sqlCmdsQuery[i].Transaction = sqlTrans;
                    //執行新增資料命令
                    if (Convert.ToInt32(sqlCmdsQuery[i].ExecuteScalar()) > 0)
                    {
                        rsp.ReturnMessage = "資料重覆，請檢核!!!";
                        return rsp;
                    }
                }

                //設定交易處理
                sqlCmds[i].Transaction = sqlTrans;
                //執行新增資料命令
                sqlCmds[i].ExecuteNonQuery();

                if (i == 0) //IdentityPK
                    rsp.ReturnData = sqlCmds[i].Parameters["@IdentityPK"].Value;

            }
            SqlTransactionCommit(sqlTrans);
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    #endregion

    #region 資料庫修改
    /// <summary>
    /// 資料庫修改
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandUpdate(SqlTransaction sqlTrans, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號不可修改
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    if (row.RowState == DataRowState.Modified)
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName, DataRowVersion.Original]));
                    else
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("UPDATE ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" SET ")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                sqlCmds[i].Transaction = sqlTrans;
                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫修改
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandUpdate(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        Dictionary<string, int> dCount = new Dictionary<string, int>();

        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號不可修改
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    if (row.RowState == DataRowState.Modified)
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName, DataRowVersion.Original]));
                    else
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("UPDATE ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" SET ")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);
                sqlCmds[i].Transaction = sqlTrans;

                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount; //異動筆數
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫修改-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandUpdateTransaction(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        Dictionary<string, int> dCount = new Dictionary<string, int>();

        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbFields = new StringBuilder();
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                foreach (DataColumn dc in ds.Tables[i].Columns)
                {
                    if (dc.AutoIncrement) continue; //自動編號不可修改
                    //加入欄位
                    sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    if (row.RowState == DataRowState.Modified)
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName, DataRowVersion.Original]));
                    else
                        sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("UPDATE ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" SET ")
                    .Append(sbFields.Remove(sbFields.Length - 1, 1))
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);
                sqlCmds[i].Transaction = sqlTrans;

                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
           
            rsp.ReturnSqlTransaction = sqlTrans;
            rsp.ReturnData = dCount; //異動筆數
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    #endregion

    #region 資料庫刪除
    /// <summary>
    /// 資料庫刪除
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandDelete(SqlTransaction sqlTrans, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlConnection sqlConn = sqlTrans.Connection;
        
        try
        {
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                sqlCmds[i].Transaction = sqlTrans;
                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }
            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫刪除
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandDelete(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);
                sqlCmds[i].Transaction = sqlTrans;
                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }
            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫刪除-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandDeleteTransaction(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                StringBuilder sbPk = new StringBuilder();
                sqlCmds[i] = new SqlCommand();
                DataRow row = ds.Tables[i].Rows[0];
                //加入pk過濾
                foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                {
                    //加入欄位
                    sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                    //加作Cmd參數
                    sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
                }

                //整合SQL語法
                StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                    .Append(ds.Tables[i].TableName)
                    .Append(" WHERE ")
                    .Append(sbPk.Remove(sbPk.Length - 5, 5));

                SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                if (sqlTrans == null)
                    sqlTrans = SetSqlTransaction(sqlConn);
                sqlCmds[i].Transaction = sqlTrans;
                dCount[ds.Tables[i].TableName] = sqlCmds[i].ExecuteNonQuery();
            }
            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            rsp.ReturnSqlTransaction = sqlTrans;
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    #endregion

    #region 資料庫批次新增
    /// <summary>
    /// 資料庫批次新增
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchAppend(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            sqlConn = setSqlConnection(connAddress);

            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPara = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append(",");
                        //加作SQL參數
                        sbPara.Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }
                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" (")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" ) VALUES (")
                        .Append(sbPara.Remove(sbPara.Length - 1, 1))
                        .Append(" )");

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    //設定交易處理
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    //執行新增資料命令
                    sqlCmds[i].ExecuteNonQuery();
                }
            }
            SqlTransactionCommit(sqlTrans);
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫批次新增-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchAppendTransaction(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            sqlConn = setSqlConnection(connAddress);

            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPara = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append(",");
                        //加作SQL參數
                        sbPara.Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" (")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" ) VALUES (")
                        .Append(sbPara.Remove(sbPara.Length - 1, 1))
                        .Append(" )");

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    //設定交易處理
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    //執行新增資料命令
                    sqlCmds[i].ExecuteNonQuery();
                }
            }
            rsp.ReturnSqlTransaction = sqlTrans;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
        }
        return rsp;
    }

    /// <summary>
    /// 資料庫批次新增
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchAppend(SqlTransaction sqlTrans, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        SqlCommand[] sqlCmds = new SqlCommand[ds.Tables.Count];
        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < ds.Tables.Count; i++)
            {
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPara = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append(",");
                        //加作SQL參數
                        sbPara.Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }
                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" (")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" ) VALUES (")
                        .Append(sbPara.Remove(sbPara.Length - 1, 1))
                        .Append(" )");

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    //設定交易處理
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    //執行新增資料命令
                    sqlCmds[i].ExecuteNonQuery();
                }
            }
            SqlTransactionCommit(sqlTrans);
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }

    #endregion

    #region 資料庫批次修改
    /// <summary>
    /// 資料庫批次修改
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchUpdate(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                if (ds.Tables[i].Rows.Count == 0)
                    continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue; //自動編號不可修改

                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName, DataRowVersion.Original]));
                        else
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("UPDATE ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" SET ")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName, DataRowVersion.Original];
                        else
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue;
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }
                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫批次修改-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchUpdateTransaction(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                if (ds.Tables[i].Rows.Count == 0)
                    continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue; //自動編號不可修改

                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName, DataRowVersion.Original]));
                        else
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("UPDATE ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" SET ")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName, DataRowVersion.Original];
                        else
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue;
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }
                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);

            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫批次修改
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchUpdate(SqlTransaction sqlTrans, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);

                if (ds.Tables[i].Rows.Count == 0)
                    continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbFields = new StringBuilder();
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue; //自動編號不可修改

                        //加入欄位
                        sbFields.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(",");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }
                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@pk_").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName, DataRowVersion.Original]));
                        else
                            sqlCmds[i].Parameters.Add(new SqlParameter("@pk_" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("UPDATE ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" SET ")
                        .Append(sbFields.Remove(sbFields.Length - 1, 1))
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        if (dr.RowState == DataRowState.Modified)
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName, DataRowVersion.Original];
                        else
                            sqlCmds[i].Parameters["@pk_" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }

                    foreach (DataColumn dc in ds.Tables[i].Columns)
                    {
                        if (dc.AutoIncrement) continue;
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];
                    }
                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }
    #endregion

    #region 資料庫批次刪除
    /// <summary>
    /// 資料庫批次刪除
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchDelete(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫批次刪除-交易階段(尚未執行)
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchDeleteTransaction(string connAddress, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlTransaction sqlTrans = null;
        SqlConnection sqlConn = null;
        try
        {
            //取得連線
            sqlConn = setSqlConnection(connAddress);
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    if (sqlTrans == null)
                        sqlTrans = SetSqlTransaction(sqlConn);
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
            
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
            //SqlConnectionClose(sqlConn);
        }
        return rsp;
    }
    /// <summary>
    /// 資料庫批次刪除
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <param name="ds">資料</param>
    /// <returns></returns>
    public static RServiceProvider SqlCommandBatchDelete(SqlTransaction sqlTrans, DataSet ds)
    {
        RServiceProvider rsp = new RServiceProvider();
        Dictionary<string, int> dCount = new Dictionary<string, int>();
        int tablesCount = ds.Tables.Count;
        if (ds.Tables.Contains("sys_emDaily"))
            tablesCount--;
        SqlCommand[] sqlCmds = new SqlCommand[tablesCount];
        SqlConnection sqlConn = sqlTrans.Connection;
        try
        {
            //取得欄位名稱
            for (int i = 0; i < tablesCount; i++)
            {
                dCount.Add(ds.Tables[i].TableName, 0);
                if (ds.Tables[i].Rows.Count == 0) continue;

                foreach (DataRow dr in ds.Tables[i].Rows)
                {
                    StringBuilder sbPk = new StringBuilder();
                    sqlCmds[i] = new SqlCommand();

                    //加入pk過濾
                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                    {
                        //加入欄位
                        sbPk.Append(dc.ColumnName).Append("=").Append("@").Append(dc.ColumnName).Append(" AND ");
                        //加作Cmd參數
                        sqlCmds[i].Parameters.Add(new SqlParameter("@" + dc.ColumnName, dr[dc.ColumnName]));
                    }

                    //整合SQL語法
                    StringBuilder sbSql = new StringBuilder("DELETE FROM ")
                        .Append(ds.Tables[i].TableName)
                        .Append(" WHERE ")
                        .Append(sbPk.Remove(sbPk.Length - 5, 5));

                    SqlConnectionOpen(sqlCmds[i], sqlConn, sbSql.ToString());
                    sqlCmds[i].Transaction = sqlTrans;

                    foreach (DataColumn dc in ds.Tables[i].PrimaryKey)
                        sqlCmds[i].Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                    dCount[ds.Tables[i].TableName] += sqlCmds[i].ExecuteNonQuery();
                }
            }

            if (ds.Tables.Contains("sys_emDaily"))
                //更新員工記錄檔
                emDailyInsert(ds.Tables[ds.Tables.Count - 1], sqlTrans);
            SqlTransactionCommit(sqlTrans);
            rsp.ReturnData = dCount;
            rsp.Result = true;
        }
        catch (Exception ex)
        {
            rsp.ReturnMessage = ex.ToString();
            SqlTransactionRollback(sqlTrans);
            logger.Error(ex);logger_db.Error(ex);
        }
        finally
        {
        }
        return rsp;
    }

    #endregion

    #region 判斷是否為 主Key
    /// <summary>
    /// 判斷是否為 主Key
    /// </summary>
    /// <param name="primarykey">主Key欄位</param>
    /// <param name="dc">欲判斷欄位</param>
    /// <returns></returns>
    protected static bool IsPrimaryKey(DataColumn[] primarykey, DataColumn dc)
    {
        bool pk = false;
        foreach (DataColumn dcPk in primarykey)
        {
            if (dc.ColumnName == dcPk.ColumnName)
            {
                pk = true;
                break;
            }
        }
        return pk;
    }
    #endregion

    #region 異動員工維護資料
    /// <summary>
    /// 異動員工維護資料
    /// </summary>
    /// <param name="dt">資料</param>
    /// <param name="sqlTrans">交易處理</param>
    public static void emDailyInsert(DataTable dt, SqlTransaction sqlTrans)
    {
        try
        {
            SqlCommand sqlCmds = new SqlCommand();
            //取得欄位名稱
            StringBuilder sbFields = new StringBuilder();
            StringBuilder sbPara = new StringBuilder();
            DataRow row = dt.Rows[0];
            foreach (DataColumn dc in dt.Columns)
            {
                //加入欄位
                sbFields.Append(dc.ColumnName).Append(",");
                //加作SQL參數
                sbPara.Append("@").Append(dc.ColumnName).Append(",");
                //加作Cmd參數
                sqlCmds.Parameters.Add(new SqlParameter("@" + dc.ColumnName, row[dc.ColumnName]));
            }
            //整合SQL語法
            StringBuilder sbSql = new StringBuilder("INSERT INTO ")
                .Append(dt.TableName)
                .Append(" (")
                .Append(sbFields.Remove(sbFields.Length - 1, 1))
                .Append(" ) VALUES (")
                .Append(sbPara.Remove(sbPara.Length - 1, 1))
                .Append(" )");

            SqlConnectionOpen(sqlCmds, sqlTrans.Connection, sbSql.ToString());
            //設定交易處理
            sqlCmds.Transaction = sqlTrans;

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                    sqlCmds.Parameters["@" + dc.ColumnName].Value = dr[dc.ColumnName];

                //執行新增資料命令
                sqlCmds.ExecuteNonQuery();
            }
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
            throw new Exception();
        }
    }
    #endregion

    #region 取得伺服器日期時間
    /// <summary>
    /// 取得伺服器日期時間
    /// </summary>
    /// <param name="connAddress">平台</param>
    /// <returns></returns>
    public static DateTime GetServerDateTime(string connAddress)
    {
        try
        {
            string cmdText = "SELECT getDate()";
            object obj = SqlcommandExecuteScalar(connAddress, cmdText);
            return Convert.ToDateTime(obj);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
            throw new Exception();
        }

    }

    /// <summary>
    /// 取得伺服器日期時間
    /// </summary>
    /// <param name="sqlConn">資料庫連線</param>
    /// <returns></returns>
    public static DateTime GetServerDateTime(SqlConnection sqlConn)
    {
        try
        {
            string cmdText = "SELECT getDate()";
            object obj = SqlcommandExecuteScalar(cmdText, sqlConn);
            return Convert.ToDateTime(obj);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
            throw new Exception();
        }

    }

    /// <summary>
    /// 取得伺服器日期時間
    /// </summary>
    /// <param name="sqlTrans">交易處理</param>
    /// <returns></returns>
    public static DateTime GetServerDateTime(SqlTransaction sqlTrans)
    {
        try
        {
            string cmdText = "SELECT getDate()";
            object obj = SqlcommandExecuteScalar(cmdText, sqlTrans);
            return Convert.ToDateTime(obj);
        }
        catch (Exception ex)
        {
            logger.Error(ex);logger_db.Error(ex);
            throw new Exception();
        }

    }

    #endregion

    #region 截取帳號密碼
    private static string[] getAccountPwd()
    {
        string[] str = CONN_Main.Split(';');
        string[] account = str[2].Split('=');
        string[] pwd = str[3].Split('=');

        return new string[] { account[1], pwd[1] };
    }
    #endregion
}
