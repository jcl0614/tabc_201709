﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;

/// <summary>
/// DataModel 的摘要描述
/// </summary>
public class DataModel
{
    #region 顯示資料行
    #region BoundField
    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, int ItemStyleWidth)
    {
        return CreateBoundField(mDataField, mHeaderText, ItemStyleWidth, "Center" , false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, int ItemStyleWidth, bool sort)
    {
        return CreateBoundField(mDataField, mHeaderText, ItemStyleWidth, "Center", sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign)
    {
        return CreateBoundField(mDataField, mHeaderText, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, bool sort)
    {
        BoundField bf = new BoundField();
        return setBoundField(bf, mDataField, mHeaderText, ItemStyleWidth, mAlign, sort, "");
    }

    public static BoundField CreateBoundField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, bool sort, string SortExpression)
    {
        BoundField bf = new BoundField();
        return setBoundField(bf, mDataField, mHeaderText, ItemStyleWidth, mAlign, sort, SortExpression);
    }

    //共用
    /// <summary>
    /// BoundField 共用設定方法
    /// </summary>
    /// <param name="bf">BoundField</param>
    /// <param name="mDataField">資料欄位名稱</param>
    /// <param name="mHeaderText">標頭</param>
    /// <param name="ItemStyleWidth">欄位寬度</param>
    /// <param name="mAlign">對劑方式</param>
    /// <param name="sort">排序</param>
    private static BoundField setBoundField(BoundField bf, string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, bool sort, string SortExpression)
    {
        bf.HeaderText = mHeaderText;
        bf.DataField = mDataField;
        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        if (sort)
        {
            if(SortExpression == "")
                bf.SortExpression = mDataField;
            else
                bf.SortExpression = SortExpression;
        }
        switch (mAlign)
        {
            case "Left":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                break;
            case "Right":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                break;
            case "Center":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
            default :
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
        }
        return bf;
    }

    //----
    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, int ItemStyleWidth)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode, ItemStyleWidth, "Center", false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, int ItemStyleWidth, bool sort)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode, ItemStyleWidth, "Center", sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, int ItemStyleWidth, string mAlign)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, int ItemStyleWidth, string mAlign, bool sort)
    {
        BoundField bf = new BoundField();
        return setBoundField(bf, mDataField, mHeaderText, mHtmlEncode, ItemStyleWidth, mAlign, sort);
    }

    //共用
    /// <summary>
    /// BoundField 共用設定方法
    /// </summary>
    /// <param name="bf">BoundField</param>
    /// <param name="mDataField">資料欄位名稱</param>
    /// <param name="mHeaderText">標頭</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="ItemStyleWidth">欄位寬度</param>
    /// <param name="mAlign">對劑方式</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    private static BoundField setBoundField(BoundField bf, string mDataField, string mHeaderText, bool mHtmlEncode, int ItemStyleWidth, string mAlign, bool sort)
    {
        return setBoundField(bf, mDataField, mHeaderText, mHtmlEncode, "", ItemStyleWidth, mAlign, sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, string mDataFormatString, int ItemStyleWidth)
    {
        return CreateBoundField(mDataField, mHeaderText, mDataFormatString, ItemStyleWidth, "Center", false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, string mDataFormatString, int ItemStyleWidth, bool sort)
    {
        return CreateBoundField(mDataField, mHeaderText, mDataFormatString, ItemStyleWidth, "Center", sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>    
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, string mDataFormatString, int ItemStyleWidth, string mAlign)
    {
        return CreateBoundField(mDataField, mHeaderText, mDataFormatString, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, string mDataFormatString, int ItemStyleWidth, string mAlign, bool sort)
    {
        BoundField bf = new BoundField();
        return setBoundField(bf, mDataField, mHeaderText, mDataFormatString, ItemStyleWidth, mAlign, sort);
    }

    //-----
    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, string mDataFormatString, int ItemStyleWidth)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode, mDataFormatString, ItemStyleWidth, "Center", false);
    }

    // <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>     
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, string mDataFormatString, int ItemStyleWidth, bool sort)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode , mDataFormatString, ItemStyleWidth, "Center", sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>    
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, string mDataFormatString, int ItemStyleWidth, string mAlign)
    {
        return CreateBoundField(mDataField, mHeaderText, mHtmlEncode, mDataFormatString, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateBoundField(string mDataField, string mHeaderText, bool mHtmlEncode, string mDataFormatString, int ItemStyleWidth, string mAlign, bool sort)
    {
        BoundField bf = new BoundField();
        return setBoundField(bf, mDataField, mHeaderText, mHtmlEncode, mDataFormatString, ItemStyleWidth, mAlign, sort);
    }


    //-----

    //共用
    /// <summary>
    /// BoundField 共用設定方法
    /// </summary>
    /// <param name="bf">BoundField</param>
    /// <param name="mDataField">資料欄位名稱</param>
    /// <param name="mHeaderText">標頭</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">欄位寬度</param>
    /// <param name="mAlign">對劑方式</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    private static BoundField setBoundField(BoundField bf, string mDataField, string mHeaderText, string mDataFormatString, int ItemStyleWidth, string mAlign, bool sort)
    {
        return setBoundField(bf, mDataField, mHeaderText, true, mDataFormatString, ItemStyleWidth, mAlign, sort);
    }
        //共用
    /// <summary>
    /// BoundField 共用設定方法
    /// </summary>
    /// <param name="bf">BoundField</param>
    /// <param name="mDataField">資料欄位名稱</param>
    /// <param name="mHeaderText">標頭</param>
    /// <param name="mHtmlEncode">HTML 編碼(預設true)</param>
    /// <param name="mDataFormatString">顯示格式</param>
    /// <param name="ItemStyleWidth">欄位寬度</param>
    /// <param name="mAlign">對劑方式</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    private static BoundField setBoundField(BoundField bf, string mDataField, string mHeaderText, bool mHtmlEncode, string mDataFormatString, int ItemStyleWidth, string mAlign, bool sort)
    {
        bf.HeaderText = mHeaderText;
        bf.DataField = mDataField;
        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        bf.HtmlEncode = mHtmlEncode;
        bf.DataFormatString = mDataFormatString;
        if (sort)
            bf.SortExpression = mDataField;
        switch (mAlign)
        {
            case "Left":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                break;
            case "Right":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                break;
            case "Center":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
            default:
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
        }
        return bf;
    }

    #endregion

    #region CheckBoxField
    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateCheckBoxField(string mDataField, string mHeaderText, int ItemStyleWidth)
    {
        return CreateCheckBoxField(mDataField, mHeaderText, ItemStyleWidth, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static BoundField CreateCheckBoxField(string mDataField, string mHeaderText, int ItemStyleWidth, bool sort)
    {
        return CreateCheckBoxField(mDataField, mHeaderText, ItemStyleWidth, "Center", sort);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <returns></returns>
    public static BoundField CreateCheckBoxField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign)
    {
        return CreateCheckBoxField(mDataField, mHeaderText, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// 顯示資料行
    /// </summary>
    /// <param name="mDataField">欄位名稱 DataFieldName</param>
    /// <param name="mHeaderText">標題 HeaderText</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="mAlign">水平對齊</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static BoundField CreateCheckBoxField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, bool sort)
    {
        CheckBoxField bf = new CheckBoxField();
        return setBoundField(bf, mDataField, mHeaderText, ItemStyleWidth, mAlign, sort, "");
    }
    #endregion

    #region HyperLinkField
    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth)
    {
        return CreateHyperLinkField(mDataField, mHeaderText, ItemStyleWidth, false);
    }

    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth, bool sort)
    {
        return CreateHyperLinkField(mDataField, mHeaderText, ItemStyleWidth, "Center", false);
    }

    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign)
    {
        return CreateHyperLinkField(mDataField, mHeaderText, ItemStyleWidth, mAlign, false);
    }

    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, string mTarget)
    {
        return CreateHyperLinkField(mDataField, mHeaderText, ItemStyleWidth, mAlign, mTarget, false);
    }


    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, bool sort)
    {
        return CreateHyperLinkField(mDataField, mHeaderText, ItemStyleWidth, mAlign, "_blank", sort);
    }

    public static HyperLinkField CreateHyperLinkField(string mDataField, string mHeaderText, int ItemStyleWidth, string mAlign, string mTarget, bool sort)
    {
        StringBuilder sbUrl = new StringBuilder();
        HyperLinkField bf = new HyperLinkField();
        bf.HeaderText = mHeaderText;
        bf.DataTextField = mDataField;
        bf.Target = "blank";
        if (sort)
            bf.SortExpression = mDataField;
        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        bf.Target = mTarget;
        switch (mAlign)
        {
            case "Left":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                break;
            case "Right":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                break;
            case "Center":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
        }
       return bf;
    }
    #endregion

    #region ImageField
    public static ImageField CreateImageField(string mDataField, string mHeaderText, string ImageUrlFormatString, int ItemStyleWidth)
    {
        return CreateImageField(mDataField, mHeaderText, ImageUrlFormatString, ItemStyleWidth, false);
    }

    public static ImageField CreateImageField(string mDataField, string mHeaderText, string ImageUrlFormatString, int ItemStyleWidth, bool sort)
    {
        return CreateImageField(mDataField, mHeaderText, ImageUrlFormatString, ItemStyleWidth, "Center", sort);
    }
    
    public static ImageField CreateImageField(string mDataField, string mHeaderText, string ImageUrlFormatString, int ItemStyleWidth, string mAlign)
    {
        return CreateImageField(mDataField, mHeaderText, ImageUrlFormatString, ItemStyleWidth, mAlign, false);
    }

    /// <summary>
    /// ImageField
    /// </summary>
    /// <param name="mDataField">資料欄位名稱</param>
    /// <param name="mHeaderText">標頭</param>
    /// <param name="ImageUrlFormatString"></param>
    /// <param name="ItemStyleWidth"></param>
    /// <param name="mAlign">對劑方式</param>
    /// <param name="sort">排序</param>
    /// <returns></returns>
    public static ImageField CreateImageField(string mDataField, string mHeaderText, string ImageUrlFormatString, int ItemStyleWidth, string mAlign, bool sort)
    {
        StringBuilder sbUrl = new StringBuilder();
        ImageField bf = new ImageField();
        bf.HeaderText = mHeaderText;        
        bf.DataImageUrlField= mDataField;
        bf.DataImageUrlFormatString = ImageUrlFormatString;
        if (sort)
            bf.SortExpression = mDataField;
        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        switch (mAlign)
        {
            case "Left":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                break;
            case "Right":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                break;
            case "Center":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
        }
        return bf;
    }

    #endregion

    #region 顯示按鈕
    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="type">按鈕類型</param>
    /// <param name="mText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(ButtonType type, string mText, string commandName, int ItemStyleWidth)
    {
        return CreateButtonField(type, mText, commandName, "", ItemStyleWidth);
    }
    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="mHeaderText">表頭</param>
    /// <param name="type">按鈕類型</param>
    /// <param name="mText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(string mHeaderText, ButtonType type, string mText, string commandName, int ItemStyleWidth)
    {
        return CreateButtonField(mHeaderText, type, mText, commandName, "", ItemStyleWidth);
    }
    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="type">按鈕類型</param>
    /// <param name="mText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="imageUrl">影像圖示</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(ButtonType type, string mText, string commandName, string imageUrl, int ItemStyleWidth)
    {
        return CreateButtonField(type, "編輯", "",  mText, commandName, imageUrl, ItemStyleWidth, false);
    }

    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="mHeaderText">表頭</param>
    /// <param name="type">按鈕類型</param>
    /// <param name="mText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="imageUrl">影像圖示</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(string mHeaderText, ButtonType type, string mText, string commandName, string imageUrl, int ItemStyleWidth)
    {
        return CreateButtonField(mHeaderText, type, "", mText, commandName, imageUrl, ItemStyleWidth, false, "");
    }

    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="type">按鈕類型</param>
    /// <param name="mDataField">欄位</param>    
    /// <param name="mHeaderText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="imageUrl">影像圖示</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(ButtonType type, string mHeaderText, string mDataField, string mText, string commandName, string imageUrl, int ItemStyleWidth, bool sort)
    {
        return CreateButtonField(mHeaderText, type, mDataField, mText, commandName, imageUrl, ItemStyleWidth, sort, "");
    }
    public static ButtonField CreateButtonField(ButtonType type, string mHeaderText, string mDataField, string mText, string commandName, string imageUrl, int ItemStyleWidth, bool sort, string mAlign)
    {
        return CreateButtonField(mHeaderText, type, mDataField, mText, commandName, imageUrl, ItemStyleWidth, sort, mAlign);
    }

    /// <summary>
    /// 顯示按鈕
    /// </summary>
    /// <param name="mHeaderText">表頭</param>
    /// <param name="type">按鈕類型</param>
    /// <param name="mDataField">欄位</param>    
    /// <param name="mText">標題</param>
    /// <param name="commandName">命令名稱</param>
    /// <param name="imageUrl">影像圖示</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <param name="sort">排序</param>
    /// <returns>ButtonField</returns>
    public static ButtonField CreateButtonField(string mHeaderText, ButtonType type, string mDataField, string mText, string commandName, string imageUrl, int ItemStyleWidth, bool sort, string mAlign)
    {
        ButtonField bf = new ButtonField();
        bf.ButtonType = type;
        bf.CommandName = commandName;
        bf.HeaderText = mHeaderText;
        bf.Text = mText;
        bf.DataTextField = mDataField;
        if (sort)
            bf.SortExpression = mDataField;

        ///Button Type 為 Image才需設定imageUrl;
        if (type == ButtonType.Image) {
            if (imageUrl.Trim().Length == 0)
                throw new Exception(string.Format("{0}之ButtonType設定為Image時，需設定imageUrl", mText));
            bf.ImageUrl = imageUrl;
            
        }
        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        switch (mAlign)
        {
            case "Left":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Left;
                break;
            case "Right":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Right;
                break;
            case "Center":
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
            default:
                bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
                break;
        }
        return bf;
    }
    #endregion

    #region CommandField
    /// <summary>
    /// 命令按鈕
    /// </summary>
    /// <param name="type">按鈕類型</param>
    /// <param name="SelectButton">選取功能</param>
    /// <param name="EditButton">選取功能</param>
    /// <param name="DeleteButton">選取功能</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static CommandField CreateCommandField(ButtonType type, bool SelectButton, bool EditButton, bool DeleteButton, int ItemStyleWidth)
    {
        return CreateCommandField(type, "功能", SelectButton, EditButton, DeleteButton, ItemStyleWidth);
    }

    /// <summary>
    /// 命令按鈕
    /// </summary>
    /// <param name="type">按鈕類型</param>
    /// <param name="mHeaderText">表頭</param>
    /// <param name="SelectButton">選取功能</param>
    /// <param name="EditButton">選取功能</param>
    /// <param name="DeleteButton">選取功能</param>
    /// <param name="ItemStyleWidth">寬度</param>
    /// <returns></returns>
    public static CommandField CreateCommandField(ButtonType type, string mHeaderText, bool SelectButton, bool EditButton, bool DeleteButton, int ItemStyleWidth)
    {
        CommandField bf = new CommandField();
        bf.ButtonType = type;
        bf.EditText = "編輯";
        bf.UpdateText = "更新";
        bf.CancelText = "取消";
        bf.DeleteText = "刪除";

        //功能
        bf.ShowSelectButton = SelectButton;
        bf.ShowEditButton = EditButton;
        bf.ShowDeleteButton = DeleteButton;

        bf.HeaderText = mHeaderText;

        bf.ItemStyle.Width = Unit.Pixel(ItemStyleWidth);
        bf.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
        return bf;
    }
    #endregion
    #endregion

}
