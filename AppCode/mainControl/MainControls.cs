﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Runtime.Remoting.Contexts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Reflection;
using System.ComponentModel;

/// <summary>
/// MainControls 的摘要描述
/// </summary>
public class MainControls
{
    #region 以參數取出左邊起算第N個字元 或右邊起算第N個字元

    /// <summary>
    /// 以參數取出左邊起算第N個字元 或右邊起算第N個字元
    /// </summary>
    /// <param name="str">要處理的字串</param>
    /// <param name="form">L 或 R </param>
    /// <param name="N">第N</param>
    /// <returns></returns>
    public static string GetWord(string str, char form, int N)
    {
        try
        {
            if (N > str.Length)
                return str;

            //L R
            if (form == 'L' || form == 'l')
            {
                str = str.Substring(0, N);
            }
            else if (form == 'R' || form == 'r')
            {
                str = str.Substring(str.Length - N);
            }
        }
        catch { }
        return str;
    }

    #endregion

    #region 取得字串 某符號的左側或右側字元

    /// <summary>
    ///  以某符號分隔字串
    /// </summary>
    /// <param name="str">欲分隔字串</param>
    /// <param name="chr">分隔符號</param>
    /// <param name="index">給參數0表示取得符合左邊字串；給參數1表示取得符合右邊字串</param>
    /// <returns></returns>
    public static string GetSubString(string str, char chr, int index)
    {
        string[] temp = null;
        string replyValue = "";

        if (str.IndexOf(chr) >= 0)
        {
            temp = str.Trim().Split(chr);
            replyValue = temp[index].ToString().Trim();
        }
        return replyValue;
    }

    #endregion

    #region 分頁Sql語法
    /// <summary>
    /// 分頁Sql語法
    /// </summary>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="table">檔名</param>
    /// <param name="where">過濾條件</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelect(string columns, string table, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        columns = Lib.FdVP(columns);
        table = Lib.FdVP(table);

        string cmdtxt = "SELECT {COLUMNS} FROM (SELECT {COLUMNS},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM {TABLE} {WHERE}) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }
    /// <summary>
    /// 分頁Sql語法 For 虛擬欄位
    /// </summary>
    /// <param name="virtualColumns">包含虛擬欄位名稱</param>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="table">檔名</param>
    /// <param name="where">過濾條件</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelect(string virtualColumns, string columns, string table, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {COLUMNS},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM {TABLE} {WHERE} ) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }
    /// <summary>
    /// 分頁Sql語法 For 虛擬欄位
    /// </summary>
    /// <param name="virtualColumns">包含虛擬欄位名稱</param>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="table">檔名</param>
    /// <param name="table">GROUP BY</param>
    /// <param name="where">過濾條件</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelectGroup(string virtualColumns, string columns, string table, string group, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {COLUMNS},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM {TABLE} GROUP BY {GROUP} {WHERE} ) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{GROUP}", group);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }
    /// <summary>
    /// 分頁Sql語法 For 虛擬欄位 包兩層
    /// </summary>
    /// <param name="virtualColumns2">包含虛擬欄位名稱(外層)</param>
    /// <param name="virtualColumns1">包含虛擬欄位名稱(第二層)</param>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="table">檔名</param>
    /// <param name="where">過濾條件</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelect2L(string virtualColumns, string columns, string table, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS2} FROM (SELECT {VIRTUAL_COLUMNS1},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM (SELECT {COLUMNS} FROM {TABLE} {WHERE} ) {TABLE} ) {TABLE} WHERE RowNumber > 0 AND RowNumber <= 50 Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS2}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS1}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }

    /// <summary>
    /// 分頁Sql語法 For Join
    /// </summary>
    /// <param name="virtualColumns">包含虛擬欄位名稱</param>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="join">Join語法</param>
    /// <param name="table">檔名</param>
    /// <param name="where">過濾條件</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelect(string virtualColumns, string columns, string join, string table, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {COLUMNS},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM {JOIN} {WHERE}) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{JOIN}", join);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }

    public static string sqlSelectJoinGroup(string virtualColumns, string columns, string join, string table, string group, string where, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {COLUMNS},ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM {JOIN} {WHERE} GROUP BY {GROUP}) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{JOIN}", join);
        cmdtxt = cmdtxt.Replace("{GROUP}", group);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE}", where);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }

    /// <summary>
    /// 分頁Sql語法 -- UNION 欄位相同，無子查詢
    /// </summary>
    /// <param name="columns">包含實體欄位名稱及子查詢</param>
    /// <param name="table1">檔名1</param>
    /// <param name="table2">檔名2</param>
    /// <param name="where1">過濾條件1</param>
    /// <param name="where2">過濾條件2</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelectUnion(string columns, string table1, string table2, string where1, string where2, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {COLUMNS} FROM (SELECT {COLUMNS}, ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM (SELECT {COLUMNS} FROM {TABLE1} {WHERE1} UNION ALL SELECT {COLUMNS} FROM {TABLE2} {WHERE2}) {TABLE1} ) {TABLE1} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";

        cmdtxt = cmdtxt.Replace("{COLUMNS}", columns);
        cmdtxt = cmdtxt.Replace("{TABLE1}", table1);
        cmdtxt = cmdtxt.Replace("{TABLE2}", table2);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE1}", where1);
        cmdtxt = cmdtxt.Replace("{WHERE2}", where2.Trim().Length == 0 ? where1 : where2);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }

    /// <summary>
    /// 分頁Sql語法 -- UNION 欄位相同，有子查詢(虛擬欄位)
    /// </summary>
    /// <param name="virtualColumns">包含虛擬欄位名稱</param>
    /// <param name="columns1">包含實體欄位名稱及子查詢1</param>
    /// <param name="columns2">包含實體欄位名稱及子查詢2</param>
    /// <param name="table1">檔名1</param>
    /// <param name="table2">檔名2</param>
    /// <param name="where1">過濾條件1</param>
    /// <param name="where2">過濾條件2</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelectUnion(string virtualColumns, string columns1, string columns2, string table1, string table2, string where1, string where2, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {VIRTUAL_COLUMNS}, ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM (SELECT {COLUMNS1} FROM {TABLE1} {WHERE1} UNION ALL SELECT {COLUMNS2} FROM {TABLE2} {WHERE2}) {TABLE1}) {TABLE1} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS1}", columns1);
        cmdtxt = cmdtxt.Replace("{COLUMNS2}", columns2);
        cmdtxt = cmdtxt.Replace("{TABLE1}", table1);
        cmdtxt = cmdtxt.Replace("{TABLE2}", table2);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE1}", where1);
        cmdtxt = cmdtxt.Replace("{WHERE2}", where2.Trim().Length == 0 ? where1 : where2);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }

    /// <summary>
    /// 分頁Sql語法 For Join -- UNION 
    /// </summary>
    /// <param name="virtualColumns">包含虛擬欄位名稱</param>
    /// <param name="columns1">包含實體欄位名稱及子查詢1</param>
    /// <param name="columns2">包含實體欄位名稱及子查詢2</param>
    /// <param name="join1">Join1語法</param>
    /// <param name="join2">Join2語法</param>
    /// <param name="table">檔名</param>
    /// <param name="where1">過濾條件1</param>
    /// <param name="where2">過濾條件2</param>
    /// <param name="sortExpression">排序欄位</param>
    /// <param name="startRowIndex">起始位置</param>
    /// <param name="maximumRows">載入筆數</param>
    /// <returns></returns>
    public static string sqlSelectUnion(string virtualColumns, string columns1, string columns2, string join1, string join2, string table, string where1, string where2, string sortExpression, int startRowIndex, int maximumRows)
    {
        string cmdtxt = "SELECT {VIRTUAL_COLUMNS} FROM (SELECT {VIRTUAL_COLUMNS}, ROW_NUMBER() OVER(ORDER BY {SORT}) As RowNumber FROM (SELECT {COLUMNS1} FROM {JOIN1} {WHERE1} UNION ALL SELECT {COLUMNS2} FROM {JOIN2} {WHERE2}) {TABLE} ) {TABLE} WHERE RowNumber > {START} AND RowNumber <= {FETCH_SIZE} Order By RowNumber";
        cmdtxt = cmdtxt.Replace("{VIRTUAL_COLUMNS}", virtualColumns);
        cmdtxt = cmdtxt.Replace("{COLUMNS1}", columns1);
        cmdtxt = cmdtxt.Replace("{COLUMNS2}", columns2);
        cmdtxt = cmdtxt.Replace("{JOIN1}", join1);
        cmdtxt = cmdtxt.Replace("{JOIN2}", join2);
        cmdtxt = cmdtxt.Replace("{TABLE}", table);
        cmdtxt = cmdtxt.Replace("{SORT}", sortExpression);
        cmdtxt = cmdtxt.Replace("{WHERE1}", where1);
        cmdtxt = cmdtxt.Replace("{WHERE2}", where2.Trim().Length == 0 ? where1 : where2);
        cmdtxt = cmdtxt.Replace("{START}", startRowIndex.ToString());
        cmdtxt = cmdtxt.Replace("{FETCH_SIZE}", (startRowIndex + maximumRows).ToString());
        return cmdtxt;
    }


    #endregion

    #region 員工登入作業否
    /// <summary>
    /// 員工登入作業否
    /// </summary>
    /// <param name="mulv"></param>
    /// <returns></returns>
    public static bool MsUserLoingValid(string mulv)
    {
        return mulv.Trim() == "" ? true : false;
    }
    #endregion

    #region 選擇項判斷控制
    /// <summary>
    /// 選擇項判斷控制
    /// </summary>
    /// <param name="dist">元件ListControl</param>
    /// <param name="mSelectValue">值</param>
    public static void ddlIndexSelectValue(ListControl dist, string mSelectValue)
    {
        dist.ClearSelection();
        ListItem listItem = dist.Items.FindByValue(mSelectValue);
        if (listItem == null)
            dist.SelectedIndex = -1;
        else
            listItem.Selected = true;
    }
    public static void ddlIndexSelectValue(ListControl dist, string mSelectValue, int getLength)
    {
        ddlIndexSelectValue(dist, mSelectValue, 0, getLength);
    }

    /// <summary>
    /// 選擇項判斷控制
    /// </summary>
    /// <param name="dist">ListControl</param>
    /// <param name="mSelectValue">選項值</param>
    /// <param name="start">截取字串起始位置</param>
    /// <param name="getLength">截取字串長度</param>
    /// <returns></returns>
    public static void ddlIndexSelectValue(ListControl dist, string mSelectValue, int start, int getLength)
    {
        int m_dis = -1;
        for (int dis = 0; dis < dist.Items.Count; dis++)
        {
            if (dist.Items[dis].Value.Trim().Substring(start, getLength) == mSelectValue.Trim())
                m_dis = dis;
        }
        dist.SelectedIndex = m_dis;
    }


    public static void ddlIndexSelectText(ListControl dist, string mSelectText)
    {
        dist.ClearSelection();
        ListItem listItem = dist.Items.FindByText(mSelectText);
        if (listItem == null)
            dist.SelectedIndex = -1;
        else
            listItem.Selected = true;

        //int m_dis = 0;
        //for (int dis = 0; dis < dist.Items.Count; dis++)
        //{
        //    if (dist.Items[dis].Text.Trim() == mSelectText.Trim())
        //        m_dis = dis;
        //}
        //dist.SelectedIndex = m_dis;
    }
    public static void ddlIndexSelectText(ListControl dist, string mSelectText, int getLength)
    {
        ddlIndexSelectText(dist, mSelectText, 0, getLength);
    }


    /// <summary>
    /// 選擇項判斷控制
    /// </summary>
    /// <param name="dist">ListControl</param>
    /// <param name="mSelectText">顯示值</param>
    /// <param name="start">截取字串起始位置</param>
    /// <param name="getLength">截取字串長度</param>
    /// <returns></returns>
    public static void ddlIndexSelectText(ListControl dist, string mSelectText, int start, int getLength)
    {
        int m_dis = -1;
        for (int dis = 0; dis < dist.Items.Count; dis++)
        {
            if (dist.Items[dis].Text.Trim().Substring(start, getLength) == mSelectText.Trim())
                m_dis = dis;
        }
        dist.SelectedIndex = m_dis;
    }

    public static void ddlIndexSelectAttribute(ListControl dist, string attributes, DataTable dt, string mSelectText)
    {
        dist.ClearSelection();
        DataRow[] dr_list = dt.Select(string.Format("{0} = '{1}'", attributes, mSelectText));
        foreach (DataRow dr in dr_list)
        {
            dist.Items.FindByValue(dr["TypeNo"].ToString()).Selected = true;
        }
    }
    #endregion

    #region 設定選擇項
    /// <summary>
    /// 設定選擇項 DropDownList, RadioButtonList, CheckBoxList...All ListControl
    /// </summary>
    /// <param name="lc">ListControl元件</param>
    /// <param name="all">產生[*全部]選項</param>
    /// <param name="dItems">選項內容,格式(Value,Text):dItems.Add("Value1","Text1"); dItems.Add("Value2","Text2");</param>
    public static void ListControlAddItems(ListControl lc, bool all, Dictionary<string, string> dItems)
    {
        if (all)
            lc.Items.Add(new ListItem("全部", "*"));
        foreach (KeyValuePair<string, string> kvp in dItems)
            lc.Items.Add(new ListItem(kvp.Value, kvp.Key));
    }

    /// <summary>
    /// 設定選擇項 DropDownList, RadioButtonList, CheckBoxList...All ListControl
    /// </summary>
    /// <param name="lc">ListControl元件</param>
    /// <param name="all">產生[*全部]選項</param>
    /// <param name="dItems">選項內容,格式(Value,Text):string[] items = {"Value1,Text1", "Value2,Text2", ....}</param>
    public static void ListControlAddItems(ListControl lc, bool all, string[] dItems)
    {
        if (all)
            lc.Items.Add(new ListItem("全部", "*"));
        foreach (string str in dItems)
        {
            string[] item = str.Split(',');
            lc.Items.Add(new ListItem(item[1], item[0]));
        }
    }
    #endregion

    #region CheckBoxList資料轉至成字串
    /// <summary>
    /// CheckBoxList內容轉成字串
    /// </summary>
    /// <param name="mossRCBL">CheckBox</param>
    /// <returns>string</returns>
    public static string CheckBoxListJoinValues(CheckBoxList mossRCBL)
    {
        StringBuilder sbLimit = new StringBuilder();
        foreach (ListItem li in mossRCBL.Items)
            if (li.Selected)
                sbLimit.Append(li.Value);
        return sbLimit.ToString();
    }

    /// <summary>
    /// CheckBoxList內容轉成字串
    /// </summary>
    /// <param name="mossRCBL">CheckBoxList</param>
    /// <param name="getLength">取字串長度</param>
    /// <returns>string</returns>
    public static string CheckBoxListJoinValues(CheckBoxList mossRCBL, int getLength)
    {
        return CheckBoxListJoinValues(mossRCBL, 0, getLength);
    }

    /// <summary>
    /// CheckBoxList內容轉成字串
    /// </summary>
    /// <param name="mossRCBL">CheckBoxList</param>
    /// <param name="start">字中起始位置</param>
    /// <param name="getLenth">取字串長度</param>
    /// <returns>string</returns>
    public static string CheckBoxListJoinValues(CheckBoxList mossRCBL, int start, int getLenth)
    {
        StringBuilder sbLimit = new StringBuilder();
        foreach (ListItem li in mossRCBL.Items)
            if (li.Selected)
                sbLimit.Append(li.Value.Substring(start, getLenth));
        return sbLimit.ToString();
    }
    #endregion

    #region 資料轉至CheckBoxList
    /// <summary>
    /// 資料轉至CheckBoxList
    /// </summary>
    /// <param name="mossRCBL">CheckBoxList</param>
    /// <param name="values">Select Values</param>
    /// <param name="getLength">取內容長度</param>
    /// <returns>CheckBoxList</returns>
    public static void cblSelectedValue(CheckBoxList mossRCBL, string values, int getLength)
    {
        //預設起始位置為0
        cblSelectedValue(mossRCBL, values, 0, getLength);
    }

    /// <summary>
    /// 資料轉至CheckBoxList
    /// </summary>
    /// <param name="mossRCBL">CheckBoxList</param>
    /// <param name="values">Select Values</param>
    /// <param name="start">取內容起始位置</param>
    /// <param name="getLength">由起始位置取內容長度</param>
    /// <returns>CheckBoxList</returns>
    public static void cblSelectedValue(CheckBoxList mossRCBL, string values, int start, int getLength)
    {
        mossRCBL.ClearSelection();
        values = values.Trim();
        for (int i = 0; i < values.Length; i += getLength)
        {
            foreach (ListItem li in mossRCBL.Items)
                if (li.Value.Substring(start, getLength) == values.Substring(i, getLength))
                    li.Selected = true;
        }
    }
    #endregion

    #region 元件內容傳至DataSet
    public static DataTable UpLoadToDataTable(DataTable dt, Hashtable hsData)
    {
        DataRow row = dt.NewRow();
        List<string> remove_columnNames = new List<string>();
        foreach (DataColumn dc in dt.Columns)
        {
            if (hsData.ContainsKey(dc.ColumnName))
            {
                if (hsData[dc.ColumnName] == null || hsData[dc.ColumnName] == DBNull.Value || hsData[dc.ColumnName].ToString().Trim() == "")
                {
                    if (dc.AllowDBNull) //可接受DBNull.Value
                    {
                        if (hsData[dc.ColumnName] == null || hsData[dc.ColumnName] == DBNull.Value)
                            row[dc.ColumnName] = DBNull.Value;
                        else
                        {
                            if (dc.DataType != typeof(string))
                                row[dc.ColumnName] = DBNull.Value;
                            else
                                row[dc.ColumnName] = "";
                        }
                    }
                    else //無法接受DBNull.Value, 給預設值
                    {
                        if (dc.DataType == typeof(decimal) ||
                            dc.DataType == typeof(int))
                            row[dc.ColumnName] = 0;
                        else if (dc.DataType == typeof(DateTime))
                            row[dc.ColumnName] = DateTime.MinValue;
                        else if (dc.DataType == typeof(bool))
                            row[dc.ColumnName] = false;
                        else
                            row[dc.ColumnName] = "";
                    }

                }
                else
                {
                    if (dc.DataType == typeof(decimal))
                        row[dc.ColumnName] = Convert.ToDecimal(hsData[dc.ColumnName]);
                    else if (dc.DataType == typeof(int))
                        row[dc.ColumnName] = Convert.ToInt32(hsData[dc.ColumnName]);
                    else if (dc.DataType == typeof(DateTime))
                        row[dc.ColumnName] = Convert.ToDateTime(hsData[dc.ColumnName]);
                    else if (dc.DataType == typeof(bool))
                        row[dc.ColumnName] = Convert.ToBoolean(hsData[dc.ColumnName]);
                    else
                        row[dc.ColumnName] = hsData[dc.ColumnName];
                }
            }
            else
            {
                remove_columnNames.Add(dc.ColumnName);
                if (!dc.AllowDBNull)
                {
                    if (dc.DataType == typeof(decimal) ||
                            dc.DataType == typeof(int))
                        row[dc.ColumnName] = 0;
                    else if (dc.DataType == typeof(DateTime))
                        row[dc.ColumnName] = DateTime.MinValue;
                    else if (dc.DataType == typeof(bool))
                        row[dc.ColumnName] = false;
                    else
                        row[dc.ColumnName] = "";
                }
            }
        }
        dt.Rows.Add(row);
        foreach (var item in remove_columnNames)
            dt.Columns.Remove(item);
        return dt;
    }
    public static DataTable UpLoadToDataTable(DataTable dt, DataTable Data)
    {
        foreach (DataRow dr in Data.Rows)
        {
            DataRow row = dt.NewRow();
            List<string> remove_columnNames = new List<string>();
            foreach (DataColumn dc in dt.Columns)
            {
                if (Data.Columns.Contains(dc.ColumnName))
                {
                    if (dr[dc.ColumnName] == null || dr[dc.ColumnName] == DBNull.Value || dr[dc.ColumnName].ToString().Trim() == "")
                    {
                        if (dc.AllowDBNull) //可接受DBNull.Value
                        {
                            if (dr[dc.ColumnName] == null || dr[dc.ColumnName] == DBNull.Value)
                                row[dc.ColumnName] = DBNull.Value;
                            else
                            {
                                if (dc.DataType != typeof(string))
                                    row[dc.ColumnName] = DBNull.Value;
                                else
                                    row[dc.ColumnName] = "";
                            }
                        }
                        else //無法接受DBNull.Value, 給預設值
                        {
                            if (dc.DataType == typeof(decimal) ||
                                dc.DataType == typeof(int))
                                row[dc.ColumnName] = 0;
                            else if (dc.DataType == typeof(DateTime))
                                row[dc.ColumnName] = DateTime.MinValue;
                            else if (dc.DataType == typeof(bool))
                                row[dc.ColumnName] = false;
                            else
                                row[dc.ColumnName] = "";
                        }

                    }
                    else
                    {
                        if (dc.DataType == typeof(decimal))
                            row[dc.ColumnName] = Convert.ToDecimal(dr[dc.ColumnName]);
                        else if (dc.DataType == typeof(int))
                            row[dc.ColumnName] = Convert.ToInt32(dr[dc.ColumnName]);
                        else if (dc.DataType == typeof(DateTime))
                            row[dc.ColumnName] = Convert.ToDateTime(dr[dc.ColumnName]);
                        else if (dc.DataType == typeof(bool))
                            row[dc.ColumnName] = Convert.ToBoolean(dr[dc.ColumnName]);
                        else
                            row[dc.ColumnName] = dr[dc.ColumnName];
                    }
                }
                else
                {
                    remove_columnNames.Add(dc.ColumnName);
                    if (!dc.AllowDBNull)
                    {
                        if (dc.DataType == typeof(decimal) ||
                                dc.DataType == typeof(int))
                            row[dc.ColumnName] = 0;
                        else if (dc.DataType == typeof(DateTime))
                            row[dc.ColumnName] = DateTime.MinValue;
                        else if (dc.DataType == typeof(bool))
                            row[dc.ColumnName] = false;
                        else
                            row[dc.ColumnName] = "";
                    }
                }
            }
            dt.Rows.Add(row);
            foreach (var item in remove_columnNames)
                dt.Columns.Remove(item);
        }

        return dt;
    }
    public static DataTable UpLoadToDataTable(DataTable dt, Dictionary<string, object> dData)
    {
        DataRow row = dt.NewRow();
        List<string> remove_columnNames = new List<string>();
        foreach (DataColumn dc in dt.Columns)
        {
            if (dData.ContainsKey(dc.ColumnName))
            {
                if (dData[dc.ColumnName] == null || dData[dc.ColumnName] == DBNull.Value || dData[dc.ColumnName].ToString().Trim() == "")
                {
                    if (dc.AllowDBNull) //可接受DBNull.Value
                    {
                        if (dData[dc.ColumnName] == null || dData[dc.ColumnName] == DBNull.Value)
                            row[dc.ColumnName] = DBNull.Value;
                        else
                        {
                            if (dc.DataType != typeof(string))
                                row[dc.ColumnName] = DBNull.Value;
                            else
                                row[dc.ColumnName] = "";
                        }
                    }
                    else //無法接受DBNull.Value, 給預設值
                    {
                        if (dc.DataType == typeof(decimal) ||
                            dc.DataType == typeof(int))
                            row[dc.ColumnName] = 0;
                        else if (dc.DataType == typeof(DateTime))
                            row[dc.ColumnName] = DateTime.MinValue;
                        else
                            row[dc.ColumnName] = "";
                    }

                }
                else
                {
                    if (dc.DataType == typeof(decimal))
                        row[dc.ColumnName] = Convert.ToDecimal(dData[dc.ColumnName]);
                    else if (dc.DataType == typeof(int))
                        row[dc.ColumnName] = Convert.ToInt32(dData[dc.ColumnName]);
                    else if (dc.DataType == typeof(DateTime))
                        row[dc.ColumnName] = Convert.ToDateTime(dData[dc.ColumnName]);
                    else
                        row[dc.ColumnName] = dData[dc.ColumnName];
                }
            }
            else
            {
                remove_columnNames.Add(dc.ColumnName);
                if (!dc.AllowDBNull)
                {
                    if (dc.DataType == typeof(decimal) ||
                            dc.DataType == typeof(int))
                        row[dc.ColumnName] = 0;
                    else if (dc.DataType == typeof(DateTime))
                        row[dc.ColumnName] = DateTime.MinValue;
                    else if (dc.DataType == typeof(bool))
                        row[dc.ColumnName] = false;
                    else
                        row[dc.ColumnName] = "";
                }
            }
        }
        dt.Rows.Add(row);
        foreach (var item in remove_columnNames)
            dt.Columns.Remove(item);
        return dt;
    }
    #endregion

    #region 元件內容轉至SqlParameter
    public static Dictionary<string, SqlParameter> UpLoadToSqlParameters(Dictionary<string, object> data)
    {
        Dictionary<string, SqlParameter> dParas = new Dictionary<string, SqlParameter>();
        foreach (KeyValuePair<string, object> kvp in data)
            dParas.Add(kvp.Key, SqlAccess.CreateSqlParameter("@" + kvp.Key, kvp.Value));
        return dParas;
    }
    #endregion

    #region GridViewsSytle
    /// <summary>
    /// GridView 樣式設定
    /// </summary>
    /// <param name="mgv">GridView</param>
    /// <param name="width">寬度</param>
    public static void GridViewStyleCtrl(GridView mgv, int width)
    {
        GridViewStyleCtrl(mgv, width, false);
    }
    public static void GridViewStyleCtrl1(GridView mgv, int width)
    {
        GridViewStyleCtrl1(mgv, width, false);
    }
    public static void GridViewStyleCtrl2(GridView mgv, int width)
    {
        GridViewStyleCtrl2(mgv, width, false);
    }

    /// <summary>
    /// GridView 樣式設定
    /// </summary>
    /// <param name="mgv">GridView</param>
    /// <param name="width">寬度</param>
    /// <param name="width">啟用排序param>
    public static void GridViewStyleCtrl(GridView mgv, int width, bool sort)
    {
        mgv.Width = System.Web.UI.WebControls.Unit.Pixel(width);
        mgv.HeaderStyle.CssClass = "GridViewHead";
        mgv.CssClass = "GridViewItem";
        mgv.AlternatingRowStyle.CssClass = "GridViewAlterItem";
        mgv.SelectedRowStyle.CssClass = "GridViewSelectItem";
        mgv.AllowSorting = sort;
    }
    public static void GridViewStyleCtrl1(GridView mgv, int width, bool sort)
    {
        mgv.Width = System.Web.UI.WebControls.Unit.Pixel(width);
        mgv.HeaderStyle.CssClass = "GridViewHead1";
        //mgv.RowStyle.CssClass = "GridViewItem";
        mgv.AlternatingRowStyle.CssClass = "GridViewAlterItem";
        //mgv.SelectedRowStyle.CssClass = "GridViewSelectItem";
        mgv.AllowSorting = sort;
    }
    public static void GridViewStyleCtrl2(GridView mgv, int width, bool sort)
    {
        mgv.Width = System.Web.UI.WebControls.Unit.Pixel(width);
        mgv.HeaderStyle.CssClass = "GridViewHead2";
        mgv.RowStyle.CssClass = "GridViewItem";
        //mgv.AlternatingRowStyle.CssClass = "GridViewAlterItem";
        //mgv.SelectedRowStyle.CssClass = "GridViewSelectItem";
        mgv.AllowSorting = sort;
    }
    public static void GridViewStyleCtrl3(GridView mgv, int width, bool sort)
    {
        mgv.Width = System.Web.UI.WebControls.Unit.Pixel(width);
        mgv.HeaderStyle.CssClass = "GridViewHead1";
        mgv.RowStyle.CssClass = "GridViewItem2";
        //mgv.AlternatingRowStyle.CssClass = "GridViewAlterItem";
        //mgv.SelectedRowStyle.CssClass = "GridViewSelectItem";
        mgv.AllowSorting = sort;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="mStatus">onMouseOut:1、onMouseOver:2</param>
    /// <returns></returns>
    public static string GVItemStyleOMout(int mStatus)
    {
        string mstyle = "";
        switch (mStatus)
        {
            case 1:
                mstyle = "this.style.backgroundColor='#FFFFFF'";    //.backgroundColor='#FFFFFF'
                break;
            case 2:
                mstyle = "this.style.backgroundColor='#ffefdf'";   //background-image: url('images/GVSelect.jpg')    .backgroundColor='#6BB0F5'
                break;
            case 3:
                mstyle = "this.style.backgroundColor='NavajoWhite'";    //backgroundColor='#6BB0F5'
                break;
        }
        return mstyle;
    }
    public static string GVAlterItemStyleOMout(int mStatus)
    {
        string mstyle = "";
        switch (mStatus)
        {
            case 1:
                mstyle = "this.style.backgroundColor='#FAFAFA'";    //.backgroundColor='#D9F3FD'
                break;
            case 2:
                mstyle = "this.style.backgroundColor='#ffefdf'";    //backgroundColor='#6BB0F5'
                break;
            case 3:
                mstyle = "this.style.backgroundColor='#FFCCFF'";    //backgroundColor='#6BB0F5'
                break;
        }
        return mstyle;
    }
    public static string GVItemStyleOMout1(int mStatus)
    {
        string mstyle = "";
        switch (mStatus)
        {
            case 1:
                mstyle = "this.style.backgroundColor='#FFFFFF'";    //.backgroundColor='#FFFFFF'
                break;
            case 2:
                mstyle = "this.style.backgroundColor='#FEDADA'";   //background-image: url('images/GVSelect.jpg')    .backgroundColor='#6BB0F5'
                break;
            case 3:
                mstyle = "this.style.backgroundColor='NavajoWhite'";    //backgroundColor='#6BB0F5'
                break;
        }
        return mstyle;
    }
    public static string GVAlterItemStyleOMout1(int mStatus)
    {
        string mstyle = "";
        switch (mStatus)
        {
            case 1:
                mstyle = "this.style.backgroundColor='#F3F3F3'";    //.backgroundColor='#D9F3FD'
                break;
            case 2:
                mstyle = "this.style.backgroundColor='#FEDADA'";    //backgroundColor='#6BB0F5'
                break;
            case 3:
                mstyle = "this.style.backgroundColor='#FFCCFF'";    //backgroundColor='#6BB0F5'
                break;
        }
        return mstyle;
    }
    #endregion

    #region 設定GridView欄位隱藏
    public static void SetGridViewColHide(GridView mgv, string cols)
    {
        foreach (string field in cols.Split(','))
        {
            BoundField bf = DataModel.CreateBoundField(field, field, 10);
            bf.HeaderStyle.CssClass = "GridViewColHidden";
            bf.ItemStyle.CssClass = "GridViewColHidden";
            mgv.Columns.Add(bf);
        }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數10
    public static int GridViewPage10
    {
        get { return 10; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數15
    public static int GridViewPage15
    {
        get { return 15; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數20
    public static int GridViewPage20
    {
        get { return 20; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數25
    public static int GridViewPage25
    {
        get { return 25; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數30
    public static int GridViewPage30
    {
        get { return 30; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數50
    public static int GridViewPage50
    {
        get { return 50; }
    }
    #endregion

    #region 瀏覽 GridView 的預設每頁筆數100
    public static int GridViewPage100
    {
        get { return 100; }
    }
    #endregion

    #region 控制 GridView 可選擇的每頁筆數
    public static void GridViewPageSet(DropDownList mDDL, int GVPRs)
    {
        mDDL.Items.Clear();
        int[] lis = { 10, 15, 20, 25, 30, 50, 100 };
        foreach (int ls in lis)
        {
            ListItem li = new ListItem();
            li.Text = string.Format("每頁{0}筆", ls);
            li.Value = ls.ToString();
            mDDL.Items.Add(li);
        }
        MainControls.ddlIndexSelectValue(mDDL, GVPRs.ToString().Trim());
    }
    #endregion

    #region 控制 GridView 可選擇的頁數
    public static void GridViewPageChoices(DropDownList mDDL, int PageCounts, int StartValue)
    {
        mDDL.Items.Clear();
        if (PageCounts == 0)
            mDDL.SelectedIndex = -1;
        else
        {
            for (int ls = 0; ls < PageCounts; ls++)
            {
                ListItem li = new ListItem();
                li.Text = string.Format("第{0}頁", ls + StartValue);
                li.Value = (ls + StartValue).ToString();
                mDDL.Items.Add(li);
            }
            mDDL.SelectedIndex = 0;
        }
    }
    #endregion

    #region 顯示 GridView 目前資料總數
    public static string GridViewPageCountDisplay(int mGPC)
    {
        return string.Format("總筆數：<font class='GridPageCountInt'>{0}</font>", mGPC);
    }
    #endregion

    #region 換算 GridView 起始頁數
    public static int GridViewPageCount(int mGVPC, int mPageRows)
    {
        int mPC = 0;
        mPC = mGVPC / mPageRows;
        mPC += (mGVPC % mPageRows) == 0 ? 0 : 1;
        return mPC;
    }
    #endregion

    #region 換頁事件觸發處理 first.prev.next.last 等
    public static void GridViewChangePage(GridView mgv, string commandName)
    {
        switch (commandName)
        {
            case "First":
                mgv.PageIndex = 0;
                break;
            case "Prev":
                mgv.PageIndex -= 1;
                break;
            case "Next":
                mgv.PageIndex += 1;
                break;
            case "Last":
                mgv.PageIndex = mgv.PageCount - 1;
                break;
        }
    }
    #endregion

    #region 置換 &nbsp; to 空字串
    /// <summary>
    /// 置換空白符號
    /// </summary>
    /// <param name="source">原字串</param>
    /// <returns>置換後字串</returns>
    public static string ReplaceSpace(string source)
    {
        return WebUtility.HtmlDecode(source.Replace("&nbsp;", ""));
    }
    #endregion

    #region 驗証浮點數資料
    /// <summary>
    /// 驗証整數資料
    /// </summary>
    /// <param name="num">資料</param>
    /// <param name="intLength">整數長度</param>
    /// <returns></returns>
    public static bool CheckNumber(string num, int intLength)
    {
        return CheckNumber(num, intLength, 0);
    }
    /// <summary>
    /// 驗証浮點數資料
    /// </summary>
    /// <param name="num">資料</param>
    /// <param name="intLength">整數長度</param>
    /// <param name="decLength">小數長度</param>
    /// <returns></returns>
    public static bool CheckNumber(string num, int intLength, int decLength)
    {
        StringBuilder sbRegex = new StringBuilder();
        if (intLength <= 0 || decLength < 0)
            throw new Exception("整數長度不可設為0 或 小數位長度設為負數!!!");

        if (decLength == 0) //整數(小數位長度為零,表示是整數)
        {
            sbRegex.Append("^[-]?([0-9]{1,")
                .Append(intLength).Append("})?$");
        }
        else //小數
        {
            sbRegex.Append("^[-]?([0-9]{1,")
                .Append(intLength).Append("})?")
                .Append("(.[0-9]{1,")
                .Append(decLength)
                .Append("})?$");
        }

        Regex objPattern = new Regex(sbRegex.ToString());
        return objPattern.IsMatch(num);
    }

    /// <summary>
    /// 驗証整數資料
    /// </summary>
    /// <param name="num">資料</param>
    /// <param name="intLength">整數長度</param>
    /// <returns></returns>
    public static bool CheckIntNumber(string num)
    {
        int tmp = 0;
        return int.TryParse(num, out tmp);
    }

    public static bool CheckDecimalNumber(string num)
    {
        decimal tmp = 0;
        return decimal.TryParse(num, out tmp);
    }

    #endregion

    #region 資料格式驗証
    /// <summary>
    /// 資料格式驗証
    /// </summary>
    /// <param name="evdt_Format">要驗証的資料格式</param>
    /// <param name="evdt_Data">要驗証的資料</param>
    /// <returns>轉回true或false</returns>
    public static bool ValidDataType(string evdt_Format, string evdt_Data)
    {
        Regex m_evdt = new Regex(@evdt_Format);
        return (m_evdt.Match(evdt_Data).Success);
    }
    #endregion

    #region 驗証email
    public static bool ValidEmail(string email)
    {
        return ValidDataType(@"^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})*$", email);
    }
    #endregion

    #region 身分證驗證-TW
    public static bool checkSSN(string taiwanSSN)
    {
        System.Collections.ArrayList list = new System.Collections.ArrayList(taiwanSSN.ToCharArray());
        if (list.ToArray().Length == 10)
        {
            String[] alph = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };
            String[] num = { "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35" };
            int n = 0;
            for (int i = 0; i < alph.Length; i++)
            {
                if (list[0].ToString() == alph[i])
                    n = i;
            }
            Double tot1 = Convert.ToDouble(num[n].ToCharArray().GetValue(0).ToString()) + (Convert.ToDouble(num[n].ToCharArray().GetValue(1).ToString()) * 9);
            Double tot2 = 0;
            for (int i = 1; i < taiwanSSN.Length - 1; i++)
            {
                tot2 += Convert.ToDouble(list[i].ToString()) * (9 - i);
            }
            Double tot3 = Convert.ToDouble(list[9].ToString());
            Double tot4 = tot1 + tot2 + tot3;
            if ((tot4 % 10) != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
            return false;
    }
    #endregion

    #region 檢核日期資料(西元年)
    /// <summary>
    /// 檢核日期資料(西元年)
    /// </summary>
    /// <param name="date">日期資料</param>
    /// <returns></returns>
    public static bool ValidDate(string date)
    {
        return ValidDate(date, "yyyy/M/d");
    }
    /// <summary>
    /// 檢核日期資料(西元年)
    /// </summary>
    /// <param name="date">日期資料</param>
    /// <param name="format">日期格式</param>
    /// <returns></returns>
    public static bool ValidDate(string date, string format)
    {
        DateTime dt = new DateTime();
        return DateTime.TryParseExact(date, format, null, System.Globalization.DateTimeStyles.None, out dt);
    }
    #endregion

    #region 計算目前指標位置
    /// <summary>
    /// 計算目前指標位置
    /// </summary>
    /// <param name="pageIndex">頁數</param>
    /// <param name="pageSize">每頁筆數</param>
    /// <param name="rowIndex">資料索引位置</param>
    /// <returns></returns>
    public static int GetRowIndex(int pageIndex, int pageSize, int rowIndex)
    {
        return (pageIndex * pageSize) + rowIndex;
    }
    #endregion

    #region 設定排序
    /// <summary>
    /// 設定DataTable 與 GridView 排序方式相同
    /// </summary>
    /// <param name="mdt">資料表</param>
    /// <param name="mgv">GridView</param>
    /// <returns></returns>
    public static DataView setSort(DataTable mdt, GridView mgv)
    {
        DataView dv = mdt.DefaultView;
        dv.Sort = mgv.SortExpression;
        if (mgv.SortDirection == SortDirection.Descending)
            dv.Sort += " DESC";

        return dv;
    }
    #endregion

    #region 發送EMail
    /// <param name="from">寄件者</param> //不得為空值
    /// <param name="to">收件者</param> //不得為空值
    /// <param name="CC">副本</param> //空值不寄發；mail以「;」區隔
    /// <param name="BCC">密件副本</param> //空值不寄發；mail以「;」區隔
    /// <param name="subject">主題</param> //不得為空值
    /// <param name="body">內文</param> //不得為空值
    /// <param name="file">附加檔案</param> //若為空值，視為無附加檔案
    /// <param name="displayName">寄件者顯示名稱</param> //空值則顯示MAIL
    /// <returns></returns>
    public static RServiceProvider sendMail(string from, string to, string CC, string BCC, string subject, string body, string file, string displayName, Boolean html)
    {
        RServiceProvider rsp = new RServiceProvider();
        try
        {
            MailMessage SysMail = new MailMessage();

            if (displayName != "")
                SysMail.From = new MailAddress(from, displayName);
            else
                SysMail.From = new MailAddress(from);
            SysMail.To.Add(new MailAddress(to));

            if (CC != "")
            {
                string[] ccString = CC.Split(';');
                foreach (string s in ccString)
                    if (s != null && s != "")
                        SysMail.CC.Add(new MailAddress(s)); //副本
            }
            if (BCC != "")
            {
                string[] bccString = BCC.Split(';');
                foreach (string s in bccString)
                    if (s != null && s != "")
                        SysMail.Bcc.Add(s); //密件副本
            }

            SysMail.Subject = subject;
            SysMail.SubjectEncoding = Encoding.UTF8;

            SysMail.IsBodyHtml = html;
            SysMail.Body = body;
            SysMail.BodyEncoding = Encoding.UTF8;
            SysMail.SubjectEncoding = Encoding.UTF8;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = new WebSetValue().WebMailServer;
            smtpClient.Port = 25;
            smtpClient.Credentials = new NetworkCredential(Lib.PuchAesChinese(new WebSetValue().WebSendMailID, DataSysValue.eMail), Lib.PuchAesChinese(new WebSetValue().WebSendMailPWD, DataSysValue.eMail));
            smtpClient.EnableSsl = false;

            //設定附件檔案(Attachment) 
            if (file.Trim() != "")
            {
                string[] files = file.Split(';');
                foreach (string item in files)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(item);
                        attachment1.Name = System.IO.Path.GetFileName(item);
                        attachment1.NameEncoding = Encoding.UTF8;
                        attachment1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                        // 設定該附件為一個內嵌附件(Inline Attachment)
                        attachment1.ContentDisposition.Inline = true;
                        attachment1.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;

                        SysMail.Attachments.Add(attachment1);
                    }
                }
            }

            smtpClient.Send(SysMail);
            rsp.Result = true;
        }
        catch (Exception e)
        {
            rsp.Result = false;
            rsp.ReturnMessage = e.Message;
        }
        return rsp;
    }
    /// <param name="from">寄件者</param> //不得為空值
    /// <param name="to">收件者</param> //不得為空值
    /// <param name="CC">副本</param> //空值不寄發；mail以「;」區隔
    /// <param name="BCC">密件副本</param> //空值不寄發；mail以「;」區隔
    /// <param name="subject">主題</param> //不得為空值
    /// <param name="body">內文</param> //不得為空值
    /// <param name="files">附加檔案</param> //若為空值，視為無附加檔案
    /// <param name="displayName">寄件者顯示名稱</param> //空值則顯示MAIL
    /// <returns></returns>
    public static RServiceProvider sendMail(string from, string to, string CC, string BCC, string subject, string body, Dictionary<String, Stream> files, string displayName, Boolean html)
    {
        RServiceProvider rsp = new RServiceProvider();
        try
        {
            MailMessage SysMail = new MailMessage();

            if (displayName != "")
                SysMail.From = new MailAddress(from, displayName);
            else
                SysMail.From = new MailAddress(from);
            SysMail.To.Add(new MailAddress(to));

            if (CC != "")
            {
                string[] ccString = CC.Split(';');
                foreach (string s in ccString)
                    if (s != null && s != "")
                        SysMail.CC.Add(new MailAddress(s)); //副本
            }
            if (BCC != "")
            {
                string[] bccString = BCC.Split(';');
                foreach (string s in bccString)
                    if (s != null && s != "")
                        SysMail.Bcc.Add(s); //密件副本
            }

            SysMail.Subject = subject;
            SysMail.SubjectEncoding = Encoding.UTF8;

            SysMail.IsBodyHtml = html;
            SysMail.Body = body;
            SysMail.BodyEncoding = Encoding.UTF8;
            SysMail.SubjectEncoding = Encoding.UTF8;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = new WebSetValue().WebMailServer;
            smtpClient.Port = 25;
            smtpClient.Credentials = new NetworkCredential(Lib.PuchAesChinese(new WebSetValue().WebSendMailID, DataSysValue.eMail), Lib.PuchAesChinese(new WebSetValue().WebSendMailPWD, DataSysValue.eMail));
            smtpClient.EnableSsl = false;

            //設定附件檔案(Attachment) 
            if (files.Count != 0)
            {
                foreach (var item in files)
                {
                    if (!string.IsNullOrEmpty(item.Key) && item.Value.Length != 0)
                    {
                        System.Net.Mail.Attachment attachment1 = new System.Net.Mail.Attachment(item.Value, item.Key);
                        attachment1.Name = item.Key;
                        attachment1.NameEncoding = Encoding.UTF8;
                        attachment1.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                        // 設定該附件為一個內嵌附件(Inline Attachment)
                        attachment1.ContentDisposition.Inline = true;
                        attachment1.ContentDisposition.DispositionType = System.Net.Mime.DispositionTypeNames.Inline;

                        SysMail.Attachments.Add(attachment1);
                    }
                }
            }

            smtpClient.Send(SysMail);
            rsp.Result = true;
        }
        catch (Exception e)
        {
            rsp.Result = false;
            rsp.ReturnMessage = e.Message;
        }
        return rsp;
    }
    #endregion

    #region 訊息視窗 (UpdatePanel + javascript)
    public static void showMsg(Control ctl, string msg)
    {
        //1.沒寫<script></script> =true  反之則用false
        ScriptManager.RegisterClientScriptBlock(ctl, HttpContext.Current.GetType(), "Alert", string.Format("alert('{0}');", msg), true);
    }
    #endregion

    #region 網頁直接列印 (UpdatePanel + javascript)
    public static void Print(Control ctl, string code1, string code2)
    {
        //1.沒寫<script></script> =true  反之則用false
        ScriptManager.RegisterClientScriptBlock(ctl, HttpContext.Current.GetType(), "PagePrint", string.Format("WebBrowser.ExecWB({0},{1});", code1, code2), true);
    }
    public static void ReYoPrint(Control ctl)
    {
        //1.沒寫<script></script> =true  反之則用false
        ScriptManager.RegisterClientScriptBlock(ctl, HttpContext.Current.GetType(), "ReYoPrint", "ReYoPrint.PrintDirect();", true);
    }
    #endregion

    #region TextBox元件 focus, 內容會變成被選取狀態
    /// <summary>
    /// TextBox元件 focus, 內容會變成被選取狀態
    /// </summary>
    /// <param name="ctrl"></param>
    public static void SelectTextAll(TextBox ctrl)
    {
        ctrl.Attributes.Add("onfocus", "this.select();");
    }
    #endregion

    #region 檢核DataSet是否有資料
    /// <summary>
    /// 檢核DataSet是否有資料
    /// </summary>
    /// <param name="ds">資料集</param>
    /// <returns></returns>
    public static bool checkDataSet(object data)
    {
        if (data is DataSet)
        {
            DataSet ds = data as DataSet;
            return ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0;
        }
        else if (data is DataTable)
        {
            DataTable dt = data as DataTable;
            return dt != null && dt.Rows.Count > 0;
        }
        return false;
    }
    #endregion

    #region 防止按鈕連按兩次以上
    /// <summary>
    /// 防止按鈕連按兩次以上
    /// </summary>
    /// <param name="p">Page</param>
    /// <param name="ctrl">元件</param>
    public static void submitonce(Page p, WebControl ctrl)
    {
        ctrl.Attributes["Onclick"] = "this.disabled=true;" + p.ClientScript.GetPostBackEventReference(ctrl, "");
    }
    /// <summary>
    /// 防止按鈕連按兩次以上
    /// </summary>
    /// <param name="p">Page</param>
    /// <param name="ctrl">元件</param>
    /// <param name="confirmMessage">確認訊息</param>
    public static void submitonce(Page p, WebControl ctrl, string confirmMessage)
    {
        ctrl.Attributes["Onclick"] = string.Format("if(confirm('{0}')==false) return false; this.disabled=true; {1}", confirmMessage, p.ClientScript.GetPostBackEventReference(ctrl, ""));
    }
    #endregion

    #region 驗証上傳圖檔副檔名
    public static bool checkImgSubFileName(string mFileName)
    {
        string[] fn = mFileName.ToLower().Split('.');
        return "gifpngjpgjpeg".IndexOf(fn[fn.Length - 1]) >= 0 ? true : false;
    }
    #endregion

    #region 驗証上傳XLS副檔名
    public static bool checkXLSSubFileName(string mFileName)
    {
        string[] fn = mFileName.ToLower().Split('.');
        return "xlscsv".IndexOf(fn[fn.Length - 1]) >= 0 ? true : false;
    }
    #endregion

    #region 驗証上傳副檔名
    public static bool checkSubFileName(string mFileName, string subFileName)
    {
        string[] fn = mFileName.ToLower().Split('.');
        return subFileName.IndexOf(fn[fn.Length - 1]) >= 0 ? true : false;
    }
    #endregion

    #region 上傳影像檔案的作業
    public static void FileUpdate(FileUpload mFUname, TextBox txtfield, Label msglbl, string filePath) //存檔使用原檔名
    {
        msglbl.Text = "";
        try
        {
            if (mFUname.FileName.Trim().Length != 0)
            {
                if (MainControls.checkImgSubFileName(mFUname.FileName))
                {
                    mFUname.PostedFile.SaveAs(string.Format("{0}{1}", filePath, mFUname.FileName));
                    txtfield.Text = mFUname.FileName;
                    msglbl.Text = " 檔案上傳成功！";
                }
                else
                    msglbl.Text = " 檔案格式錯誤！";
            }
        }
        catch { msglbl.Text = " 檔案上傳失敗！"; }
    }
    public static void FileUpdate2(FileUpload mFUname, TextBox txtfield, Label msglbl, string filePath) //存檔使用其它檔名
    {
        msglbl.Text = "";
        try
        {
            if (mFUname.FileName.Trim().Length != 0)
            {
                if (MainControls.checkImgSubFileName(mFUname.FileName))
                {
                    if (txtfield.Text.Trim() != "")
                    {
                        mFUname.PostedFile.SaveAs(string.Format("{0}{1}", filePath, txtfield.Text.Trim()));
                        txtfield.Text = txtfield.Text;
                    }
                    else
                    {
                        mFUname.PostedFile.SaveAs(string.Format("{0}{1}", filePath, mFUname.FileName.Trim()));
                        txtfield.Text = mFUname.FileName;
                    }
                    msglbl.Text = " 檔案上傳成功！";
                }
                else
                    msglbl.Text = " 檔案格式錯誤！";
            }
        }
        catch { msglbl.Text = " 檔案上傳失敗！"; }
    }
    #endregion

    #region 超連結切換頁面選單
    public static void HyperLinkTargetDDL(DropDownList mDDL)
    {
        string[] mHLT = {  "_self,在目前的頁框開啟連結"
                          ,"_blank,在新的瀏覽器視窗上"
                          ,"_parent,在目前視窗之上一層"
                          ,"_top,在目前視窗中開啟（頁框用）" };
        mDDL.Items.Clear();
        foreach (string str in mHLT)
        {
            string[] sr = str.Split(',');
            ListItem lt = new ListItem();
            lt.Text = sr[1];
            lt.Value = sr[0];
            mDDL.Items.Add(lt);
        }
    }
    #endregion

    #region  取得伺服器時間
    public static DateTime ServerDateTime()
    {
        DataSet emds = new DataSet();
        string serverTime = "";
        serverTime = SqlAccess.SqlcommandExecute("0", "SELECT CONVERT(char(19), getdate(), 120)").Tables[0].Rows[0][0].ToString();
        if (serverTime != "")
            return DateTime.Parse(serverTime);
        else
            return DateTime.Now;
    }
    public static string GetDate(string mode)
    {
        DataSet emds = new DataSet();
        string serverTime = "";
        switch (mode)
        {
            case "Date": serverTime = SqlAccess.SqlcommandExecute("0", "SELECT CONVERT(varchar(10), GETDATE(), 111)").Tables[0].Rows[0][0].ToString();// 2012/02/10
                break;
            case "Time": serverTime = SqlAccess.SqlcommandExecute("0", "SELECT CONVERT(varchar(8), GETDATE(), 108)").Tables[0].Rows[0][0].ToString();// 16:11:10
                break;
            default: serverTime = SqlAccess.SqlcommandExecute("0", "SELECT CONVERT(char(19), getdate(), 120)").Tables[0].Rows[0][0].ToString();// 2012-02-10 16:11:10
                break;
        }
        return serverTime;
    }
    public static string GetDateStr()
    {
        DataSet emds = new DataSet();
        string serverTime = SqlAccess.SqlcommandExecute("0", "select replace(replace(replace(CONVERT(varchar, getdate(), 120 ),'-',''),' ',''),':','')").Tables[0].Rows[0][0].ToString();// 20120210161110
        return serverTime;
    }
    #endregion

    #region  取得伺服器時間
    public static int DateDiffDays(DateTime d1, DateTime d2)
    {
        return d1.Subtract(d2).Days;
    }
    #endregion

    #region '西元年'下拉式選單
    public static void yearDDL(DropDownList ddl, Boolean b)//false 表示滿18歲
    {
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem("", ""));
        int lastIndex = 0;
        if (b)
            lastIndex = int.Parse(((DateTime)SqlAccess.GetServerDateTime("0")).Year.ToString());
        else
            lastIndex = int.Parse(((DateTime)SqlAccess.GetServerDateTime("0")).Year.ToString()) - 17;
        for (int i = lastIndex; i >= 1911; i--)
        {
            ListItem lt = new ListItem();
            lt.Text = i.ToString();
            lt.Value = i.ToString();
            ddl.Items.Add(lt);
        }
    }
    #endregion

    #region '月份'下拉式選單
    public static void momthDDL(DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem("", ""));
        for (int i = 1; i <= 12; i++)
        {
            ListItem lt = new ListItem();
            lt.Text = i.ToString("00");
            lt.Value = i.ToString("00");
            ddl.Items.Add(lt);
        }
    }
    #endregion

    #region '日期'下拉式選單
    public static void dayDDL(DropDownList ddl, int year, int momth)
    {
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem("", ""));
        for (int i = 1; i <= DateTime.DaysInMonth(year, momth); i++)
        {
            ListItem lt = new ListItem();
            lt.Text = i.ToString("00");
            lt.Value = i.ToString("00");
            ddl.Items.Add(lt);
        }
    }
    #endregion

    #region 產生自動號碼
    public static string random(string mode, int length)
    {
        string s = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        Random rnd = new Random(Guid.NewGuid().GetHashCode());
        StringBuilder rndString = new StringBuilder();
        for (int i = 0; i < length; i++)
            switch (mode)
            {
                case "1"://純數字
                    rndString.Append(s.Substring(rnd.Next(0, 9), 1));
                    break;
                case "2"://純字母大寫
                    rndString.Append(s.Substring(rnd.Next(10, 35), 1));
                    break;
                case "3"://純字母小寫
                    rndString.Append(s.Substring(rnd.Next(36, 35), 1));
                    break;
                case "4"://純數字+字母大寫
                    rndString.Append(s.Substring(rnd.Next(0, 35), 1));
                    break;
                case "5"://純數字+字母小寫
                    rndString.Append(s.Substring(rnd.Next(0, 35), 1).ToLower());
                    break;
                case "6"://純字母大小寫
                    rndString.Append(s.Substring(rnd.Next(10, 61), 1));
                    break;
                case "0"://全部
                    rndString.Append(s.Substring(rnd.Next(0, 61), 1));
                    break;
                default:
                    rndString.Append(s.Substring(rnd.Next(0, 61), 1));
                    break;

            }
        return rndString.ToString();
    }
    #endregion

    #region 隨機不重覆
    /// <summary>
    /// 產生亂數。
    /// </summary>
    /// <param name="start">起始數字。</param>
    /// <param name="end">結束數字。</param>        
    public static int[] GenerateRandom(int start, int end)
    {
        int iLength = end - start + 1;
        int[] arrList = new int[iLength];
        for (int N1 = 0; N1 < iLength; N1++)
        {
            arrList[N1] = N1 + start;
        }

        arrList = Shuffle<int>(arrList);
        return arrList;
    }
    //洗牌。
    static T[] Shuffle<T>(IEnumerable<T> values)
    {
        List<T> list = new List<T>(values);
        T tmp;
        int iS;
        Random r = new Random();
        for (int N1 = 0; N1 < list.Count; N1++)
        {
            iS = r.Next(N1, list.Count);
            tmp = list[N1];
            list[N1] = list[iS];
            list[iS] = tmp;
        }
        return list.ToArray();
    }
    #endregion

    #region  日期對應星座
    public static string StarString(string date)
    {
        DataSet emds = new DataSet();
        StringBuilder cmdtxt = new StringBuilder("SELECT ");
        DateTime dt = new DateTime();
        if (DateTime.TryParseExact(date, "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.None, out dt))
        {
            string year = date.Substring(0, 4);
            cmdtxt.Append("CASE ");
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/01/20' AND '{0}/02/18' THEN '水瓶座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/02/19' AND '{0}/03/20' THEN '雙魚座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/03/21' AND '{0}/04/20' THEN '牡羊座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/04/21' AND '{0}/05/20' THEN '金牛座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/05/21' AND '{0}/06/21' THEN '雙子座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/06/22' AND '{0}/07/22' THEN '巨蟹座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/07/23' AND '{0}/08/22' THEN '獅子座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/08/23' AND '{0}/09/22' THEN '處女座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/09/23' AND '{0}/10/22' THEN '天秤座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/10/23' AND '{0}/11/21' THEN '天蠍座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/11/22' AND '{0}/12/21' THEN '射手座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/12/22' AND '{0}/12/31' THEN '摩羯座' ", year);
            cmdtxt.AppendFormat("WHEN @date BETWEEN '{0}/01/01' AND '{0}/01/19' THEN '摩羯座' ", year);
            cmdtxt.Append("ELSE '' ");
            cmdtxt.Append("END ");

            emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), SqlAccess.CreateSqlParameter("@date", date));
            return emds.Tables[0].Rows[0][0].ToString();
        }
        else if (DateTime.TryParseExact(date, "MM/dd", null, System.Globalization.DateTimeStyles.None, out dt))
        {
            cmdtxt.Append("CASE ");
            cmdtxt.Append("WHEN @date BETWEEN '01/20' AND '02/18' THEN '水瓶座' ");
            cmdtxt.Append("WHEN @date BETWEEN '02/19' AND '03/20' THEN '雙魚座' ");
            cmdtxt.Append("WHEN @date BETWEEN '03/21' AND '04/20' THEN '牡羊座' ");
            cmdtxt.Append("WHEN @date BETWEEN '04/21' AND '05/20' THEN '金牛座' ");
            cmdtxt.Append("WHEN @date BETWEEN '05/21' AND '06/21' THEN '雙子座' ");
            cmdtxt.Append("WHEN @date BETWEEN '06/22' AND '07/22' THEN '巨蟹座' ");
            cmdtxt.Append("WHEN @date BETWEEN '07/23' AND '08/22' THEN '獅子座' ");
            cmdtxt.Append("WHEN @date BETWEEN '08/23' AND '09/22' THEN '處女座' ");
            cmdtxt.Append("WHEN @date BETWEEN '09/23' AND '10/22' THEN '天秤座' ");
            cmdtxt.Append("WHEN @date BETWEEN '10/23' AND '11/21' THEN '天蠍座' ");
            cmdtxt.Append("WHEN @date BETWEEN '11/22' AND '12/21' THEN '射手座' ");
            cmdtxt.Append("WHEN @date BETWEEN '12/22' AND '01/19' THEN '摩羯座' ");
            cmdtxt.Append("ELSE '' ");
            cmdtxt.Append("END ");

            emds = SqlAccess.SqlcommandExecute("0", cmdtxt.ToString(), SqlAccess.CreateSqlParameter("@date", date));
            return emds.Tables[0].Rows[0][0].ToString();
        }
        else
            return "";


    }
    #endregion

    #region 播放WAV音效
    public static void PlaySound(string Path)
    {
        if (Path.Length == 0 || Path.IndexOf(".wav") == -1)
        {
            return;
        }
        System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
        sp.SoundLocation = Path;
        sp.Play();
    }
    #endregion

    #region 取HTML純文字
    public static string htmlText(string strHtml)
    {
        string[] aryReg ={
          @"<script[^>]*?>.*?</script>",
          @"<(\/\s*)?!?((\w+:)?\w+)(\w+(\s*=?\s*(([""'])(\\[""'tbnr]|[^\7])*?\7|\w+)|.{0})|\s)*?(\/\s*)?>",
          @"<[^>]*? \/>",
          @"([\r\n])[\s]+",
          @"&(quot|#34);",
          @"&(amp|#38);",
          @"&(lt|#60);",
          @"&(gt|#62);", 
          @"&(nbsp|#160);", 
          @"&(iexcl|#161);",
          @"&(cent|#162);",
          @"&(pound|#163);",
          @"&(copy|#169);",
          @"&#(\d+);",
          @"-->",
          @"<!--.*\n"
         
         };

        string[] aryRep = {
           "",
           "",
           "",
           "",
           "\"",
           "&",
           "<",
           ">",
           " ",
           "\xa1",//chr(161),
           "\xa2",//chr(162),
           "\xa3",//chr(163),
           "\xa9",//chr(169),
           "",
           "\r\n",
           ""
          };

        string newReg = aryReg[0];
        string strOutput = strHtml;
        for (int i = 0; i < aryReg.Length; i++)
        {
            Regex regex = new Regex(aryReg[i], RegexOptions.IgnoreCase);
            strOutput = regex.Replace(strOutput, aryRep[i]);
        }

        strOutput.Replace("<", "");
        strOutput.Replace(">", "");
        strOutput.Replace("\r\n", "");

        return strOutput;
    }
    #endregion

    #region FTP取得'目錄中的檔案'或'檔案'
    public static string[] FtpFileList(string listUrl, string fileType, string UserName, string Password)
    {
        string[] s = null;
        StreamReader reader = null;
        try
        {
            FtpWebRequest listRequest = (FtpWebRequest)WebRequest.Create(listUrl);
            if (UserName.Length > 0)//如果需要帳號登入
            {
                NetworkCredential nc = new NetworkCredential(UserName, Password);
                listRequest.Credentials = nc; //設定帳號
            }
            listRequest.UsePassive = false;
            listRequest.Method = WebRequestMethods.Ftp.ListDirectory;// //設定Method取得目錄資訊
            FtpWebResponse listResponse = (FtpWebResponse)listRequest.GetResponse();
            reader = new StreamReader(listResponse.GetResponseStream());
            StringBuilder sb = new StringBuilder();
            while (reader.Peek() >= 0)
            {//開始讀取回傳資訊
                string data = reader.ReadLine().ToString();
                if (fileType == "")
                {
                    if (data != "" && data != null && (data.LastIndexOf(".jpg") != -1 || data.LastIndexOf(".gif") != -1 || data.LastIndexOf(".png") != -1))
                        sb.AppendFormat("{0},", data);
                }
                else
                {
                    if (data != "" && data != null && data.LastIndexOf(fileType) != -1)
                        sb.AppendFormat("{0},", data);
                }
            }
            s = sb.ToString().Remove(sb.ToString().LastIndexOf(","), 1).Split(',');
        }
        catch (Exception ex)
        {
            //throw new Exception(ex.Message);
        }
        finally
        {
            if (reader != null)
                reader.Close();
        }
        return s;//回傳目前清單
    }
    #endregion

    #region 小數點以下無條件進位至某位數
    public static double RoundUp(double input, int num)
    {
        double tmpNum = Math.Floor(input);
        if ((input - tmpNum) > 0) //判斷input是否為整數     
        {
            if ((input - Math.Round(input, num)) != 0) //判斷所要取的位數是否存在         
            {
                //利用四捨五入的方法判斷是否要進位，若取的下一位數大於等於5則不用進位             
                if (Convert.ToInt32(input * Math.Pow(10, num + 1) % 10) < 5)
                {
                    return Math.Round(input, num, MidpointRounding.AwayFromZero) + Math.Pow(0.1, num);
                }
                else
                {
                    return Math.Round(input, num, MidpointRounding.AwayFromZero);
                }
            }
            else
            {
                return input;
            }
        }
        else
        {
            return input;
        }
    }
    #endregion

    #region 發送簡訊
    /// <summary>
    /// 簡訊王：http://www.kotsms.com.tw/index.php
    /// 回傳值kmsgid大於'0'為發送成功,小於'0'為發送失敗
    /// 文字跳行為 \n
    /// 70個字以內-----扣1點
    /// 134個字以內----扣2點
    /// 201個字以內----扣3點
    /// 268個字以內----扣4點
    /// 335個字以內----扣5點
    /// PHS門號0968、0966與極少部分手機不支援70字以上長簡訊發送
    /// </summary>
    public static string SendPhoneMsg(string phone, string msg, string page)
    {
        String postContent = string.Format("username={0}&password={1}&dstaddr={2}&smbody={3}&response={4}",
                                         DataWebSet.dataReader("smsID"),
                                         Lib.PuchAesChinese(DataWebSet.dataReader("smsPWD"), DataSysValue.Password),
                                         phone,
                                         HttpUtility.UrlEncode(msg, Encoding.GetEncoding("big5")),
                                         page == "" ? "" : HttpUtility.UrlEncode(string.Format("{0}/log/{1}", new WebSetValue().WebUrl, page), Encoding.GetEncoding("big5"))
                                        );
        WebClient wc = new WebClient();
        byte[] myDataBuffer = wc.DownloadData("http://202.39.48.216/kotsmsapi-1.php?" + postContent);
        return Encoding.GetEncoding("big5").GetString(myDataBuffer).Replace("kmsgid=", "").Replace("\n", "");
    }
    #endregion

    #region 中文數字大寫
    public static string ConvertNumberStr(string str)
    {
        string rstr = "";
        if (MainControls.ValidDataType("^\\d+$", str))
        {
            string tmpstr = "";
            int strlen = str.Length;
            if (strlen <= 4)//數字長度小於四位 
            {
                rstr = ConvertDigit(str);
            }
            else
            {
                if (strlen <= 8)//數字長度大於四位，小於八位 
                {
                    tmpstr = str.Substring(strlen - 4, 4);//先截取最後四位數字 
                    rstr = ConvertDigit(tmpstr);//轉換最後四位數字 
                    tmpstr = str.Substring(0, strlen - 4);//截取其餘數字 
                    //將兩次轉換的數字加上萬後相連接 
                    rstr = String.Concat(ConvertDigit(tmpstr) + "萬", rstr);
                    rstr = rstr.Replace("零萬", "萬");
                    rstr = rstr.Replace("零零", "零");

                }
                else
                    if (strlen <= 12)//數字長度大於八位，小於十二位 
                    {
                        tmpstr = str.Substring(strlen - 4, 4);//先截取最後四位數字 
                        rstr = ConvertDigit(tmpstr);//轉換最後四位數字 
                        tmpstr = str.Substring(strlen - 8, 4);//再截取四位數字 
                        rstr = String.Concat(ConvertDigit(tmpstr) + "萬", rstr);
                        tmpstr = str.Substring(0, strlen - 8);
                        rstr = String.Concat(ConvertDigit(tmpstr) + "億", rstr);
                        rstr = rstr.Replace("零億", "億");
                        rstr = rstr.Replace("零萬", "零");
                        rstr = rstr.Replace("零零", "零");
                        rstr = rstr.Replace("零零", "零");
                    }
            }
            strlen = rstr.Length;
            if (strlen >= 2)
            {
                switch (rstr.Substring(strlen - 2, 2))
                {
                    case "佰零": rstr = rstr.Substring(0, strlen - 2) + "佰"; break;
                    case "仟零": rstr = rstr.Substring(0, strlen - 2) + "仟"; break;
                    case "萬零": rstr = rstr.Substring(0, strlen - 2) + "萬"; break;
                    case "億零": rstr = rstr.Substring(0, strlen - 2) + "億"; break;

                }
            }
        }
        else
            rstr = "";
        return rstr;
    }
    public static string ConvertDigit(string str)
    {
        int strlen = str.Length;
        string rstr = "";
        switch (strlen)
        {
            case 1: rstr = ConvertChinese(str); break;
            case 2: rstr = Convert2Digit(str); break;
            case 3: rstr = Convert3Digit(str); break;
            case 4: rstr = Convert4Digit(str); break;
        }
        rstr = rstr.Replace("拾零", "拾");
        strlen = rstr.Length;

        return rstr;
    }
    /// 
    /// 轉換四位數字 
    /// 
    public static string Convert4Digit(string str)
    {
        string str1 = str.Substring(0, 1);
        string str2 = str.Substring(1, 1);
        string str3 = str.Substring(2, 1);
        string str4 = str.Substring(3, 1);
        string rstring = "";
        rstring += ConvertChinese(str1) + "仟";
        rstring += ConvertChinese(str2) + "佰";
        rstring += ConvertChinese(str3) + "拾";
        rstring += ConvertChinese(str4);
        rstring = rstring.Replace("零仟", "零");
        rstring = rstring.Replace("零佰", "零");
        rstring = rstring.Replace("零拾", "零");
        rstring = rstring.Replace("零零", "零");
        rstring = rstring.Replace("零零", "零");
        rstring = rstring.Replace("零零", "零");
        return rstring;
    }
    /// 
    /// 轉換三位數字 
    /// 
    public static string Convert3Digit(string str)
    {
        string str1 = str.Substring(0, 1);
        string str2 = str.Substring(1, 1);
        string str3 = str.Substring(2, 1);
        string rstring = "";
        rstring += ConvertChinese(str1) + "佰";
        rstring += ConvertChinese(str2) + "拾";
        rstring += ConvertChinese(str3);
        rstring = rstring.Replace("零佰", "零");
        rstring = rstring.Replace("零拾", "零");
        rstring = rstring.Replace("零零", "零");
        rstring = rstring.Replace("零零", "零");
        return rstring;
    }
    /// 
    /// 轉換二位數字 
    /// 
    public static string Convert2Digit(string str)
    {
        string str1 = str.Substring(0, 1);
        string str2 = str.Substring(1, 1);
        string rstring = "";
        rstring += ConvertChinese(str1) + "拾";
        rstring += ConvertChinese(str2);
        rstring = rstring.Replace("零拾", "零");
        rstring = rstring.Replace("零零", "零");
        return rstring;
    }
    /// 
    /// 將一位數字轉換成中文大寫數字 
    /// 
    public static string ConvertChinese(string str)
    {
        //"零壹貳三肆伍陸柒捌玖拾佰仟萬億圓整角分" 
        string cstr = "";
        switch (str)
        {
            case "0": cstr = "零"; break;
            case "1": cstr = "壹"; break;
            case "2": cstr = "貳"; break;
            case "3": cstr = "參"; break;
            case "4": cstr = "肆"; break;
            case "5": cstr = "伍"; break;
            case "6": cstr = "陸"; break;
            case "7": cstr = "柒"; break;
            case "8": cstr = "捌"; break;
            case "9": cstr = "玖"; break;
        }
        return (cstr);
    }
    #endregion

    #region 開起子視窗
    /// <summary>
    /// showModalDialog / showModelessDialog
    /// </summary>
    /// <param name="ctrl">主視窗控制項</param>
    /// <param name="key">視窗名稱</param>
    /// <param name="focus">焦點</param>
    /// <param name="scroll">捲軸</param>
    /// <param name="resizable">調整大小</param>
    /// <param name="width">寬度</param>
    /// <param name="height">高度</param>
    /// <param name="url">目標位址</param>
    public static void ShowModalDialog(Control ctrl, string key, bool focus, bool scroll, bool resizable, int? width, int? height, string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            StringBuilder script = new StringBuilder();
            //Dialog 參數
            if (width != null && height != null)
                script.AppendFormat("var windowSetting = 'dialogwidth:{2}px; dialogheight:{3}px; center:yes; scroll:{0}; resizable:{1};';", scroll ? "yes" : "no", resizable ? "yes" : "no", width, height);
            else
            {
                script.Append("var width = (window.screen.width-16).toString(); var height = (window.screen.height-40).toString();");
                script.AppendFormat("var windowSetting = 'dialogwidth:'+ width +'px; dialogheight:'+ height +'px; center:yes; scroll:{0}; resizable:{1};';", scroll ? "yes" : "no", resizable ? "yes" : "no");
            }

            script.AppendFormat("{0}('{1}', window, windowSetting);", focus ? "showModalDialog" : "showModelessDialog", url);
            //開啟對話視窗
            ScriptManager.RegisterStartupScript(ctrl, ctrl.GetType(),
                                                key,
                                                script.ToString(), true);
        }
        else
            return;
    }
    /// <summary>
    /// window.open
    /// </summary>
    /// <param name="ctrl">主視窗控制項</param>
    /// <param name="key">視窗名稱</param>
    /// <param name="scroll">捲軸</param>
    /// <param name="resizable">調整大小</param>
    /// <param name="width">寬度</param>
    /// <param name="height">高度</param>
    /// <param name="url">目標位址</param>
    public static void WindowOpen(Control ctrl, string key, bool focus, bool scroll, bool resizable, int? width, int? height, string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            StringBuilder script = new StringBuilder();
            //Dialog 參數
            if (width != null && height != null)
            {
                script.AppendFormat("var width = {0}; var height = {1}; var top = (Math.round((window.screen.height-80-height)/2)).toString(); var left = (Math.round((window.screen.width-width)/2)).toString();", width, height);
                script.AppendFormat("var windowSetting = 'width='+ width +', height='+ height +', top='+ top +', left='+ left +', scrollbars={0}, resizable={1}';", scroll ? "yes" : "no", resizable ? "yes" : "no");
            }
            else
            {
                script.Append("var width = (window.screen.width-20).toString(); var height = (window.screen.height-80).toString(); var top = (Math.round((window.screen.height-80-height)/2)).toString(); var left = (Math.round((window.screen.width-20-width)/2)).toString();");
                script.AppendFormat("var windowSetting = 'width='+ width +', height='+ height +', top='+ top +', left='+ left +', scrollbars={0}, resizable={1}';", scroll ? "yes" : "no", resizable ? "yes" : "no");
            }

            if (focus)
                script.AppendFormat("var w = window.open('{0}', '{1}', windowSetting); w.focus();", url, key);
            else
                script.AppendFormat("window.open('{0}', '{1}', windowSetting);", url, key);
            //開啟對話視窗
            ScriptManager.RegisterStartupScript(ctrl, ctrl.GetType(),
                                                key,
                                                script.ToString(), true);
        }
        else
            return;
    }

    #endregion

    #region 網頁導向
    public static string RedirectLinkStr(string text, string title, string target, string ClassStyle, string link)
    {
        if (target.ToLower() == "_blank" || target == "_new")
        {
            if (!bool.Parse(DataWebSet.dataReader("WindowFull")))
                return string.Format("<a href=\"{4}\" title=\"{1}\" target=\"{2}\" {3}>{0}</a>", text, title, target, ClassStyle, link);
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<a href=\"javascript:WindowOpen('{3}', '{4}');\" title=\"{1}\" {2}>{0}</a>", text, title, ClassStyle, link, link.Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty).Replace("?", "_").Replace("=", "_").Replace("&", "_"));
                return sb.ToString();
            }
        }
        else
            return string.Format("<a href=\"{4}\" title=\"{1}\" target=\"{2}\" {3}>{0}</a>", text, title, target, ClassStyle, link);
    }
    public static string RedirectLinkStr(string text, string title, string target, string ClassStyle, bool WindowOpen, string link, int? width, int? height)
    {
        if (target.ToLower() == "_blank" || target == "_new")
        {
            if (!WindowOpen)
                return string.Format("<a href=\"{4}\" title=\"{1}\" target=\"{2}\" {3}>{0}</a>", text, title, target, ClassStyle, link);
            else
            {
                StringBuilder sb = new StringBuilder();
                if (width != null && height != null)
                    sb.AppendFormat("<a href=\"javascript:WindowOpenSize('{3}', '{4}', '{5}', '{6}');\" title=\"{1}\" {2}>{0}</a>", text, title, ClassStyle, link, link.Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty).Replace("?", "_").Replace("=", "_").Replace("&", "_"), width, height);
                else
                    sb.AppendFormat("<a href=\"javascript:WindowOpen('{3}', '{4}');\" title=\"{1}\" {2}>{0}</a>", text, title, ClassStyle, link, link.Replace(".", string.Empty).Replace(":", string.Empty).Replace("/", string.Empty).Replace("?", "_").Replace("=", "_").Replace("&", "_"));
                return sb.ToString();
            }
        }
        else
            return string.Format("<a href=\"{4}\" title=\"{1}\" target=\"{2}\" {3}>{0}</a>", text, title, target, ClassStyle, link);
    }
    #endregion

    #region 產生 Html:select
    /// <summary>
    /// DataTable 第0欄:Text, 第1欄:Value
    /// </summary>
    /// <param name="id">id</param>
    /// <param name="Class">class名稱</param>
    /// <param name="style">CSS樣式</param>
    /// <param name="dt">資料表</param>
    /// <param name="selectIndex">預設選擇</param>
    /// <param name="text">預設文字:Value為""</param>
    /// <returns></returns>
    public static string CreateHtml_select(string id, string Class, string style, DataTable dt, string keyName, string valueName, int? selectIndex, string text)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            if (selectIndex == null)
                selectIndex = 0;

            sb.AppendFormat("<select id=\"{0}\" {1} {2}>", id, !string.IsNullOrEmpty(Class) ? string.Format("class=\"{0}\"", Class) : string.Empty, !string.IsNullOrEmpty(style) ? string.Format("style=\"{0}\"", style) : string.Empty);
            if (text != null)
                sb.AppendFormat("<option value=\"{1}\" {2}>{0}</option>", text, "", selectIndex == 0 ? "selected=\"selected\"" : string.Empty);
            foreach (DataRow dr in dt.Rows)
                sb.AppendFormat("<option value=\"{1}\" {2}>{0}</option>", dr[keyName].ToString(), dr[valueName].ToString(), (text == null ? dt.Rows.IndexOf(dr) : (dt.Rows.IndexOf(dr) + 1)) == selectIndex ? "selected=\"selected\"" : string.Empty);
            sb.Append("</select>");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return sb.ToString();
    }

    #endregion

    #region 產生 Html:checkbox
    /// <summary>
    /// DataTable 第0欄:Text, 第1欄:Value
    /// </summary>
    /// <param name="id">id</param>
    /// <param name="Class">class名稱</param>
    /// <param name="style">CSS樣式</param>
    /// <returns></returns>
    public static string CreateHtml_checkbox(DataTable dt, string keyName, string valueName, string Class, string style)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<div style=\"float:left; margin: 1px 20px 1px 0px;\">");
                sb.AppendFormat("<label {3}><input type=\"checkbox\" value=\"{1}\" {2}> {0}</label>", dr[keyName].ToString(), dr[valueName].ToString(), !string.IsNullOrEmpty(Class) ? string.Format("class=\"{0}\"", Class) : string.Empty, !string.IsNullOrEmpty(style) ? string.Format("style=\"{0}\"", style) : string.Empty);
                sb.Append("</div>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return sb.ToString();
    }
    #endregion

    #region 產生 Html:radio
    /// <summary>
    /// DataTable 第0欄:Text, 第1欄:Value
    /// </summary>
    /// <param name="id">id</param>
    /// <param name="Class">class名稱</param>
    /// <param name="style">CSS樣式</param>
    /// <returns></returns>
    public static string CreateHtml_radio(DataTable dt, string keyName, string valueName, string groupName, string Class, string style)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<div style=\"float:left; margin: 1px 20px 1px 0px;\">");
                sb.AppendFormat("<label {3}><input name=\"{3}\" type=\"radio\" value=\"{1}\" {2}> {0}</label>", dr[keyName].ToString(), dr[valueName].ToString(), !string.IsNullOrEmpty(Class) ? string.Format("class=\"{0}\"", Class) : string.Empty, !string.IsNullOrEmpty(style) ? string.Format("style=\"{0}\"", style) : string.Empty, groupName);
                sb.Append("</div>");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        return sb.ToString();
    }
    #endregion

    #region 取得列舉值的 Description
    /// <summary>取得列舉值的 Description</summary>
    /// <param name="enunValue"></param>
    /// <returns></returns>
    public static string GetEnumDescription(Enum enunValue)
    {
        FieldInfo fi = enunValue.GetType().GetField(enunValue.ToString());

        if (fi == null)
            return "";

        DescriptionAttribute[] attributes =
            (DescriptionAttribute[])fi.GetCustomAttributes(
            typeof(DescriptionAttribute),
            false);

        if (attributes != null && attributes.Length > 0)
            return attributes[0].Description;
        else
            return "";
    }
    #endregion

    #region  抓取IP
    public static string GetUserIPAddress()
    {
        string ip = "";
        try
        {
            ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (!string.IsNullOrEmpty(ip))
            {
                string[] ipRange = ip.Split(',');
                ip = ipRange[0];
            }
            else
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
        }
        catch (Exception ex) { }
        return ip.Trim();
    }
    #endregion

    #region 壓縮目錄中所有圖檔
    /// <summary>
    /// 壓縮目錄中所有圖檔
    /// </summary>
    /// <param name="Path">原始路徑</param>
    /// <param name="savePath">儲存路徑</param>
    /// <param name="compress">壓縮品質：100L~1L</param>
    public static void CompressImage(string Path, string savePath, long compress)
    {
        DirectoryInfo DirPath = new DirectoryInfo(Path);
        FileInfo[] filelist_png = DirPath.GetFiles("*.png");
        FileInfo[] filelist_gif = DirPath.GetFiles("*.gif");
        FileInfo[] filelist_jpg = DirPath.GetFiles("*.jpg");
        DirectoryInfo[] directorylist = DirPath.GetDirectories();

        if (!Directory.Exists(savePath))
            Directory.CreateDirectory(savePath);
        foreach (FileInfo File in filelist_png)
            Lib.MakeImageWH(File.FullName, string.Format("{0}\\{1}", savePath, File.Name), null, null, "", compress);
        foreach (FileInfo File in filelist_gif)
            Lib.MakeImageWH(File.FullName, string.Format("{0}\\{1}", savePath, File.Name), null, null, "", compress);
        foreach (FileInfo File in filelist_jpg)
            Lib.MakeImageWH(File.FullName, string.Format("{0}\\{1}", savePath, File.Name), null, null, "", compress);
        foreach (DirectoryInfo directory in directorylist)
            CompressImage(directory.FullName, string.Format("{0}\\{1}", savePath, directory.Name), compress);
    }
    #endregion

    #region 圖片轉BASE64字串
    public static string ImageToBase64(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            // Convert Image to byte[]
            image.Save(ms, format);
            byte[] imageBytes = ms.ToArray();

            // Convert byte[] to Base64 String
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
    #endregion

    #region BASE64字串轉圖片
    public static System.Drawing.Image Base64ToImage(string base64String)
    {
        System.Drawing.Image image = null;
        try
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            image = System.Drawing.Image.FromStream(ms, true);
        }
        catch (Exception ex)
        {

        }

        return image;
    }
    public static Stream Base64ToStream(string base64String)
    {
        Stream stream = null;
        try
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            stream = new MemoryStream(imageBytes, 0, imageBytes.Length);
        }
        catch (Exception ex)
        {

        }

        return stream;
    }
    #endregion

    #region 取得GoogleMap資訊
    /// <summary>
    /// 把地址轉成JSON格式，這樣資訊裡才有經緯度
    /// </summary>
    /// <param name="address"></param>
    /// <returns></returns>
    public static string convertAddressToJSONString(string address)
    {
        var url = string.Format("http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language={1}&address={0}", HttpContext.Current.Server.UrlEncode(address), "zh-TW");


        string result = String.Empty;
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        using (var response = request.GetResponse())
        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        {

            result = sr.ReadToEnd();
        }
        return result;
    }
    public static string convertAddressToJSONString(string address, string language)
    {
        var url = string.Format("http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language={1}&address={0}", HttpContext.Current.Server.UrlEncode(address), language);


        string result = String.Empty;
        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
        using (var response = request.GetResponse())
        using (StreamReader sr = new StreamReader(response.GetResponseStream()))
        {

            result = sr.ReadToEnd();
        }
        return result;
    }
    #endregion

    #region 動態產生CheckBox及TextBox
    public static void CreateCheckBoxWithTextBox(Control container, string ControlID, string CheckBoxID, string TextBoxID, int listCount, int TextBoxWidth = 200, Dictionary<int, string> text = null, Dictionary<int, bool> isChecked = null)
    {
        var list = new List<ListItem>();

        for (int i = 0; i < listCount; i++)
        {
            list.Add(new ListItem("", i.ToString()));
        }

        Table table = new Table();

        foreach (var item in list)
        {
            //Create new checkbox
            CheckBox CB = new CheckBox();
            CB.Text = item.Text;
            CB.ID = string.Format("{0}_{1}_{2}", ControlID, CheckBoxID, item.Value);
            if (isChecked != null && isChecked.ContainsKey(list.IndexOf(item)))
                CB.Checked = isChecked[list.IndexOf(item)];

            //Create tablr row and td, then adds them accordignly
            TableRow TR = new TableRow();
            TableCell TD = new TableCell();
            TD.Controls.Add(CB);
            TR.Controls.Add(TD);

            //Create your input element and place it in a new Table cell (TD2)
            TextBox TB = new TextBox();
            TB.ID = string.Format("{0}_{1}_{2}", ControlID, TextBoxID, item.Value);
            TB.Width = TextBoxWidth;
            if (text != null && text.ContainsKey(list.IndexOf(item)))
                TB.Text = text[list.IndexOf(item)];
            TableCell TD2 = new TableCell();
            TD2.Controls.Add(TB);
            TR.Controls.Add(TD2);

            table.Controls.Add(TR);
        }

        container.Controls.Add(table);
    }
    #endregion

    #region 動態產生CheckBox
    public static void CreateCheckBox(Control container, string ControlID, string CheckBoxID, int listCount, SortedDictionary<int, string> id = null, SortedDictionary<int, string> text = null, SortedDictionary<int, bool> isChecked = null)
    {
        var list = new List<ListItem>();

        for (int i = 0; i < listCount; i++)
        {
            list.Add(new ListItem("", i.ToString()));
        }

        Table table = new Table();

        foreach (var item in list)
        {
            //Create new checkbox
            CheckBox CB = new CheckBox();
            if (text != null && text.ContainsKey(list.IndexOf(item)))
                CB.Text = text[list.IndexOf(item)];
            if (id != null && id.ContainsKey(list.IndexOf(item)))
                CB.ID = id[list.IndexOf(item)];
            else
                CB.ID = string.Format("{0}_{1}_{2}", ControlID, CheckBoxID, item.Value);
            if (isChecked != null && isChecked.ContainsKey(list.IndexOf(item)))
                CB.Checked = isChecked[list.IndexOf(item)];

            //Create tablr row and td, then adds them accordignly
            TableRow TR = new TableRow();
            TableCell TD = new TableCell();
            TD.Controls.Add(CB);
            TR.Controls.Add(TD);

            table.Controls.Add(TR);
        }

        container.Controls.Add(table);
    }
    #endregion

    #region 動態產生RadioButton及TextBox
    public static void CreateRadioButtonWithTextBox(Control container, string ControlID, string RadioButton, string TextBoxID, int listCount, int TextBoxWidth = 200, Dictionary<int, string> text = null, Dictionary<int, bool> isChecked = null)
    {
        var list = new List<ListItem>();

        for (int i = 0; i < listCount; i++)
        {
            list.Add(new ListItem("", i.ToString()));
        }

        Table table = new Table();

        foreach (var item in list)
        {
            //Create new checkbox
            RadioButton RB = new RadioButton();
            RB.Text = item.Text;
            RB.ID = string.Format("{0}_{1}_{2}", ControlID, RadioButton, item.Value);
            if (isChecked != null && isChecked.ContainsKey(list.IndexOf(item)))
                RB.Checked = isChecked[list.IndexOf(item)];
            RB.GroupName = ControlID;

            //Create tablr row and td, then adds them accordignly
            TableRow TR = new TableRow();
            TableCell TD = new TableCell();
            TD.Controls.Add(RB);
            TR.Controls.Add(TD);

            //Create your input element and place it in a new Table cell (TD2)
            TextBox TB = new TextBox();
            TB.ID = string.Format("{0}_{1}_{2}", ControlID, TextBoxID, item.Value);
            TB.Width = TextBoxWidth;
            if (text != null && text.ContainsKey(list.IndexOf(item)))
                TB.Text = text[list.IndexOf(item)];
            TableCell TD2 = new TableCell();
            TD2.Controls.Add(TB);
            TR.Controls.Add(TD2);

            table.Controls.Add(TR);
        }

        container.Controls.Add(table);
    }
    #endregion

    #region 動態產生RadioButton
    public static void CreateRadioButton(Control container, string ControlID, string RadioButton, int listCount, SortedDictionary<int, string> id = null, SortedDictionary<int, string> text = null, SortedDictionary<int, bool> isChecked = null)
    {
        var list = new List<ListItem>();

        for (int i = 0; i < listCount; i++)
        {
            list.Add(new ListItem("", i.ToString()));
        }

        Table table = new Table();

        foreach (var item in list)
        {
            //Create new checkbox
            RadioButton RB = new RadioButton();
            if (text != null && text.ContainsKey(list.IndexOf(item)))
                RB.Text = text[list.IndexOf(item)];
            if (id != null && id.ContainsKey(list.IndexOf(item)))
                RB.ID = id[list.IndexOf(item)];
            else
                RB.ID = string.Format("{0}_{1}_{2}", ControlID, RadioButton, item.Value);
            if (isChecked != null && isChecked.ContainsKey(list.IndexOf(item)))
                RB.Checked = isChecked[list.IndexOf(item)];
            RB.GroupName = ControlID;

            //Create tablr row and td, then adds them accordignly
            TableRow TR = new TableRow();
            TableCell TD = new TableCell();
            TD.Controls.Add(RB);
            TR.Controls.Add(TD);

            table.Controls.Add(TR);
        }

        container.Controls.Add(table);
    }
    #endregion
}
