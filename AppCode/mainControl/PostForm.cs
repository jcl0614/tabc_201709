﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// PostForm 的摘要描述
/// </summary>
public class PostForm
{
    public static void RedirectAndPOST(Page page, string destinationUrl, Dictionary<string, string> data)
    {
        string strForm = PreparePOSTForm(destinationUrl, data);
        page.Controls.Add(new LiteralControl(strForm));
    }

    public static String PreparePOSTForm(string url, Dictionary<string, string> data)
    {
        //Set a name for the form 
        string formID = "PostForm";
        //Build the form using the specified data to be posted. 
        System.Text.StringBuilder strForm = new System.Text.StringBuilder();
        strForm.Append("<form id=\"" + formID + "\" name=\"" +
                       formID + "\" action=\"" + url +
                       "\" method=\"POST\">");

        foreach (KeyValuePair<string, string> key in data)
        {
            strForm.Append("<input type=\"hidden\" name=\"" + key.Key +
                           "\" value=\"" + key.Value + "\">");
        }

        strForm.Append("</form>");
        //Build the JavaScript which will do the Posting operation. 
        System.Text.StringBuilder strScript = new System.Text.StringBuilder();
        strScript.Append("<script type=\"text/javascript\">");
        strScript.Append("var v" + formID + " = document." +
                         formID + ";");
        strScript.Append("v" + formID + ".submit();");
        strScript.Append("</script>");
        //Return the form and the script concatenated. 
        //(The order is important, Form then JavaScript) 
        return strForm.ToString() + strScript.ToString();
    }
}