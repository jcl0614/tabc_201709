﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

/// <summary>
    /// 128条码打印类
    /// </summary>
    public class Code128
    {
        //各个编码的开始标记
        private static string START_A = "11010000100";
        private static string START_B = "11010010000";
        private static string START_C = "11010011100";
 
        //编码结束标记
        private static string STOP = "1100011101011";
 
        //编码切换标记
        private static string SWITCH_B_TO_C = "10111011110";
        private static string SWITCH_C_TO_B = "10111101110";
 
        //当前的编码
        private string mCurrCode = string.Empty;
 

        //128 码对应的编码规则
        private static string[] CodePaterm = { "11011001100", "11001101100", "11001100110", "10010011000", "10010001100",  // 0-4
             "10001001100", "10011001000", "10011000100", "10001100100", "11001001000","11001000100" , // 5-10
             "11000100100", "10110011100", "10011011100", "10011001110", "10111001100",    // 11-15
             "10011101100", "10011100110", "11001110010", "11001011100", "11001001110",    // 16-20
             "11011100100", "11001110100", "11101101110", "11101001100", "11100101100",    // 21-25
             "11100100110", "11101100100", "11100110100", "11100110010", "11011011000",    // 26-30
             "11011000110", "11000110110", "10100011000", "10001011000", "10001000110",    // 31-35
             "10110001000", "10001101000", "10001100010", "11010001000", "11000101000",    // 36-40
             "11000100010", "10110111000", "10110001110", "10001101110", "10111011000",    // 41-45
             "10111000110", "10001110110", "11101110110", "11010001110", "11000101110",    // 46-50
             "11011101000", "11011100010", "11011101110", "11101011000", "11101000110",    // 51-55
             "11100010110", "11101101000", "11101100010", "11100011010", "11101111010",    // 56-60
             "11001000010", "11110001010", "10100110000", "10100001100", "10010110000",    // 61-65
             "10010000110", "10000101100", "10000100110", "10110010000", "10110000100",    // 66-70
             "10011010000", "10011000010", "10000110100", "10000110010", "11000010010",    // 71-75
             "11001010000", "11110111010", "11000010100", "10001111010", "10100111100",    // 76-80
             "10010111100", "10010011110", "10111100100", "10011110100", "10011110010",    // 81-85
             "11110100100", "11110010100", "11110010010", "11011011110", "11011110110",    // 86-90
             "11110110110", "10101111000", "10100011110", "10001011110", "10111101000",    // 91-95
             "10111100010", "11110101000", "11110100010", "10111011110", "10111101110",    // 96-100
             "11101011110", "11110101110", "11010000100", "11010010000", "11010011100",    // 101-105
             "1100011101011"                                                               // 106
        };
 
        public Code128()
        {
        }
 
        //条码宽、高
        private float _fWidth = 37.29f;
        private float _fHeight = 12.93f;
 
        //当前编码子集 CODE A OR B OR C
        private string CURR_CODE_SET = string.Empty;
 
        public float Width
        {
            get
            {
                return _fWidth;
            }
            set
            {
                _fWidth = value;
            }
        }
 
        public float Height
        {
            get
            {
                return _fHeight;
            }
            set
            {
                _fHeight = value;
            }
        }
 

        /// <summary>
        /// 方法名称：GetCodeValue
        /// 内容摘要：返回CODE 128 B中的字符对应的数值
        /// </summary>
        private int GetCodeValue(string m_Code)
        {
            int numValue = 0;
            switch (m_Code)
            {
                case "!":
                    numValue = 01;
                    break;
                case "#":
                    numValue = 03;
                    break;
                case "$":
                    numValue = 04;
                    break;
                case "%":
                    numValue = 05;
                    break;
                case "&":
                    numValue = 06;
                    break;
                case "'":
                    numValue = 07;
                    break;
                case "(":
                    numValue = 08;
                    break;
                case ")":
                    numValue = 09;
                    break;
                case "*":
                    numValue = 10;
                    break;
                case "+":
                    numValue = 11;
                    break;
                case ",":
                    numValue = 12;
                    break;
                case "-":
                    numValue = 13;
                    break;
                case ".":
                    numValue = 14;
                    break;
                case "/":
                    numValue = 15;
                    break;
                case "0":
                    numValue = 16;
                    break;
                case "1":
                    numValue = 17;
                    break;
                case "2":
                    numValue = 18;
                    break;
                case "3":
                    numValue = 19;
                    break;
                case "4":
                    numValue = 20;
                    break;
                case "5":
                    numValue = 21;
                    break;
                case "6":
                    numValue = 22;
                    break;
                case "7":
                    numValue = 23;
                    break;
                case "8":
                    numValue = 24;
                    break;
                case "9":
                    numValue = 25;
                    break;
                case "A":
                    numValue = 33;
                    break;
                case "B":
                    numValue = 34;
                    break;
                case "C":
                    numValue = 35;
                    break;
                case "D":
                    numValue = 36;
                    break;
                case "E":
                    numValue = 37;
                    break;
                case "F":
                    numValue = 38;
                    break;
                case "G":
                    numValue = 39;
                    break;
                case "H":
                    numValue = 40;
                    break;
                case "I":
                    numValue = 41;
                    break;
                case "J":
                    numValue = 42;
                    break;
                case "K":
                    numValue = 43;
                    break;
                case "L":
                    numValue = 44;
                    break;
                case "M":
                    numValue = 45;
                    break;
                case "N":
                    numValue = 46;
                    break;
                case "O":
                    numValue = 47;
                    break;
                case "P":
                    numValue = 48;
                    break;
                case "Q":
                    numValue = 49;
                    break;
                case "R":
                    numValue = 50;
                    break;
                case "S":
                    numValue = 51;
                    break;
                case "T":
                    numValue = 52;
                    break;
                case "U":
                    numValue = 53;
                    break;
                case "V":
                    numValue = 54;
                    break;
                case "W":
                    numValue = 55;
                    break;
                case "X":
                    numValue = 56;
                    break;
                case "Y":
                    numValue = 57;
                    break;
                case "Z":
                    numValue = 58;
                    break;
                case "[":
                    numValue = 59;
                    break;
                case "//":
                    numValue = 60;
                    break;
                case "]":
                    numValue = 61;
                    break;
                case "^":
                    numValue = 62;
                    break;
                case "_":
                    numValue = 63;
                    break;
                case "`":
                    numValue = 64;
                    break;
                case "a":
                    numValue = 65;
                    break;
                case "b":
                    numValue = 66;
                    break;
                case "c":
                    numValue = 67;
                    break;
                case "d":
                    numValue = 68;
                    break;
                case "e":
                    numValue = 69;
                    break;
                case "f":
                    numValue = 70;
                    break;
                case "g":
                    numValue = 71;
                    break;
                case "h":
                    numValue = 72;
                    break;
                case "i":
                    numValue = 73;
                    break;
                case "j":
                    numValue = 74;
                    break;
                case "k":
                    numValue = 75;
                    break;
                case "l":
                    numValue = 76;
                    break;
                case "m":
                    numValue = 77;
                    break;
                case "n":
                    numValue = 78;
                    break;
                case "o":
                    numValue = 79;
                    break;
                case "p":
                    numValue = 80;
                    break;
                case "q":
                    numValue = 81;
                    break;
                case "r":
                    numValue = 82;
                    break;
                case "s":
                    numValue = 83;
                    break;
                case "t":
                    numValue = 84;
                    break;
                case "u":
                    numValue = 85;
                    break;
                case "v":
                    numValue = 86;
                    break;
                case "w":
                    numValue = 87;
                    break;
                case "x":
                    numValue = 88;
                    break;
                case "y":
                    numValue = 89;
                    break;
                case "z":
                    numValue = 90;
                    break;
                case "{":
                    numValue = 91;
                    break;
                case "|":
                    numValue = 92;
                    break;
                case "}":
                    numValue = 93;
                    break;
                case "~":
                    numValue = 94;
                    break;
 
            }
            return numValue;
        }
 
        /// <summary>
        /// 打印128条码。
        /// </summary>
        /// <param name="g">打印对象</param>
        /// <param name="pt">坐标</param>
        public System.Drawing.Bitmap Draw128Barcode(string strCode, System.Drawing.Point pt, int height)
        {
            //生成编码字符
            string strCodeString = GenerateCodingString(strCode);

            //新建一個bmp圖片
            Bitmap bitmapp = new System.Drawing.Bitmap(strCodeString.Length * 15 / 10, height);
            //bitmapp.SetResolution(1000, 1000);
            System.Drawing.Image bitmap = bitmapp;

            //新建一個畫板
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmapp);

            //填上底色
            g.FillRectangle(Brushes.White, 0, 0, strCodeString.Length * 15 / 10, height);

            //开始打印位置
            float xStart = pt.X;
            float yStart = pt.Y;
 
            //计算条形宽度
            //float lineWidth = _fWidth / strCodeString.Length ;
            float lineWidth = 1.5f;
 
            System.Drawing.SolidBrush brush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
 
            float xPosition = 0;
 
            //画每个编码的条形.
            for (int i = 0; i < strCodeString.Length; i++)
            {
                if (strCodeString.Substring(i, 1) == "1")
                    g.FillRectangle(Brushes.Black, xPosition + xStart, yStart, lineWidth, _fHeight);
                else
                    g.FillRectangle(Brushes.White, xPosition + xStart, yStart, lineWidth, _fHeight);
 
                xPosition += lineWidth;
 
            }
            return bitmapp;
        }
 
        /// <summary>
        /// 生成要打印的128编码字符
        /// </summary>
        /// <param name="strCode">要打印的字符</param>
        /// <returns>128编码字符</returns>
        private string GenerateCodingString(string strCode)
        {
            //要处理的字符
            string strToCode = strCode;
 
            //当前处理的字符
            string strCurr = string.Empty;
 
            //下一个非数字字符前一位
            int iNextNumPos = 0;
 
            //序号
            int iNo = 1;
            int iCodeNo = 0;
 
            //检验值
            long lngCheckDigitSum = 0;
            long lngCheckDigit = 0;
 
            //生成的编码字符
            System.Text.StringBuilder sbCode128 = new System.Text.StringBuilder();
 
            //设置开始的编码，数字大于3位时采用C编码，否则采用B编码
            if (GetNumberStringLen(strToCode) > 3)
            {
                //采用编码C
                mCurrCode = START_C;
                lngCheckDigitSum = 105;
            }
            else
            {
                mCurrCode = START_B;
                lngCheckDigitSum = 104;
            }
 
            //开始的编码置于第一位
            sbCode128.Append(mCurrCode);
 
            //处理所有的字符，生成要打印的编码
            while (strToCode != string.Empty)
            {
                //从当前开始数字字符长度
                iNextNumPos = GetNumberStringLen(strToCode);
 
                //编码切换处理
                if (iNextNumPos < 2)                   //1个数字或无数字，如不是B编码，则必须转为B编码
                {
                    //不是B编码，则切换到B编码
                    if ((mCurrCode != START_B) && (mCurrCode != SWITCH_C_TO_B))
                    {
                        sbCode128.Append(SWITCH_C_TO_B);
                        mCurrCode = SWITCH_C_TO_B;
                        iCodeNo = 100;
                        lngCheckDigitSum = lngCheckDigitSum + iNo * iCodeNo;
                        iNo++;
                    }
                }
                else if (iNextNumPos > 3)           //4个或以上数字，则必须转为C编码
                {
                    //不是C编码，则切换到C编码
                    if ((mCurrCode != START_C) && (mCurrCode != SWITCH_B_TO_C))
                    {
                        sbCode128.Append(SWITCH_B_TO_C);
                        mCurrCode = SWITCH_B_TO_C;
                        iCodeNo = 99;
                        lngCheckDigitSum = lngCheckDigitSum + iNo * iCodeNo;
                        iNo++;
                    }
                }
 
                //处理当前字符对于编码B，直接取第一个字符，获得对应编码字符
                if ((mCurrCode == START_B) || (mCurrCode == SWITCH_C_TO_B))
                {
                    //截取第一个字符
                    strCurr = strToCode.Substring(0, 1);
                    strToCode = strToCode.Substring(1);
 
                    //添加到字符编码中
                    iCodeNo = GetCodeValue(strCurr);
                    sbCode128.Append(CodePaterm[iCodeNo]);
 
                }
                else
                {
                    //C编码，则取2位字符编码
                    strCurr = strToCode.Substring(0, 2);
                    strToCode = strToCode.Substring(2);
 
                    //添加到字符编码中
                    iCodeNo = Convert.ToInt32(strCurr);
                    sbCode128.Append(CodePaterm[iCodeNo]);
                }
 
                //计算校验位
                lngCheckDigitSum = lngCheckDigitSum + iNo * iCodeNo;
 
                iNo++;
            }
 
            //添加校验位
            lngCheckDigit = lngCheckDigitSum % 103;
            sbCode128.Append(CodePaterm[lngCheckDigit]);
 
            //添加结束位
            sbCode128.Append(STOP);
 
            return sbCode128.ToString();
        }
 
        /// <summary>
        /// 返回数字字符长度
        /// </summary>
        /// <returns></returns>
        private int GetNumberStringLen(string strCode)
        {
            int i = 0;
 
            //获得数字部分长度
            for (i = 0; i < strCode.Length; i++)
            {
                if (!char.IsNumber(strCode, i))
                    break;
            }
 
            return i;
 

        }
    }
