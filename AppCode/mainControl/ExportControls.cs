﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Data.OleDb;
using System.Text;
using System.Web.UI.WebControls;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


/// <summary>
/// ExportControl 的摘要描述
/// </summary>
public class ExportControls
{
    
    #region DataTable to RealExcel by NPOI
    /// <summary>
    /// DataTable to RealExcel by NPOI
    /// </summary>
    /// <param name="dt">資料表</param>
    /// <param name="fileName">檔案名稱</param>
    public static void DataTableToExcelbyNPOI(DataTable dt, string fileName)
    {
        DataTableToExcelbyNPOI(dt, fileName, true);
    }
    public static void DataTableToExcelbyNPOI(DataTable dt, string fileName, bool web)
    {
        DataTableToExcelbyNPOI("", dt, fileName, web);
    }

    /// <summary>
    /// DataTable to RealExcel by NPOI
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="dt">資料表</param>
    /// <param name="fileName">檔案名稱</param>
    public static void DataTableToExcelbyNPOI(string title, DataTable dt, string fileName)
    {
        DataTableToExcelbyNPOI(title, dt, fileName, true);
    }

    public static void DataTableToExcelbyNPOI(string title, DataTable dt, string fileName, bool web)
    {
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(title, dt) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(fileName.IndexOf(".xls") == -1 ? (fileName.IndexOf(".csv") == -1 ? string.Format("{0}.csv", fileName) : fileName) : fileName)));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataTableToExcel(title, dt, fileName);
    }
    public static void DataTableToExcelbyNPOI(string title, string ISheet, DataTable dt, string fileName, bool web)
    {
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(title, ISheet, dt) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(fileName.IndexOf(".xls") == -1 ? (fileName.IndexOf(".csv") == -1 ? string.Format("{0}.csv", fileName) : fileName) : fileName)));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataTableToExcel(title, dt, fileName);
    }
    public static void DataTableToExcelbyNPOI(string title, DataTable dt, string fileName, bool web, bool headvisible)
    {
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(headvisible, dt) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(fileName.IndexOf(".xls") == -1 ? (fileName.IndexOf(".csv") == -1 ? string.Format("{0}.csv", fileName) : fileName) : fileName)));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataTableToExcel(title, dt, fileName);
    }

    /// <summary>
    /// DataTable to RealExcel by NPOI
    /// </summary>
    /// <param name="dt">資料表</param>
    /// <param name="fileName">檔案名稱</param>
    /// <returns>轉Excel</returns>
    public static void DataTableToExcelbyNPOI(DataTable dt, Dictionary<string, string> dFieldCaption, string fileName)
    {
        DataTableToExcelbyNPOI("", dt, dFieldCaption, fileName);
    }

    public static void DataTableToExcelbyNPOI(string title, DataTable dt, Dictionary<string, string> dFieldCaption, string fileName)
    {
        DataTableToExcelbyNPOI(title, dt, dFieldCaption, fileName, true);
    }

    public static void DataTableToExcelbyNPOI(string title, DataTable dt, Dictionary<string, string> dFieldCaption, string fileName, bool web)
    {
        foreach (DataColumn col in dt.Columns)
            if (dFieldCaption.Keys.Contains(col.ColumnName))
                col.Caption = dFieldCaption[col.ColumnName];
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(title, dt) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}.xls", HttpContext.Current.Server.UrlPathEncode(fileName)));
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataTableToExcel(title, dt, fileName);
    }
    #region Excel Column
    /// <summary>
    /// Excel欄位,索引位置轉成文字(例如： 1 -> A, 2-B.....
    /// 最大至IV
    /// </summary>
    /// <param name="col">欄位索引</param>
    /// <returns></returns>
    public static string getColumnName(int col)
    {
        StringBuilder sb = new StringBuilder();
        string str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        List<int> list = new List<int>();

        //被除數, 除數, 餘數
        int a = col, b = 26, c = 0;
        //26進位
        while (true)
        {
            //商
            a = Math.DivRem(a, b, out c);
            list.Add(c);
            if (a == 0)
                break;
        }

        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list.Count == 1)
            {
                sb.Append(str[list[i]]);
            }
            else
            {
                if (i == list.Count - 1)
                    sb.Append(str[list[i] - 1]);
                else
                    sb.Append(str[list[i]]);
            }

        }

        return sb.ToString();
    }
    #endregion

    #endregion

    #region DataView to RealExcel by NPOI
    /// <summary>
    /// DataView to RealExcel by NPOI
    /// </summary>
    /// <param name="dv">資料</param>
    /// <param name="fileName">檔案名稱</param>
    public static void DataViewToExcelbyNPOI(DataView dv, string fileName)
    {
        DataViewToExcelbyNPOI("", dv, fileName);
    }

    public static void DataViewToExcelbyNPOI(bool headvisible, DataView dv, string fileName)
    {
        DataViewToExcelbyNPOI("", headvisible, dv, fileName, true);
    }
    /// <summary>
    /// DataView to RealExcel by NPOI
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="dv">資料</param>
    /// <param name="fileName">檔案名稱</param>
    public static void DataViewToExcelbyNPOI(string title, DataView dv, string fileName)
    {
        DataViewToExcelbyNPOI(title, true, dv, fileName, true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="title"></param>
    /// <param name="headvisible"></param>
    /// <param name="dv"></param>
    /// <param name="fileName"></param>
    /// <param name="web"></param>
    public static void DataViewToExcelbyNPOI(string title, bool headvisible, DataView dv, string fileName, bool web)
    {
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataViewToExcel(title, headvisible, dv) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + HttpContext.Current.Server.UrlPathEncode(fileName) + ".xls"));
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataViewToExcel(title, dv, fileName);

    }

    /// <summary>
    /// DataView to RealExcel by NPOI
    /// </summary>
    /// <param name="dv">資料</param>
    /// <param name="fileName">檔案名稱</param>
    /// <returns>轉Excel</returns>
    public static void DataViewToExcelbyNPOI(DataView dv, Dictionary<string, string> dFieldCaption, string fileName)
    {
        DataViewToExcelbyNPOI("", dv, dFieldCaption, fileName);
    }

    /// <summary>
    /// DataView to RealExcel by NPOI
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="dv">資料</param>
    /// <param name="dFieldCaption">欄位說明</param>
    /// <param name="fileName">檔案名稱</param>
    public static void DataViewToExcelbyNPOI(string title, DataView dv, Dictionary<string, string> dFieldCaption, string fileName)
    {
        DataViewToExcelbyNPOI(title, dv, dFieldCaption, fileName, true);
    }

    public static void DataViewToExcelbyNPOI(string title, DataView dv, Dictionary<string, string> dFieldCaption, string fileName, bool web)
    {
        foreach (DataColumn col in dv.Table.Columns)
            if (dFieldCaption.Keys.Contains(col.ColumnName))
                col.Caption = dFieldCaption[col.ColumnName];
        if (web)
        {
            MemoryStream ms = DataTableRenderToExcel.RenderDataViewToExcel(title, dv) as MemoryStream;
            // 設定強制下載標頭。
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=" + HttpContext.Current.Server.UrlPathEncode(fileName) + ".xls"));
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
        else
            DataTableRenderToExcel.RenderDataViewToExcel(title, dv, fileName);
    }

    #endregion

    #region GridView To Excel
    public static void GridViewToExcel(Page page, GridView mgv, string fileName)
    {
        //匯出Excel
        string style = "<style> .text { mso-number-format:@; } </style> ";
        System.IO.StringWriter tw = new System.IO.StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);
        page.Response.ContentType = "application/vnd.ms-excel";
        page.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlPathEncode(fileName));
        page.Response.Write("<meta http-equiv=Content-Type content=text/html;charset=utf-8>");

        //關閉換頁跟排序
        mgv.AllowSorting = false;
        mgv.AllowPaging = false;
        //移去不要的欄位
        //mgv.Columns.RemoveAt(0);
        //mgv.DataBind();
        //建立假HtmlForm避免以下錯誤
        //Control 'GridView1' of type 'GridView' must be placed inside
        //a form tag with runat=server.
        //另一種做法是override VerifyRenderingInServerForm後不做任何事
        //這樣就可以直接GridView1.RenderControl(hw);
        HtmlForm hf = new HtmlForm();
        page.Controls.Add(hf);
        hf.Controls.Add(mgv);
        hf.RenderControl(hw);
        page.Response.Write(style);
        int start = tw.ToString().IndexOf("<div>");
        int end = tw.ToString().IndexOf("</div>")+6;
        page.Response.Write(tw.ToString().Replace(tw.ToString().Substring(start, end-start), ""));
        page.Response.End();
    }


    #endregion

    #region csv 轉 DataTable
    /// <summary>
    /// csv 轉 DataTable
    /// </summary>
    /// <param name="file">檔案路徑</param>
    /// <param name="isRowOneHeader"></param>
    /// <returns></returns>
    //public static RServiceProvider csvToDataTable(string file, bool isRowOneHeader)
    //{
    //    RServiceProvider rsp = new RServiceProvider();
    //    DataTable csvDataTable = new DataTable();
    //    try
    //    {
    //        //no try/catch - add these in yourselfs or let exception happen
    //        //String[] csvData = File.ReadAllLines(HttpContext.Current.Server.MapPath(file));
    //        String[] csvData = File.ReadAllLines(file, Encoding.GetEncoding(950));

    //        //if no data in file ‘manually’ throw an exception
    //        if (csvData.Length == 0)
    //        {
    //            throw new Exception("CSV檔是空檔");
    //        }

    //        String[] headings = csvData[0].Split(',');
    //        int index = 0; //will be zero or one depending on isRowOneHeader

    //        if (isRowOneHeader) //if first record lists headers
    //        {
    //            index = 1; //so we won’t take headings as data

    //            //for each heading
    //            for (int i = 0; i < headings.Length; i++)
    //            {
    //                //replace spaces with underscores for column names
    //                headings[i] = headings[i].Replace(" ", "").Replace("\"", "").Replace("'", "");
    //                //add a column for each heading
    //                csvDataTable.Columns.Add(headings[i], typeof(string));
    //            }
    //        }
    //        else //if no headers just go for col1, col2 etc.
    //        {
    //            for (int i = 0; i < headings.Length; i++)
    //            {
    //                //create arbitary column names
    //                csvDataTable.Columns.Add("col" + (i + 1).ToString(), typeof(string));
    //            }
    //        }

    //        //populate the DataTable
    //        for (int i = index; i < csvData.Length; i++)
    //        {
    //            //create new rows
    //            DataRow row = csvDataTable.NewRow();
    //            string csvdata = csvData[i];
    //            //欄位不夠, 加下一列資料,直到欄位與表頭相等
    //            while (csvdata.Split(',').Length < headings.Length)
    //            {
    //                i++;
    //                csvdata += csvData[i];
    //            }

    //            string[] csvrows = csvdata.Replace("\r", "").Replace("\n", "").Split(',');

    //            for (int j = 0; j < headings.Length; j++)
    //            {
    //                string csvrow = csvrows[j];
    //                //fill them
    //                row[j] = csvrow.Replace("\"", "").Replace("'", "");
    //            }

    //            //add rows to over DataTable
    //            csvDataTable.Rows.Add(row);
    //        }
    //        rsp.Result = true;
    //        rsp.ReturnData = csvDataTable;
    //    }
    //    catch (Exception ex)
    //    {
    //        rsp.Result = false;
    //        rsp.ReturnMessage = ex.Message;
    //    }
    //    //return the CSV DataTable
    //    return rsp;

    //}

    /// <summary>
    /// csv 轉 DataTable (split {"} 、 {'} )
    /// </summary>
    /// <param name="file">檔案</param>
    /// <param name="isRowOneHeader">表頭</param>
    /// <returns></returns>
    public static RServiceProvider csvToDataTable(Stream stream, bool isRowOneHeader)
    {
        RServiceProvider rsp = new RServiceProvider();
        DataTable csvDataTable = new DataTable();
        try
        {
            //no try/catch - add these in yourselfs or let exception happen
            //String[] csvData = File.ReadAllLines(HttpContext.Current.Server.MapPath(file));
            String[] csvData = ReadLines(stream, Encoding.GetEncoding(950)).ToArray();

            //if no data in file ‘manually’ throw an exception
            if (csvData.Length == 0)
            {
                throw new Exception("CSV檔是空檔");
            }

            String[] headings = splitCSV(csvData[0]);
            int index = 0; //will be zero or one depending on isRowOneHeader
            if (isRowOneHeader) //if first record lists headers
            {
                index = 1; //so we won’t take headings as data

                //for each heading
                for (int i = 0; i < headings.Length; i++)
                {
                    //replace spaces with underscores for column names
                    //headings[i] = headings[i].Replace(" ", "").Replace("\"", "").Replace("'", "");
                    //add a column for each heading
                    csvDataTable.Columns.Add(headings[i].Trim(), typeof(string));
                }
            }
            else //if no headers just go for col1, col2 etc.
            {
                for (int i = 0; i < headings.Length; i++)
                {
                    //create arbitary column names
                    csvDataTable.Columns.Add("col" + (i + 1).ToString(), typeof(string));
                }
            }

            //populate the DataTable
            for (int i = index; i < csvData.Length; i++)
            {
                //create new rows
                DataRow row = csvDataTable.NewRow();
                string csvdata = csvData[i];
                //欄位不夠, 加下一列資料,直到欄位與表頭相等
                while (splitCSV(csvdata).Length < headings.Length)
                {
                    i++;
                    csvdata += ",";
                }

                string[] csvrows = splitCSV(csvdata.Replace("\r", "").Replace("\n", "").Replace("\t", ""));

                for (int j = 0; j < headings.Length; j++)
                    //fill them
                    row[j] = csvrows[j].Replace("\"","").Replace("'","");

                //add rows to over DataTable
                csvDataTable.Rows.Add(row);
            }
            rsp.Result = true;
            rsp.ReturnData = csvDataTable;
        }
        catch (Exception ex)
        {
            rsp.Result = false;
            rsp.ReturnMessage = ex.Message;
        }
        //return the CSV DataTable
        return rsp;
    }

    public static IEnumerable<string> ReadLines(Stream streamProvider, Encoding encoding)
    {
        using (var stream = streamProvider)
        using (var reader = new StreamReader(stream, encoding))
        {
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                yield return line;
            }
        }
    }

    /// <summary>
    /// 解析CSV檔
    /// </summary>
    /// <param name="src"></param>
    /// <returns></returns>
    private static String[] splitCSV(String src)
    {
        if (src == null || src.Equals("")) return new String[0];
        StringBuilder st = new StringBuilder();
        List<string> result = new List<string>();
        char beginWithQuote = ' ';
        for (int i = 0; i < src.Length; i++)
        {
            char ch = src[i];
            if (ch == '\"' || ch == '\'')
            {
                if (beginWithQuote == ch) //是不是結尾
                {
                    i++;
                    if (i >= src.Length)
                    {
                        result.Add(st.ToString());
                        st = new StringBuilder();
                        beginWithQuote = ' ';
                    }
                    else
                    {
                        ch = src[i];
                        if (ch == '\"' || ch == '\'')
                        {
                            st.Append(ch);
                        }
                        else if (ch == ',')
                        {
                            result.Add(st.ToString());
                            st = new StringBuilder();
                            beginWithQuote = ' ';
                        }
                        //else
                        //{
                        //    throw new Exception("Single double-quote char mustn't exist in filed " + (result.Count + 1) + " while it is begined with quote\nchar at:" + i);
                        //}
                    }
                }
                else if (st.Length == 0 )
                {
                    if( beginWithQuote == ' ' )
                        beginWithQuote = ch;
                }
                //else
                //{
                //    throw new Exception("Quote cannot exist in a filed which doesn't begin with quote!\nfield:" + (result.Count + 1));
                //}
            }
            else if (ch == ',')
            {
                if (beginWithQuote != ' ')
                {
                    st.Append(ch);
                }
                else
                {
                    result.Add(st.ToString());
                    st = new StringBuilder();
                    beginWithQuote = ' ';
                }
            }
            else
            {
                st.Append(ch);
            }
        }
        if (st.Length != 0)
        {
            //if (beginWithQuote != ' ')
            //{
            //    throw new Exception("last field is begin with but not end with double quote");
            //}
            //else
            //{
                result.Add(st.ToString());
            //}
        }
        if (src.EndsWith(","))
            result.Add("");
        String[] rs = new String[result.Count];

        for (int i = 0; i < rs.Length; i++)
        {
            rs[i] = (String)result[i];
        }
        return rs;
    }

    //public static String[] splitCSV(String src)
    //{
    //    if (src == null || src.Equals("")) return new String[0];
    //    StringBuilder st = new StringBuilder();
    //    List<string> result = new List<string>();
    //    bool beginWithQuote = false;
    //    for (int i = 0; i < src.Length; i++)
    //    {
    //        char ch = src[i];
    //        if (ch == '\"')
    //        {
    //            if (beginWithQuote)
    //            {
    //                i++;
    //                if (i >= src.Length)
    //                {
    //                    result.Add(st.ToString());
    //                    st = new StringBuilder();
    //                    beginWithQuote = false;
    //                }
    //                else
    //                {
    //                    ch = src[i];
    //                    if (ch == '\"')
    //                    {
    //                        st.Append(ch);
    //                    }
    //                    else if (ch == ',')
    //                    {
    //                        result.Add(st.ToString());
    //                        st = new StringBuilder();
    //                        beginWithQuote = false;
    //                    }
    //                    else
    //                    {
    //                        throw new Exception("Single double-quote char mustn't exist in filed " + (result.Count + 1) + " while it is begined with quote\nchar at:" + i);
    //                    }
    //                }
    //            }
    //            else if (st.Length == 0)
    //            {
    //                beginWithQuote = true;
    //            }
    //            else
    //            {
    //                throw new Exception("Quote cannot exist in a filed which doesn't begin with quote!\nfield:" + (result.Count + 1));
    //            }
    //        }
    //        else if (ch == ',')
    //        {
    //            if (beginWithQuote)
    //            {
    //                st.Append(ch);
    //            }
    //            else
    //            {
    //                result.Add(st.ToString());
    //                st = new StringBuilder();
    //                beginWithQuote = false;
    //            }
    //        }
    //        else
    //        {
    //            st.Append(ch);
    //        }
    //    }
    //    if (st.Length != 0)
    //    {
    //        if (beginWithQuote)
    //        {
    //            throw new Exception("last field is begin with but not end with double quote");
    //        }
    //        else
    //        {
    //            result.Add(st.ToString());
    //        }
    //    }
    //    if (src.EndsWith(","))
    //        result.Add("");
    //    String[] rs = new String[result.Count];

    //    for (int i = 0; i < rs.Length; i++)
    //    {
    //        rs[i] = (String)result[i];
    //    }
    //    return rs;
    //}

    #endregion

    #region 匯入Excel資料轉成DataTable
    public static RServiceProvider ReadExcel(FileUpload fu, int SheetIndex)
    {
        RServiceProvider rsp = new RServiceProvider();
        DataTable table = new DataTable();
        try
        {
            HSSFWorkbook workbook = new HSSFWorkbook(fu.FileContent);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheetAt(SheetIndex);
            HSSFRow headerRow = (HSSFRow)sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue);
                table.Columns.Add(column);
            }
            int rowCount = sheet.LastRowNum;
            for (int i = (sheet.FirstRowNum + 1); i < sheet.LastRowNum + 1; i++)
            {
                HSSFRow row = (HSSFRow)sheet.GetRow(i);
                if (row != null)
                {
                    DataRow dataRow = table.NewRow();
                    for (int j = row.FirstCellNum; j < cellCount; j++)
                    {
                        if (row.GetCell(j) != null)
                            dataRow[j] = row.GetCell(j).ToString();
                    }
                    table.Rows.Add(dataRow);
                }
            }
            workbook = null;
            sheet = null;

            rsp.Result = true;
            rsp.ReturnData = table;
        }
        catch (Exception ex)
        {
            rsp.Result = false;
            rsp.ReturnMessage = ex.Message;
        }
        return rsp;
    }
    public static DataTable ReadExcel(string fileName)
    {
        using (FileStream fs = new FileStream(fileName, FileMode.Open))
        {
            HSSFWorkbook wb = new HSSFWorkbook(fs);
            ISheet sheet = wb.GetSheetAt(0);
            DataTable table = new DataTable();
            //由第一列取標題做為欄位名稱
            IRow headerRow = sheet.GetRow(0);
            int cellCount = headerRow.LastCellNum;
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
                //以欄位文字為名新增欄位，此處全視為字串型別以求簡化
                table.Columns.Add(
                    new DataColumn(headerRow.GetCell(i).StringCellValue));

            //略過第零列(標題列)，一直處理至最後一列
            for (int i = (sheet.FirstRowNum + 1); i < sheet.LastRowNum + 1; i++)
            {
                IRow row = sheet.GetRow(i);
                if (row == null) continue;
                DataRow dataRow = table.NewRow();
                //依先前取得的欄位數逐一設定欄位內容
                for (int j = row.FirstCellNum; j < cellCount; j++)
                    if (row.GetCell(j) != null)
                        //如要針對不同型別做個別處理，可善用.CellType判斷型別
                        //再用.StringCellValue, .DateCellValue, .NumericCellValue...取值
                        //此處只簡單轉成字串
                        dataRow[j] = row.GetCell(j).ToString();
                table.Rows.Add(dataRow);
            }
            return table;
        }
    }
    #endregion

    #region DataSet to Excel
    /// <summary>
    /// DataSet to Excel
    /// </summary>
    /// <param name="ds">資料集</param>
    /// <param name="columnsOrder">欄位順序(Name, Caption)</param>
    /// <returns>轉Excel字串</returns>
    public static string DataSourceToExcel(string fileName, DataSet ds, Dictionary<string, string> columnsOrder)
    {
        return DataSourceToExcel(fileName, ds, "", columnsOrder, 0);
    }

    public static string DataSourceToExcel(string fileName, DataSet ds, Dictionary<string, string> columnsOrder, string title)
    {
        return DataSourceToExcel(fileName, ds, columnsOrder, title, 0);
    }


    public static string DataSourceToExcel(string fileName, DataSet ds, Dictionary<string, string> columnsOrder, int border)
    {
        return DataSourceToExcel(fileName, ds, columnsOrder, "", border);
    }

    public static string DataSourceToExcel(string fileName, DataSet ds, Dictionary<string, string> columnsOrder, string title, int border)
    {
        return DataSourceToExcel(fileName, ds, "", columnsOrder, title, border);
    }

    public static string DataSourceToExcel(string fileName, DataSet ds, string sort, Dictionary<string, string> columnsOrder)
    {
        return DataSourceToExcel(fileName, ds, sort, columnsOrder, 0);
    }

    public static string DataSourceToExcel(string fileName, DataSet ds, string sort, Dictionary<string, string> columnsOrder, string title)
    {
        return DataSourceToExcel(fileName, ds, sort, columnsOrder, title, 0);
    }


    public static string DataSourceToExcel(string fileName, DataSet ds, string sort, Dictionary<string, string> columnsOrder, int border)
    {
        return DataSourceToExcel(fileName, ds, sort, columnsOrder, "", border);
    }

    /// <summary>
    /// DataSet to Excel
    /// </summary>
    /// <param name="ds">資料集</param>
    /// <param name="columnsOrder">欄位順序(Name, Caption)</param>
    /// <param name="border">格線</param>
    /// <returns>轉Excel字串</returns>
    public static string DataSourceToExcel(string fileName, DataSet ds, string sort, Dictionary<string, string> columnsOrder, string title, int border)
    {
        if (ds != null && ds.Tables.Count == 0)
            return "........查無資料..........";
        else
            return DataSourceToExcel(fileName, ds.Tables[0], sort, columnsOrder, title, border);
    }

    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="dt">資料表</param>
    /// <param name="columnsOrder">欄位順序(Name, Caption)</param>
    /// <returns>轉Excel字串</returns>
    public static string DataSourceToExcel(string fileName, DataTable dt, Dictionary<string, string> columnsOrder)
    {
        return DataSourceToExcel(fileName, dt, columnsOrder, 0);
    }

    public static string DataSourceToExcel(string fileName, DataTable dt, Dictionary<string, string> columnsOrder, string title)
    {
        return DataSourceToExcel(fileName, dt, columnsOrder, title, 0);
    }
    public static string DataSourceToExcel(string fileName, DataTable dt, Dictionary<string, string> columnsOrder, int border)
    {
        return DataSourceToExcel(fileName, dt, columnsOrder, "", border);
    }

    public static string DataSourceToExcel(string fileName, DataTable dt, Dictionary<string, string> columnsOrder, string title, int border)
    {
        return DataSourceToExcel(fileName, dt, "", columnsOrder, title, border);
    }

    public static string DataSourceToExcel(string fileName, DataTable dt, string sort, Dictionary<string, string> columnsOrder)
    {
        return DataSourceToExcel(fileName, dt, sort, columnsOrder, "");
    }

    public static string DataSourceToExcel(string fileName, DataTable dt, string sort, Dictionary<string, string> columnsOrder, string title)
    {
        return DataSourceToExcel(fileName, dt, sort, columnsOrder, title, 0);
    }
    public static string DataSourceToExcel(string fileName, DataTable dt, string sort, Dictionary<string, string> columnsOrder, int border)
    {
        return DataSourceToExcel(fileName, dt, sort, columnsOrder, "", border);
    }
    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="dt">資料表</param>
    /// <param name="columnsOrder">欄位順序(Name, Caption)</param>
    /// <param name="border">格線</param>
    /// <returns>轉Excel字串</returns>
    public static string DataSourceToExcel(string fileName, DataTable dt, string sort, Dictionary<string, string> columnsOrder, string title, int border)
    {

        if (dt.Rows.Count == 0)
            return "........查無資料..........";
        else
        {
            string fn = string.Format("inline; filename={0}", HttpContext.Current.Server.UrlEncode(string.Format("{0}{1}.xls", fileName, SqlAccess.GetServerDateTime("0").ToString("yyyyMMddHHmm"))));
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.AppendHeader("content-disposition", fn);
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.Expires = 0;
            HttpContext.Current.Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\">");

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<table id=\"table1\" cellspacing=\"0\" cellpadding=\"1\" border=\"{0}\">", border);
            if (title.Trim().Length > 0) //表頭
                sb.AppendFormat("<tr><td colspan=\"{0}\" aling=\"center\">{1}</td></tr>", dt.Columns.Count, title);
            sb.Append("<tr>");
            foreach (KeyValuePair<string, string> kvp in columnsOrder)
            {
                if (dt.Columns.Contains(kvp.Key))
                    sb.AppendFormat("<td align=\"center\">{0}</td>", kvp.Value);
            }
            sb.Append("</tr>");
            DataView dv = dt.DefaultView;
            dv.Sort = sort;
            foreach (DataRowView drv in dv)
            {
                sb.Append("<tr>");
                foreach (string cn in columnsOrder.Keys)
                {
                    if (!dt.Columns.Contains(cn))
                        continue;

                    if (dt.Columns[cn].DataType == typeof(string))
                        sb.AppendFormat("<td align=\"center\">{0}&nbsp;</td>", drv[cn]);
                    else
                        sb.AppendFormat("<td align=\"center\">{0}</td>", drv[cn]);
                }
                sb.Append("</tr>");
            }
            sb.AppendFormat("<tr><td align=\"center\" colspan=\"{0}\"><hr></td></tr></table>", dt.Columns.Count - 1);
            return sb.ToString();
        }
    }
    #endregion

    /// <summary>
    /// DataTable to Excel(XML)
    /// </summary>
    /// <param name="dt">資料表</param>
    /// <param name="columnsOrder">欄位順序(Name, Caption)</param>
    /// <param name="border">格線</param>
    /// <returns>轉Excel字串</returns>
    //public static string DataSourceToExcelXML(DataTable dt, string sort, Dictionary<string, string> columnsOrder, string title, int border)
    //{
    //    string fn = string.Format("inline; filename=ProductsLabel{0}.xls", SqlAccess.GetServerDateTime("0").ToString("yyyy/MM/dd"));
    //    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
    //    HttpContext.Current.Response.AppendHeader("content-disposition", fn);
    //    HttpContext.Current.Response.Charset = "utf-8";
    //    HttpContext.Current.Response.Expires = 0;
    //    HttpContext.Current.Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">");

    //    if (dt.Rows.Count == 0)
    //        return "........查無資料..........";
    //    else
    //    {

    //        StringBuilder sb = new StringBuilder();

    //        <?xml version="1.0"?>
    //        <?mso-application progid="Excel.ISheet"?>
    //        <Workbook
    //        xmlns="urn:schemas-microsoft-com:office:spreadsheet"
    //         xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
    //         <Worksheet ss:Name="Sheet1">
    //         <Table>
    //          <IRow>
    //           <Cell ss:MergeAcross="1"><Data ss:Type="String">Excel xml</Data></Cell>
    //          </IRow>
    //          <IRow>
    //          <Cell><Data ss:Type="String">A2</Data></Cell>
    //          <Cell><Data ss:Type="Number">0.112</Data></Cell>
    //          </IRow>
    //         </Table>
    //         </Worksheet>
    //        </Workbook>
    //        sb.AppendFormat("<table id=\"table1\" cellspacing=\"0\" cellpadding=\"1\" border=\"{0}\">", border);
    //        if (title.Trim().Length > 0) //表頭
    //            sb.AppendFormat("<tr><td colspan=\"16\" aling=\"center\">{0}</td></tr>", title);
    //        sb.Append("<tr>");
    //        foreach (KeyValuePair<string, string> kvp in columnsOrder)
    //        {
    //            if (dt.Columns.Contains(kvp.Key))
    //                sb.AppendFormat("<td align=\"center\">{0}</td>", kvp.Value);
    //        }

    //        sb.Append("</tr>");

    //        DataView dv = dt.DefaultView;
    //        dv.Sort = sort;

    //        foreach (DataRowView drv in dv)
    //        {
    //            sb.Append("<tr>");
    //            foreach (string cn in columnsOrder.Keys)
    //            {
    //                if (!dt.Columns.Contains(cn))
    //                    continue;

    //                if (dt.Columns[cn].DataType == typeof(string))
    //                    sb.AppendFormat("<td align=\"center\">{0}&nbsp;</td>", drv[cn]);
    //                else
    //                    sb.AppendFormat("<td align=\"center\">{0}</td>", drv[cn]);
    //            }
    //            sb.Append("</tr>");
    //        }
    //        sb.AppendFormat("<tr><td align=\"center\" colspan=\"{0}\"><hr></td></tr></table>", dt.Columns.Count - 1);
    //        return sb.ToString();
    //    }
    //}

    #region DataTable 轉 CSV
    public static void CreateCSVFile(DataTable dt, string strFilePath)
    {
        // Create the CSV file to which grid data will be exported.
        StreamWriter sw = new StreamWriter(strFilePath, false, Encoding.UTF8);
        // First we will write the headers.
        //DataTable dt = m_dsProducts.Tables[0];
        int iColCount = dt.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {
            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }
        }
        sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();
    }

    public static void CreateCSVFile(Page page, DataTable dt, string fileName)
    {
        // Create the CSV file to which grid data will be exported.
        StringWriter sw = new StringWriter();
        // First we will write the headers.
        //DataTable dt = m_dsProducts.Tables[0];
        int iColCount = dt.Columns.Count;
        for (int i = 0; i < iColCount; i++)
        {
            sw.Write(dt.Columns[i]);
            if (i < iColCount - 1)
            {
                sw.Write(",");
            }
        }
        sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]))
                {
                    sw.Write(dr[i].ToString());
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlPathEncode(fileName));
        HttpContext.Current.Response.Charset = "UTF-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.HeaderEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.ContentType = "text/csv";
        HttpContext.Current.Response.BinaryWrite(new byte[] { 0xEF, 0xBB, 0xBF });

        HttpContext.Current.Response.Write(sw);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }

    /// <summary>
    /// 無分號斷點
    /// </summary>
    /// <param name="page"></param>
    /// <param name="dt"></param>
    /// <param name="fileName"></param>
    public static void CreateCSVFile2(Page page, DataTable dt, string fileName)
    {
        // Create the CSV file to which grid data will be exported.
        StringWriter sw = new StringWriter();
        // First we will write the headers.
        //DataTable dt = m_dsProducts.Tables[0];
        int iColCount = dt.Columns.Count;
        //for (int i = 0; i < iColCount; i++)
        //{
        //    sw.Write(dt.Columns[i]);
        //    if (i < iColCount - 1)
        //    {
        //        sw.Write(",");
        //    }
        //}
        //sw.Write(sw.NewLine);
        // Now write all the rows.
        foreach (DataRow dr in dt.Rows)
        {
            for (int i = 0; i < iColCount; i++)
            {
                if (!Convert.IsDBNull(dr[i]) && !dr[i].ToString().Contains(","))
                {
                    sw.Write(dr[i].ToString());
                }
                else
                {
                    string val = dr[i].ToString().Replace(",", "");
                    sw.Write(val);
                }
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
        }
        sw.Close();
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlPathEncode(fileName));
        HttpContext.Current.Response.Charset = "UTF-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.HeaderEncoding = System.Text.Encoding.UTF8;
        HttpContext.Current.Response.ContentType = "text/csv";
        HttpContext.Current.Response.BinaryWrite(new byte[] { 0xEF, 0xBB, 0xBF });

        HttpContext.Current.Response.Write(sw);
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
    }
    #endregion



}
