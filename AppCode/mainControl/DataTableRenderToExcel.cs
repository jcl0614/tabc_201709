﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using NPOI;
using NPOI.HPSF;
using NPOI.HSSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.POIFS;
using NPOI.Util;
using System.Text;
using System.Net;
using System.Drawing.Imaging;

/// <summary>
/// NPOI 的摘要描述
/// </summary>
public class DataTableRenderToExcel
{

    #region DataTable to Excel
    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="SourceTable">資料表</param>
    /// <returns></returns>
    public static Stream RenderDataTableToExcel(DataTable SourceTable)
    {
        return RenderDataTableToExcel("", SourceTable);
    }
    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="SourceTable">資料表</param>
    /// <returns></returns>
    public static Stream RenderDataTableToExcel(string title, DataTable SourceTable)
    {
        return RenderDataTableToExcel(title, true, SourceTable);
    }
    public static Stream RenderDataTableToExcel(string title, string ISheet, DataTable SourceTable)
    {
        return RenderDataTableToExcel(title, ISheet, true, SourceTable);
    }
    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="headvisible">表頭</param>
    /// <param name="SourceTable">資料</param>
    /// <returns></returns>
    public static Stream RenderDataTableToExcel(bool headvisible, DataTable SourceTable)
    {
        return RenderDataTableToExcel("", headvisible, SourceTable);
    }

    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="headvisible">表頭</param>
    /// <param name="SourceTable">資料</param>
    /// <returns></returns>
    public static Stream RenderDataTableToExcel(string title, bool headvisible, DataTable SourceTable)
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        MemoryStream ms = new MemoryStream();
        ISheet sheet = workbook.CreateSheet();
        sheet.DisplayGridlines = true;
        int header = 0;
        if (title.Trim().Length > 0)
        {         
            IRow titleRow = sheet.CreateRow(0);            
            ICell cell = titleRow.CreateCell(0);
            cell.SetCellValue(title);
            ICellStyle　style　=　workbook.CreateCellStyle();
            style.Alignment　=　HorizontalAlignment.Center;
            IFont　font　=　workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.Bold; //粗體
            font.FontHeight　=　20*20;
            style.SetFont(font);
            cell.CellStyle　=　style;

            //合併ICell
            sheet.AddMergedRegion(new　NPOI.SS.Util.CellRangeAddress(0,　0,　0,　SourceTable.Columns.Count-1));
            
            header++;
        }
        IRow headerRow = sheet.CreateRow(header);
        ICellStyle headerStyle = workbook.CreateCellStyle();
        headerStyle.BorderBottom = BorderStyle.Thin;
        headerStyle.BorderTop = BorderStyle.Thin;
        headerStyle.BorderLeft = BorderStyle.Thin;
        headerStyle.BorderRight = BorderStyle.Thin;

        if (headvisible)
        {
            // handling header.
            foreach (DataColumn column in SourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = headerStyle;
                if (column.Caption.Trim().Length == 0)
                    headerCell.SetCellValue(column.ColumnName);
                else
                    headerCell.SetCellValue(column.Caption);
            }
            header++;
        }

        // handling value.
        int rowIndex = header;

        foreach (DataRow row in SourceTable.Rows)
        {
            IRow dataRow = sheet.CreateRow(rowIndex);

            foreach (DataColumn column in SourceTable.Columns)
            {
                sheet.AutoSizeColumn(SourceTable.Columns.IndexOf(column));
                ICell dataCell = dataRow.CreateCell(column.Ordinal);
                dataCell.CellStyle = headerStyle;
                if (column.DataType == typeof(int) || column.DataType == typeof(decimal))
                {
                    if( row[column].ToString().Trim().Length == 0 )
                        dataCell.SetCellValue("");
                    else
                        dataCell.SetCellValue(Convert.ToDouble(row[column]));
                }
                else if (column.DataType == typeof(DateTime))
                {
                    if (row[column].ToString().Trim().Length == 0)
                        dataCell.SetCellValue("");
                    else
                        dataCell.SetCellValue(Convert.ToDateTime(row[column]));
                }
                else
                    dataCell.SetCellValue(row[column].ToString());
            }

            rowIndex++;
        }

        workbook.Write(ms);
        ms.Flush();
        ms.Position = 0;

        sheet = null;
        headerRow = null;
        workbook = null;

        return ms;
    }
    public static Stream RenderDataTableToExcel(string title, string ISheet, bool headvisible, DataTable SourceTable)
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        MemoryStream ms = new MemoryStream();
        ISheet sheet = workbook.CreateSheet(ISheet);
        sheet.DisplayGridlines = true;
        int header = 0;
        if (title.Trim().Length > 0)
        {
            IRow titleRow = sheet.CreateRow(0);
            ICell cell = titleRow.CreateCell(0);
            cell.SetCellValue(title);
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            IFont font = workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.Bold; //粗體
            font.FontHeight = 20 * 20;
            style.SetFont(font);
            cell.CellStyle = style;

            //合併ICell
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, SourceTable.Columns.Count - 1));

            header++;
        }
        IRow headerRow = sheet.CreateRow(header);
        ICellStyle headerStyle = workbook.CreateCellStyle();
        headerStyle.BorderBottom = BorderStyle.Thin;
        headerStyle.BorderTop = BorderStyle.Thin;
        headerStyle.BorderLeft = BorderStyle.Thin;
        headerStyle.BorderRight = BorderStyle.Thin;

        if (headvisible)
        {
            // handling header.
            foreach (DataColumn column in SourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = headerStyle;
                if (column.Caption.Trim().Length == 0)
                    headerCell.SetCellValue(column.ColumnName);
                else
                    headerCell.SetCellValue(column.Caption);
            }
            header++;
        }

        // handling value.
        int rowIndex = header;

        foreach (DataRow row in SourceTable.Rows)
        {
            IRow dataRow = sheet.CreateRow(rowIndex);

            foreach (DataColumn column in SourceTable.Columns)
            {
                sheet.AutoSizeColumn(SourceTable.Columns.IndexOf(column));
                ICell dataCell = dataRow.CreateCell(column.Ordinal);
                dataCell.CellStyle = headerStyle;
                if (column.DataType == typeof(int) || column.DataType == typeof(decimal))
                {
                    if (row[column].ToString().Trim().Length == 0)
                        dataCell.SetCellValue("");
                    else
                        dataCell.SetCellValue(Convert.ToDouble(row[column]));
                }
                else if (column.DataType == typeof(DateTime))
                {
                    if (row[column].ToString().Trim().Length == 0)
                        dataCell.SetCellValue("");
                    else
                        dataCell.SetCellValue(Convert.ToDateTime(row[column]));
                }
                else
                    dataCell.SetCellValue(row[column].ToString());
            }

            rowIndex++;
        }

        workbook.Write(ms);
        ms.Flush();
        ms.Position = 0;

        sheet = null;
        headerRow = null;
        workbook = null;

        return ms;
    }

    public static Stream RenderDataTableToExcel(Dictionary<string, DataTable> dt)
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        MemoryStream ms = new MemoryStream();
        foreach (var item in dt)
        {
            ISheet sheet = workbook.CreateSheet(item.Key);
            sheet.DisplayGridlines = true;
            int header = 0;
            //if (item.Value.TableName.Trim().Length > 0)
            //{
            //    IRow titleRow = sheet.CreateRow(0);
            //    ICell cell = titleRow.CreateCell(0);
            //    cell.SetCellValue(item.Value.TableName);
            //    ICellStyle style = workbook.CreateCellStyle();
            //    style.Alignment = HorizontalAlignment.Center;
            //    IFont font = workbook.CreateFont();
            //    font.Boldweight = (short)FontBoldWeight.Bold; //粗體
            //    font.FontHeight = 20 * 20;
            //    style.SetFont(font);
            //    cell.CellStyle = style;

            //    //合併ICell
            //    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, item.Value.Columns.Count - 1));

            //    header++;
            //}
            IRow headerRow = sheet.CreateRow(header);
            ICellStyle headerStyle = workbook.CreateCellStyle();
            headerStyle.BorderBottom = BorderStyle.Thin;
            headerStyle.BorderTop = BorderStyle.Thin;
            headerStyle.BorderLeft = BorderStyle.Thin;
            headerStyle.BorderRight = BorderStyle.Thin;

            if (true)
            {
                // handling header.
                foreach (DataColumn column in item.Value.Columns)
                {
                    ICell headerCell = headerRow.CreateCell(column.Ordinal);
                    headerCell.CellStyle = headerStyle;
                    if (column.Caption.Trim().Length == 0)
                        headerCell.SetCellValue(column.ColumnName);
                    else
                        headerCell.SetCellValue(column.Caption);
                }
                header++;
            }

            // handling value.
            int rowIndex = header;

            foreach (DataRow row in item.Value.Rows)
            {
                IRow dataRow = sheet.CreateRow(rowIndex);

                foreach (DataColumn column in item.Value.Columns)
                {
                    sheet.AutoSizeColumn(item.Value.Columns.IndexOf(column));
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);
                    dataCell.CellStyle = headerStyle;
                    if (column.DataType == typeof(int) || column.DataType == typeof(decimal))
                    {
                        if (row[column].ToString().Trim().Length == 0)
                            dataCell.SetCellValue("");
                        else
                            dataCell.SetCellValue(Convert.ToDouble(row[column]));
                    }
                    else if (column.DataType == typeof(DateTime))
                    {
                        if (row[column].ToString().Trim().Length == 0)
                            dataCell.SetCellValue("");
                        else
                            dataCell.SetCellValue(Convert.ToDateTime(row[column]));
                    }
                    else
                        dataCell.SetCellValue(row[column].ToString());
                }

                rowIndex++;
            }
        }

        workbook.Write(ms);
        ms.Flush();
        ms.Position = 0;

        //sheet = null;
        //headerRow = null;
        //workbook = null;

        return ms;
    }

    /// <summary>
    /// DataTable 轉換 EXCEL 匯出 (含圖片參數)
    /// </summary>
    /// <param name="title">於第一列印出標題文字</param>
    /// <param name="ISheet">工作表名稱</param>
    /// <param name="headvisible">是否顯示標頭</param>
    /// <param name="SourceTable">來源資料(DataTable)</param>
    /// <param name="picCollIndex">圖片URL所在欄位索引(此欄位將轉換成圖片)</param>
    /// <param name="maxPicWidth">圖片顯示最大寬度</param>
    /// <returns></returns>
    public static MemoryStream RenderDataTableToExcel(string title, string ISheet, bool headvisible, DataTable SourceTable, int picCollIndex, int maxPicWidth)
    {
        HSSFWorkbook workbook = new HSSFWorkbook();
        MemoryStream ms = new MemoryStream();
        ISheet sheet = workbook.CreateSheet(ISheet);
        sheet.DisplayGridlines = true;
        var request = new WebClient();
        var patriarch = sheet.CreateDrawingPatriarch();
        int header = 0;
        if (title.Trim().Length > 0)
        {
            IRow titleRow = sheet.CreateRow(0);
            ICell cell = titleRow.CreateCell(0);
            cell.SetCellValue(title);
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            IFont font = workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.Bold; //粗體
            font.FontHeight = 20 * 20;
            style.SetFont(font);
            cell.CellStyle = style;

            //合併ICell
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, SourceTable.Columns.Count - 1));

            header++;
        }
        IRow headerRow = sheet.CreateRow(header);
        ICellStyle headerStyle = workbook.CreateCellStyle();
        headerStyle.BorderBottom = BorderStyle.Thin;
        headerStyle.BorderTop = BorderStyle.Thin;
        headerStyle.BorderLeft = BorderStyle.Thin;
        headerStyle.BorderRight = BorderStyle.Thin;

        if (headvisible)
        {
            // handling header.
            foreach (DataColumn column in SourceTable.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = headerStyle;
                if (column.Caption.Trim().Length == 0)
                    headerCell.SetCellValue(column.ColumnName);
                else
                    headerCell.SetCellValue(column.Caption);
            }
            header++;
        }

        // handling value.
        int rowIndex = header;

        foreach (DataRow row in SourceTable.Rows)
        {
            IRow dataRow = sheet.CreateRow(rowIndex);

            foreach (DataColumn column in SourceTable.Columns)
            {
                sheet.AutoSizeColumn(SourceTable.Columns.IndexOf(column));
                if (SourceTable.Columns.IndexOf(column) != picCollIndex)
                {
                    ICell dataCell = dataRow.CreateCell(column.Ordinal);
                    dataCell.CellStyle = headerStyle;
                    if (column.DataType == typeof(int) || column.DataType == typeof(decimal))
                    {
                        if (row[column].ToString().Trim().Length == 0)
                            dataCell.SetCellValue("");
                        else
                            dataCell.SetCellValue(Convert.ToDouble(row[column]));
                    }
                    else if (column.DataType == typeof(DateTime))
                    {
                        if (row[column].ToString().Trim().Length == 0)
                            dataCell.SetCellValue("");
                        else
                            dataCell.SetCellValue(Convert.ToDateTime(row[column]));
                    }
                    else
                        dataCell.SetCellValue(row[column].ToString());
                }
                else
                {
                    sheet.SetColumnWidth(picCollIndex, maxPicWidth / 6 * 256); 
                    try
                    {
                        // 下載圖片             
                        var image = System.Drawing.Image.FromStream(request.OpenRead(new Uri(row[picCollIndex].ToString().Trim())));
                        // 產生縮圖             
                        decimal sizeRatio = ((decimal)image.Height / image.Width);
                        int thumbWidth = image.Width > maxPicWidth ? maxPicWidth : image.Width;
                        int thumbHeight = image.Width > maxPicWidth ? decimal.ToInt32(sizeRatio * thumbWidth) : image.Height;
                        var thumbStream = image.GetThumbnailImage(thumbWidth, thumbHeight, () => false, IntPtr.Zero);
                        var memoryStream = new MemoryStream();
                        thumbStream.Save(memoryStream, ImageFormat.Jpeg);
                        // 將縮圖加入到 workbook 中            
                        var pictureIndex = workbook.AddPicture(memoryStream.ToArray(), PictureType.JPEG);
                        // 將縮圖定位到 worksheet 中             
                        var anchor = new HSSFClientAnchor(0, 0, thumbWidth, thumbHeight, picCollIndex, dataRow.RowNum, picCollIndex, dataRow.RowNum);
                        var picture = patriarch.CreatePicture(anchor, pictureIndex);
                        dataRow.HeightInPoints = thumbHeight;
                        picture.Resize(1);

                    }
                    catch (Exception ex)
                    {
                        // 圖片載入失敗，顯示錯誤訊息             
                        string s = ex.Message;
                    }
                }

            }

            rowIndex++;
        }

        workbook.Write(ms);
        ms.Flush();
        ms.Position = 0;

        sheet = null;
        headerRow = null;
        workbook = null;

        return ms;
    }

    /// <summary>
    /// DataTable to Excel
    /// </summary>
    /// <param name="SourceTable">資料表</param>
    /// <param name="FileName">檔案名稱</param>
    public static void RenderDataTableToExcel(DataTable SourceTable, string FileName)
    {
        RenderDataTableToExcel("", SourceTable, FileName);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="title"></param>
    /// <param name="SourceTable"></param>
    /// <param name="FileName"></param>
    public static void RenderDataTableToExcel(bool headvisible, DataTable SourceTable, string FileName)
    {
        RenderDataTableToExcel("", headvisible, SourceTable, FileName);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="SourceTable">資料表</param>
    /// <param name="FileName">檔案名稱</param>
    public static void RenderDataTableToExcel(string title, DataTable SourceTable, string FileName)
    {
        RenderDataTableToExcel(title, true, SourceTable, FileName);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="title"></param>
    /// <param name="headvisible"></param>
    /// <param name="SourceTable"></param>
    /// <param name="FileName"></param>
    public static void RenderDataTableToExcel(string title, bool headvisible, DataTable SourceTable, string FileName)
    {

        MemoryStream ms = RenderDataTableToExcel(title, headvisible, SourceTable) as MemoryStream;
        FileStream fs = new FileStream(FileName.IndexOf(".xls")==-1 ? (FileName.IndexOf(".csv")==-1 ? string.Format("{0}.csv",FileName) : FileName) : FileName, FileMode.Create, FileAccess.Write);
        byte[] data = ms.ToArray();

        fs.Write(data, 0, data.Length);
        fs.Flush();
        fs.Close();

        data = null;
        ms = null;
        fs = null;
    }
    #endregion

    #region DataView to Excel
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="SourceView">資料</param>
    /// <returns></returns>
    public static Stream RenderDataViewToExcel(DataView SourceView)
    {
        return RenderDataViewToExcel("", SourceView);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="headvisible"></param>
    /// <returns></returns>
    public static Stream RenderDataViewToExcel(bool headvisible, DataView SourceView)
    {
        return RenderDataViewToExcel("", headvisible, SourceView);
    }
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="SourceView">資料</param>
    /// <returns></returns>
    public static Stream RenderDataViewToExcel(string title, DataView SourceView)
    {
        return RenderDataViewToExcel(title, true, SourceView);
    }
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="title"></param>
    /// <param name="header"></param>
    /// <param name="SourceView"></param>
    /// <returns></returns>
    public static Stream RenderDataViewToExcel(string title, bool headVisible, DataView SourceView)
    {

        HSSFWorkbook workbook = new HSSFWorkbook();
        MemoryStream ms = new MemoryStream();
        ISheet sheet = workbook.CreateSheet();
        sheet.DisplayGridlines = true;
        int header = 0;
        if (title.Trim().Length > 0)
        {
            IRow titleRow = sheet.CreateRow(0);
            ICell cell = titleRow.CreateCell(0);
            cell.SetCellValue(title);
            ICellStyle style = workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            IFont font = workbook.CreateFont();
            font.Boldweight = (short)FontBoldWeight.Bold; //粗體
            font.FontHeight = 20 * 20;
            style.SetFont(font);
            cell.CellStyle = style;

            //合併ICell
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 0, SourceView.Table.Columns.Count - 1));

            header++;
        }
        IRow headerRow = sheet.CreateRow(header);
        ICellStyle headerStyle = workbook.CreateCellStyle();
        headerStyle.BorderBottom = BorderStyle.Thin;
        headerStyle.BorderTop = BorderStyle.Thin;
        headerStyle.BorderLeft = BorderStyle.Thin;
        headerStyle.BorderRight = BorderStyle.Thin;

        if (headVisible)
        {
            // handling header.
            foreach (DataColumn column in SourceView.Table.Columns)
            {
                ICell headerCell = headerRow.CreateCell(column.Ordinal);
                headerCell.CellStyle = headerStyle;
                if (column.Caption.Trim().Length == 0)
                    headerCell.SetCellValue(column.ColumnName);
                else
                    headerCell.SetCellValue(column.Caption);

            }
            header++;
        }
        // handling value.
        int rowIndex = header;

        foreach (DataRowView drv in SourceView)
        {
            IRow dataRow = sheet.CreateRow(rowIndex);

            foreach (DataColumn column in SourceView.Table.Columns)
            {
                sheet.AutoSizeColumn(SourceView.Table.Columns.IndexOf(column));
                ICell dataCell = dataRow.CreateCell(column.Ordinal);
                dataCell.CellStyle = headerStyle;
                if (column.DataType == typeof(int) || column.DataType == typeof(decimal))
                {
                    if (drv[column.ColumnName].ToString().Trim().Length == 0)
                        dataCell.SetCellValue("");
                    else
                        dataCell.SetCellValue(Convert.ToDouble(drv[column.ColumnName]));
                }
                else
                    dataCell.SetCellValue(drv[column.ColumnName].ToString());
            }

            rowIndex++;
        }

        workbook.Write(ms);
        ms.Flush();
        ms.Position = 0;

        sheet = null;
        headerRow = null;
        workbook = null;

        return ms;
    }

    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="SourceView">資料</param>
    /// <param name="FileName">檔案名稱</param>
    public static void RenderDataViewToExcel(DataView SourceView, string FileName)
    {
        RenderDataViewToExcel("", SourceView, FileName);
    }
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="headvisible">表頭</param>
    /// <param name="SourceView">資料</param>
    /// <param name="FileName">檔名</param>
    public static void RenderDataViewToExcel(bool headvisible, DataView SourceView, string FileName)
    {
        RenderDataViewToExcel("", headvisible, SourceView, FileName);
    }
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="SourceView">資料</param>
    /// <param name="FileName">檔案名稱</param>
    public static void RenderDataViewToExcel(string title, DataView SourceView, string FileName)
    {
        RenderDataViewToExcel(title, true, SourceView, FileName);
    }
    /// <summary>
    /// DataView to Excel
    /// </summary>
    /// <param name="title">標題</param>
    /// <param name="headvisible">表頭</param>
    /// <param name="SourceView">資料</param>
    /// <param name="FileName">檔名</param>
    public static void RenderDataViewToExcel(string title, bool headvisible, DataView SourceView, string FileName)
    {
        MemoryStream ms = RenderDataViewToExcel(title, headvisible, SourceView) as MemoryStream;
        FileStream fs = new FileStream(FileName.IndexOf(".xls") == -1 ? string.Format("{0}.xls", FileName) : FileName, FileMode.Create, FileAccess.Write);
        byte[] data = ms.ToArray();

        fs.Write(data, 0, data.Length);
        fs.Flush();
        fs.Close();

        data = null;
        ms = null;
        fs = null;
    }
    #endregion

    #region Excel to DataTable
    /// <summary>
    /// Excel to DataTable
    /// </summary>
    /// <param name="ExcelFileStream">Excel檔案</param>
    /// <param name="SheetName">ISheet Name</param>
    /// <param name="HeaderRowIndex">表頭位置</param>
    /// <returns></returns>
    public static DataTable RenderDataTableFromExcel(Stream ExcelFileStream, string SheetName, int RowIndex)
    {
        return RenderDataTableFromExcel(ExcelFileStream, SheetName, RowIndex, true);
    }
    public static DataTable RenderDataTableFromExcel(Stream ExcelFileStream, string SheetName, int RowIndex, bool header)
    {
        HSSFWorkbook workbook = new HSSFWorkbook(ExcelFileStream);
        ISheet sheet = workbook.GetSheet(SheetName);

        DataTable table = new DataTable();

        IRow headerRow = sheet.GetRow(RowIndex);
        int cellCount = headerRow.LastCellNum;

        if (header)
        {
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue.Replace("　","").Trim());
                table.Columns.Add(column);
            }
        }
        else
        {
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(string.Format("col{0}", i + 1));
                table.Columns.Add(column);
            }
            RowIndex--;
        }
        int rowCount = sheet.LastRowNum;

        for (int i = (RowIndex + 1); i <= sheet.LastRowNum; i++)
        {
            IRow row = sheet.GetRow(i);
            DataRow dataRow = table.NewRow();

            for (int j = row.FirstCellNum; j < cellCount; j++)
            {
                if (row.GetCell(j) != null)
                {
                    if (row.GetCell(j).CellType == CellType.Numeric && NPOI.SS.UserModel.DateUtil.IsCellDateFormatted(row.GetCell(j)))
                    {
                        if (row.GetCell(j).ToString().Trim().Length == 0) continue;
                        dataRow[j] = row.GetCell(j).DateCellValue.ToString("yyyy/MM/dd");
                    }
                    else
                        dataRow[j] = row.GetCell(j).ToString();
                }
            }
        }

        ExcelFileStream.Close();
        workbook = null;
        sheet = null;
        return table;
    }

    /// <summary>
    /// Excel to DataTable
    /// </summary>
    /// <param name="ExcelFileStream">Excel 檔案</param>
    /// <param name="SheetIndex">ISheet 位置</param>
    /// <param name="HeaderRowIndex">表頭位置</param>
    /// <returns></returns>
    public static DataTable RenderDataTableFromExcel(Stream ExcelFileStream, int SheetIndex, int RowIndex)
    {
        return RenderDataTableFromExcel(ExcelFileStream, SheetIndex, RowIndex, true);
    }
    public static DataTable RenderDataTableFromExcel(Stream ExcelFileStream, int SheetIndex, int RowIndex, bool header)
    {

        HSSFWorkbook workbook = new HSSFWorkbook(ExcelFileStream);
        ISheet sheet = workbook.GetSheetAt(SheetIndex);

        DataTable table = new DataTable();

        IRow headerRow = sheet.GetRow(RowIndex);
        int cellCount = headerRow.LastCellNum;

        if (header)
        {
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue.Replace("　","").Trim());
                table.Columns.Add(column);
            }
        }
        else
        {
            for (int i = headerRow.FirstCellNum; i < cellCount; i++)
            {
                DataColumn column = new DataColumn(string.Format("col{0}", i + 1));
                table.Columns.Add(column);
            }
            RowIndex--;
        }

        int rowCount = sheet.LastRowNum;

        for (int i = (RowIndex + 1); i <= sheet.LastRowNum; i++)
        {
            IRow row = sheet.GetRow(i);
            DataRow dataRow = table.NewRow();

            for (int j = row.FirstCellNum; j < cellCount; j++)
            {
                if (row.GetCell(j) != null)
                {
                    if (row.GetCell(j).CellType == CellType.Numeric && NPOI.SS.UserModel.DateUtil.IsCellDateFormatted(row.GetCell(j)))
                    {
                        if (row.GetCell(j).ToString().Trim().Length == 0) continue;
                        dataRow[j] = row.GetCell(j).DateCellValue.ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    else
                        dataRow[j] = row.GetCell(j).ToString();
                }
            }

            table.Rows.Add(dataRow);
        }

        ExcelFileStream.Close();
        workbook = null;
        sheet = null;
        return table;
    }
    #endregion
}

