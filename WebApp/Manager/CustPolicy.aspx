﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="CustPolicy.aspx.cs" Inherits="tabc_201709.Manager.CustPolicy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc1" %>
<%@ Register src="ascx/selCustomer0.ascx" tagname="selCustomer0" tagprefix="uc2" %>
<%@ Register src="ascx/selCustomer.ascx" tagname="selCustomer" tagprefix="uc3" %>
<%@ Register Src="~/Manager/ascx/selPdt.ascx" TagPrefix="uc1" TagName="selPdt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
    <script>
        function showDetail(title, url) {
            $('#div_detail').dialog({
                modal: true,
                width: $(window).width() - 200,
                height: $(window).height() - 100,
                title: title,
                open: function (ev, ui) {
                    $('#iframe_detail').attr('src', url);
                    $('#iframe_detail').height($(window).height() - 180);
                },
                close: function (event, ui) {
                    $('#iframe_detail').attr('src', '');
                }
            });
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 保單列表</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <div id="div_detail" style="display:none;">
            <iframe id="iframe_detail" src="" width="100%" frameborder="0" height="620"></iframe>
        </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable" id="QueryMainTable" runat="server">
                                        <tr>
                                            <td align="center" class="tdQueryHead">客戶名稱/姓名：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_CustName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">投保公司：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_ComSName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="5">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="5">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">身分證號：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_ID" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶來源：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustSource" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_PolNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">性別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Sex" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">聯絡方式：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_ContactMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">生效日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc1:DateRange ID="DateRange_PO_AccureDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">投保年齡：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_Age" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">手機號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Mobile" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單備註：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_Polmmemo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">婚姻：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Marry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">地址：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Address" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load" DataSourceID="ObjectDataSource1">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataCustPolicy"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">保單號碼</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_PolNo" runat="server" MaxLength="255"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">投保公司</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_ComSName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">要保人</td>
                                            <td class="tdData10s" width="16%">
                                                <uc3:selCustomer ID="selCustomer1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">投保日期</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_AccureDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_sDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_PO_AccureDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">投保年齡</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_InsureAge" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">保單狀態</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_PolState" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">繳費日期</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_PaidToDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_PO_PaidToDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_PO_PaidToDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">繳別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_Mode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">繳法</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_PaidMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">應繳保費</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_ModePrem" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">幣別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_PolmCurrency" runat="server">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>台幣</asp:ListItem>
                                                    <asp:ListItem>人民</asp:ListItem>
                                                    <asp:ListItem>日圓</asp:ListItem>
                                                    <asp:ListItem>美元</asp:ListItem>
                                                    <asp:ListItem>紐幣</asp:ListItem>
                                                    <asp:ListItem>港幣</asp:ListItem>
                                                    <asp:ListItem>歐元</asp:ListItem>
                                                    <asp:ListItem>澳幣</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">&nbsp;</td>
                                            <td class="tdData10s" width="16%">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">主約保額</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_PolicyAmt" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">主約保費</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_PolicyPrem" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">紅利選擇</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_PO_Dividend" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">&nbsp;</td>
                                            <td class="tdData10s" width="16%">&nbsp;</td>
                                            <td class="MS_tdTitle" width="16%">招攬業務</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_Predecessor" runat="server" MaxLength="80"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">維護業務</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_PO_POLBELONGAGENT" runat="server" MaxLength="80"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">收費地址</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_PO_PolZip" runat="server" Width="30px" style="text-align:center"></asp:TextBox>
                                                &nbsp;<asp:TextBox ID="txt_PO_PolAddr" runat="server" MaxLength="80" Width="300px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">保單備註</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_PO_PolMemo" runat="server" MaxLength="4000" Rows="5" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="6">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_PO_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_CU_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_CU_UidNo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="background-color: #FFCC99; font-size: 18px; padding-top: 10px; padding-bottom: 10px; color: #333333;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="33.3%">&nbsp;</td>
                                                        <td align="center" width="33.3%">投保商品</td>
                                                        <td align="right" width="33.3%">
                                                            <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="1" CssClass="MainHeadTdLink" ForeColor="#0066CC" OnCommand="lnk_CommandAdd"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增投保商品</asp:LinkButton>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!" OnLoad="GridView2_Load" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" OnRowDataBound="GridView2_RowDataBound">
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                <div id="div_PD_edit" style="display: none;">
                                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">約別</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_PD_Kind" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">商品名稱</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_PD_PdtName" runat="server" ReadOnly="True" Width="300px" Visible="False"></asp:TextBox>
                                                                <uc1:selPdt runat="server" ID="selPdt" OnSelectedIndexChanged="selPdt_SelectedIndexChanged" OnSearchButtonClick="selPdt_SearchButtonClick" OnRefreshButtonClick="selPdt_RefreshButtonClick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">關係</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_PD_Relation" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">被保險人</td>
                                                            <td class="tdData10s">
                                                                <uc2:selCustomer0 ID="selCustomer01" runat="server" OnSelectedIndexChanged="selCustomer01_SelectedIndexChanged" OnSearchButtonClick="selCustomer01_SearchButtonClick" OnRefreshButtonClick="selCustomer01_RefreshButtonClick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">投保日期</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_PD_AccureDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                                <asp:CalendarExtender ID="txt_PD_AccureDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_PD_AccureDate" TodaysDateFormat="yyyy年M月d日">
                                                                </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">幣別</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_PD_PolmCurrency" runat="server" Enabled="False">
                                                                    <asp:ListItem></asp:ListItem>
                                                                    <asp:ListItem>台幣</asp:ListItem>
                                                                    <asp:ListItem>人民</asp:ListItem>
                                                                    <asp:ListItem>日圓</asp:ListItem>
                                                                    <asp:ListItem>美元</asp:ListItem>
                                                                    <asp:ListItem>紐幣</asp:ListItem>
                                                                    <asp:ListItem>港幣</asp:ListItem>
                                                                    <asp:ListItem>歐元</asp:ListItem>
                                                                    <asp:ListItem>澳幣</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_CM_ZIP0" runat="server">
                                                            <td align="right" class="MS_tdTitle" width="100">年期</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_PD_PdtYear" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_CM_ZIP1" runat="server">
                                                            <td align="right" class="MS_tdTitle" width="100">台幣保額</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_PD_Amtinput" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">應繳保費</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_PD_ModePrem" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">險種狀態</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_PD_PdtCodeState" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td align="center" class="tdMaintainButton" colspan="2">
                                                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command_PD" Text="存檔" />
                                                                    <asp:Button ID="btnEdit0" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command_PD" Text="存檔" />
                                                                    <asp:Button ID="btnDelete0" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command_PD" Text="刪除" />
                                                                    <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command_PD" Text="取消" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2" style="height: 20px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" style="line-height: 125%">
                                                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="hif_PD_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_PD_UidNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_TransNo1" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_UidNo1" runat="server" />
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="background-color: #FFCC99; font-size: 18px; padding-top: 10px; padding-bottom: 10px; color: #333333;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="33.3%">&nbsp;</td>
                                                        <td align="center" width="33.3%">保單受益人</td>
                                                        <td align="right" width="33.3%">
                                                            <asp:LinkButton ID="lnkMaintain1" runat="server" CommandName="2" CssClass="MainHeadTdLink" ForeColor="#0066CC" OnCommand="lnk_CommandAdd"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增受益人</asp:LinkButton>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!" OnLoad="GridView3_Load" OnSelectedIndexChanged="GridView3_SelectedIndexChanged" OnRowDataBound="GridView3_RowDataBound">
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                <div id="div_BE_edit" style="display: none;">
                                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">種類</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_BE_Kind" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">受益人</td>
                                                            <td class="tdData10s">
                                                                <uc3:selCustomer ID="selCustomer3" runat="server" OnSelectedIndexChanged="selCustomer3_SelectedIndexChanged" OnSearchButtonClick="selCustomer3_SearchButtonClick" OnRefreshButtonClick="selCustomer3_RefreshButtonClick" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">關係</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_BE_Relation" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">順位</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_BE_BenefSuccession" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">比例</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_BE_BenefPercent" runat="server" Width="50px"></asp:TextBox>
                                                                %</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">身分證號</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_BE_BenefID" runat="server" MaxLength="255"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">年齡</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_BE_Age" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td align="center" class="tdMaintainButton" colspan="2">
                                                                    <asp:Button ID="btnAppend1" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command_BE" Text="存檔" />
                                                                    <asp:Button ID="btnEdit1" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command_BE" Text="存檔" />
                                                                    <asp:Button ID="btnDelete1" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command_BE" Text="刪除" />
                                                                    <asp:Button ID="btnCancel1" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command_BE" Text="取消" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2" style="height: 20px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" style="line-height: 125%">
                                                                                <asp:Label ID="lbl_Msg1" runat="server" CssClass="errmsg12"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="hif_BE_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_BE_UidNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_TransNo0" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_UidNo0" runat="server" />
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    

</asp:Content>
