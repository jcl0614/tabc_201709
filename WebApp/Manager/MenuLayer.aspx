﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="MenuLayer.aspx.cs" Inherits="tabc_201709.Manager.MenuLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkSortSet" runat="server" CommandName="1" CssClass="MainHeadTdLink" oncommand="lnk_Command"><img src="<%=ResolveUrl("images/sort.png") %>" border="0" style="vertical-align:bottom" /> 順序設定</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0" CssClass="MainHeadTdLink" oncommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkSortSet" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <asp:Panel ID="Panel_Form" runat="server">
                        <table border="0" cellpadding="2" cellspacing="1">
                            <tr>
                                <td align="center">
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="24">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" EnableModelValidation="True" onrowcreated="GridView1_RowCreated" onrowupdating="GridView1_RowUpdating">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" height="24">
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" onupdating="ObjectDataSource1_Updating" SelectCountMethod="GetCount" SelectMethod="DataMenuLayerReader" SortParameterName="sortExpression" TypeName="DataMenuLayer" UpdateMethod="MenuLayerUpdate"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel_Sort" runat="server">
                    <ContentTemplate>
                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">目前顯示順序</td>
                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">新設顯示順序</td>
                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333" valign="top"></td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="250px"></asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <p>
                                            <asp:ImageButton ID="ibtnRefresh" runat="server" CommandName="refresh" ImageUrl="~/Manager/images/Refresh-icon.png" oncommand="btnSort_Command" />
                                            <br />
                                            <br />
                                            <asp:Button ID="btnAddAll" runat="server" CommandName="addAll" CssClass="btnAddAll" oncommand="btnSort_Command" Text="全部加入" />
                                            <br />
                                            <br />
                                            <asp:Button ID="btnRemoveAll" runat="server" CommandName="removeAll" CssClass="btnRemoveAll" oncommand="btnSort_Command" Text="全部移除" />
                                            <br />
                                            <br />
                                            <asp:ImageButton ID="ibtnRight" runat="server" CommandName="add" ImageUrl="~/Manager/images/Forward-icon.png" oncommand="btnSort_Command" />
                                            <br />
                                            <br />
                                            <asp:ImageButton ID="ibtnLeft" runat="server" CommandName="remove" ImageUrl="~/Manager/images/Back-icon.png" oncommand="btnSort_Command" />
                                        </p>
                                    </td>
                                    <td align="center">
                                        <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="250px"></asp:ListBox>
                                    </td>
                                    <td align="center">
                                        <asp:Button ID="btnSaveSort" runat="server" CommandName="save" CssClass="btnExcute" oncommand="btnSort_Command" Text="執行" />
                                        <br />
                                        <br />
                                        <asp:Button ID="btnCancelSort" runat="server" CommandName="cancel" CssClass="btnCancel" oncommand="btnSort_Command" Text="取消" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px">
                                        <asp:Label ID="labSortMsg" runat="server" CssClass="errmsg12"></asp:Label>
                                    </td>
                                    <td align="center"></td>
                                    <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px">
                                        <asp:ImageButton ID="ibtnUp" runat="server" CommandName="up" ImageUrl="~/Manager/images/Up-icon.png" oncommand="btnSort_Command" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="ibtnDown" runat="server" CommandName="down" ImageUrl="~/Manager/images/Down-icon.png" oncommand="btnSort_Command" />
                                    </td>
                                    <td align="center"></td>
                                </tr>
                            </table>
                        </ContentTemplate>
                </asp:UpdatePanel>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

