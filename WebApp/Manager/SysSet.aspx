﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="SysSet.aspx.cs" Inherits="tabc_201709.Manager.SysSet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <table align="center" border="0" cellpadding="2" cellspacing="1" 
                    width="100%">
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            系統限制IP</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebMapIP" runat="server" MaxLength="500" Width="800px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">多筆以「 ; 」分隔</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #993333" width="180">
                            發信伺服器</td>
                        <td class="tdData10s">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left">
                                        <asp:TextBox ID="txt_WebMailServer" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                                        <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                        <font style="color: #D663A5">最多輸入50字元</font></td>
                                    <td align="left" width="10">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <asp:Button ID="btn_SendMail" runat="server" onclick="btn_SendMail_Click" 
                                                                onclientclick="return confirm(&quot;確定要發出測試嗎?\n注意：紅色欄位必須先儲存更新!&quot;);" 
                                                                Text="發信測試" />
                                                        </td>
                                                        <td width="10">
                                                            &nbsp;</td>
                                                        <td width="20">
                                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                                AssociatedUpdatePanelID="UpdatePanel2">
                                                                <ProgressTemplate>
                                                                    <img alt="" src="images/loading.gif" width="20" />
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #993333" width="180">
                            發信郵件</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebSendMail" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">電子信箱格式</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #993333" width="180">
                            發信郵件帳號</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebSendMailID" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #993333" width="180">
                            發信郵件密碼</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebSendMailPWD" runat="server" MaxLength="50" 
                                Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            收信郵件</td>
                        <td class="tdData10s" style="height: 26px">
                            <asp:TextBox ID="txt_WebAcceptMail" runat="server" Width="800px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">電子信箱格式；多筆以「 ; 」做區隔</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #660066;" width="180">
                            系統-FTP帳號</td>
                        <td class="tdData10s" style="height: 26px">
                            <asp:TextBox ID="txt_SysFtpID" runat="server" BackColor="#CCCCFF"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #660066;" width="180">
                            系統-FTP密碼</td>
                        <td class="tdData10s" style="height: 26px">
                            <asp:TextBox ID="txt_SysFtpPWD" runat="server" BackColor="#CCCCFF"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #660066;" width="180">
                            系統-FTP位址</td>
                        <td class="tdData10s" style="height: 26px">
                            <asp:TextBox ID="txt_SysFtpHost" runat="server" BackColor="#CCCCFF"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #3366CC" width="180">
                            Google 帳號</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_gMail" runat="server" BackColor="#CAE4F4" Width="400px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">電子信箱格式</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #3366CC" width="180">
                            Google 密碼</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_gPwd" runat="server" BackColor="#CAE4F4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #3366CC" width="180">
                            GoogleAnalytics ID</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_gWebId" runat="server" BackColor="#CAE4F4"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #3366CC" width="180">Google reCAPTCHA </td>
                        <td class="tdData10s">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>Site key：</td>
                                    <td>
                                        <asp:TextBox ID="txt_gCAPTCHA_SiteKey" runat="server" BackColor="#CAE4F4"></asp:TextBox>
                                    </td>
                                    <td width="10">&nbsp;</td>
                                    <td>Secret key：</td>
                                    <td>
                                        <asp:TextBox ID="txt_gCAPTCHA_Secretkey" runat="server" BackColor="#CAE4F4"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #333333" width="180">
                            簡訊帳號</td>
                        <td class="tdData10s">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txt_smsID" runat="server" BackColor="#F6F6F6"></asp:TextBox>
                                                &nbsp;<asp:Label ID="lbl_smsPoint" runat="server" Font-Names="微軟正黑體" 
                                                    ForeColor="#333333"></asp:Label>
                                            </td>
                                            <td width="20">
                                                &nbsp;</td>
                                            <td style="color: #333333">
                                                手機：</td>
                                            <td>
                                                <asp:TextBox ID="txt_smsTest" runat="server" Font-Names="微軟正黑體" MaxLength="10" Width="100px" BackColor="#EBEBEB"></asp:TextBox>
                                            </td>
                                            <td width="3">
                                                &nbsp;</td>
                                            <td>
                                                <asp:Button ID="btn_sendSMS" runat="server" onclick="btn_sendSMS_Click" 
                                                    onclientclick="return confirm(&quot;寄發測試簡訊成功將扣除點數1點!\n確定要寄發嗎?&quot;);" 
                                                    Text="發送測試" />
                                            </td>
                                            <td width="10">
                                                &nbsp;</td>
                                            <td>
                                                <asp:UpdateProgress ID="UpdateProgress2" runat="server" 
                                                    AssociatedUpdatePanelID="UpdatePanel3">
                                                    <ProgressTemplate>
                                                        <img alt="" src="images/loading.gif" width="20" />
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" style="color: #333333" width="180">
                            簡訊密碼</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_smsPWD" runat="server" BackColor="#F6F6F6"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">簡訊王：<a href="http://www.kotsms.com.tw/index.php" 
                                target="_blank">http://www.kotsms.com.tw/index.php</a></font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            系統登出時間</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_sysLogout" runat="server"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">以「秒」為單位；不得小於0</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            網站登出時間</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_webLogout" runat="server"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">以「秒」為單位；不得小於0</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            網站啟用</td>
                        <td class="tdData10s">
                            <asp:CheckBox ID="cbox_WebEnable" runat="server" />
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">若非啟用則隱藏網站內容</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            網站複製／右鍵啟用</td>
                        <td class="tdData10s">
                            <asp:CheckBox ID="cbox_WebCopyEnable" runat="server" />
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">若非啟用則無法複製網站及使用右鍵</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            SSL憑證啟用</td>
                        <td class="tdData10s">
                            <asp:CheckBox ID="cbox_SSL" runat="server" />
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">憑證必須在有效期內才能啟用：啟用後連結路徑必須是「https://」或「相對路徑」</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">系統視窗全屏模式</td>
                        <td class="tdData10s">
                            <asp:CheckBox ID="cbox_WindowFull" runat="server" />
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">若變更模式將關閉目前視窗；重新登入後生效</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">系統登入驗證碼</td>
                        <td class="tdData10s">
                            <asp:DropDownList ID="ddl_Recaptcha" runat="server">
                                <asp:ListItem Value="0">不啟用</asp:ListItem>
                                <asp:ListItem Value="1">一般圖文驗證</asp:ListItem>
                                <asp:ListItem Value="2">Google Recaptcha</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">
                            系統提示文字</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebMessage" runat="server" Width="800px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">於網站後台顯示之重要訊息</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="180">登入帳號同步</td>
                        <td class="tdData10s">
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btn_update1" runat="server" OnClick="btn_update1_Click" OnClientClick="return confirm(&quot;確定要執行嗎?&quot;);" Text="同步 tblPerson" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btn_update2" runat="server" OnClick="btn_update2_Click" OnClientClick="return confirm(&quot;確定要執行嗎?&quot;);" Text="同步 tabcSales" />
                                    &nbsp;
                                    <asp:UpdateProgress ID="UpdateProgress3" runat="server" AssociatedUpdatePanelID="UpdatePanel4">
                                        <ProgressTemplate>
                                            <img alt="" src="images/loading.gif" width="20" />
                                            處理中...
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="tdMaintainButton" colspan="2">
                            <asp:Button ID="btnUppend" runat="server" CssClass="btnSave" 
                                onclick="btnUppend_Click" Text="更新資料" OnClientClick="return confirm(&quot;確定要更新嗎?&quot;);" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="line-height: 125%">
                                        <asp:Label ID="labMsg" runat="server" CssClass="errmsg12"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
