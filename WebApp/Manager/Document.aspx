﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="Document.aspx.cs" Inherits="tabc_201709.Manager.Document" %>

<%@ Register Src="~/Manager/ascx/UpdateProgress.ascx" TagPrefix="uc1" TagName="UpdateProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="1100" oncontextmenu="return false" onselectstart="return false" oncopy="return false">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                            <tr>
                                <td align="center" class="tdQueryHead">類別：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:DropDownList ID="ddlSh_category" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_category_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="ddlSh_category_sub" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_category_sub_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td align="center" class="tdQueryHead">檔案名稱：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:TextBox ID="txtSh_Name" runat="server"></asp:TextBox>
                                </td>
                                <td align="center" width="85">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                </td>
                                <td align="center" width="35">
                                    <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:UpdateProgress runat="server" ID="UpdateProgress" />
                        <asp:Panel ID="Panel1" runat="server" Height="600px" ScrollBars="Auto">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="檔案名稱">
                                        <ItemTemplate>
                                            <%# Eval("Name") %><%# (bool)Eval("isNew") ? "&nbsp;<img src='/images/Icon_New.gif' />" : "" %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="800px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="格式">
                                        <ItemTemplate>
                                            <%# Eval("Extension") %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="70px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="大小">
                                        <ItemTemplate>
                                            <%# Eval("Length") %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="70px" />
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="修改日期">
                                        <ItemTemplate>
                                            <%# Eval("LastWriteDate") %>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="下載">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName='<%# Eval("FullName") %>' ImageUrl="~/Manager/images/download-icon.png" OnClick="ImageButton1_Click" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="60px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>
    </asp:Content>
