﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="MyEAttendTot.aspx.cs" Inherits="tabc_201709.Manager.MyEAttendTot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 設定出席人員</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelSet" runat="server" CssClass="MainHeadTdLink" OnClick="lnkExcelSet_Click"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 匯出</asp:LinkButton>
<%--            <a class="MainHeadTdLink"  onclick="printScreen(print_all,'');"><img src="<%=ResolveUrl("images/Action-file-print-icon.png") %>" border="0" style="vertical-align:bottom" /> 列印</a>--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:PostBackTrigger ControlID="lnkExcelSet" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">年度：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EO_Year" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">月份：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EO_Month" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">單位：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EO_Unit" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" BackColor="#7DC44E" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnStyle" OnClick="btnRestQuery_Click" BackColor="#7B7BC0" Text="條件清除" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">處經理：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_DM" runat="server" Width="100px" Enabled="False"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">出席人員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EO_AccID" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_my" SelectMethod="DataReader_my"
                                        TypeName="DataEAttendTot"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">年度</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:DropDownList ID="ddl_EO_year" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">月份</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:DropDownList ID="ddl_EO_Month" runat="server">
                                                    <asp:ListItem Value="1">1 月</asp:ListItem>
                                                    <asp:ListItem Value="2">2 月</asp:ListItem>
                                                    <asp:ListItem Value="3">3 月</asp:ListItem>
                                                    <asp:ListItem Value="4">4 月</asp:ListItem>
                                                    <asp:ListItem Value="5">5 月</asp:ListItem>
                                                    <asp:ListItem Value="6">6 月</asp:ListItem>
                                                    <asp:ListItem Value="7">7 月</asp:ListItem>
                                                    <asp:ListItem Value="8">8 月</asp:ListItem>
                                                    <asp:ListItem Value="9">9 月</asp:ListItem>
                                                    <asp:ListItem Value="10">10 月</asp:ListItem>
                                                    <asp:ListItem Value="11">11 月</asp:ListItem>
                                                    <asp:ListItem Value="12">12 月</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:DropDownList ID="ddl_EO_Unit" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">人員</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:DropDownList ID="ddl_EO_AccID" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <uc3:selPerson_AccID ID="selPerson_AccID_EO_DM" runat="server" OnRefreshButtonClick="selPerson_AccID_EO_DM_RefreshButtonClick" OnSearchButtonClick="selPerson_AccID_EO_DM_SearchButtonClick" OnSelectedIndexChanged="selPerson_AccID_EO_DM_SelectedIndexChanged" OnTextChanged="selPerson_AccID_EO_DM_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">應出席次數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:TextBox ID="txt_EO_ShouldAtt" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="padding: 10px; background-color: #FFBD6E">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnStyle" OnCommand="btn_Command" Text="確認" BackColor="#7DC44E" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnStyle" OnCommand="btn_Command" Text="確認" BackColor="#7DC44E" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnStyle" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" BackColor="#EB8484" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="cancel" CssClass="btnStyle" OnCommand="btnApply_Command" Text="回查詢清單" BackColor="#7B7BC0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EO_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EO_AccID" runat="server" />
                                                <asp:HiddenField ID="hif_EO_DM" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <asp:UpdatePanel ID="UpdatePanel_Sort" runat="server">
                                        <ContentTemplate>
                        <table border="0" cellpadding="2" cellspacing="1" width="100%" style="background-color: #F8F8C8">
                            <tr>
                                <td align="center" colspan="2" style="background-color: #FFBD6E; padding-top: 20px; padding-bottom: 20px; font-weight: bold; color: #6F3512; font-size: 18px;">設定出席人員</td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">年度</td>
                                <td class="tdData10s" style="background-color: #F8F8C8">
                                    <asp:DropDownList ID="ddlSh_year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_year_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">月份</td>
                                <td class="tdData10s" style="background-color: #F8F8C8">
                                    <asp:DropDownList ID="ddlSh_Month" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_Month_SelectedIndexChanged">
                                        <asp:ListItem Value="1">1 月</asp:ListItem>
                                        <asp:ListItem Value="2">2 月</asp:ListItem>
                                        <asp:ListItem Value="3">3 月</asp:ListItem>
                                        <asp:ListItem Value="4">4 月</asp:ListItem>
                                        <asp:ListItem Value="5">5 月</asp:ListItem>
                                        <asp:ListItem Value="6">6 月</asp:ListItem>
                                        <asp:ListItem Value="7">7 月</asp:ListItem>
                                        <asp:ListItem Value="8">8 月</asp:ListItem>
                                        <asp:ListItem Value="9">9 月</asp:ListItem>
                                        <asp:ListItem Value="10">10 月</asp:ListItem>
                                        <asp:ListItem Value="11">11 月</asp:ListItem>
                                        <asp:ListItem Value="12">12 月</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">單位</td>
                                <td class="tdData10s" style="background-color: #F8F8C8">
                                    <asp:DropDownList ID="ddl_EO_Unit0" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">處經理</td>
                                <td class="tdData10s" style="background-color: #F8F8C8">
                                    <uc3:selPerson_AccID ID="selPerson_AccID_EO_DM0" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">應出席次數</td>
                                <td class="tdData10s" style="background-color: #F8F8C8">
                                    <asp:TextBox ID="txt_EO_ShouldAtt0" runat="server" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">
                                                        早會日期</td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">應出席人員</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                                    </td>
                                                    <td align="center">
                                                        &nbsp;</td>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                    <td align="center"></td>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                </tr>
                                            </table>
                                        
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="background-color: #FFBD6E">
                                    
                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Append" CssClass="btnStyle" OnClientClick="return confirm(&quot;確定要設定新增出席人員資料嗎?&quot;);" OnCommand="btnApply_Command" Text="產生早會資料" BackColor="#7DC44E" />
                                    <asp:Button ID="btnCancel0" runat="server" CommandName="cancel" CssClass="btnStyle" OnCommand="btnApply_Command" Text="回查詢清單" BackColor="#7B7BC0" />
                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel_Sort" DisplayAfter="1000">
                                        <ProgressTemplate>
                                            <table cellpadding="０" cellspacing="０">
                                                <tr>
                                                    <td>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Manager/images/loading.gif" />
                                                    </td>
                                                    <td width="5">&nbsp;</td>
                                                    <td>
                                                        資料處理中．．．</td>
                                                </tr>
                                            </table>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="height: 20px">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" style="line-height: 125%">
                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tr>
                        </table>
                        </ContentTemplate>
                                    </asp:UpdatePanel>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="center" style="background-color: #FFBD6E; padding-top: 20px; padding-bottom: 20px; font-weight: bold; color: #6F3512; font-size: 18px;">人員名單</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding: 10px; background-color: #F8F8C8;">
                                    <asp:Label ID="lbl_mTitle" runat="server" Font-Bold="True" ForeColor="#0066CC"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    
                                    <asp:Panel ID="Panel1" runat="server">
                                        <div id="print_all">
                                        <asp:GridView ID="GridView2" runat="server" Font-Size="12px" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2" ForeColor="Black" GridLines="None">
                                            <AlternatingRowStyle BackColor="PaleGoldenrod" />
                                            <FooterStyle BackColor="Tan" />
                                            <HeaderStyle BackColor="Tan" Font-Bold="True" HorizontalAlign="Center" />
                                            <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                            <RowStyle HorizontalAlign="Center" />
                                            <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                            <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                            <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                            <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                            <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                        </asp:GridView>
                                            </div>
                                    </asp:Panel>
                                        
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <a class="btnStyle" onclick="printScreen(print_all,'');" style="background: #EB8484;">列印名單</a> &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back10" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>

</asp:Content>
