﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ETeacher.aspx.cs" Inherits="tabc_201709.Manager.ETeacher" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">講師類型：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_ET_TeacherType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">公司名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Company" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">講師代碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_TCode" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">授課講師：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_TName" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">聯絡電話：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Phone" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">專長領域：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Specialty" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">講師簡介：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_TSummary" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataETeacher"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">講師類型</td>
                                            <td class="tdData10s" colspan="3">
                                                <asp:DropDownList ID="ddl_ET_TeacherType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">公司名稱</td>
                                            <td class="tdData10s" width="300">
                                                <asp:TextBox ID="txt_ET_Company" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">部門名稱</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_ET_Department" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">職稱</td>
                                            <td class="tdData10s" width="300">
                                                <asp:TextBox ID="txt_ET_JobTitle" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">講師代碼</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_ET_TCode" runat="server" Width="50px" MaxLength="100"></asp:TextBox>
                                                <uc4:selPerson_AccID ID="selPerson_AccID_ET_TCode" runat="server" OnSelectedIndexChanged="selPerson_AccID_ET_TCode_SelectedIndexChanged" OnTextChanged="selPerson_AccID_ET_TCode_TextChanged" OnSearchButtonClick="selPerson_AccID_ET_TCode_SearchButtonClick" OnRefreshButtonClick="selPerson_AccID_ET_TCode_RefreshButtonClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">授課講師</td>
                                            <td class="tdData10s" width="300">
                                                <asp:TextBox ID="txt_ET_TName" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">身份證號</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_ET_ID" runat="server" Width="200px" MaxLength="16"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">地址</td>
                                            <td class="tdData10s" width="300">
                                                <asp:TextBox ID="txt_ET_Addr" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">聯絡電話</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_ET_Phone" runat="server" Width="200px" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">專長領域</td>
                                            <td class="tdData10s" width="300">
                                                <asp:TextBox ID="txt_ET_Specialty" runat="server" Rows="5" TextMode="MultiLine" Width="300px" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">時薪</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_ET_Salary" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">講師簡介</td>
                                            <td class="tdData10s" colspan="3">
                                                <asp:TextBox ID="txt_ET_TSummary" runat="server" Rows="5" TextMode="MultiLine" Width="98%" MaxLength="500"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="4">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnAppend" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnSave" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy"
                                                    OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="複製" CssClass="btnCopy" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete"
                                                    OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="刪除" CssClass="btnDelete" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel"
                                                    OnCommand="btn_Command" Text="取消" CssClass="btnCancel" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_ET_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_ET_Code" runat="server" />
                                            </td>
                                        </tr>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
