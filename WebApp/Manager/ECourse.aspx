﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ECourse.aspx.cs" Inherits="tabc_201709.Manager.ECourse" %>

<%@ Register src="ascx/selTeacher.ascx" tagname="selTeacher" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">課程名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_CName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">學院：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Type" runat="server"></asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">分級：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Rating" runat="server"></asp:DropDownList>
                                            </td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True" OnLoad="GridView1_Load">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataECourse"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">課程代碼</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_Code" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">分級</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:DropDownList ID="ddl_EC_Rating" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">學院</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:DropDownList ID="ddl_EC_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">訓練通報</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:DropDownList ID="ddl_EC_TrainType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">課程名稱</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_CName" runat="server" Width="98%" MaxLength="100"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">收費方式</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:DropDownList ID="ddl_EC_FeeWay" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">授課講師</td>
                                            <td class="tdData10s" width="35%">
                                                <uc1:selTeacher ID="selTeacher_ET_TName" runat="server" OnSelectedIndexChanged="selTeacher_ET_TName_SelectedIndexChanged" OnTextChanged="selTeacher_ET_TName_TextChanged" OnSearchButtonClick="selTeacher_ET_TName_SearchButtonClick" OnRefreshButtonClick="selTeacher_ET_TName_RefreshButtonClick" />
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">收費金額</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_FeeAmt" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">講師簡介</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_ET_TSummary" runat="server" Rows="5" TextMode="MultiLine" Width="98%" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">收費點數</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_FeePoints" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">課後測驗</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:RadioButtonList ID="rbl_EC_IsExam" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="Y">是</asp:ListItem>
                                                    <asp:ListItem Value="N" Selected="True">否</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">測驗題數</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_QuizNo" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">強制訓練</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:RadioButtonList ID="rbl_EC_IsForce" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="Y">是</asp:ListItem>
                                                    <asp:ListItem Value="N" Selected="True">否</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">成績限制</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_PassScore" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">應訓練年度</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:DropDownList ID="ddl_EC_Annual" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">測驗時間限制</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_ExamTime" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;分鐘</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">課程簡介</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_Summary" runat="server" Rows="5" TextMode="MultiLine" Width="98%" MaxLength="300"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">通報時數</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_PassHours" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">備註</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_Memo" runat="server" Rows="5" TextMode="MultiLine" Width="98%" MaxLength="1000"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">計算</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:CheckBox ID="cbox_EC_IsAtt" runat="server" Text="出席率" />
                                                <br />
                                                <asp:CheckBox ID="cbox_EC_IsHours" runat="server" Text="訓練時數" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="15%">測驗注意事項</td>
                                            <td class="tdData10s" width="35%">
                                                <asp:TextBox ID="txt_EC_QuizNotify" runat="server" MaxLength="1000" Rows="5" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="15%">&nbsp;</td>
                                            <td class="tdData10s" width="35%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="4">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnAppend" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnSave" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy"
                                                    OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="複製" CssClass="btnCopy" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete"
                                                    OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="刪除" CssClass="btnDelete" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel"
                                                    OnCommand="btn_Command" Text="取消" CssClass="btnCancel" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_Code" runat="server" />
                                                <asp:HiddenField ID="hif_tblETeacher_ET_TName" runat="server" />
                                                <asp:HiddenField ID="hif_tblETeacher_ET_TransNo" runat="server" />
                                            </td>
                                        </tr>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
