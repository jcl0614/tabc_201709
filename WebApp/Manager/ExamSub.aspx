﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ExamSub.aspx.cs" Inherits="tabc_201709.Manager.ExamSub" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">測驗大分類：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EB_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">測驗次分類：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EB_SubType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_EB_SubType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">說明：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EB_Explan" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">科目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EB_Subject" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataExamSub"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">測驗大分類</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EB_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">測驗次分類</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EB_SubType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_EB_SubType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">科目</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EB_Subject" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">說明</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EB_Explan" runat="server" MaxLength="100" Width="400px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">報名費</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EB_Regist" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">考試時間</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EB_TimeLength" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;分鐘</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">複查費</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EB_Reviewfee" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">警示訊息</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EB_Warnings" runat="server" MaxLength="100" Rows="5" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">題庫檔案</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EF1" runat="server" MaxLength="100" ReadOnly="True" Width="300px"></asp:TextBox>
                                                &nbsp;<asp:Literal ID="Literal_EF1" runat="server"></asp:Literal>
                                                <br />
                                                <asp:FileUpload ID="FileUpload_EF1" runat="server" />
                                                <asp:Label ID="lbl_Msg_EF1" runat="server" CssClass="errmsg12"></asp:Label>
                                                &nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">檔案格式：.pdf .doc .xls .ppt</span> </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">題庫檔案大小</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EFSize1" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                &nbsp;MB&nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">可整數或小數2位</span>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">證照說明檔案</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EF2" runat="server" MaxLength="100" ReadOnly="True" Width="300px"></asp:TextBox>
                                                &nbsp;<asp:Literal ID="Literal_EF2" runat="server"></asp:Literal>
                                                <br />
                                                <asp:FileUpload ID="FileUpload_EF2" runat="server" />
                                                <asp:Label ID="lbl_Msg_EF2" runat="server" CssClass="errmsg12"></asp:Label>
                                                &nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">檔案格式：.pdf .doc .xls .ppt</span> </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">證照說明檔案大小</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EFSize2" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                &nbsp;MB&nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">可整數或小數2位</span>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EB_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EF_TransNo1" runat="server" />
                                                <asp:HiddenField ID="hif_EF_UidNo1" runat="server" />
                                                <asp:HiddenField ID="hif_EF_TransNo2" runat="server" />
                                                <asp:HiddenField ID="hif_EF_UidNo2" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
                <asp:PostBackTrigger ControlID="btnAppend" />
                <asp:PostBackTrigger ControlID="btnEdit" />
                <asp:PostBackTrigger ControlID="GridView1" />
            </Triggers>
    </asp:UpdatePanel>


</asp:Content>
