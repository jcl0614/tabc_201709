﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ckeditor.ascx.cs" Inherits="tabc_201709.Manager.ascx.ckeditor" %>
<script>
    if (!window.jQuery) {
        document.write('<script src="<%: ResolveUrl("~/jQuery/jquery-2.0.3.min.js") %>"><\/script>');
    }
</script>
<script src="<%: ResolveUrl("~/manager/js/ckeditor/ckeditor.js") %>" type="text/javascript"></script>
<asp:TextBox ID="txt_ckeditor" runat="server" TextMode="MultiLine" ClientIDMode="Static" Height="490" Width="100%"></asp:TextBox>
<script>
    CKEDITOR.replace('<%= txt_ckeditor.ClientID %>', {
    //filebrowserBrowseUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/ckfinder.aspx") %>",
    filebrowserImageBrowseUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/ckfinder.aspx?type=Images") %>",
    //filebrowserFlashBrowseUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/ckfinder.aspx?type=Flash") %>",
    //filebrowserUploadUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/core/connector/aspx/connector_.aspx?command=QuickUpload&type=Files") %>",
    filebrowserImageUploadUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/core/connector/aspx/connector_.aspx?command=QuickUpload&type=Images") %>",
    //filebrowserFlashUploadUrl: "<%: ResolveUrl("~/manager/js/ckeditor/plugins/ckfinder/core/connector/aspx/connector_.aspx?command=QuickUpload&type=Flash") %>",
    width: '<%= txt_ckeditor.Width %>',
    height: '<%= txt_ckeditor.Height %>',
    extraPlugins: 'colorbutton,colordialog,a11ychecker',
    colorButton_enableMore: true,
    colorButton_enableAutomatic: true,
    font_names:
    '微軟正黑體;新細明體;標楷體;' +
    'Arial/Arial, Helvetica, sans-serif;' +
    'Times New Roman/Times New Roman, Times, serif;' +
    'Verdana',
    disallowedContent: 'script;*[on*]',
    allowedContent: {
        'section': { styles: '*', attributes: '*', classes: '*' }, 'nav': { styles: '*', attributes: '*', classes: '*' }, 'article': { styles: '*', attributes: '*', classes: '*' }, 'aside': { styles: '*', attributes: '*', classes: '*' }, 'h1': { styles: '*', attributes: '*', classes: '*' }, 'h2': { styles: '*', attributes: '*', classes: '*' }, 'h3': { styles: '*', attributes: '*', classes: '*' }, 'h4': { styles: '*', attributes: '*', classes: '*' }, 'h5': { styles: '*', attributes: '*', classes: '*' }, 'h6': { styles: '*', attributes: '*', classes: '*' }, 'hgroup': { styles: '*', attributes: '*', classes: '*' }, 'header': { styles: '*', attributes: '*', classes: '*' }, 'footer': { styles: '*', attributes: '*', classes: '*' }, 'address': { styles: '*', attributes: '*', classes: '*' },
        'p': { styles: '*', attributes: '*', classes: '*' }, 'hr': { styles: '*', attributes: '*', classes: '*' }, 'br': { styles: '*', attributes: '*', classes: '*' }, 'pre': { styles: '*', attributes: '*', classes: '*' }, 'blockquote': { styles: '*', attributes: '*', classes: '*' }, 'ol': { styles: '*', attributes: '*', classes: '*' }, 'ul': { styles: '*', attributes: '*', classes: '*' }, 'li': { styles: '*', attributes: '*', classes: '*' }, 'dl': { styles: '*', attributes: '*', classes: '*' }, 'dt': { styles: '*', attributes: '*', classes: '*' }, 'dd': { styles: '*', attributes: '*', classes: '*' }, 'figure': { styles: '*', attributes: '*', classes: '*' }, 'figcaption': { styles: '*', attributes: '*', classes: '*' }, 'div': { styles: '*', attributes: '*', classes: '*' },
        'a': { styles: '*', attributes: '*', classes: '*' }, 'em': { styles: '*', attributes: '*', classes: '*' }, 'strong': { styles: '*', attributes: '*', classes: '*' }, 'small': { styles: '*', attributes: '*', classes: '*' }, 'cite': { styles: '*', attributes: '*', classes: '*' }, 'q': { styles: '*', attributes: '*', classes: '*' }, 'dfn': { styles: '*', attributes: '*', classes: '*' }, 'abbr': { styles: '*', attributes: '*', classes: '*' }, 'time': { styles: '*', attributes: '*', classes: '*' }, 'code': { styles: '*', attributes: '*', classes: '*' }, 'var': { styles: '*', attributes: '*', classes: '*' }, 'samp': { styles: '*', attributes: '*', classes: '*' }, 'kbd': { styles: '*', attributes: '*', classes: '*' }, 'sub': { styles: '*', attributes: '*', classes: '*' }, 'sup': { styles: '*', attributes: '*', classes: '*' }, 'i': { styles: '*', attributes: '*', classes: '*' }, 's': { styles: '*', attributes: '*', classes: '*' }, 'u': { styles: '*', attributes: '*', classes: '*' }, 'b': { styles: '*', attributes: '*', classes: '*' }, 'mark': { styles: '*', attributes: '*', classes: '*' }, 'ruby': { styles: '*', attributes: '*', classes: '*' }, 'rt': { styles: '*', attributes: '*', classes: '*' }, 'rp': { styles: '*', attributes: '*', classes: '*' }, 'bdo': { styles: '*', attributes: '*', classes: '*' }, 'span': { styles: '*', attributes: '*', classes: '*' },
        'ins': { styles: '*', attributes: '*', classes: '*' }, 'del': { styles: '*', attributes: '*', classes: '*' },
        'img': { styles: '*', attributes: '*', classes: '*' }, 'param': { styles: '*', attributes: '*', classes: '*' }, 'video': { styles: '*', attributes: '*', classes: '*' }, 'audio': { styles: '*', attributes: '*', classes: '*' }, 'source': { styles: '*', attributes: '*', classes: '*' }, 'canvas': { styles: '*', attributes: '*', classes: '*' }, 'map': { styles: '*', attributes: '*', classes: '*' }, 'area': { styles: '*', attributes: '*', classes: '*' },
        'table': { styles: '*', attributes: '*', classes: '*' }, 'caption': { styles: '*', attributes: '*', classes: '*' }, 'colgroup': { styles: '*', attributes: '*', classes: '*' }, 'col': { styles: '*', attributes: '*', classes: '*' }, 'tbody': { styles: '*', attributes: '*', classes: '*' }, 'thread': { styles: '*', attributes: '*', classes: '*' }, 'tfoot': { styles: '*', attributes: '*', classes: '*' }, 'tr': { styles: '*', attributes: '*', classes: '*' }, 'td': { styles: '*', attributes: '*', classes: '*' }, 'th': { styles: '*', attributes: '*', classes: '*' },
        'details': { styles: '*', attributes: '*', classes: '*' }, 'summary': { styles: '*', attributes: '*', classes: '*' }, 'command': { styles: '*', attributes: '*', classes: '*' }, 'menu': { styles: '*', attributes: '*', classes: '*' },
        'iframe': { styles: '*', attributes: '*', classes: '*' },
        'style': { attributes: '*' },
    }

});
</script>