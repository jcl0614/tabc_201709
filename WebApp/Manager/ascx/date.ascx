﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="date.ascx.cs" Inherits="tabc_201709.Manager.ascx.date" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:DropDownList ID="ddl_year" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    年</td>
                <td>
                    <asp:DropDownList ID="ddl_momth" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddl_momth_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>
                    月</td>
                <td>
                    <asp:DropDownList ID="ddl_day" runat="server">
                    </asp:DropDownList>
                </td>
                <td>
                    日</td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>