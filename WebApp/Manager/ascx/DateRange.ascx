﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateRange.ascx.cs" Inherits="tabc_201709.Manager.ascx.DateRange" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:TextBox ID="txt_sDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
<asp:CalendarExtender ID="txt_sDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_sDate" TodaysDateFormat="yyyy年M月d日"></asp:CalendarExtender>
～<asp:TextBox ID="txt_eDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
<asp:CalendarExtender ID="txt_eDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_eDate" TodaysDateFormat="yyyy年M月d日"></asp:CalendarExtender>
