﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopLink.ascx.cs" Inherits="tabc_201709.Manager.ascx.TopLink" %>

<%@ Register src="Calendar.ascx" tagname="Calendar" tagprefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel_TopLink" runat="server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="border-bottom-style: solid; border-bottom-width: 1px; border-bottom-color: #edcd66; padding-bottom: 2px;">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" style="padding-right: 10px; font-size: 0px;">
                                            <asp:Button ID="btn_v0" runat="server" CausesValidation="False" CommandName="0" 
                                                Font-Names="微軟正黑體" Font-Size="Small" ForeColor="#666666" 
                                                oncommand="btn_Command" 
                                                style="background: url('images/line8.gif') repeat-x center bottom; cursor: pointer;" 
                                                Text="重要資訊" Width="120px" BorderColor="#EDCD66" BorderWidth="1px" />
                                        </td>
                                        <td align="center" style="padding-right: 10px; font-size: 0px;">
                                            <asp:Button ID="btn_v1" runat="server" CausesValidation="False" CommandName="1" 
                                                Font-Names="微軟正黑體" Font-Size="Small" ForeColor="Gray" 
                                                oncommand="btn_Command" 
                                                style="background: url('images/line8.gif') repeat-x center bottom; cursor: pointer;" 
                                                Text="資訊標籤" Width="120px" Font-Italic="False" BorderColor="White" BorderWidth="1px" />
                                        </td>
                                        <td align="center" style="padding-right: 10px; font-size: 0px;">
                                            <asp:Button ID="btn_v2" runat="server" CausesValidation="False" CommandName="2" 
                                                Font-Names="微軟正黑體" Font-Size="Small" ForeColor="Gray" 
                                                oncommand="btn_Command" 
                                                style="background: url('images/line8.gif') repeat-x center bottom; cursor: pointer;" 
                                                Text="資訊標籤" Width="120px" Font-Italic="False" BorderColor="White" BorderWidth="1px" />
                                        </td>
                                        <td align="center" style="padding-right: 10px; font-size: 0px;">
                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                                AssociatedUpdatePanelID="UpdatePanel_TopLink">
                                                <progresstemplate>
                                                    <img alt="" src="images/loading.gif" border="0" height="20" />
                                                </progresstemplate>
                                            </asp:UpdateProgress>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right" style="font-family: 微軟正黑體; color: #555555; font-size: 14px;">
                                &nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <asp:UpdatePanel ID="UpdatePanel_LinkVal" runat="server" 
                                onload="UpdatePanel_TopLink_Load">
                                <ContentTemplate>
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td width="50">
                                                <uc1:Calendar ID="Calendar1" runat="server" />
                                            </td>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="font-family: 微軟正黑體; color: #555555; font-size: 14px;">
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">
                                                            <asp:HyperLink ID="hlink_t1" runat="server" ForeColor="#D75304" NavigateUrl="#" Target="_blank" Visible="False">年度獎勵競賽</asp:HyperLink>
                                                        </td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                        <td align="left" style="height: 26px; padding-right: 50px;">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Timer ID="Timer1" runat="server" Interval="300000" Enabled="False">
                                    </asp:Timer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
