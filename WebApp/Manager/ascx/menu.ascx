﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="menu.ascx.cs" Inherits="tabc_201709.Manager.ascx.menu" %>

  

<div id="Panel_menu">
    <table align="center" border="0" cellpadding="0"
        cellspacing="0" width="195">
        <tr>
            <td style="padding-top: 2px; padding-bottom: 5px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center">
                            <div id="menu_open" align="center" class="menuSubhl" style="width: 50px; cursor: pointer">
                                展開
                            </div>
                        </td>
                        <td align="center">
                            <div id="menu_close" align="center" class="menuSubhl" style="cursor: pointer; width: 50px">
                                縮合
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Literal ID="Literal_menu" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</div>
