﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="selcolor.aspx.cs" Inherits="tabc_201709.Manager.ascx.selcolor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>顏色選擇</title>
    <script language="javascript">
        // URL參數值
        var URLParams = new Object();
        var aParams = document.location.search.substr(1).split('&');
        for (i = 0 ; i < aParams.length ; i++) {
            var aParam = aParams[i].split('=');
            URLParams[aParam[0]] = aParam[1];
        }
        // 是否為色碼
        function IsColor(color) {
            var temp = color;
            if (temp == "") return true;
            if (temp.length != 7) return false;
            return (temp.search(/\#[a-fA-F0-9]{6}/) != -1);
        }

        var sAction = URLParams['action'];
        var color = "";
        var oMyObject = window.dialogArguments;
        color = oMyObject.firstName;

        switch (sAction) {
            default:			//  其它顏色框
                if (URLParams['color']) {
                    color = decodeURIComponent(URLParams['color']);
                }
                break;
        }

        //  預設顯示值
        if (!color) color = "#000000";

        // 返回有背曈顏色屬性的對象
        function GetParent(obj) {
            while (obj != null && obj.tagName != "td" && obj.tagName != "tr" && obj.tagName != "TH" && obj.tagName != "TABLE")
                obj = obj.parentElement;
            return obj;
        }

        //  返回選取區的選定物件
        function GetControl(obj, sTag) {
            obj = obj.item(0);
            if (obj.tagName == sTag) {
                return obj;
            }
            return null;
        }

        //  數值轉為RGB16進位顏色格式
        function N2Color(s_Color) {
            s_Color = s_Color.toString(16);
            switch (s_Color.length) {
                case 1:
                    s_Color = "0" + s_Color + "0000";
                    break;
                case 2:
                    s_Color = s_Color + "0000";
                    break;
                case 3:
                    s_Color = s_Color.substring(1, 3) + "0" + s_Color.substring(0, 1) + "00";
                    break;
                case 4:
                    s_Color = s_Color.substring(2, 4) + s_Color.substring(0, 2) + "00";
                    break;
                case 5:
                    s_Color = s_Color.substring(3, 5) + s_Color.substring(1, 3) + "0" + s_Color.substring(0, 1);
                    break;
                case 6:
                    s_Color = s_Color.substring(4, 6) + s_Color.substring(2, 4) + s_Color.substring(0, 2);
                    break;
                default:
                    s_Color = "";
            }
            return '#' + s_Color;
        }

        // 
        function InitDocument() {
            ShowColor.bgColor = color;
            RGB.innerHTML = color;
            SelColor.value = color;
        }


        var SelRGB = color;
        var DrRGB = '';
        var SelGRAY = '120';

        var hexch = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

        function ToHex(n) {
            var h, l;

            n = Math.round(n);
            l = n % 16;
            h = Math.floor((n / 16)) % 16;
            return (hexch[h] + hexch[l]);
        }

        function DoColor(c, l) {
            var r, g, b;

            r = '0x' + c.substring(1, 3);
            g = '0x' + c.substring(3, 5);
            b = '0x' + c.substring(5, 7);

            if (l > 120) {
                l = l - 120;

                r = (r * (120 - l) + 255 * l) / 120;
                g = (g * (120 - l) + 255 * l) / 120;
                b = (b * (120 - l) + 255 * l) / 120;
            } else {
                r = (r * l) / 120;
                g = (g * l) / 120;
                b = (b * l) / 120;
            }

            return '#' + ToHex(r) + ToHex(g) + ToHex(b);
        }

        function EndColor() {
            var i;

            if (DrRGB != SelRGB) {
                DrRGB = SelRGB;
                for (i = 0; i <= 30; i++)
                    GrayTable.rows(i).bgColor = DoColor(SelRGB, 240 - i * 8);
            }

            SelColor.value = DoColor(RGB.innerText, GRAY.innerText);
            ShowColor.bgColor = SelColor.value;
        }
    </script>
    <script language="javascript">
        function ColorTable2_over() {
            if (event.srcElement.bgColor != "") {
                GRAY.innerText = "120";
                RGB.innerText = event.srcElement.bgColor;
                EndColor();
            }
        }
    </script>
    <script language="javascript">
        function ColorTable2_out() {
            GRAY.innerText = SelGRAY;
            RGB.innerText = SelRGB;
            EndColor();
        }
    </script>
    <script language="javascript">
        function ColorTable_click() {
            SelGRAY = "120";
            GRAY.innerText = "120";
            SelRGB = event.srcElement.bgColor;
            EndColor();
        }
    </script>
    <script language="javascript">
        function ColorTable_over() {
            GRAY.innerText = "120";
            RGB.innerText = event.srcElement.bgColor;
            EndColor();
        }
    </script>
    <script language="javascript">
        function ColorTable_out() {
            GRAY.innerText = SelGRAY;
            RGB.innerText = SelRGB;
            EndColor();
        }
    </script>
    <script language="javascript">
        function GrayTable_click() {
            SelGRAY = event.srcElement.title;
            RGB.innerText = $get('SelColor').value;
            EndColor();
        }
    </script>
    <script language="javascript">
        function GrayTable_over() {
            GRAY.innerText = event.srcElement.title;
            RGB.innerText = $get('SelColor').value;
            EndColor();
        }
    </script>
    <script language="javascript">
        function GrayTable_out() {
            GRAY.innerText = SelGRAY;
            RGB.innerText = SelRGB;
            EndColor();
        }
    </script>
    <script language="javascript">
        function OK() {
            color = SelColor.value;
            if (!IsColor(color)) {
                alert('無效的顏色值');
                return;
            }

            switch (sAction) {
                default:
                    window.returnValue = color;
                    break;
            }
            window.close();
        }
    </script>
    <script language="javascript">
        function c() {
            color = event.srcElement.bgColor;
            if (!IsColor(color)) {
                alert('無效的顏色值');
                return;
            }

            switch (sAction) {
                default:
                    window.returnValue = color;
                    break;
            }
            window.close();
        }
    </script>
</head>
<body oncontextmenu="return false" bgcolor="#cccccc" onload="InitDocument()" style="background-image: url('images/doeasy_bg_1.jpg'),url('../images/doeasy_bg_1.jpg'); background-repeat: repeat-x; background-position: 0px -100px; background-color: #CCCCCC;">
    <table align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div align="center">
                    <center>
                        <table border="0" cellpadding="0" cellspacing="10">
                            <tr>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" id="ColorTable" onclick="ColorTable_click()" onmouseover="ColorTable_over()" onmouseout="ColorTable_out()" style="CURSOR: pointer">
                                        <script language="javascript">
                                            function wc(r, g, b, n) {
                                                r = ((r * 16 + r) * 3 * (15 - n) + 0x80 * n) / 15;
                                                g = ((g * 16 + g) * 3 * (15 - n) + 0x80 * n) / 15;
                                                b = ((b * 16 + b) * 3 * (15 - n) + 0x80 * n) / 15;

                                                document.write('<td BGCOLOR=#' + ToHex(r) + ToHex(g) + ToHex(b) + ' height=8 width=8></td>');
                                            }

                                            var cnum = new Array(1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0);

                                            for (i = 0; i < 16; i++) {
                                                document.write('<tr>');
                                                for (j = 0; j < 30; j++) {
                                                    n1 = j % 5;
                                                    n2 = Math.floor(j / 5) * 3;
                                                    n3 = n2 + 3;

                                                    wc((cnum[n3] * n1 + cnum[n2] * (5 - n1)),
                                                        (cnum[n3 + 1] * n1 + cnum[n2 + 1] * (5 - n1)),
                                                        (cnum[n3 + 2] * n1 + cnum[n2 + 2] * (5 - n1)), i);
                                                }

                                                document.writeln('</tr>');
                                            }
                                        </script>
                                    </table>

                                </td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0" id="GrayTable" onclick="GrayTable_click()" onmouseover="GrayTable_over()" onmouseout="GrayTable_out()" style="CURSOR: pointer">
                                        <script language="javascript">
                                            for (i = 255; i >= 0; i -= 8.5)
                                                document.write('<tr BGCOLOR=#' + ToHex(i) + ToHex(i) + ToHex(i) + '><td TITLE=' + Math.floor(i * 16 / 17) + ' height=4 width=20></td></tr>');
                                        </script>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table border="0" id="ColorTable2" cellpadding="" cellspacing="4" style="CURSOR: pointer">
                            <%
                                string[] color = DataWebSet.dataReader("SelColor").Split(',');
                                string[] id = new string[15] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O" };
                                Response.Write("<tr>");
                                for (int i = 0; i < 15; i++)
                                    Response.Write(string.Format("<td BGCOLOR='{0}' onClick='c()' onmouseover='ColorTable2_over()' onmouseout='ColorTable2_out()' id='{1}' height='13' width='13'></td>", color[i], id[i]));
                                Response.Write("</tr>");
                                Response.Write("<tr>");
                                for (int i = 0; i < 15; i++)
                                    Response.Write(string.Format("<td style='font-size: small;font-family: Arial;color: #ffffff' height='13' width='13'>{0}</td>", id[i]));
                                Response.Write("</tr>");
                            %>
                        </table>


                    </center>
                </div>
                <div align="center">
                    <center>
                        <table border="0" cellpadding="0" cellspacing="10">
                            <tr>
                                <td align="center" rowspan="2" valign="middle">
                                    <table border="1"
                                        cellpadding="0" cellspacing="0" height="30" id="ShowColor"
                                        bgcolor="" style="border: 2px outset #FFFFFF; width: 50px; height: 50px">
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                                <td rowspan="2" style="color: #666666; font-size: smaller;">
                                    <table>
                                        <tr>
                                            <td style="color: #666666; font-size: 13px; font-weight: bold; font-family: 微軟正黑體;">顏色：</td>
                                            <td>
                                                <span id="RGB"
                                                    style="color: #003366; font-size: small; font-family: Arial, Helvetica, sans-serif;">&nbsp;</span></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #666666; font-size: 13px; font-weight: bold; font-family: 微軟正黑體;">亮度：</td>
                                            <td>
                                                <span id="GRAY"
                                                    style="color: #003366; font-family: Arial, Helvetica, sans-serif;">120</span></td>
                                        </tr>
                                        <tr>
                                            <td style="color: #666666; font-size: 13px; font-weight: bold; font-family: 微軟正黑體;">代碼：</td>
                                            <td>
                                                <input id="SelColor" size="7"
                                                    style="border: 1px solid #999999; width: 60px; color: #003366; font-size: small; font-family: Arial, Helvetica, sans-serif; text-align: center;"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <img alt="" src="../images/Check-icon.png" id="Ok" onclick="OK();" style="CURSOR: pointer" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img alt="" src="../images/Delete-icon.png" onclick="window.close();" style="CURSOR: pointer" /></td>
                            </tr>
                        </table>
                        <form id="form1" runat="server" style="padding: 0px; margin: 0px">
                            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
                            <script type="text/javascript">
                                function f() {
                                    RGB.innerText = $get('SelColor').value;
                                    SelRGB = $get('SelColor').value;
                                    EndColor();
                                    $get($get('Select1').value).bgColor = $get('SelColor').value;
                                    PageMethods.m($get('A').bgColor + $get('B').bgColor + $get('C').bgColor + $get('D').bgColor + $get('E').bgColor + $get('F').bgColor + $get('G').bgColor + $get('H').bgColor + $get('I').bgColor + $get('J').bgColor + $get('K').bgColor + $get('L').bgColor + $get('M').bgColor + $get('N').bgColor + $get('O').bgColor);
                                }
                                function Button1_onclick() {

                                }

                            </script>
                            <table>
                                <tr>
                                    <td style="font-size: 13px; font-family: 微軟正黑體; height: 21px;">色彩位置</td>
                                    <td>
                                        <select id="Select1" name="D1" style="font-size: 11px">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="C">C</option>
                                            <option value="D">D</option>
                                            <option value="E">E</option>
                                            <option value="F">F</option>
                                            <option value="G">G</option>
                                            <option value="H">H</option>
                                            <option value="I">I</option>
                                            <option value="J">J</option>
                                            <option value="K">K</option>
                                            <option value="L">L</option>
                                            <option value="M">M</option>
                                            <option value="N">N</option>
                                            <option value="O">O</option>
                                        </select></td>
                                    <td>
                                        <input id="Button1" type="button" onclick="f();" value="儲存色彩"
                                            style="font-size: 11px; font-family: 微軟正黑體; height: 21px;"
                                            onclick="return Button1_onclick()" /></td>
                                </tr>
                            </table>
                        </form>


                    </center>
                </div>
            </td>
        </tr>
    </table>

</body>
</html>