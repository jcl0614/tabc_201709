﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ExamEregion.aspx.cs" Inherits="tabc_201709.Manager.ExamEregion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">

    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">報名日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EG_ExamDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">最新狀態：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_Status" runat="server">
                                                    <asp:ListItem Value="1">報名中</asp:ListItem>
                                                    <asp:ListItem Value="2">報名截止</asp:ListItem>
                                                    <asp:ListItem Value="3">考試結束</asp:ListItem>
                                                    <asp:ListItem Value="4">尚未開放報名</asp:ListItem>
                                                    <asp:ListItem Value="5">入場證下載</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" BackColor="#7DC44E" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnStyle" OnClick="btnRestQuery_Click" BackColor="#7B7BC0" Text="條件清除" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">證照分類：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EG_Type" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_EG_Type_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">證照科目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EG_Subject" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">證照關鍵字：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EG_Name" runat="server" MaxLength="100" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">考區：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EG_Region" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_exam" SelectMethod="DataReader_exam"
                                        TypeName="DataERegion"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E">
                                                <table cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 20px 10px 20px 10px">
                                                            <asp:Label ID="lbl_EG_Name0" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                            </td>
                                                        <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                                <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">證照分類</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Type" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">證照科目</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Subject" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">考試名稱</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Name" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">最新狀態</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_Status" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">報名期間</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_StartDate" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">考試日期</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_ExamDate" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">區域</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Region" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                                <asp:LinkButton ID="lbtn_start" runat="server" BackColor="#EB8484" CommandName="2" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">報名</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back6" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td width="100%">
                                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E">
                                                <table cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 20px 10px 20px 10px">
                                                            <asp:Label ID="lbl_EG_Name1" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                        </td>
                                                        <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                                <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">學員證號</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_ID" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_NAME" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">單位</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_Unit" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">學歷</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:DropDownList ID="ddl_Education" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">最近模擬測驗</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_examSub" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">證照分類</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Type0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">報考科目</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Subject0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">報名日期</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_StartDate0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">考試日期</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_ExamDate0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">報名費</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EB_Regist" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">考試地區</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:RadioButtonList ID="rbl_EG_Region" runat="server" RepeatDirection="Horizontal">
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2" style="height: 50px; background-color: #F2DE97; color: #333333; font-weight: bold; font-family: 微軟正黑體; font-size: 18px;">繳費登錄資料</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">轉出銀行</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:TextBox ID="txt_EP_Bank" runat="server" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">轉出帳號末5碼</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:TextBox ID="txt_EP_Account" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">轉帳金額</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:TextBox ID="txt_EP_TransAmt" runat="server" Width="50px"></asp:TextBox>
                                                            &nbsp;(NTD)</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">轉帳日期</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:TextBox ID="txt_EP_TransDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                            <asp:CalendarExtender ID="txt_EP_TransDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EP_TransDate" TodaysDateFormat="yyyy年M月d日">
                                                            </asp:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">轉帳時間</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <uc1:TimeRangeAll ID="TimeRangeAll_EP_TransTime" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">&nbsp;</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:CheckBox ID="cbox_EP_IsCash" runat="server" AutoPostBack="True" OnCheckedChanged="cbox_EP_IsCash_CheckedChanged" Text="繳交現金" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" colspan="2" style="height: 20px">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="line-height: 125%">
                                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:HiddenField ID="hif_EP_TransNo" runat="server" />
                                                            <asp:HiddenField ID="hif_EP_UidNo" runat="server" />
                                                            <asp:HiddenField ID="hif_EG_TransNo" runat="server" />
                                                            <asp:HiddenField ID="hif_EG_UidNo" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                                <asp:LinkButton ID="lbtn_AttendPersonal" runat="server" BackColor="#7DC44E" CommandName="Attend" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="" OnClientClick="return confirm(&quot;確定要送出報名資料嗎?\n報名截止日結束後即無法修改，請再次確認報名資料是否有誤，以免影響考試權益。&quot;);">確認</asp:LinkButton>
                                                <asp:LinkButton ID="lbtn_DeletePersonal" runat="server" BackColor="#EB8484" CommandName="Delete" CssClass="btnStyle" Font-Underline="False" OnClientClick="return confirm(&quot;確定要刪除報名資料嗎?&quot;);" OnCommand="btn_Command" style="">刪除</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back5" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td width="100%">
                                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E">
                                                <table cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 20px 10px 20px 10px">
                                                            <asp:Label ID="lbl_EG_Name2" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                        </td>
                                                        <td align="right" style="padding: 20px 10px 20px 10px">
                                                            <asp:HyperLink ID="hlink_passDL" runat="server" BackColor="#EB8484" CssClass="btnStyle" Target="_blank">入場證下載</asp:HyperLink>
                                                            &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back8" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background-color: #F8F8C8; padding: 10px">
                                                <asp:Literal ID="Literal_EF1" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background-color: #FFBD6E; padding: 15px">
                                                <asp:HyperLink ID="hlink_passDL0" runat="server" BackColor="#EB8484" CssClass="btnStyle" Target="_blank">入場證下載</asp:HyperLink>
                                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back7" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td width="100%">
                                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E">
                                                <table cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 20px 10px 20px 10px">
                                                            <asp:Label ID="lbl_EG_Name3" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                        </td>
                                                        <td align="right" style="padding: 20px 10px 20px 10px">
                                                            <asp:LinkButton ID="lbtn_start1" runat="server" BackColor="#7DC44E" CommandName="1" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">我要報名</asp:LinkButton>
                                                            &nbsp;<asp:HyperLink ID="hlink_quesDL0" runat="server" BackColor="#0099CC" CssClass="btnStyle" Target="_blank" Font-Underline="False">題庫下載</asp:HyperLink>
                                                            &nbsp;<asp:LinkButton ID="lbtn_Back11" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background-color: #F8F8C8; padding: 10px">
                                                <asp:Literal ID="Literal_EF2" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="background-color: #FFBD6E; padding: 15px">
                                                <asp:LinkButton ID="lbtn_start2" runat="server" BackColor="#7DC44E" CommandName="1" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">我要報名</asp:LinkButton>
                                                &nbsp;<asp:HyperLink ID="hlink_quesDL" runat="server" BackColor="#0099CC" CssClass="btnStyle" Target="_blank" Font-Underline="False">題庫下載</asp:HyperLink>
                                                &nbsp;<asp:LinkButton ID="lbtn_Back12" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>
</asp:Content>
