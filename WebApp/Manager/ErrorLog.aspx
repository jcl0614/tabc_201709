﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_Open.master" AutoEventWireup="true" CodeBehind="ErrorLog.aspx.cs" Inherits="tabc_201709.Manager.ErrorLog" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkExport" runat="server" CommandName="2"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 匯出</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command" OnClientClick="return confirm(&quot;確定要刪除日期區間資料嗎?&quot;);"><img src="<%=ResolveUrl("images/close_2.png") %>" border="0" style="vertical-align:bottom" /> 批次刪除</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:AsyncPostBackTrigger ControlID="lnkDelete" />
            <asp:PostBackTrigger ControlID="lnkExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txt_edDate_BEG_q" runat="server" MaxLength="10" Width="85px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_edDate_BEG_q_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_edDate_BEG_q" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                                &nbsp;～&nbsp;<asp:TextBox ID="txt_edDate_END_q" runat="server" MaxLength="10" Width="85px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_edDate_END_q_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_edDate_END_q" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataErrorLog"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">編號</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_Id" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">訊息</td>
                                            <td class="tdData10s">
                                                <asp:Literal ID="Literal_Message" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">日期</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_ApplyDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete"
                                                    OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="刪除" CssClass="btnDelete" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel"
                                                    OnCommand="btn_Command" Text="取消" CssClass="btnCancel" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
