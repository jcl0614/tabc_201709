﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PdtDetail.aspx.cs" Inherits="tabc_201709.Manager.PdtDetail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href="/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script src="/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/jQuery/jquery.blockUI.js" type="text/javascript"></script>
    <style>
        .ui-tabs-anchor{
                font-family: 微軟正黑體;
                font-weight: bold;
        }
        .ui-tabs-panel{
                font-family: 微軟正黑體;
        }
    </style>
    <script> 
        $(function () {
            $("#tabs").tabs();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">商品費率</a></li>
                <li><a href="#tabs-2">保障利益</a></li>
                <li><a href="#tabs-3">規則說明</a></li>
                <li><a href="#tabs-4">商品條款</a></li>
            </ul>
            <div id="tabs-1">
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
            <div id="tabs-2">
                <asp:Literal ID="Literal2" runat="server"></asp:Literal>
            </div>
            <div id="tabs-3">
                <asp:Literal ID="Literal3" runat="server"></asp:Literal>
            </div>
            <div id="tabs-4">
                <asp:Literal ID="Literal4" runat="server"></asp:Literal>
            </div>
        </div>
    </form>
</body>
</html>
