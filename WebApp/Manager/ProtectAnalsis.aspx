﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProtectAnalsis.aspx.cs" Inherits="tabc_201709.Manager.ProtectAnalsis" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    
    <link href="/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script src="/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/jQuery/jquery.blockUI.js" type="text/javascript"></script>
    <style>
        .ui-tabs-anchor{
                font-family: 微軟正黑體;
                font-weight: bold;
        }
        .ui-tabs-panel{
                font-family: 微軟正黑體;
        }
        table {
            font-family: 微軟正黑體;
            border: 1px solid #000;
            border-collapse: collapse;
        }

        tr, td {
            border: 1px solid #000;
            padding: 5px;
        }
    </style>
    <script> 
        $(function () {
            $("#tabs").tabs();
            $("#tabs_sub1").tabs();
            $("#tabs_sub2").tabs();
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">投保項目總覽</a></li>
                <li><a href="#tabs-2">每月保費</a></li>
                <li><a href="#tabs-3">每年度保費</a></li>
                <li><a href="#tabs-4">生存領回</a></li>
                <li><a href="#tabs-5">保障分析</a></li>
                <li><a href="#tabs-6">綜合投保利益</a></li>
                <li><a href="#tabs-7">醫療明細</a></li>
                <li><a href="#tabs-8">保障明細表</a></li>
                <li><a href="#tabs-9">綜合保障明細</a></li>
            </ul>
            <div id="tabs-1">
                <asp:Literal ID="Literal_ItemList" runat="server"></asp:Literal>
            </div>
            <div id="tabs-2">
                <asp:Literal ID="Literal_dtPerMonthFee" runat="server"></asp:Literal>
            </div>
            <div id="tabs-3">
                <asp:Literal ID="Literal_dtFee" runat="server"></asp:Literal>
            </div>
            <div id="tabs-4">
                <asp:Literal ID="Literal_dtBack" runat="server"></asp:Literal>
            </div>
            <div id="tabs-5">
                <asp:Literal ID="Literal_dtProtectAnalsis" runat="server"></asp:Literal>
            </div>
            <div id="tabs-6">
                <asp:Literal ID="Literal_dtProfit" runat="server"></asp:Literal>
            </div>
            <div id="tabs-7">
                <asp:Literal ID="Literal_tbHealthy" runat="server"></asp:Literal>
            </div>
            <div id="tabs-8">
                <asp:Literal ID="Literal_FamilyDetail" runat="server"></asp:Literal>
            </div>
            <div id="tabs-9">
                <asp:Literal ID="Literal_FamilyAll" runat="server"></asp:Literal>
            </div>
        </div>
        <div style="display:none;">
            <asp:Literal ID="Literal_error" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
