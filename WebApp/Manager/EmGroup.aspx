﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="EmGroup.aspx.cs" Inherits="tabc_201709.Manager.EmGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
        <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1" 
        CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0" 
        CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="Panel_Form" runat="server">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <table ID="Table3" align="center" border="0" cellpadding="0" cellspacing="0" 
                        width="750">
                        <tr>
                            <td align="center" height="3">
                                <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                    <tr>
                                        <td align="center" class="tdQueryHead">
                                            群組名稱：</td>
                                        <td align="center" class="tdQueryData">
                                            <asp:TextBox ID="txtSh_name" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="center" class="tdQueryHead">
                                            啟用：</td>
                                        <td align="center" class="tdQueryData">
                                            <asp:DropDownList ID="ddlSh_enable" runat="server">
                                                <asp:ListItem Value=""></asp:ListItem>
                                                <asp:ListItem Value="true">啟用</asp:ListItem>
                                                <asp:ListItem Value="false">停用</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="center" width="85">
                                            <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                        </td>
                                        <td align="center" width="35">
                                            <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                        </td>
                                    </tr>
                                </table>
                                <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                                CssClass="ddlPage">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                                CssClass="ddlPage">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                                Width="120px"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                                CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                                CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                                CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                                CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                    CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                    EmptyDataText="查無符合資料!!!" 
                                    onselectedindexchanged="GridView1_SelectedIndexChanged">
                                    <PagerSettings Visible="False" />
                                </asp:GridView>
                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                    SelectCountMethod="GetCount" SelectMethod="DataReader" TypeName="DataEmGroup">
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table ID="Table13" align="center" border="0" cellpadding="2" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <table ID="Table11" border="0" cellpadding="2" cellspacing="1" width="450">
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="100">
                                            群組名稱</td>
                                        <td align="left" class="tdData10s">
                                            <asp:TextBox ID="txt_name" runat="server" MaxLength="10"></asp:TextBox>
                                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                            <span style="color: #D663A5">長度10↓的字元</span>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="100">
                                            限制IP</td>
                                        <td align="left" class="tdData10s">
                                            <asp:TextBox ID="txt_ipLimit" runat="server" Width="300px"></asp:TextBox>
                                            <br />
                                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                            <font style="color: #D663A5">多筆以「 ; 」分隔</font></td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="100">
                                            啟用</td>
                                        <td align="left" class="tdData10s">
                                            <asp:CheckBox ID="cbox_enable" runat="server" Checked="True" />
                                            <asp:HiddenField ID="hif_id" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="tdMaintainButton" colspan="2">
                                            <asp:Button ID="btnAppend" runat="server" CommandName="Append" 
                                                CssClass="btnAppend" oncommand="btn_Command" Text="存檔" />
                                            <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" 
                                                oncommand="btn_Command" Text="存檔" />
                                            <asp:Button ID="btnDelete" runat="server" CommandName="Delete" 
                                                CssClass="btnDelete" onclientclick="return confirm(&quot;確定要刪除嗎?&quot;);" 
                                                oncommand="btn_Command" Text="刪除" />
                                            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                                                CssClass="btnCancel" oncommand="btn_Command" Text="取消" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="10">
                                &nbsp;</td>
                            <td valign="top" width="670">
                                <table border="0" cellpadding="2" cellspacing="1" class="EditTable" 
                                    width="100%">
                                    <tr>
                                        <td align="center" class="QueryMainTable" 
                                            style="font-family: 微軟正黑體; font-weight: bold; color: #333333; letter-spacing: 5px;">
                                            權限管理 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table cellpadding="2" cellspacing="1" width="100%">
                                                <tr>
                                                    <td align="center" class="MS_tdTitle" height="24" width="120">
                                                        系統類別選單</td>
                                                    <td align="center" class="tdData10s" height="24">
                                                        <asp:DropDownList ID="ddl_muLayer" runat="server" AutoPostBack="True" 
                                                            onselectedindexchanged="ddl_muLayer_SelectedIndexChanged" Width="180px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="center" bgcolor="#EBEBEB" rowspan="2" width="200">
                                                        <asp:CheckBoxList ID="cbl_Function" runat="server" CellPadding="2" 
                                                            CellSpacing="0" CssClass="string10" RepeatColumns="2">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                    <td align="center" class="tdMaintainButton" rowspan="2" width="100">
                                                        <asp:Button ID="btnInsert" runat="server" CommandName="Insert" 
                                                            CssClass="btnAppend" oncommand="btnInsert_Command" Text="新增" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" class="MS_tdTitle" width="120">
                                                        系統功能選單</td>
                                                    <td align="center" class="tdData10s">
                                                        <asp:DropDownList ID="ddl_muNo" runat="server" AutoPostBack="True" 
                                                            Width="180px" onselectedindexchanged="ddl_muNo_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 20px">
                                            <asp:Label ID="lbl_Msg1" runat="server" CssClass="errmsg12"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl" 
                                                width="100%">
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlPageRow1" runat="server" AutoPostBack="True" 
                                                            CssClass="ddlPage">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlPageChange1" runat="server" AutoPostBack="True" 
                                                            CssClass="ddlPage">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="center">
                                                        <asp:Label ID="labGridViewRows1" runat="server" CssClass="GridPageCount"></asp:Label>
                                                    </td>
                                                    <td align="center">
                                                        <asp:LinkButton ID="lbtnPageCtrlFirst1" runat="server" CommandName="First" 
                                                            CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lbtnPageCtrlPrev1" runat="server" CommandName="Prev" 
                                                            CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lbtnPageCtrlNext1" runat="server" CommandName="Next" 
                                                            CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                                        &nbsp;<asp:LinkButton ID="lbtnPageCtrlLast1" runat="server" CommandName="Last" 
                                                            CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                                DataSourceID="ObjectDataSource2" onrowcreated="GridView2_RowCreated" 
                                                onrowdeleting="GridView2_RowDeleting" onrowupdating="GridView2_RowUpdating">
                                                <PagerSettings Visible="False" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
                                                DeleteMethod="EmGroupLimitDelete" EnablePaging="True" 
                                                ondeleting="ObjectDataSource2_Deleting" onupdating="ObjectDataSource2_Updating" 
                                                SelectCountMethod="GetCount" SelectMethod="EmGroupLimitReaderForEmGroup" 
                                                SortParameterName="sortExpression" TypeName="DataEmGroupLimit" 
                                                UpdateMethod="EmGroupLimitUpdate"></asp:ObjectDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

