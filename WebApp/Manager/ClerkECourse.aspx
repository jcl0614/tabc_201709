﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ClerkECourse.aspx.cs" Inherits="tabc_201709.Manager.ClerkECourse" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>


<%@ Register src="ascx/UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc3" %>


<%--<%@ Register TagPrefix="aspNewControls" Namespace="AppCode.Lib"%>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <asp:Timer ID="Timer1" runat="server" Enabled="False" Interval="1000" OnTick="Timer1_Tick"></asp:Timer>
    <asp:Timer ID="Timer2" runat="server" Enabled="False" Interval="1000" OnTick="Timer2_Tick"></asp:Timer>
    <%--<%@ Register TagPrefix="aspNewControls" Namespace="AppCode.Lib"%>--%>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">開課日期：</td>
                                            <td align="center" class="tdQueryData" colspan="3">
                                                <uc2:DateRange ID="DateRangeSh_EC_SDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">課程名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_CName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" BackColor="#7DC44E" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnStyle" OnClick="btnRestQuery_Click" Text="條件清除" BackColor="#7B7BC0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">學院：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">分級：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Rating" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">講師：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Name" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">型態：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Mode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">型別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Category" runat="server">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="1">數位</asp:ListItem>
                                                    <asp:ListItem Value="2">實體</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">上課地點：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_Place" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True" >
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_clerk" SelectMethod="DataReader_clerk"
                                        TypeName="DataECourse"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName0" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">
                                                <asp:LinkButton ID="lbtn_Course" runat="server" BackColor="Turquoise" CssClass="btnStyle" Font-Underline="False" style="" OnCommand="lnk_Command" CommandName="3">進入課程</asp:LinkButton>
                                                <asp:LinkButton ID="lbtn_Exam" runat="server" BackColor="#EB8484" CssClass="btnStyle" Font-Underline="False" style="" OnCommand="lnk_Command" CommandName="7">進入測驗</asp:LinkButton>
                                                <asp:LinkButton ID="lbtn_Group" runat="server" BackColor="#7DC44E" CssClass="btnStyle" Font-Underline="False" style="" OnCommand="lnk_Command" CommandName="5">團體報名</asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lbtn_Back" runat="server" BackColor="#7B7BC0" CssClass="btnStyle" Font-Underline="False" style="" OnCommand="lnk_Command" CommandName="0">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <span style="font-size: 16px; color: #FFFFFF; background-color: #FC9C00; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">課程簡介</span>
                                    <br /><br />
                                    <asp:Label ID="lbl_EC_Summary" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #2183C9; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">授課講師</span>&nbsp;<br /><br /><asp:Label ID="lbl_ET_TName" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #2183C9; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">講師簡介</span>&nbsp;<br /><br /><asp:Label ID="lbl_ET_TSummary" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #6DB040; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">課程明細與相關條件限制</span>
                                    <br />
                                    <br />
                                    <table cellpadding="6" cellspacing="0" style="font-size: 14px">
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">學院：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Type" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">課程分級：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_Rating" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">課程名稱：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_CName" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">訓練通報：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_TrainType" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">通報時數：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_PassHours" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">收費方式：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeeWay" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">課後測驗：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_IsExam" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">收費點數：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeePoints" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">檔案格式：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EM_Type" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">收費金額：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeeAmt" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">檔案大小：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EM_FileSize" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">測驗題數：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_QuizNo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">影音檔時間長度：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EM_FileTime" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" width="150" style="font-weight: bold">成績限制：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_PassScore" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" style="font-weight: bold">備註：</td>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="lbl_EC_Memo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_Course0" runat="server" BackColor="Turquoise" CommandName="3" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">進入課程</asp:LinkButton>
                                    <asp:LinkButton ID="lbtn_Exam0" runat="server" BackColor="#EB8484" CommandName="7" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">進入測驗</asp:LinkButton>
                                    <asp:LinkButton ID="lbtn_Group0" runat="server" BackColor="#7DC44E" CommandName="5" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">團體報名</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back0" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName1" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category0" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">
                                                <asp:LinkButton ID="lbtn_personal" runat="server" BackColor="Turquoise" CommandName="4" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">個人報名</asp:LinkButton>
                                                <asp:LinkButton ID="lbtn_Group1" runat="server" BackColor="#7DC44E" CommandName="5" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">團體報名</asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lbtn_Back1" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #FC9C00; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">課程簡介</span>
                                    <br />
                                    <br />
                                    <asp:Label ID="lbl_EC_Summary0" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #2183C9; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">授課講師</span>&nbsp;<br /><br /><asp:Label ID="lbl_ET_TName0" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #2183C9; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">講師簡介</span>&nbsp;<br /><br /><asp:Label ID="lbl_ET_TSummary0" runat="server" Font-Size="13px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #6DB040; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">課程明細與相關條件限制</span>
                                    <br />
                                    <br />
                                    <table cellpadding="6" cellspacing="0" style="font-size: 14px">
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">學院：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Type0" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">課程分級：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_Rating0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">課程名稱：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_CName2" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">訓練通報：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_TrainType0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">通報時數：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_PassHours0" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">收費方式：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeeWay0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">強制訓練：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_IsForce" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">收費點數：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeePoints0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">應訓練年度：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Annual" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">收費金額：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_FeeAmt0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">開課單位：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Unit" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">測驗題數：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_QuizNo0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">開課日期：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_SDate" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">成績限制：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_PassScore0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">上課時間：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Time" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">授課時數：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_Hours" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">上課地點：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Place" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">人數限制：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_PNo" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">主持人：</td>
                                            <td align="left" width="300">
                                                <asp:Label ID="lbl_EC_Moderator" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="font-weight: bold" width="150">飲食習慣：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_EatHabits" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">&nbsp;</td>
                                            <td align="left" width="300">&nbsp;</td>
                                            <td align="right" style="font-weight: bold" width="150">交通方式：</td>
                                            <td align="left">
                                                <asp:Label ID="lbl_EC_Transport" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" style="font-weight: bold" width="150">備註：</td>
                                            <td align="left" colspan="3">
                                                <asp:Label ID="lbl_EC_Memo0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px"><span style="font-size: 16px; color: #FFFFFF; background-color: #6DB040; padding: 5px; font-weight: bold; font-family: 微軟正黑體;">課程附件</span>
                                    <br /><br />
                                    <asp:Literal ID="Literal_EM_File" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_personal0" runat="server" BackColor="Turquoise" CommandName="4" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">個人報名</asp:LinkButton>
                                    <asp:LinkButton ID="lbtn_Group2" runat="server" BackColor="#7DC44E" CommandName="5" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">團體報名</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back2" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName5" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category3" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName10" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">上課日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_SDate0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">收費點數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_FeePoints1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">收費金額</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_FeeAmt1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">通報時數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_PassHours1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_start" runat="server" BackColor="#EB8484" CommandName="6" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">開始上課</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back6" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName4" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category2" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName13" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">上課日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_SDate2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">收費點數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_FeePoints2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">收費金額</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_FeeAmt2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">通報時數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_PassHours2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">強制訓練</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_IsForce0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">飲食習慣</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:RadioButtonList ID="rbl_ED_EatHabits" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">交通方式</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:RadioButtonList ID="rbl_ED_Transport" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        
                                        
                                    </table>
                                    <table id="tb_Pay" runat="server" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="center" colspan="2" style="height: 50px; background-color: #F2DE97; color: #333333; font-weight: bold; font-family: 微軟正黑體; font-size: 18px;">繳費登錄資料</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉出銀行</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:TextBox ID="txt_EP_Bank" runat="server" MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉出帳號末5碼</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:TextBox ID="txt_EP_Account" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳金額</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:TextBox ID="txt_EP_TransAmt" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;(NTD)</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:TextBox ID="txt_EP_TransDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_EP_TransDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EP_TransDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <uc1:TimeRangeAll ID="TimeRangeAll_EP_TransTime" runat="server" />
                                            </td>
                                        </tr>
                                        
                                        </table>
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_ED_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EP_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_ED_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_TransNo" runat="server" />
                                            </td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_AttendPersonal" runat="server" BackColor="#7DC44E" CommandName="AttendPersonal" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="" OnClientClick="return confirm(&quot;確定要送出報名資料嗎?&quot;);">確認</asp:LinkButton>
                                    <asp:LinkButton ID="lbtn_DeletePersonal" runat="server" BackColor="#EB8484" CommandName="DeletePersonal" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="" OnClientClick="return confirm(&quot;確定要刪除報名資料嗎?&quot;);">刪除</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back5" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName3" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category1" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table cellpadding="0" cellspacing="0" align="center" border="0">
                                        <tr>
                                            <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">
                                                <table cellpadding="6" cellspacing="0" style="font-size: 14px">
                                                    <tr>
                                                        <td align="right">單位：</td>
                                                        <td align="left">
                                                            <asp:Label ID="lbl_PS_Unit2" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">職級：</td>
                                                        <td align="left">
                                                            <asp:Label ID="lbl_PS_Title" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br />
                                                <asp:TextBox ID="txtSh_AccID" runat="server"></asp:TextBox>
                                                &nbsp;<asp:Button ID="btn_searchAccID" runat="server" CommandName="search" OnCommand="btnApply_Command" Text="人員查詢" />
                                            </td>
                                            <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                            <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">報名人員</td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                            </td>
                                            <td align="center">
                                                <p>
                                                    <asp:ImageButton ID="ibtnRefresh" runat="server" CommandName="refresh" ImageUrl="~/Manager/images/Refresh-icon.png" OnCommand="btnApply_Command" />
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="btnAddAll" runat="server" CommandName="addAll" CssClass="btnAddAll" OnCommand="btnApply_Command" Text="全部加入" />
                                                    <br />
                                                    <br />
                                                    <asp:Button ID="btnRemoveAll" runat="server" CommandName="removeAll" CssClass="btnRemoveAll" OnCommand="btnApply_Command" Text="全部移除" />
                                                    <br />
                                                    <br />
                                                    <asp:ImageButton ID="ibtnRight" runat="server" CommandName="add" ImageUrl="~/Manager/images/Forward-icon.png" OnCommand="btnApply_Command" />
                                                    <br />
                                                    <br />
                                                    <asp:ImageButton ID="ibtnLeft" runat="server" CommandName="remove" ImageUrl="~/Manager/images/Back-icon.png" OnCommand="btnApply_Command" />
                                                </p>
                                            </td>
                                            <td align="center">
                                                <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <table id="tb_Group" runat="server" cellpadding="6" cellspacing="0" style="font-size: 14px">
                                        <tr>
                                            <td align="right" width="150" class="MS_tdTitle">交通方式：</td>
                                            <td align="left" class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:RadioButtonList ID="rbl_ED_EatHabits0" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" width="150" class="MS_tdTitle">飲食習慣：</td>
                                            <td align="left" class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:RadioButtonList ID="rbl_ED_Transport0" runat="server" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background-color: #F8F8C8; padding: 5px 5px 5px 10px">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" style="line-height: 125%">
                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_Group4" runat="server" BackColor="#EB8484" CommandName="Append" CssClass="btnStyle" Font-Underline="False" OnCommand="btnApply_Command" style="">團體報名</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back4" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View7" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName6" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category4" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName11" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">上課日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_SDate1" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hif_sDatetime" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">上課時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_EC_STime" runat="server"></asp:Label>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_inExam" runat="server" BackColor="#EB8484" CommandName="7" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">進入測驗</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back7" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Literal ID="Literal_EM_File0" runat="server"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:GridView ID="GridView_EA_File" runat="server" AutoGenerateColumns="False" CellPadding="5">
                                        <AlternatingRowStyle BackColor="#FCF3F3" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="附件類別">
                                                <ItemTemplate>
                                                    <%# Eval("EA_Type") %>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="附件名稱">
                                                <ItemTemplate>
                                                    <%# Eval("EA_Name") %>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="說明">
                                                <ItemTemplate>
                                                    <%# Eval("EA_Explan") %>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="300px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="檔案名稱">
                                                <ItemTemplate>
                                                    <%# Eval("EA_File") %>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="200px" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="下載">
                                                <ItemTemplate>
                                                    <%# Eval("EA_File", "<a href=\"/uploads/ETmatt/{0}\" target=\"_blank\"><img src=\"/images/download.png\" border=\"0\" /></a>") %>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#FFBD6E" Font-Size="13px" ForeColor="#784219" />
                                        <RowStyle BackColor="#F4F9FD" Font-Size="12px" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View8" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName7" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category5" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName12" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">測驗日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_ED_ExamDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">測驗注意事項</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_QuizNotify" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:CheckBox ID="cbox_agree" runat="server" Text="我知道了" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_start0" runat="server" BackColor="#EB8484" CommandName="8" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">開始測驗</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back8" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View9" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName8" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category6" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td class="MS_tdTitle">測驗開始時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_STime" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗結束時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_EX_ETime" runat="server"></asp:Label>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="MS_tdTitle">剩餘時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_RemainingTime" runat="server"></asp:Label>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel_EA_ListItem1" runat="server">
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_finish" runat="server" BackColor="#EB8484" CommandName="9" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="" OnClientClick="return confirm(&quot;確定要送出測驗資料嗎?&quot;);">完成測驗</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back9" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View10" runat="server">
                        <table id="print_exam" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EC_CName9" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                &nbsp;<asp:Label ID="lbl_EC_Category7" runat="server"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td class="MS_tdTitle">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit3" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName14" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_ED_ExamDate0" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗開始時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_STime0" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗結束時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_ETime0" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">總分數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_Score" runat="server" Font-Names="Arial" Font-Size="Medium"></asp:Label>
                                                <asp:HiddenField ID="hif_EX_TransNo" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel_EA_ListItem2" runat="server" Enabled="False">
                                    </asp:Panel>
                                </td>
                            </tr>
                            
                        </table>
                        <table class="Noprint" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <%--                                    <asp:LinkButton ID="lbtn_start2" runat="server" BackColor="#EB8484" CommandName="PrintExam" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="">列印測驗卷</asp:LinkButton>--%>
                                    <a class="btnStyle" style="background: #EB8484;" onclick="printScreen(print_exam,'');">列印測驗卷</a>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back10" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
    <%--<%@ Register TagPrefix="aspNewControls" Namespace="AppCode.Lib"%>--%>

    <input id="hif_scrollTop" type="hidden" value="0" />
    <input id="hif_postback" type="hidden" value="0" />

    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>
</asp:Content>
