﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ECoursePrint.aspx.cs" Inherits="tabc_201709.Manager.ECoursePrint" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/selTeacher.ascx" tagname="selTeacher" tagprefix="uc1" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>
<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>

<%@ Register src="ascx/TimeRange.ascx" tagname="TimeRange" tagprefix="uc4" %>

<%--<%@ Register TagPrefix="aspNewControls" Namespace="AppCode.Lib"%>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">

    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">開課日期：</td>
                                            <td align="center" class="tdQueryData" colspan="3">
                                                <uc2:DateRange ID="DateRangeSh_EC_SDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">課程名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_CName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">分級：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Rating" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">型態：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Mode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">課程代碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_Code" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">學院：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">適用人員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Staff" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">課程簡介：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_Summary" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_P" SelectMethod="DataReader_P"
                                        TypeName="DataECourse"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
