﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="OSMenu.aspx.cs" Inherits="tabc_201709.Manager.OSMenu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
        <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <table border="0" cellpadding="1" cellspacing="1">
                    <tr>
                        <td valign="top">
                            <table align="center" border="0" cellpadding="2" cellspacing="1" 
                                class="EditTable" width="450">
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        選單編號</td>
                                    <td class="tdData10s">
                                        <asp:TextBox ID="txt_muNo" runat="server" MaxLength="5" Width="50px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        選單分類</td>
                                    <td class="tdData10s">
                                        <asp:DropDownList ID="ddl_muLayer" runat="server" AutoPostBack="True" 
                                            onselectedindexchanged="ddl_muLayer_SelectedIndexChanged" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        顯示順序</td>
                                    <td class="tdData10s">
                                        <asp:TextBox ID="txt_muSort" runat="server" MaxLength="2" Width="50px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        選單名稱</td>
                                    <td class="tdData10s">
                                        <asp:TextBox ID="txt_muName" runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        選單連結</td>
                                    <td class="tdData10s">
                                        <asp:TextBox ID="txt_muHyperLink" runat="server" MaxLength="100" Width="200px"></asp:TextBox>
                                        <asp:HiddenField ID="hif_muHyperLink" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        連結目標</td>
                                    <td class="tdData10s">
                                        <asp:DropDownList ID="ddl_muTarget" runat="server">
                                            <asp:ListItem Selected="True" Value="_self">取代目前視窗並開啟_self</asp:ListItem>
                                            <asp:ListItem Value="_blank">在新的視窗中開啟_blank</asp:ListItem>
                                            <asp:ListItem Value="_parent">在目前視窗中開啟_parent</asp:ListItem>
                                            <asp:ListItem Value="_top">在目前視窗之上一層_top</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        啟用</td>
                                    <td class="tdData10s">
                                        <asp:CheckBox ID="cbox_muEnable" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="MS_tdTitle" width="100">次項目</td>
                                    <td class="tdData10s">
                                        <asp:CheckBox ID="cbox_muIsSub" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        連線區域</td>
                                    <td class="tdData10s">
                                        <asp:DropDownList ID="ddl_muLocation" runat="server" Width="100px">
                                            <asp:ListItem Value="0">內外網</asp:ListItem>
                                            <asp:ListItem Value="1">內部網</asp:ListItem>
                                            <asp:ListItem Value="2">外部網</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="MS_tdTitle" width="100" align="right">
                                        說明文件</td>
                                    <td class="tdData10s">
                                        <asp:DropDownList ID="ddl_Km" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="tdMaintainButton" colspan="2">
                                        <asp:Button ID="btnAppend" runat="server" CommandName="Append" 
                                            CssClass="btnAppend" oncommand="btn_Command" Text="存檔" />
                                        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" 
                                            oncommand="btn_Command" Text="存檔" />
                                        <asp:Button ID="btnDelete" runat="server" CommandName="Delete" 
                                            CssClass="btnDelete" onclientclick="return confirm(&quot;確定要刪除嗎?&quot;);" 
                                            oncommand="btn_Command" Text="刪除" />
                                        <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                                            CssClass="btnCancel" oncommand="btn_Command" Text="取消" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td align="left" style="line-height: 125%">
                                                    <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="center" valign="top" width="20">
                            &nbsp;</td>
                        <td align="center" valign="top" width="500">
                            <table cellpadding="0" cellspacing="0" class="GridViewTable" width="100%">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                            <tr>
                                                <td class="tdQueryHead">
                                                    分類：</td>
                                                <td class="tdQueryData">
                                                    <asp:DropDownList ID="ddl_QmuLayer" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tdQueryHead">
                                                    連線區域：</td>
                                                <td class="tdQueryData">
                                                    <asp:DropDownList ID="ddlSh_muLocation" runat="server">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                        <asp:ListItem Value="0">內外網</asp:ListItem>
                                                        <asp:ListItem Value="1">內部網</asp:ListItem>
                                                        <asp:ListItem Value="2">外部網</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="tdQueryHead">
                                                    啟用：</td>
                                                <td class="tdQueryData">
                                                    <asp:DropDownList ID="ddl_QmuEnable" runat="server">
                                                        <asp:ListItem Value=""></asp:ListItem>
                                                        <asp:ListItem Value="true">啟用</asp:ListItem>
                                                        <asp:ListItem Value="false">停用</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td align="center" width="85">
                                                    <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                                </td>
                                                <td align="center" width="35">
                                                    <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                                        CssClass="ddlPage">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                                        CssClass="ddlPage">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                                        Width="120px"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                                        CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                                        CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                                        CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                                        CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                            CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                            EmptyDataText="查無符合資料!!!" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged">
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                            SelectCountMethod="GetCount" SelectMethod="menuReader" 
                                            SortParameterName="sortExpression" TypeName="DataMenu">
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
 </asp:Content>

