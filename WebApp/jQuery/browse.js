﻿/* --------------------------------------------------------------------------
 * browse.js
 * -------------------------------------------------------------------------- */
(function ($) {

    $(function () {
        // on DOM ready start 'er up
        JACK.browse.init();
    });

    JACK.browse = {

        init: function () {
            var base = this;
            base.$context = JACK.core.$context;

            base.$context.find('.swatches').jack_ImageSwitcher({
                mainImage: base.getSwatchMainImage,
                after: $.proxy(this, 'onSwatchSwitch'),
                swap: false
            });
            base.attachEvents();
            // base.prepareSwatches($('.product_list'));
        },

        /* ------------------------------------------------------- */
        /* attachEvents
        /* attach click events
        /* ------------------------------------------------------- */
        attachEvents: function () {
            var base = this;

            base.$context.delegate('.browse_swatches', 'click', function (e) {
                e.preventDefault();

                if (!$(e.currentTarget).hasClass('active')) {
                    base.prepareSwatches($(e.currentTarget));
                }
            });

            base.$context.delegate('img.product_image', 'mouseenter mouseleave', function (e) {
                base.hoverAndOut($(e.currentTarget));
            });
        },

        hoverAndOut: function ($target) {
            var hoverImageLocation = $target.data('hover'),
                normalImageLocation = $target.attr('src');

            $target.attr('src', hoverImageLocation).data('hover', normalImageLocation);
        },

        /* ------------------------------------------------------- */
        /* prepareSwatches
        /* attach switchImage event to swatches
        /* ------------------------------------------------------- */
        prepareSwatches: function (target) {
            var self = this,
            oldImg = target.parents('li').find('a:first > img.product_image');

            self.switchImage(oldImg, target.find('img'), { m: '250' }, true);

            // remove all highlighted swatches for this product
            target.parents('ul:first').find('a').removeClass('browse_swatches_highlight');
            target.parents('ul:first').find('a').removeClass('active');

            // highlight the clicked swatch
            target.addClass('browse_swatches_highlight');
            target.addClass('active');
        },

        onSwatchSwitch: function (img, trigger) {
            // change the href value of the anchors so that it
            // links through to the right product

            JACK.core.updateProductPrice(img, trigger);
        },

        getSwatchMainImage: function () {
            return $(this).prev().find('img');
        },

        /* ------------------------------------------------------- */
        /* switchImage
        /* Replace the old image with new image.
        /* Must pass isSwatch as 'true' if switching with a swatch
        /* ------------------------------------------------------- */
        /* oldImg (object)
        /* newImg (object)
        /* dimensions (object) e.g. {s: '100', m: '200', l: '300'}
        /* isSwatch (boolean)
        /* ------------------------------------------------------- */
        switchImage: function (oldImg, newImg, dimensions, isSwatch) {
            isSwatch = isSwatch || false;
            newImg = newImg.attr('src');

            // new main image
            var newSource;

            if (!isSwatch) {
                newSource = newImg.replace('/' + dimensions.s + '/', '/' + dimensions.m + '/');
            } else {
                newSource = newImg.replace('/swatches/', '/' + dimensions.m + '/');
            }

            var oldImageAnchor = oldImg.parents('a');

            // replace href to image
            if (oldImageAnchor.attr('href').indexOf('.jpg') !== -1) {
                var zoomImageHref = newSource.replace('/' + dimensions.m + '/', '/' + dimensions.l + '/');
                $core.preloadImage(zoomImageHref);
                oldImageAnchor.attr('href', zoomImageHref);

                // replace href to product
            } else {
                var productCode = newSource.match(/[a-zA-Z0-9]+.jpg/)[0].split('.')[0];
                var productHref = oldImageAnchor.attr('href').replace(/[0-9]+/, productCode);
                oldImageAnchor.attr('href', productHref);
            }

            // fade main image out, change its src value and then fade it in
            oldImg.stop(true, true).animate({ 'opacity': '0' }, 200, function () {
                JACK.core.helpers.preloadImage(newSource, function () {
                    oldImg.attr('src', newSource);

                    if (oldImg.data('hover')) {
                        oldImg.data('hover', newSource.replace('/' + dimensions.m + '/', '/' + dimensions.m + '_hover/'));
                    }

                    oldImg.animate({ 'opacity': '1' }, 200);
                });
            });

            return newSource;
        }

    };

} (jQuery));