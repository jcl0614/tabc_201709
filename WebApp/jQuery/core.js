/* --------------------------------------------------------------------------
* core.js
* -------------------------------------------------------------------------- */
(function ($) {

    $(function () {
        // on DOM ready start 'er up
        JACK.core.init();
    });

    JACK.core = {
        init: function () {
            var base = this;
            base.$context = $('#content');
        }
    };


    /* --------------------------------------------------------------------------
    * JACK.core.helpers
    * resuable pieces of code to aid development throughout the site
    * -------------------------------------------------------------------------- */
    JACK.core.helpers = {


        /* --------------------------------------------------------------------------
        * getFilenameFromPath
        * returns the string found after the last forward slash in a string
        * -------------------------------------------------------------------------- */
        getFilenameFromPath: function (path) {
            return path.substring(path.lastIndexOf('/') + 1, path.length);
        },


        /* --------------------------------------------------------------------------
        * onPage
        * -------------------------------------------------------------------------- */
        onPage: function (page) {
            var regex = new RegExp(page, 'ig');
            return regex.test(window.location.href);
        },


        /* --------------------------------------------------------------------------
        * updateButtonState
        * button - DOM el/jQuery instance
        * enabled - boolean. should button be enabled
        * disableAttribute - boolean. should disabled attribute be added (true)
        * -------------------------------------------------------------------------- */
        updateButtonState: function (button, enabled, disableAttribute) {
            // button could be jQuery instance or just DOM element
            // so make sure we have access to both
            var $button = $(button);
            button = $button.get(0);

            // unless user specifically passes false to disableAttribute
            // then disableAttribute will be true (when disabling)
            if (enabled === false && disableAttribute !== false) {
                disableAttribute = true;
            }

            if (button.tagName === 'BUTTON' || button.tagName === 'INPUT') {
                $button.attr('disabled', (disableAttribute && !enabled) ? true : false);
            }

            $button[enabled ? 'removeClass' : 'addClass']('disabled');
        },


        /* --------------------------------------------------------------------------
        * disableButton
        * calls updateButtonState and passes false
        * -------------------------------------------------------------------------- */
        disableButton: function (button, disableAttribute) {
            this.updateButtonState(button, false, disableAttribute);
        },


        /* --------------------------------------------------------------------------
        * enableButton
        * calls updateButtonState and passes true
        * -------------------------------------------------------------------------- */
        enableButton: function (button) {
            this.updateButtonState(button, true);
        },


        /* --------------------------------------------------------------------------
        * setupTemplate
        * sets up and caches passed template
        * -------------------------------------------------------------------------- */
        setupTemplate: function (template) {
            var el = document.getElementById(template);

            // if the template doesn't already exist
            // try to set it up
            if (!$.template[template]) {

                // only set it up if the template isn't empty
                if (el && (el.firstChild || el.innerHTML.length)) {
                    $.template(template, el);
                    return true;
                }

            } else {
                // template already exists so it's been set up before
                return true;
            }

            // if it gets this far, there was a problem
            return false;
        },


        /* --------------------------------------------------------------------------
        * preloadImage
        * pass the path of the image you wish to preload
        * -------------------------------------------------------------------------- */
        preloadImage: function (src, callback) {
            $('<img />').load(function () {
                if (callback) callback.call(this);
            }).attr('src', src);

            return true;
        },


        /* --------------------------------------------------------------------------
        * urlify
        * formats a string for use in URLs
        * -------------------------------------------------------------------------- */
        urlify: function (string) {
            if (!string) { return; }

            // replace spaces with hyphens and lowercase
            string = string.toLowerCase().replace(/\s/g, '-');

            // remove unwanted characters
            string = string.replace(/(\.)?(\')?/g, '');

            return string;
        }


       

    }

} (jQuery));
