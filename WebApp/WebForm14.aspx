﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm14.aspx.cs" Inherits="tabc_201709.WebForm14" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">


.QueryMainTable
{
	text-align: center;
	background: repeat-x center bottom;
    background-image: url('../images/ClassLink_bg.gif'),url('../../images/ClassLink_bg.gif');
	padding: 5px 0px 5px 0px;
    width: 100%;
}

.tdQueryHead
{
	text-align: right;
	color: #666666;
	font-size: 16px;
    font-family: 微軟正黑體;
    padding-left: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
}

.tdQueryData
{
	font-size: 10pt;
	text-align: left;
	color: #666666;
    font-family: 微軟正黑體;
    padding-right: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
}

input[type="text"] {
    border: 1px solid #999999;
    padding: 3px;
 }
.btnSearch
{
    background: #f4f4f4 no-repeat 10px center;
    background-image: url("../images/Search-icon.png"),url("../../images/Search-icon.png");
    cursor:pointer;
    border: 1px solid #999999;
    font-size: 15px;
    font-family:微軟正黑體;
    padding: 3px 10px 3px 28px;
    margin: 0px 5px 0px 5px;
    color: #181818;
    border-radius: 4px;
}
.btnRefresh
{
    background: #f4f4f4 no-repeat 10px center;
    background-image: url("../images/refresh.png"),url("../../images/refresh.png");
    cursor:pointer;
    border: 1px solid #999999;
    font-size: 15px;
    font-family:微軟正黑體;
    padding: 3px 10px 3px 28px;
    margin: 0px 5px 0px 5px;
    color: #181818;
    border-radius: 4px;
}

.tablePageCtrl
{
    height: 40px;
	background: repeat-x center center;
    background-image: url('../images/ClassLink_tbg.gif'),url('../../images/ClassLink_tbg.gif');
	border-top-style: dotted; 
	border-top-width: 1px; 
	border-top-color: #ffffff;
    width:100%;
}
.tablePageCtrl td
{
    text-align:center;
}

.ddlPage
{
    background: #f6f6f6;
    cursor:pointer;
    font-size: 13px;
    font-family:微軟正黑體;
    margin: 0px 0px 0px 0px;
    padding: 2px 2px 2px 2px;
    border-radius: 2px;
    color: #555555;
    border-radius: 4px;
    border: 1px solid #cccccc;
}
select {
    border: 1px solid #999999;
    padding: 3px;
 }

.GridPageCount
{
	font-size: 16px;
	color: #555555;
	font-family:微軟正黑體;
}

.btnPage_first
{
    background: #f6f6f6 no-repeat 5px center;
    background-image: url("../images/page_first.png"),url("../../images/page_first.png");
    cursor:pointer;
    font-size: 14px;
    font-family:微軟正黑體;
    padding: 2px 5px 2px 21px;
    margin: 0px 0px 0px 0px;
    color: #555555;
    border-radius: 4px;
    text-decoration: none;
    border: 1px solid #cccccc;
}

.btnPage_pre
{
    background: #f6f6f6 no-repeat 5px center;
    background-image: url("../images/page_pre.png"),url("../../images/page_pre.png");
    cursor:pointer;
    font-size: 14px;
    font-family:微軟正黑體;
    padding: 2px 5px 2px 21px;
    margin: 0px 0px 0px 0px;
    color: #555555;
    border-radius: 4px;
    text-decoration: none;
    border: 1px solid #cccccc;
}

.btnPage_next
{
    background: #f6f6f6 no-repeat 55px center;
    background-image: url("../images/page_next.png"),url("../../images/page_next.png");
    cursor:pointer;
    font-size: 14px;
    font-family:微軟正黑體;
    padding: 2px 21px 2px 5px;
    margin: 0px 0px 0px 0px;
    color: #555555;
    border-radius: 4px;
    text-decoration: none;
    border: 1px solid #cccccc;
}

.btnPage_final
{
    background: #f6f6f6 no-repeat 55px center;
    background-image: url("../images/page_final.png"),url("../../images/page_final.png");
    cursor:pointer;
    font-size: 14px;
    font-family:微軟正黑體;
    padding: 2px 21px 2px 5px;
    margin: 0px 0px 0px 0px;
    color: #555555;
    border-radius: 4px;
    text-decoration: none;
    border: 1px solid #cccccc;
}

.GridViewTable
{
	vertical-align: top;
	text-align: center;
}

    </style>
    <link href="/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script src="/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="/jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/jQuery/jquery.blockUI.js" type="text/javascript"></script>
    <script type="text/javascript"> 
        function showDetail(title) {
            $('#div_detail').dialog({
                modal: true,
                height: 600,
                width: 700,
                title: title
            });
        }
</script> 
</head>
<body>
    
    <form id="form1" runat="server">
        
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="True"></asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div id="div_detail" style="display: none; width: 800px; height: 600px;">
                    <asp:Literal ID="Literal_tetail" runat="server"></asp:Literal>
                </div>
                <table align="center" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center" height="3">
                            <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                <tr>
                                    <td align="center" class="tdQueryHead">保單類別：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:DropDownList ID="ddl_P_iINS_TYPE" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_P_iINS_TYPE_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" class="tdQueryHead">保單號碼：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_cINS_NO" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="center" class="tdQueryHead">轄下業務員：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:DropDownList ID="ddl_P_cSAL_ID_S" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" width="85" rowspan="4">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                    </td>
                                    <td align="center" width="35" rowspan="4">
                                        <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="tdQueryHead">保險公司：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:DropDownList ID="ddl_P_cINS_FK" runat="server">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddl_P_cINS_FK_P" runat="server" Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" class="tdQueryHead">要保人姓名：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_cINS_MAN_A" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="center" class="tdQueryHead">要保人身分證：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_cINS_MAN_ID_A" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="tdQueryHead">保單狀態：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:DropDownList ID="ddl_P_cINS_STATUS" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" class="tdQueryHead">被保人姓名：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_cINS_MAN_B" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="center" class="tdQueryHead">被保人身分證：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_cINS_MAN_ID_B" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" class="tdQueryHead">日期類別：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:DropDownList ID="ddl_P_iDATE_TYPE" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="center" class="tdQueryHead">日期(起)：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_dDATE_S" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                        <asp:CalendarExtender ID="txt_P_dDATE_S_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_P_dDATE_S" TodaysDateFormat="yyyy年M月d日"></asp:CalendarExtender>
                                    </td>
                                    <td align="center" class="tdQueryHead">日期(迄)：</td>
                                    <td align="center" class="tdQueryData">
                                        <asp:TextBox ID="txt_P_dDATE_E" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                        <asp:CalendarExtender ID="txt_P_dDATE_E_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_P_dDATE_E" TodaysDateFormat="yyyy年M月d日"></asp:CalendarExtender>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                            Width="120px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                            CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                            CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                            CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                            CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="GridView1" runat="server"
                                AutoGenerateColumns="False" CssClass="GridViewTable"
                                DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                EnableModelValidation="True">
                                <PagerSettings Visible="False" />
                                <Columns>
                                    <asp:TemplateField HeaderText="保單號碼">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName='<%# Eval("保單號碼") %>' CommandArgument='<%# Eval("保單代號") %>' OnClick="LinkButton1_Click"><%# Eval("保單號碼") %></asp:LinkButton>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                SelectCountMethod="GetCount" SelectMethod="DataReader"
                                TypeName="DataInsWork_Policy"></asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        
    </form>
</body>
</html>
