﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm11.aspx.cs" Inherits="tabc_201709.WebForm11" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        table {
            border: 1px solid #000;
            border-collapse: collapse;
        }

        tr, td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <br />
        <asp:Literal ID="Literal_ItemList" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_dtPerMonthFee" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_dtFee" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_dtBack" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_dtProtectAnalsis" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_dtProfit" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_tbHealthy" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_FamilyDetail" runat="server"></asp:Literal>
        <asp:Literal ID="Literal_FamilyAll" runat="server"></asp:Literal>
    </div>
    </form>
</body>
</html>
