﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm12.aspx.cs" Inherits="tabc_201709.WebForm12" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <link href="jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="js/fullcalendar-3.7.0/fullcalendar.min.css" rel="stylesheet" />
    <link href="js/fullcalendar-3.7.0/fullcalendar.print.min.css" rel="stylesheet" media='print' />
    <script src="js/fullcalendar-3.7.0/lib/moment.min.js"></script>
    <script src="js/fullcalendar-3.7.0/fullcalendar.min.js"></script>
    <script src="/js/fullcalendar-3.7.0/locale/zh-tw.js"></script>
    <script>
        $(document).ready(function () {
            $('#calendar').fullCalendar({
                themeSystem: 'jquery-ui',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                eventLimit: true,
                timeFormat: 'HH:mm',
                events: function (start, end, timezone, callback) {
                    $.ajax({
                        url: '/WCF/ScheduleCalendar.asmx/List',
                        success: function (response) {
                            var events = [];

                            var array = response.ListItem;
                            $.each(array, function (i, item) {
                                events.push({
                                    title: item.title,
                                    start: item.start,
                                    end: item.end,
                                    url: item.url,
                                    color: item.color,

                                });
                            });

                            callback(events);
                        }
                    });
                },

            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="calendar"></div>
    </form>
</body>
</html>
