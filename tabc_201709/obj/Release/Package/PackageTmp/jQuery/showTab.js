﻿$(function () {
    var _showTab = 0;
    $('ul.TabTitle li').eq(_showTab).addClass('on');
    $('.TabInfo').hide().eq(_showTab).show();
    $('ul.TabTitle li').click(function () { //mouseover
        var $this = $(this),
                _indexTab = $this.index();
        $this.addClass('on').siblings('.on').removeClass('on');
        $('.TabInfo').eq(_indexTab).stop(false, true).fadeIn().siblings().hide();
        return false;
    });
});