﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="tabc_201709.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                開始：<asp:Label ID="lbl_sTime" runat="server" Text="09:00"></asp:Label>
                &nbsp;<br /> 結束：<asp:Label ID="lbl_eTime" runat="server">10:00</asp:Label>
                &nbsp;<br />
                <asp:HiddenField ID="hif_eTime" runat="server" />
                <br />
                <br />
                <asp:Label ID="lbl_Time" runat="server"></asp:Label>
                <br />
                <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick" Enabled="False">
                </asp:Timer>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
