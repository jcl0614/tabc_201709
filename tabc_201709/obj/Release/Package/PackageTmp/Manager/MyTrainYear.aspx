﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="MyTrainYear.aspx.cs" Inherits="tabc_201709.Manager.MyTrainYear" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="Apply" CssClass="MainHeadTdLink" OnCommand="btn_Command" OnClientClick="return confirm(&quot;確定要執行嗎?&quot;);"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 時數匯總</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">年度：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EY_Year" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">處經理：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_DM" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" BackColor="#7DC44E" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnStyle" OnClick="btnRestQuery_Click" BackColor="#7B7BC0" Text="條件清除" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">單位：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EY_Unit" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">受訓人員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EY_AccID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_my" SelectMethod="DataReader_my"
                                        TypeName="DataETrainYear"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">年度</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_Year" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_Unit" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">處經理編號</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_DM_AccID" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">處經理姓名</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_DM_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">受訓人員編號</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_AccID" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">受訓人員姓名</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">應該訓時數</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_Hours" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">實訓練時數</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_EY_AttHours" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">尚缺時數</td>
                                            <td class="tdData10s" style="background-color: #FBEAD4">
                                                <asp:Label ID="lbl_LackHour" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="padding: 10px; background-color: #FFBD6E">
                                                <asp:Button ID="btnCancel" runat="server" BackColor="#7B7BC0" CommandName="Cancel" CssClass="btnStyle" OnCommand="btn_Command" Text="回查詢清單" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
