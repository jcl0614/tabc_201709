﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="tabc_201709.Manager.Customer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
    <script>
        function showDetail(title, url) {
            $('#div_detail').dialog({
                modal: true,
                width: $(window).width() - 200,
                height: $(window).height() - 50,
                title: title,
                open: function (ev, ui) {
                    $('#iframe_detail').attr('src', url);
                    $('#iframe_detail').height($(window).height() - 130);
                },
                close: function (event, ui) {
                    $('#iframe_detail').attr('src', '');
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 客戶列表</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <div id="div_detail" style="display:none;">
            <iframe id="iframe_detail" src="" width="100%" frameborder="0" height="620"></iframe>
        </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">客戶名稱/姓名：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_CustName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">投保公司：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_ComSName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="5">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="5">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">身分證號：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_ID" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶來源：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustSource" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_PolNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">性別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Sex" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">聯絡方式：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_ContactMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">生效日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc1:DateRange ID="DateRange_PO_AccureDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">投保年齡：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_Age" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">手機號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Mobile" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單備註：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_Polmmemo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">婚姻：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Marry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">地址：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Address" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load" DataSourceID="ObjectDataSource1">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataCustomer"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">客戶名稱/姓名</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_CustName" runat="server" MaxLength="255"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶類別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_CustType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">公司名稱</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_CompName" runat="server" MaxLength="80"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">身分證號</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_ID" runat="server" MaxLength="255"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶等級</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_CustClass" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">部門</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_DepartName" runat="server" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">性別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_Sex" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶來源</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_CustSource" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">職稱</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_CustTitle" runat="server" MaxLength="20"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">出生日期</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_Birth" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_sDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_CU_Birth" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶分類一</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_Category" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">職業</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_JobName" runat="server" MaxLength="120"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">投保年齡</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_CU_Age" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶分類二</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_Category1" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">職業等級</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_JobPA" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">婚姻</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_Marry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">客戶分類三</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_Category2" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">聯絡方式</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_CU_ContactMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">客戶備註</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_CU_CustMemo" runat="server" Rows="5" TextMode="MultiLine" Width="98%" MaxLength="4000"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="6">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_CU_UidNo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="background-color: #FFCC99; font-size: 18px; padding-top: 10px; padding-bottom: 10px; color: #333333;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="33.3%">&nbsp;</td>
                                                        <td align="center" width="33.3%">聯絡資料</td>
                                                        <td align="right" width="33.3%">
                                                            <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="1" CssClass="MainHeadTdLink" ForeColor="#0066CC" OnCommand="lnk_CommandAdd"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增聯絡資料</asp:LinkButton>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!" OnLoad="GridView2_Load" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" OnRowDataBound="GridView2_RowDataBound">
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                <div id="div_CM_edit" style="display: none;">
                                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">聯絡類別</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_CM_CLAS" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_CM_CLAS_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">聯絡次類別</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_CM_TYPE" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="tr_CM_ZIP" runat="server">
                                                            <td align="right" class="MS_tdTitle" width="100">郵遞區號</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CM_ZIP" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">聯絡內容</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CM_DESC" runat="server" Width="300px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td align="center" class="tdMaintainButton" colspan="2">
                                                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command_CM" Text="存檔" />
                                                                    <asp:Button ID="btnEdit0" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command_CM" Text="存檔" />
                                                                    <asp:Button ID="btnDelete0" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command_CM" Text="刪除" />
                                                                    <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command_CM" Text="取消" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2" style="height: 20px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" style="line-height: 125%">
                                                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="hif_CM_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_CM_UidNo" runat="server" />
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="background-color: #FFCC99; font-size: 18px; padding-top: 10px; padding-bottom: 10px; color: #333333;">
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td width="33.3%">&nbsp;</td>
                                                        <td align="center" width="33.3%">家屬資料</td>
                                                        <td align="right" width="33.3%">
                                                            <asp:LinkButton ID="lnkMaintain1" runat="server" CommandName="2" CssClass="MainHeadTdLink" ForeColor="#0066CC" OnCommand="lnk_CommandAdd"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增家屬資料</asp:LinkButton>
                                                            &nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6">
                                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!" OnLoad="GridView3_Load" OnSelectedIndexChanged="GridView3_SelectedIndexChanged" OnRowDataBound="GridView3_RowDataBound">
                                                    <PagerSettings Visible="False" />
                                                </asp:GridView>
                                                <div id="div_FA_edit" style="display: none;">
                                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">關係</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_FA_RelaName" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">姓名</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CU_CustName0" runat="server" MaxLength="255"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">身分證號</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CU_ID0" runat="server" MaxLength="255"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">性別</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_CU_Sex0" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">生日</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CU_Birth0" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                                <asp:CalendarExtender ID="txt_CU_Birth0_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_CU_Birth0" TodaysDateFormat="yyyy年M月d日">
                                                                </asp:CalendarExtender>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">投保年齡</td>
                                                            <td class="tdData10s">
                                                                <asp:TextBox ID="txt_CU_Age0" runat="server" Width="50px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="right" class="MS_tdTitle" width="100">職級</td>
                                                            <td class="tdData10s">
                                                                <asp:DropDownList ID="ddl_CU_JobPA0" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                                <td align="center" class="tdMaintainButton" colspan="2">
                                                                    <asp:Button ID="btnAppend1" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command_FA" Text="存檔" />
                                                                    <asp:Button ID="btnEdit1" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command_FA" Text="存檔" />
                                                                    <asp:Button ID="btnDelete1" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command_FA" Text="刪除" />
                                                                    <asp:Button ID="btnCancel1" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command_FA" Text="取消" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2" style="height: 20px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" style="line-height: 125%">
                                                                                <asp:Label ID="lbl_Msg1" runat="server" CssClass="errmsg12"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="hif_FA_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_FA_UidNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_CU_UidNo0" runat="server" />
                                                                </td>
                                                            </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    

</asp:Content>
