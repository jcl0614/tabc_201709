﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ETm.aspx.cs" Inherits="tabc_201709.Manager.ETm" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<%@ Register src="ascx/selCourse.ascx" tagname="selCourse" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">教材類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EM_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">課程類型：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Category" runat="server" Enabled="False">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="1">數位課程</asp:ListItem>
                                                    <asp:ListItem Value="2">實體課程</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">教材名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EM_Name" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">說明：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EM_Explan" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">檔名：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EM_File" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">相關課程：</td>
                                            <td align="center" class="tdQueryData" colspan="5">
                                                <uc1:selCourse ID="selCourseSh_EC_TransNo" runat="server" OnRefreshButtonClick="selCourse_EC_TransNo_RefreshButtonClick" OnSearchButtonClick="selCourse_EC_TransNo_SearchButtonClick" OnSelectedIndexChanged="selCourse_EC_TransNo_SelectedIndexChanged" OnTextChanged="selCourse_EC_TransNo_TextChanged" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataETm"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">教材類別</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EM_Type" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_EM_Type_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">相關課程</td>
                                            <td class="tdData10s">
                                                <uc1:selCourse ID="selCourse_EC_TransNo" runat="server" OnSelectedIndexChanged="selCourse_EC_TransNo_SelectedIndexChanged" OnTextChanged="selCourse_EC_TransNo_TextChanged" OnSearchButtonClick="selCourse_EC_TransNo_SearchButtonClick" OnRefreshButtonClick="selCourse_EC_TransNo_RefreshButtonClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">教材名稱</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_Name" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">說明</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_Explan" runat="server" Width="98%" MaxLength="500" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_EM_URL" runat="server">
                                            <td align="right" class="MS_tdTitle" width="100">影音連結</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_URL" runat="server" MaxLength="200" Width="300px"></asp:TextBox>
                                                <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5">來源網址：https://vimeo.com/ </span>
                                            </td>
                                        </tr>
                                        <tr id="tr_EM_File" runat="server">
                                            <td align="right" class="MS_tdTitle" width="100">檔案名稱</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_File" runat="server" MaxLength="100" Width="300px" ReadOnly="True"></asp:TextBox>
                                                &nbsp;<asp:Literal ID="Literal_EM_File" runat="server"></asp:Literal>
                                                <br />
                                                <asp:FileUpload ID="FileUpload_EM_File" runat="server" />
                                                <asp:Label ID="lbl_Msg_EM_File" runat="server" CssClass="errmsg12"></asp:Label>
                                                &nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5">檔案格式：<asp:Label ID="lbl_fileType" runat="server"></asp:Label> </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">檔案大小</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_FileSize" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                &nbsp;MB
                                                <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5">可整數或小數2位</span> </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">時間長度</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EM_FileTime" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                &nbsp;分鐘</td>
                                        </tr>
                                        
                                        <tr id="tr_att" runat="server">
                                            <td align="right" class="MS_tdTitle" width="100">附件</td>
                                            <td class="tdData10s">
                                                <asp:Panel ID="Panel_attBtn" runat="server">
                                                    <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="1" CssClass="MainHeadTdLink" ForeColor="#FF9900" OnCommand="lnk_Command_att"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增附件</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBrowse0" runat="server" CommandName="0" CssClass="MainHeadTdLink" ForeColor="#FF9900" OnCommand="lnk_Command_att" Visible="False"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽附件</asp:LinkButton>
                                                    <hr />
                                                    <%--        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>--%>
                                                </asp:Panel>
                                                <asp:MultiView ID="MultiView2" runat="server" ActiveViewIndex="0">
                                                    <asp:View ID="View3" runat="server">
                                                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CssClass="GridViewTable" OnSelectedIndexChanged="GridView2_SelectedIndexChanged" OnRowDataBound="GridView_RowDataBound">
                                                            <PagerSettings Visible="False" />
                                                        </asp:GridView>
                                                    </asp:View>
                                                    <asp:View ID="View4" runat="server">
                                                        <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                            <tr>
                                                                <td align="right" class="MS_tdTitle" width="100">類別</td>
                                                                <td class="tdData10s">
                                                                    <asp:DropDownList ID="ddl_EA_Type" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="MS_tdTitle" width="100">名稱</td>
                                                                <td class="tdData10s">
                                                                    <asp:TextBox ID="txt_EA_Name" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="MS_tdTitle" width="100">說明</td>
                                                                <td class="tdData10s">
                                                                    <asp:TextBox ID="txt_EA_Explan" runat="server" MaxLength="500" Rows="10" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="MS_tdTitle" width="100">檔案名稱</td>
                                                                <td class="tdData10s">
                                                                    <asp:TextBox ID="txt_EA_File" runat="server" MaxLength="100" ReadOnly="True" Width="300px"></asp:TextBox>
                                                                    &nbsp;<asp:Literal ID="Literal_EA_File" runat="server"></asp:Literal>
                                                                    <br />
                                                                    <asp:FileUpload ID="FileUpload_EA_File" runat="server" />
                                                                    <asp:Label ID="lbl_Msg_EA_File" runat="server" CssClass="errmsg12"></asp:Label>
                                                                    &nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">檔案格式：.pdf </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right" class="MS_tdTitle" width="100">檔案大小</td>
                                                                <td class="tdData10s">
                                                                    <asp:TextBox ID="txt_EA_FileSize" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                                    &nbsp;MB
                                                                    <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                                    <span style="color: #D663A5">可整數或小數2位</span> </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" class="tdMaintainButton" colspan="2">
                                                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command_att" Text="存檔" />
                                                                    <asp:Button ID="btnEdit0" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command_att" Text="存檔" />
                                                                    <asp:Button ID="btnDelete0" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command_att" Text="刪除" />
                                                                    <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command_att" Text="取消" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="2" style="height: 20px">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="left" style="line-height: 125%">
                                                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:HiddenField ID="hif_EA_TransNo" runat="server" />
                                                                    <asp:HiddenField ID="hif_EA_UidNo" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 20px" colspan="2">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EM_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_TransNo" runat="server" />
                                            </td>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAppend" />
                <asp:PostBackTrigger ControlID="btnAppend0" />
                <asp:PostBackTrigger ControlID="btnEdit" />
                <asp:PostBackTrigger ControlID="btnEdit0" />
                <asp:PostBackTrigger ControlID="GridView1" />
            </Triggers>
    </asp:UpdatePanel>


</asp:Content>
