﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" validateRequest="false" CodeBehind="SysNews.aspx.cs" Inherits="tabc_201709.Manager.SysNews" %>


<%@ Register src="ascx/ckeditor.ascx" tagname="ckeditor" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">

            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1" 
                CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0" 
                CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">

            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">
                                                分類：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_class" runat="server" Width="100px">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="1">系統公告</asp:ListItem>
                                                    <asp:ListItem Value="2">行事曆</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">
                                                標題文字：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_title" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">
                                                發佈人員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_emNo" runat="server" Width="100px"></asp:TextBox>
                                                &nbsp;(姓名／編號)</td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl" 
                                        width="100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                        CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                        EmptyDataText="查無符合資料!!!" 
                                        onselectedindexchanged="GridView1_SelectedIndexChanged">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                        SelectCountMethod="GetCount" SelectMethod="DataReader" TypeName="DataSysNews">
                                    </asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" 
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="110">
                                                分類</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_class" runat="server" Width="100px">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="1">消息公告</asp:ListItem>
                                                    <asp:ListItem Value="2">行事曆</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="110">
                                                標題文字</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_title" runat="server" MaxLength="50" Width="500px"></asp:TextBox>
                                                <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5; letter-spacing: 1px;">長度最多50個字元</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="110">
                                                內容</td>
                                            <td class="tdData10s">
                                                <uc1:ckeditor ID="ckeditor1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="110">
                                                發佈日期</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_addDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="110">
                                                發佈人員</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_name" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hf_id" runat="server" />
                                                <asp:HiddenField ID="hf_emNo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" 
                                                    CssClass="btnAppend" oncommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" 
                                                    oncommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" 
                                                    CssClass="btnDelete" onclientclick="return confirm(&quot;確定要刪除嗎?&quot;);" 
                                                    oncommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" 
                                                    CssClass="btnCancel" oncommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>

</asp:Content>
