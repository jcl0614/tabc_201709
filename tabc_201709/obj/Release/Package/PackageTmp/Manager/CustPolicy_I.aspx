﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="CustPolicy_I.aspx.cs" Inherits="tabc_201709.Manager.CustPolicy_I" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc1" %>
<%@ Register src="ascx/selCustomer0.ascx" tagname="selCustomer0" tagprefix="uc2" %>
<%@ Register src="ascx/selCustomer.ascx" tagname="selCustomer" tagprefix="uc3" %>
<%@ Register Src="~/Manager/ascx/selPdt.ascx" TagPrefix="uc1" TagName="selPdt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
    <script>
        function showDetail(title, url) {
            $('#div_detail').dialog({
                modal: true,
                width: $(window).width() - 200,
                height: $(window).height() - 100,
                title: title,
                open: function (ev, ui) {
                    $('#iframe_detail').attr('src', url);
                    $('#iframe_detail').height($(window).height() - 180);
                },
                close: function (event, ui) {
                    $('#iframe_detail').attr('src', '');
                }
            });
        }
    </script>
    <style>
        .PopMemo{
            width: 500px;
            padding: 10px;
            border: 3px solid #EAEAEA;
            background: #FFFFCC;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 保單列表</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <div id="div_detail" style="display:none;">
            <iframe id="iframe_detail" src="" width="100%" frameborder="0" height="620"></iframe>
        </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable" id="QueryMainTable" runat="server">
                                        <tr>
                                            <td align="center" class="tdQueryHead">客戶名稱/姓名：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_CustName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">投保公司：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_ComSName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="5">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="5">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">身分證號：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_ID" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">客戶來源：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_CustSource" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_PolNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">性別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Sex" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">聯絡方式：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_ContactMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">生效日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc1:DateRange ID="DateRange_PO_AccureDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">投保年齡：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_CU_Age" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">手機號碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Mobile" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">保單備註：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PO_Polmmemo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">婚姻：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_CU_Marry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">地址：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_Address" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load" DataSourceID="ObjectDataSource1">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataCustIndustPolm"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">保單號碼</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Polno" runat="server" MaxLength="255"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">投保公司</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_ComSName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">要保人</td>
                                            <td class="tdData10s" width="16%">
                                                <uc3:selCustomer ID="selCustomer1" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">投保日期</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_StartDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_sDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_IP_StartDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">到期日期</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_EndDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_IP_EndDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_IP_EndDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">投保期間(年)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Insuterm" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">保險費(元)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Modeprem" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;元</td>
                                            <td class="MS_tdTitle" width="16%">強制險</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:CheckBox ID="cbox_IP_PdtCoactive" runat="server" Text="有" />
                                            </td>
                                            <td class="MS_tdTitle" width="16%">任意險</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:CheckBox ID="cbox_IP_PdtArbitrariness" runat="server" Text="有" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">投保險種</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:DropDownList ID="ddl_IP_ProdKind" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_IP_ProdKind_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="tr_car_1" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">身分</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Insnature" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">原始發照日</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_IssueDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_IP_IssueDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_IP_IssueDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">製造年份(西元)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_ProductDate" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_car_2" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">車主</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_CarOwner" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">車型</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_CarModel" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">排氣量(c.c.)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Carcc" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="tr_car_3" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">車輛種類</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Cartype" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">牌照號碼</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_PlateNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">乘載限制(人/噸)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_LoadLimit" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_car_4" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">車輛廠牌</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Millnam" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">引擎號碼</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_EngineNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">購買車價(萬)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_CarPrice" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;萬</td>
                                        </tr>
                                        <tr id="tr_house_1" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">屋主</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_CarOwner2" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">權狀面積(坪)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Archar" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;坪</td>
                                            <td class="MS_tdTitle" width="16%">借款人</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Borrower" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr_house_2" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">建物結構</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Buildconst" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">總樓層數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Totfloor" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">貸款戶貸款別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Banktype" runat="server">
                                                </asp:DropDownList>
                                                &nbsp;<font style="color: #D663A5">
                                                    <asp:Label ID="lbl_tip" runat="server" Text='<img alt="" src="/manager/images/help.gif" /> 說明' Font-Underline="True"></asp:Label></font>
                                                    <asp:HoverMenuExtender ID="lbl_tip_HoverMenuExtender" runat="server" DynamicServicePath="" Enabled="True" PopupControlID="Panel_memo" PopupPosition="Left" TargetControlID="lbl_tip">
                                                </asp:HoverMenuExtender>
                                                <asp:Panel ID="Panel_memo" runat="server" CssClass="PopMemo" style="display:none;">
                                                    <p>『新貸（貸款戶）』－係指被保險人之房屋首次向金融機構（含產、壽險公司）申請貸款，而投保住宅地震基本保險者。</p>
                                                    <p>例如：第一次向某銀行申請貸款而填寫住宅地震基本保險要保書。</p>
                                                    <p>『增貸／續貸（貸款戶）』－係指被保險人之房屋已向金融機構（含產、壽險公司）申請貸款，又向原金融機構（含產、壽險公司）申請增加貸款額度者；或被保險人之房屋已向金融機構（含產、壽險公司申請貸款，因所投保之住宅地震基本保險到期而繼續投保該保險者（貸款狀態仍持續）。</p>
                                                    <p>例如：先前已向甲銀行申請貸款，又以原房屋向甲銀行申請增加貸款額度；或先前已向甲銀行申請貸款，因所投保之住宅地震基本保險到期而繼續投保該保險，而填寫住宅地震基本保險要保書。</p>
                                                    <p>『轉貸（貸款戶）』－係指被保險人之房屋已向甲金融機構（含產、壽險公司）申請貸款，又向乙金融機構（含產、壽險公司）申請貸款，而投保住宅地震基本保險者。</p>
                                                    <p>例如：先前已向甲金融機構（含產、壽險公司）申請貸款，同時又以原房屋轉向乙金融機構（含產、壽險公司）申請貸款，而填寫住宅地震基本保險要保書。</p>
                                                </asp:Panel>

                                                </td>
                                        </tr>
                                        <tr id="tr_house_3" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">建造年份(民國)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Archyy" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">裝潢總價(萬)</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_IP_Decoration" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;萬</td>
                                            <td class="MS_tdTitle" width="16%">非貸款戶貸款別</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:DropDownList ID="ddl_IP_Banktype2" runat="server">
                                                </asp:DropDownList>
                                                &nbsp;<font style="color: #D663A5">
                                                    <asp:Label ID="lbl_tip2" runat="server" Text='<img alt="" src="/manager/images/help.gif" /> 說明' Font-Underline="True"></asp:Label></font>
                                                    <asp:HoverMenuExtender ID="HoverMenuExtender1" runat="server" DynamicServicePath="" Enabled="True" PopupControlID="Panel_memo2" PopupPosition="Left" TargetControlID="lbl_tip2">
                                                </asp:HoverMenuExtender>
                                                <asp:Panel ID="Panel_memo2" runat="server" CssClass="PopMemo" style="display:none;">
                                                    <p>『新保（非貸款戶）』－係指被保險人之房屋於投保住宅地震基本保險時並無向金融機構（含產、壽險公司）申請貸款，而首次投保住宅地震基本保險者。</p>
                                                    <p>『續保（非貸款戶）』指該保險標的曾經在保險公司投保過住宅地震險，不論曾在其他保險公司投保或曾經斷保過，均納為續保。</p>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr id="tr_house_4" runat="server" visible="false">
                                            <td align="right" class="MS_tdTitle" width="20%">建物地址</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_IP_Targetzip" runat="server" Width="30px" style="text-align:center"></asp:TextBox>
                                                &nbsp;<asp:TextBox ID="txt_IP_Targetaddr" runat="server" MaxLength="80" Width="300px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">備註</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_IP_Memo" runat="server" MaxLength="4000" Rows="5" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="6">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_IP_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_CU_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_CU_UidNo" runat="server" />
                                            </td>
                                        </tr>
                                                                                
                                        </tr>
                        </table>
                        
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>

            
        </ContentTemplate>
    </asp:UpdatePanel>
    

</asp:Content>
