﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" validateRequest="false" CodeBehind="PdCompare.aspx.cs" Inherits="tabc_201709.Manager.PdCompare" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register src="ascx/ckeditor.ascx" tagname="ckeditor" tagprefix="uc1" %>
<%@ Register Src="~/Manager/ascx/UpdateProgress.ascx" TagPrefix="uc1" TagName="UpdateProgress" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
<script>

    var isIE = function (ver) {
        var b = document.createElement('b')
        b.innerHTML = '<!--[if IE ' + ver + ']><i></i><![endif]-->'
        return b.getElementsByTagName('i').length === 1
    }
    function printScreen(html, title) {
        var printPage = window.open("", title, "");
        printPage.document.open();
        var ua = window.navigator.userAgent;
        if (ua.indexOf('.NET ') != -1 || ua.indexOf('MSIE ') != -1 || ua.indexOf('Explorer ') != -1 || ua.indexOf('Edge ') != -1) {
            printPage.document.write("<OBJECT classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2' height=0 id=wc name=wc width=0></OBJECT>");
            printPage.document.write("<HTML><head></head><BODY onload='javascript:wc.execwb(7,1);window.close()'>");
        }
        else {
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
        }
        printPage.document.write("<PRE>");
        printPage.document.write(html);
        printPage.document.write("</PRE>");
        printPage.document.close("</BODY></HTML>");
    }
    </script>
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <style>
        .tittle01 {
    font-family: "微軟正黑體";
    font-size: 20px;
    line-height: 25px;
    font-weight: normal;
    /*color: #27A0B6;*/
    color: #F0641C;
    text-decoration: none;
    background-image: url(images/icon04.gif);
    background-repeat: no-repeat;
    background-position: left center;
    padding-top: 0px;
    padding-right: 0px;
    padding-bottom: 0px;
    padding-left: 12px;
}
.text02 {
    font-family: Arial, Helvetica, sans-serif;
    /*font-size: 13px;*/
    font-size: 14px;
    line-height: 30px;
    color: #333333;
    text-decoration: none;
}

td.text02 {
    /*padding-left: 4px;
    padding-right: 4px;
    font-size: 13px;*/
    padding-left: 3px;
    padding-right: 3px;
    font-size: 14px;
}
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <uc1:UpdateProgress runat="server" ID="UpdateProgress" />
            <asp:Panel ID="Panel_Form" runat="server">

                <asp:Panel ID="Panel1" runat="server">
               <div class="tab-content">

                 <table cellpadding="0" cellspacing="0" class="QueryMainTable" style="font-size: 14px; line-height: 20px;">
                <tbody><tr>
                    <td align="center" class="tdQueryHead">性別</td>
                    <td>
                        <asp:RadioButtonList ID="rbl_sex" runat="server" 
                            RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True">男</asp:ListItem>
                            <asp:ListItem >女</asp:ListItem>
                        </asp:RadioButtonList>                        
                    </td>
                    <td align="center" class="tdQueryHead">生日 </td>
                    <td style="text-align: left; vertical-align: middle;">&nbsp;
                                                            <asp:TextBox ID="txt_birth" runat="server" MaxLength="10" Width="85px"></asp:TextBox>
                                        <asp:CalendarExtender ID="txt_birth_CalendarExtender" runat="server" 
                                                    DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" 
                                                    TargetControlID="txt_birth" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                    </td>
                    <td align="center" class="tdQueryHead">職業等級</td>
                    <td>
                   <asp:DropDownList ID="ddljob1" runat="server" >
                                                    <asp:ListItem Selected="True" Value="1">1</asp:ListItem>
                                                    <asp:ListItem Value="2">2</asp:ListItem>
                                                    <asp:ListItem Value="3">3</asp:ListItem>
                                                    <asp:ListItem Value="4">4</asp:ListItem>
                                                    <asp:ListItem Value="5">5</asp:ListItem>
                                                    <asp:ListItem Value="6">6</asp:ListItem>
                   </asp:DropDownList>
                    </td>
                   <td align="center" width="85">
                     <asp:Button ID="tab1" runat="server" CssClass="btnSearch"  Text="險種選擇" 
                           onclick="tab1_Click" />
                   </td>
                   <td align="center" width="85">
                     <asp:Button ID="tab2" runat="server" CssClass="btnCompare"  Text="開始比較" 
                           onclick="tab2_Click" />
                   </td>
                   <td align="center" width="85">
                     <asp:Button ID="tab3" runat="server" CssClass="btnPrint"  Text="列印報表" 
                           onclick="tab3_Click1" />
                   </td>
                </tr>
            </tbody>
                 </table>
                </div>
                </asp:Panel>

                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" height="3">
                                   <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">
                                                公司：</td>
                                            <td align="center" class="tdQueryData">
                                               <asp:DropDownList ID="ddlSh_ComKind" runat="server" AutoPostBack="True" 
                                               onselectedindexchanged="ddlSh_ComKind_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True" Value="0">壽險</asp:ListItem>
                                                    <asp:ListItem Value="1">產險</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlSh_Company" runat="server" >
                                                </asp:DropDownList>
                                                <asp:CheckBox ID="chk_stop" runat="server" Text="含停售" />
                                            </td>
                                            <td align="center" class="tdQueryHead">
                                                險種代號：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PdtNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">
                                                險隀關鍵字：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_KeyWord" runat="server" Width="100px"></asp:TextBox>
                                                &nbsp;</td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                            <td>
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                            <td width="20%" height="100%">

                                 <asp:Panel ID="Panel2" runat="server" GroupingText="類型" Height="100%">
                                     <asp:RadioButtonList ID="rblSh_Type" runat="server">
                                     </asp:RadioButtonList>
                                 </asp:Panel>
                                                             </td>
                            <td width="60%">

                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                               <tr>

                                  <td align="center" height="3"width="60%">
                                     <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl" 
                                       width="100%" >
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                </tr>
                                 <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                        CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                        EmptyDataText="查無符合資料!!!" 
                                        onselectedindexchanged="GridView1_SelectedIndexChanged">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                        SelectCountMethod="GetCount" SelectMethod="DataReader" 
                                        TypeName="DataPdCompare">
                                    </asp:ObjectDataSource>
                                </td>
                                 </tr>
                                 <tr>
                                 <td>
                                 <table style="width: 100%; font-size: 14px; margin: 10px;">
                                    <tbody><tr>
                                        <td style="font-size: 18px; vertical-align: middle;"><span>保額:</span><asp:TextBox 
                                                ID="txt_Amtinput" style="width: 100px; text-align: right;" value="1" 
                                                runat="server" ></asp:TextBox>
                                            <asp:DropDownList ID="ddl_Amtinput" runat="server"   AutoPostBack="True"
                                                onselectedindexchanged="ddl_Amtinput_SelectedIndexChanged" Visible="False">
                                            </asp:DropDownList>
                                            <asp:label id="pdtUnit" runat="server"></asp:label>&nbsp;&nbsp;
                                            保費:<asp:TextBox ID="txt_ModePrem" style="width: 100px; text-align: right;" 
                                                runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        <td rowspan="2" style="text-align: center;">
                                                             <asp:Button ID="btn_ok" runat="server" CssClass="btnAppend" 
                                                                 class="btn btn-sm btn-primary" Text="加入" onclick="btn_ok_Click" 
                            />
                                            &nbsp;&nbsp;
                                            <asp:Button ID="btn_close" runat="server" CssClass="btnExcute" Text="完成查詢" 
                                                                 class="btn btn-sm  btn-danger" onclick="btn_close_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 18px"><strong>投保限制:</strong><strong><asp:Literal 
                                                ID="InsLimit" runat="server"></asp:Literal>
                                            </strong>
                                            <asp:HiddenField ID="HF_InsName" runat="server" />
                                            <asp:HiddenField ID="HF_DispMark" runat="server" />
                                            <asp:HiddenField ID="HF_Mark" runat="server" />
                                            <asp:HiddenField ID="HF_DispClass" runat="server" />
                                            <asp:HiddenField ID="HF_CompanyNo" runat="server" />
                                            <asp:HiddenField ID="HF_MinVal" runat="server" />
                                            <asp:HiddenField ID="HF_MaxVal" runat="server" />
                                            </td>
                                    </tr>
                                                                            <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                </tbody></table>
                                 </td>
                                 </tr>
                              </table>
                              </td>
                             <td width="20%" height="100%">
                                    <asp:Panel ID="Panel3" runat="server" GroupingText="年期" Height="100%">
                                         <asp:RadioButtonList ID="rblYear_list" runat="server">
                                         </asp:RadioButtonList>
                                     </asp:Panel>
                                     </td>
                             </tr>
                              </table>
                            </td>
                            </tr>
                         </table>
                    </asp:View>
                </asp:MultiView>
                    <asp:MultiView ID="MultiView2" runat="server">
                    <asp:View ID="View2" runat="server">
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                 <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                                        CssClass="GridViewTable" 
                                        onselectedindexchanged="GridView2_SelectedIndexChanged" 
                                        onrowdatabound="GridView_RowDataBound" onrowdeleting="GridView2_RowDeleting">
                                        <HeaderStyle BackColor="#BCE9F5" BorderStyle="Inset" BorderWidth="2px" 
                                            Height="30px" />
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                </td>
                                 </tr> 
                               </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <asp:Panel ID="Panel4" runat="server">
                            <asp:Button ID="Button1" runat="server" Text=" 分析保障" onclick="Button1_Click" />
                            <asp:Button ID="Button2" runat="server" Text="保障圖表" onclick="Button2_Click" />
                            <asp:Button ID="Button3" runat="server" Text="其他保障" onclick="Button3_Click" />
                        </asp:Panel>
                        <asp:MultiView ID="MultiView3" runat="server">
                            <asp:View ID="View4" runat="server">
                            <div id="subrow1" style="display: block;">
                            <table style="display: none;width: 100%">
                                <tbody>
                                    <tr>
                                        <td class="table01">
                                            <input type="checkbox" id="chk_showoption" checked="checked"><span class="text01">&nbsp;&nbsp;只顯示有保障項目</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                                <asp:Literal ID="Ltl_CompareResult" runat="server"></asp:Literal>
                         </div>
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                            <div id="subrow2" >
                             <asp:Literal ID="Ltl_graph" runat="server"></asp:Literal>
                        </div>
                            </asp:View>
                            <asp:View ID="View6" runat="server">
                            <div id="subrow3" >
                            <asp:Literal ID="Ltl_rider" runat="server"></asp:Literal>
                        </div>
                            </asp:View>
                        </asp:MultiView>

                    </asp:View>
                  </asp:MultiView>
          </asp:Panel>
                </ContentTemplate>

        </asp:UpdatePanel>
</asp:Content>
