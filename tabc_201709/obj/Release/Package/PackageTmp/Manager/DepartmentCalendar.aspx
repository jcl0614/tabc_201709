﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="DepartmentCalendar.aspx.cs" Inherits="tabc_201709.Manager.DepartmentCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4; height: 50px;" width="100%">
                                    <tr>
                                        <td align="right">開課單位：</td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSh_EC_Unit" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right">年度：</td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSh_year" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right">月份：</td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlSh_Month" runat="server">
                                                <asp:ListItem Value="1">1 月</asp:ListItem>
                                                <asp:ListItem Value="2">2 月</asp:ListItem>
                                                <asp:ListItem Value="3">3 月</asp:ListItem>
                                                <asp:ListItem Value="4">4 月</asp:ListItem>
                                                <asp:ListItem Value="5">5 月</asp:ListItem>
                                                <asp:ListItem Value="6">6 月</asp:ListItem>
                                                <asp:ListItem Value="7">7 月</asp:ListItem>
                                                <asp:ListItem Value="8">8 月</asp:ListItem>
                                                <asp:ListItem Value="9">9 月</asp:ListItem>
                                                <asp:ListItem Value="10">10 月</asp:ListItem>
                                                <asp:ListItem Value="11">11 月</asp:ListItem>
                                                <asp:ListItem Value="12">12 月</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="center">
                                            <asp:LinkButton ID="lbtn_AttendPersonal" runat="server" BackColor="#7DC44E" CommandName="search" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="">查詢</asp:LinkButton>
                                        </td>
                                        <td align="center">
                                            <asp:LinkButton ID="lbtn_Back" runat="server" BackColor="#7B7BC0" CommandName="reset" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="">條件清除</asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding-top: 5px; padding-bottom: 5px">
                                <asp:Label ID="lbl_title" runat="server" Font-Size="18px" ForeColor="#366092"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="8" Font-Size="12px" ForeColor="#333333" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:BoundField DataField="Day" HeaderText="日期">
                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Week" HeaderText="星期">
                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="歸屬單位課程">
                                            <ItemTemplate>
                                                <ul>
                                                    <asp:Repeater ID="Repeater1" runat="server">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="lbtn_Course_Unit" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="lbtn_Command"><%# Eval("EC_CName") %></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="200px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="單位區部課程">
                                            <ItemTemplate>
                                                <ul>
                                                    <asp:Repeater ID="Repeater2" runat="server">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="lbtn_Course_Unit0" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="lbtn_Command"><%# Eval("EC_CName") %></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="200px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="總公司課程">
                                            <ItemTemplate>
                                                <ul>
                                                    <asp:Repeater ID="Repeater3" runat="server">
                                                        <ItemTemplate>
                                                            <li>
                                                                <asp:LinkButton ID="lbtn_Course_Unit1" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="lbtn_Command"><%# Eval("EC_CName") %></asp:LinkButton>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="200px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Memo" HeaderText="備註" HtmlEncode="False">
                                        <HeaderStyle HorizontalAlign="Center" Width="250px" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#745741" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                    <SortedDescendingHeaderStyle BackColor="#820000" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                        <tr>
                            <td align="left" style="background-color: #FFBD6E">
                                <table cellpadding="0" width="100%">
                                    <tr>
                                        <td align="center" style="padding: 20px 10px 20px 10px">
                                            <asp:Label ID="lbl_" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C">行事曆-明細</asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="120">日期</td>
                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                            <asp:Label ID="lbl_EC_SDate" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                            <asp:Label ID="lbl_EC_CName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="120">講師</td>
                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                            <asp:Label ID="lbl_ET_TName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="120">主持人</td>
                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                            <asp:Label ID="lbl_EC_Moderator" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="MS_tdTitle" width="120">備註</td>
                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                            <asp:Label ID="lbl_EC_Memo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                <asp:LinkButton ID="lbtn_Back8" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
