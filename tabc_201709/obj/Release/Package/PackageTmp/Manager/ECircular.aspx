﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ECircular.aspx.cs" Inherits="tabc_201709.Manager.ECircular" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>


<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkMaintain0" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 設定人員</asp:LinkButton>
            <br />
            <asp:LinkButton ID="lnkExcelSet" runat="server" CssClass="MainHeadTdLink" CommandName="1" OnCommand="export_Command"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 年度在職教育訓練匯出</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelSet1" runat="server" CssClass="MainHeadTdLink" CommandName="2" OnCommand="export_Command"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 年金銷售資格匯出</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelSet2" runat="server" CssClass="MainHeadTdLink" CommandName="3" OnCommand="export_Command"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 年度防制洗錢教育訓練匯出</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:PostBackTrigger ControlID="lnkExcelSet" />
            <asp:PostBackTrigger ControlID="lnkExcelSet1" />
            <asp:PostBackTrigger ControlID="lnkExcelSet2" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">申請日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EI_AppDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">教育訓練類型：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EI_Mode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">職稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EI_Title" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">業務員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EI_Name" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataECircular"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">教育訓練類型</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EI_Mode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">申請日期</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EI_AppDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_sDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EI_AppDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">業務員</td>
                                            <td class="tdData10s">
                                                <uc3:selPerson_AccID ID="selPerson_AccID_EI_AccID" runat="server" OnRefreshButtonClick="selPerson_AccID_EI_AccID_RefreshButtonClick" OnSearchButtonClick="selPerson_AccID_EI_AccID_SearchButtonClick" OnSelectedIndexChanged="selPerson_AccID_EI_AccID_SelectedIndexChanged" OnTextChanged="selPerson_AccID_EI_AccID_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">職級</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_PS_Title" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">地區</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_TYPE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_NAME" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">處經理</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_DN_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">職稱</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EI_Title" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">完訓日期</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EI_CompDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_EI_CompDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EI_CompDate" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">分數</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EI_Score" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EI_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EI_AccID" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">教育訓練類型</td>
                                <td class="tdData10s">
                                    <asp:DropDownList ID="ddl_EI_Mode0" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">申請日期</td>
                                <td class="tdData10s">
                                    <asp:TextBox ID="txt_EI_AppDate0" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                    <asp:CalendarExtender ID="txt_EI_AppDate0_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EI_AppDate0" TodaysDateFormat="yyyy年M月d日">
                                    </asp:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">職稱</td>
                                <td class="tdData10s">
                                    <asp:DropDownList ID="ddl_EI_Title0" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">完訓日期</td>
                                <td class="tdData10s">
                                    <asp:TextBox ID="txt_EI_CompDate0" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                    <asp:CalendarExtender ID="txt_EI_CompDate0_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EI_CompDate0" TodaysDateFormat="yyyy年M月d日">
                                    </asp:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="120">分數</td>
                                <td class="tdData10s">
                                    <asp:TextBox ID="txt_EI_Score0" runat="server" Width="50px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel_Sort" runat="server">
                                        <ContentTemplate>
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">
                                                        <br />
                                                        <asp:TextBox ID="txtSh_AccID" runat="server"></asp:TextBox>
                                                        &nbsp;<asp:Button ID="btn_searchAccID" runat="server" CommandName="search" OnCommand="btnApply_Command" Text="人員查詢" />
                                                    </td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">設定人員</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                                    </td>
                                                    <td align="center">
                                                        <p>
                                                            <asp:ImageButton ID="ibtnRefresh" runat="server" CommandName="refresh" ImageUrl="~/Manager/images/Refresh-icon.png" OnCommand="btnApply_Command" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="btnAddAll" runat="server" CommandName="addAll" CssClass="btnAddAll" OnCommand="btnApply_Command" Text="全部加入" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="btnRemoveAll" runat="server" CommandName="removeAll" CssClass="btnRemoveAll" OnCommand="btnApply_Command" Text="全部移除" />
                                                            <br />
                                                            <br />
                                                            <asp:ImageButton ID="ibtnRight" runat="server" CommandName="add" ImageUrl="~/Manager/images/Forward-icon.png" OnCommand="btnApply_Command" />
                                                            <br />
                                                            <br />
                                                            <asp:ImageButton ID="ibtnLeft" runat="server" CommandName="remove" ImageUrl="~/Manager/images/Back-icon.png" OnCommand="btnApply_Command" />
                                                        </p>
                                                    </td>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                    <td align="center"></td>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="tdMaintainButton" colspan="2">
                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Append" CssClass="btnAppend" OnClientClick="return confirm(&quot;確定要設定新增通報資料嗎?&quot;);" OnCommand="btnApply_Command" Text="產生通報資料" />
                                    <asp:Button ID="btnCancel0" runat="server" CommandName="cancel" CssClass="btnCancel" OnCommand="btnApply_Command" Text="取消" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="height: 20px">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" style="line-height: 125%">
                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
