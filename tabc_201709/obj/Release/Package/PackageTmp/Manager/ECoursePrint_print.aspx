﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ECoursePrint_print.aspx.cs" Inherits="tabc_201709.Manager.ECoursePrint_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        
        <table align="center" cellpadding="0" cellspacing="0" style="font-family: 微軟正黑體">
            <tr>
                <td style="padding-top: 5px; padding-bottom: 5px; font-size: 18px; padding-left: 10px;">
                    <span style="font-family: Arial, Helvetica, sans-serif">◎</span> 主題：<%= EC_CName %>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px; padding-bottom: 5px; font-size: 18px; padding-left: 10px;">
                    <span style="font-family: Arial, Helvetica, sans-serif">◎</span> 講師：<%= ET_TNam %>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px; padding-bottom: 5px; font-size: 18px; padding-left: 10px;">
                    <span style="font-family: Arial, Helvetica, sans-serif">◎</span> 時間：<%= EC_SDate %>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px; padding-bottom: 5px; font-size: 18px; padding-left: 10px;">
                    <span style="font-family: Arial, Helvetica, sans-serif">◎</span> 地點：<%= EC_Place %>
                </td>
            </tr>
            <tr>
                <td style="padding-top: 5px">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="NO.">
                                <ItemTemplate><%#Container.DataItemIndex + 1%></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="業務員編號">
                                <ItemTemplate><%# Eval("ED_AccID") %></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="單位">
                                <ItemTemplate><%# Eval("cZON_PK") %></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="姓名">
                                <ItemTemplate><%# Eval("ED_Name") %></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="處經理">
                                <ItemTemplate><%# Eval("DM_NAME") %></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="午餐">
                                <ItemTemplate><%# Eval("ED_EatHabits") %></ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="簽到">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle Width="150px" />

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="備註">
                                <HeaderStyle HorizontalAlign="Center" Font-Bold="False" />
                                <ItemStyle Width="80px" />

                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Size="15px" Height="30px" />
                        <RowStyle Font-Size="18px" Height="24px" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
        
    </form>
</body>
</html>
