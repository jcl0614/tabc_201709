﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_Open.master" AutoEventWireup="true" CodeBehind="CustomerImport.aspx.cs" Inherits="tabc_201709.Manager.CustomerImport" %>

<%@ Register Src="ascx/selTeacher.ascx" TagName="selTeacher" TagPrefix="uc1" %>

<%@ Register Src="ascx/DateRange.ascx" TagName="DateRange" TagPrefix="uc2" %>

<%@ Register Src="ascx/selRegion.ascx" TagName="selRegion" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <script>
        $(function () {

            $("#<%= btn_Import.ClientID%>").click(function () {
                if (confirm('確定要執行嗎？')) {
                    $("#<%= btn_Import.ClientID%>").hide();
                    $("#loading").show();
                    $("#ImportMsg").html('');
                    $("#ImportMsg").prepend('資料處理中...<br>');

                    Import(1, -1);

                }
            });

        });
        function Import(i, index) {
            $.ajax({
                type: "post",
                url: "/WCF/CustomerImport.asmx/Import",
                data: {
                    i: i,
                    index: index
                },
                success: function (response) {
                    if (response.result) {
                        if (!response.i) {
                            $("#<%= btn_Import.ClientID%>").show();
                            $("#loading").hide();
                            $("#progress").text('');
                            $("#progress").width(0);
                            alert("執行完成！");
                        }
                        else {
                            Import(response.i, response.index);
                        }

                        $("#ImportMsg").prepend(response.result);
                        if (response.ExceptionMsg)
                            $("#ImportMsg").prepend(response.ExceptionMsg + '<br>');
                        if (response.progress_val) {
                            $("#progress_msg").text(response.progress_msg);
                            $("#progress_bar").animate({ width: response.progress_val * 500 });
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#<%= btn_Import.ClientID%>").show();
                    $("#loading").hide();
                    alert('執行中止！\n例外中止訊息：' + xhr.responseText);
                }
            });
        }
    </script>

    <table border="0" cellpadding="2" cellspacing="1" width="100%">

        <tr>
            <td align="left" style="padding-top: 5px; padding-bottom: 5px">
                <input id="btn_Import" runat="server" type="button" value="客戶資料匯入" style="cursor: pointer" class="btnExport" />
                <div id="loading" style="display: none;">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Manager/images/loading.gif" />
                            </td>
                            <td width="5">&nbsp;</td>
                            <td>
                                <asp:Label ID="lbl_msg0" runat="server" Font-Size="Small" ForeColor="#2C6A9C" Text="執行中．．．"></asp:Label>
                            </td>
                            <td style="border: 1px solid #dbdbdb; position: relative; background-color: #efefef; width: 500px;">
                                <div id="progress_msg" style="font-family: 微軟正黑體; font-size: 14px; color: #333333; text-align: center; width: 500px; padding-top: 5px; padding-bottom: 5px; position: absolute; top: 0px; z-index: 2;">0%</div>
                                <div id="progress_bar" style="font-family: 微軟正黑體; font-size: 14px; background-position: bottom; background-repeat: repeat-x; background-image: url('/Manager/images/bg_tab.png'); color: #FFFFFF; text-align: center; padding-top: 5px; padding-bottom: 5px; width: 0px; position: absolute; top: 0px;">　</div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td align="left" style="padding-top: 5px; padding-bottom: 5px">
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" __designer:mapid="22e" />
                            <font style="color: #D663A5; font-size: small;">匯入執行中請勿關閉視窗；匯入將覆寫更新原始資料</font></td>
        </tr>
        <tr>
            <td align="left">
                <div id="ImportMsg" style="font-size: 14px; font-family: 微軟正黑體; height: 600px; overflow: auto;">
                </div>
            </td>
        </tr>
    </table>


</asp:Content>
