﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="Business.aspx.cs" Inherits="tabc_201709.Manager.Business" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/UpdateProgress.ascx" tagname="updateprogress" tagprefix="uc1" %>
<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>
<%@ Register src="ascx/DateTimeRange.ascx" tagname="DateTimeRange" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
    <style>
        .tdQueryData input[type="radio"]{
            width: 20px;
            height: 20px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_detail" style="display: none; width: 800px; height: 600px;">
                <asp:Literal ID="Literal_tetail" runat="server"></asp:Literal>
            </div>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="1200">
                <tr>
                    <td align="center" height="3">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" class="tdQueryData">日期：<uc2:DateRange ID="DateRange1" runat="server" />
                                </td>
                                <td align="center" width="85" rowspan="2">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                </td>
                                <td align="center" width="35" rowspan="2">
                                    <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="tdQueryData">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Font-Size="12px" ForeColor="#CC6600" CellPadding="10">
                                        <asp:ListItem Value="0" Selected="True"><span><img src="images/other/生日關懷.jpg" width="50" height="50" /><br />　　生日關懷</span></asp:ListItem>
                                        <asp:ListItem Value="1"><span><img src="images/other/增齡通知.jpg" width="50" height="50" /><br />　　增齡通知</span></asp:ListItem>
                                        <asp:ListItem Value="2"><span><img src="images/other/繳費通知.jpg" width="50" height="50" /><br />　　繳費通知</span></asp:ListItem>
                                        <asp:ListItem Value="3"><span><img src="images/other/保單週年服務.jpg" width="50" height="50" /><br />　　保單週年服務</span></asp:ListItem>
                                        <asp:ListItem Value="4"><span><img src="images/other/產險傷害險到期.jpg" width="50" height="50" /><br />　　產險傷害險到期</span></asp:ListItem>
                                        <asp:ListItem Value="5"><span><img src="images/other/汽機車險到期.jpg" width="50" height="50" /><br />　　汽機車險到期</span></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:UpdateProgress runat="server" ID="UpdateProgress" />
                        <asp:GridView ID="GridView1" runat="server"
                            AutoGenerateColumns="False" CssClass="GridViewTable"
                            EmptyDataText="查無符合資料!!!"
                            EnableModelValidation="True" OnRowDataBound="GridView1_RowDataBound">
                            <PagerSettings Visible="False" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="<%#Container.DisplayIndex + 1%>" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="行程日期">
                                    <ItemTemplate>
                                        <uc3:DateTimeRange ID="DateTimeRange1" runat="server" />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="類型／計畫">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddl_SC_Kind" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_SC_Kind_SelectedIndexChanged" DataSourceID="ObjectDataSource1" DataTextField="TypeName" DataValueField="TypeNo" ToolTip="<%#Container.DisplayIndex%>"></asp:DropDownList>
                                        <asp:DropDownList ID="ddl_SC_Plan" runat="server"></asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="客戶姓名">
                                    <ItemTemplate>
                                        <%# Eval("CU_CustName") %><asp:HiddenField ID="hif_CU_TransNo" runat="server" Value='<%# Eval("CU_TransNo") %>' />
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="年齡">
                                    <ItemTemplate>
                                        <%# Eval("CU_Age") %>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="備註">
                                     <HeaderTemplate>
                                         備註 
                                         <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" Text="加入行程" OnClick="btnAppend_Click" />
                                     </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txt_SC_Remark" runat="server" Rows="5" TextMode="MultiLine" Width="300"></asp:TextBox>
                                    </ItemTemplate>
                                     <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="False" SelectMethod="DataReader_TypeCode" TypeName="DataPhrase" OnSelecting="ObjectDataSource1_Selecting"></asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
