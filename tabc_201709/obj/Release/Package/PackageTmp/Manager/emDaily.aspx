﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_Open.master" AutoEventWireup="true" CodeBehind="emDaily.aspx.cs" Inherits="tabc_201709.Manager.emDaily" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
         <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="MainHeadTdLink" 
                onclick="LinkButton1_Click" 
                onclientclick="return confirm(&quot;確定要結轉嗎?&quot;);">結轉30天以上記錄至暫存</asp:LinkButton>
            　<asp:LinkButton ID="lnkExcelSet" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnClick="lnkExcelSet_Click"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 匯出</asp:LinkButton>
        </ContentTemplate>
        <triggers>
            <asp:PostBackTrigger ControlID="lnkExcelSet" />
        </triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <table align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="QueryMainTable" border="0">
                                <tr>
                                    <td class="tdQueryHead">
                                        維護日期：</td>
                                    <td class="tdQueryData">
                                        <asp:TextBox ID="txt_edDate_BEG_q" runat="server" MaxLength="10" Width="85px"></asp:TextBox>
                                        <asp:CalendarExtender ID="txt_edDate_BEG_q_CalendarExtender" runat="server" 
                                                    DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" 
                                                    TargetControlID="txt_edDate_BEG_q" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                        &nbsp;～
                                        <asp:TextBox ID="txt_edDate_END_q" runat="server" MaxLength="10" Width="85px"></asp:TextBox>
                                        &nbsp;yyyy/mm/dd</td>
                                    <asp:CalendarExtender ID="txt_edDate_END_q_CalendarExtender" runat="server" 
                                                    DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" 
                                                    TargetControlID="txt_edDate_END_q" TodaysDateFormat="yyyy年M月d日">
                                                </asp:CalendarExtender>
                                    <td class="tdQueryHead" width="150">歷史(暫存)資料：</td>
                                    <td class="tdQueryData">
                                        <asp:CheckBox ID="chkHIS" runat="server" />
                                    </td>
                                    <td rowspan="2" width="85">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                    </td>
                                    <td rowspan="2" width="35">
                                        <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tdQueryHead">
                                        資料關鍵值：</td>
                                    <td class="tdQueryData">
                                        <asp:TextBox ID="txt_key_q" runat="server" MaxLength="32" Width="200px"></asp:TextBox>
                                    </td>
                                    <td class="tdQueryHead">員工代碼／姓名：</td>
                                    <td class="tdQueryData">
                                        <asp:TextBox ID="txt_emNo_q" runat="server" MaxLength="14"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                            Width="120px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                            CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                            CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                            CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                            CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr style="display:none">
                                    <td align="center" valign="top">
                                        <asp:Panel ID="pnlPagers" runat="server" BorderWidth="0px">
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                            CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                            EmptyDataText="查無符合資料!!!" onload="GridView_Load">
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                            SelectCountMethod="GetCount" SelectMethod="emDailyReader" 
                                            SortParameterName="sortExpression" TypeName="DataemDaily">
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

