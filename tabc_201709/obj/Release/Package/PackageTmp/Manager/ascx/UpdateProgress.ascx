﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdateProgress.ascx.cs" Inherits="tabc_201709.Manager.ascx.UpdateProgress" %>

<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <progresstemplate>
        <table cellpadding="０" cellspacing="０">
            <tr>
                <td>
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Manager/images/loading.gif" />
                    </td>
                <td width="5">
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_msg" runat="server" Font-Size="Small" ForeColor="#2C6A9C" 
                        Text="資料處理中．．．"></asp:Label>
                </td>
            </tr>
        </table>
    </progresstemplate>
</asp:UpdateProgress>