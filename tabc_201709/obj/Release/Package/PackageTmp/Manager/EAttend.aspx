﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="EAttend.aspx.cs" Inherits="tabc_201709.Manager.EAttend" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>
<%@ Register src="ascx/selCourse.ascx" tagname="selCourse" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelSet" runat="server" CssClass="MainHeadTdLink" OnClick="lnkExcelSet_Click"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 匯出</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:PostBackTrigger ControlID="lnkExcelSet" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">開課日期：</td>
                                            <td align="center" class="tdQueryData" colspan="3">
                                                <uc2:DateRange ID="DateRangeSh_EC_SDate" runat="server" />
                                            </td>
                                            <td align="center" width="85" rowspan="3">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">課程代碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_Code" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">人員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PS_NAME" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">課程名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_CName" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">轉出銀行：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EP_Bank" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataEAttend"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">開課日期</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EC_SDateTime" runat="server" Font-Names="Arial" Font-Size="Medium"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s">
                                                <uc1:selCourse ID="selCourse_EC_TransNo" runat="server" OnSelectedIndexChanged="selCourse_EC_TransNo_SelectedIndexChanged" OnTextChanged="selCourse_EC_TransNo_TextChanged" OnSearchButtonClick="selCourse_EC_TransNo_SearchButtonClick" OnRefreshButtonClick="selCourse_EC_TransNo_RefreshButtonClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">人員</td>
                                            <td class="tdData10s">
                                                <uc3:selPerson_AccID ID="selPerson_AccID_ED_AccID" runat="server" OnRefreshButtonClick="selPerson_AccID_ED_AccID_RefreshButtonClick" OnSearchButtonClick="selPerson_AccID_ED_AccID_SearchButtonClick" OnSelectedIndexChanged="selPerson_AccID_ED_AccID_SelectedIndexChanged" OnTextChanged="selPerson_AccID_ED_AccID_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">飲食習慣</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_ED_EatHabits" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">交通方式</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_ED_Transport" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 50px; background-color: #F2DE97; color: #333333; font-weight: bold; font-family: 微軟正黑體; font-size: 18px;">繳費登錄資料</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉出銀行</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EP_Bank" runat="server" MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉出帳號末5碼</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EP_Account" runat="server" Width="50px" MaxLength="5"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳金額</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EP_TransAmt" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;(NTD)</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳日期</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EP_TransDate" runat="server" MaxLength="10" Width="80px"></asp:TextBox>
                                                <asp:CalendarExtender ID="txt_EP_TransDate_CalendarExtender" runat="server" DaysModeTitleFormat="yyyy年M月" Enabled="True" Format="yyyy/MM/dd" TargetControlID="txt_EP_TransDate" TodaysDateFormat="yyyy年M月d日"></asp:CalendarExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">轉帳時間</td>
                                            <td class="tdData10s">
                                                <uc1:TimeRangeAll ID="TimeRangeAll_EP_TransTime" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EP_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_ED_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_ED_AccID" runat="server" />
                                            </td>
                                        </tr>
                        </table>
                                    </td>
                                </tr>
                            </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
