﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="EQRule.aspx.cs" Inherits="tabc_201709.Manager.EQRule" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/selExamSub.ascx" tagname="selExamSub" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">考試類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EB_SubType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_EB_SubType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">科目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EB_Subject" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">報考項目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EE_Item" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataEQRule"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">報考科目</td>
                                            <td class="tdData10s" colspan="5">
                                                <uc1:selExamSub ID="selExamSub_EB_TransNo" runat="server" OnRefreshButtonClick="selExamSub_EB_TransNo_RefreshButtonClick" OnSearchButtonClick="selExamSub_EB_TransNo_SearchButtonClick" OnSelectedIndexChanged="selExamSub_EB_TransNo_SelectedIndexChanged" OnTextChanged="selExamSub_EB_TransNo_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">考試項目</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:DropDownList ID="ddl_EE_Item" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">測驗時間</td>
                                            <td class="tdData10s" colspan="5">
                                                <asp:TextBox ID="txt_EE_TimeLength" runat="server" Width="50px"></asp:TextBox>
                                                &nbsp;分鐘</td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">是非題數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_TrueFalse" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">是非題分數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_TrueFalseS" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">是非題順序</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_TrueFalseO" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">單選題數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_OneChoice" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">單選題分數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_OneChoiceS" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">單選題順序</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_OneChoiceO" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="20%">複選題數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_MultiChoice" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">複選題分數</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_MultiChoiceS" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                            <td class="MS_tdTitle" width="16%">複選題順序</td>
                                            <td class="tdData10s" width="16%">
                                                <asp:TextBox ID="txt_EE_MultiChoiceO" runat="server" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="6">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="6" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EE_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EB_TransNo" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
