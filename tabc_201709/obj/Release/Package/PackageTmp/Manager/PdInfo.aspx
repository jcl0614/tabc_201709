﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" validateRequest="false" CodeBehind="PdInfo.aspx.cs" Inherits="tabc_201709.Manager.PdInfo" %>


<%@ Register src="ascx/ckeditor.ascx" tagname="ckeditor" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
  <link href="/Manager/css/style.css" rel="stylesheet" type="text/css"/>
         <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
            <asp:Panel ID="Panel_Form" runat="server" style="text-align: left">
                  <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
               <table align="center" cellpadding="0" cellspacing="0" Width="100%" >
                                               <tr>
                                    <td align="center" valign="top" style="color: #FF0000">※所有商品資料皆來自保發中心，僅提供參考，正確資料仍依保險公司提供之建議書系統為依據。
                                    </td>
                                </tr>

                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" class="QueryMainTable" border="0">
                                <tr>
                                    <td class="tdQueryHead" style="width: 75px" >
                                        保險公司：</td>
                                    <td class="tdQueryData" Width="20%">
                                           <asp:DropDownList ID="ddlSh_ComKind" runat="server" AutoPostBack="True" Width="20%"
                                               onselectedindexchanged="ddlSh_ComKind_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True" Value="0">壽險</asp:ListItem>
                                                    <asp:ListItem Value="1">產險</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlSh_Company" runat="server" Width="70%">
                                                </asp:DropDownList>
</td>
                                            <td align="center" class="tdQueryHead" >主附約：</td>
                                            <td align="center" class="tdQueryData"  Width="20%">
                                                <asp:DropDownList ID="ddlSh_kind" runat="server" Width="100%">
                                                 </asp:DropDownList>
                                            </td>
                                    <td rowspan="2" width="10%" >
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                    </td>
                                    <td rowspan="2" width="35">
                                        <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                    </td>
                                </tr>
                                <tr>
                                            <td align="center" class="tdQueryHead" style="width: 85px">險種類別：</td>
                                            <td align="center" class="tdQueryData" >
                                                <asp:DropDownList ID="ddlSh_Type" runat="server" Width="100%">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">狀態：</td>
                                            <td align="center" class="tdQueryData" >
                                                <asp:DropDownList ID="ddlSh_stop" runat="server" >
                                                    <asp:ListItem Selected="True" Value="2">全部</asp:ListItem>
                                                    <asp:ListItem Value="1">現售</asp:ListItem>
                                                    <asp:ListItem Value="0">停售</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox ID="tabcSale" runat="server" Text="台名銷售" Visible="False" />

                                            </td>
                                                                                    </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead" style="width: 10%">險種代號：</td>
                                            <td align="center" class="tdQueryData" >
                                                <asp:TextBox ID="txtSh_PdtNo" runat="server" Width="100%"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead" style="width: 10%">險種名稱：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PdtName" runat="server" Width="100%"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">關鍵字查詢：</td>
                                            <td align="center" class="tdQueryData" Width="20%">
                                                <asp:TextBox ID="txtSh_KeyWord" runat="server" Width="100%"></asp:TextBox>
                                            </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                            CssClass="ddlPage">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                            Width="120px"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                            CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                            CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                            CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                            CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr style="display:none">
                                    <td align="center" valign="top">
                                        <asp:Panel ID="pnlPagers" runat="server" BorderWidth="0px">
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                            CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                            EmptyDataText="查無符合資料!!!" onload="GridView_Load" 
                                            onselectedindexchanged="GridView1_SelectedIndexChanged">
                                            <PagerSettings Visible="False" />
                                        </asp:GridView>
                                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                            SelectCountMethod="GetCount" SelectMethod="PdInfoReader" 
                                            SortParameterName="sortExpression" TypeName="DataPdInfo">
                                        </asp:ObjectDataSource>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
                   </asp:View>
                    <asp:View ID="View7" runat="server">
                    <asp:Panel ID="Panel1" runat="server">
                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="回列表" />
                        <asp:Button ID="Button2" runat="server" Text="輔銷工具下載" onclick="Button2_Click" />
                        <asp:Button ID="Button3" runat="server" Text="佣金率/特別津貼" 
                            onclick="Button3_Click" Visible="False" />
                        <asp:Button ID="Button4" runat="server" onclick="Button4_Click" Text="詳細商品資訊" />
                        </asp:Panel>
                        <asp:MultiView ID="MultiView3" runat="server">
                            <asp:View ID="View8" runat="server">
                            <div class="tab-pane fade item1 active in" id="tab0">                    
                    <table class="table table-bordered" style="border: 1px solid; width: 100%; height: auto; font-size: 14px;">
                        <tbody>
                            <tr class="label-info" style="background-color: #EEEEEE">
                                <td style="width: 80px; text-align: center;" class="text03">公司</td>
                                <td style="width: 80px; text-align: center;" class="text03">約別</td>
                                <td style="width: 80px; text-align: center;" class="text03">險種代號</td>
                                <td class="text03">險種名稱</td>
                                <td style="width: 80px; text-align: center;" class="text03">DM</td>
                                <td style="width: 80px; text-align: center;" class="text03">試算表</td>
                                <td style="width: 80px; text-align: center;" class="text03">商品簡報</td>
                                <td style="width: 80px; text-align: center;" class="text03">經代專區</td>
                            </tr>
                            <tr id="pdtrow_1" class="table_list01" style="vertical-align: middle; height: 25px; background-color: rgb(255, 255, 255);">
                                <td class="text02" style="text-align: center; color: #000;">
                                    <asp:Label id="ComSname1" runat="server" Text="國泰人壽"></asp:Label>
                                </td>
                                <td class="text02" style="text-align: center; color: #000;">
                                    <asp:Label id="kindname" runat="server" Text="壽險主約"></asp:Label>
                                </td>
                                <td class="text02" style="text-align: left; color: #000;">
                                    <asp:Label id="PdtCode" runat="server" Text="XU"></asp:Label>
                                </td>
                                <td class="text02" style="text-align: left;">
                                    <asp:Label id="PdtName" runat="server" Text="好利High利率變動型美元終身壽險(OIU)"></asp:Label>
                                </td>
                                <td class="text02" style="text-align: center; color: rgb(0, 0, 0);">
                                 <asp:ImageButton ID="ImageButton5" runat="server" 
                                ImageUrl="~/Manager/images/DM.jpg"  Height="24px" />
                                </td>
                                <td class="text02" style="text-align: center; color: rgb(0, 0, 0);">
                                 <asp:ImageButton ID="ImageButton6" runat="server" 
                                ImageUrl="~/Manager/images/doc_excel.png"  
                                        Height="24px" />
                                    </td>
                                <td class="text02" style="text-align: center; color: rgb(0, 0, 0);">
                                  <asp:ImageButton ID="ImageButton7" runat="server" 
                                ImageUrl="~/Manager/images/doc_ppt.png" 
                                        Height="24px" />
                                    </td>
                                <td class="text02" style="text-align: center; color: rgb(0, 0, 0);">
                                  <asp:ImageButton ID="ImageButton8" runat="server" 
                                ImageUrl="~/Manager/images/doc_edit.jpg" 
                                        Height="24px" />
                                    </td>
                            </tr>
                        </tbody>
                    </table>
                    <input id="TABC_sno" type="hidden" value="28906">
                    <table id="tbl_pdtList" class="table table-bordered" style="border: 1px solid; width: 100%; height: auto; font-size: 14px; display: none;">
                        <tbody><tr>
                            <th colspan="3" class="warning">檔案文件下載</th>
                        </tr>
                        <tr class="label-info" style="background-color: #EEEEEE">
                            <td style="width: 15%; text-align: center;" class="text03">序號</td>
                            <td style="width: 15%; text-align: center;" class="text03">檔案編號</td>
                            <td style="text-align: center;" class="text03">檔案名稱</td>
                        </tr>
                    </tbody></table>
                </div>
                           </asp:View>
                            <asp:View ID="View9" runat="server">
                                <asp:Literal ID="Literal5" runat="server" Mode="PassThrough"></asp:Literal>
                           </asp:View>
                    <asp:View ID="View2" runat="server">
                                        <asp:Panel ID="Panel2" runat="server">
                        <asp:ImageButton ID="ImageButton1" runat="server" 
                            ImageUrl="~/Manager/images/p11_1.gif" onclick="ImageButton1_Click" />
                        <asp:ImageButton ID="ImageButton2" runat="server" 
                            ImageUrl="~/Manager/images/p12_1.gif" onclick="ImageButton2_Click" />
                        <asp:ImageButton ID="ImageButton3" runat="server" 
                            ImageUrl="~/Manager/images/p13_1.gif" onclick="ImageButton3_Click" />
                        <asp:ImageButton ID="ImageButton4" runat="server" 
                            ImageUrl="~/Manager/images/p14_1.gif" onclick="ImageButton4_Click" />
                        </asp:Panel>

                        <asp:MultiView ID="MultiView2" runat="server">
                            <asp:View ID="View3" runat="server">                                                               
                                <asp:Literal ID="Literal1" runat="server" Mode="PassThrough"></asp:Literal>
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <asp:Literal ID="Literal2" runat="server" Mode="PassThrough"></asp:Literal>
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                                 <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                           </asp:View>
                            <asp:View ID="View6" runat="server">
                                 <asp:Literal ID="Literal4" runat="server" Mode="PassThrough"></asp:Literal>
                           </asp:View>
                        </asp:MultiView>
                   </asp:View>
                        </asp:MultiView>

                        

                   </asp:View>
                </asp:MultiView>
            </asp:Panel>
</asp:Content>

