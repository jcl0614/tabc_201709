﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="InsWork_PolicyRecord.aspx.cs" Inherits="tabc_201709.Manager.InsWork_PolicyRecord" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Manager/ascx/UpdateProgress.ascx" TagPrefix="uc1" TagName="UpdateProgress" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
    <script type="text/javascript"> 
        function showDetail(title) {
            $('#div_detail').dialog({
                modal: true,
                height: 600,
                width: 750,
                title: title
            });
        }
</script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_detail" style="display: none; width: 800px; height: 600px;">
                <asp:Literal ID="Literal_tetail" runat="server"></asp:Literal>
            </div>
            <table align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" height="3">
                        <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                            <tr>
                                <td align="center" class="tdQueryHead">保單類別：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:DropDownList ID="ddl_P_iINS_TYPE" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="center" class="tdQueryHead">照會狀態：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:DropDownList ID="ddl_P_cRES_STATUS" runat="server">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Y">已處理</asp:ListItem>
                                        <asp:ListItem Value="N">未處理</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td align="center" class="tdQueryHead">轄下業務員：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:DropDownList ID="ddl_P_cSAL_ID_S" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td align="center" width="85" rowspan="2">
                                    <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                </td>
                                <td align="center" width="35" rowspan="2">
                                    <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="tdQueryHead">
                                    <asp:DropDownList ID="ddl_field" runat="server">
                                    </asp:DropDownList>
                                    ：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:TextBox ID="txt_keyword" runat="server"></asp:TextBox>
                                </td>
                                <td align="center" class="tdQueryHead">要保人姓名：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:TextBox ID="txt_P_cINS_MAN_A" runat="server" Width="100px"></asp:TextBox>
                                </td>
                                <td align="center" class="tdQueryHead">被保人姓名：</td>
                                <td align="center" class="tdQueryData">
                                    <asp:TextBox ID="txt_P_cINS_MAN_B" runat="server" Width="100px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                        CssClass="ddlPage">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                        CssClass="ddlPage">
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                        Width="120px"></asp:Label>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                        CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                        CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                        CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                        CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <uc1:UpdateProgress runat="server" ID="UpdateProgress" />
                        <asp:GridView ID="GridView1" runat="server"
                            AutoGenerateColumns="False" CssClass="GridViewTable"
                            EmptyDataText="查無符合資料!!!"
                            EnableModelValidation="True">
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                            SelectCountMethod="GetCount" SelectMethod="DataReader"
                            TypeName="DataInsWork_PolicyRecord"></asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
