﻿/**
 * @license Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.plugins.setLang( 'a11ychecker', 'en', {
    toolbar: '檢查輔助功能',
	closeBtn: '關閉',
	testability: {
	    '0': '注意',
	    '0.5': '警告',
	    '1': '錯誤'
	},
	ignoreBtn: '忽略',
	ignoreBtnTitle: '忽略這個問題',
	stopIgnoreBtn: '不要忽視',
	listeningInfo: '等待目前內容變更。完成後， 點擊下方的<strong>再次檢查</strong>按鈕',
	listeningCheckAgain: '再次檢查',
	balloonLabel: '輔助功能檢查器',
	navigationNext: '下一個',
	navigationNextTitle: '下一個的問題',
	navigationPrev: '上一個',
	navigationPrevTitle: '上一個問題',
	quickFixButton: '快速修復',
	quickFixButtonTitle: '快速修復這個問題',
	navigationCounter: '問題 {current} 到 {total} ({testability})',
	noIssuesMessage: '該文件不包含任何可輔助的問題。'
} );
