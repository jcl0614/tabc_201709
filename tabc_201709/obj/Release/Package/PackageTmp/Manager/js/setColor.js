function ShowDialog(url, width, height, optForm, optShow) {
    var myObject = new Object();
    myObject.firstName = optForm.value;
    var arr = showModalDialog(url, myObject, "dialogWidth:" + width + "px;dialogHeight:" + height + "px;help:no;scroll:no;status:no");
    if (arr != undefined) {
        optForm.value = arr;
        $('#' + optShow).css('background', arr);
    }
}
