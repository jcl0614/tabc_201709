﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="tabc_201709.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        tr:hover{
            background: #ff0000;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="8" Font-Size="12px" ForeColor="#333333" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Day" HeaderText="日期">
                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="Week" HeaderText="星期">
                    <HeaderStyle HorizontalAlign="Center" Width="50px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="單位課程">
                    <ItemTemplate>
                        <ul>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <li><asp:LinkButton ID="lbtn_Course_Unit" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="btn_Command"><%# Eval("EC_CName") %></asp:LinkButton></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="區部課程">
                    <ItemTemplate>
                        <ul>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <li><asp:LinkButton ID="lbtn_Course_Unit" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="btn_Command"><%# Eval("EC_CName") %></asp:LinkButton></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="總公課程">
                    <ItemTemplate>
                        <ul>
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <li><asp:LinkButton ID="lbtn_Course_Unit" runat="server" CommandName='<%# Eval("EC_TransNo") %>' Font-Underline="False" ForeColor="#0099CC" OnCommand="btn_Command"><%# Eval("EC_CName") %></asp:LinkButton></li>
                            </ItemTemplate>
                        </asp:Repeater>
                        </ul>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="200px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="Memo" HeaderText="備註" HtmlEncode="False">
                    <HeaderStyle HorizontalAlign="Center" Width="250px"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundField>
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#745741" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </form>
</body>
</html>
