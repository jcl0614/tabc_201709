﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

using NPOI;
using NPOI.HPSF;
using NPOI.HSSF;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.POIFS;
using NPOI.Util;
using NPOI.XSSF.UserModel;

namespace tabc_201709
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("考試項目", typeof(string));
            dt.Columns.Add("考試日期", typeof(string));
            dt.Columns.Add("報考人數", typeof(int));
            dt.Columns.Add("現金繳交", typeof(int));
            dt.Columns.Add("銀行轉帳", typeof(int));
            dt.Columns.Add("合計金額", typeof(int));
            dt.Columns.Add("備註", typeof(string));

            DataTable dt_examPay_statistics = DataEPayment.DataReader_examPay_statistics("2017", "8").Tables[0];

            foreach (DataRow dr in dt_examPay_statistics.Rows)
            {
                StringBuilder sb_memo = new StringBuilder();
                DataTable dr_payment = DataEPayment.DataReader_EG_TransNo(dr["備註"].ToString()).Tables[0];
                foreach (DataRow dr_p in dr_payment.Rows)
                {
                    DataRow dr_person = DataPerson.DataReader_AccID(dr_p["EP_AccID"].ToString());
                    if (dr_person != null)
                    {
                        if (dr_p["EP_Memo"].ToString() != "")
                            sb_memo.AppendFormat("【{0}】{1}\n", dr_person["PS_NAME"].ToString(), dr_p["EP_Memo"].ToString());
                    }
                }
                dt.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], sb_memo.ToString());
            }

            DataTable dt_details = new DataTable();
            dt_details.Columns.Add("費用類別", typeof(string));
            dt_details.Columns.Add("考試項目", typeof(string));
            dt_details.Columns.Add("考試日期", typeof(string));
            dt_details.Columns.Add("業務員編號", typeof(string));
            dt_details.Columns.Add("ID", typeof(string));
            dt_details.Columns.Add("姓名", typeof(string));
            dt_details.Columns.Add("現金繳交", typeof(int));
            dt_details.Columns.Add("銀行轉帳", typeof(int));

            DataTable dt_examPay_statistics_dt_details = DataEPayment.DataReader_examPay_statistics_details("2017", "8").Tables[0];

            foreach (DataRow dr in dt_examPay_statistics_dt_details.Rows)
            {
                dt_details.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], dr[6], dr[7]);
            }
            Dictionary<string, DataTable> list = new Dictionary<string, DataTable>();
            list.Add(string.Format("考試費用對帳月統計表_{0}年{1}月", 2017, 8), dt);
            list.Add(string.Format("考試費用對帳明細表_{0}年{1}月", 2017, 8), dt_details);
            MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(list) as MemoryStream;

            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("考試費用對帳統計表_{0}年{1}月_{2}.xls", 2017, 8, DateTime.Now.ToString("yyyyMMddHHmmss")))));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            Excel.Application App = new Excel.Application();

            //取得欲寫入的檔案路徑
            string strPath = Server.MapPath("~/XLS/test.xls");
            Excel.Workbook Wbook = App.Workbooks.Open(strPath);

            //將欲修改的檔案屬性設為非唯讀(Normal)，若寫入檔案為唯讀，則會無法寫入
            System.IO.FileInfo xlsAttribute = new FileInfo(strPath);
            xlsAttribute.Attributes = FileAttributes.Normal;

            //取得batchItem的工作表
            Excel.Worksheet Wsheet = (Excel.Worksheet)Wbook.Sheets[1];

            Wsheet.Copy(Type.Missing, Wbook.Sheets[1]); // copy
            Wsheet.Copy(Type.Missing, Wbook.Sheets[1]); // copy
            Wbook.Sheets[2].Name = "資格測驗報名表2"; // rename
            Wbook.Sheets[3].Name = "資格測驗報名表3"; // rename

            //取得工作表的單元格
            //列(左至右)ABCDE、行(上至下)12345
            Excel.Range aRangeChange = Wsheet.get_Range("K23");

            //在工作表的特定儲存格，設定內容
            aRangeChange.Value2 = "777";

            //設置禁止彈出保存和覆蓋的詢問提示框
            Wsheet.Application.DisplayAlerts = false;
            Wsheet.Application.AlertBeforeOverwriting = false;

            //保存工作表，因為禁止彈出儲存提示框，所以需在此儲存，否則寫入的資料會無法儲存
            string guid = System.Guid.NewGuid().ToString("N");
            Wbook.SaveAs(Server.MapPath(string.Format("~/XLS/{0}.xls", guid)));

            //關閉EXCEL
            Wsheet = null;
            Wbook.Close();
            Wbook = null;
            //離開應用程式
            
            App.Quit();
            App = null;

            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] file = wc.DownloadData(Server.MapPath(string.Format("~/XLS/{0}.xls", guid)));
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("考試費用對帳統計表_{0}年{1}月_{2}.xls", 2017, 8, DateTime.Now.ToString("yyyyMMddHHmmss")))));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(file);
            

            if (File.Exists(Server.MapPath(string.Format("~/XLS/{0}.xls", guid))))
                File.Delete(Server.MapPath(string.Format("~/XLS/{0}.xls", guid)));

            Response.End();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            HSSFWorkbook workbook;
            using (FileStream fs = new FileStream(Server.MapPath("~/XLS/test2.xls"), FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(fs);
            }
            MemoryStream ms = new MemoryStream();
            ISheet sheet = workbook.GetSheetAt(0);
            DataTable dt = DataEPayment.DataReader_examPay_upload_details("E011", "2017", "6").Tables[0];
            int rowIndex = 26;
            var dt_groupby_EX_Date = dt.AsEnumerable().GroupBy(r => new { EX_Date = r["EX_Date"] }).ToList();
            foreach (var date in dt_groupby_EX_Date)
            {
                if (dt_groupby_EX_Date.IndexOf(date) > 0)
                {
                    sheet.CopySheet(string.Format("資格測驗報名表_{0}", date.Key.EX_Date.ToString().Replace("/", "")), true);
                    sheet = workbook.GetSheetAt(dt_groupby_EX_Date.IndexOf(date) + 1);
                }
                else
                    workbook.SetSheetName(0, string.Format("資格測驗報名表_{0}", date.Key.EX_Date.ToString().Replace("/", "")));
                DataRow[] dr_list = dt.Select(string.Format("EX_Date='{0}'", date.Key.EX_Date));
                Int32 count = (Int32)dt.Compute("count(EG_RegistFee)", string.Format("EX_Date='{0}'", date.Key.EX_Date));
                Int64 amt = (Int64)dt.Compute("sum(EG_RegistFee)", string.Format("EX_Date='{0}'", date.Key.EX_Date));
                foreach (DataRow dr in dr_list)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        if (dc.ColumnName != "")
                        {
                            switch (dc.ColumnName)
                            {
                                case "SerialNo":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(0);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(0);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EP_Region":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(1);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(1);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EP_Name":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(2);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(2);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "Gender":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(3);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(3);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "type":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(4);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(4);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EP_ID":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(5);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(5);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                    break;
                                case "Birth":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(6);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(6);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EP_EduLevel":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(7);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(7);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EB_Subject":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(8);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(8);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EX_Date_range":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(9);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(9);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                                case "EX_Score":
                                    {
                                        ICell cell = sheet.GetRow(rowIndex).GetCell(10);
                                        if (cell == null)
                                            cell = sheet.GetRow(rowIndex).CreateCell(10);
                                        cell.SetCellValue(dr[dc.ColumnName].ToString());
                                    }
                                    break;
                            }

                        }
                    }
                    rowIndex += 1;
                }
                DateTime EX_Date = DateTime.Parse(date.Key.EX_Date.ToString());
                sheet.GetRow(22).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (EX_Date.Year - 1911).ToString("0000"), EX_Date.Month.ToString("00"), EX_Date.Day.ToString("00")));
                sheet.GetRow(22).GetCell(6).SetCellValue(count);
                sheet.GetRow(22).GetCell(10).SetCellValue(amt);
                
            }
            workbook.Write(ms);
            ms.Flush();
            ms.Position = 0;

            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("考試費用對帳統計表_{0}年{1}月_{2}.xls", 2017, 8, DateTime.Now.ToString("yyyyMMddHHmmss")))));
            HttpContext.Current.Response.Charset = "big5";
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
        }
    }
}