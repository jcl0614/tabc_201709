﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "1");
            //if (dr_p != null)
            //{
            //    DataTable dt_member = new DataTable();
            //    dt_member.Columns.Add("業務員編號", typeof(string));
            //    dt_member.Columns.Add("姓名", typeof(string));
            //    dt_member.Columns.Add("訓練總時數", typeof(string));
            //    dt_member.Columns.Add("規定時數", typeof(string));
            //    int preDays = 0;
            //    int.TryParse(dr_p["Limit"].ToString(), out preDays);
            //    DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
            //    DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
            //    foreach (DataRow dr_sales in dt_sales.Rows)
            //    {
            //        DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
            //        int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
            //        DataRow dr_erule = DataERule.DataReader_ER_YearNo(iYear.ToString());
            //        if (dr_erule != null)
            //        {
            //            double ER_Hours = 0;
            //            double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
            //            double hoursCount = 0;
            //            var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
            //            double.TryParse(hours, out hoursCount);
            //            if (hoursCount < ER_Hours)
            //            {
            //                dt_member.Rows.Add(dr_sales["cSAL_FK"].ToString(), dr_sales["NAME"].ToString(), hoursCount.ToString(), ER_Hours.ToString());
            //            }
            //        }
            //    }
            //    GridView2.DataSource = dt_member;
            //    GridView2.DataBind();
            //}
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            GridView gv = (GridView)((CheckBox)sender).Parent.Parent.Parent.Parent;
            foreach (GridViewRow gvr in gv.Rows)
            {
                ((CheckBox)gvr.Cells[0].Controls[1]).Checked = ((CheckBox)sender).Checked;
            }
        }
    }
}