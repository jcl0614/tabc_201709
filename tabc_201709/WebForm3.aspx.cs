﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lbl_sTime.Text = DateTime.Now.ToString("HH:mm");
                Session["eTime"] = DateTime.Now.AddSeconds(10);
            }
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            lbl_eTime.Text = DateTime.Now.ToString("HH:mm");

            TimeSpan MySpan = ((DateTime)Session["eTime"]).Subtract(DateTime.Now);
            if ((int)MySpan.TotalSeconds >= 0)
            {
                string diffHour = Convert.ToString(MySpan.Hours);
                string diffMin = Convert.ToString(MySpan.Minutes);
                string diffSec = Convert.ToString(MySpan.Seconds);
                lbl_Time.Text = string.Format("{00}:{01}:{02}", diffHour, diffMin, diffSec);

                if ((int)MySpan.TotalSeconds == 0)
                {
                    Timer1.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(
                        this, HttpContext.Current.GetType(), "Alert",
                        string.Format("alert('{0}');location.href='{1}'", "時間結束！", "http://www.google.com"),
                        true);

                }

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = true;
        }
    }
}