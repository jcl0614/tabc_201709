﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["aspxerrorpath"] != null)
            {
                if (Request.QueryString["aspxerrorpath"].ToLower().IndexOf("/web/") != -1)
                    Response.Redirect(string.Format("{0}/Web/Error", Application["WebUrl"].ToString()));
                else if (Request.QueryString["aspxerrorpath"].ToLower().IndexOf("/manager/") != -1)
                    Response.Redirect(string.Format("{0}/Manager/Login", Application["WebUrl"].ToString()));
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Close", "window.opener=null; window.open('', '_self', ''); window.close();", true);

            }
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Close", "window.opener=null; window.open('', '_self', ''); window.close();", true);
        }
    }
}