﻿var defaultHtmlModel = '<div id="boxes">'
            + '<div id="dialog">'
            + '<table background="images/CrCenter/backg.jpg" width="278" height="154">'
            + '<tr>'
            + '     <td align="right" valign="top" height="25" >'
            + '         <img src="images/CrCenter/backg_x.jpg" class="close" style="cursor: pointer; width: 22; height: 22;"/>'
            + '    </td>'
            + '</tr>'
            + '<tr>'
            + '     <td align="center" height="93" >'
            + '         <div id="dCenterDiv" >'
            + '        </div>'
            + '    </td>'
            + '</tr>'
            + '<tr>'
            + '     <td align="right" valign="top" height="25" >'
            + '    </td>'
            + '</tr>'
            + '</table>'
            + '</div>'
            + '<div id="mask"></div>'
            + '</div>';

function modal(msg) {

    //把Html轉成jQuery物件,並加入到<body>中
    var $boxes = $(defaultHtmlModel);
    $boxes.appendTo('body');

    //設定Title和要顯示的Message
    $('#dCenterDiv').html(msg);

    //點擊.close時,將boxes從畫面中移除
    $('#dialog .close').click(function (e) {
        $boxes.remove();
    });

    //將Mask的寬和高設定成和畫面大小一樣，然後用動畫顯示，並設定透明度為0.6
    $('#mask').css({ 'width': $(window).width()
            , 'height': $(document).height()
    }).fadeTo("slow", 0.6);

    //將Dialog的位置設定在畫面中央，然後動畫顯示
    $("#dialog").floatdiv("middle");
    //$("#dialog").fadeIn(2000);
    $("#dialog").show();

}

function modalAutoHide(msg, timeout) {

    //把Html轉成jQuery物件,並加入到<body>中
    var $boxes = $(defaultHtmlModel);
    $boxes.appendTo('body');

    //設定Title和要顯示的Message
    $('#dCenterDiv').html(msg);

    //點擊.close時,將boxes從畫面中移除
    $('#dialog .close').click(function (e) {
        $boxes.remove();
    });

    //將Mask的寬和高設定成和畫面大小一樣，然後用動畫顯示，並設定透明度為0.6
    $('#mask').css({ 'width': $(window).width()
            , 'height': $(document).height()
    }).fadeTo("slow", 0.6);

    //將Dialog的位置設定在畫面中央，然後動畫顯示
    $("#dialog").floatdiv("middle");
    //if (timeout == 0) {
    //$("#dialog").fadeIn(2000);
    //    $("#dialog").show();
    //}
    //else
    $("#dialog").slideDown(0).delay(timeout).slideUp(0, function () {
        $boxes.remove();
    });


}

