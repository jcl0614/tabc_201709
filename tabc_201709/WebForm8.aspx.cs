﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i <= 3; i++)
            {
                sb.Append("<div style=\"width: 220mm; height: 90mm\">");
                sb.Append(" <table width=\"100%\" >");
                sb.AppendFormat("<tr><td style=\"width: 100%; height: 55mm; padding-top: 5%; padding-left: 5%;\" align=\"left\" valign=\"top\">{0}<br />{1}</td></tr>", "寄件地址", "寄件單位");
                sb.AppendFormat("<tr><td style=\"width: 100%; height: 55mm\" align=\"center\">{0}<br /><br />{1}</td></tr>", "收件地址", "收件人");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div style=\"page-break-before:always; line-height: 0px; height: 0px;\"></div>");
            }

            ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "Alert", string.Format("printHtml('{0}');", sb.ToString()), true);
        }
    }
}