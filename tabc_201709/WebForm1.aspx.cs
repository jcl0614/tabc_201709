﻿using System;
using System.Text;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web;
using System.Xml;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using NLog;
using System.Reflection;

namespace tabc_201709
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        protected void Page_Init(object sender, EventArgs e)
        {
            Panel1.Controls.Clear();
            Dictionary<int, string> text = new Dictionary<int, string>();
            for (int i = 0; i <= 10;i++ )
            {
                if (sender is DropDownList)
                    text.Add(i, "文字_" + ((DropDownList)sender).SelectedValue);
                else
                    text.Add(i, "文字_" + i.ToString());
            }

            Dictionary<int, bool> isChecked = new Dictionary<int, bool>();
            isChecked.Add(3, true);

            MainControls.CreateCheckBoxWithTextBox(Panel1, "cbl", "cb", "text", 10, 200, text, isChecked);
            MainControls.CreateRadioButtonWithTextBox(Panel1, "rbl", "rb", "text", 10, 200, text, isChecked);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DoTrans();
        }

        private void DoTrans()
        {
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                            "  <Requests Name=\"業務員基本資料\" Text=\"Request\" RB=\"RB(N)\">" +
                            "   <Request Name=\"業務員基本資料\" Program=\"TM_A001_Request\" MaxRows=\"0\" MinLags=\"0\">" +
                            "    <Parameters>" +
                            "     <Parameter Name=\"@P_cUSER_FK\" Value=\"'ALL'\"/>" +//'{USR_PK}'
                            "    </Parameters>" +
                            "   </Request>" +
                            "  </Requests>" +
                            " </BIS>";
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ret);
                XmlNodeList NodeList = doc.SelectNodes("BIS/Responds/Respond/N");
                DataTable dtDisPlay = new DataTable();    //業務員基本資料表
                Dictionary<string, int> dictItem = new Dictionary<string, int>();
                //
                dictItem.Add("編號", 80);
                dictItem.Add("密碼", 100);
                dictItem.Add("身分證號", 100);
                dictItem.Add("姓名", 80);
                dictItem.Add("狀態", 80);
                dictItem.Add("壽險登入證號", 80);
                dictItem.Add("產險登入證號", 100);
                dictItem.Add("壽險登入日期", 100);
                dictItem.Add("產險登入日期", 80);
                dictItem.Add("壽險撤登日期", 100);
                dictItem.Add("產險撤登日期", 80);
                dictItem.Add("職級", 100);

                foreach (KeyValuePair<string, int> kvp in dictItem)
                {
                    dtDisPlay.Columns.Add(kvp.Key);
                }

                foreach (XmlNode COB in NodeList)
                {
                    DataRow drnew = dtDisPlay.NewRow();
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        if (dictItem.ContainsKey(Attr.Name.ToString()))
                        {
                            drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value.ToString().Trim();
                        }
                    }
                    RServiceProvider rsp = employeeApply(drnew["編號"].ToString(), drnew["編號"].ToString(), drnew["密碼"].ToString(), drnew["姓名"].ToString(), drnew["編號"].ToString());
                    drnew.EndEdit();
                    dtDisPlay.Rows.Add(drnew);
                }
                GridView1.DataSource = dtDisPlay;
                GridView1.DataBind();
                #endregion

                

            }
        }

        private RServiceProvider employeeApply(string emNo, string emID, string emPWD, string emName, string AccID)
        {
            DataSet ds = new DataSet();
            
            RServiceProvider rsp = new RServiceProvider();
            if (DataEmployee.emNoValid(emNo))
            {
                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema2(true), dataToHashtable_employee(emNo, emID, emPWD, emName, AccID, true)));
                rsp = DataEmployee.Append("00000000", "sys_Employee", ds);

            }
            //else
            //{
            //    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema2(false), dataToHashtable_employee(emNo, emID, emPWD, emName, AccID, false)));
            //    rsp = DataEmployee.Update("00000000", "sys_Employee", ds);
            //}
            return rsp;
        }

        private Hashtable dataToHashtable_employee(string emNo, string emID, string emPWD, string emName, string AccID, bool isApply)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("emNo", emNo);
            hsData.Add("emID", emID);
            hsData.Add("emPWD", Lib.GetMD5(emPWD));
            hsData.Add("emName", emName);
            hsData.Add("emSex", null);
            hsData.Add("emMail", null);
            hsData.Add("emPhone", null);
            hsData.Add("emEnable", "true");
            hsData.Add("ipLimit", null);
            if (isApply)
                hsData.Add("EmGroup", "2");
            hsData.Add("AccID", AccID);

            return hsData;
        }

        private RServiceProvider personApply(string PS_ID, string PS_NAME, string PS_PW, string PS_Title, string AccID, string PS_LifeNo, string PS_ProductNo, string PS_FirstDate, string PS_StartDate)
        {
            DataSet ds = new DataSet();

            RServiceProvider rsp = new RServiceProvider();
            if (DataPerson.valid_AccID(AccID))
            {
                ds.Tables.Add(MainControls.UpLoadToDataTable(DataPerson.GetSchema(), dataToHashtable_Person(PS_ID, PS_NAME, PS_PW, PS_Title, AccID, PS_LifeNo, PS_ProductNo, PS_FirstDate, PS_StartDate, true)));
                rsp = DataEmployee.Append("00000000", "tblPerson", ds);

            }
            //else
            //{
            //    ds.Tables.Add(MainControls.UpLoadToDataTable(DataPerson.GetSchema2(false), dataToHashtable_Person(PS_ID, PS_NAME, PS_PW, PS_Title, AccID, PS_LifeNo, PS_ProductNo, PS_FirstDate, PS_StartDate, false)));
            //    rsp = DataEmployee.Update("00000000", "sys_Employee", ds);
            //}
            return rsp;
        }

        private Hashtable dataToHashtable_Person(string PS_ID, string PS_NAME, string PS_PW, string PS_Title, string AccID, string PS_LifeNo, string PS_ProductNo, string PS_FirstDate, string PS_StartDate, bool isApply)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PS_ID", PS_ID);  //身分證字號
            hsData.Add("PS_NAME", PS_NAME);  //身分證字號
            hsData.Add("PS_PW", PS_PW);  //密碼

            hsData.Add("PS_Title", PS_Title);    //職級
            hsData.Add("PS_Use", "1");    //PS_Use
            hsData.Add("PS_OFF", "0");    //PS_OFF
            hsData.Add("PS_PowerLevel", "0");  //PS_PowerLevel
            hsData.Add("ComyCode", "0261");    //保經公司代號
            hsData.Add("AccID", AccID);    //業務員編號
            hsData.Add("CreatorID", "Admin");    //建立人員
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd"));    //建立日期
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd"));    //
            hsData.Add("ModiState", "A");    //異動狀態
            hsData.Add("PS_SetPaidDate", false);    //PS_SetPaidDate
            hsData.Add("PS_SaleMode", "1");    //PS_SaleMode
            hsData.Add("PS_SysVer", "3");    //PS_SysVer
            hsData.Add("PS_LifeNo", PS_LifeNo);  //壽險登入證號
            hsData.Add("PS_ProductNo", PS_ProductNo); //產險登入證號  
            hsData.Add("PS_FirstDate", PS_FirstDate == "" ? null : PS_FirstDate); //壽險登入日期
            hsData.Add("PS_StartDate", PS_StartDate == "" ? null : PS_StartDate);  //產險登入日期

            return hsData;
        }


        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        

        public void CreateCheckBoxWithTextBox(Control control, string ControlID, string CheckBoxID, string TextBoxID, int listCount)
        {
            var list = new List<ListItem>();

            for(int i=0;i<listCount;i++)
            {
                list.Add(new ListItem("", i.ToString()));
            }

            Table table = new Table();

            foreach (var item in list)
            {
                //Create new checkbox
                CheckBox CB = new CheckBox();
                CB.Text = item.Text;
                CB.ID = string.Format("{0}_{1}_{2}", ControlID, CheckBoxID, item.Value);

                //Create tablr row and td, then adds them accordignly
                TableRow TR = new TableRow();
                TableCell TD = new TableCell();
                TD.Controls.Add(CB);
                TR.Controls.Add(TD);

                //Create your input element and place it in a new Table cell (TD2)
                TextBox TB = new TextBox();
                TB.ID = string.Format("{0}_{1}_{2}", ControlID, TextBoxID, item.Value);
                TableCell TD2 = new TableCell();
                TD2.Controls.Add(TB);
                TR.Controls.Add(TD2);

                table.Controls.Add(TR);
            }

            control.Controls.Add(table);
        }

        public void CreateRadioButtonWithTextBox(Control control, string ControlID, string RadioButton, string TextBoxID, int listCount)
        {
            var list = new List<ListItem>();

            for (int i = 0; i < listCount; i++)
            {
                list.Add(new ListItem("", i.ToString()));
            }

            Table table = new Table();

            foreach (var item in list)
            {
                //Create new checkbox
                RadioButton RB = new RadioButton();
                RB.Text = item.Text;
                RB.ID = string.Format("{0}_{1}_{2}", ControlID, RadioButton, item.Value);
                RB.GroupName = ControlID;

                //Create tablr row and td, then adds them accordignly
                TableRow TR = new TableRow();
                TableCell TD = new TableCell();
                TD.Controls.Add(RB);
                TR.Controls.Add(TD);

                //Create your input element and place it in a new Table cell (TD2)
                TextBox TB = new TextBox();
                TB.ID = string.Format("{0}_{1}_{2}", ControlID, TextBoxID, item.Value);
                TableCell TD2 = new TableCell();
                TD2.Controls.Add(TB);
                TR.Controls.Add(TD2);

                table.Controls.Add(TR);
            }

            control.Controls.Add(table);
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string s = "";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Page_Init(DropDownList1, e);

            string s = ((TextBox)Panel1.FindControl("cbl_text_0")).Text;
            MainControls.showMsg(UpdatePanel1, s);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string s = ((TextBox)Panel1.FindControl("cbl_text_0")).Text;
            MainControls.showMsg(UpdatePanel1, s);
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            logger_db.Error("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
        }
    }
}