﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace tabc_201709.WCF
{
    /// <summary>
    ///SysData 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [System.Web.Script.Services.ScriptService]
    public class SysData : System.Web.Services.WebService
    {
        [WebMethod]
        public void Main()
        {


        }

        #region 後台系統自動登出
        [WebMethod(enableSession: true)]
        private void Logout()
        {
            Session["emUser"] = "";
            Session["emID"] = "";
            Session["emNo"] = "";
            Session["emOS"] = "0";
            Session["emDefOM"] = "";
            Session["emLimit"] = "";
            Session["userMenu"] = "";

        }
        #endregion

        #region 系統持續登入
        [WebMethod(enableSession: true)]
        private void Login()
        {
            Session["emUser"] = Session["emUser"];
            Session["emID"] = Session["emID"];
            Session["emNo"] = Session["emNo"];
            Session["emOS"] = Session["emOS"];
            Session["emDefOM"] = Session["emDefOM"];
            Session["emLimit"] = Session["emLimit"];
            Session["userMenu"] = Session["userMenu"];
        }
        #endregion
    }
}
