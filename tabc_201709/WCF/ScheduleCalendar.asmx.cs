﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace tabc_201709.WCF
{
    /// <summary>
    ///ScheduleCalendar 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [System.Web.Script.Services.ScriptService]
    public class ScheduleCalendar : System.Web.Services.WebService
    {
        [WebMethod(enableSession: true)]
        public void List()
        {
            //Session["UidNo"] = "11526";
            //Session["AccID"] = "TB0232";

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";

            Dictionary<String, Object> dic = new Dictionary<string, object>();

            DataTable dt = DataScheduleM.DataReader_AccID(Session["AccID"].ToString()).Tables[0];
            dic["ListItem"] = ListItem(dt);

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            //輸出json格式
            this.Context.Response.Write(serializer.Serialize(dic));

        }

        private List<Item> ListItem(DataTable dt)
        {
            List<Item> responseItems = new List<Item>();
            foreach (DataRow dr in dt.Rows)
                responseItems.Add(
                    new Item {
                        title = string.Format("{0},{1}", dr["SC_CustName"].ToString(), dr["SC_Plan"].ToString()),
                        state = dr["SC_State"].ToString(),
                        start = DateTime.Parse(dr["SC_StartTime"].ToString()).ToString("yyyy/MM/dd HH:mm:00"),
                        end = DateTime.Parse(dr["SC_EndTime"].ToString()).ToString("yyyy/MM/dd HH:mm:00"),
                        url = string.Format("/manager/Schedule?id={0}", dr["SC_TransNo"].ToString()),
                        color = "#1197C1"
                    }
                    );

            return responseItems;
        }

        public class Item
        {
            public string title { get; set; }
            public string state { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public string url { get; set; }
            public string color { get; set; }
        }
    }
}
