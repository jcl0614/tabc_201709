﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace tabc_201709.WCF
{
    /// <summary>
    ///WebUserCount 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [System.Web.Script.Services.ScriptService]
    public class WebUserCount : System.Web.Services.WebService
    {
        [WebMethod(enableSession: true)]
        public void Main()
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";

            Dictionary<String, Object> dic = new Dictionary<string, object>();
            dic["WebUserCount"] = GetUserCount();

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            //輸出json格式
            this.Context.Response.Write(serializer.Serialize(dic));

        }

        #region 前台線上使用者總數
        private string GetUserCount()
        {
            //宣告整數變數。(距離多少時間(秒)的「用戶最後存取時間」才算是離開)
            int intCheckTickSeconds = 60;
            //新增此 Session 變數以使 Session 物件能正常運作。(建議要加)
            Session["Flag"] = true;
            //宣告日期物件變數。(取得目前時間)
            DateTime dtDateTime = DateTime.Now;

            //鎖定全域變數存取。
            Application.Lock();

            if (Application["OnlineUser_A"] != null)
            {
                //== 檢查所有連線到此網頁之瀏覽器的最近存取時間，重新計算線上人數名單。
                //== 如果與目前時間相差30秒以上，表示結束連線。

                //使用動態陣列讀取。(取得 Application 的「線上用戶名單」陣列)
                ArrayList arrltOnlineUserT = (ArrayList)Application["OnlineUser_A"];
                //指派累加值為0。
                int intNum = 0;

                //宣告日期物件操作案例。
                DateTime dtLastAccessTime = new DateTime();

                //讀取「線上用戶名單」陣列項目。
                for (int i = 0; i < arrltOnlineUserT.Count; i++)
                {
                    //當 Application 全域變數「用戶最後存取時間」不是無值時。
                    if (Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"] != null)
                    {
                        //取得 Application 全域變數的「用戶最後存取時間」。
                        dtLastAccessTime = DateTime.Parse(Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"].ToString());
                        //宣告刻度間隔物件操作案例。(取得「目前時間」與「用戶最後存取時間」相差距的秒數)
                        TimeSpan tsTicks = new TimeSpan(dtDateTime.Ticks - dtLastAccessTime.Ticks);
                        //當相差距的秒數小於 30 秒時。
                        if (Convert.ToInt32(tsTicks.TotalSeconds) < intCheckTickSeconds)
                        {
                            //顯示目前「線上用戶名單」與「用戶最後存取時間」與「目前時間」與「用戶最後存取時間」相差距的秒數。
                            //Response.Write("全域變數用戶：" + arrltOnlineUserT[i].ToString() + " 存取時間：" + Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"] + " 相差: " + Convert.ToInt32(tsTicks.TotalSeconds).ToString() + " 秒");
                            //累加計數值。(統計人數)
                            intNum += 1;
                        }
                        else //當相差距的秒數大於 30 秒時。
                        {
                            //設定目前用戶的「用戶最後存取時間」Application 變數為無值。(清除 Application)
                            Application.Set(arrltOnlineUserT[i].ToString() + "LastAccessTime_A", null);
                            Application.Set(arrltOnlineUserT[i].ToString(), null);
                            Application.Remove(arrltOnlineUserT[i].ToString() + "LastAccessTime_A");
                            Application.Remove(arrltOnlineUserT[i].ToString());
                        }
                    }
                }

                //當上面所清查完成的「目前線上人數」與 Application「線上所有人數」不同就表示中間有人斷線。
                if (intNum != (int)Application["TotalUsers_A"])
                {
                    //將陣列項目值「線上用戶名單」放入 Application["OnlineUser_A"] 全域陣列值裡面。
                    Application.Set("OnlineUser_A", arrltOnlineUserT);
                    //將上面所清查的「線上人數」放入 Application["TotalUsers_A"] 全域變數裡面。
                    Application.Set("TotalUsers_A", intNum);
                }

                //輸出結果。
                //Response.Write("目前線上人數：" + Application["TotalUsers_A"]);

                //不鎖定全域變數存取。
                Application.UnLock();
            }

            return string.Format("{0}", Application["TotalUsers_A"]);
        }
        #endregion
    }
}
