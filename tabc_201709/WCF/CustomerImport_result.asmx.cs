﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace tabc_201709.WCF
{
    /// <summary>
    ///CustomerImport_result 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [System.Web.Script.Services.ScriptService]
    public class CustomerImport_result : System.Web.Services.WebService
    {

        [WebMethod(enableSession: true)]
        public void ImportResultMsg()
        {
            HttpContext.Current.Server.ScriptTimeout = int.MaxValue;

            this.Context.Response.ContentType = "application/json";
            Dictionary<String, Object> dic = new Dictionary<string, object>();
            if (Session["CustomerImportMsg"] != null && Session["CustomerImportMsg"].ToString() != "")
                dic["result"] = Session["CustomerImportMsg"].ToString();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            this.Context.Response.Write(serializer.Serialize(dic));

        }
    }
}
