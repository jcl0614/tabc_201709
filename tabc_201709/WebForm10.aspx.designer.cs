﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------

namespace tabc_201709 {
    
    
    public partial class WebForm10 {
        
        /// <summary>
        /// form1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// ScriptManager1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.ScriptManager ScriptManager1;
        
        /// <summary>
        /// Image2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image Image2;
        
        /// <summary>
        /// lbl_msg0 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lbl_msg0;
        
        /// <summary>
        /// UpdatePanel2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel UpdatePanel2;
        
        /// <summary>
        /// Button1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button1;
        
        /// <summary>
        /// GridView1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridView1;
        
        /// <summary>
        /// Button2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button2;
        
        /// <summary>
        /// GridView2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridView2;
        
        /// <summary>
        /// GridView4 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridView4;
        
        /// <summary>
        /// Button3 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Button3;
        
        /// <summary>
        /// GridView3 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView GridView3;
    }
}
