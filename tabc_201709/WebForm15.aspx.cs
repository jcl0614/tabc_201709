﻿using AppCode.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm15 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string ApiURL = "http://w11.gouptech.com.tw/InsAPI/GetInsItemListByCmpy";
            //string IP = "192.168.1.1";
            //string no = string.Format("{0}|{1}|{2}|{3}|{4}", "1", "TW", DateTime.UtcNow.ToString("yyyyMMddHH"), "TABC", IP);
            //no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            //string err = "";
            //string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
            //if (err.Length == 0)
            //{
            //    DataTable dt = JsonConvert.DeserializeObject<DataTable>(ret);
            //    GridView1.DataSource = dt;
            //    GridView1.DataBind();
            //}
            if(!IsPostBack)
            {
                selPdt.iStatus = "1";
                selPdt.CompanyNo = "TW";
            }
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

    }
}