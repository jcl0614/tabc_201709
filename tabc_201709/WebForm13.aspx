﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="WebForm13.aspx.cs" Inherits="tabc_201709.WebForm13" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <table cellpadding="0" cellspacing="0" class="QueryMainTable">
            <tr>
                <td align="center" class="tdQueryHead">檔案名稱：</td>
                <td align="center" class="tdQueryData">
                    
                    <asp:DropDownList ID="ddlSh_category" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_category_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlSh_category_sub" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_category_sub_SelectedIndexChanged">
                    </asp:DropDownList>
                    
                </td>
                <td align="center" class="tdQueryHead">檔案名稱：</td>
                <td align="center" class="tdQueryData">
                    <asp:TextBox ID="txtSh_Name" runat="server"></asp:TextBox>
                </td>
                <td align="center" width="85">
                    <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                </td>
                <td align="center" width="35">
                    <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                </td>
            </tr>
        </table>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="檔案名稱">
                    <ItemTemplate>
                        <%# Eval("Name") %><%# (bool)Eval("isNew") ? "&nbsp;<img src='/images/Icon_New.gif' />" : "" %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="500px" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="格式">
                    <ItemTemplate>
                        <%# Eval("Extension") %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="大小">
                    <ItemTemplate>
                        <%# Eval("Length") %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="修改日期">
                    <ItemTemplate>
                        <%# Eval("LastWriteDate") %>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="100px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="下載">
                    <ItemTemplate>
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Manager/images/download-icon.png" CommandName='<%# Eval("FullName") %>' OnClick="ImageButton1_Click" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
