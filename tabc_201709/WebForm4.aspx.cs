﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Day", typeof(string));
            dt.Columns.Add("Week", typeof(string));
            dt.Columns.Add("Course_Unit", typeof(string));
            dt.Columns.Add("Course_Area", typeof(string));
            dt.Columns.Add("Course_TB", typeof(string));
            dt.Columns.Add("Memo", typeof(string));

            DataTable dt_course = DataECourse.DataReader_DepartmentCalendar("", DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString()).Tables[0];
            ViewState["dt_course"] = dt_course; 

            int days = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            for(int i=1;i<=days;i++)
            {
                //string nowDate = string.Format("#{0}/{1}/{2}#", DateTime.Now.Year, DateTime.Now.Month, i);
                //DataRow[] dr_course_day = dt_course.Select("EC_SDate=" + nowDate + "");
                //StringBuilder sb_EC_CName = new StringBuilder();
                //StringBuilder sb_EC_Memo = new StringBuilder();
                //foreach(DataRow dr in dr_course_day)
                //{
                //    sb_EC_CName.AppendFormat("{0}<br>", dr["EC_CName"].ToString());
                //    if (dr["EC_Memo"].ToString().Trim() != "")
                //        sb_EC_Memo.AppendFormat("{0}<br>", dr["EC_Memo"].ToString());
                //}
                string DayName = GetDayName(DateTime.Parse(string.Format("{0}/{1}/{2}", DateTime.Now.Year, DateTime.Now.Month, i)));
                dt.Rows.Add(i.ToString(), DayName, "", "", "", "");
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        private string GetDayName(DateTime today)
        {
            string result = "";

            if (today.DayOfWeek == DayOfWeek.Monday)
            {
                result = "一";
            }
            else if (today.DayOfWeek == DayOfWeek.Tuesday)
            {
                result = "二";
            }
            else if (today.DayOfWeek == DayOfWeek.Wednesday)
            {
                result = "三";
            }
            else if (today.DayOfWeek == DayOfWeek.Thursday)
            {
                result = "四";
            }
            else if (today.DayOfWeek == DayOfWeek.Friday)
            {
                result = "五";
            }
            else if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                result = "六";
            }
            else if (today.DayOfWeek == DayOfWeek.Sunday)
            {
                result = "日";
            }

            return result;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFBD6'");
                e.Row.Style.Add("border-bottom", "solid 1px #ffffff");
                if (e.Row.Cells[1].Text == "六" || e.Row.Cells[1].Text == "日")
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#e6e6fa");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#e6e6fa'");
                }
                else if (MainControls.ReplaceSpace(e.Row.Cells[5].Text.Trim()) != "")
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC7DA");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFC7DA'");
                }
                else
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#A0FFFF");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#A0FFFF'");
                }

                DataTable dt_course = ViewState["dt_course"] as DataTable;
                string nowDate = string.Format("#{0}/{1}/{2}#", DateTime.Now.Year, DateTime.Now.Month, e.Row.Cells[0].Text);
                DataRow[] dr_course_day = dt_course.Select("EC_SDate=" + nowDate + " and EC_Unit<>'TB'");
                DataTable dt_course_ = new DataTable();
                if (dr_course_day.Length != 0)
                    dt_course_ = dr_course_day.CopyToDataTable();
                ((Repeater)e.Row.Cells[2].Controls[1]).DataSource = dt_course_;
                ((Repeater)e.Row.Cells[2].Controls[1]).DataBind();
                ((Repeater)e.Row.Cells[3].Controls[1]).DataSource = dt_course_;
                ((Repeater)e.Row.Cells[3].Controls[1]).DataBind();
                DataRow[] dr_course_day2 = dt_course.Select("EC_SDate=" + nowDate + " and EC_Unit='TB'");
                DataTable dt_course2 = new DataTable();
                if (dr_course_day2.Length != 0)
                    dt_course2 = dr_course_day2.CopyToDataTable();
                ((Repeater)e.Row.Cells[4].Controls[1]).DataSource = dt_course2;
                ((Repeater)e.Row.Cells[4].Controls[1]).DataBind();
            }
        }

        protected void btn_Command(object sender, CommandEventArgs e)
        {
            
        }
    }
}