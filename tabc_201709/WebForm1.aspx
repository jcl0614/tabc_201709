﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="tabc_201709.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="jQuery/jquery-2.0.3.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
         <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        <br />
        <asp:GridView ID="GridView1" runat="server">
        </asp:GridView>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
    <div>
       
        <br />
        <asp:Panel ID="Panel1" runat="server"></asp:Panel>
                                            <br />
        <asp:Panel ID="Panel2" runat="server"></asp:Panel>
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Button" />
    </div>
        
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    <asp:ListItem>111111</asp:ListItem>
                    <asp:ListItem>222222</asp:ListItem>
                    <asp:ListItem>333333</asp:ListItem>
                </asp:DropDownList>
                <br />
                <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" Text="Button" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <script>
            Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(EndRequestHandler);
            function EndRequestHandler() {
                $(function () {

                    selectChangeConfirm('<%= DropDownList1.ClientID %>', "確定嗎?");
                });
            }
            function selectChangeConfirm(controlID, message) {
                var s = $('#' + controlID).val();
                $('#' + controlID).removeAttr('onchange');
                $('#' + controlID).change(function (e) {
                    e.preventDefault();
                    if (confirm(message)) {
                        setTimeout("__doPostBack('" + controlID + "','')", 0);
                        s = $('#' + controlID).val();
                    }
                    else {
                        $('#' + controlID).val(s);
                    }
                });
            }
        </script>
    </form>

</body>
</html>
