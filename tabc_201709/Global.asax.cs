﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using Microsoft.AspNet.FriendlyUrls;
using System.Web.Security;
using tabc_201709;
using NLog;
using System.Reflection;
using NLog.Targets;

namespace tabc_201709
{
    public class Global : HttpApplication
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes(RouteTable.Routes);

            var dbtarget = LogManager.Configuration.FindTargetByName("DB") as DatabaseTarget;
            if (dbtarget != null)
            {
                dbtarget.DBProvider = "System.Data.SqlClient";
                dbtarget.ConnectionString = Lib.PuchAesChinese(System.Configuration.ConfigurationManager.ConnectionStrings["MainDBConnectionString"].ConnectionString, DataSysValue.Connection);
                dbtarget.CommandText = "insert into sys_ErrorLog ([ApplyDate],[Logger],[Level],[Message]) values(@ApplyDate,@Logger,@Level,@Message);";
                //dbtarget.Parameters.Clear();
                //dbtarget.Parameters.Add(new DatabaseParameterInfo("ApplyDate", "${date}"));
                //dbtarget.Parameters.Add(new DatabaseParameterInfo("Logger", "${logger}"));
                //dbtarget.Parameters.Add(new DatabaseParameterInfo("Level", "${level}"));
                //dbtarget.Parameters.Add(new DatabaseParameterInfo("Message", "${message}"));
            }

            // 應用程式啟動時執行的程式碼
            Application["WebTitle"] = DataWebSet.dataReader("WebTitle");
            Application["WebUrl"] = DataWebSet.dataReader("WebUrl");
            Application["WebKeywords"] = DataWebSet.dataReader("WebKeywords");
            Application["WebDescription"] = DataWebSet.dataReader("WebDescription");
            Application["WebCopyright"] = DataWebSet.dataReader("WebCopyright");
            Application["WindowFull"] = DataWebSet.dataReader("WindowFull");

            Application["TotalUsers_A"] = 0;

            //EO.Web.Runtime.AddLicense(
            //"bEOXpLHLn1mXpM0M452X+Aob5HaZ0fYZ9FuX+vYd8qLm8s7NsGqltLPLrneE" +
            //"jrHLn1mXpLHLu5rb6LEf+KncwbP+66Lb6d4Q7a6ZpAcQ8azg8//ooWqossHN" +
            //"n2i1kZvLn1mXpLHLn3XY6PXL87Ln6c7N05rZ1wUd6KmZpAcQ8azg8//ooWqo" +
            //"ssHNn2i1kZvLn1mXpLHLn3XY6PXL87Ln6c7N06vc6ecU5LCZpAcQ8azg8//o" +
            //"oWqossHNn2i1kZvLn1mXpLHLn3XY6PXL87Ln6c7NzK7j+Pr74KDcprEh5Kvq" +
            //"7QAZvFuotb/boVmmwp61n1mXpLHLn1mz5fUPn63w9PbooXzY8PYZ45rpprEh" +
            //"5Kvq7QAZvFuotb/boVmmwp61n1mXpLHLn1mz5fUPn63w9PbooXzY8P0N4Jzi" +
            //"prEh5Kvq7QAZvFuotb/boVmmwp61n1mXpLHLn1mz5fUPn63w9PbooXrBxekA" +
            //"76Xm5fUQ8VuX+vYd8qLm8s7NsGqltLPLrneEjrHLn1mXpLHLu5rb6LEf+Knc" +
            //"wbPv6Jrj8/jNn6/c9gQU7qe0psLcrWmZpMDpjEOXpLHLn1mXpM0M452X+Aob" +
            //"5HaZ1wEX6K3r6QPNn6/c9gQU7qe0psLcrWmZpMDpjEOXpLHLn1mXpM0M452X" +
            //"+Aob5HaZ0fIe6p7byfUU81uX+vYd8qLm8s7NsGqltLPLrneEjrHLn1mXpLHL" +
            //"u5rb6LEf+KncwbPy8aLbprEh5Kvq7QAZvFuotb/boVmmwp61n1mXpLHLn1mz" +
            //"5fUPn63w9PbooXzm8AAdz6La7/YdoVnt6QMe6KjlwbPcsGenprHavUaBpLHL" +
            //"n1mXpLHn4J3bpAUk7560puQb5KXjx/kQ4qTc9rPL9Z7p9/oa7XaZtcLZr1uX" +
            //"s8+4iVmXpLHLn1mXwPIP41nr/QEQvFu86Pof7quZpAcQ8azg8//ooWqossHN" +
            //"n2i1kZvLn1mXpLHLn3XY6PXL87Ln6c7NyKbY6/YF7qjkprEh5Kvq7QAZvFuo" +
            //"tb/boVmmwp61n1mXpLHLn1mz5fUPn63w9PbooX3m+/8X7prb6QPNn6/c9gQU" +
            //"7qe0psLcrWmZpMDpjEOXpLHLn1mXpM0M452X+Aob5HaZyv0a4K3c9rPL9Z7p" +
            //"9/oa7XaZtcLZr1uXs8+4iVmXpLHLn1mXwPIP41nr/QEQvFvK8PoP5FuX+vYd" +
            //"8qLm8s7NsGqltLPLrneEjrHLn1mXpLHLu5rb6LEf+KncwbPx67Lm+QXNn6/c" +
            //"9gQU7qe0psLcrWmZpMDpjEOXpLHLn1mXpM0M452X+Aob5HaZyfUU85rZ8Pb3" +
            //"4Jvc8LPL9Z7p9/oa7XaZtcLZr1uXs8+4iVmXpLHLn1mXwPIP41nr/QEQvFvD" +
            //"7QQfwajvprEh5Kvq7QAZvFuotb/boVmmwp61n1mXpLHLn1mz5fUPn63w9Pbo" +
            //"oXzm8fMawajvprEh5Kvq7QAZvFuotb/boVmmwp61n1mXpLHLn1mz5fUPn63w" +
            //"9PbooXzY9AUO55qZpAcQ8azg8//ooWqossHNn2i1kZvLn1mXpLHLn3XY6PXL" +
            //"87Ln6c7N06jm8OUU71uX+vYd8qLm8s7NsGqltLPLrneEjrHLn1mXpLHLu5rb" +
            //"6LEf+KncwbP94K3g8vjNn6/c9gQU7qe0psLcrWmZpMDpjEOXpLHLn1mXpM0M" +
            //"452X+Aob5HaZ1/0U457pprEh5Kvq7QAZvFuotb/boVmmwp61n1mXpM3a4KXj" +
            //"8wjpjEOXpLHLu6jp6PYdyKfd87EP4K3cwbPirnCmtsHcs1uX+vYd8qLm8s7N" +
            //"sGqZpMDpjEOXpLHLu6zg6/8M867p6c8Qx4nxx/vyx6iu27zcwGjk2eD80XrI" +
            //"wc7nrqzg6/8M867p6c+4iXWm8PoO5Kfq6c+4iXXj7fQQ7azcwp61n1mXpM0X" +
            //"6Jzc8gQQyJ21vcHwsm2uvMMCuG2nt8Xhs26zs/0U4p7l9/b043eEjrHLn1mz" +
            //"8PoO5Kfq6fbp2J7fpN4M7lnK7PYZu2jj7fQQ7azc6c+4iVmXpLHn4KXj8wjp" +
            //"jA==");
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.EnableFriendlyUrls();
        }


        void Application_End(object sender, EventArgs e)
        {
            //  應用程式關閉時執行的程式碼

        }

        void Application_Error(object sender, EventArgs e)
        {
            // 發生未處理錯誤時執行的程式碼
            Exception ex = Server.GetLastError();

            if (Request != null)
            {
                if (((HttpException)(ex)).GetHttpCode() != 404)
                {
                    logger.Error(string.Format("來源頁面：{0}\r\n{1}", Request.Path, ex.ToString()));
                    logger_db.Error(string.Format("來源頁面：{0}\r\n{1}", Request.Path, ex.ToString()));

                    //清除Error 
                    //Server.ClearError();
                }
                else
                {
                    if (ex.Message.ToLower().IndexOf("/web/") != -1)
                        Response.Redirect("~/Web/Error");
                    else if (ex.Message.ToLower().IndexOf("/manager/") != -1)
                        Response.Redirect("~/Manager/Login");
                }
            }
            else
            {
                logger.Error(ex);
                logger_db.Error(ex);
            }
        }

        void Session_Start(object sender, EventArgs e)
        {
            // 啟動新工作階段時執行的程式碼
            Session["emUser"] = "";
            Session["emID"] = "";           //員工編號
            Session["emNo"] = "";           //員工代碼
            Session["emOS"] = "0";          //登入者限用平台
            Session["emDefOM"] = "";
            Session["emLimit"] = "";
            Session["userMenu"] = "";
            Session["memberNO"] = "";       //會員編號
            Session["PageUrl"] = "0";       //頁面網址

            Session["UidNo"] = "";
            Session["AccID"] = "";
            Session["ComyCode"] = "";
        }

        void Session_End(object sender, EventArgs e)
        {
            // 工作階段結束時執行的程式碼。 
            // 注意: 只有在 Web.config 檔將 sessionstate 模式設定為 InProc 時，
            // 才會引發 Session_End 事件。如果將工作階段模式設定為 StateServer 
            // 或 SQLServer，就不會引發這個事件。

        }
    }
}
