﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm10.aspx.cs" Inherits="tabc_201709.WebForm10" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="/jQuery/jquery-2.0.3.min.js" type="text/javascript"></script>
    <script>
        var isLoop = false;
        $(function () {
            
            $("#btn_Import").click(function () {
                if (confirm('確定要執行嗎？')) {
                    $("#btn_Import").hide();
                    $("#loading").show();
                    $("#ImportMsg").prepend('資料處理中...<br>');
                    //var interval = setInterval(function () {
                    //    $.ajax({
                    //        type: "post",
                    //        url: "/WCF/CustomerImport_result.asmx/ImportResultMsg",
                    //        success: function (response) {
                    //            if (response.result)
                    //                $("#ImportMsg").html(response.result);
                                
                    //        }
                    //    });
                    //}, 10000);
                    //isLoop = true;
                    //getImportResultMsg();
                    <%--$.ajax({
                        type: "post",
                        url: "/WCF/CustomerImport.asmx/Import",
                        success: function (response) {
                            if (response.result) {
                                alert(response.result);
                                $("#btn_Import").show();
                                $("#loading").hide();
                                if (response.ExceptionMsg)
                                    $("#ImportMsg").prepend(response.ExceptionMsg + '<br>');
                                $("#ImportMsg").prepend(response.result + '<br>');
                                //clearInterval(interval);
                                setTimeout(function () {
                                    isLoop = false;
                                }, 10000);
                                
                                <% Session["CustomerImportMsg"] = null; %>
                            }
                        },
                        //error: function (XMLHttpRequest, textStatus) {
                        //    if (textStatus == 'timeout')
                        //        alert('逾時');
                        //    else
                        //        alert('錯誤');
                        //}
                    });--%>

                    Import(1, -1);
                    
                }
            });
            
        });
        function Import(i, index) {
            $.ajax({
                type: "post",
                url: "/WCF/CustomerImport.asmx/Import",
                data: {
                    i: i,
                    index: index
                },
                success: function (response) {
                    if (response.result) {
                        if (!response.i) {
                            $("#btn_Import").show();
                            $("#loading").hide();
                            $("#progress").text('');
                            $("#progress").width(0);
                            alert("執行完成！");
                        }
                        else {
                            Import(response.i, response.index);
                        }
                        
                        $("#ImportMsg").prepend(response.result);
                        if (response.ExceptionMsg)
                            $("#ImportMsg").prepend(response.ExceptionMsg + '<br>');
                        if (response.progress_val) {
                            $("#progress_msg").text(response.progress_msg);
                            $("#progress_bar").animate({ width: response.progress_val * 500 });
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#btn_Import").show();
                    $("#loading").hide();
                    alert('執行中止！\n例外中止訊息：' + xhr.responseText);
                }
            });
        }
        function getImportResultMsg()
        {
            if (isLoop) {
                $.ajax({
                    type: "post",
                    url: "/WCF/CustomerImport_result.asmx/ImportResultMsg",
                    success: function (response) {
                        if (response.result) {
                            $("#ImportMsg").html(response.result);
                            getImportResultMsg();
                        }

                    }
                });
            }
        }
        function onSuccess(msg) {
            $("#ImportMsg").html(msg);
            //$("#ImportMsg").animate({ scrollTop: $("#ImportMsg").prop("scrollHeight") }, 1000);
        }
        function onError(error) {
            if (error != null)
                alert(error.get_message());
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
    <div>
        
                
        <input id="btn_Import" type="button" value="客戶資料匯入" style="cursor: pointer" />
        <div id="loading" style="display:none;">
            <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Manager/images/loading.gif" />
                    </td>
                <td width="5">
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_msg0" runat="server" Font-Size="Small" ForeColor="#2C6A9C" 
                        Text="執行中．．．"></asp:Label>
                </td>
                <td style="border: 1px solid #dbdbdb; position: relative; background-color: #efefef; width: 500px;">
                    <div id="progress_msg" style="font-family: 微軟正黑體; font-size: 14px; color: #333333; text-align: center; width: 500px; padding-top: 5px; padding-bottom: 5px; position: absolute; top: 0px; z-index: 2;">0%</div>
                    <div id="progress_bar" style="font-family: 微軟正黑體; font-size: 14px; background-position: bottom; background-repeat: repeat-x; background-image: url('/Manager/images/bg_tab.png'); color: #FFFFFF; text-align: center; padding-top: 5px; padding-bottom: 5px; width: 0px; position: absolute; top: 0px;">&nbsp;</div>
                </td>
            </tr>
        </table>
        </div>
        
        <div id="ImportMsg" style="font-size: 14px; font-family:微軟正黑體; height:600px; overflow:auto;"></div>

        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" Text="保單資料" OnClick="Button1_Click" style="height: 21px" />
                <br />
                <asp:GridView ID="GridView1" runat="server">
                </asp:GridView>
                <br />
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="保單明細" />
                <br />
                <asp:GridView ID="GridView2" runat="server">
                </asp:GridView>
                <asp:GridView ID="GridView4" runat="server">
                </asp:GridView>
                <br />
                <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="昇華-險種" />
                <br />
                <asp:GridView ID="GridView3" runat="server">
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        
    </form>
</body>
</html>
