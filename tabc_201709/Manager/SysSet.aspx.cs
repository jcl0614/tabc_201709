﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace tabc_201709.Manager
{
    public partial class SysSet : System.Web.UI.Page
    {
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.SetFocus(txt_WebMapIP);
            Panel_Form.DefaultButton = btnUppend.ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //預設更新按鈕
                btnUppend.Enabled = true & (ViewState["SysValue"] as SysValue).Authority.Update;
                //讀取資料
                dataBind();
            }
        }

        #region 讀取資料
        private void dataBind()
        {
            txt_WebMapIP.Text = DataWebSet.dataReader("WebMapIP");
            txt_WebMailServer.Text = DataWebSet.dataReader("WebMailServer");
            txt_WebSendMail.Text = DataWebSet.dataReader("WebSendMail");
            txt_WebSendMailID.Text = DataWebSet.dataReader("WebSendMailID");
            txt_WebSendMailPWD.Text = DataWebSet.dataReader("WebSendMailPWD");
            txt_WebAcceptMail.Text = DataWebSet.dataReader("WebAcceptMail");
            txt_SysFtpID.Text = DataWebSet.dataReader("sys_ftpID");
            txt_SysFtpPWD.Text = DataWebSet.dataReader("sys_ftpPWD");
            txt_SysFtpHost.Text = DataWebSet.dataReader("sys_ftpHost");
            txt_sysLogout.Text = DataWebSet.dataReader("SysLogoutTime");
            txt_gMail.Text = DataWebSet.dataReader("gMail");
            txt_gPwd.Text = DataWebSet.dataReader("gPwd");
            txt_gWebId.Text = DataWebSet.dataReader("gWebId");
            txt_gCAPTCHA_SiteKey.Text = DataWebSet.dataReader("gCAPTCHA_SiteKey");
            txt_gCAPTCHA_Secretkey.Text = DataWebSet.dataReader("gCAPTCHA_Secretkey");
            txt_smsID.Text = DataWebSet.dataReader("smsID");
            txt_smsPWD.Text = DataWebSet.dataReader("smsPWD");
            MainControls.ddlIndexSelectValue(ddl_Recaptcha, DataWebSet.dataReader("SysLoginRecaptcha"));
            try
            {
                lbl_smsPoint.Text = string.Format("剩餘點數：{0}", Encoding.GetEncoding("big5").GetString(new WebClient().DownloadData(string.Format("http://mail2sms.com.tw/memberpoint.php?username={0}&password={1}", txt_smsID.Text.Trim(), Lib.PuchAesChinese(txt_smsPWD.Text.Trim(), DataSysValue.Password)))).Replace("\n", ""));
            }catch(Exception ex)
            {
                lbl_smsPoint.Text = "無法連線伺服器";
            }
            txt_webLogout.Text = DataWebSet.dataReader("WebLogoutTime");
            cbox_WebEnable.Checked = DataWebSet.dataReader("WebEnable") == "True" ? true : false;
            cbox_WebCopyEnable.Checked = DataWebSet.dataReader("WebCopyEnable") == "True" ? true : false;
            cbox_SSL.Checked = DataWebSet.dataReader("SSL") == "True" ? true : false;
            cbox_WindowFull.Checked = DataWebSet.dataReader("WindowFull") == "True" ? true : false;
            txt_WebMessage.Text = DataWebSet.dataReader("WebMessage");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            //if (!MainControls.ValidDataType("^(\\d+)(\\.\\d+)(\\.\\d+)(\\.\\d+)?$", txt_WebMapIP.Text.Trim()))
            //    sbError.Append("●「網站對映IP」格式錯誤!<br>");
            //if (txt_WebMailServer.Text.Trim() == "")
            //    sbError.Append("●「發信伺服器」必須輸入!<br>");
            //else
            //    if (!MainControls.ValidDataType("([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?", txt_WebMailServer.Text.Trim()))
            //        sbError.Append("●「發信伺服器」格式錯誤!<br>");
            //if (txt_WebSendMail.Text.Trim() == "")
            //    sbError.Append("●「發信郵件」必須輸入!<br>");
            //else
            //    if (!MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$", txt_WebSendMail.Text.Trim()))
            //        sbError.Append("●「發信郵件」格式錯誤!<br>");
            //if (txt_WebSendMailID.Text.Trim() == "")
            //    sbError.Append("●「發信郵件帳號」必須輸入!<br>");
            //if (txt_WebSendMailPWD.Text.Trim() == "")
            //    sbError.Append("●「發信郵件密碼」必須輸入!<br>");
            //if (txt_WebAcceptMail.Text.Trim() != "")
            //    if (!MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-;]+)+", txt_WebAcceptMail.Text.Trim()))
            //        sbError.Append("●「收信郵件」格式錯誤!<br>");
            if (txt_sysLogout.Text.Trim() == "")
                sbError.Append("●「系統登出時間」必須輸入!<br>");
            else
                if (int.Parse(txt_sysLogout.Text.Trim()) < 0 || !MainControls.ValidDataType("^\\d+$", (txt_sysLogout.Text.Trim())))
                    sbError.Append("●「系統登出時間」格式錯誤!<br>");
            if (txt_webLogout.Text.Trim() == "")
                sbError.Append("●「網站登出時間」必須輸入!<br>");
            else
                if (int.Parse(txt_webLogout.Text.Trim()) < 0 || !MainControls.ValidDataType("^\\d+$", (txt_webLogout.Text.Trim())))
                    sbError.Append("●「網站登出時間」格式錯誤!<br>");
            if (txt_gMail.Text.Trim() != "")
                if (!MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$", txt_gMail.Text.Trim()))
                    sbError.Append("●「Google 帳號」格式錯誤!<br>");


            return sbError.ToString();
        }

        #endregion

        #region 資料更新
        protected void btnUppend_Click(object sender, EventArgs e)
        {
            if (dataValid() != "")
            {
                labMsg.Text = dataValid();
                return;
            }
            else
            {
                string[] NameVale = new string[] { string.Format("{0},{1}", "WebMapIP", Lib.FdVP(txt_WebMapIP.Text.Trim())), 
                                           string.Format("{0},{1}", "WebMailServer", Lib.FdVP(txt_WebMailServer.Text.Trim())), 
                                           string.Format("{0},{1}", "WebSendMail", Lib.FdVP(txt_WebSendMail.Text.Trim())), 
                                           string.Format("{0},{1}", "WebSendMailID", txt_WebSendMailID.Text.Trim()),
                                           string.Format("{0},{1}", "WebSendMailPWD", txt_WebSendMailPWD.Text.Trim()),
                                           string.Format("{0},{1}", "WebAcceptMail", Lib.FdVP(txt_WebAcceptMail.Text.Trim())), 
                                           string.Format("{0},{1}", "sys_ftpID", txt_SysFtpID.Text.Trim()),
                                           string.Format("{0},{1}", "sys_ftpPWD", txt_SysFtpPWD.Text.Trim()),
                                           string.Format("{0},{1}", "sys_ftpHost", txt_SysFtpHost.Text.Trim()),
                                           string.Format("{0},{1}", "SysLogoutTime", Lib.FdVP(txt_sysLogout.Text.Trim())),
                                           string.Format("{0},{1}", "WebLogoutTime", Lib.FdVP(txt_webLogout.Text.Trim())),
                                           string.Format("{0},{1}", "gMail", txt_gMail.Text.Trim()),
                                           string.Format("{0},{1}", "gPwd", txt_gPwd.Text.Trim()),
                                           string.Format("{0},{1}", "gWebId", txt_gWebId.Text.Trim()),
                                           string.Format("{0},{1}", "gCAPTCHA_SiteKey", txt_gCAPTCHA_SiteKey.Text.Trim()),
                                           string.Format("{0},{1}", "gCAPTCHA_Secretkey", txt_gCAPTCHA_Secretkey.Text.Trim()),
                                           string.Format("{0},{1}", "smsID", txt_smsID.Text.Trim()),
                                           string.Format("{0},{1}", "smsPWD", txt_smsPWD.Text.Trim()),
                                           string.Format("{0},{1}", "WebEnable", cbox_WebEnable.Checked.ToString()), 
                                           string.Format("{0},{1}", "WebCopyEnable", cbox_WebCopyEnable.Checked.ToString()),
                                           string.Format("{0},{1}", "SSL", cbox_SSL.Checked.ToString()),
                                           string.Format("{0},{1}", "WindowFull", cbox_WindowFull.Checked.ToString()),
                                           string.Format("{0},{1}", "SysLoginRecaptcha", ddl_Recaptcha.SelectedValue),
                                           string.Format("{0},{1}", "WebMessage", Lib.FdVP(txt_WebMessage.Text.Trim())) };
                RServiceProvider rsp = DataWebSet.updateWebSet(NameVale);
                if (rsp.Result)
                {
                    updateApplication();
                    labMsg.Text = "更新成功!";
                }
                else
                    labMsg.Text = rsp.ReturnMessage;
            }
            //剩餘簡訊點數
            try
            {
                lbl_smsPoint.Text = string.Format("剩餘點數：{0}", Encoding.GetEncoding("big5").GetString(new WebClient().DownloadData(string.Format("http://mail2sms.com.tw/memberpoint.php?username={0}&password={1}", txt_smsID.Text.Trim(), Lib.PuchAesChinese(txt_smsPWD.Text.Trim(), DataSysValue.Password)))).Replace("\n", ""));
            }
            catch (Exception ex) { }
        }
        #endregion

        #region 修正Application_Start
        private void updateApplication()
        {
            if (bool.Parse(Application["WindowFull"].ToString()) != cbox_WindowFull.Checked)
            {
                Application["WindowFull"] = cbox_WindowFull.Checked;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Login", "window.opener=null; window.open('', '_self', ''); window.close();", true);
            }

        }
        #endregion

        #region 發信測試
        protected void btn_SendMail_Click(object sender, EventArgs e)
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_WebMailServer.Text.Trim() == "")
                sbError.Append("「發信伺服器」必須輸入!\\n");
            else
                if (!MainControls.ValidDataType("([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?", txt_WebMailServer.Text.Trim()))
                    sbError.Append("「發信伺服器」格式錯誤!\\n");
            if (txt_WebSendMail.Text.Trim() == "")
                sbError.Append("「發信郵件」必須輸入!\\n");
            else
                if (!MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$", txt_WebSendMail.Text.Trim()))
                    sbError.Append("「發信郵件」格式錯誤!\\n");
            if (txt_WebSendMailID.Text.Trim() == "")
                sbError.Append("「發信郵件帳號」必須輸入!\\n");
            if (txt_WebSendMailPWD.Text.Trim() == "")
                sbError.Append("「發信郵件密碼」必須輸入!\\n");

            if (string.IsNullOrEmpty(sbError.ToString()))
            {
                RServiceProvider rsp = MainControls.sendMail(new WebSetValue().WebSendMail, new WebSetValue().WebSendMail, "", "", "系統發信測試", string.Format("發信伺服器：{0}<br>發信郵件：{1}", new WebSetValue().WebMailServer, new WebSetValue().WebSendMail), "", "系統發信測試", true);
                if (rsp.Result)
                    MainControls.showMsg(this, "發信成功!");
                else
                    MainControls.showMsg(this, string.Format("發信失敗!\\n{0}", rsp.ReturnMessage));
            }
            else
                MainControls.showMsg(this, sbError.ToString());
        }
        #endregion

        #region 寄發簡訊測試
        protected void btn_sendSMS_Click(object sender, EventArgs e)
        {
            try
            {
                if (!MainControls.ValidDataType("09\\d{8,8}", txt_smsTest.Text.Trim()))
                    MainControls.showMsg(this, "手機號碼格式錯誤!");
                else
                {
                    string s = MainControls.SendPhoneMsg(txt_smsTest.Text.Trim(), "系統寄發簡訊測試!  " + DateTime.Now.ToString(), "");
                    if (s.IndexOf("-") != -1)
                        MainControls.showMsg(this, string.Format("發送簡訊至 {0} 失敗!", txt_smsTest.Text.Trim()));
                    else
                        MainControls.showMsg(this, string.Format("發送簡訊至 {0} 成功!", txt_smsTest.Text.Trim()));
                }
                txt_smsTest.Text = string.Empty;
                try
                {
                    //剩餘簡訊點數
                    lbl_smsPoint.Text = string.Format("剩餘點數：{0}", Encoding.GetEncoding("big5").GetString(new WebClient().DownloadData(string.Format("http://mail2sms.com.tw/memberpoint.php?username={0}&password={1}", txt_smsID.Text.Trim(), Lib.PuchAesChinese(txt_smsPWD.Text.Trim(), DataSysValue.Password)))).Replace("\n", ""));
                }
                catch (Exception ex) { }
            }
            catch(Exception ex)
            {
                lbl_smsPoint.Text = "無法連線伺服器";
                MainControls.showMsg(this, string.Format("發送失敗!\\n{0}", ex.Message));
            }
        }
        #endregion

        protected void btn_update1_Click(object sender, EventArgs e)
        {
            int count_sucess = 0, count_fail = 0;
            DataTable dt = DataPerson.baseDataReader(true, "").Tables[0];
            foreach (DataRow dr in dt.Rows)
            {
                RServiceProvider rsp = employeeApply(dr["AccID"].ToString(), dr["AccID"].ToString(), dr["PS_PW"].ToString(), dr["PS_NAME"].ToString(), dr["AccID"].ToString());
                if (rsp.Result)
                    count_sucess += 1;
                else
                    count_fail += 1;
            }
            MainControls.showMsg(this, string.Format("執行完成！\\n{0}筆同步完成；{1}筆同步失敗", count_sucess, count_fail));
        }

        private void DoTrans()
        {
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                            "  <Requests Name=\"業務員基本資料\" Text=\"Request\" RB=\"RB(N)\">" +
                            "   <Request Name=\"業務員基本資料\" Program=\"TM_A001_Request\" MaxRows=\"0\" MinLags=\"0\">" +
                            "    <Parameters>" +
                            "     <Parameter Name=\"@P_cUSER_FK\" Value=\"'ALL'\"/>" +//'{USR_PK}'
                            "    </Parameters>" +
                            "   </Request>" +
                            "  </Requests>" +
                            " </BIS>";
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(ret);
                XmlNodeList NodeList = doc.SelectNodes("BIS/Responds/Respond/N");
                DataTable dtDisPlay = new DataTable();    //業務員基本資料表
                Dictionary<string, int> dictItem = new Dictionary<string, int>();
                //
                dictItem.Add("編號", 80);
                dictItem.Add("密碼", 100);
                dictItem.Add("身分證號", 100);
                dictItem.Add("姓名", 80);
                dictItem.Add("狀態", 80);
                dictItem.Add("壽險登入證號", 80);
                dictItem.Add("產險登入證號", 100);
                dictItem.Add("壽險登入日期", 100);
                dictItem.Add("產險登入日期", 80);
                dictItem.Add("壽險撤登日期", 100);
                dictItem.Add("產險撤登日期", 80);
                dictItem.Add("職級", 100);

                foreach (KeyValuePair<string, int> kvp in dictItem)
                {
                    dtDisPlay.Columns.Add(kvp.Key);
                }
                int count_sucess = 0, count_fail = 0;
                foreach (XmlNode COB in NodeList)
                {
                    DataRow drnew = dtDisPlay.NewRow();
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        if (dictItem.ContainsKey(Attr.Name.ToString()))
                        {
                            drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value.ToString().Trim();
                        }
                    }
                    RServiceProvider rsp = employeeApply(drnew["編號"].ToString(), drnew["編號"].ToString(), drnew["密碼"].ToString(), drnew["姓名"].ToString(), drnew["編號"].ToString());
                    if (rsp.Result)
                        count_sucess += 1;
                    else
                        count_fail += 1;
                    drnew.EndEdit();
                    dtDisPlay.Rows.Add(drnew);
                }
                MainControls.showMsg(this, string.Format("執行完成！\\n{0}筆同步完成；{1}筆同步失敗", count_sucess, count_fail));
                #endregion



            }
        }

        private RServiceProvider employeeApply(string emNo, string emID, string emPWD, string emName, string AccID)
        {
            DataSet ds = new DataSet();

            RServiceProvider rsp = new RServiceProvider();
            if (DataEmployee.emNoValid(emNo))
            {
                try
                {
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema2(true), dataToHashtable_employee(emNo, emID, emPWD, emName, AccID, true)));
                    rsp = DataEmployee.Append("00000000", "sys_Employee", ds);
                }
                catch (Exception ex) { }
            }
            else
            {
                try
                {
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema2(false), dataToHashtable_employee(emNo, emID, emPWD, emName, AccID, false)));
                    rsp = DataEmployee.Update("00000000", "sys_Employee", ds);
                }
                catch (Exception ex) { }
            }
            return rsp;
        }

        private Hashtable dataToHashtable_employee(string emNo, string emID, string emPWD, string emName, string AccID, bool isApply)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("emNo", emNo);
            hsData.Add("emID", emID);
            hsData.Add("emPWD", Lib.GetMD5(emPWD));
            hsData.Add("emName", emName);
            hsData.Add("emSex", null);
            hsData.Add("emMail", null);
            hsData.Add("emPhone", null);
            DataRow dr = Data_tabcSales.DataReader_AccID(AccID);
            if(dr != null)
            {
                if(dr["Leave_Date"].ToString() != "")
                {
                    if(DateTime.Parse(dr["Leave_Date"].ToString()).ToString("yyyy/MM/dd") == "1900/01/01")
                        hsData.Add("emEnable", "True");
                    else
                        hsData.Add("emEnable", "False");
                }
                else
                    hsData.Add("emEnable", "False");
            }
            else
                hsData.Add("emEnable", "False");
            hsData.Add("ipLimit", null);
            if (isApply)
                hsData.Add("EmGroup", "2");
            hsData.Add("AccID", AccID);

            return hsData;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        public class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        protected void btn_update2_Click(object sender, EventArgs e)
        {
            DoTrans();
            
        }
    }
}