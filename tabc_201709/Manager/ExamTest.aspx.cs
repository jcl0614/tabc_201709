﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ExamTest : BasePage
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));
        private static string pageTitle = "";

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
            if (Request.QueryString["c"] != null && Request.QueryString["i"] != null)
                QuesInit();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EG_Name);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                pageTitle = lbl_PageTitle.Text;
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                if (Request.QueryString["id"] != null)
                {
                    DataRow dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
                    if (dr_EB != null)
                    {
                        ViewState["dt_EB"] = dr_EB.Table;
                        if (Request.QueryString["c"] != null)
                        {
                            switch (Request.QueryString["c"])
                            {
                                case "1":

                                    ChangeMultiView("1");
                                    break;
                                case "2":

                                    ChangeMultiView("2");
                                    break;
                                case "3":

                                    ChangeMultiView("3");
                                    break;
                                case "4":

                                    ChangeMultiView("4");
                                    break;
                                default:
                                    ChangeMultiView("0");
                                    break;
                            }
                        }
                        else
                            ChangeMultiView("0");
                    }
                    else
                        MainControls.showMsg(this, "證照考試不存在！");
                }
                else
                    ChangeMultiView("0");
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataExamSub.DataColumn_test(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EG_Type"] = DataPhrase.DDL_TypeName(ddlSh_EG_Type, true, "", "144");
            //ViewState["ddl_EG_Type"] = DataPhrase.DDL_TypeName(ddl_EG_Type, false, "", "144");
            //DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
            //DataPhrase.DDL_TypeName(ddl_EG_Subject, false, "", "138");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();



            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_EP = new DataSet();

            switch (e.CommandName)
            {
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable_EX(string ModiState, string EX_Score)
        {
            Hashtable hsData = new Hashtable();

            hif_EX_TransNo.Value = System.Guid.NewGuid().ToString("N");

            hsData.Add("EX_TransNo", hif_EX_TransNo.Value);
            hsData.Add("EX_UidNo", Session["UidNo"].ToString());
            DataRow dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
            if (dr_EB != null)
            {
                hsData.Add("EB_TransNo", dr_EB["EB_TransNo"].ToString());
                hsData.Add("EB_UidNo", dr_EB["EB_UidNo"].ToString());
                DataRow dr_EE = DataEQRule.DataReader_EE_ExamType_EE_Subject_EE_Item(dr_EB["EB_SubType"].ToString(), dr_EB["EB_Subject"].ToString(), Request.QueryString["i"]);
                if (dr_EE != null)
                {
                    hsData.Add("EE_TransNo", dr_EE["EE_TransNo"].ToString());
                    hsData.Add("EE_UidNo", dr_EE["EE_UidNo"].ToString());
                }

            }

            hsData.Add("EX_Date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("EX_Score", EX_Score);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_E9(string ModiState,
            string EQ_TransNo, string EQ_UidNo,
            string E9_OrderNo, string E9_Question, string E9_AnsListItem, string E9_Answer, string E9_Score,
            string EA_TransNo1, string EA_UidNo1, string EA_TransNo2, string EA_UidNo2, string EA_TransNo3, string EA_UidNo3, string EA_TransNo4, string EA_UidNo4,
            string E9_IsSelected1, string E9_IsCorrect1, string E9_IsSelected2, string E9_IsCorrect2, string E9_IsSelected3, string E9_IsCorrect3, string E9_IsSelected4, string E9_IsCorrect4
            )
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("E9_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("E9_UidNo", Session["UidNo"].ToString());
            hsData.Add("EX_TransNo", hif_EX_TransNo.Value);
            hsData.Add("EX_UidNo", Session["UidNo"].ToString());
            DataRow dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
            if (dr_EB != null)
            {
                hsData.Add("EB_TransNo", dr_EB["EB_TransNo"].ToString());
                hsData.Add("EB_UidNo", dr_EB["EB_UidNo"].ToString());
            }
            hsData.Add("EQ_TransNo", EQ_TransNo);
            hsData.Add("EQ_UidNo", EQ_UidNo);
            hsData.Add("E9_OrderNo", E9_OrderNo);
            hsData.Add("E9_Question", E9_Question);
            hsData.Add("E9_AnsListItem", E9_AnsListItem);
            hsData.Add("E9_Answer", E9_Answer);
            hsData.Add("E9_Score", E9_Score);
            hsData.Add("EA_TransNo1", EA_TransNo1);
            hsData.Add("EA_UidNo1", EA_UidNo1);
            hsData.Add("EA_TransNo2", EA_TransNo2);
            hsData.Add("EA_UidNo2", EA_UidNo2);
            hsData.Add("EA_TransNo3", EA_TransNo3);
            hsData.Add("EA_UidNo3", EA_UidNo3);
            hsData.Add("EA_TransNo4", EA_TransNo4);
            hsData.Add("EA_UidNo4", EA_UidNo4);
            hsData.Add("E9_IsSelected1", E9_IsSelected1 == "1" ? true : false);
            hsData.Add("E9_IsCorrect1", E9_IsCorrect1);
            hsData.Add("E9_IsSelected2", E9_IsSelected2 == "1" ? true : false);
            hsData.Add("E9_IsCorrect2", E9_IsCorrect2);
            hsData.Add("E9_IsSelected3", E9_IsSelected3 == "1" ? true : false);
            hsData.Add("E9_IsCorrect3", E9_IsCorrect3);
            hsData.Add("E9_IsSelected4", E9_IsSelected4 == "1" ? true : false);
            hsData.Add("E9_IsCorrect4", E9_IsCorrect4);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {

        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(e.CommandName);
        }

        private void ChangeMultiView(string CommandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataRow dr_EB = null;
            switch (CommandName)
            {
                case "1":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "進入測驗");
                    if (ViewState["dt_EB"] != null)
                        dr_EB = (ViewState["dt_EB"] as DataTable).Rows[0];
                        
                    else
                        dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
                    if (dr_EB != null)
                    {
                        ViewState["dt_EB"] = dr_EB.Table;
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr_EB["EB_Subject"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Subject.Text = dr_Phrase["TypeName"].ToString();
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_EB["EB_SubType"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                        lbl_EB_SubTypeName.Text = string.Format("{0}-{1}", lbl_EG_Type.Text, lbl_EG_Subject.Text);
                        lbl_EB_Warnings.Text = dr_EB["EB_Warnings"].ToString();
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_ID0.Text = dr_person["PS_ID"].ToString();
                            lbl_PS_NAME0.Text = dr_person["PS_NAME"].ToString();

                        }
                        DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit0.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                            DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                            if (dr_sales2 != null)
                            {
                                lbl_DM_Name0.Text = dr_sales2["NAME"].ToString();
                            }
                        }
                        DataRow dr_Phrase_ = DataPhrase.dataReader_TypeCode_TypeNo("163", Request.QueryString["i"]);
                        if (dr_Phrase_ != null)
                            lbl_EE_ItemName.Text = dr_Phrase_["TypeName"].ToString();
                    }
                    break;
                case "2":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "開始測驗");
                        Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                        if (!cbox_agree.Checked)
                        {
                            MainControls.showMsg(this, "請確認資訊並勾選「我知道了」。");
                            return;
                        }
                        else if (ques_list.Count == 0)
                        {
                            MainControls.showMsg(this, "模擬測驗題庫尚未建立，請聯絡相關人員確認。");
                            return;
                        }
                        else
                        {
                            if (ViewState["dt_EB"] != null)
                                dr_EB = (ViewState["dt_EB"] as DataTable).Rows[0];

                            else
                                dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
                            if (dr_EB != null)
                            {
                                ViewState["dt_EB"] = dr_EB.Table;
                                lbl_EB_SubTypeName0.Text = lbl_EB_SubTypeName.Text;
                                lbl_PS_ID1.Text = lbl_PS_ID0.Text;
                                lbl_PS_NAME1.Text = lbl_PS_NAME0.Text;
                                lbl_PS_Unit1.Text = lbl_PS_Unit0.Text;
                                lbl_DM_Name1.Text = lbl_DM_Name0.Text;
                                lbl_EG_Subject1.Text = lbl_EG_Subject.Text;
                                lbl_ExamDate1.Text = DateTime.Now.ToString("yyyy/MM/dd");
                                DataRow dr_Phrase_ = DataPhrase.dataReader_TypeCode_TypeNo("163", Request.QueryString["i"]);
                                if (dr_Phrase_ != null)
                                    lbl_EE_ItemName0.Text = dr_Phrase_["TypeName"].ToString();

                                lbl_EX_STime.Text = DateTime.Now.ToString("HH:mm");
                                int ExamTime = 0;
                                DataRow dr_EE = DataEQRule.DataReader_EE_ExamType_EE_Subject(dr_EB["EB_SubType"].ToString(), dr_EB["EB_Subject"].ToString());
                                if (dr_EE != null)
                                {
                                    int.TryParse(dr_EE["EE_TimeLength"].ToString(), out ExamTime);
                                }

                                Session["eTime"] = DateTime.Now.AddMinutes(ExamTime);
                                Timer2.Enabled = true;
                            }

                        }
                    }
                    break;
                case "3":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "完成測驗");
                        Timer2.Enabled = false;
                        if (ViewState["dt_EB"] != null)
                            dr_EB = (ViewState["dt_EB"] as DataTable).Rows[0];
                        else
                            dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
                        if (dr_EB != null)
                        {
                            ViewState["dt_EB"] = dr_EB.Table;
                            lbl_EB_SubTypeName1.Text = lbl_EB_SubTypeName.Text;
                            lbl_PS_ID2.Text = lbl_PS_ID0.Text;
                            lbl_PS_NAME2.Text = lbl_PS_NAME0.Text;
                            lbl_PS_Unit2.Text = lbl_PS_Unit0.Text;
                            lbl_DM_Name2.Text = lbl_DM_Name0.Text;
                            lbl_EG_Subject2.Text = lbl_EG_Subject.Text;
                            lbl_ExamDate2.Text = DateTime.Now.ToString("yyyy/MM/dd");
                            lbl_EX_STime0.Text = lbl_EX_STime.Text;
                            lbl_EX_ETime0.Text = lbl_EX_ETime.Text;
                            DataRow dr_Phrase_ = DataPhrase.dataReader_TypeCode_TypeNo("163", Request.QueryString["i"]);
                            if (dr_Phrase_ != null)
                                lbl_EE_ItemName1.Text = dr_Phrase_["TypeName"].ToString();

                            Panel_EA_ListItem2.Controls.Clear();
                            int EX_Score = 0;
                            int EE_TrueFalseS = 0;
                            int EE_OneChoiceS = 0;
                            int EE_MultiChoiceS = 0;
                            DataRow dr_EE = DataEQRule.DataReader_EE_ExamType_EE_Subject(dr_EB["EB_SubType"].ToString(), dr_EB["EB_Subject"].ToString());
                            if (dr_EE != null)
                            {
                                int.TryParse(dr_EE["EE_TrueFalseS"].ToString(), out EE_TrueFalseS);
                                int.TryParse(dr_EE["EE_OneChoiceS"].ToString(), out EE_OneChoiceS);
                                int.TryParse(dr_EE["EE_MultiChoiceS"].ToString(), out EE_MultiChoiceS);
                            }
                            int NO = 1;
                            #region 寫入交易 測驗結果 tblExam
                            DataSet ds_EX = new DataSet();
                            RServiceProvider rsp = new RServiceProvider();
                            //if (!this.IsRefresh)
                            //{
                            ds_EX.Tables.Add(MainControls.UpLoadToDataTable(DataExam.GetSchema(), dataToHashtable_EX("A", EX_Score.ToString())));
                            rsp = DataExam.AppendTransaction(sysValue.emNo, sysValue.ProgId, ds_EX);
                            if (rsp.Result) //執行成功
                            {

                            }
                            else
                            {
                                MainControls.showMsg(this, "證照模擬測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                logger.Error(string.Format("數位課程測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                logger_db.Error(string.Format("數位課程測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                            }
                            //}
                            #endregion
                            Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                            foreach (var ques in ques_list)
                            {
                                DataRow dr = DataEQues.DataReader_EQ_TransNo(ques.Key);
                                if (dr != null)
                                {
                                    string EQ_TransNo = "", EQ_UidNo = "",
                                        E9_OrderNo = "", E9_Question = "", E9_AnsListItem = "", E9_Answer = "", E9_Score = "",
                                        EA_TransNo1 = "", EA_UidNo1 = "", EA_TransNo2 = "", EA_UidNo2 = "", EA_TransNo3 = "", EA_UidNo3 = "", EA_TransNo4 = "", EA_UidNo4 = "",
                                        E9_IsSelected1 = "", E9_IsCorrect1 = "", E9_IsSelected2 = "", E9_IsCorrect2 = "", E9_IsSelected3 = "", E9_IsCorrect3 = "", E9_IsSelected4 = "", E9_IsCorrect4 = "";
                                    EQ_TransNo = dr["EQ_TransNo"].ToString();
                                    EQ_UidNo = dr["EQ_UidNo"].ToString();
                                    E9_OrderNo = NO.ToString();
                                    E9_Question = dr["EQ_Question"].ToString();
                                    switch (dr["EQ_Type"].ToString())
                                    {
                                        case "是非題":
                                            {
                                                Label lbl_ques = new Label();
                                                lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                                Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                                SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                                SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                                SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                                bool ansResult = false;
                                                StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                                foreach (var ans in ques.Value)
                                                {
                                                    DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                    text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                    RadioButton rbtn = ((RadioButton)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                    isChecked.Add(ans.Key, rbtn.Checked);
                                                    if (dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked)
                                                        ansResult = true;
                                                    if (rbtn.Checked)
                                                        E9_Answer = string.Format("{0}.{1}", ans.Key + 1, rbtn.Text);
                                                    sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, rbtn.Text);
                                                    switch (ans.Key)
                                                    {
                                                        case 0:
                                                            EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected1 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 1:
                                                            EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected2 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                    }
                                                }
                                                E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                                MainControls.CreateRadioButton(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "rb", 2, id, text, isChecked);
                                                Label lbl_score = new Label();
                                                lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_score.Text = string.Format("得分：{0}", ansResult ? EE_TrueFalseS : 0);
                                                Panel_EA_ListItem2.Controls.Add(lbl_score);
                                                E9_Score = ansResult ? EE_TrueFalseS.ToString() : "0";
                                                if (ansResult)
                                                    EX_Score += EE_TrueFalseS;
                                            }
                                            break;
                                        case "單選題":
                                            {

                                                Label lbl_ques = new Label();
                                                lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                                Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                                SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                                SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                                SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                                bool ansResult = false;
                                                StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                                foreach (var ans in ques.Value)
                                                {
                                                    DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                    text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                    RadioButton rbtn = ((RadioButton)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                    isChecked.Add(ans.Key, rbtn.Checked);
                                                    if (dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked)
                                                        ansResult = true;
                                                    if (rbtn.Checked)
                                                        E9_Answer = string.Format("{0}.{1}", ans.Key + 1, rbtn.Text);
                                                    sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, rbtn.Text);
                                                    switch (ans.Key)
                                                    {
                                                        case 0:
                                                            EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected1 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 1:
                                                            EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected2 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 2:
                                                            EA_TransNo3 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo3 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected3 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect3 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 3:
                                                            EA_TransNo4 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo4 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected4 = rbtn.Checked ? "1" : "0";
                                                            E9_IsCorrect4 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                            break;
                                                    }
                                                }
                                                E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                                MainControls.CreateRadioButton(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "rb", 4, id, text, isChecked);
                                                Label lbl_score = new Label();
                                                lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_score.Text = string.Format("得分：{0}", ansResult ? EE_OneChoiceS : 0);
                                                Panel_EA_ListItem2.Controls.Add(lbl_score);
                                                E9_Score = ansResult ? EE_OneChoiceS.ToString() : "0";
                                                if (ansResult)
                                                    EX_Score += EE_OneChoiceS;
                                            }
                                            break;
                                        case "複選題":
                                            {
                                                Label lbl_ques = new Label();
                                                lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                                Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                                SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                                SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                                SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                                bool ansResult = true;
                                                StringBuilder sb_E9_Answer = new StringBuilder();
                                                StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                                foreach (var ans in ques.Value)
                                                {
                                                    DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                    text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                    CheckBox cbox = ((CheckBox)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                    isChecked.Add(ans.Key, cbox.Checked);
                                                    if ((dr_ans["EA_IsCorrect"].ToString() == "Y" && !cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && cbox.Checked))
                                                        ansResult = false;
                                                    if (cbox.Checked)
                                                        sb_E9_Answer.AppendFormat("{0}.{1};", ans.Key + 1, cbox.Text);
                                                    sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, cbox.Text);
                                                    switch (ans.Key)
                                                    {
                                                        case 0:
                                                            EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected1 = cbox.Checked ? "1" : "0";
                                                            E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 1:
                                                            EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected2 = cbox.Checked ? "1" : "0";
                                                            E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 2:
                                                            EA_TransNo3 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo3 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected3 = cbox.Checked ? "1" : "0";
                                                            E9_IsCorrect3 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                            break;
                                                        case 3:
                                                            EA_TransNo4 = dr_ans["EA_TransNo"].ToString();
                                                            EA_UidNo4 = dr_ans["EA_UidNo"].ToString();
                                                            E9_IsSelected4 = cbox.Checked ? "1" : "0";
                                                            E9_IsCorrect4 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                            break;
                                                    }
                                                }
                                                E9_Answer = sb_E9_Answer.ToString().TrimEnd(';');
                                                E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                                MainControls.CreateCheckBox(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "cb", 4, id, text, isChecked);
                                                Label lbl_score = new Label();
                                                lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                                lbl_score.Text = string.Format("得分：{0}", ansResult ? EE_MultiChoiceS : 0);
                                                Panel_EA_ListItem2.Controls.Add(lbl_score);
                                                E9_Score = ansResult ? EE_MultiChoiceS.ToString() : "0";
                                                if (ansResult)
                                                    EX_Score += EE_MultiChoiceS;
                                            }
                                            break;
                                    }

                                    NO += 1;
                                    #region 寫入交易 測驗結果 tblExamD
                                    //if (!this.IsRefresh)
                                    //{
                                    if (rsp.Result) //執行成功
                                    {
                                        DataSet ds_E9 = new DataSet();
                                        ds_E9.Tables.Add(MainControls.UpLoadToDataTable(DataExamD.GetSchema(),
                                            dataToHashtable_E9("A", EQ_TransNo, EQ_UidNo,
                                            E9_OrderNo, E9_Question, E9_AnsListItem, E9_Answer, E9_Score,
                                            EA_TransNo1, EA_UidNo1, EA_TransNo2, EA_UidNo2, EA_TransNo3, EA_UidNo3, EA_TransNo4, EA_UidNo4,
                                            E9_IsSelected1, E9_IsCorrect1, E9_IsSelected2, E9_IsCorrect2, E9_IsSelected3, E9_IsCorrect3, E9_IsSelected4, E9_IsCorrect4)
                                            ));
                                        RServiceProvider rsp_E9 = DataExamD.AppendTransaction(sysValue.emNo, sysValue.ProgId, ds_E9, rsp.ReturnSqlTransaction);
                                        if (rsp_E9.Result) //執行成功
                                        {

                                        }
                                        else
                                        {
                                            //取消SQL交易，不寫入DB
                                            SqlAccess.SqlTransactionRollback(rsp.ReturnSqlTransaction);

                                            MainControls.showMsg(this, "數位課程測驗結果資料登錄！\\n請稍後再試，或聯絡相關處理人員。");
                                            logger.Error(string.Format("數位課程測驗結果資料登錄(答案明細)[新增]失敗：\r\n", rsp_E9.ReturnMessage));
                                            logger_db.Error(string.Format("數位課程測驗結果資料登錄(答案明細)[新增]失敗：\r\n", rsp_E9.ReturnMessage));

                                            return;
                                        }
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, "數位課程測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                        logger.Error(string.Format("數位課程測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                        logger_db.Error(string.Format("數位課程測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                    }
                                    //}
                                    #endregion

                                }



                            }
                            //執行SQL交易，正式寫入DB
                            try
                            {
                                SqlAccess.SqlTransactionCommit(rsp.ReturnSqlTransaction);
                                //更新測驗總分
                                RServiceProvider rsp_EX_Score = DataExam.updateEX_Score(hif_EX_TransNo.Value, EX_Score.ToString());
                            }
                            catch (Exception ex) //發生例外錯誤事件
                            {
                                //取消SQL交易，不寫入DB
                                SqlAccess.SqlTransactionRollback(rsp.ReturnSqlTransaction);
                                MainControls.showMsg(this, "數位課程-測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                logger.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", ex));
                                logger_db.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", ex));
                            }
                            lbl_EX_Score.Text = EX_Score.ToString();
                        }
                    }
                    break;
                case "4":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "最佳成績");
                    if (ViewState["dt_EB"] != null)
                        dr_EB = (ViewState["dt_EB"] as DataTable).Rows[0];
                    else
                        dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
                    if (dr_EB != null)
                    {
                        ViewState["dt_EB"] = dr_EB.Table;
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr_EB["EB_Subject"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Subject3.Text = dr_Phrase["TypeName"].ToString();
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_EB["EB_SubType"].ToString());
                        if (dr_Phrase != null)
                            lbl_EB_SubTypeName2.Text = string.Format("{0}-{1}", dr_Phrase["TypeName"].ToString(), lbl_EG_Subject3.Text);
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_ID3.Text = dr_person["PS_ID"].ToString();
                            lbl_PS_NAME3.Text = dr_person["PS_NAME"].ToString();

                        }
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("163", Request.QueryString["i"]);
                        if (dr_Phrase != null)
                            lbl_EE_ItemName2.Text = dr_Phrase["TypeName"].ToString();
                        DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit3.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                            DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                            if (dr_sales2 != null)
                            {
                                lbl_DM_Name3.Text = dr_sales2["NAME"].ToString();
                            }
                        }
                        DataRow dr_EX = DataExam.DataReader_EB_TransNo_AccID_MAX(dr_EB["EB_TransNo"].ToString(), Session["AccID"].ToString());
                        if (dr_EX != null)
                        {
                            lbl_ExamDate3.Text = DateTime.Parse(dr_EX["EX_Date"].ToString()).ToString("yyyy/MM/dd");
                            lbl_EX_Score0.Text = dr_EX["EX_Score"].ToString();
                            lbl_EX_STime1.Text = DateTime.Parse(dr_EX["EX_Date"].ToString()).ToString("HH:mm");
                            lbl_EX_ETime1.Text = DateTime.Parse(dr_EX["EX_Date"].ToString()).ToString("HH:mm");

                            Panel_EA_ListItem3.Controls.Clear();
                            DataTable dt_E9 = DataExamD.DataReader_EX_TransNo(dr_EX["EX_TransNo"].ToString()).Tables[0];
                            foreach(DataRow dr_E9 in dt_E9.Rows)
                            {
                                Label lbl_ques = new Label();
                                lbl_ques.Text = string.Format("{2}{0}. {1}<br>", dr_E9["E9_OrderNo"].ToString(), dr_E9["E9_Question"].ToString(), dt_E9.Rows.IndexOf(dr_E9) == 0 ? "<br>" : "<br>");
                                Panel_EA_ListItem3.Controls.Add(lbl_ques);
                                string[] E9_AnsListItem = dr_E9["E9_AnsListItem"].ToString().Split(new string[] { "(;)" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach(string ans in E9_AnsListItem)
                                {
                                    RadioButton rbtn = new RadioButton();
                                    rbtn.Enabled = false;
                                    rbtn.Text = string.Format("{0}<br>", ans);
                                    rbtn.Checked = dr_E9[string.Format("E9_IsSelected{0}", Array.IndexOf(E9_AnsListItem, ans) + 1)].ToString() == "True";
                                    Panel_EA_ListItem3.Controls.Add(rbtn);
                                    
                                }
                                Label lbl_score = new Label();
                                if (dr_E9["E9_Score"].ToString() != "" && dr_E9["E9_Score"].ToString() != "0")
                                    lbl_score.Text = "正確<br>";
                                else
                                    lbl_score.Text = "錯誤<br>";
                                Panel_EA_ListItem3.Controls.Add(lbl_score);
                            }
                        }

                    }
                    break;
                default:
                    if (!IsPostBack)
                        dataBind();
                    else
                        Response.Redirect("~/manager/ExamTest");
                    break;
            }
            MultiView1.ActiveViewIndex = Convert.ToInt16(CommandName);
            //this.IsRefresh = true;
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;

        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

            }

        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EB_SubType")) return;
            if (ddlSh_EG_Type.SelectedIndex > 0)
                e.InputParameters.Add("EB_SubType", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                e.InputParameters.Add("EB_SubType", "");
            e.InputParameters.Add("EB_Subject", ddlSh_EG_Subject.SelectedValue);
            e.InputParameters.Add("EG_Name", txtSh_EG_Name.Text.Trim());
            e.InputParameters.Add("EX_Date_s", DateRangeSh_EX_Date.Date_start);
            e.InputParameters.Add("EX_Date_e", DateRangeSh_EX_Date.Date_end);
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EG_Type.SelectedIndex = -1;
            ddlSh_EG_Subject.SelectedIndex = -1;
            ddlSh_EG_Subject.Items.Clear();
            txtSh_EG_Name.Text = string.Empty;
            DateRangeSh_EX_Date.Init();
            
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void ddlSh_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlSh_EG_Type.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "160");
                    break;
                default:
                    ddlSh_EG_Subject.Items.Clear();
                    break;
            }
        }
        //protected void ddl_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    switch (ddl_EG_Type.SelectedValue)
        //    {
        //        case "1":
        //            DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "", "138");
        //            break;
        //        case "2":
        //            DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "", "158");
        //            break;
        //        case "3":
        //            DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "", "159");
        //            break;
        //        case "4":
        //            DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "", "160");
        //            break;
        //        default:
        //            ddlSh_EG_Subject.Items.Clear();
        //            break;
        //    }
        //}

        private void QuesInit()
        {
            Panel_EA_ListItem1.Controls.Clear();
            DataRow dr_EB = DataExamSub.DataReader_EB_TransNo(Request.QueryString["id"]);
            //if (dr_EB == null)
            //    dr_EB = DataExamSub.DataReader_EB_SubType((ViewState["ddl_EG_Type"] as DataTable).Rows[ddl_EG_Type.SelectedIndex]["TypeSubCode"].ToString(), ddl_EG_Subject.SelectedValue);
            if (dr_EB != null)
            {
                ViewState["dt_EB"] = dr_EB.Table;
                DataRow dr_EE = DataEQRule.DataReader_EE_ExamType_EE_Subject_EE_Item(dr_EB["EB_SubType"].ToString(), dr_EB["EB_Subject"].ToString(), Request.QueryString["i"]);
                if (dr_EE != null)
                {
                    if (!IsPostBack)
                    {
                        int NO = 1;
                        Dictionary<string, Dictionary<int, string>> ques_list = new Dictionary<string, Dictionary<int, string>>();
                        List<EQRule> list_rule = new List<EQRule>();
                        list_rule.Add(new EQRule { EQ_Type = "是非題", Count = dr_EE["EE_TrueFalse"].ToString(), Order = dr_EE["EE_TrueFalseO"].ToString(), Score = dr_EE["EE_TrueFalseS"].ToString() });
                        list_rule.Add(new EQRule { EQ_Type = "單選題", Count = dr_EE["EE_OneChoice"].ToString(), Order = dr_EE["EE_OneChoiceO"].ToString(), Score = dr_EE["EE_OneChoiceS"].ToString() });
                        list_rule.Add(new EQRule { EQ_Type = "複選題", Count = dr_EE["EE_MultiChoice"].ToString(), Order = dr_EE["EE_MultiChoiceO"].ToString(), Score = dr_EE["EE_MultiChoiceS"].ToString() });
                        foreach (var item in list_rule.OrderBy(o => o.Order))
                        {
                            DataTable dt_ques = DataEQues.DataReader_EB_TransNo(dr_EB["EB_TransNo"].ToString(), dr_EE["EE_Item"].ToString(), item.EQ_Type, item.Count).Tables[0];
                            foreach (DataRow dr in dt_ques.Rows)
                            {
                                Dictionary<int, string> ans = new Dictionary<int, string>();
                                switch (dr["EQ_Type"].ToString())
                                {
                                    case "是非題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                            foreach (DataRow dr_ans in dt_ans.Rows)
                                            {
                                                ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                                id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "rb", 2, id, text);
                                        }
                                        break;
                                    case "單選題":
                                        {

                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                            foreach (DataRow dr_ans in dt_ans.Rows)
                                            {
                                                ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                                id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "rb", 4, id, text);
                                        }
                                        break;
                                    case "複選題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                            foreach (DataRow dr_ans in dt_ans.Rows)
                                            {
                                                ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                                id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateCheckBox(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "cb", 4, id, text);
                                        }
                                        break;
                                }
                                ques_list.Add(dr["EQ_TransNo"].ToString(), ans);
                                NO += 1;
                            }

                        }
                        Session["ques_list"] = ques_list;

                    }
                    else
                    {
                        int NO = 1;
                        Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                        foreach (var ques in ques_list)
                        {
                            DataRow dr = DataEQues.DataReader_EQ_TransNo(ques.Key);
                            if (dr != null)
                            {
                                switch (dr["EQ_Type"].ToString())
                                {
                                    case "是非題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "rb", 2, id, text);
                                        }
                                        break;
                                    case "單選題":
                                        {

                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "rb", 4, id, text);
                                        }
                                        break;
                                    case "複選題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                            Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                            }
                                            MainControls.CreateCheckBox(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "cb", 4, id, text);
                                        }
                                        break;
                                }
                            }
                            NO += 1;
                        }
                    }
                }
                
            }

        }

        private class EQRule
        {
            public string EQ_Type { set; get; }
            public string Count { set; get; }
            public string Score { set; get; }
            public string Order { set; get; }
        }

        protected void Timer2_Tick(object sender, EventArgs e)
        {
            lbl_EX_ETime.Text = DateTime.Now.ToString("HH:mm");
            TimeSpan tSpan = ((DateTime)Session["eTime"]).Subtract(DateTime.Now);
            if ((int)tSpan.TotalSeconds >= 0)
            {
                lbl_RemainingTime.Text = string.Format("{0}:{1}:{2}", tSpan.Hours.ToString("00"), tSpan.Minutes.ToString("00"), tSpan.Seconds.ToString("00"));

                if ((int)tSpan.TotalSeconds <= 0)
                {
                    Timer2.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(
                        this, HttpContext.Current.GetType(), "Alert",
                        string.Format("alert('{0}');location.href='/manager/ExamTest?id={1}&c=3&open=1'", "測驗時間結束！", Request.QueryString["id"]),
                        true);

                }

            }
        }

        


    }
}