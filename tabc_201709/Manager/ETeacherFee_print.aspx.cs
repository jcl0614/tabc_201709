﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ETeacherFee_print : System.Web.UI.Page
    {
        protected static string ET_TNam;
        protected static string ET_ID;
        protected static string ET_Phone;
        protected static string ET_Company;
        protected static string ET_Addr;
        protected static string EC_CName;
        protected static string E6_ShouldPay_str;
        protected static string E6_RealPay_str;
        protected static string E6_Tax_str;
        protected static string E6_Type_str;
        protected static string E6_TransDate_str;
        protected static string E6_PIC_Name;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                if (ViewState["SysValue"] != null)
                {
                    dataBind();
                    Page.Title = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                }
            }
            
        }

        private void dataBind()
        {
            if (Request.QueryString["id"] != null)
            {
                DataRow dr = DataETeacherFee.DataReader_E6_TransNo(Request.QueryString["id"]);
                if (dr != null)
                {
                    Repeater1.DataSource = dr.Table;
                    Repeater1.DataBind();
                    E6_ShouldPay_str = string.Format("新台幣 <span style=\"font-weight: bold\">{0}</span> 元整　　<span style=\"font-weight: bold\">NT$ {1:N0}</span>", MainControls.ConvertNumberStr(dr["E6_ShouldPay"].ToString()), dr["E6_ShouldPay"]);
                    E6_RealPay_str = string.Format("新台幣 <span style=\"font-weight: bold\">{0}</span> 元整　　<span style=\"font-weight: bold\">NT$ {1:N0}</span>", MainControls.ConvertNumberStr(dr["E6_RealPay"].ToString()), dr["E6_RealPay"]);
                    E6_Tax_str = string.Format("{0:N0}", dr["E6_Tax"]);
                    DataTable dt_E6_Type = DataPhrase.DataReader_TypeCode("E009").Tables[0];
                    StringBuilder sb_E6_Type = new StringBuilder();
                    foreach (DataRow dr_E6_Type in dt_E6_Type.Rows)
                    {
                        if (dr_E6_Type["TypeNo"].ToString() == dr["E6_Type"].ToString())
                            sb_E6_Type.AppendFormat("<span style=\"font-size: 30px; font-family: Arial;\">■</span>{0}　　", dr_E6_Type["TypeName"].ToString());
                        else
                            sb_E6_Type.AppendFormat("<span style=\"font-size: 30px; font-family: Arial;\">□</span>{0}　　", dr_E6_Type["TypeName"].ToString());
                    }
                    E6_Type_str = sb_E6_Type.ToString().TrimEnd();
                    DateTime date;
                    DateTime.TryParse(dr["E6_TransDate"].ToString(), out date);
                    if (date.Year != 1)
                        E6_TransDate_str = string.Format("中華民國　{0}　年　{1}　月　{2}　日", date.Year - 1911, date.Month, date.Day);
                    DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr["ET_TransNo"].ToString());
                    if (dr_ET != null)
                    {
                        ET_TNam = dr_ET["ET_TName"].ToString();
                        ET_ID = dr_ET["ET_ID"].ToString();
                        ET_Phone = dr_ET["ET_Phone"].ToString();
                        ET_Company = dr_ET["ET_Company"].ToString();
                        ET_Addr = dr_ET["ET_Addr"].ToString();
                    }
                    DataRow dr_course = DataECourse.DataReader_EC_TransNo(dr["EC_TransNo"].ToString());
                    if (dr_course != null)
                    {
                        EC_CName = dr_course["EC_CName"].ToString();
                    }
                    DataRow dr_person = DataPerson.DataReader_AccID(dr["E6_PIC"].ToString());
                    if (dr_person != null)
                    {
                        E6_PIC_Name = dr_person["PS_NAME"].ToString();
                    }
                }

            }
        }
    }
}