﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ECircular_PassUpload.aspx.cs" Inherits="tabc_201709.Manager.ECircular_PassUpload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>
<%@ Register src="ascx/selRegion.ascx" tagname="selRegion" tagprefix="uc5" %>
<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">考試日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EI_ExamDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">考試類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EI_ExamType" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">業務員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_PS_NAME" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_pass" SelectMethod="DataReader_pass"
                                        TypeName="DataECircular"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">考試名稱</td>
                                            <td class="tdData10s">
                                                <uc5:selRegion ID="selRegion_EG_TransNo" runat="server" OnRefreshButtonClick="selRegion_EG_TransNo_RefreshButtonClick" OnSearchButtonClick="selRegion_EG_TransNo_SearchButtonClick" OnSelectedIndexChanged="selRegion_EG_TransNo_SelectedIndexChanged" OnTextChanged="selRegion_EG_TransNo_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">考試類別</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EG_Type" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">考試日期</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EG_ExamDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">考試地區</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EI_Region" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">入場證號</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EI_Pass" runat="server" MaxLength="100"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">業務員</td>
                                            <td class="tdData10s">
                                                <uc3:selPerson_AccID ID="selPerson_AccID_EI_AccID" runat="server" OnRefreshButtonClick="selPerson_AccID_EI_AccID_RefreshButtonClick" OnSearchButtonClick="selPerson_AccID_EI_AccID_SearchButtonClick" OnSelectedIndexChanged="selPerson_AccID_EI_AccID_SelectedIndexChanged" OnTextChanged="selPerson_AccID_EI_AccID_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_PS_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">地區</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_TYPE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_NAME" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">准考證檔案</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EF1" runat="server" MaxLength="100" ReadOnly="True" Width="300px"></asp:TextBox>
                                                &nbsp;<asp:Literal ID="Literal_EF1" runat="server"></asp:Literal>
                                                <br />
                                                <asp:FileUpload ID="FileUpload_EF1" runat="server" />
                                                <asp:Label ID="lbl_Msg_EF1" runat="server" CssClass="errmsg12"></asp:Label>
                                                &nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">檔案格式：.pdf </span> </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="150">准考證檔案大小</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EFSize1" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                                &nbsp;MB&nbsp;<img alt="" src="<%=ResolveUrl("images/help.gif") %>" /><span style="color: #D663A5">可整數或小數2位</span>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EI_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EG_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EI_AccID" runat="server" />
                                                <asp:HiddenField ID="hif_EF_TransNo1" runat="server" />
                                                <asp:HiddenField ID="hif_EF_UidNo1" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
                <asp:PostBackTrigger ControlID="btnAppend" />
                <asp:PostBackTrigger ControlID="btnEdit" />
                <asp:PostBackTrigger ControlID="GridView1" />
            </Triggers>
    </asp:UpdatePanel>


</asp:Content>
