﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ECoursePrint_print : System.Web.UI.Page
    {
        protected static string EC_CName;
        protected static string EC_SDate;
        protected static string EC_Place;
        protected static string ET_TNam;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                if (ViewState["SysValue"] != null)
                {
                    dataBind();
                    Page.Title = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                }

            }
        }

        private void dataBind()
        {
            if (Request.QueryString["id"] != null)
            {
                DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                if (dr_course != null)
                {
                    EC_CName = dr_course["EC_CName"].ToString();
                    EC_Place = dr_course["EC_Place"].ToString();
                    DateTime ec_SDate = DateTime.Parse(dr_course["EC_SDate"].ToString());
                    DateTime ec_SDate_s = DateTime.Parse(string.Format("{0} {1}", ec_SDate.ToString("yyyy/MM/dd"), dr_course["EC_STime"].ToString()));
                    DateTime ec_SDate_e = DateTime.Parse(string.Format("{0} {1}", ec_SDate.ToString("yyyy/MM/dd"), dr_course["EC_ETime"].ToString()));
                    TimeSpan ts = new TimeSpan(ec_SDate_e.Ticks - ec_SDate_s.Ticks);
                    EC_SDate = string.Format("{0} 年 {1} 月 {2} 日　{3}~{4} (共計{5}小時)", ec_SDate.Year, ec_SDate.Month, ec_SDate.Day, dr_course["EC_STime"].ToString(), dr_course["EC_ETime"].ToString(), ts.TotalHours);
                    DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr_course["ET_TransNo"].ToString());
                    if (dr_ET != null)
                        ET_TNam = string.Format("{0}　{1}　{2}", dr_ET["ET_Company"].ToString(), dr_ET["ET_TName"].ToString(), dr_ET["ET_JobTitle"].ToString());
                    DataTable dt_EAttend = DataEAttend.DataReader_EC_TransNo(dr_course["EC_TransNo"].ToString()).Tables[0];
                    if (dt_EAttend != null && dt_EAttend.Rows.Count > 0)
                    {
                        DataTable dt_view = new DataTable();
                        dt_view.Columns.Add("ED_AccID", typeof(string));
                        dt_view.Columns.Add("cZON_PK", typeof(string));
                        dt_view.Columns.Add("ED_Name", typeof(string));
                        dt_view.Columns.Add("DM_NAME", typeof(string));
                        dt_view.Columns.Add("ED_EatHabits", typeof(string));
                        foreach (DataRow dr in dt_EAttend.Rows)
                        {
                            string cZON_PK = "";
                            string DM_NAME = "";
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["ED_AccID"].ToString());
                            if (dr_sales != null)
                            {
                                cZON_PK = dr_sales["cZON_PK"].ToString();
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    DM_NAME = dr_sales2["NAME"].ToString();
                                }
                            }
                            dt_view.Rows.Add(
                                dr["ED_AccID"].ToString(),
                                cZON_PK,
                                dr["ED_Name"].ToString(),
                                DM_NAME,
                                dr["ED_EatHabits"].ToString()
                                );
                        }
                        GridView1.DataSource = dt_view;
                        GridView1.DataBind();
                    }
                }


            }
        }
    }
}