﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ENotice.aspx.cs" Inherits="tabc_201709.Manager.ENotice" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>


<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnk_notice" runat="server" CommandName="1" CssClass="MainHeadTdLink" OnCommand="notice_Command">第一次通知</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnk_notice1" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnCommand="notice_Command">第二次通知</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnk_notice2" runat="server" CommandName="3" CssClass="MainHeadTdLink" OnCommand="notice_Command">書面通知</asp:LinkButton>
            <br />
            <asp:LinkButton ID="lnk_remove" runat="server" CommandName="4" CssClass="MainHeadTdLink" OnCommand="notice_Command">註銷名單批次作業</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnk_pass" runat="server" CommandName="5" CssClass="MainHeadTdLink" OnCommand="notice_Command" Visible="False">合格通報</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">年度：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EN_Year" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">通知類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EN_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">通知日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EN_InfoDate" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">業務員：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EN_AccID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataENotice"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">通知年度</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EN_Year" runat="server" MaxLength="100" Width="50px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">通知狀態</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EN_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">業務員</td>
                                            <td class="tdData10s">
                                                <uc3:selPerson_AccID ID="selPerson_AccID_EN_AccID" runat="server" OnRefreshButtonClick="selPerson_AccID_EN_AccID_RefreshButtonClick" OnSearchButtonClick="selPerson_AccID_EN_AccID_SearchButtonClick" OnSelectedIndexChanged="selPerson_AccID_EN_AccID_SelectedIndexChanged" OnTextChanged="selPerson_AccID_EN_AccID_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">職級</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_PS_Title" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">地區</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_TYPE" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">單位</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_cZON_NAME" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">處經理</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_DN_Name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EN_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EN_AccID" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table id="print_notice1" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView2" runat="server" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="7" Font-Size="12px">
                                        <Columns>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox3" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#7C4318" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                            </table>
                        <table border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <asp:Button ID="btnAppend0" runat="server" CommandName="Notice_1" CssClass="btnChat" OnCommand="notice_Command" Text="寄發通知" />
                                    <button class="btnPrint" onclick="printScreen(print_notice1,'');">列印</button>
                                    <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <table id="print_notice2" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView3" runat="server" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="7" Font-Size="12px">
                                        <Columns>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="CheckBox4" runat="server" />
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckBox5" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox6" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#7C4318" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <asp:Button ID="btnAppend1" runat="server" CommandName="Notice_2" CssClass="btnChat" OnCommand="notice_Command" Text="寄發通知" />
                                    <button class="btnPrint" onclick="printScreen(print_notice2,'');">列印</button>
                                    <asp:Button ID="btnCancel1" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <table id="print_notice3" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView4" runat="server" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="7" Font-Size="12px">
                                        <Columns>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="CheckBox7" runat="server" />
                                                </EditItemTemplate>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckBox8" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox9" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#7C4318" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <asp:Button ID="btnAppend2" runat="server" CommandName="Notice_3" CssClass="btnChat" OnCommand="notice_Command" Text="寄發通知" />
                                    <button class="btnPrint" onclick="printScreen(print_notice3,'');">列印</button>
                                    <asp:Button ID="btnPrintMail" runat="server" CommandName="PrintMail" CssClass="btnChat" OnCommand="notice_Command" Text="列印信封" />
                                    <asp:Button ID="btnCancel2" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View7" runat="server">
                        <table id="print_notice4" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="7" Font-Size="12px" OnRowDataBound="GridView5_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="業務員" HeaderText="業務員" />
                                            <asp:BoundField DataField="性別" HeaderText="性別" />
                                            <asp:BoundField DataField="證件號碼" HeaderText="證件號碼" />
                                            <asp:BoundField DataField="出生日期" HeaderText="出生日期" />
                                            <asp:BoundField DataField="訓練總時數" HeaderText="訓練總時數" />
                                            <asp:BoundField DataField="規定時數" HeaderText="規定時數" />
                                            <asp:TemplateField HeaderText="註銷原因">
                                                <HeaderTemplate>
                                                    註銷原因
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DropDownList1" runat="server">
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#7C4318" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <asp:Button ID="btnAppend3" runat="server" CommandName="Notice_4" CssClass="btnChat" OnCommand="notice_Command" Text="寄發通知" />
                                    <asp:Button ID="btnExport" runat="server" CommandName="Export_fail" CssClass="btnExport" OnCommand="notice_Command" Text="匯出" />
                                    <button class="btnPrint" onclick="printScreen(print_notice4,'');">列印</button>
                                    <asp:Button ID="btnCancel3" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View8" runat="server">
                        <table id="print_notice5" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView6" runat="server" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="7" Font-Size="12px">
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#7C4318" HorizontalAlign="Center" Width="100px" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <asp:Button ID="btnAppend4" runat="server" CommandName="Notice_2" CssClass="btnChat" OnCommand="notice_Command" Text="寄發通知" />
                                    <asp:Button ID="btnExport0" runat="server" CommandName="Export_sucess" CssClass="btnExport" OnCommand="notice_Command" Text="匯出" />
                                    <button class="btnPrint" onclick="printScreen(print_notice5,'');">
                                        列印
                                    </button>
                                    <asp:Button ID="btnCancel4" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
            <asp:PostBackTrigger ControlID="btnExport0" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
        function printHtml(value) {
            var printPage = window.open("", "", "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>

</asp:Content>
