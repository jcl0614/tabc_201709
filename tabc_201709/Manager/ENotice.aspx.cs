﻿using NLog;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ENotice : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));


        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EN_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_EN_Year);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_EN_Year);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 800);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataENotice.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EN_Type, true, "", "E004");
            DataPhrase.DDL_TypeName(ddl_EN_Type, true, "請選擇", "E004");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();
            
            if (txt_EN_Year.Text.Trim() == "")
                sbError.Append("●「年度」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", txt_EN_Year.Text.Trim()))
                    sbError.Append("●「年度」格式錯誤!<br>");
                else
                {
                    if (int.Parse(txt_EN_Year.Text.Trim()) < 1911)
                        sbError.Append("●「年度」格式錯誤!<br>");
                }
            }
            if (ddl_EN_Type.SelectedValue == "")
                sbError.Append("●「通知狀態」必須選取!<br>");
            if (selPerson_AccID_EN_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「業務員」必須選取!<br>");

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EN_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EN_UidNo":
                            hif_EN_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EN_Type":
                            MainControls.ddlIndexSelectValue(ddl_EN_Type, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EN_Year":
                            txt_EN_Year.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EN_AccID":
                            selPerson_AccID_EN_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_EN_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_EN_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
                            if (dr_person != null)
                                lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
                            else
                                lbl_PS_Title.Text = string.Empty;
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
                            if (dr_sales != null)
                            {
                                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                                if (dr_Unit != null)
                                {
                                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                                }
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                                }
                            }
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selPerson_AccID_EN_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
            if (dr_person != null)
                lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
            else
                lbl_PS_Title.Text = string.Empty;
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                if (dr_Unit != null)
                {
                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                }
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                }
            }
            else
            {
                lbl_cZON_NAME.Text = string.Empty;
                lbl_cZON_TYPE.Text = string.Empty;
                lbl_DN_Name.Text = string.Empty;
            }
        }
        protected void selPerson_AccID_EN_AccID_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EN_AccID_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EN_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EN_AccID.search(hif_EN_AccID.Value);
            selPerson_AccID_EN_AccID.SelectedValue = hif_EN_AccID.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataENotice.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataENotice.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EN_UidNo.Value = string.Empty;
                        this.SetFocus(txt_EN_Year);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataENotice.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataENotice.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EN_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EN_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EN_UidNo", hif_EN_UidNo.Value);
            }
            hsData.Add("EN_Year", txt_EN_Year.Text.Trim());
            hsData.Add("EN_Type", ddl_EN_Type.SelectedValue);
            hsData.Add("EN_AccID", selPerson_AccID_EN_AccID.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
            if (dr_sales != null)
            {
                hsData.Add("EN_Name", dr_sales["NAME"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EN_AccID.SelectedValue);
            if (dr_person != null)
                hsData.Add("EN_Title", dr_person["PS_Title"].ToString());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_batch(string EN_AccID, string EN_Type, string EN_InfoDate, string EN_Email, string EN_Address, string EN_Memo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EN_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("EN_UidNo", Session["UidNo"].ToString());
            hsData.Add("EN_Year", DateTime.Now.Year.ToString());
            hsData.Add("EN_Type", EN_Type);
            hsData.Add("EN_AccID", EN_AccID);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(EN_AccID);
            if (dr_sales != null)
            {
                hsData.Add("EN_Name", dr_sales["NAME"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(EN_AccID);
            if (dr_person != null)
                hsData.Add("EN_Title", dr_person["PS_Title"].ToString());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EN_TransNo", TransNo);
            hsData.Add("EN_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EN_UidNo.Value = string.Empty;
            ddl_EN_Type.SelectedIndex = -1;
            txt_EN_Year.Text = string.Empty;
            lbl_PS_Title.Text = string.Empty;
            lbl_cZON_TYPE.Text = string.Empty;
            lbl_cZON_NAME.Text = string.Empty;
            lbl_DN_Name.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EN_Year);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EN_Year);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[5].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[6].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[5].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[5].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[6].Text, GridView1.Rows[i].Cells[7].Text)));

                    //RServiceProvider rsp = DataENotice.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataENotice.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EN_Year")) return;
            e.InputParameters.Add("EN_Year", txtSh_EN_Year.Text.Trim());
            e.InputParameters.Add("EN_Type", ddlSh_EN_Type.SelectedValue);
            e.InputParameters.Add("EN_AccID", txtSh_EN_AccID.Text.Trim());
            e.InputParameters.Add("EN_InfoDate_s", DateRangeSh_EN_InfoDate.Date_start);
            e.InputParameters.Add("EN_InfoDate_e", DateRangeSh_EN_InfoDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EN_Type.SelectedIndex = -1;
            txtSh_EN_Year.Text = string.Empty;
            txtSh_EN_AccID.Text = string.Empty;
            DateRangeSh_EN_InfoDate.Init();
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion
        #endregion

        protected void notice_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            switch (e.CommandName)
            {
                case "1":
                    {
                        DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "1");
                        if (dr_p != null)
                        {
                            DataTable dt_member = new DataTable();
                            dt_member.Columns.Add("業務員編號", typeof(string));
                            dt_member.Columns.Add("姓名", typeof(string));
                            dt_member.Columns.Add("訓練總時數", typeof(string));
                            dt_member.Columns.Add("規定時數", typeof(string));
                            int preDays = 0;
                            int.TryParse(dr_p["Limit"].ToString(), out preDays);
                            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
                            DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
                            foreach (DataRow dr_sales in dt_sales.Rows)
                            {
                                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                                double ER_Hours_count = 0;
                                double ED_AttHours_count = 0;
                                DataTable dt_erule = DataERule.DataReader_ER_YearNo(iYear.ToString()).Tables[0];
                                foreach (DataRow dr_erule in dt_erule.Rows)
                                {
                                    double ER_Hours = 0;
                                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                                    ER_Hours_count += ER_Hours;
                                    double ED_AttHours = 0;
                                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
                                    double.TryParse(hours, out ED_AttHours);
                                    ED_AttHours_count += ED_AttHours;
                                }
                                if (ED_AttHours_count < ER_Hours_count)
                                {
                                    string Birth = "";
                                    if (dr_sales["Birth"].ToString() != "")
                                    {
                                        DateTime birthDay = DateTime.Parse(dr_sales["Birth"].ToString());
                                        Birth = string.Format("{0}{1}{2}", birthDay.Year.ToString("0000"), birthDay.Month.ToString("00"), birthDay.Day.ToString("00"));
                                    }
                                    dt_member.Rows.Add(dr_sales["cSAL_FK"].ToString(), dr_sales["NAME"].ToString(), ED_AttHours_count.ToString(), ER_Hours_count.ToString());
                                }
                            }
                            GridView2.DataSource = dt_member;
                            GridView2.DataBind();
                        }

                        ChangeMultiView(2);
                        lnkBrowse.Visible = true;
                    }
                    break;
                case "Notice_1":
                    {
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "1");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        foreach (GridViewRow gvr in GridView2.Rows)
                        {
                            if (((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                            {
                                string mail_from = "", mail_from_name = "";
                                DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                                if (dr_person != null)
                                {
                                    if (dr_person["PS_EMAIL"].ToString() != "")
                                        mail_from = dr_person["PS_EMAIL"].ToString();
                                    else
                                        mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = dr_person["PS_NAME"].ToString();
                                }
                                else
                                {
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = DataWebSet.dataReader("WebTitle");

                                }
                                DataRow dr_person2 = DataPerson.DataReader_AccID(gvr.Cells[1].Text.Trim());
                                if (dr_person2["PS_EMAIL"].ToString().Trim() != "")
                                {
                                    //Email通知
                                    RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", "教育訓練通知", E7_Memo, "", mail_from_name, false);
                                    if (!rsp_sendMail.Result)
                                    {
                                        MainControls.showMsg(this, string.Format("通知失敗！\\n{0}", "請確認您的EMAIL是否正確。"));
                                        logger.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    }
                                    else
                                    {
                                        DataSet ds = new DataSet();
                                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable_batch(dr_person2["AccID"].ToString(), "1", DateTime.Now.ToString("yyyy/MM/dd"), dr_person2["PS_EMAIL"].ToString(), "", "")));
                                        RServiceProvider rsp = DataENotice.Append(sysValue.emNo, sysValue.ProgId, ds);
                                    }
                                }
                                else
                                    MainControls.showMsg(this, "該人員EMAIL不存在！");
                            }
                        }
                    }
                    break;
                case "2":
                    {
                        DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "2");
                        if (dr_p != null)
                        {
                            DataTable dt_member = new DataTable();
                            dt_member.Columns.Add("業務員編號", typeof(string));
                            dt_member.Columns.Add("姓名", typeof(string));
                            dt_member.Columns.Add("訓練總時數", typeof(string));
                            dt_member.Columns.Add("規定時數", typeof(string));
                            int preDays = 0;
                            int.TryParse(dr_p["Limit"].ToString(), out preDays);
                            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
                            DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
                            foreach (DataRow dr_sales in dt_sales.Rows)
                            {
                                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                                double ER_Hours_count = 0;
                                double ED_AttHours_count = 0;
                                DataTable dt_erule = DataERule.DataReader_ER_YearNo(iYear.ToString()).Tables[0];
                                foreach (DataRow dr_erule in dt_erule.Rows)
                                {
                                    double ER_Hours = 0;
                                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                                    ER_Hours_count += ER_Hours;
                                    double ED_AttHours = 0;
                                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
                                    double.TryParse(hours, out ED_AttHours);
                                    ED_AttHours_count += ED_AttHours;
                                }
                                if (ED_AttHours_count < ER_Hours_count)
                                {
                                    string Birth = "";
                                    if (dr_sales["Birth"].ToString() != "")
                                    {
                                        DateTime birthDay = DateTime.Parse(dr_sales["Birth"].ToString());
                                        Birth = string.Format("{0}{1}{2}", birthDay.Year.ToString("0000"), birthDay.Month.ToString("00"), birthDay.Day.ToString("00"));
                                    }
                                    dt_member.Rows.Add(dr_sales["cSAL_FK"].ToString(), dr_sales["NAME"].ToString(), ED_AttHours_count.ToString(), ER_Hours_count.ToString());
                                }
                            }
                            GridView3.DataSource = dt_member;
                            GridView3.DataBind();
                        }

                        ChangeMultiView(3);
                        lnkBrowse.Visible = true;
                    }
                    break;
                case "Notice_2":
                    {
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "2");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        foreach (GridViewRow gvr in GridView3.Rows)
                        {
                            if (((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                            {
                                string mail_from = "", mail_from_name = "";
                                DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                                if (dr_person != null)
                                {
                                    if (dr_person["PS_EMAIL"].ToString() != "")
                                        mail_from = dr_person["PS_EMAIL"].ToString();
                                    else
                                        mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = dr_person["PS_NAME"].ToString();
                                }
                                else
                                {
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = DataWebSet.dataReader("WebTitle");

                                }
                                DataRow dr_person2 = DataPerson.DataReader_AccID(gvr.Cells[1].Text.Trim());
                                if (dr_person2["PS_EMAIL"].ToString().Trim() != "")
                                {
                                    //Email通知
                                    RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", "教育訓練通知", E7_Memo, "", mail_from_name, false);
                                    if (!rsp_sendMail.Result)
                                    {
                                        MainControls.showMsg(this, string.Format("通知失敗！\\n{0}", "請確認您的EMAIL是否正確。"));
                                        logger.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    }
                                    else
                                    {
                                        DataSet ds = new DataSet();
                                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable_batch(dr_person2["AccID"].ToString(), "2", DateTime.Now.ToString("yyyy/MM/dd"), dr_person2["PS_EMAIL"].ToString(), "", "")));
                                        RServiceProvider rsp = DataENotice.Append(sysValue.emNo, sysValue.ProgId, ds);
                                    }
                                }
                                else
                                    MainControls.showMsg(this, "該人員EMAIL不存在！");
                            }
                        }
                    }
                    break;
                case "3":
                    {
                        DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "3");
                        if (dr_p != null)
                        {
                            DataTable dt_member = new DataTable();
                            dt_member.Columns.Add("業務員", typeof(string));
                            dt_member.Columns.Add("性別", typeof(string));
                            dt_member.Columns.Add("證件號碼", typeof(string));
                            dt_member.Columns.Add("聯絡地址", typeof(string));
                            dt_member.Columns.Add("訓練總時數", typeof(string));
                            dt_member.Columns.Add("規定時數", typeof(string));
                            int preDays = 0;
                            int.TryParse(dr_p["Limit"].ToString(), out preDays);
                            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
                            DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
                            foreach (DataRow dr_sales in dt_sales.Rows)
                            {
                                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                                double ER_Hours_count = 0;
                                double ED_AttHours_count = 0;
                                DataTable dt_erule = DataERule.DataReader_ER_YearNo(iYear.ToString()).Tables[0];
                                foreach (DataRow dr_erule in dt_erule.Rows)
                                {
                                    double ER_Hours = 0;
                                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                                    ER_Hours_count += ER_Hours;
                                    double ED_AttHours = 0;
                                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
                                    double.TryParse(hours, out ED_AttHours);
                                    ED_AttHours_count += ED_AttHours;
                                }
                                if (ED_AttHours_count < ER_Hours_count)
                                {
                                    string Birth = "";
                                    if (dr_sales["Birth"].ToString() != "")
                                    {
                                        DateTime birthDay = DateTime.Parse(dr_sales["Birth"].ToString());
                                        Birth = string.Format("{0}{1}{2}", birthDay.Year.ToString("0000"), birthDay.Month.ToString("00"), birthDay.Day.ToString("00"));
                                    }
                                    dt_member.Rows.Add(dr_sales["NAME"].ToString(), dr_sales["Gender"].ToString() == "1" ? "男" : "女", dr_sales["cSAL_FK"].ToString(), Birth, ED_AttHours_count.ToString(), ER_Hours_count.ToString());
                                }
                            }
                            GridView4.DataSource = dt_member;
                            GridView4.DataBind();
                        }

                        ChangeMultiView(4);
                        lnkBrowse.Visible = true;
                    }
                    break;
                case "Notice_3":
                    {
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "3");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        foreach (GridViewRow gvr in GridView4.Rows)
                        {
                            if (((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                            {
                                string mail_from = "", mail_from_name = "";
                                DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                                if (dr_person != null)
                                {
                                    if (dr_person["PS_EMAIL"].ToString() != "")
                                        mail_from = dr_person["PS_EMAIL"].ToString();
                                    else
                                        mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = dr_person["PS_NAME"].ToString();
                                }
                                else
                                {
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = DataWebSet.dataReader("WebTitle");

                                }
                                DataRow dr_person2 = DataPerson.DataReader_AccID(gvr.Cells[3].Text.Trim());
                                if (dr_person2["PS_EMAIL"].ToString().Trim() != "")
                                {
                                    //Email通知
                                    RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", "教育訓練通知", E7_Memo, "", mail_from_name, false);
                                    if (!rsp_sendMail.Result)
                                    {
                                        MainControls.showMsg(this, string.Format("通知失敗！\\n{0}", "請確認您的EMAIL是否正確。"));
                                        logger.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    }
                                    else
                                    {
                                        DataSet ds = new DataSet();
                                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable_batch(dr_person2["AccID"].ToString(), "3", DateTime.Now.ToString("yyyy/MM/dd"), dr_person2["PS_EMAIL"].ToString(), "", "")));
                                        RServiceProvider rsp = DataENotice.Append(sysValue.emNo, sysValue.ProgId, ds);
                                    }
                                }
                                else
                                    MainControls.showMsg(this, "該人員EMAIL不存在！");
                            }
                        }
                    }
                    break;
                case "PrintMail":
                    StringBuilder sb = new StringBuilder();

                    foreach (GridViewRow gvr in GridView4.Rows)
                    {
                        if (((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                        {
                            sb.Append("<div style=\"width: 220mm; height: 90mm\">");
                            sb.Append(" <table width=\"100%\" >");
                            sb.AppendFormat("<tr><td style=\"width: 100%; height: 55mm; padding-top: 5%; padding-left: 5%;\" align=\"left\" valign=\"top\">{0}<br />{1}</td></tr>", "寄件地址", "寄件單位");
                            sb.AppendFormat("<tr><td style=\"width: 100%; height: 55mm\" align=\"center\">{0}<br /><br />{1}</td></tr>", gvr.Cells[4].Text, gvr.Cells[1].Text);
                            sb.Append("</table>");
                            sb.Append("</div>");
                            sb.Append("<div style=\"page-break-before:always; line-height: 0px; height: 0px;\"></div>");
                        }
                    }

                    ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "Alert", string.Format("printHtml('{0}');", sb.ToString()), true);
                    break;
                case "4":
                    {
                        DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "4");
                        if (dr_p != null)
                        {
                            DataTable dt_member = new DataTable();
                            dt_member.Columns.Add("業務員", typeof(string));
                            dt_member.Columns.Add("性別", typeof(string));
                            dt_member.Columns.Add("證件號碼", typeof(string));
                            dt_member.Columns.Add("出生日期", typeof(string));
                            dt_member.Columns.Add("訓練總時數", typeof(string));
                            dt_member.Columns.Add("規定時數", typeof(string));
                            int preDays = 0;
                            int.TryParse(dr_p["Limit"].ToString(), out preDays);
                            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
                            DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
                            foreach (DataRow dr_sales in dt_sales.Rows)
                            {
                                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;

                                double ER_Hours_count = 0;
                                double ED_AttHours_count = 0;
                                DataTable dt_erule = DataERule.DataReader_ER_YearNo(iYear.ToString()).Tables[0];
                                foreach (DataRow dr_erule in dt_erule.Rows)
                                {
                                    double ER_Hours = 0;
                                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                                    ER_Hours_count += ER_Hours;
                                    double ED_AttHours = 0;
                                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
                                    double.TryParse(hours, out ED_AttHours);
                                    ED_AttHours_count += ED_AttHours;
                                }
                                if (ED_AttHours_count < ER_Hours_count)
                                {
                                    string Birth = "";
                                    if (dr_sales["Birth"].ToString() != "")
                                    {
                                        DateTime birthDay = DateTime.Parse(dr_sales["Birth"].ToString());
                                        Birth = string.Format("{0}{1}{2}", birthDay.Year.ToString("0000"), birthDay.Month.ToString("00"), birthDay.Day.ToString("00"));
                                    }
                                    dt_member.Rows.Add(dr_sales["NAME"].ToString(), dr_sales["Gender"].ToString() == "1" ? "男" : "女", dr_sales["cSAL_FK"].ToString(), Birth, ED_AttHours_count.ToString(), ER_Hours_count.ToString());
                                }
                                
                            }
                            GridView5.DataSource = dt_member;
                            GridView5.DataBind();
                        }

                        ChangeMultiView(5);
                        lnkBrowse.Visible = true;
                    }
                    break;
                case "Notice_4":
                    {
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "4");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        foreach (GridViewRow gvr in GridView5.Rows)
                        {
                            //if (((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                            //{
                                string mail_from = "", mail_from_name = "";
                                DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                                if (dr_person != null)
                                {
                                    if (dr_person["PS_EMAIL"].ToString() != "")
                                        mail_from = dr_person["PS_EMAIL"].ToString();
                                    else
                                        mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = dr_person["PS_NAME"].ToString();
                                }
                                else
                                {
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                    mail_from_name = DataWebSet.dataReader("WebTitle");

                                }
                                DataRow dr_person2 = DataPerson.DataReader_AccID(gvr.Cells[2].Text.Trim());
                                if (dr_person2["PS_EMAIL"].ToString().Trim() != "")
                                {
                                    //Email通知
                                    RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", "教育訓練通知", E7_Memo, "", mail_from_name, false);
                                    if (!rsp_sendMail.Result)
                                    {
                                        MainControls.showMsg(this, string.Format("通知失敗！\\n{0}", "請確認您的EMAIL是否正確。"));
                                        logger.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("教育訓練通知Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    }
                                    else
                                    {
                                        DataSet ds = new DataSet();
                                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataENotice.GetSchema(), dataToHashtable_batch(dr_person2["AccID"].ToString(), "4", DateTime.Now.ToString("yyyy/MM/dd"), dr_person2["PS_EMAIL"].ToString(), "", ((DropDownList)gvr.Cells[6].Controls[1]).SelectedItem.Text)));
                                        RServiceProvider rsp = DataENotice.Append(sysValue.emNo, sysValue.ProgId, ds);
                                    }
                                }
                                else
                                    MainControls.showMsg(this, "該人員EMAIL不存在！");
                            //}
                        }
                    }
                    break;
                case "Export_fail"://13登錄註銷換證申請表
                    {
                        HSSFWorkbook workbook;
                        using (FileStream fs = new FileStream(Server.MapPath("~/XLS/13登錄註銷換證申請表--LR_01060523_309901220.xls"), FileMode.Open, FileAccess.Read))
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        MemoryStream ms = new MemoryStream();
                        ISheet sheet = workbook.GetSheetAt(3);
                        int rowIndex = 26;
                        foreach (GridViewRow gvr in GridView5.Rows)
                        {
                            if (sheet.GetRow(rowIndex) == null)
                                sheet.CreateRow(rowIndex);
                            for (int i = 0; i <= 10; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[0].Text));
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(3);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[1].Text).Trim() == "男" ? "1" : "2");
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[2].Text));
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[3].Text));
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(7);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(8);
                                            if (((DropDownList)gvr.Cells[6].Controls[1]).SelectedItem.Text.IndexOf("-") != -1)
                                                cell.SetCellValue(((DropDownList)gvr.Cells[6].Controls[1]).SelectedItem.Text.Split('-')[0]);
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(9);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        DateTime apply_Date = DateTime.Now;
                        sheet.GetRow(22).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (apply_Date.Year - 1911).ToString("0000"), apply_Date.Month.ToString("00"), apply_Date.Day.ToString("00")));
                        sheet.GetRow(22).GetCell(6).SetCellValue(GridView5.Rows.Count);

                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("登錄註銷換證申請表_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
                case "5":
                    {
                        DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeNo("E004", "4");
                        if (dr_p != null)
                        {
                            DataTable dt_member = new DataTable();
                            dt_member.Columns.Add("業務員", typeof(string));
                            dt_member.Columns.Add("登錄類別", typeof(string));
                            dt_member.Columns.Add("性別", typeof(string));
                            dt_member.Columns.Add("證件類別", typeof(string));
                            dt_member.Columns.Add("證件號碼", typeof(string));
                            dt_member.Columns.Add("出生日期", typeof(string));
                            dt_member.Columns.Add("學歷", typeof(string));
                            dt_member.Columns.Add("戶籍地址", typeof(string));
                            dt_member.Columns.Add("金融市場常識與職業道德測驗合格", typeof(string));
                            int preDays = 0;
                            int.TryParse(dr_p["Limit"].ToString(), out preDays);
                            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year)).AddDays(-preDays);
                            DataTable dt_sales = Data_tabcSales.baseDataReader(true, "").Tables[0];
                            foreach (DataRow dr_sales in dt_sales.Rows)
                            {
                                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;

                                    double ER_Hours_count = 0;
                                    double ED_AttHours_count = 0;
                                    DataTable dt_erule = DataERule.DataReader_ER_YearNo(iYear.ToString()).Tables[0];
                                    foreach (DataRow dr_erule in dt_erule.Rows)
                                    {
                                        double ER_Hours = 0;
                                        double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                                        ER_Hours_count += ER_Hours;
                                        double ED_AttHours = 0;
                                        var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}'", dr_erule["ER_Type"].ToString())).ToString();
                                        double.TryParse(hours, out ED_AttHours);
                                        ED_AttHours_count += ED_AttHours;
                                    }
                                    if (ED_AttHours_count >= ER_Hours_count)
                                    {
                                        string Birth = "";
                                        if (dr_sales["Birth"].ToString() != "")
                                        {
                                            DateTime birthDay = DateTime.Parse(dr_sales["Birth"].ToString());
                                            Birth = string.Format("{0}{1}{2}", birthDay.Year.ToString("0000"), birthDay.Month.ToString("00"), birthDay.Day.ToString("00"));
                                        }
                                        string EI_Benotified = "";
                                        DataTable dt_EI = DataECircular.DataReader_EI_AccID(dr_sales["cSAL_FK"].ToString()).Tables[0];
                                        var dr_EI = dt_EI.AsEnumerable().OrderByDescending(o => o.Field<DateTime>("EI_CompDate"));
                                        if (dr_EI != null && dr_EI.Count() != 0)
                                            EI_Benotified = dr_EI.ToList()[0]["EI_Benotified"].ToString();
                                        dt_member.Rows.Add(
                                            dr_sales["NAME"].ToString(),
                                            EI_Benotified,
                                            dr_sales["Gender"].ToString() == "1" ? "男" : "女",
                                            MainControls.checkSSN(dr_sales["cSAL_FK"].ToString()) ? "1" : "2",
                                            dr_sales["cSAL_FK"].ToString(),
                                            Birth,
                                            dr_sales["Education"].ToString(),
                                            dr_sales["OAdress"].ToString(),
                                            "1");
                                    }

                            }
                            ViewState["dt_member_sucess"] = dt_member;
                            GridView6.DataSource = dt_member;
                            GridView6.DataBind();
                        }
                        ChangeMultiView(6);
                    }
                    break;
                case "Export_sucess"://13登錄註銷換證申請表
                    {
                        HSSFWorkbook workbook;
                        using (FileStream fs = new FileStream(Server.MapPath("~/XLS/13登錄註銷換證申請表--LR_01060523_309901220.xls"), FileMode.Open, FileAccess.Read))
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        MemoryStream ms = new MemoryStream();
                        #region 業務員登錄申請表
                        ISheet sheet = workbook.GetSheetAt(0);
                        int rowIndex = 26;
                        foreach (GridViewRow gvr in GridView6.Rows)
                        {
                            if (sheet.GetRow(rowIndex) == null)
                                sheet.CreateRow(rowIndex);
                            for (int i = 0; i <= 10; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[1].Text));
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[0].Text));
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(3);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[2].Text.Trim()) == "男" ? "1" : "2");
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[3].Text));
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[4].Text));
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[5].Text));
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(7);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[6].Text));
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(8);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[7].Text));
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(9);
                                            cell.SetCellValue(MainControls.ReplaceSpace(gvr.Cells[8].Text));
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        DateTime apply_Date = DateTime.Now;
                        sheet.GetRow(22).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (apply_Date.Year - 1911).ToString("0000"), apply_Date.Month.ToString("00"), apply_Date.Day.ToString("00")));
                        sheet.GetRow(22).GetCell(6).SetCellValue(GridView6.Rows.Count);
                        #endregion

                        #region 投資型登錄申請表
                        DataTable dt_member_sucess = ViewState["dt_member_sucess"] as DataTable;
                        DataTable dt_member_sucess_1 = new DataTable();
                        dt_member_sucess_1 = dt_member_sucess.Clone();
                        foreach (DataRow dr_sucess in dt_member_sucess.Rows)
                        {
                            DataTable dt_EI = DataECircular.DataReader_EI_AccID(dr_sucess["證件號碼"].ToString()).Tables[0];
                            var dr_EI = dt_EI.AsEnumerable().OrderByDescending(o => o.Field<DateTime>("EI_CompDate"));
                            if (dr_EI != null && dr_EI.Count() != 0)
                            {
                                if (dr_EI.ToList()[0]["EI_Type"].ToString() == "2" && dr_EI.ToList()[0]["EI_Category"].ToString() == "3" && dr_EI.ToList()[0]["EI_Benotified"].ToString() == "1")
                                {
                                    dt_member_sucess_1.Rows.Add(dr_sucess);
                                }
                            }
                        }
                        ISheet sheet1 = workbook.GetSheetAt(1);
                        rowIndex = 26;
                        foreach (DataRow dr in dt_member_sucess_1.Rows)
                        {
                            if (sheet.GetRow(rowIndex) == null)
                                sheet.CreateRow(rowIndex);
                            for (int i = 0; i <= 10; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue(dr[1].ToString());
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue(dr[0].ToString());
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(3);
                                            cell.SetCellValue(dr[2].ToString().Trim() == "男" ? "1" : "2");
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue(dr[3].ToString());
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue(dr[4].ToString());
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(dr[5].ToString());
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(7);
                                            cell.SetCellValue(dr[6].ToString());
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(8);
                                            cell.SetCellValue(dr[7].ToString());
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet1.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet1.GetRow(rowIndex).CreateCell(9);
                                            cell.SetCellValue(dr[8].ToString());
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        apply_Date = DateTime.Now;
                        sheet1.GetRow(22).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (apply_Date.Year - 1911).ToString("0000"), apply_Date.Month.ToString("00"), apply_Date.Day.ToString("00")));
                        sheet1.GetRow(22).GetCell(6).SetCellValue(GridView6.Rows.Count);
                        #endregion

                        #region 外幣登錄申請表
                        DataTable dt_member_sucess_2 = new DataTable();
                        dt_member_sucess_1 = dt_member_sucess.Clone();
                        foreach (DataRow dr_sucess in dt_member_sucess.Rows)
                        {
                            DataTable dt_EI = DataECircular.DataReader_EI_AccID(dr_sucess["證件號碼"].ToString()).Tables[0];
                            var dr_EI = dt_EI.AsEnumerable().OrderByDescending(o => o.Field<DateTime>("EI_CompDate"));
                            if (dr_EI != null && dr_EI.Count() != 0)
                            {
                                if (dr_EI.ToList()[0]["EI_Type"].ToString() == "2" && dr_EI.ToList()[0]["EI_Category"].ToString() == "4" && dr_EI.ToList()[0]["EI_Benotified"].ToString() == "1")
                                {
                                    dt_member_sucess_2.Rows.Add(dr_sucess);
                                }
                            }
                        }
                        ISheet sheet2 = workbook.GetSheetAt(2);
                        rowIndex = 26;
                        foreach (DataRow dr in dt_member_sucess_2.Rows)
                        {
                            if (sheet.GetRow(rowIndex) == null)
                                sheet.CreateRow(rowIndex);
                            for (int i = 0; i <= 10; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue(dr[1].ToString());
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue(dr[0].ToString());
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(3);
                                            cell.SetCellValue(dr[2].ToString().Trim() == "男" ? "1" : "2");
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue(dr[3].ToString());
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue(dr[4].ToString());
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(dr[5].ToString());
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(7);
                                            cell.SetCellValue(dr[6].ToString());
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(8);
                                            cell.SetCellValue(dr[7].ToString());
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet2.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet2.GetRow(rowIndex).CreateCell(9);
                                            cell.SetCellValue("*");
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        apply_Date = DateTime.Now;
                        sheet2.GetRow(22).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (apply_Date.Year - 1911).ToString("0000"), apply_Date.Month.ToString("00"), apply_Date.Day.ToString("00")));
                        sheet2.GetRow(22).GetCell(6).SetCellValue(GridView6.Rows.Count);
                        #endregion

                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("登錄註銷換證申請表_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
            }
        }



        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            GridView gv = (GridView)((CheckBox)sender).Parent.Parent.Parent.Parent;
            foreach (GridViewRow gvr in gv.Rows)
            {
                ((CheckBox)gvr.Cells[0].Controls[1]).Checked = ((CheckBox)sender).Checked;
            }
        }

        protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewState["cbl_EC_Staff"] = DataPhrase.DDL_TypeName(((DropDownList)e.Row.Cells[6].Controls[1]), false, "", "E016");
                

            }
        }

    }
}