﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class Schedule : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_SC_CustName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(selCustomer1);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(selCustomer1);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("Schedule", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(2);
                if (Request.QueryString["d"] != null)
                {
                    string d = Server.UrlDecode(Request.QueryString["d"]);
                    if (MainControls.ValidDate(d, "yyyy/MM/dd"))
                    {
                        DateRange2.Date_start = DateTime.Parse(d).ToString("yyyy/MM/dd");
                        DateRange2.Date_end = DateTime.Parse(d).ToString("yyyy/MM/dd");
                        ChangeMultiView(0);
                    }
                }
                //資料繫結
                dataBind();

                if (Request.QueryString["id"] != null)
                {
                    DataRow dr = DataScheduleM.DataReader_SC_TransNo(Request.QueryString["id"]);
                    if (dr != null)
                    {
                        viewDate(dr);
                    }
                }
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataScheduleM.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_SC_Kind"] = DataPhrase.DDL_TypeName(ddlSh_SC_Kind, true, "", "107");
            ViewState["ddl_SC_Kind"] = DataPhrase.DDL_TypeName(ddl_SC_Kind, true, "", "107");
            DataPhrase.DDL_TypeName(ddlSh_SC_State, true, "", "114");
            DataPhrase.DDL_TypeName(ddl_SC_State, true, "", "114");
            DataPhrase.DDL_TypeName(ddlSh_SC_CustType, true, "", "023");
            DataPhrase.DDL_TypeName(ddl_SC_Days, true, "", "204");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            if (selCustomer1.SelectedValue == "")
                sbError.Append("●「客戶」必須選取!<br>");
            DateTimeRange1.FieldName = "行程日期";
            if (DateTimeRange1.valid != "")
                sbError.AppendFormat("{0}<br>", DateTimeRange1.valid);
            if (ddl_SC_Kind.SelectedValue == "")
                sbError.Append("●「種類」必須選取!<br>");
            if (ddl_SC_Plan.SelectedValue == "")
                sbError.Append("●「項目」必須選取!<br>");


            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "SC_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "SC_UidNo":
                            hif_SC_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
                            if (dr_customer != null)
                            {
                                selCustomer1.search(dr_customer["CU_CustName"].ToString());
                                selCustomer1.SelectedValue = hif_CU_TransNo.Value;
                                DataTable dt_CM = DataCommunication.DataReader_CU_TransNo(selCustomer1.SelectedValue).Tables[0];
                                var list_CM = dt_CM.AsEnumerable().Where(
                                    x => x.Field<string>("CM_CLAS").ToString() == "地址" && x.Field<string>("CM_TYPE").ToString() == "收費地址");
                                if (list_CM != null && list_CM.Count() != 0)
                                {
                                    DataRow dr_CM = list_CM.ToList()[0];
                                    txt_SC_Address.Text = dr_CM["CM_DESC"].ToString();
                                }
                            }
                            break;
                        case "SC_StartTime":
                            DateTimeRange1.DateTime_start = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd HH:mm:ss");
                            break;
                        case "SC_EndTime":
                            DateTimeRange1.DateTime_end = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd HH:mm:ss");
                            break;
                        case "SC_Address":
                            txt_SC_Address.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "SC_Kind":
                            MainControls.ddlIndexSelectText(ddl_SC_Kind, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            if (ddl_SC_Kind.SelectedIndex != 0)
                                DataPhrase.DDL_TypeName(ddl_SC_Plan, true, "", (ViewState["ddl_SC_Kind"] as DataTable).Rows[ddl_SC_Kind.SelectedIndex - 1]["TypeSubCode"].ToString());
                            break;
                        case "SC_Plan":
                            MainControls.ddlIndexSelectText(ddl_SC_Plan, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "SC_State":
                            MainControls.ddlIndexSelectText(ddl_SC_State, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "SC_Days":
                            MainControls.ddlIndexSelectText(ddl_SC_Days, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "SC_Remind":
                            cbox_SC_Remind.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "Y";
                            break;
                        case "SC_Score":
                            txt_SC_Score.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "SC_Inreply":
                            cbox_SC_Inreply.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "Y";
                            break;
                        case "SC_Remark":
                            txt_SC_Remark.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt_CM = DataCommunication.DataReader_CU_TransNo(selCustomer1.SelectedValue).Tables[0];
            var list_CM = dt_CM.AsEnumerable().Where(
                x => x.Field<string>("CM_CLAS").ToString() == "地址" && x.Field<string>("CM_TYPE").ToString() == "收費地址");
            if (list_CM != null && list_CM.Count() != 0)
            {
                DataRow dr_CM = list_CM.ToList()[0];
                txt_SC_Address.Text = dr_CM["CM_DESC"].ToString();
            }
        }
        protected void selCustomer_RefreshButtonClick(object sender, EventArgs e)
        {
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
            if (dr_customer != null)
            {
                selCustomer1.search(dr_customer["CU_CustName"].ToString());
                selCustomer1.SelectedValue = hif_CU_TransNo.Value;
                DataTable dt_CM = DataCommunication.DataReader_CU_TransNo(selCustomer1.SelectedValue).Tables[0];
                var list_CM = dt_CM.AsEnumerable().Where(
                    x => x.Field<string>("CM_CLAS").ToString() == "地址" && x.Field<string>("CM_TYPE").ToString() == "收費地址");
                if (list_CM != null && list_CM.Count() != 0)
                {
                    DataRow dr_CM = list_CM.ToList()[0];
                    txt_SC_Address.Text = dr_CM["CM_DESC"].ToString();
                }
            }
        }
        private void viewDate(DataRow dr)
        {
            foreach (DataColumn dc in dr.Table.Columns)
            {
                switch (dc.ColumnName)
                {
                    case "SC_TransNo":
                        hif_TransNo.Value = dr[dc.ColumnName].ToString();
                        break;
                    case "SC_UidNo":
                        hif_SC_UidNo.Value = dr[dc.ColumnName].ToString();
                        break;
                    case "CU_TransNo":
                        hif_CU_TransNo.Value = dr[dc.ColumnName].ToString();
                        DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
                        if (dr_customer != null)
                        {
                            selCustomer1.search(dr_customer["CU_CustName"].ToString());
                            selCustomer1.SelectedValue = hif_CU_TransNo.Value;
                            DataTable dt_CM = DataCommunication.DataReader_CU_TransNo(selCustomer1.SelectedValue).Tables[0];
                            var list_CM = dt_CM.AsEnumerable().Where(
                                x => x.Field<string>("CM_CLAS").ToString() == "地址" && x.Field<string>("CM_TYPE").ToString() == "收費地址");
                            if (list_CM != null && list_CM.Count() != 0)
                            {
                                DataRow dr_CM = list_CM.ToList()[0];
                                txt_SC_Address.Text = dr_CM["CM_DESC"].ToString();
                            }
                        }
                        break;
                    case "SC_StartTime":
                        DateTimeRange1.DateTime_start = DateTime.Parse(dr[dc.ColumnName].ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                        break;
                    case "SC_EndTime":
                        DateTimeRange1.DateTime_end = DateTime.Parse(dr[dc.ColumnName].ToString()).ToString("yyyy/MM/dd HH:mm:ss");
                        break;
                    case "SC_Address":
                        txt_SC_Address.Text = dr[dc.ColumnName].ToString();
                        break;
                    case "SC_Kind":
                        MainControls.ddlIndexSelectText(ddl_SC_Kind, dr[dc.ColumnName].ToString());
                        if (ddl_SC_Kind.SelectedIndex != 0)
                            DataPhrase.DDL_TypeName(ddl_SC_Plan, true, "", (ViewState["ddl_SC_Kind"] as DataTable).Rows[ddl_SC_Kind.SelectedIndex - 1]["TypeSubCode"].ToString());
                        break;
                    case "SC_Plan":
                        MainControls.ddlIndexSelectText(ddl_SC_Plan, dr[dc.ColumnName].ToString());
                        break;
                    case "SC_State":
                        MainControls.ddlIndexSelectText(ddl_SC_State, dr[dc.ColumnName].ToString());
                        break;
                    case "SC_Days":
                        MainControls.ddlIndexSelectText(ddl_SC_Days, dr[dc.ColumnName].ToString());
                        break;
                    case "SC_Remind":
                        cbox_SC_Remind.Checked = dr[dc.ColumnName].ToString() == "Y";
                        break;
                    case "SC_Score":
                        txt_SC_Score.Text = dr[dc.ColumnName].ToString();
                        break;
                    case "SC_Inreply":
                        cbox_SC_Inreply.Checked = dr[dc.ColumnName].ToString() == "Y";
                        break;
                    case "SC_Remark":
                        txt_SC_Remark.Text = dr[dc.ColumnName].ToString();
                        break;
                }
            }

            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataScheduleM.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataScheduleM.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hif_CU_TransNo.Value = selCustomer1.SelectedValue;
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataScheduleM.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataScheduleM.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hif_CU_TransNo.Value = selCustomer1.SelectedValue;
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataScheduleM.GetSchema(), dataToHashtable("D")));

                        RServiceProvider rsp = DataScheduleM.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("SC_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("SC_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("SC_UidNo", hif_SC_UidNo.Value);
            }
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(selCustomer1.SelectedValue);
            if (dr_customer != null)
            {
                hsData.Add("CU_TransNo", dr_customer["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_customer["CU_UidNo"].ToString());
                hsData.Add("SC_CustType", dr_customer["CU_CustType"].ToString());
                hsData.Add("SC_ID", dr_customer["CU_ID"].ToString());
                hsData.Add("SC_CustName", dr_customer["CU_CustName"].ToString());
                hsData.Add("SC_Sex", dr_customer["CU_Sex"].ToString());
                hsData.Add("SC_Birth", dr_customer["CU_Birth"].ToString());
                hsData.Add("SC_Age", dr_customer["CU_Age"].ToString());
                DataTable dt_CM = DataCommunication.DataReader_CU_TransNo(selCustomer1.SelectedValue).Tables[0];
                var list_CM = dt_CM.AsEnumerable().Where(
                    x => x.Field<string>("CM_CLAS").ToString() == "地址" && x.Field<string>("CM_TYPE").ToString() == "收費地址");
                if (list_CM != null && list_CM.Count() != 0)
                {
                    DataRow dr_CM = list_CM.ToList()[0];
                    hsData.Add("SC_Zip", dr_CM["CM_ZIP"].ToString());
                }
                list_CM = dt_CM.AsEnumerable().Where(
                    x => x.Field<string>("CM_CLAS").ToString() == "其他" && x.Field<string>("CM_TYPE").ToString().IndexOf("件址") != 0);
                if (list_CM != null && list_CM.Count() != 0)
                {
                    DataRow dr_CM = list_CM.ToList()[0];
                    hsData.Add("SC_Email", dr_CM["CM_DESC"].ToString());
                }
                list_CM = dt_CM.AsEnumerable().Where(
                    x => x.Field<string>("CM_CLAS").ToString() == "電話" && x.Field<string>("CM_TYPE").ToString().IndexOf("聯絡電話") != 0);
                if (list_CM != null && list_CM.Count() != 0)
                {
                    DataRow dr_CM = list_CM.ToList()[0];
                    hsData.Add("SC_Phone", dr_CM["CM_DESC"].ToString());
                }
                list_CM = dt_CM.AsEnumerable().Where(
                    x => x.Field<string>("CM_CLAS").ToString() == "電話" && x.Field<string>("CM_TYPE").ToString().IndexOf("行動電話") != 0);
                if (list_CM != null && list_CM.Count() != 0)
                {
                    DataRow dr_CM = list_CM.ToList()[0];
                    hsData.Add("SC_MobilePhone", dr_CM["CM_DESC"].ToString());
                }
            }
            hsData.Add("SC_StartTime", DateTimeRange1.DateTime_start);
            hsData.Add("SC_EndTime", DateTimeRange1.DateTime_end);
            hsData.Add("SC_Address", txt_SC_Address.Text.Trim());
            hsData.Add("SC_Kind", ddl_SC_Kind.SelectedItem.Text);
            hsData.Add("SC_Plan", ddl_SC_Plan.SelectedItem.Text);
            hsData.Add("SC_State", ddl_SC_State.SelectedItem.Text);
            if (ddl_SC_Days.SelectedValue != "")
                hsData.Add("SC_Days", ddl_SC_Days.SelectedItem.Text);
            hsData.Add("SC_Score", txt_SC_Score.Text.Trim());
            hsData.Add("SC_Remark", txt_SC_Remark.Text.Trim());
            hsData.Add("SC_Remind", cbox_SC_Remind.Checked ? "Y" : "N");
            hsData.Add("SC_Inreply", cbox_SC_Inreply.Checked ? "Y" : "N");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("SC_TransNo", TransNo);
            hsData.Add("SC_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_SC_UidNo.Value = string.Empty;
            hif_CU_TransNo.Value = string.Empty;
            selCustomer1.Clear();
            DateTimeRange1.Init();
            txt_SC_Address.Text = string.Empty;
            ddl_SC_Kind.SelectedIndex = -1;
            ddl_SC_Plan.SelectedIndex = -1;
            ddl_SC_Days.SelectedIndex = -1;
            txt_SC_Score.Text = string.Empty;
            txt_SC_Remark.Text = string.Empty;
            cbox_SC_Remind.Checked = false;
            cbox_SC_Inreply.Checked = false;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkBrowse0.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    lnkBrowse0.Visible = true;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    lnkBrowse0.Visible = true;
                    break;
                case 2:
                    clearControlContext();
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(selCustomer1);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(selCustomer1);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
                #region cbox,sendbtn
                e.Row.Cells[9].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[10].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[9].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[9].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[10].Text, GridView1.Rows[i].Cells[11].Text)));

                    //RServiceProvider rsp = DataECourse.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataECourse.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1; ;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("SC_CustName")) return;
            e.InputParameters.Add("SC_CustName", Lib.FdVP(txtSh_SC_CustName.Text).Trim());
            e.InputParameters.Add("SC_CustType", ddlSh_SC_CustType.SelectedItem.Text);
            e.InputParameters.Add("SC_Kind", ddlSh_SC_Kind.SelectedValue);
            e.InputParameters.Add("SC_Plan", ddlSh_SC_Plan.SelectedValue);
            e.InputParameters.Add("SC_State", ddlSh_SC_State.SelectedValue);
            e.InputParameters.Add("SC_Inreply", ddlSh_SC_Inreply.SelectedValue);
            e.InputParameters.Add("SC_StartTime", DateRange2.Date_start);
            e.InputParameters.Add("SC_EndTime", DateRange2.Date_end);
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_SC_CustName.Text = string.Empty;
            ddlSh_SC_CustType.SelectedIndex = -1;
            ddlSh_SC_Kind.SelectedIndex = -1;
            ddlSh_SC_State.SelectedIndex = -1;
            ddlSh_SC_Inreply.SelectedIndex = -1;
            DateRange2.Init();
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void ddlSh_SC_Kind_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSh_SC_Kind.SelectedIndex != 0)
                DataPhrase.DDL_TypeName(ddlSh_SC_Plan, true, "", (ViewState["ddlSh_SC_Kind"] as DataTable).Rows[ddlSh_SC_Kind.SelectedIndex - 1]["TypeSubCode"].ToString());
        }

        protected void ddl_SC_Kind_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_SC_Kind.SelectedIndex != 0)
                DataPhrase.DDL_TypeName(ddl_SC_Plan, true, "", (ViewState["ddl_SC_Kind"] as DataTable).Rows[ddl_SC_Kind.SelectedIndex - 1]["TypeSubCode"].ToString());
        }
    }
}