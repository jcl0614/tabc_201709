﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ExamEregion : BasePage
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));
        private static string pageTitle = "";

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EG_Name);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                pageTitle = lbl_PageTitle.Text;
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                if (Request.QueryString["id"] != null)
                {
                    DataRow dr_region = DataERegion.DataReader_EG_TransNo(Request.QueryString["id"]);
                    if (dr_region != null)
                    {
                        ViewState["dt_region"] = dr_region.Table;
                        if (Request.QueryString["c"] != null)
                        {
                            switch (Request.QueryString["c"])
                            {
                                case "1":

                                    ChangeMultiView("1");
                                    break;
                                case "2":

                                    ChangeMultiView("2");
                                    break;
                                case "3":

                                    ChangeMultiView("3");
                                    break;
                                case "4":

                                    ChangeMultiView("4");
                                    break;
                                default:
                                    ChangeMultiView("0");
                                    break;
                            }
                        }
                        else
                            ChangeMultiView("0");
                    }
                    else
                        MainControls.showMsg(this, "證照考試不存在！");
                }
                else
                    ChangeMultiView("0");
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataERegion.DataColumn_exam(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EG_Type"] = DataPhrase.DDL_TypeName(ddlSh_EG_Type, true, "", "144");
            DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
            ViewState["ddlSh_EG_Region"] = DataPhrase.DDL_TypeName(ddlSh_EG_Region, true, "", "E005");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (rbl_EG_Region.SelectedIndex == -1)
                sbError.Append("●「考試地區」必須選取!<br>");
            //if (txt_EP_Bank.Text.Trim() == "")
            //    sbError.Append("●「轉出銀行」必須輸入!<br>");
            if (txt_EP_Account.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\d{5}$", (txt_EP_Account.Text.Trim())))
                    sbError.Append("●「轉出帳號末5碼」格式錯誤!<br>");
            }
            if (txt_EP_TransAmt.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EP_TransAmt.Text.Trim())))
                    sbError.Append("●「轉帳金額」格式錯誤!<br>");
            }

            if (txt_EP_TransDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_EP_TransDate.Text.Trim()))
                    sbError.Append("●「轉帳日期」格式錯誤!<br>");
            }
            if (TimeRangeAll_EP_TransTime.Time_start != "" && TimeRangeAll_EP_TransTime.Time_end != "")
            {
                TimeRangeAll_EP_TransTime.FieldName = "轉帳時間";
                if (TimeRangeAll_EP_TransTime.valid != "")
                    sbError.Append(TimeRangeAll_EP_TransTime.valid);
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_EP = new DataSet();

            switch (e.CommandName)
            {
                case "Attend":
                    {
                        lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                        if (lbl_Msg.Text.Trim().Length > 0) return;

                        RServiceProvider rsp_EP = new RServiceProvider();
                        DataRow dr_EP = DataEPayment.DataReader_EP_AccID_EG_TransNo(Session["AccID"].ToString(), Request.QueryString["id"]);
                        if (dr_EP != null) //已報名
                        {
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("M")));
                            rsp_EP = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds_EP);
                        }
                        else
                        {
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                            rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                        }
                        if (rsp_EP.Result) //執行成功
                        {
                            DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EP_TransNo(hif_EG_TransNo.Value, hif_EP_TransNo.Value);
                            if (dr_EI == null)
                            {
                                DataSet ds_EI = new DataSet();
                                ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI("A", System.Guid.NewGuid().ToString("N"), Session["UidNo"].ToString(),  hif_EP_TransNo.Value, hif_EP_UidNo.Value, "2")));
                                RServiceProvider rsp = DataECircular.Append(sysValue.emNo, sysValue.ProgId, ds_EI);
                            }
                            else
                            {
                                DataSet ds_EI = new DataSet();
                                ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI("M", dr_EI["EI_TransNo"].ToString(), dr_EI["EI_UidNo"].ToString(), hif_EP_TransNo.Value, hif_EP_UidNo.Value, "2")));
                                RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds_EI);
                            }
                            MainControls.showMsg(this, "報名完成！");
                            return;
                        }
                        else
                        {
                            MainControls.showMsg(this, "證照考試報名失敗！\\n請稍後再試，或聯絡相關處理人員。");
                            logger.Error(string.Format("證照考試報名失敗：\r\n", rsp_EP.ReturnMessage));
                            logger_db.Error(string.Format("證照考試報名失敗：\r\n", rsp_EP.ReturnMessage));
                        }
                    }
                    break;
                case "Delete":
                    {
                        ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("M")));
                        RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                        if (rsp_EP.Result) //執行成功
                        {
                            DataRow dr = DataEPayment.DataReader_EP_TransNo(hif_EP_TransNo.Value);
                            if (dr != null)
                            {
                                DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EP_TransNo(dr["EG_TransNo"].ToString(), dr["EP_TransNo"].ToString());
                                if (dr_EI != null)
                                {
                                    DataSet ds_EI = new DataSet();

                                    ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI_dlete(dr_EI["EI_TransNo"].ToString(), dr_EI["EI_UidNo"].ToString())));
                                    RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds_EI);
                                }
                            }
                            MainControls.showMsg(this, "報名刪除完成！");
                            lbl_PS_ID.Text = string.Empty;
                            lbl_PS_NAME.Text = string.Empty;
                            lbl_PS_Unit.Text = string.Empty;
                            lbl_EG_Type0.Text = string.Empty;
                            lbl_EG_Subject0.Text = string.Empty;
                            lbl_EG_StartDate0.Text = string.Empty;
                            lbl_EG_ExamDate0.Text = string.Empty;
                            lbl_EB_Regist.Text = string.Empty;
                            txt_EP_Bank.Text = string.Empty;
                            txt_EP_Account.Text = string.Empty;
                            txt_EP_TransAmt.Text = string.Empty;
                            txt_EP_TransDate.Text = string.Empty;
                            hif_EP_TransNo.Value = string.Empty;
                            hif_EP_UidNo.Value = string.Empty;
                            TimeRangeAll_EP_TransTime.Init();
                            lbtn_DeletePersonal.Visible = false;
                            return;
                        }
                        else
                        {
                            MainControls.showMsg(this, "報名繳費資料刪除失敗！\\n請稍後再試，或聯絡相關處理人員。");
                            logger.Error(string.Format("實體課程-個人報名(繳費登錄資料)[刪除]失敗：\r\n", rsp_EP.ReturnMessage));
                            logger_db.Error(string.Format("實體課程-個人報名(繳費登錄資料)[刪除]失敗：\r\n", rsp_EP.ReturnMessage));
                        }
                    }
                    break;

            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable_EP(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", hif_EP_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EP_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EP_UidNo", hif_EP_UidNo.Value);
            }
            hsData.Add("EG_TransNo", hif_EG_TransNo.Value);
            DataRow dr_region = DataERegion.DataReader_EG_TransNo(hif_EG_TransNo.Value);
            if (dr_region != null)
            {
                hsData.Add("EP_Type", dr_region["EG_Type"].ToString());
                DataRow dr_EB = DataExamSub.DataReader_EB_SubType(dr_region["EG_Type"].ToString(), dr_region["EG_Subject"].ToString());
                if (dr_EB != null)
                {
                    hsData.Add("EB_TransNo", dr_EB["EB_TransNo"].ToString());
                    hsData.Add("EB_UidNo", dr_EB["EB_UidNo"].ToString());
                    DataTable dt_exam = DataExam.DataReader_EB_TransNo_AccID(dr_EB["EB_TransNo"].ToString(), Session["AccID"].ToString()).Tables[0];
                    if (dt_exam != null && dt_exam.Rows.Count != 0) //尚未完成模擬測驗
                    {
                        DataRow dr_EX = dt_exam.Rows[0];//最佳成績測驗
                        hsData.Add("EX_TransNo", dr_EX["EX_TransNo"].ToString());
                        hsData.Add("EX_UidNo", dr_EX["EX_UidNo"].ToString());
                    }
                }
            }
            
            if (ModiState == "A")
            {
                hsData.Add("EG_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EG_UidNo", hif_EP_UidNo.Value);
            }
            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if (dr_person != null)
            {
                hsData.Add("EP_AccID", dr_person["AccID"].ToString());
                hsData.Add("EP_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EP_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("EP_SignDate", DateTime.Now);
            hsData.Add("EP_EduLevel", ddl_Education.SelectedValue);
            hsData.Add("EP_Bank", txt_EP_Bank.Text.Trim());
            hsData.Add("EP_Account", txt_EP_Account.Text.Trim());
            hsData.Add("EP_TransAmt", txt_EP_TransAmt.Text.Trim());
            hsData.Add("EP_TransDate", txt_EP_TransDate.Text.Trim());
            hsData.Add("EP_TransTimeS", TimeRangeAll_EP_TransTime.Time_start);
            hsData.Add("EP_TransTimeE", TimeRangeAll_EP_TransTime.Time_end);
            hsData.Add("EP_IsCash", cbox_EP_IsCash.Checked);
            hsData.Add("EP_Region", rbl_EG_Region.SelectedValue);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EP_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", TransNo);
            hsData.Add("EP_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EI(string ModiState, string EI_TransNo, string EI_UidNo, string EP_TransNo, string EP_UidNo, string EI_ExamType)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", EI_TransNo);
            hsData.Add("EI_UidNo", EI_UidNo);
            hsData.Add("EI_Type", "2");
            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if (dr_person != null)
            {
                hsData.Add("EI_AccID", dr_person["AccID"].ToString());
                hsData.Add("EI_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EI_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("EP_TransNo", EP_TransNo);
            hsData.Add("EP_UidNo", EP_UidNo);
            DataRow dr_region = DataERegion.DataReader_EG_TransNo(hif_EG_TransNo.Value);
            if (dr_region != null)
            {
                hsData.Add("EG_TransNo", dr_region["EG_TransNo"].ToString());
                hsData.Add("EG_UidNo", dr_region["EG_UidNo"].ToString());
                hsData.Add("EI_ExamType", dr_region["EG_Type"].ToString());
                hsData.Add("EI_ExamDate", dr_region["EG_ExamDate"].ToString());
            }
            hsData.Add("EI_Region", rbl_EG_Region.SelectedValue);
            hsData.Add("EI_Education", ddl_Education.SelectedValue);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EI_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", TransNo);
            hsData.Add("EI_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {

        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(e.CommandName);
        }

        private void ChangeMultiView(string CommandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataRow dr_region = null;
            switch (CommandName)
            {
                case "1":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "報考資訊");
                    if (ViewState["dt_region"] != null)
                        dr_region = (ViewState["dt_region"] as DataTable).Rows[0];
                        
                    else
                        dr_region = DataERegion.DataReader_EG_TransNo(Request.QueryString["id"]);
                    if (dr_region != null)
                    {
                        DateTime ServerDateTime = MainControls.ServerDateTime();
                        ViewState["dt_region"] = dr_region.Table;
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr_region["EG_Subject"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Subject.Text = dr_Phrase["TypeName"].ToString();
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_region["EG_Type"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                        lbl_EG_StartDate.Text = string.Format("{0}~{1}", DateTime.Parse(dr_region["EG_StartDate"].ToString()).ToString("yyyy/MM/dd"), DateTime.Parse(dr_region["EG_Deadline"].ToString()).ToString("yyyy/MM/dd"));
                        lbl_EG_Name.Text = dr_region["EG_Name"].ToString();
                        lbl_EG_Name0.Text = dr_region["EG_Name"].ToString();
                        lbl_EG_ExamDate.Text = DateTime.Parse(dr_region["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
                        string EG_Region = dr_region["EG_Region"].ToString();
                        StringBuilder sb_EG_Region = new StringBuilder();
                        if (EG_Region.IndexOf(";") != -1)
                        {
                            string[] EG_Region_item = EG_Region.Split(';');
                            foreach (string item in EG_Region_item)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    string[] value = item.Split(',');
                                    if (value[0] == "Y")
                                        sb_EG_Region.AppendFormat("{0}、", value[3]);
                                }
                            }
                        }
                        lbl_EG_Region.Text = sb_EG_Region.ToString().TrimEnd('、');
                        DataTable dt_EI = DataECircular.DataReader_EI_AccID(Session["AccID"].ToString()).Tables[0];
                        var list_EI = dt_EI.AsEnumerable().Where(
                            x => x.Field<int>("EI_Type").ToString() == "2" && x.Field<string>("EI_ExamType").ToString() == dr_region["EG_Type"].ToString() && x.Field<DateTime>("EI_ExamDate").ToString("yyyy/MM/dd") != DateTime.Parse(dr_region["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd")).OrderByDescending(o => o.Field<DateTime>("EI_ExamDate"));
                        if (list_EI != null && list_EI.Count() != 0)
                        {
                            DataRow dr_EI = list_EI.ToList()[0];
                            if (dr_EI["EI_ScoreF"].ToString() == "" && dr_EI["EI_Score1"].ToString() == "" && dr_EI["EI_Score2"].ToString() == "" && dr_EI["EI_Score3"].ToString() == "" && dr_EI["EI_Score4"].ToString() == "")
                            {
                                lbtn_start.Visible = false;
                            }
                        }
                        else
                        {
                            
                            
                            //DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EI_AccID(dr_region["EG_TransNo"].ToString(), Session["AccID"].ToString());
                            //if(dr_EI != null)
                            //{
                            //    if (dr_EI["EI_Type"].ToString() == "" || dr_EI["EI_Type"].ToString() == "2")
                            //    {
                                    DataRow dr_EB = DataExamSub.DataReader_EB_SubType(dr_region["EG_Type"].ToString(), dr_region["EG_Subject"].ToString());
                                    if (dr_EB != null)
                                    {
                                        DataTable dr_exam = DataExam.DataReader_EB_TransNo_AccID(dr_EB["EB_TransNo"].ToString(), Session["AccID"].ToString()).Tables[0];
                                        if (dr_exam == null || dr_exam.Rows.Count == 0) //尚未完成模擬測驗
                                        {
                                            lbtn_start.Visible = false;
                                        }
                                        else
                                        {
                                            DataRow dr_ePayment = DataEPayment.DataReader_EP_AccID_EG_TransNo(Session["AccID"].ToString(), dr_region["EG_TransNo"].ToString());
                                            if (dr_ePayment != null) //已報名
                                            {
                                                if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr_region["EG_ExamDate"].ToString())) < 0)
                                                {
                                                    DataRow dr_EF1 = DataEFile.DataReader_EF_TransNo(dr_region["EF_TransNo1"].ToString());
                                                    if (dr_EF1 != null) //入場證已上傳
                                                    {
                                                        lbl_Status.Text = "入場證下載";
                                                    }
                                                    else
                                                    {
                                                        lbl_Status.Text = "已報名";
                                                    }
                                                }
                                                else //考試結束
                                                {
                                                    lbl_Status.Text = "已放榜";

                                                }
                                                lbtn_start.Visible = false;
                                            }
                                            else //尚未報名
                                            {
                                                if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr_region["EG_Deadline"].ToString())) <= 0)
                                                {
                                                    lbl_Status.Text = "報名中";
                                                }
                                                else //報名截止
                                                {
                                                    lbl_Status.Text = "報名截止";
                                                    lbtn_start.Visible = false;
                                                }

                                            }
                                        }
                                    }
                                    else
                                        lbtn_start.Visible = false;
                            //    }
                            //}

                        }
                        if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr_region["EG_Deadline"].ToString())) > 0)
                            lbtn_DeletePersonal.Visible = false;
                    }
                    break;
                case "2":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "考試報名");
                    if (ViewState["dt_region"] != null)
                        dr_region = (ViewState["dt_region"] as DataTable).Rows[0];
                        
                    else
                        dr_region = DataERegion.DataReader_EG_TransNo(Request.QueryString["id"]);
                    if (dr_region != null)
                    {
                        ViewState["dt_region"] = dr_region.Table;
                        lbl_EG_Name1.Text = dr_region["EG_Name"].ToString();
                        hif_EG_TransNo.Value = dr_region["EG_TransNo"].ToString();
                        hif_EG_UidNo.Value = dr_region["EG_UidNo"].ToString();
                        string[] EG_Region = dr_region["EG_Region"].ToString().Split(';');
                        foreach (string item in EG_Region)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                string[] value = item.Split(',');
                                if (value[0] == "Y")
                                    rbl_EG_Region.Items.Add(new ListItem(value[3], value[2]));
                            }
                        }
                        DataPhrase.DDL_TypeName(ddl_Education, false, "", "E006");
                        //Data_tabcEducation.DDL_cEDU_DESC(ddl_Education, false, "");
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_ID.Text = dr_person["PS_ID"].ToString();
                            lbl_PS_NAME.Text = dr_person["PS_NAME"].ToString();

                        }
                        DataRow dr_sales = Data_tabcSales.DataReader_Full_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                            MainControls.ddlIndexSelectValue(ddl_Education, dr_sales["Education"].ToString());
                        }
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr_region["EG_Subject"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Subject0.Text = dr_Phrase["TypeName"].ToString();
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_region["EG_Type"].ToString());
                        if (dr_Phrase != null)
                            lbl_EG_Type0.Text = dr_Phrase["TypeName"].ToString();
                        lbl_EG_StartDate0.Text = string.Format("{0}~{1}", DateTime.Parse(dr_region["EG_StartDate"].ToString()).ToString("yyyy/MM/dd"), DateTime.Parse(dr_region["EG_Deadline"].ToString()).ToString("yyyy/MM/dd"));
                        lbl_EG_ExamDate0.Text = DateTime.Parse(dr_region["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
                        lbl_EB_Regist.Text = dr_region["EB_Regist"].ToString();
                        DataRow dr_EP = DataEPayment.DataReader_EP_AccID_EG_TransNo(Session["AccID"].ToString(), dr_region["EG_TransNo"].ToString());
                        if (dr_EP != null) //已報名
                        {
                            MainControls.ddlIndexSelectValue(ddl_Education, dr_EP["EP_EduLevel"].ToString());
                            MainControls.ddlIndexSelectValue(rbl_EG_Region, dr_EP["EP_Region"].ToString());
                            hif_EP_TransNo.Value = dr_EP["EP_TransNo"].ToString();
                            hif_EP_UidNo.Value = dr_EP["EP_UidNo"].ToString();
                            hif_EG_TransNo.Value = dr_EP["EG_TransNo"].ToString();
                            txt_EP_Bank.Text = dr_EP["EP_Bank"].ToString();
                            txt_EP_Account.Text = dr_EP["EP_Account"].ToString();
                            txt_EP_TransAmt.Text = dr_EP["EP_TransAmt"].ToString();
                            if (dr_EP["EP_TransDate"].ToString() != "")
                                txt_EP_TransDate.Text = DateTime.Parse(dr_EP["EP_TransDate"].ToString()).ToString("yyyy/MM/dd");
                            TimeRangeAll_EP_TransTime.Time_start = dr_EP["EP_TransTimeS"].ToString();
                            TimeRangeAll_EP_TransTime.Time_end = dr_EP["EP_TransTimeE"].ToString();
                            cbox_EP_IsCash.Checked = dr_EP["EP_IsCash"].ToString() == "True" ? true : false;
                            if (cbox_EP_IsCash.Checked)
                            {
                                txt_EP_Bank.Text = string.Empty;
                                txt_EP_Account.Text = string.Empty;
                                txt_EP_TransDate.Text = string.Empty;
                                TimeRangeAll_EP_TransTime.Init();
                            }
                            txt_EP_Bank.Enabled = !cbox_EP_IsCash.Checked;
                            txt_EP_Account.Enabled = !cbox_EP_IsCash.Checked;
                            txt_EP_TransDate.Enabled = !cbox_EP_IsCash.Checked;
                            TimeRangeAll_EP_TransTime.Enabled = !cbox_EP_IsCash.Checked;
                            DateTime ServerDateTime = MainControls.ServerDateTime();
                            if (MainControls.DateDiffDays(ServerDateTime, DateTime.Parse(dr_region["EG_Deadline"].ToString())) <= 0)
                                lbtn_DeletePersonal.Visible = true;
                            else
                                lbtn_DeletePersonal.Visible = false;
                        }
                        else
                        {
                            lbtn_DeletePersonal.Visible = false;
                            hif_EP_TransNo.Value = System.Guid.NewGuid().ToString("N");
                            hif_EP_UidNo.Value = Session["UidNo"].ToString();
                        }
                        DataTable dr_exam = DataExam.DataReader_EB_TransNo_AccID(dr_region["EB_TransNo"].ToString(), Session["AccID"].ToString()).Tables[0];
                        if (dr_exam != null && dr_exam.Rows.Count != 0)
                        {
                            lbl_examSub.Text = string.Format("最佳成績：{0}", dr_exam.AsEnumerable().Max(row => row["EX_Score"]).ToString());
                        }
                    }
                    break;
                case "3":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "入場證下載");
                    if (ViewState["dt_region"] != null)
                        dr_region = (ViewState["dt_region"] as DataTable).Rows[0];
                    else
                        dr_region = DataERegion.DataReader_EG_TransNo(Request.QueryString["id"]);
                    if (dr_region != null)
                    {
                        ViewState["dt_region"] = dr_region.Table;
                        lbl_EG_Name2.Text = dr_region["EG_Name"].ToString();
                        DataRow dr_EF = DataEFile.DataReader_EF_TransNo(dr_region["EF_TransNo"].ToString());
                        if (dr_EF != null && File.Exists(Server.MapPath(string.Format("~{0}", dr_EF["EF_Appendix"].ToString()))))
                        {
                            Literal_EF1.Text = string.Format("<iframe src=\"{0}\" width=\"100%\" height=\"600\"></iframe>", dr_EF["EF_Appendix"].ToString());
                            hlink_passDL.NavigateUrl = dr_EF["EF_Appendix"].ToString();
                            hlink_passDL0.NavigateUrl = dr_EF["EF_Appendix"].ToString();
                        }
                        else
                        {
                            Literal_EF1.Text = "入場證尚未上傳，請耐心等候或請洽相關人員詢問！";
                            hlink_passDL.Visible = false;
                            hlink_passDL0.Visible = false;
                        }
                    }
                    break;
                case "4":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "報考資訊/題庫下載");
                    if (ViewState["dt_region"] != null)
                        dr_region = (ViewState["dt_region"] as DataTable).Rows[0];
                    else
                        dr_region = DataERegion.DataReader_EG_TransNo(Request.QueryString["id"]);
                    if (dr_region != null)
                    {
                        ViewState["dt_region"] = dr_region.Table;
                        lbl_EG_Name3.Text = dr_region["EG_Name"].ToString();
                        DataRow dr_EF1 = DataEFile.DataReader_EF_TransNo(dr_region["EF_TransNo1"].ToString());
                        if (dr_EF1 != null && File.Exists(Server.MapPath(string.Format("~{0}", dr_EF1["EF_Appendix"].ToString()))))
                        {
                            hlink_quesDL.NavigateUrl = dr_EF1["EF_Appendix"].ToString();
                            hlink_quesDL0.NavigateUrl = dr_EF1["EF_Appendix"].ToString();
                        }
                        else
                        {
                            hlink_quesDL.Visible = false;
                            hlink_quesDL0.Visible = false;
                            Literal_EF2.Text = "題庫檔案尚未上傳，請耐心等候或請洽相關人員詢問！";
                        }
                        DataRow dr_EF2 = DataEFile.DataReader_EF_TransNo(dr_region["EF_TransNo2"].ToString());
                        if (dr_EF2 != null && File.Exists(Server.MapPath(string.Format("~{0}",dr_EF2["EF_Appendix"].ToString()))))
                        {
                            Literal_EF2.Text = string.Format("<iframe src=\"{0}\" width=\"100%\" height=\"600\"></iframe>", dr_EF2["EF_Appendix"].ToString());
                            
                        }
                        else
                        {
                            Literal_EF2.Text = "證照考試相關說明尚未上傳，請耐心等候或請洽相關人員詢問！";
                            
                        }
                    }
                    break;
                default:
                    if (!IsPostBack)
                        dataBind();
                    else
                        Response.Redirect("~/manager/ExamEregion");
                    break;
            }
            MultiView1.ActiveViewIndex = Convert.ToInt16(CommandName);
            this.IsRefresh = true;
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;

        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

            }

        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("Status")) return;
            e.InputParameters.Add("Status", ddlSh_Status.SelectedValue);
            if (ddlSh_EG_Type.SelectedIndex == 0)
                e.InputParameters.Add("EG_Type", "");
            else
                e.InputParameters.Add("EG_Type", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            e.InputParameters.Add("EG_Subject", ddlSh_EG_Subject.SelectedValue);
            string EG_Region = "";
            if (ddlSh_EG_Region.SelectedValue != "")
                EG_Region = string.Format("{0},{1},{2},{3}",
                        ddlSh_EG_Region.SelectedItem.Selected ? "Y" : "N",
                        ddlSh_EG_Region.SelectedItem.Value,
                        (ViewState["ddlSh_EG_Region"] as DataTable).Rows[ddlSh_EG_Region.SelectedIndex - 1]["TypeSubCode"].ToString(),
                        ddlSh_EG_Region.SelectedItem.Text);
            e.InputParameters.Add("EG_Region", EG_Region);
            e.InputParameters.Add("EG_Name", txtSh_EG_Name.Text.Trim());
            e.InputParameters.Add("EG_ExamDate_s", DateRangeSh_EG_ExamDate.Date_start);
            e.InputParameters.Add("EG_ExamDate_e", DateRangeSh_EG_ExamDate.Date_end);
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_Status.SelectedIndex = -1;
            ddlSh_EG_Type.SelectedIndex = -1;
            ddlSh_EG_Subject.SelectedIndex = -1;
            ddlSh_EG_Region.SelectedIndex = -1;
            txtSh_EG_Name.Text = string.Empty;
            DateRangeSh_EG_ExamDate.Init();
            
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void ddlSh_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlSh_EG_Type.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "160");
                    break;
                default:
                    ddlSh_EG_Subject.Items.Clear();
                    break;
            }
        }

        protected void cbox_EP_IsCash_CheckedChanged(object sender, EventArgs e)
        {
            if (cbox_EP_IsCash.Checked)
            {
                txt_EP_Bank.Text = string.Empty;
                txt_EP_Account.Text = string.Empty;
                txt_EP_TransDate.Text = string.Empty;
                TimeRangeAll_EP_TransTime.Init();
            }
            txt_EP_Bank.Enabled = !cbox_EP_IsCash.Checked;
            txt_EP_Account.Enabled = !cbox_EP_IsCash.Checked;
            txt_EP_TransDate.Enabled = !cbox_EP_IsCash.Checked;
            TimeRangeAll_EP_TransTime.Enabled = !cbox_EP_IsCash.Checked;
        }


    }
}