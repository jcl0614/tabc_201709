﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MyCourse : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EC_CName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEAttend.DataColumn_my(this.GridView1);

            StringBuilder sb = new StringBuilder();
            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year));
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
            if (dr_sales != null)
            {
                Literal4.Text = string.Format("{0}，您好<br>本年度您的壽險／產險年度教育訓練時數如下", dr_sales["Name"].ToString());

                sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"tb\" style=\"font-size: 14px\">");
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FFE8B4; color: #883938; font-weight: bold;\">{0}</td><td align=\"center\" width=\"50\" style=\"background-color: #FFE8B4; color: #883938; font-weight: bold;\">{1}</td></tr>", "壽險", "時數");
                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                DataRow dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "1");//壽險
                double ER_Hours = 0;
                if (dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("1").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "應訓時數", ER_Hours.ToString());
                double ED_AttHours = 0;
                if (dr_erule != null && dt_attend != null)
                {
                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                    double.TryParse(hours, out ED_AttHours);
                }
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "實訓時數", ED_AttHours.ToString());
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "尚缺時數", ER_Hours - ED_AttHours < 0 ? 0 : ER_Hours - ED_AttHours);
                sb.Append("</table>");
                Literal1.Text = sb.ToString();

                sb = new StringBuilder();
                sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"tb\" style=\"font-size: 14px\">");
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FFE8B4; color: #883938; font-weight: bold;\">{0}</td><td align=\"center\" width=\"50\" style=\"background-color: #FFE8B4; color: #883938; font-weight: bold;\">{1}</td></tr>", "產險", "時數");
                dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                if (iYear % 5 == 0)
                    iYear = 5;
                else
                    iYear = iYear % 5;
                dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "2");//產險
                ER_Hours = 0;
                if (dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("2").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "應訓時數", ER_Hours.ToString());
                ED_AttHours = 0;
                if (dr_erule != null && dt_attend != null)
                {
                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                    double.TryParse(hours, out ED_AttHours);
                }
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "實訓時數", ED_AttHours.ToString());
                sb.AppendFormat("<tr><td align=\"center\" width=\"100\" style=\"background-color: #FCF3F3;\">{0}</td><td align=\"right\" width=\"50\" style=\"background-color: #FCF3F3;\">{1}</td></tr>", "尚缺時數", ER_Hours - ED_AttHours < 0 ? 0 : ER_Hours - ED_AttHours);
                sb.Append("</table>");
                Literal2.Text = sb.ToString();

                sb = new StringBuilder();
                sb.Append("<table cellpadding=\"5\" cellspacing=\"0\" class=\"tb\" style=\"font-size: 14px\">");
                sb.Append("<tr><td align=\"center\" colspan=\"3\" style=\"background-color: #FFE8B4; color: #883938; font-weight: bold;\">年度強制訓練</td></tr>");
                sb.AppendFormat("<tr><td style=\"background-color: #FCF3F3; font-weight: bold;\" align=\"center\" width=\"300\">{0}</td><td style=\"background-color: #FCF3F3; font-weight: bold;\" align=\"center\" width=\"80\">{1}</td><td style=\"background-color: #FCF3F3; font-weight: bold;\" align=\"center\" width=\"100\">{2}</td></tr>", "課程名稱", "狀態", "完訓日期");
                DataTable dt = DataECourse.DataReader_EC_Year_IsForce(MainControls.ServerDateTime().Year.ToString()).Tables[0];
                foreach(DataRow dr in dt.Rows)
                {
                    string ED_TypeName = "";
                    string ED_FinDate = "";
                    DataRow dr_ED = DataEAttend.DataReader_EC_TransNo_ED_AccID(dr["EC_TransNo"].ToString(), Session["AccID"].ToString());
                    if (dr_ED != null)
                    {
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("131", dr_ED["ED_Type"].ToString());
                        if (dr_Phrase != null)
                        {
                            ED_TypeName = dr_Phrase["TypeName"].ToString();
                        }
                        ED_FinDate = dr_ED["ED_FinDate"].ToString() == "" ? "" : DateTime.Parse(dr_ED["ED_FinDate"].ToString()).ToString("yyyy/MM/dd");
                    }
                    sb.AppendFormat("<tr><td style=\"background-color: #FCF3F3;\" align=\"left\">{0}</td><td style=\"background-color: #FCF3F3; \" align=\"center\">{1}</td><td style=\"background-color: #FCF3F3; \" align=\"center\">{2}</td></tr>", dr["EC_CName"].ToString(), ED_TypeName, ED_FinDate);
                }
                sb.Append("</table>");
                Literal3.Text = sb.ToString();
            }
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EC_Type, true, "", "E001");
            DataPhrase.DDL_TypeName(ddlSh_EC_Rating, true, "", "E002");
            DataPhrase.DDL_TypeName(ddlSh_ED_Type, true, "", "131");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;

            switch (e.CommandName)
            {
                case "Member":
                    {
                        DataRow dr_course = DataECourse.DataReader_EC_TransNo(e.CommandArgument.ToString());
                        if (dr_course != null)
                        {
                            lbl_EC_CName.Text = dr_course["EC_CName"].ToString();
                            if (dr_course["EC_SDate"].ToString() != "")
                                lbl_EC_SDate.Text = string.Format("{0}&nbsp;&nbsp;{1}~{2}", DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_course["EC_STime"].ToString(), dr_course["EC_ETime"].ToString());
                            DataTable dt_EAttend = DataEAttend.DataReader_EC_TransNo(dr_course["EC_TransNo"].ToString()).Tables[0];
                            if (dt_EAttend != null && dt_EAttend.Rows.Count > 0)
                            {
                                DataTable dt_view = new DataTable();

                                dt_view.Columns.Add("cZON_PK_NAME", typeof(string));
                                dt_view.Columns.Add("ED_AccID", typeof(string));
                                dt_view.Columns.Add("ED_Name", typeof(string));
                                dt_view.Columns.Add("ED_SignDate", typeof(string));
                                dt_view.Columns.Add("ED_ExamDate", typeof(string));
                                dt_view.Columns.Add("ED_Score", typeof(string));
                                foreach (DataRow dr in dt_EAttend.Rows)
                                {
                                    string cZON_PK_NAME = "";
                                    string ED_SignDate = "";
                                    DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["ED_AccID"].ToString());
                                    if (dr_sales != null)
                                    {
                                        cZON_PK_NAME = dr_sales["cZON_PK"].ToString() + dr_sales["cZON_NAME"].ToString();
                                        if (dr["ED_SignDate"].ToString() != "")
                                            ED_SignDate = string.Format("{0}&nbsp;&nbsp;{1}~{2}", DateTime.Parse(dr["ED_SignDate"].ToString()).ToString("yyyy/MM/dd"), dr["ED_SigninTime"].ToString(), dr["ED_SignoffTime"].ToString());
                                        if (dr_sales["cSAL_FK"].ToString().Trim() == Session["AccID"].ToString() || dr_sales["DM"].ToString().Trim() == Session["AccID"].ToString())
                                        {
                                            dt_view.Rows.Add(
                                                cZON_PK_NAME,
                                                dr["ED_AccID"].ToString(),
                                                dr["ED_Name"].ToString(),
                                                ED_SignDate,
                                                dr["ED_ExamDate"].ToString() == "" ? "" : DateTime.Parse(dr["ED_ExamDate"].ToString()).ToString("yyyy/MM/dd"),
                                                dr["ED_Score"].ToString()
                                                );
                                        }
                                    }
                                    
                                }
                                GridView2.DataSource = dt_view;
                                GridView2.DataBind();
                            }
                        }

                        ChangeMultiView(1);
                    }
                    break;
                case "SearchMember":
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach(GridViewRow gvr in GridView3.Rows)
                        {
                            if(((CheckBox)gvr.Cells[0].Controls[1]).Checked)
                            {
                                sb.AppendFormat("'{0}',", ((CheckBox)gvr.Cells[0].Controls[1]).ToolTip);
                            }
                        }
                        ViewState["ED_AccID"] = sb.ToString().TrimEnd(',');
                        GridView1.DataBind();
                        ChangeMultiView(0);
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    break;
                case 1:
                    {
                        
                    }
                    break;
                case 2:
                    {
                        DataTable dt = Data_tabcSales.DataReader_Full_DM(Session["AccID"].ToString()).Tables[0];
                        GridView3.DataSource = dt;
                        GridView3.DataBind();
                    }
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    break;
                default:
                    //TODO:程式非共用修改-10
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                Label lbl = new Label();
                lbl.Text = string.Format("{0}<br>", MainControls.ReplaceSpace(e.Row.Cells[8].Text));
                e.Row.Cells[8].Controls.Add(lbl);
                ImageButton ibtn = new ImageButton();
                ibtn.ID = string.Format("ibtn_{0}", e.Row.Cells[12].Text);
                ibtn.CommandName = "Member";
                ibtn.CommandArgument = e.Row.Cells[14].Text;
                ibtn.ToolTip = "上課人員名單";
                ibtn.Command += btn_Command;
                ibtn.ImageUrl = "~/images/more.png";
                ibtn.Enabled = sysValue.Authority.Append;
                e.Row.Cells[8].Controls.Add(ibtn);

                #endregion
            }

        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EC_Type")) return;
            e.InputParameters.Add("EC_Type", ddlSh_EC_Type.SelectedItem.Text);
            e.InputParameters.Add("EC_Rating", ddlSh_EC_Rating.SelectedItem.Text);
            e.InputParameters.Add("EC_CName", Lib.FdVP(txtSh_EC_CName.Text).Trim());
            e.InputParameters.Add("EC_Place", Lib.FdVP(txtSh_EC_Place.Text).Trim());
            if (ViewState["ED_AccID"] == null)
                e.InputParameters.Add("ED_AccID", string.Format("'{0}'", Session["AccID"].ToString()));
            else
                e.InputParameters.Add("ED_AccID", ViewState["ED_AccID"].ToString());
            e.InputParameters.Add("ED_Type", ddlSh_ED_Type.SelectedValue);
            e.InputParameters.Add("EC_SDate_s", DateRangeSh_EC_SDate.Date_start);
            e.InputParameters.Add("EC_SDate_e", DateRangeSh_EC_SDate.Date_end);
            e.InputParameters.Add("ED_Category", ddlSh_ED_Category.SelectedValue);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EC_CName.Text = string.Empty;
            ddlSh_EC_Type.SelectedIndex = -1;
            ddlSh_EC_Rating.SelectedIndex = -1;
            ddlSh_ED_Type.SelectedIndex = -1;
            ddlSh_ED_Category.SelectedIndex = -1;
            txtSh_EC_Place.Text = string.Empty;
            ViewState["ED_AccID"] = null;
            DateRangeSh_EC_SDate.Init();

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

    }
}