﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" ValidateRequest="False" CodeBehind="SysKm.aspx.cs" Inherits="tabc_201709.Manager.SysKm" %>

<%@ Register src="ascx/ckeditor.ascx" tagname="ckeditor" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">

            <asp:LinkButton ID="lnkSortSet" runat="server" CommandName="2"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/sort.png") %>" border="0" style="vertical-align:bottom" /> 順序設定</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">

            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">功能選單：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_name" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">類別：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_mulayer" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">啟用：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_enable" runat="server">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="true" Selected="True">啟用</asp:ListItem>
                                                    <asp:ListItem Value="false">停用</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" width="85">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataSysKm"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">類別</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_mulayer" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">排列順序</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_sort" runat="server" MaxLength="2" Width="100px"></asp:TextBox>
                                                <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5; letter-spacing: 1px;">數字0~99</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">功能選單名稱</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_name" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
                                                <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                                                <span style="color: #D663A5; letter-spacing: 1px;">長度最多50個字元</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">說明內容</td>
                                            <td class="tdData10s">
                                                <uc1:ckeditor ID="ckeditor1" runat="server" />
                                                </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">啟用</td>
                                            <td class="tdData10s">
                                                <asp:CheckBox ID="cbox_enable" runat="server" Checked="True" />
                                                <asp:HiddenField ID="hf_id" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">編修人員</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_name" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">更新日期</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_updateDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnAppend" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit"
                                                    OnCommand="btn_Command" Text="存檔" CssClass="btnSave" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete"
                                                    OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command"
                                                    Text="刪除" CssClass="btnDelete" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel"
                                                    OnCommand="btn_Command" Text="取消" CssClass="btnCancel" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="left" style="line-height: 125%">
                                                            <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <asp:UpdatePanel ID="UpdatePanel_Sort" runat="server">
                            <ContentTemplate>
                                <table align="center" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">目前顯示順序<br />
                                            <asp:DropDownList ID="ddlSort_Class" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSort_Class_SelectedIndexChanged" Width="300px">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                        <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">新設顯示順序</td>
                                        <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                        </td>
                                        <td align="center">
                                            <p>
                                                <asp:ImageButton ID="ibtnRefresh" runat="server" CommandName="refresh" ImageUrl="~/Manager/images/Refresh-icon.png" OnCommand="btnSort_Command" />
                                                <br />
                                                <br />
                                                <asp:Button ID="btnAddAll" runat="server" CommandName="addAll" CssClass="btnAddAll" OnCommand="btnSort_Command" Text="全部加入" />
                                                <br />
                                                <br />
                                                <asp:Button ID="btnRemoveAll" runat="server" CommandName="removeAll" CssClass="btnRemoveAll" OnCommand="btnSort_Command" Text="全部移除" />
                                                <br />
                                                <br />
                                                <asp:ImageButton ID="ibtnRight" runat="server" CommandName="add" ImageUrl="~/Manager/images/Forward-icon.png" OnCommand="btnSort_Command" />
                                                <br />
                                                <br />
                                                <asp:ImageButton ID="ibtnLeft" runat="server" CommandName="remove" ImageUrl="~/Manager/images/Back-icon.png" OnCommand="btnSort_Command" />
                                            </p>
                                        </td>
                                        <td align="center">
                                            <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                        </td>
                                        <td align="center">
                                            <asp:Button ID="btnSaveSort" runat="server" CommandName="save" CssClass="btnExcute" OnCommand="btnSort_Command" Text="執行" />
                                            <br />
                                            <br />
                                            <asp:Button ID="btnCancelSort" runat="server" CommandName="cancel" CssClass="btnCancel" OnCommand="btnSort_Command" Text="取消" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px">
                                            <asp:Label ID="labSortMsg" runat="server" CssClass="errmsg12"></asp:Label>
                                        </td>
                                        <td align="center"></td>
                                        <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px">
                                            <asp:ImageButton ID="ibtnUp" runat="server" CommandName="up" ImageUrl="~/Manager/images/Up-icon.png" OnCommand="btnSort_Command" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="ibtnDown" runat="server" CommandName="down" ImageUrl="~/Manager/images/Down-icon.png" OnCommand="btnSort_Command" />
                                        </td>
                                        <td align="center"></td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>



</asp:Content>
