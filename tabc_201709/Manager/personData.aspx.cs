﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace tabc_201709.Manager
{
    public partial class personData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("personData", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                ViewState["PageTitle"] = lbl_PageTitle.Text;
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1400);
                MainControls.GridViewStyleCtrl(GridView2, 300);
                MainControls.GridViewStyleCtrl(GridView3, 600);
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();

                showPhoto();

            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataRow dr_PS = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if(dr_PS != null)
            {
                lbl_CP_ComyName.Text = "台名";
                lbl_PS_NAME.Text = dr_PS["PS_NAME"].ToString();
                lbl_PS_Title.Text = dr_PS["PS_Title"].ToString();
                lbl_AccID.Text = dr_PS["AccID"].ToString();
                lbl_PS_MOIBLE.Text = dr_PS["PS_MOIBLE"].ToString();
                lbl_PS_EMAIL.Text = dr_PS["PS_EMAIL"].ToString();
                lbl_PS_LifeNo.Text = dr_PS["PS_LifeNo"].ToString();
                if(dr_PS["PS_FirstDate"].ToString().Trim() != "")
                    lbl_PS_FirstDate.Text = DateTime.Parse(dr_PS["PS_FirstDate"].ToString()).ToString("yyyy/MM/dd");
                lbl_PS_ProductNo.Text = dr_PS["PS_ProductNo"].ToString();
                if (dr_PS["PS_StartDate"].ToString().Trim() != "")
                    lbl_PS_StartDate.Text = DateTime.Parse(dr_PS["PS_StartDate"].ToString()).ToString("yyyy/MM/dd");
            }
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
            if (dr_sales != null)
            {
                DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year));
                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                DataRow dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "1");//壽險
                double ER_Hours = 0;
                if (dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("1").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                double ED_AttHours = 0;
                if (dr_erule != null && dt_attend != null)
                {
                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                    double.TryParse(hours, out ED_AttHours);
                }
                lbl_ER_Hours.Text = ER_Hours.ToString();
                lbl_ED_AttHours.Text = ED_AttHours.ToString();

                dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                if (iYear % 5 == 0)
                    iYear = 5;
                else
                    iYear = iYear % 5;
                dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "2");//產險
                ER_Hours = 0;
                if (dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("2").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                    double.TryParse(dr_erule["ER_Hours"].ToString(), out ER_Hours);
                ED_AttHours = 0;
                if (dr_erule != null && dt_attend != null)
                {
                    var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                    double.TryParse(hours, out ED_AttHours);
                }
                lbl_ER_Hours1.Text = ER_Hours.ToString();
                lbl_ED_AttHours1.Text = ED_AttHours.ToString();
            }
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ddlSh_year.Items.Clear();
            int y = DateTime.Now.Year - 1911;
            for (int i=y; i>y-10; i--)
            {
                ddlSh_year.Items.Add(i.ToString());
            }
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            switch (index)
            {
                case 0:
                    lbl_PageTitle.Text = string.Format("{0}-{1}", ViewState["PageTitle"], "基本資料");
                    break;
                case 1:
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", ViewState["PageTitle"], "個人傭金");
                        getSalary(ddlSh_year.SelectedValue);
                    }
                    break;
                case 2:
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", ViewState["PageTitle"], "公積金");
                        getFund();
                    }
                    break;
                case 3:
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", ViewState["PageTitle"], "出席率");
                        DateRange1.Date_start = string.Format("{0}/{1}/01", DateTime.Now.AddMonths(-2).Year, DateTime.Now.AddMonths(-2).Month.ToString("00"));
                        DateRange1.Date_end = string.Format("{0}/{1}/{2}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));
                        Attendance();
                    }
                    break;
            }

        }
        #endregion

        #region 取得年份薪資
        private void getSalary(string year)
        {
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                                          "     <Requests Name=\"TM_查詢_0001_業查_個人薪資\" Text=\"Request\" RB=\"RB(N)\">" +
                                          "      <Request Name=\"TM_查詢_0001_業查_個人薪資\" Program=\"TM_001_Request\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                                          "       <Parameters>" +
                                          "        <Parameter Name=\"@P_iBAL_YEAR\" Value=\"" + ddlSh_year.SelectedValue + "\"/>" +
                                          "        <Parameter Name=\"@P_cSAL_FK\" Value=\"'" + Session["AccID"].ToString() + "'\"/>" +
                                          "       </Parameters>" +
                                          "       <ReturnCols/>" +
                                          "      </Request>" +
                                          "     </Requests>" +
                                          "</BIS>";
            DataTable dtDisPlay = getAPI(setSearch);
            if (dtDisPlay != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("薪資類別", typeof(string));
                dt.Columns.Add("期數", typeof(string));
                dt.Columns.Add("承攬所得", typeof(string));
                dt.Columns.Add("營運金", typeof(string));
                dt.Columns.Add("其他所得", typeof(string));
                dt.Columns.Add("所得扣款", typeof(string));
                dt.Columns.Add("應稅所得", typeof(string));
                dt.Columns.Add("所得稅", typeof(string));
                dt.Columns.Add("本期應付", typeof(string));
                dt.Columns.Add("本期應收", typeof(string));
                dt.Columns.Add("上期應收不足", typeof(string));
                dt.Columns.Add("公積金", typeof(string));
                dt.Columns.Add("合計", typeof(string));
                foreach (DataRow dr in dtDisPlay.Rows)
                {
                    dt.Rows.Add(
                        dr["薪資類別"].ToString(),
                        dr["期數"].ToString(),
                        dr["承攬所得"].ToString(),
                        dr["營運金"].ToString(),
                        dr["其他所得"].ToString(),
                        dr["所得扣款"].ToString(),
                        dr["應稅所得"].ToString(),
                        dr["所得稅"].ToString(),
                        dr["本期應付"].ToString(),
                        dr["本期應收"].ToString(),
                        dr["上期應收不足"].ToString(),
                        dr["公積金"].ToString(),
                        dr["合計"].ToString()
                        );
                }
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
        }
        protected void ddlSh_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            getSalary(ddlSh_year.SelectedValue);
        }
        #endregion

        #region 取得公積金
        private void getFund()
        {
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                                          "     <Requests Name=\"TM_查詢_0009_業查_公積金\" Text=\"Request\" RB=\"RB(N)\">" +
                                          "      <Request Name=\"TM_查詢_0009_業查_公積金\" Program=\"TM_009_Request\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                                          "       <Parameters>" +
                                          "        <Parameter Name=\"@P_cSAL_FK\" Value=\"" + Session["AccID"].ToString() + "\"/>" +
                                          "       </Parameters>" +
                                          "       <ReturnCols/>" +
                                          "      </Request>" +
                                          "     </Requests>" +
                                          "</BIS>";
            DataTable dtDisPlay = getAPI(setSearch);
            if (dtDisPlay != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("年度", typeof(string));
                dt.Columns.Add("金額", typeof(string));
                dt.Columns.Add("累計金額", typeof(string));
                foreach (DataRow dr in dtDisPlay.Rows)
                {
                    dt.Rows.Add(
                        dr["年度"].ToString(),
                        dr["金額"].ToString(),
                        dr["累計金額"].ToString()
                        );
                }
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
        }
        #endregion

        #region 取得出席率
        private void Attendance()
        {
            string setSearch = "<BIS Request=\"SWP(N)\">"+
                                "<Requests Name=\"TM_查詢_0010_業查_出席率\" Text=\"Request\" RB=\"RB(N)\">"+
                                "<Request Name=\"TM_查詢_0010_業查_出席率\" Program=\"TM_010_Request\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">"+
                                "<Parameters>"+
                                "<Parameter Name=\"@P_dSTARTDATE\" Value=\"'" + DateRange1.Date_start + "'\"/>"+
                                "<Parameter Name=\"@P_dENDDATE\"   Value=\"'" + DateRange1.Date_end + "'\"/>"+
                                "<Parameter Name=\"@P_cSAL_STATUS\"   Value=\"''\"/>"+
                                "<Parameter Name=\"@P_cSAL_FK\"     Value=\"" + Session["AccID"].ToString() + "\"/>"+
                                "</Parameters>"+
                                "<ReturnCols/>"+
                                "</Request>"+
                                "</Requests>"+
                                "</BIS>";
            DataTable dtDisPlay = getAPI(setSearch);
            if (dtDisPlay != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("業務員", typeof(string));
                dt.Columns.Add("月份", typeof(string));
                dt.Columns.Add("應出席次數", typeof(string));
                dt.Columns.Add("實際出席次數", typeof(string));
                dt.Columns.Add("當月出席率", typeof(string));
                foreach (DataRow dr in dtDisPlay.Rows)
                {
                    dt.Rows.Add(
                        dr["業務員"].ToString(),
                        dr["月份"].ToString(),
                        dr["應出席次數"].ToString(),
                        dr["實際出席次數"].ToString(),
                        dr["當月出席率"].ToString()
                        );
                }
                DataRow dr_PS = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                if (dr_PS != null)
                {
                    GridView3.DataSource = dt.AsEnumerable().Where(x => x.Field<string>("業務員").IndexOf(dr_PS["PS_NAME"].ToString().Trim()) != -1).AsDataView();
                    GridView3.DataBind();
                }
                
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Attendance();
        }
        #endregion

        private void showPhoto()
        {
            if (File.Exists(Server.MapPath(string.Format("~/Uploads/PersonPhoto/{0}.jpg", Session["AccID"].ToString()))))
                Literal_photo.Text = string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"{0}\" width=\"100\" border=\"0\" /></a>", string.Format("/Uploads/PersonPhoto/{0}.jpg?{1}", Session["AccID"].ToString(), DateTime.Now.ToString("yyyyMMddHHmmss")));
        }

        protected void btnUplod_Click(object sender, EventArgs e)
        {
            if (FileUpload1.FileName != "")
            {
                Lib.MakeImageTitleWH(
                    FileUpload1.FileContent,
                    Server.MapPath(string.Format("~/Uploads/PersonPhoto/{0}.jpg", Session["AccID"].ToString())),
                    600, 600, "", ""
                    );
                showPhoto();
            }
        }

        private DataTable getAPI(string setSearch)
        {
            XmlDocument doc = new XmlDocument();
            XmlNodeList NodeList = null;
            DataTable dtDisPlay = new DataTable();
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                dtDisPlay = XMLtoDataTable(ret, "BIS/Responds/Respond/N");
            }
            return dtDisPlay;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }
        private static DataTable XMLtoDataTable(string xml, string SelectNodes)
        {
            DataTable dtDisPlay = new DataTable();
            try
            {
                #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(
                    xml
                    .Replace("&", HttpUtility.HtmlEncode("&"))
                    .Replace("\u0002", "")
                    );
                XmlNodeList NodeList = doc.SelectNodes(SelectNodes);
                foreach (XmlNode COB in NodeList)
                {
                    if (dtDisPlay.Columns.Count == 0)
                    {
                        foreach (XmlAttribute Attr in COB.Attributes)
                        {
                            dtDisPlay.Columns.Add(Attr.Name.ToString());
                        }
                    }

                    DataRow drnew = dtDisPlay.NewRow();
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        if (dtDisPlay.Columns.Contains(Attr.Name.ToString()))
                        {
                            drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value;
                        }
                    }
                    drnew.EndEdit();
                    dtDisPlay.Rows.Add(drnew);
                }
                #endregion
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return dtDisPlay;
        }

        
    }
}