﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MenuLayer : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 700);
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataMenuLayer.MenuLayerDataColumn(this.GridView1);
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Enabled = false;
            lnkSortSet.Enabled = false;
            switch (index)
            {
                case 0:
                    lnkSortSet.Enabled = true;
                    break;
                case 1:
                    lnkBrowse.Enabled = true;
                    MarkSortSetInit();
                    break;
            }
        }
        #endregion

        #region 資料順序設定模組

        #region 順序調整初始化
        protected void MarkSortSetInit()
        {
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            DataSet ds = DataMenuLayer.DataMenuLayerReader(false);
            if (ds != null)
            {
                foreach (DataRow dw in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem();
                    li.Text = dw["name"].ToString().Trim();
                    li.Value = dw["id"].ToString().Trim();
                    lboxSur.Items.Add(li);
                }
            }
        }
        #endregion

        #region 模組按鈕觸發處理
        protected void btnSort_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) < 0)
                            lboxObj.Items.Add(m_item);
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            lboxObj.Items.Add(itemSur);
                            lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                            lboxSur.Items.Remove(itemSur);
                            if (lboxSur.Items.Count > siSur)
                                lboxSur.SelectedIndex = siSur;
                            else
                                lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "up"://上移按鈕
                    int intUp = lboxObj.SelectedIndex;
                    ListItem itemUp = lboxObj.SelectedItem;
                    if (intUp > 0)
                    {
                        lboxObj.Items.Remove(itemUp);
                        lboxObj.Items.Insert(intUp - 1, itemUp);
                        lboxObj.SelectedIndex = intUp - 1;
                    }
                    break;
                case "down"://下移按鈕
                    int intDown = lboxObj.SelectedIndex;
                    ListItem itemDown = lboxObj.SelectedItem;
                    if (intDown < lboxObj.Items.Count - 1)
                    {
                        lboxObj.Items.Remove(itemDown);
                        lboxObj.Items.Insert(intDown + 1, itemDown);
                        lboxObj.SelectedIndex = intDown + 1;
                    }
                    break;
                case "save"://儲存新顯示順序設定
                    if (lboxObj.Items.Count != 0)
                    {
                        RServiceProvider rsp = DataMenuLayer.SortUpdate(lboxObj);
                        if (rsp.Result)
                        {
                            MainControls.showMsg(this, "儲存完成!");
                            GridView1.DataBind();
                        }
                        else
                            MainControls.showMsg(this, "儲存失敗!");
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入項目!");
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    labSortMsg.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }
        #endregion

        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView1_RowDataBound;
            grv.DataBound += GridView1_DataBound;
            objDS.Selected += ObjectDataSource1_Selected;
            objDS.Selecting += ObjectDataSource1_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows1_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex1_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl1_Command;
            lbtnPagePre.Command += lbtnPageCtrl1_Command;
            lbtnPageNext.Command += lbtnPageCtrl1_Command;
            lbtnPageLast.Command += lbtnPageCtrl1_Command;
        }

        bool chagePageRow1 = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow1 = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices1(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //預防無資料時分頁控制按鈕預設會啟用
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);

            if (e.ReturnValue is DataSet)
                ViewState["table1"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow1)
                {
                    chagePageRow1 = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl1_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((Button)e.Row.Cells[4].Controls[0]).Text == "編輯")
                {
                    ((Button)e.Row.Cells[4].Controls[0]).CssClass = "btnEdit";
                    ((Button)e.Row.Cells[4].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[4].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }
                if (((Button)e.Row.Cells[4].Controls[0]).Text == "更新")
                {
                    ((Button)e.Row.Cells[4].Controls[0]).CssClass = "btnSave2";
                    ((Button)e.Row.Cells[4].Controls[2]).CssClass = "btnCancel2";
                    ((Button)e.Row.Cells[4].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[4].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //if (e.InputParameters.Contains("")) return;
            //e.InputParameters.Add("", );
        }
        #endregion

        #endregion

        #region 更新權限資料
        protected void ObjectDataSource1_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            string id = GridView1.SelectedRow.Cells[0].Text;
            if (e.InputParameters.Contains("id")) return;
            {
                e.InputParameters.Insert(0, "id", id);
                e.InputParameters.Insert(1, "emNo", sysValue.emNo);
                e.InputParameters.Insert(2, "progId", sysValue.ProgId);
            }
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView1.SelectedIndex = e.RowIndex;
        }
        #endregion

        #region command field及button field加確認屬性的方法 && GridView 編輯時，調整欄寬
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)
                    || e.Row.RowState == DataControlRowState.Edit
                    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit)
                    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit | DataControlRowState.Alternate)
                    )
                {
                    //Width屬性是Unit類型，不能直接給整數值
                    //Unit.Pixel():依像表設定絕對寬度；Unit.Percent():依百分比設定寬度
                    ((TextBox)e.Row.Cells[1].Controls[0]).Width = Unit.Pixel(290);
                    ((TextBox)e.Row.Cells[1].Controls[0]).MaxLength = 20;
                    ((TextBox)e.Row.Cells[2].Controls[0]).Style.Add("text-align", "center");
                }

                if (((Button)e.Row.Cells[4].Controls[0]).Text == "編輯")
                    ((Button)e.Row.Cells[4].Controls[0]).Enabled = sysValue.Authority.Update;
            }
        }
        #endregion

    }
}