﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ChangePWD.aspx.cs" Inherits="tabc_201709.Manager.ChangePWD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            &nbsp;
            <asp:Panel ID="Panel_Form" runat="server">
                <table align="center" border="0" cellpadding="2" cellspacing="1" width="580">
                    <tr>
                        <td class="MS_tdTitle" width="110">
                            帳　號</td>
                        <td class="tdData10s">
                            <asp:Label ID="lab_emID" runat="server" Font-Size="Small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="110">
                            姓　名</td>
                        <td class="tdData10s">
                            <asp:Label ID="lab_emName" runat="server" Font-Size="Small"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="110">
                            舊密碼</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_emPwd" runat="server" TextMode="Password" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="110">
                            新密碼</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_emNewPwd1" runat="server" MaxLength="20" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="110">
                            新密碼確認</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_emNewPwd2" runat="server" MaxLength="20" 
                                TextMode="Password" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="tdMaintainButton" colspan="2">
                            <asp:Button ID="btnEdit" runat="server" CommandName="RunChange" 
                                CssClass="btnSave" onclientclick=" return confirm(&quot;確定要儲存變更嗎?&quot;);" 
                                oncommand="btn_Command" Text="密碼變更" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="line-height: 125%">
                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            &nbsp;
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

