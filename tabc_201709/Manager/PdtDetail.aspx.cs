﻿using AppCode.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class PdtDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("PdtDetail", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }

                //資料繫結
                dataBind();

                
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            if (Request.QueryString["ComSName"] != null && Request.QueryString["PdtCode"] != null)
            {
                string ComSName = Server.UrlDecode(Request.QueryString["ComSName"]);
                string PdtCode = Server.UrlDecode(Request.QueryString["PdtCode"]);
                DataRow dr_TCM = DataTABC_COMPANY_MAP.DataReader_TCM_ComCode(ComSName);
                if (dr_TCM != null && PdtCode!= "")
                {
                    DataRow dr_PdtCode = DataPlanCodeMap.DataReader_PlanMark(PdtCode);
                    if (dr_PdtCode != null)
                    {
                        DataTable dt = getAPI_GetInsFullInfo(dr_TCM["ComCode_GT"].ToString(), dr_PdtCode["GoupMark"].ToString());
                        if (dt != null && dt.Rows.Count != 0)
                        {
                            Literal1.Text = dt.Rows[0]["Premiums"].ToString();
                            Literal2.Text = dt.Rows[0]["KeyTitles"].ToString().Replace(";", "</br>");//特色
                            Literal3.Text = dt.Rows[0]["Intrude"].ToString();//簡介
                            if (!Convert.ToBoolean(dt.Rows[0]["isPDFLaw"]))
                                Literal4.Text = dt.Rows[0]["LawDisplay"].ToString();//條款
                            else
                            {
                                string LawPdfurl = GetLawPdfApi(dt.Rows[0]["LawDisplay"].ToString());
                                StringBuilder sb = new StringBuilder();
                                sb.AppendFormat("<iframe src=\"{0}\" width=\"100%\" height=\"500\"></iframe>", LawPdfurl);
                                Literal4.Text = sb.ToString();
                            }
                        }
                    }
                }
            }
        }
        #endregion

        private DataTable getAPI_GetInsFullInfo(string CompanyNo, string Mark)
        {
            DataTable dt = new DataTable();
            try
            {
                string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsFullInfo");
                string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
                string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
                string no = string.Format("{0}|{1}|{2}|{3}|{4}", CompanyNo, Mark, DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
                no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
                string err = "";
                string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
                if (err.Length == 0)
                {
                    dt = JsonConvert.DeserializeObject<DataTable>(string.Format("[{0}]", ret));
                }

            }
            catch (Exception ex)
            {
                MainControls.showMsg(this, "取得資料失敗，請稍後再試！");
            }
            return dt;
        }

        public string GetLawPdfApi(string lawdisplay)
        {
            string ApiURL = string.Format("{0}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_GetLawPdfApi"]);
            string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
            string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
            //  string lawdisplay = tBoxIN.Text; //lawdisplay = "pr/pr-086.pdf"
            string[] lawFile = lawdisplay.Replace('\\', '/').Split('/');
            // CompanyNo|PDFName|yyyyMMddHH|ID|Channel
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", lawFile[0], lawFile[1]
                               , DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            ApiURL = string.Format("{0}?no={1}", ApiURL, no);
            return ApiURL;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }
    }
}