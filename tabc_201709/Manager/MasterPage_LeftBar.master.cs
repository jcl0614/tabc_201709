﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MasterPage_LeftBar : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["WebMessage_show"] = DataWebSet.dataReader("WebMessage") != "" ? true : false;
                if (Session["emUser"].ToString().Trim() != "")
                    labMsUser.Text = Session["emUser"].ToString().Trim();

                lbl_WebMessage.Text = DataWebSet.dataReader("WebMessage");
                pnl_WebMessage.Visible = bool.Parse(ViewState["WebMessage_show"].ToString());

                //操作說明 
                DataTable dtKm = new DataTable();
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    DataTable dtMenu = DataMenu.menuReader(Request.Url.Segments[i].Replace("/", string.Empty));
                    if (dtMenu.Rows.Count != 0)
                    {
                        dtKm = DataSysKm.id_DataReader(dtMenu.Rows[0]["muKm"].ToString());
                        break;
                    }
                }
                if (dtKm.Rows.Count != 0)
                {
                    if (Boolean.Parse(dtKm.Rows[0]["enable"].ToString()))
                    {
                        lbl_km.Visible = true;
                        lbl_km.Text = MainControls.RedirectLinkStr(string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align:bottom\" /> 操作說明", ResolveUrl("images/info-icon.png")), "", "_blank", "style=\" color: #3399FF; font-size: 14px; text-decoration: none; font-family: 微軟正黑體;\"", ResolveUrl(string.Format("km/{0}", dtKm.Rows[0]["id"].ToString())));
                    }
                    else
                    {
                        lbl_km.Visible = false;
                    }
                }
                else
                    lbl_km.Visible = false;
            }

            lbl_startDate.Text = string.Format("{0} {1}", MainControls.GetDate("Date"), MainControls.GetDate("Time"));
            DateTime start = DateTime.Parse(lbl_startDate.Text);
            DateTime end = start.AddSeconds(int.Parse(DataWebSet.dataReader("SysLogoutTime")));
            lbl_endDate.Text = end.ToString("yyyy/M/d H:mm:ss");
        }

        protected void img_signoff_Click(object sender, ImageClickEventArgs e)
        {
            pnl_WebMessage.Visible = false;
            ViewState["WebMessage_show"] = false;
        }


    }
}