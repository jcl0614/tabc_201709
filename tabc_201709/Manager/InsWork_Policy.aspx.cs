﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace tabc_201709.Manager
{
    public partial class InsWork_Policy : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("InsWork_Policy", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1400);
                //下拉選單設定
                setddlChoices();
                //資料繫結
                dataBind();

            }
            else
                GridView1.DataSource = ObjectDataSource1;
        }

        #region 資料繫結
        private void dataBind()
        {
            DataInsWork_Policy.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            XmlNodeList NodeList = getAPI_Initial(Session["AccID"].ToString());
            if (NodeList != null)
            {
                foreach (XmlNode COB in NodeList)
                {
                    String StrNodeName = COB.Name.ToString();
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        StringBuilder sb = new StringBuilder("");
                        String StrAttr = Attr.Name.ToString();
                        String StrValue = COB.Attributes[Attr.Name.ToString()].Value;
                        XmlNodeList COBNodeList = COB.SelectNodes("N");
                        if (StrValue == "COB_轄下業務員")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_cSAL_ID_S, false, "");
                        }
                        else if (StrValue == "COB_日期類別")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_iDATE_TYPE, false, "");
                        }
                        else if (StrValue == "COB_保單類別")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_iINS_TYPE, false, "");
                        }
                        else if (StrValue == "COB_壽險保險公司")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_cINS_FK, false, "");

                        }
                        else if (StrValue == "COB_產險保險公司")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_cINS_FK_P, false, "");

                        }
                        else if (StrValue == "COB_保單狀態")
                        {
                            getTagSelect(COBNodeList, "Label", "Value", ddl_P_cINS_STATUS, true, "");

                        }

                    }

                }
            }
            DataPhrase.DDL_TypeName(ddl_field, true, "請選擇", "119");
        }
        private void getTagSelect(XmlNodeList COBNodeList, string displayfd, string valfd, DropDownList DDL, bool all, string allString)
        {
            DDL.Items.Clear();
            if (COBNodeList != null)
            {
                if (all)
                    DDL.Items.Add(new ListItem(allString, ""));
                foreach (XmlNode COBNode in COBNodeList)
                {
                    ListItem listItem = new ListItem(COBNode.Attributes[displayfd].Value, COBNode.Attributes[valfd].Value);
                    DDL.Items.Add(listItem);
                }
            }
        }
        #endregion

        private XmlNodeList getAPI_Initial(string AccID)
        {
            XmlDocument doc = new XmlDocument();
            XmlNodeList NodeList = null;
            DataTable dtDisPlay = new DataTable();
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                               "     <Requests Name=\"TM_查詢_0003_業查_保單查詢\" Text=\"Initial\" RB=\"RB(N)\">" +
                               "      <Request Name=\"TM_查詢_0003_業查_保單查詢\" Program=\"TM_003_Initial\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                               "       <Parameters>" +
                               "        <Parameter Name=\"@P_cSAL_FK\" Value=\"" + AccID + "\"/>" +
                               "       </Parameters>" +
                               "       <ReturnCols/>" +
                               "      </Request>" +
                               "     </Requests>" +
                               "</BIS>";
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                doc.LoadXml(ret);
                //建立根節點
                NodeList = doc.SelectNodes("BIS/Responds/Respond/COB");
            }
            return NodeList;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }
        private static DataTable XMLtoDataTable(string xml, string SelectNodes)
        {
            DataTable dtDisPlay = new DataTable();
            try
            {
                #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(
                    xml
                    .Replace("&", HttpUtility.HtmlEncode("&"))
                    .Replace("\u0002", "")
                    );
                XmlNodeList NodeList = doc.SelectNodes(SelectNodes);
                foreach (XmlNode COB in NodeList)
                {
                    if (dtDisPlay.Columns.Count == 0)
                    {
                        foreach (XmlAttribute Attr in COB.Attributes)
                        {
                            dtDisPlay.Columns.Add(Attr.Name.ToString());
                        }
                    }

                    DataRow drnew = dtDisPlay.NewRow();
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        if (dtDisPlay.Columns.Contains(Attr.Name.ToString()))
                        {
                            drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value;
                        }
                    }
                    drnew.EndEdit();
                    dtDisPlay.Rows.Add(drnew);
                }
                #endregion
            }
            catch (Exception ex)
            {
                string s = ex.Message;
            }
            return dtDisPlay;
        }

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("P_cINS_NO")) return;
            e.InputParameters.Add("P_cINS_NO", txt_P_cINS_NO.Text.Trim());
            e.InputParameters.Add("P_cINS_FK", ddl_P_cINS_FK.SelectedValue);
            e.InputParameters.Add("P_cINS_FK_P", ddl_P_cINS_FK_P.SelectedValue);
            e.InputParameters.Add("P_iINS_TYPE", ddl_P_iINS_TYPE.SelectedValue);
            e.InputParameters.Add("P_cINS_MAN_A", txt_P_cINS_MAN_A.Text.Trim());
            e.InputParameters.Add("P_cINS_MAN_ID_A", txt_P_cINS_MAN_ID_A.Text.Trim());
            e.InputParameters.Add("P_cINS_MAN_B", txt_P_cINS_MAN_B.Text.Trim());
            e.InputParameters.Add("P_cINS_MAN_ID_B", txt_P_cINS_MAN_ID_B.Text.Trim());
            e.InputParameters.Add("P_dDATE_S", txt_P_dDATE_S.Text.Trim());
            e.InputParameters.Add("P_dDATE_E", txt_P_dDATE_E.Text.Trim());
            e.InputParameters.Add("P_cSAL_ID_S", ddl_P_cSAL_ID_S.SelectedValue);
            e.InputParameters.Add("P_iDATE_TYPE", ddl_P_iDATE_TYPE.SelectedValue);
            e.InputParameters.Add("P_cINS_STATUS", ddl_P_cINS_STATUS.SelectedValue);
            if (!IsPostBack)
                e.InputParameters.Add("P_cSAL_FK", "");
            else
                e.InputParameters.Add("P_cSAL_FK", Session["AccID"].ToString());
            e.InputParameters.Add("field", ddl_field.SelectedItem.Text);
            e.InputParameters.Add("keyword", txt_keyword.Text.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txt_P_cINS_NO.Text = string.Empty;
            ddl_P_cINS_FK.SelectedIndex = -1;
            ddl_P_cINS_FK_P.SelectedIndex = -1;
            ddl_P_iINS_TYPE.SelectedIndex = -1;
            txt_P_cINS_MAN_A.Text = string.Empty;
            txt_P_cINS_MAN_ID_A.Text = string.Empty;
            txt_P_cINS_MAN_B.Text = string.Empty;
            txt_P_cINS_MAN_ID_B.Text = string.Empty;
            txt_P_dDATE_S.Text = string.Empty;
            txt_P_dDATE_E.Text = string.Empty;
            ddl_P_cSAL_ID_S.Text = string.Empty;
            ddl_P_iDATE_TYPE.SelectedIndex = -1;
            ddl_P_cINS_STATUS.SelectedIndex = -1;
            ddl_field.SelectedIndex = -1;
            txt_keyword.Text = string.Empty;

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }

        #endregion

        #endregion

        protected void ddl_P_iINS_TYPE_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_P_iINS_TYPE.SelectedValue == "1" || ddl_P_iINS_TYPE.SelectedValue == "2")
            {
                ddl_P_cINS_FK.SelectedIndex = -1;
                ddl_P_cINS_FK_P.SelectedIndex = -1;
                ddl_P_cINS_FK.Visible = true;
                ddl_P_cINS_FK_P.Visible = false;
            }
            else
            {
                ddl_P_cINS_FK.SelectedIndex = -1;
                ddl_P_cINS_FK_P.SelectedIndex = -1;
                ddl_P_cINS_FK.Visible = false;
                ddl_P_cINS_FK_P.Visible = true;
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Literal_tetail.Text = string.Empty;
            try
            {
                WebClient wc = new WebClient();
                Literal_tetail.Text = wc.DownloadString(string.Format("https://agent.tabc.com.tw/SalesWeb/{0}.asp?iINS_PK={1}", ((LinkButton)sender).CommandArgument.Split('_')[1] == "3" ? "Prod_Ins" : "Life_Ins", ((LinkButton)sender).CommandArgument.Split('_')[0]));
                ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", string.Format("showDetail('保單號碼：{0}');", ((LinkButton)sender).CommandName), true);
            }
            catch (Exception ex)
            {
                MainControls.showMsg(this, "讀取資料失敗！");
            }
        }
    }
}