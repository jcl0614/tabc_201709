﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EPaymentExam : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EG_Name);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(selRegion_EG_TransNo);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(selRegion_EG_TransNo);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 900);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEPayment.DataColumn_exam(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
            ddlSh_year.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
            ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
            ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selRegion_EG_TransNo.SelectedValue.Trim() == "")
                sbError.Append("●「考試名稱」必須選取!<br>");
            if (rbl_EP_Region.SelectedIndex == -1)
                sbError.Append("●「考試地區」必須選取!<br>");
            if (txt_EP_SignDate.Text.Trim() == "")
                sbError.Append("●「報名日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EP_SignDate.Text.Trim()))
                    sbError.Append("●「報名日期」格式錯誤!<br>");
            }
            if (selPerson_AccID_EP_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「人員」必須選取!<br>");
            if (txt_EG_Training.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_EG_Training.Text.Trim()))
                    sbError.Append("●「訓練日期」格式錯誤!<br>");
            }
            //if (txt_EP_Bank.Text.Trim() == "")
            //    sbError.Append("●「轉出銀行」必須輸入!<br>");
            if (txt_EP_Account.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\d{5}$", (txt_EP_Account.Text.Trim())))
                    sbError.Append("●「轉出帳號末5碼」格式錯誤!<br>");
            }
            if (txt_EP_TransAmt.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EP_TransAmt.Text.Trim())))
                    sbError.Append("●「轉帳金額」格式錯誤!<br>");
            }
            
            if (txt_EP_TransDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_EP_TransDate.Text.Trim()))
                    sbError.Append("●「轉帳日期」格式錯誤!<br>");
            }
            if (TimeRangeAll_EP_TransTime.Time_start != "" && TimeRangeAll_EP_TransTime.Time_end != "")
            {
                TimeRangeAll_EP_TransTime.FieldName = "轉帳時間";
                if (TimeRangeAll_EP_TransTime.valid != "")
                    sbError.Append(TimeRangeAll_EP_TransTime.valid);
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EP_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_UidNo":
                            hif_EP_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_ExamDate":
                            lbl_EG_ExamDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_SignDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_EP_SignDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_Training":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_EG_Training.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EP_Memo":
                            txt_EP_Memo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_TransNo":
                            {
                                hif_EG_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                selRegion_EG_TransNo.search(hif_EG_TransNo.Value);
                                selRegion_EG_TransNo.SelectedValue = hif_EG_TransNo.Value;
                                DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
                                if (dr != null)
                                {
                                    string[] EG_Region = dr["EG_Region"].ToString().Split(';');
                                    foreach (string item in EG_Region)
                                    {
                                        if (!string.IsNullOrEmpty(item))
                                        {
                                            string[] value = item.Split(',');
                                            if (value[0] == "Y")
                                                rbl_EP_Region.Items.Add(new ListItem(value[3], value[2]));
                                        }
                                    }
                                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr["EG_Subject"].ToString());
                                    if (dr_Phrase != null)
                                        lbl_EG_Subject.Text = dr_Phrase["TypeName"].ToString();
                                    dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                                    if (dr_Phrase != null)
                                    {
                                        lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                                    }
                                    lbl_EP_FeeType.Text = "證照測驗";
                                    if (dr["EG_ExamDate"].ToString() != "")
                                        lbl_EG_ExamDate.Text = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");

                                }
                                else
                                {
                                    rbl_EP_Region.Items.Clear();
                                    lbl_EG_Subject.Text = string.Empty;
                                    lbl_EG_Type.Text = string.Empty;
                                    lbl_EG_ExamDate.Text = string.Empty;
                                }
                            }
                            break;
                        case "EP_Region":
                            MainControls.ddlIndexSelectValue(rbl_EP_Region, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EP_AccID":
                            {
                                selPerson_AccID_EP_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                                selPerson_AccID_EP_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                hif_EP_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
                                if (dr_person != null)
                                    lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
                                else
                                    lbl_PS_Title.Text = string.Empty;
                                DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
                                if (dr_sales != null)
                                {
                                    DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                                    if (dr_Unit != null)
                                    {
                                        lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                                        lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                                    }
                                    DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                    if (dr_sales2 != null)
                                    {
                                        lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                                    }
                                }
                                else
                                {
                                    lbl_cZON_NAME.Text = string.Empty;
                                    lbl_cZON_TYPE.Text = string.Empty;
                                    lbl_DN_Name.Text = string.Empty;
                                }
                                DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
                                if (dr != null)
                                {
                                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                                    if (dr_Phrase != null)
                                    {
                                        DataRow dr_exam = DataExam.DataReader_recent_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, dr_Phrase["FieldName"].ToString());
                                        if(dr_exam != null)
                                        {
                                            lbl_EX_TransNo.Text = dr_exam["EC_CName"].ToString();
                                        }
                                        dr_exam = DataExam.DataReader_YearMaxScore_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, DateTime.Now.Year.ToString());
                                        if (dr_exam != null)
                                        {
                                            txt_EG_Training.Text = DateTime.Parse(dr_exam["EX_Date"].ToString()).ToString("yyyy/MM/dd");
                                        }
                                    }
                                }
                            }
                            break;
                        case "EP_Bank":
                            txt_EP_Bank.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_Account":
                            txt_EP_Account.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_TransAmt":
                            txt_EP_TransAmt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_TransDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_EP_TransDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EP_TransTimeS":
                            TimeRangeAll_EP_TransTime.Time_start = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_TransTimeE":
                            TimeRangeAll_EP_TransTime.Time_end = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EP_IsCash":
                            cbox_EP_IsCash.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            if (cbox_EP_IsCash.Checked)
                            {
                                txt_EP_Bank.Text = string.Empty;
                                txt_EP_Account.Text = string.Empty;
                                txt_EP_TransDate.Text = string.Empty;
                                TimeRangeAll_EP_TransTime.Init();
                            }
                            txt_EP_Bank.Enabled = !cbox_EP_IsCash.Checked;
                            txt_EP_Account.Enabled = !cbox_EP_IsCash.Checked;
                            txt_EP_TransDate.Enabled = !cbox_EP_IsCash.Checked;
                            TimeRangeAll_EP_TransTime.Enabled = !cbox_EP_IsCash.Checked;
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selRegion_EG_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
            if(dr != null)
            {
                string[] EG_Region = dr["EG_Region"].ToString().Split(';');
                foreach(string item in EG_Region)
                {
                    if(!string.IsNullOrEmpty(item))
                    {
                        string[] value = item.Split(',');
                        if (value[0] == "Y")
                            rbl_EP_Region.Items.Add(new ListItem(value[3], value[2]));
                    }
                }
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("138", dr["EG_Subject"].ToString());
                if (dr_Phrase != null)
                {
                    lbl_EG_Subject.Text = dr_Phrase["TypeName"].ToString();
                    DataRow dr_exam = DataExam.DataReader_recent_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, dr_Phrase["FieldName"].ToString());
                    if (dr_exam != null)
                    {
                        lbl_EX_TransNo.Text = dr_exam["EC_CName"].ToString();
                    }
                    dr_exam = DataExam.DataReader_YearMaxScore_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, DateTime.Now.Year.ToString());
                    if (dr_exam != null)
                    {
                        txt_EG_Training.Text = DateTime.Parse(dr_exam["EX_Date"].ToString()).ToString("yyyy/MM/dd");
                    }
                }
                dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                if (dr_Phrase != null)
                    lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                lbl_EP_FeeType.Text = "證照測驗";
                if (dr["EG_ExamDate"].ToString() != "")
                    lbl_EG_ExamDate.Text = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            }
            else
            {
                rbl_EP_Region.Items.Clear();
                lbl_EG_Subject.Text = string.Empty;
                lbl_EG_Type.Text = string.Empty;
                lbl_EG_ExamDate.Text = string.Empty;
            }
        }
        protected void selRegion_EG_TransNo_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selRegion_EG_TransNo_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selRegion_EG_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selRegion_EG_TransNo.search(hif_EG_TransNo.Value);
            selRegion_EG_TransNo.SelectedValue = hif_EG_TransNo.Value;
        }
        protected void selPerson_AccID_EP_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
            if (dr_person != null)
                lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
            else
                lbl_PS_Title.Text = string.Empty;
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                if (dr_Unit != null)
                {
                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                }
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                }
            }
            else
            {
                lbl_cZON_NAME.Text = string.Empty;
                lbl_cZON_TYPE.Text = string.Empty;
                lbl_DN_Name.Text = string.Empty;
            }
            DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
            if (dr != null)
            {
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                if (dr_Phrase != null)
                {
                    DataRow dr_exam = DataExam.DataReader_recent_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, dr_Phrase["FieldName"].ToString());
                    if (dr_exam != null)
                    {
                        lbl_EX_TransNo.Text = dr_exam["EC_CName"].ToString();
                    }
                    dr_exam = DataExam.DataReader_YearMaxScore_EC_EX(selPerson_AccID_EP_AccID.SelectedValue, DateTime.Now.Year.ToString());
                    if (dr_exam != null)
                    {
                        txt_EG_Training.Text = DateTime.Parse(dr_exam["EX_Date"].ToString()).ToString("yyyy/MM/dd");
                    }
                }
            }
        }
        protected void selPerson_AccID_EP_AccID_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_EP_AccID_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_EP_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EP_AccID.search(hif_EP_AccID.Value);
            selPerson_AccID_EP_AccID.SelectedValue = hif_EP_AccID.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            DataRow dr = DataEPayment.DataReader_EP_TransNo(hif_TransNo.Value);
                            if (dr != null)
                            {
                                DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EP_TransNo(dr["EG_TransNo"].ToString(), dr["EP_TransNo"].ToString());
                                if (dr_EI == null)
                                {
                                    DataSet ds_EI = new DataSet();

                                    ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI(dr["EP_TransNo"].ToString(), dr["EP_UidNo"].ToString(), dr["EP_Type"].ToString(), "A")));
                                    rsp = DataECircular.Append(sysValue.emNo, sysValue.ProgId, ds_EI);
                                }
                            }
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Notice": //通知
                    {
                        if (selPerson_AccID_EP_AccID.SelectedValue != "")
                        {
                            string mail_from = "", mail_from_name = "";
                            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_person != null)
                            {
                                if (dr_person["PS_EMAIL"].ToString() != "")
                                    mail_from = dr_person["PS_EMAIL"].ToString();
                                else
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                mail_from_name = dr_person["PS_NAME"].ToString();
                            }
                            else
                            {
                                mail_from = DataWebSet.dataReader("WebSendMail");
                                mail_from_name = DataWebSet.dataReader("WebTitle");

                            }
                            DataRow dr_person2 = DataPerson.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
                            if (dr_person2 != null)
                            {
                                string E7_Memo = "";
                                DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "6");
                                if (dr_E7 != null)
                                {
                                    E7_Memo = dr_E7["E7_Memo"].ToString();
                                }
                                if (dr_person2["PS_EMAIL"].ToString().Trim() != "")
                                {
                                    //Email通知
                                    RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", string.Format("{0}-未繳費通知", selRegion_EG_TransNo.SelectedText), E7_Memo, "", mail_from_name, false);
                                    if (!rsp_sendMail.Result)
                                    {
                                        MainControls.showMsg(this, string.Format("通知失敗！\\n{0}", "請確認您的EMAIL是否正確。"));
                                        logger.Error(string.Format("考試費用對帳登錄檢核-未繳費Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("考試費用對帳登錄檢核-未繳費Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    }
                                }
                                else
                                    MainControls.showMsg(this, "該人員EMAIL不存在！");
                            }
                        }
                        else
                            MainControls.showMsg(this, "請選擇人員！");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEPayment.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            DataRow dr = DataEPayment.DataReader_EP_TransNo(hif_TransNo.Value);
                            if (dr != null)
                            {
                                DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EP_TransNo(dr["EG_TransNo"].ToString(), dr["EP_TransNo"].ToString());
                                if (dr_EI != null)
                                {
                                    DataSet ds_EI = new DataSet();

                                    ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI_dlete(dr_EI["EI_TransNo"].ToString(), dr_EI["EI_UidNo"].ToString())));
                                    rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds_EI);
                                }
                            }
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Export": //匯出
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("考試項目", typeof(string));
                        dt.Columns.Add("考試日期", typeof(string));
                        dt.Columns.Add("報考人數", typeof(int));
                        dt.Columns.Add("現金繳交", typeof(int));
                        dt.Columns.Add("銀行轉帳", typeof(int));
                        dt.Columns.Add("合計金額", typeof(int));
                        dt.Columns.Add("備註", typeof(string));

                        DataTable dt_examPay_statistics = DataEPayment.DataReader_examPay_statistics(ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue).Tables[0];

                        foreach (DataRow dr in dt_examPay_statistics.Rows)
                        {
                            StringBuilder sb_memo = new StringBuilder();
                            DataTable dr_payment = DataEPayment.DataReader_EG_TransNo(dr["備註"].ToString()).Tables[0];
                            foreach (DataRow dr_p in dr_payment.Rows)
                            {
                                DataRow dr_person = DataPerson.DataReader_AccID(dr_p["EP_AccID"].ToString());
                                if (dr_person != null)
                                {
                                    if (dr_p["EP_Memo"].ToString() != "")
                                        sb_memo.AppendFormat("【{0}】{1}\n", dr_person["PS_NAME"].ToString(), dr_p["EP_Memo"].ToString());
                                }
                            }
                            dt.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], sb_memo.ToString());
                        }

                        DataTable dt_details = new DataTable();
                        dt_details.Columns.Add("費用類別", typeof(string));
                        dt_details.Columns.Add("考試項目", typeof(string));
                        dt_details.Columns.Add("考試日期", typeof(string));
                        dt_details.Columns.Add("業務員編號", typeof(string));
                        dt_details.Columns.Add("ID", typeof(string));
                        dt_details.Columns.Add("姓名", typeof(string));
                        dt_details.Columns.Add("現金繳交", typeof(int));
                        dt_details.Columns.Add("銀行轉帳", typeof(int));

                        DataTable dt_examPay_statistics_dt_details = DataEPayment.DataReader_examPay_statistics_details(ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue).Tables[0];

                        foreach (DataRow dr in dt_examPay_statistics_dt_details.Rows)
                        {
                            dt_details.Rows.Add(dr[0], dr[1], dr[2], dr[3], dr[4], dr[5], dr[6], dr[7]);
                        }
                        Dictionary<string, DataTable> list = new Dictionary<string, DataTable>();
                        list.Add(string.Format("考試費用對帳月統計表_{0}年{1}月", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue), dt);
                        list.Add(string.Format("考試費用對帳明細表_{0}年{1}月", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue), dt_details);
                        MemoryStream ms = DataTableRenderToExcel.RenderDataTableToExcel(list) as MemoryStream;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("考試費用對帳統計表_{0}年{1}月_{2}.xls", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EP_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EP_UidNo", hif_EP_UidNo.Value);
            }
            DataRow dr_region = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
            if (dr_region != null)
            {
                hsData.Add("EG_TransNo", dr_region["EG_TransNo"].ToString());
                hsData.Add("EG_UidNo", dr_region["EG_UidNo"].ToString());
                hsData.Add("EP_Type", dr_region["EG_Type"].ToString());
            }
            hsData.Add("EP_SignDate", txt_EP_SignDate.Text.Trim());
            hsData.Add("EG_Training", txt_EG_Training.Text.Trim());
            hsData.Add("EP_IsCash", cbox_EP_IsCash.Checked);
            hsData.Add("EP_Region", rbl_EP_Region.SelectedValue);
            hsData.Add("EP_FeeType", "3");
            hsData.Add("EP_Memo", txt_EP_Memo.Text.Trim());
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EP_AccID.SelectedValue);
            if (dr_person != null)
            {
                hsData.Add("EP_AccID", dr_person["AccID"].ToString());
                hsData.Add("EP_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EP_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("EP_TransAmt", txt_EP_TransAmt.Text.Trim());
            if (!cbox_EP_IsCash.Checked)
            {
                hsData.Add("EP_Bank", txt_EP_Bank.Text.Trim());
                hsData.Add("EP_Account", txt_EP_Account.Text.Trim());
               
                hsData.Add("EP_TransDate", txt_EP_TransDate.Text.Trim());
                hsData.Add("EP_TransTimeS", TimeRangeAll_EP_TransTime.Time_start);
                hsData.Add("EP_TransTimeE", TimeRangeAll_EP_TransTime.Time_end);
            }
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", TransNo);
            hsData.Add("EP_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EI(string EP_TransNo, string EP_UidNo, string EI_ExamType, string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("EI_UidNo", Session["UidNo"].ToString());
            hsData.Add("EI_Type", "2");
            hsData.Add("EP_TransNo", EP_TransNo);
            hsData.Add("EP_UidNo", EP_UidNo);
            hsData.Add("EI_ExamType", EI_ExamType);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EI_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", TransNo);
            hsData.Add("EI_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EP_UidNo.Value = string.Empty;
            selRegion_EG_TransNo.Clear();
            txt_EP_SignDate.Text = string.Empty;
            txt_EG_Training.Text = string.Empty;
            txt_EP_Memo.Text = string.Empty;
            cbox_EP_IsCash.Checked = false;
            rbl_EP_Region.Items.Clear();
            selPerson_AccID_EP_AccID.Clear();
            lbl_EG_Subject.Text = string.Empty;
            lbl_EG_Type.Text = string.Empty;
            lbl_EP_FeeType.Text = string.Empty;
            lbl_EG_ExamDate.Text = string.Empty;
            lbl_PS_Title.Text = string.Empty;
            lbl_cZON_TYPE.Text = string.Empty;
            lbl_cZON_NAME.Text = string.Empty;
            lbl_EX_TransNo.Text = string.Empty;
            lbl_DN_Name.Text = string.Empty;
            txt_EP_Bank.Text = string.Empty;
            txt_EP_Account.Text = string.Empty;
            txt_EP_TransAmt.Text = string.Empty;
            txt_EP_TransDate.Text = string.Empty;
            TimeRangeAll_EP_TransTime.Init();
            txt_EP_Bank.Enabled = true;
            txt_EP_Account.Enabled = true;
            txt_EP_TransAmt.Enabled = true;
            txt_EP_TransDate.Enabled = true;
            TimeRangeAll_EP_TransTime.Enabled = true;
            hif_EG_TransNo.Value = string.Empty;
            hif_EP_AccID.Value = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnNotice.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnNotice.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EP_Bank);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EP_Bank);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[6].Controls.Clear();
                CheckBox cbox_n = new CheckBox();
                cbox_n.ID = string.Format("cbox_n_{0}", e.Row.Cells[8].Text);
                cbox_n.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(cbox_n);

                e.Row.Cells[7].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[8].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[7].Controls.Add(cboxi);
                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[6].Controls.Clear();
                Button btn_n = new Button();
                btn_n.Click += checkNNotify_Click;
                btn_n.Text = "通知";
                btn_n.CssClass = "btnChat";
                btn_n.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn_n.OnClientClick = "return confirm('確定要通知已勾選資料嗎?');";
                btn_n.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(btn_n);

                e.Row.Cells[7].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[7].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次通知
        protected void checkNNotify_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            string mail_from = "", mail_from_name = "";
            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if (dr_person != null)
            {
                if (dr_person["PS_EMAIL"].ToString() != "")
                    mail_from = dr_person["PS_EMAIL"].ToString();
                else
                    mail_from = DataWebSet.dataReader("WebSendMail");
                mail_from_name = dr_person["PS_NAME"].ToString();
            }
            else
            {
                mail_from = DataWebSet.dataReader("WebSendMail");
                mail_from_name = DataWebSet.dataReader("WebTitle");

            }
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[6].Controls[0]).Checked)
                {

                    DataRow dr_person2 = DataPerson.DataReader_AccID(GridView1.Rows[i].Cells[24].Text);
                    if (dr_person2 != null)
                    {
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E004", "6");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        //Email通知
                        RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", string.Format("{0}-未繳費通知", selRegion_EG_TransNo.SelectedText), E7_Memo, "", mail_from_name, false);
                        if (!rsp_sendMail.Result)
                        {
                            count_faile += 1;
                            logger.Error(string.Format("考試費用對帳登錄檢核-未繳費Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                            logger_db.Error(string.Format("考試費用對帳登錄檢核-未繳費Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                        }
                        else //執行成功
                            count_sucess += 1;
                    }
                }
            }
            MainControls.showMsg(this, string.Format("已成功通知{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料通知失敗！", count_sucess) : ""));
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[7].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[8].Text, GridView1.Rows[i].Cells[9].Text)));

                    //RServiceProvider rsp = DataEPayment.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        count_sucess += 1;
                        DataRow dr = DataEPayment.DataReader_EP_TransNo(GridView1.Rows[i].Cells[9].Text);
                        if (dr != null)
                        {
                            DataRow dr_EI = DataECircular.DataReader_EG_TransNo_EP_TransNo(dr["EG_TransNo"].ToString(), dr["EP_TransNo"].ToString());
                            if (dr_EI != null)
                            {
                                DataSet ds_EI = new DataSet();

                                ds_EI.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_EI_dlete(dr_EI["EI_TransNo"].ToString(), dr_EI["EI_UidNo"].ToString())));
                                rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds_EI);
                            }
                        }
                    }
                    else
                        count_faile += 1; ;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EG_Name")) return;
            e.InputParameters.Add("EG_Name", Lib.FdVP(txtSh_EG_Name.Text).Trim());
            e.InputParameters.Add("PS_NAME", Lib.FdVP(txtSh_PS_NAME.Text).Trim());
            e.InputParameters.Add("EP_Bank", Lib.FdVP(txtSh_EP_Bank.Text).Trim());
            e.InputParameters.Add("EG_ExamDate_s", DateRangeSh_EG_ExamDate.Date_start);
            e.InputParameters.Add("EG_ExamDate_e", DateRangeSh_EG_ExamDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EG_Name.Text = string.Empty;
            txtSh_PS_NAME.Text = string.Empty;
            txtSh_EP_Bank.Text = string.Empty;
            DateRangeSh_EG_ExamDate.Init();

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void cbox_EP_IsCash_CheckedChanged(object sender, EventArgs e)
        {
            if (cbox_EP_IsCash.Checked)
            {
                txt_EP_Bank.Text = string.Empty;
                txt_EP_Account.Text = string.Empty;
                txt_EP_TransDate.Text = string.Empty;
                TimeRangeAll_EP_TransTime.Init();
            }
            txt_EP_Bank.Enabled = !cbox_EP_IsCash.Checked;
            txt_EP_Account.Enabled = !cbox_EP_IsCash.Checked;
            txt_EP_TransDate.Enabled = !cbox_EP_IsCash.Checked;
            TimeRangeAll_EP_TransTime.Enabled = !cbox_EP_IsCash.Checked;
        }
    }
}