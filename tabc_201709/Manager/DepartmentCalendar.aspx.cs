﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class DepartmentCalendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //資料繫結
                dataBind();

            }

        }

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EC_Unit"] = DataPhrase.DDL_TypeName(ddlSh_EC_Unit, true, "全部單位", "E008");
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
            if (dr_sales != null)
                MainControls.ddlIndexSelectAttribute(ddlSh_EC_Unit, "TypeSubCode", ViewState["ddlSh_EC_Unit"] as DataTable, dr_sales["Zone"].ToString());
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
            ddlSh_year.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
            ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
            ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
        }
        #endregion

        #region 資料繫結
        private void dataBind()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Day", typeof(string));
            dt.Columns.Add("Week", typeof(string));
            dt.Columns.Add("Memo", typeof(string));
            lbl_title.Text = string.Format("{0}－{1}年度{2}月份行事曆", ddlSh_EC_Unit.SelectedItem.Text, int.Parse(ddlSh_year.SelectedValue), ddlSh_Month.SelectedValue);
            string EC_Unit = "";
            if(ddlSh_EC_Unit.SelectedValue != "")
                EC_Unit = (ViewState["ddlSh_EC_Unit"] as DataTable).Rows[ddlSh_EC_Unit.SelectedIndex - 1]["TypeSubCode"].ToString();
            DataTable dt_course = DataECourse.DataReader_DepartmentCalendar(EC_Unit, ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue).Tables[0];
            ViewState["dt_course"] = dt_course;

            int days = DateTime.DaysInMonth(int.Parse(ddlSh_year.SelectedValue), int.Parse(ddlSh_Month.SelectedValue));
            for (int i = 1; i <= days; i++)
            {
                string DayName = GetDayName(DateTime.Parse(string.Format("{0}/{1}/{2}", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, i)));
                dt.Rows.Add(i.ToString(), DayName, "");
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        #endregion

        private string GetDayName(DateTime today)
        {
            string result = "";

            if (today.DayOfWeek == DayOfWeek.Monday)
            {
                result = "一";
            }
            else if (today.DayOfWeek == DayOfWeek.Tuesday)
            {
                result = "二";
            }
            else if (today.DayOfWeek == DayOfWeek.Wednesday)
            {
                result = "三";
            }
            else if (today.DayOfWeek == DayOfWeek.Thursday)
            {
                result = "四";
            }
            else if (today.DayOfWeek == DayOfWeek.Friday)
            {
                result = "五";
            }
            else if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                result = "六";
            }
            else if (today.DayOfWeek == DayOfWeek.Sunday)
            {
                result = "日";
            }

            return result;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#FFFBD6'");
                e.Row.Style.Add("border-bottom", "solid 1px #ffffff");
                if (e.Row.Cells[1].Text == "六" || e.Row.Cells[1].Text == "日")
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#e6e6fa");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#e6e6fa'");
                }
                else if (MainControls.ReplaceSpace(e.Row.Cells[5].Text.Trim()) != "")
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFC7DA");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFC7DA'");
                }
                else
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#A0FFFF");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#A0FFFF'");
                }
                if (ddlSh_year.SelectedValue == DateTime.Now.Year.ToString() &&
                    ddlSh_Month.SelectedValue == DateTime.Now.Month.ToString() &&
                    e.Row.Cells[0].Text == DateTime.Now.Day.ToString())
                {
                    e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFBD6");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor='#FFFBD6'");
                }
                DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                string EC_Unit = "";
                if (dr_sales != null)
                {
                    EC_Unit = dr_sales["Zone"].ToString();
                }
                DataTable dt_course = ViewState["dt_course"] as DataTable;
                string nowDate = string.Format("#{0}/{1}/{2} 00:00:00#", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, e.Row.Cells[0].Text);
                string nowDate_ = string.Format("#{0}/{1}/{2} 23:59:59#", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, e.Row.Cells[0].Text);
                DataRow[] dr_course_day = dt_course.Select(string.Format("EC_SDate>={0} and EC_SDate<={1} and EC_Unit='{2}' and EC_Mode='1'", nowDate, nowDate_, EC_Unit));
                DataTable dt_course_ = new DataTable();
                if (dr_course_day.Length != 0)
                    dt_course_ = dr_course_day.CopyToDataTable();
                ((Repeater)e.Row.Cells[2].Controls[1]).DataSource = dt_course_;
                ((Repeater)e.Row.Cells[2].Controls[1]).DataBind();
                DataRow[] dr_course_day1 = dt_course.Select(string.Format("EC_SDate>={0} and EC_SDate<={1} and EC_Unit<>'{2}' and EC_Unit<>'TB' and EC_Mode='1'", nowDate, nowDate_, EC_Unit));
                DataTable dt_course1 = new DataTable();
                if (dr_course_day1.Length != 0)
                    dt_course1 = dr_course_day1.CopyToDataTable();
                ((Repeater)e.Row.Cells[3].Controls[1]).DataSource = dt_course1;
                ((Repeater)e.Row.Cells[3].Controls[1]).DataBind();
                DataTable dt_course_TB = DataECourse.DataReader_DepartmentCalendar("", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue).Tables[0];
                DataRow[] dr_course_day2 = dt_course_TB.Select(string.Format("EC_SDate>={0} and EC_SDate<={1} and EC_Unit='TB' and EC_Mode in (1,3)", nowDate, nowDate_));
                DataTable dt_course2 = new DataTable();
                if (dr_course_day2.Length != 0)
                    dt_course2 = dr_course_day2.CopyToDataTable();
                ((Repeater)e.Row.Cells[4].Controls[1]).DataSource = dt_course2;
                ((Repeater)e.Row.Cells[4].Controls[1]).DataBind();

                StringBuilder sb_memo = new StringBuilder();
                foreach (DataRow dr in dt_course_.Rows)
                {
                    if (dr["EC_Memo"].ToString().Trim() != "")
                        sb_memo.AppendFormat("【{0}】{1}<br>", dr["EC_CName"].ToString(), dr["EC_Memo"].ToString());
                }
                foreach (DataRow dr in dt_course1.Rows)
                {
                    if (dr["EC_Memo"].ToString().Trim() != "")
                        sb_memo.AppendFormat("【{0}】{1}<br>", dr["EC_CName"].ToString(), dr["EC_Memo"].ToString());
                }
                foreach (DataRow dr in dt_course2.Rows)
                {
                    if (dr["EC_Memo"].ToString().Trim() != "")
                        sb_memo.AppendFormat("【{0}】{1}<br>", dr["EC_CName"].ToString(), dr["EC_Memo"].ToString());
                }
                e.Row.Cells[5].Text = sb_memo.ToString();
            }
        }

        protected void lbtn_Command(object sender, CommandEventArgs e)
        {
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(e.CommandName);
            if (dr_course != null)
            {
                lbl_EC_CName.Text = dr_course["EC_CName"].ToString();
                if (dr_course["EC_SDate"].ToString() != "")
                    lbl_EC_SDate.Text = string.Format("{0}  {1}~{2}", DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_course["EC_STime"].ToString(), dr_course["EC_ETime"].ToString());
                DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr_course["ET_TransNo"].ToString());
                if (dr_ET != null)
                    lbl_ET_TName.Text = dr_ET["ET_TName"].ToString();
                DataRow dr_EC_Moderator = DataPerson.DataReader_AccID(dr_course["EC_Moderator"].ToString());
                if (dr_EC_Moderator != null)
                    lbl_EC_Moderator.Text = dr_EC_Moderator["PS_NAME"].ToString();
                lbl_EC_Memo.Text = dr_course["EC_Memo"].ToString();
            }
            MultiView1.ActiveViewIndex = 1;
        }

        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            MultiView1.ActiveViewIndex = Convert.ToInt16(e.CommandName);
        }

        protected void btn_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "search":
                    dataBind();
                    break;
                case "reset":
                    DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                    if (dr_sales != null)
                        MainControls.ddlIndexSelectAttribute(ddlSh_EC_Unit, "TypeSubCode", ViewState["ddlSh_EC_Unit"] as DataTable, dr_sales["Zone"].ToString());
                    ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
                    ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
                    dataBind();
                    break;
            }
        }
    }
}