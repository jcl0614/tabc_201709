﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EQues : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);

            ListItemInit();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddl_EQ_Type);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddl_EQ_Type);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddl_EQ_Type);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);

                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();

            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEQues.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EQ_Type, true, "", "127");
            DataPhrase.DDL_TypeName(ddl_EQ_Type, true, "請選擇", "127");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_EQ_Type.SelectedValue == "")
                sbError.Append("●「題型」必須選取!<br>");
            if (selCourse_EC_TransNo.SelectedValue == "")
                sbError.Append("●「相關課程」必須選取!<br>");
            if (txt_EQ_Question.Text.Trim() == "")
                sbError.Append("●「題目」必須輸入!<br>");

            //if (txt_EQ_QCode.Text.Trim() == "")
            //    sbError.Append("●「測驗代碼」必須輸入!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EQ_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            Dictionary<int, string> text = new Dictionary<int, string>();
                            Dictionary<int, bool> isChecked = new Dictionary<int, bool>();
                            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, hif_TransNo.Value).Tables[0];
                            foreach (DataRow dr_ans in dt_ans.Rows)
                            {
                                text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                isChecked.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_IsCorrect"].ToString() == "Y");
                            }
                            ListItemBind(text, isChecked);
                            break;
                        case "EQ_UidNo":
                            hif_EQ_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_TransNo":
                            hif_EC_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
                            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
                            selCourse_EC_TransNo.SearchText = string.Empty;
                            break;
                        case "EQ_Type":
                            MainControls.ddlIndexSelectText(ddl_EQ_Type, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            ddl_EQ_Type.Enabled = false;
                            break;
                        case "EQ_Question":
                            txt_EQ_Question.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EQ_QCode":
                            txt_EQ_QCode.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selCourse_EC_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void selCourse_EC_TransNo_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void selCourse_EC_TransNo_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selCourse_EC_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証

            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;

                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQues.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEQues.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            updateEQans("A");
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");

                            
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQues.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEQues.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            updateEQans("M");
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";

                            
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        txt_EQ_QCode.Text = string.Empty;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EQ_UidNo.Value = string.Empty;
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請填上「測驗代碼」後進行新增儲存。");
                        ddl_EQ_Type.Enabled = true;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQues.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEQues.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEQues.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            updateEQans("D");
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();

                            
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        private void updateEQans(string ModiState)
        {
            DataTable dt_ListItem = new DataTable();
            DataTable dt_ans = new DataTable();
            dt_ListItem.Columns.Add("EA_TransNo", typeof(string));
            dt_ListItem.Columns.Add("EA_UidNo", typeof(string));
            dt_ListItem.Columns.Add("EA_OrderNo", typeof(string));
            dt_ListItem.Columns.Add("EA_ListItem", typeof(string));
            dt_ListItem.Columns.Add("EA_IsCorrect", typeof(string));
            switch (ddl_EQ_Type.SelectedValue)
            {
                case "1":
                    if (ModiState == "A")
                    {
                        for (int i = 0; i < 2; i++)
                        {
                            dt_ListItem.Rows.Add(
                                System.Guid.NewGuid().ToString("N"),
                                Session["UidNo"].ToString(),
                                (i + 1).ToString(),
                                ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type1", "text", i))).Text.Trim(),
                                ((RadioButton)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type1", "rb", i))).Checked ? "Y" : "N"
                                );
                        }
                    }
                    else
                    {
                        dt_ans = DataEQans.dataReader_EQ_TransNo(true, hif_TransNo.Value).Tables[0];
                        if (dt_ans.Rows.Count != 0)
                        {
                            foreach (DataRow dr_ans in dt_ans.Rows)
                            {
                                dt_ListItem.Rows.Add(
                                    dr_ans["EA_TransNo"].ToString(),
                                    dr_ans["EA_UidNo"].ToString(),
                                    (dt_ans.Rows.IndexOf(dr_ans) + 1).ToString(),
                                    ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type1", "text", dt_ans.Rows.IndexOf(dr_ans)))).Text.Trim(),
                                    ((RadioButton)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type1", "rb", dt_ans.Rows.IndexOf(dr_ans)))).Checked ? "Y" : "N"
                                    );
                            }
                        }
                    }
                    break;
                case "2":
                    if (ModiState == "A")
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            dt_ListItem.Rows.Add(
                                System.Guid.NewGuid().ToString("N"),
                                Session["UidNo"].ToString(),
                                (i + 1).ToString(),
                                ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type2", "text", i))).Text.Trim(),
                                ((RadioButton)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type2", "rb", i))).Checked ? "Y" : "N"
                                );
                        }
                    }
                    else
                    {
                        dt_ans = DataEQans.dataReader_EQ_TransNo(true, hif_TransNo.Value).Tables[0];
                        if (dt_ans.Rows.Count != 0)
                        {
                            foreach (DataRow dr_ans in dt_ans.Rows)
                            {
                                dt_ListItem.Rows.Add(
                                    dr_ans["EA_TransNo"].ToString(),
                                    dr_ans["EA_UidNo"].ToString(),
                                    (dt_ans.Rows.IndexOf(dr_ans) + 1).ToString(),
                                    ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type2", "text", dt_ans.Rows.IndexOf(dr_ans)))).Text.Trim(),
                                    ((RadioButton)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type2", "rb", dt_ans.Rows.IndexOf(dr_ans)))).Checked ? "Y" : "N"
                                    );
                            }
                        }
                    }
                    break;
                case "3":
                    if (ModiState == "A")
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            dt_ListItem.Rows.Add(
                                System.Guid.NewGuid().ToString("N"),
                                Session["UidNo"].ToString(),
                                (i + 1).ToString(),
                                ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type3", "text", i))).Text.Trim(),
                                ((CheckBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type3", "cb", i))).Checked ? "Y" : "N"
                                );
                        }
                    }
                    else
                    {
                        dt_ans = DataEQans.dataReader_EQ_TransNo(true, hif_TransNo.Value).Tables[0];
                        if (dt_ans.Rows.Count != 0)
                        {
                            foreach (DataRow dr_ans in dt_ans.Rows)
                            {
                                dt_ListItem.Rows.Add(
                                    dr_ans["EA_TransNo"].ToString(),
                                    dr_ans["EA_UidNo"].ToString(),
                                    (dt_ans.Rows.IndexOf(dr_ans) + 1).ToString(),
                                    ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type3", "text", dt_ans.Rows.IndexOf(dr_ans)))).Text.Trim(),
                                    ((CheckBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", "Type3", "cb", dt_ans.Rows.IndexOf(dr_ans)))).Checked ? "Y" : "N"
                                    );
                            }
                        }
                    }
                    break;
            }
            foreach (DataRow dr_item in dt_ListItem.Rows)
            {
                Hashtable hsData = new Hashtable();

                hsData.Add("EA_TransNo", dr_item["EA_TransNo"].ToString());
                hsData.Add("EA_UidNo", dr_item["EA_UidNo"].ToString());
                DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
                if (dr_course != null)
                {
                    hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                    hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                }
                hsData.Add("EQ_TransNo", hif_TransNo.Value);
                if (ModiState == "A")
                {
                    hsData.Add("EQ_UidNo", Session["UidNo"].ToString());
                }
                else
                {
                    hsData.Add("EQ_UidNo", hif_EQ_UidNo.Value);
                }
                hsData.Add("EA_OrderNo", dr_item["EA_OrderNo"].ToString());
                hsData.Add("EA_ListItem", dr_item["EA_ListItem"].ToString());
                hsData.Add("EA_IsCorrect", dr_item["EA_IsCorrect"].ToString());
                hsData.Add("AccID", Session["AccID"].ToString());
                hsData.Add("ComyCode", Session["ComyCode"].ToString());
                hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                hsData.Add("ModiState", ModiState);

                DataSet ds_ans = new DataSet();
                //元件資料轉成Hashtable
                ds_ans.Tables.Add(MainControls.UpLoadToDataTable(DataEQans.GetSchema(), hsData));
                SysValue sysValue = ViewState["SysValue"] as SysValue;
                RServiceProvider rsp_ans = new RServiceProvider();
                if (ModiState == "A")
                    rsp_ans = DataEQues.Append(sysValue.emNo, sysValue.ProgId, ds_ans);
                else
                    rsp_ans = DataEQues.Update(sysValue.emNo, sysValue.ProgId, ds_ans);
                if(rsp_ans.Result)
                {

                }
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EQ_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EQ_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EQ_UidNo", hif_EQ_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }
            hsData.Add("EQ_Type", ddl_EQ_Type.SelectedItem.Text.Trim());
            hsData.Add("EQ_Question", txt_EQ_Question.Text.Trim());
            hsData.Add("EQ_QCode", txt_EQ_QCode.Text.Trim());
            hsData.Add("EQ_Category", "1");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EQ_TransNo", TransNo);
            hsData.Add("EQ_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EA_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EA_TransNo", TransNo);
            hsData.Add("EA_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EQ_UidNo.Value = string.Empty;
            hif_EC_TransNo.Value = string.Empty;
            ddl_EQ_Type.SelectedIndex = -1;
            ddl_EQ_Type.Enabled = true;
            txt_EQ_Question.Text = string.Empty;
            txt_EQ_QCode.Text = string.Empty;
            selCourse_EC_TransNo.Clear();
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EQ_Type);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EQ_Type);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[5].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[6].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[5].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[5].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQues.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[6].Text, GridView1.Rows[i].Cells[7].Text)));

                    //RServiceProvider rsp = DataEQues.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEQues.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, GridView1.Rows[i].Cells[6].Text).Tables[0];
                        foreach (DataRow dr_ans in dt_ans.Rows)
                        {
                            DataSet ds_EA = new DataSet();
                            //元件資料轉成Hashtable
                            ds_EA.Tables.Add(MainControls.UpLoadToDataTable(DataEQans.GetSchema(), dataToHashtable_EA_dlete(dr_ans["EA_TransNo"].ToString(), dr_ans["EA_UidNo"].ToString())));
                            RServiceProvider rsp_EA = DataEQans.Update(sysValue.emNo, sysValue.ProgId, ds_EA);
                        }
                        count_sucess += 1;
                    }
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EQ_Type")) return;
            e.InputParameters.Add("EQ_Type", ddlSh_EQ_Type.SelectedItem.Text.Trim());
            e.InputParameters.Add("EQ_Question", Lib.FdVP(txtSh_EQ_Question.Text).Trim());
            e.InputParameters.Add("EA_ListItem", Lib.FdVP(txtSh_EA_ListItem.Text).Trim());
            e.InputParameters.Add("EQ_QCode", Lib.FdVP(txtSh_EQ_QCode.Text).Trim());
            e.InputParameters.Add("EQ_Category", "1");
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EQ_Question.Text = string.Empty;
            txtSh_EA_ListItem.Text = string.Empty;
            txtSh_EQ_QCode.Text = string.Empty;
            ddlSh_EQ_Type.SelectedIndex = -1;

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void ddl_EQ_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListItemBind();
        }

        private void ListItemInit()
        {
            
            Panel_EA_ListItem1.Controls.Clear();
            Dictionary<int, string> text_ = new Dictionary<int, string>();
            text_.Add(0, "是");
            text_.Add(1, "否");
            Dictionary<int, bool> isChecked_ = new Dictionary<int, bool>();
            isChecked_.Add(0, true);
            MainControls.CreateRadioButtonWithTextBox(Panel_EA_ListItem1, "Type1", "rb", "text", 2, 100, text_, isChecked_);

            Panel_EA_ListItem2.Controls.Clear();
            MainControls.CreateRadioButtonWithTextBox(Panel_EA_ListItem2, "Type2", "rb", "text", 4, 300);

            Panel_EA_ListItem3.Controls.Clear();
            MainControls.CreateCheckBoxWithTextBox(Panel_EA_ListItem3, "Type3", "cb", "text", 4, 300);
            

        }

        private void ListItemBind(Dictionary<int, string> text = null, Dictionary<int, bool> isChecked = null)
        {
            Panel_EA_ListItem1.Visible = false;
            Panel_EA_ListItem2.Visible = false;
            Panel_EA_ListItem3.Visible = false;
            string typeID = "";
            string correctID = "";
            switch (ddl_EQ_Type.SelectedValue)
            {
                case "1":
                    Panel_EA_ListItem1.Visible = true;
                    typeID = "Type1";
                    correctID = "rb";
                    break;
                case "2":
                    Panel_EA_ListItem2.Visible = true;
                    typeID = "Type2";
                    correctID = "rb";
                    break;
                case "3":
                    Panel_EA_ListItem3.Visible = true;
                    typeID = "Type3";
                    correctID = "cb";
                    break;
            }

            DataTable dt_ListItem = new DataTable();
            DataTable dt_ans = DataEQans.dataReader_EQ_TransNo(true, hif_TransNo.Value).Tables[0];
            if (dt_ans.Rows.Count != 0)
            {
                foreach (DataRow dr_ans in dt_ans.Rows)
                {
                    if (correctID == "rb")
                        ((RadioButton)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", typeID, correctID, dt_ans.Rows.IndexOf(dr_ans)))).Checked = dr_ans["EA_IsCorrect"].ToString() == "Y";
                    else
                        ((CheckBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", typeID, correctID, dt_ans.Rows.IndexOf(dr_ans)))).Checked = dr_ans["EA_IsCorrect"].ToString() == "Y";
                    ((TextBox)Panel_Form.FindControl(string.Format("{0}_{1}_{2}", typeID, "text", dt_ans.Rows.IndexOf(dr_ans)))).Text = dr_ans["EA_ListItem"].ToString();

                }
            }

            
        }

    }
}