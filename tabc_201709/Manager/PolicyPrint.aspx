﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PolicyPrint.aspx.cs" Inherits="tabc_201709.Manager.PolicyPrint" %>

<%@ Register src="ascx/UpdateProgress.ascx" tagname="UpdateProgress" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    
    <script src="/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
    <style>
        body{
            font-family: 微軟正黑體;
        }

        .tb_noneStyle .tb_query{
            font-family: 微軟正黑體;
            border: 0px solid #000;
        }

        .tb_noneStyle .tb_query tr, td {
            border: 0px solid #000;
        }
.btnPrint
{
    background: #f4f4f4 no-repeat 10px center;
    background-image: url("/manager/images/Print-icon.png");
    cursor:pointer;
    border: 1px solid #999999;
    font-size: 15px;
    font-family:微軟正黑體;
    padding: 3px 10px 3px 28px;
    margin: 0px 10px 0px 10px;
    color: #181818;
    border-radius: 4px;
}
.btnPrint:hover
{
    background: #eaeaea no-repeat 10px center;
    background-image: url("/manager/images/Print-icon.png");
}
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <script> 
        $(function () {

        });

        var isIE = function (ver) {
            var b = document.createElement('b')
            b.innerHTML = '<!--[if IE ' + ver + ']><i></i><![endif]-->'
            return b.getElementsByTagName('i').length === 1
        }
        function printScreen(html, title) {
            var printPage = window.open("", title, "");
            printPage.document.open();
            var ua = window.navigator.userAgent;
            if (ua.indexOf('.NET ') != -1 || ua.indexOf('MSIE ') != -1 || ua.indexOf('Explorer ') != -1 || ua.indexOf('Edge ') != -1) {
                printPage.document.write("<OBJECT classid='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2' height=0 id=wc name=wc width=0></OBJECT>");
                printPage.document.write("<HTML><head></head><BODY onload='javascript:wc.execwb(7,1);window.close()'>");
            }
            else {
                printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            }
            printPage.document.write("<PRE>");
            printPage.document.write(html);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="tb_noneStyle" style="border-width: 0px" width="100%">
                    <tr>
                        <td align="left">
                            <table class="tb_query Noprint" cellpadding="10" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbox_Cover" runat="server" Checked="True" Text="封面" />
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td rowspan="4">
                                        <asp:Button ID="btnPrint" runat="server" CssClass="btnPrint" OnClick="btnPrint_Click" Text="預覽列印" />
                                                <br />
                                                <br />
                                                <asp:Button ID="btnExport" runat="server" CssClass="btnPrint" OnClick="btnExport_Click" Text="匯出PDF" />
                                    </td>
                                    <td rowspan="4">
                                        <uc1:UpdateProgress ID="UpdateProgress1" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbox_ItemList" runat="server" Checked="True" Text="投保項目總導覽" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_dtPerMonthFee" runat="server" Checked="True" Text="每月保費" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_dtFee" runat="server" Checked="True" Text="每年度保費" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbox_dtBack" runat="server" Checked="True" Text="生存領回" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_dtProtectAnalsis" runat="server" Checked="True" Text="保障分析" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_dtProfit" runat="server" Checked="True" Text="綜合投保利益" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="cbox_tbHealthy" runat="server" Checked="True" Text="醫療明細" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_FamilyDetail" runat="server" Checked="True" Text="保障明細表" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="cbox_FamilyAll" runat="server" Checked="True" Text="綜合保障明細" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <img alt="" src="/manager/images/help.gif" />
                            <font style="color: #D663A5">預覽列印設定：瀏覽器請設定允許彈出式視窗(IE設定允許快顯封鎖)</font>
                            <br /> 
                            <img alt="" src="/manager/images/help.gif" />
                                <font style="color: #D663A5">列印設定：勾選[背景圖形]；邊界設定[無]或最小</font></td>
                    </tr>
                    <tr>
                        <td align="left">
                    <hr class="Noprint" />
                            <asp:Literal ID="Literal_content" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExport" />
            </Triggers>
        </asp:UpdatePanel>
        <div style="display:none;">
            <asp:Literal ID="Literal_error" runat="server"></asp:Literal>
        </div>
    </form>
</body>
</html>
