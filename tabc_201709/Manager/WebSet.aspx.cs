﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class WebSet : System.Web.UI.Page
    {
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.SetFocus(txt_WebTitle);
            Panel_Form.DefaultButton = btnUppend.ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //預設更新按鈕
                btnUppend.Enabled = true & (ViewState["SysValue"] as SysValue).Authority.Update;
                //讀取資料
                dataBind();
            }
        }

        #region 讀取資料
        private void dataBind()
        {
            txt_WebTitle.Text = DataWebSet.dataReader("WebTitle");
            txt_WebUrl.Text = DataWebSet.dataReader("WebUrl");
            txt_fbUrl.Text = DataWebSet.dataReader("fb_url");
            txt_twitterUrl.Text = DataWebSet.dataReader("twitter_url");
            txt_WebKeywords.Text = DataWebSet.dataReader("WebKeywords");
            txt_WebDescription.Text = DataWebSet.dataReader("WebDescription");
            txt_WebCopyright.Text = DataWebSet.dataReader("WebCopyright");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_WebTitle.Text.Trim() == "")
                sbError.Append("●「網站標題」必須輸入!<br>");
            else
                if (txt_WebTitle.Text.Trim().Length > 50)
                    sbError.Append("●「網站標題」最多輸入50個字元!<br>");
            if (txt_WebUrl.Text.Trim() == "")
                sbError.Append("●「網站網址」必須輸入!<br>");
            else
                if (!MainControls.ValidDataType("http(s)?://([\\w-]+\\.?)+[\\w-]+(/[\\w- ./?%&=:]*)?", txt_WebUrl.Text.Trim()))
                    sbError.Append("●「網站網址」格式錯誤!<br>");
            if (txt_fbUrl.Text.Trim() != "")
                if (!MainControls.ValidDataType("http(s)?://([\\w-]+\\.?)+[\\w-]+(/[\\w- ./?%&=:]*)?", txt_fbUrl.Text.Trim()))
                    sbError.Append("●「Facebook 網址」格式錯誤!<br>");
            if (txt_twitterUrl.Text.Trim() != "")
                if (!MainControls.ValidDataType("http(s)?://([\\w-]+\\.?)+[\\w-]+(/[\\w- ./?%&=:]*)?", txt_twitterUrl.Text.Trim()))
                    sbError.Append("●「Twitter 網址」格式錯誤!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 資料更新
        protected void btnUppend_Click(object sender, EventArgs e)
        {
            if (dataValid() != "")
            {
                labMsg.Text = dataValid();
                return;
            }
            else
            {
                string[] NameVale = new string[] { string.Format("{0},{1}", "WebTitle", Lib.FdVP(txt_WebTitle.Text.Trim())), 
                                           string.Format("{0},{1}", "WebUrl", Lib.FdVP(txt_WebUrl.Text.Trim(), "/&")), 
                                           string.Format("{0},{1}", "fb_url", Lib.FdVP(txt_fbUrl.Text.Trim(), "/%&")), 
                                           string.Format("{0},{1}", "twitter_url", Lib.FdVP(txt_twitterUrl.Text.Trim(), "/%&")), 
                                           string.Format("{0},{1}", "WebKeywords", Lib.FdVP(txt_WebKeywords.Text.Trim())), 
                                           string.Format("{0},{1}", "WebDescription", txt_WebDescription.Text.Trim()), 
                                           string.Format("{0},{1}", "WebCopyright", txt_WebCopyright.Text.Trim()) };
                RServiceProvider rsp = DataWebSet.updateWebSet(NameVale);
                if (rsp.Result)
                {
                    updateApplication();
                    labMsg.Text = "更新成功!";
                }
                else
                    labMsg.Text = rsp.ReturnMessage;
            }
        }
        #endregion

        #region 修正Application_Start
        private void updateApplication()
        {
            Application["WebTitle"] = Lib.FdVP(txt_WebTitle.Text.Trim());
            Application["WebUrl"] = Lib.FdVP(txt_WebUrl.Text.Trim(), "/&");
            Application["WebKeywords"] = Lib.FdVP(txt_WebKeywords.Text.Trim());
            Application["WebDescription"] = Lib.FdVP(txt_WebDescription.Text.Trim());
            Application["WebCopyright"] = Lib.FdVP(txt_WebCopyright.Text.Trim());
        }
        #endregion
    }
}