﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ECircular_PassUpload : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddlSh_EI_ExamType);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_EI_Pass);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_EI_Pass);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 900);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataECircular.DataColumn_pass(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EI_ExamType"] = DataPhrase.DDL_TypeName(ddlSh_EI_ExamType, true, "", "144");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selRegion_EG_TransNo.SelectedValue.Trim() == "")
                sbError.Append("●「考試名稱」必須選取!<br>");
            if (txt_EI_Pass.Text.Trim() == "")
                sbError.Append("●「入場證號」必須輸入!<br>");
            if (selPerson_AccID_EI_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「業務員」必須選取!<br>");
            if (FileUpload_EF1.FileName != "")
            {
                if (!MainControls.checkSubFileName(FileUpload_EF1.FileName.ToLower(), ".pdf"))
                    sbError.Append("●「准考證檔案」格式不符!<br>");
                if (txt_EFSize1.Text.Trim() == "")
                    sbError.Append("●「准考證檔案大小」必須輸入!<br>");
            }
            if (txt_EFSize1.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^[0-9]+(.[0-9]{1,2})?$", (txt_EFSize1.Text.Trim())))
                    sbError.Append("●「准考證檔案大小」格式錯誤!<br>");
            }


            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EI_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EI_UidNo":
                            hif_EI_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_TransNo":
                            {
                                hif_EG_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                selRegion_EG_TransNo.search(hif_EG_TransNo.Value);
                                selRegion_EG_TransNo.SelectedValue = hif_EG_TransNo.Value;
                                selRegion_EG_TransNo.SearchText = string.Empty;
                                DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
                                if (dr != null)
                                {
                                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                                    if (dr_Phrase != null)
                                    {
                                        lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                                    }
                                    if (dr["EG_ExamDate"].ToString() != "")
                                        lbl_EG_ExamDate.Text = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
                                }
                                else
                                {
                                    lbl_EG_Type.Text = string.Empty;
                                    lbl_EG_ExamDate.Text = string.Empty;
                                }
                            }
                            break;
                        case "EI_Region":
                            {
                                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E005", MainControls.ReplaceSpace(gvr.Cells[i].Text));
                                if (dr_Phrase != null)
                                {
                                    lbl_EI_Region.Text = dr_Phrase["TypeName"].ToString();
                                }
                            }
                            break;
                        case "EI_Pass":
                            txt_EI_Pass.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EI_AccID":
                            {
                                selPerson_AccID_EI_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                                selPerson_AccID_EI_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                hif_EI_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                                DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
                                if (dr_person != null)
                                    lbl_PS_Name.Text = dr_person["PS_Name"].ToString();
                                else
                                    lbl_PS_Name.Text = string.Empty;
                                DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
                                if (dr_sales != null)
                                {
                                    DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                                    if (dr_Unit != null)
                                    {
                                        lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                                        lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                                    }
                                }
                                else
                                {
                                    lbl_cZON_NAME.Text = string.Empty;
                                    lbl_cZON_TYPE.Text = string.Empty;
                                }
                            }
                            break;
                        case "EF_TransNo":
                            hif_EF_TransNo1.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_EF1 = DataEFile.DataReader_EF_TransNo(hif_EF_TransNo1.Value);
                            if (dr_EF1 != null)
                            {
                                hif_EF_UidNo1.Value = dr_EF1["EF_UidNo"].ToString();
                                txt_EF1.Text = dr_EF1["EF_Name"].ToString();
                                txt_EFSize1.Text = dr_EF1["EF_Size"].ToString();
                                Literal_EF1.Text = string.Format("<a href=\"{1}\" target=\"_blank\">{0}</a>", dr_EF1["EF_Name"].ToString(), dr_EF1["EF_Appendix"].ToString());
                            }
                            else
                                Literal_EF1.Text = "檔案不存在！";
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selRegion_EG_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
            if (dr != null)
            {
                DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr["EG_Type"].ToString());
                if (dr_Phrase != null)
                {
                    lbl_EG_Type.Text = dr_Phrase["TypeName"].ToString();
                }
                if (dr["EG_ExamDate"].ToString() != "")
                    lbl_EG_ExamDate.Text = DateTime.Parse(dr["EG_ExamDate"].ToString()).ToString("yyyy/MM/dd");
            }
            else
            {
                lbl_EG_Type.Text = string.Empty;
                lbl_EG_ExamDate.Text = string.Empty;
            }
        }
        protected void selRegion_EG_TransNo_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selRegion_EG_TransNo_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selRegion_EG_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selRegion_EG_TransNo.search(hif_EG_TransNo.Value);
            selRegion_EG_TransNo.SelectedValue = hif_EG_TransNo.Value;
        }
        protected void selPerson_AccID_EI_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_person != null)
                lbl_PS_Name.Text = dr_person["PS_Name"].ToString();
            else
                lbl_PS_Name.Text = string.Empty;
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                if (dr_Unit != null)
                {
                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                }

            }
            else
            {
                lbl_cZON_NAME.Text = string.Empty;
                lbl_cZON_TYPE.Text = string.Empty;
            }

        }
        protected void selPerson_AccID_EI_AccID_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EI_AccID_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EI_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EI_AccID.search(hif_EI_AccID.Value);
            selPerson_AccID_EI_AccID.SelectedValue = hif_EI_AccID.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_EF = new DataSet();
            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataECircular.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            ds_EF.Tables.Add(MainControls.UpLoadToDataTable(DataEFile.GetSchema(), dataToHashtable_EF1("A")));
                            RServiceProvider rsp_EF = DataEFile.Append(sysValue.emNo, sysValue.ProgId, ds_EF);
                            if (rsp_EF.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "新增成功...";
                                maintainButtonEnabled("Select");
                                //上傳圖檔
                                FileUpdate1();
                                return;
                            }
                            
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            ds_EF.Tables.Add(MainControls.UpLoadToDataTable(DataEFile.GetSchema(), dataToHashtable_EF1("M")));
                            RServiceProvider rsp_EF = new RServiceProvider();
                            if(FileUpload_EF1.FileName != "")
                                rsp_EF = DataEFile.Append(sysValue.emNo, sysValue.ProgId, ds_EF);
                            else
                                rsp_EF = DataEFile.Update(sysValue.emNo, sysValue.ProgId, ds_EF);
                            if (rsp_EF.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "修改成功...";
                                //上傳圖檔
                                FileUpdate1();
                                return;
                            }
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EI_UidNo.Value = string.Empty;
                        txt_EI_Pass.Text = string.Empty;
                        this.SetFocus(txt_EI_Pass);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataECircular.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataECircular.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            ds_EF.Tables.Add(MainControls.UpLoadToDataTable(DataEFile.GetSchema(), dataToHashtable_EF1("D")));
                            RServiceProvider rsp_EF = DataEFile.Append(sysValue.emNo, sysValue.ProgId, ds_EF);
                            if (rsp_EF.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "刪除成功...";
                                maintainButtonEnabled("");
                                clearControlContext();
                                return;
                            }
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EI_UidNo", Session["UidNo"].ToString());
                hif_EF_TransNo1.Value = System.Guid.NewGuid().ToString("N");
                hif_EF_UidNo1.Value = Session["UidNo"].ToString();
            }
            else if (ModiState == "M")
            {
                hsData.Add("EI_UidNo", hif_EI_UidNo.Value);
                if(FileUpload_EF1.FileName != "")
                {
                    hif_EF_TransNo1.Value = System.Guid.NewGuid().ToString("N");
                    hif_EF_UidNo1.Value = Session["UidNo"].ToString();
                }
            }
            else
            {
                hsData.Add("EI_UidNo", hif_EI_UidNo.Value);
            }
            hsData.Add("EF_TransNo", hif_EF_TransNo1.Value);
            hsData.Add("EF_UidNo", hif_EF_UidNo1.Value);
            DataRow dr_region = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
            if (dr_region != null)
            {
                hsData.Add("EG_TransNo", dr_region["EG_TransNo"].ToString());
                hsData.Add("EG_UidNo", dr_region["EG_UidNo"].ToString());
                hsData.Add("EI_ExamType", dr_region["EG_Type"].ToString());
                hsData.Add("EI_ExamDate", dr_region["EG_ExamDate"].ToString());
            }
            hsData.Add("EI_Type", "2");
            hsData.Add("EI_Pass", txt_EI_Pass.Text.Trim());
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_person != null)
            {
                hsData.Add("EI_AccID", dr_person["AccID"].ToString());
                hsData.Add("EI_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EI_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", TransNo);
            hsData.Add("EI_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EF1(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EF_TransNo", hif_EF_TransNo1.Value);
            if (ModiState == "A")
            {
                hsData.Add("EF_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EF_UidNo", hif_EF_UidNo1.Value);
            }
            hsData.Add("EF_Type", "3");
            if (FileUpload_EF1.FileName != "")
            {
                hsData.Add("EF_Name", FileUpload_EF1.FileName);
                hsData.Add("EF_Appendix", string.Format("/Uploads/ECircular/{0}", FileUpload_EF1.FileName));
            }
            else
            {
                hsData.Add("EF_Name", txt_EF1.Text);
                hsData.Add("EF_Appendix", string.Format("/Uploads/ECircular/{0}", txt_EF1.Text));
            }
            hsData.Add("EF_Size", txt_EFSize1.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EF_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EF_TransNo", TransNo);
            hsData.Add("EF_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EI_UidNo.Value = string.Empty;
            hif_EF_TransNo1.Value = string.Empty;
            hif_EF_UidNo1.Value = string.Empty;
            txt_EI_Pass.Text = string.Empty;
            selRegion_EG_TransNo.Clear();
            selPerson_AccID_EI_AccID.Clear();
            txt_EF1.Text = string.Empty;
            Literal_EF1.Text = string.Empty;
            lbl_Msg_EF1.Text = string.Empty;
            txt_EFSize1.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EI_Pass);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EI_Pass);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[6].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[7].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[6].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[6].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[7].Text, GridView1.Rows[i].Cells[8].Text)));

                    //RServiceProvider rsp = DataECircular.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        if (MainControls.ReplaceSpace(GridView1.Rows[i].Cells[11].Text) != "" && MainControls.ReplaceSpace(GridView1.Rows[i].Cells[12].Text) != "")
                        {
                            DataSet ds_EF1 = new DataSet();
                            //元件資料轉成Hashtable
                            ds_EF1.Tables.Add(MainControls.UpLoadToDataTable(DataEFile.GetSchema(), dataToHashtable_EF_dlete(GridView1.Rows[i].Cells[11].Text, GridView1.Rows[i].Cells[12].Text)));
                            RServiceProvider rsp_EF1 = DataEFile.Update(sysValue.emNo, sysValue.ProgId, ds_EF1);

                            count_sucess += 1;
                        }

                    }
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EI_ExamType")) return;
            if (ddlSh_EI_ExamType.SelectedIndex == 0)
                e.InputParameters.Add("EI_ExamType", "");
            else
                e.InputParameters.Add("EI_ExamType", (ViewState["ddlSh_EI_ExamType"] as DataTable).Rows[ddlSh_EI_ExamType.SelectedIndex - 1]["TypeSubCode"].ToString());
            e.InputParameters.Add("PS_NAME", Lib.FdVP(txtSh_PS_NAME.Text).Trim());
            e.InputParameters.Add("EI_ExamDate_s", DateRangeSh_EI_ExamDate.Date_start);
            e.InputParameters.Add("EI_ExamDate_e", DateRangeSh_EI_ExamDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EI_ExamType.SelectedIndex = -1;
            txtSh_PS_NAME.Text = string.Empty;
            DateRangeSh_EI_ExamDate.Init();
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        #region 上傳檔案處理
        private void FileUpdate1()
        {
            try
            {
                if (FileUpload_EF1.FileName != "")
                {
                    FileUpload_EF1.SaveAs(Server.MapPath(string.Format("~/uploads/ECircular/{0}", FileUpload_EF1.FileName)));
                    txt_EF1.Text = FileUpload_EF1.FileName;
                    Literal_EF1.Text = string.Format("<a href=\"/uploads/ECircular/{0}\" target=\"_blank\">{0}</a>", txt_EF1.Text);
                    lbl_Msg_EF1.Text = "上傳成功!";
                }
                else
                    lbl_Msg_EF1.Text = "";
            }
            catch (Exception ex)
            {
                lbl_Msg_EF1.Text = "上傳失敗!";
            }
        }

        #endregion


    }
}