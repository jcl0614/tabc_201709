﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class emDaily : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage10);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.SetFocus(txt_emNo_q);
            Panel_Form.DefaultButton = btnSearch.ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1350, true);
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataemDaily.emDailyDataColumn(this.GridView1);
        }
        #endregion

        #region 資料驗証
        private string dataVaild()
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_edDate_BEG_q.Text.Trim().Length > 0)
            {
                if (!MainControls.ValidDate(txt_edDate_BEG_q.Text.Trim()))
                    sbError.Append("●「維護日期起」輸入格式錯誤!\n");
            }
            if (txt_edDate_END_q.Text.Trim().Length > 0)
            {
                if (!MainControls.ValidDate(txt_edDate_END_q.Text.Trim()))
                    sbError.Append("●「維護日期訖」輸入格式錯誤!\n");
            }
            return sbError.ToString().Trim();
        }
        #endregion

        #region 設定下拉選單的內容
        private void setddlChoices()
        {

        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 控制 GridView 數字分頁
        private void GridViewPagers(Panel pnl, int PageCounts, int StartValue, int pageIndex)
        {
            pnl.Controls.Clear();
            Panel pnlH = new Panel();
            pnlH.Height = 5;
            pnl.Controls.Add(pnlH);

            LinkButton lbtnPre = new LinkButton();
            lbtnPre.ID = "lbtnN_pre";
            lbtnPre.Text = "上一頁";
            lbtnPre.CommandName = "Prev";
            lbtnPre.Command += lbtnPageCtrl_Command;
            pagerLbtnStyle(lbtnPre);
            if (pageIndex == 0)
            {
                lbtnPre.Visible = false;
            }

            pnl.Controls.Add(lbtnPre);

            Label lbl_ = new Label();
            lbl_.Text = "";
            lbl_.Width = 7;
            pnl.Controls.Add(lbl_);

            if (pageIndex < 6 && (PageCounts - 1 - pageIndex) < 6)
            {
                createLbtn(0, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex >= 6 && (PageCounts - 1 - pageIndex) >= 6)
            {
                createLbtn(0, 1, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(pageIndex - 3, pageIndex + 3, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(PageCounts - 2, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex >= 6 && (PageCounts - 1 - pageIndex) < 6)
            {
                createLbtn(0, 1, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(pageIndex - 3, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex < 6 && (PageCounts - 1 - pageIndex) >= 6)
            {
                createLbtn(0, pageIndex + 3, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(PageCounts - 2, PageCounts - 1, pageIndex, pnl);
            }

            LinkButton lbtnNext = new LinkButton();
            lbtnNext.ID = "lbtnN_Next";
            lbtnNext.Text = "下一頁";
            lbtnNext.CommandName = "Next";
            lbtnNext.Command += lbtnPageCtrl_Command;
            pagerLbtnStyle(lbtnNext);
            if (pageIndex == PageCounts - 1)
            {
                lbtnNext.Visible = false;
            }
            pnl.Controls.Add(lbtnNext);
        }
        private void createLbtn(int s, int e, int pageIndex, Panel pnl)
        {
            for (int ls = s; ls <= e; ls++)
            {
                LinkButton lbtn = new LinkButton();
                lbtn.ID = string.Format("lbtn_p{0}", ls.ToString());
                lbtn.Text = (ls + 1).ToString();
                pagerLbtnStyle(lbtn);
                if (ls == pageIndex)
                {
                    lbtn.Enabled = false;
                    lbtn.Font.Underline = false;
                    lbtn.ForeColor = System.Drawing.Color.FromName("#FF6699");
                }
                else
                {
                    lbtn.Font.Underline = true;
                    lbtn.Click += lbtnPagers_Click;
                }
                pnl.Controls.Add(lbtn);

                Label lbl = new Label();
                lbl.Text = "";
                lbl.Width = 7;
                pnl.Controls.Add(lbl);
            }
        }
        private void createLbl(Panel pnl)
        {
            Label lbl_piont = new Label();
            lbl_piont.Text = "…";
            lbl_piont.ForeColor = System.Drawing.Color.FromName("#333333");
            lbl_piont.Font.Size = 11;
            lbl_piont.Font.Name = "Arial";
            pnl.Controls.Add(lbl_piont);

            Label lbl = new Label();
            lbl.Text = "";
            lbl.Width = 7;
            pnl.Controls.Add(lbl);
        }
        private LinkButton pagerLbtnStyle(LinkButton lbtn)
        {
            //lbtn.Width = 20;
            //lbtn.BackColor = System.Drawing.Color.FromName("#C8DAEC");
            //lbtn.BorderColor = System.Drawing.Color.FromName("#CCCCCC");
            lbtn.ForeColor = System.Drawing.Color.FromName("#336699");
            //lbtn.BorderWidth = 1;
            lbtn.Font.Size = 11;
            //lbtn.Font.Bold = true;
            lbtn.Font.Name = "Arial";

            return lbtn;
        }
        protected void GridView_Load(object sender, EventArgs e)
        {
            GridViewPagers(pnlPagers, ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageCount, 1, ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex);
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ViewState["dataCount"] = count;
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        //下拉是分頁
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        //數字分頁
        protected void lbtnPagers_Click(object sender, EventArgs e)
        {
            ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = int.Parse(((LinkButton)sender).ID.Replace("lbtn_p", ""));
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = int.Parse(((LinkButton)sender).ID.Replace("lbtn_p", ""));
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("edDateBeg")) return;
            e.InputParameters.Add("edDateBeg", txt_edDate_BEG_q.Text.Trim());
            e.InputParameters.Add("edDateEnd", txt_edDate_END_q.Text.Trim());
            e.InputParameters.Add("emNo", txt_emNo_q.Text.Trim());
            e.InputParameters.Add("key", txt_key_q.Text.Trim());
            e.InputParameters.Add("his", chkHIS.Checked);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (dataVaild() != "")
            {
                MainControls.showMsg(this, dataVaild());
            }
            else
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
            }
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txt_key_q.Text = string.Empty;
            txt_edDate_BEG_q.Text = string.Empty;
            txt_edDate_END_q.Text = string.Empty;
            txt_emNo_q.Text = string.Empty;
            chkHIS.Checked = false;
            GridView1.Sort("edDate", SortDirection.Descending);
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        #region 結轉超過30天以上員工維護記錄至暫存
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            RServiceProvider rsp = DataemDaily.emDailyDelete();
            if (rsp.Result)
                MainControls.showMsg(this, "結轉成功!!!");
            else
                MainControls.showMsg(this, rsp.ReturnMessage);
            GridView1.DataBind();
        }
        #endregion

        #region 匯出EXCEL
        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            DataTable dt = DataemDaily.emDailyReader(txt_edDate_BEG_q.Text.Trim(), txt_edDate_END_q.Text.Trim(), txt_emNo_q.Text.Trim(), txt_key_q.Text.Trim(), chkHIS.Checked, "", 0, (int)ViewState["dataCount"]).Tables[0];
            if (dt.Rows.Count > 0)
                ExportControls.DataTableToExcelbyNPOI(dt, string.Format("資料異動紀錄_{0}", DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString()));
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }
        #endregion
    }
}