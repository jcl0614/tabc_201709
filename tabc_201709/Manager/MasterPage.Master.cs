﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //次網域轉換
            if (Request.Url.DnsSafeHost != "localhost" && Request.Url.DnsSafeHost != "127.0.0.1" && Request.Url.DnsSafeHost.IndexOf("192.168") == -1)
                if (DataWebSet.dataReader("WebUrl").IndexOf(Request.Url.DnsSafeHost) == -1)
                    Response.Redirect(string.Format("{0}{1}{2}", Request.Url.AbsoluteUri.IndexOf("https://") == -1 ? "http://" : "https://", DataWebSet.dataReader("WebUrl"), Request.Url.AbsolutePath));
            // http:// 及 https:// 轉換
            if (Request.Url.AbsoluteUri.IndexOf("http://192.168") == -1 && Request.Url.AbsoluteUri.IndexOf("https://192.168") == -1 && Request.Url.AbsoluteUri.IndexOf("http://localhost") == -1)
            {
                if (DataWebSet.dataReader("SSL") == "True" ? true : false)
                {
                    if (Request.Url.AbsoluteUri.IndexOf("https://") == -1)
                        Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
                }
                else
                {
                    if (Request.Url.AbsoluteUri.IndexOf("http://") == -1)
                        Response.Redirect(Request.Url.AbsoluteUri.Replace("https://", "http://"));
                }
            }
            //檔案名稱轉換
            if (Request.Url.AbsoluteUri.ToLower().IndexOf(".aspx") != -1)
                Response.Redirect(Request.Url.AbsoluteUri.Replace(".aspx", string.Empty));
            //頁面權限
            if (!DataMenu.LocationValid(Request.Url.AbsoluteUri, Session["emNo"].ToString()))
                Response.Redirect(" ");
            if (Request.Url.Segments[Request.Url.Segments.Count() - 1].Replace("/", string.Empty).ToLower() != "login")
            {
                bool PageLimitCheck = false;
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], this.Session).Authority.Select)
                    {
                        //系統參數
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], this.Session);
                        PageLimitCheck = true;
                        break;
                    }
                    else if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        //系統參數
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        PageLimitCheck = true;
                        break;
                    }
                }
                if (!PageLimitCheck)
                    Response.Redirect(" ");
            }
            else
                //系統參數
                ViewState["SysValue"] = new SysValue(Request.Url.Segments[Request.Url.Segments.Count() - 1].Replace("/", string.Empty), this.Session);
            if (!IsPostBack || Page.Title == "")
            {
                //頁面Title文字
                Page.Title = string.Format("{1}-{0}", (ViewState["SysValue"] as SysValue).osName, DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString());
            }
            if (new WebSetValue().WebCopyEnable)
            {
                pageBody.Attributes["oncontextmenu"] = "";
                pageBody.Attributes["onselectstart"] = "";
                pageBody.Attributes["oncopy"] = "";
            }
            else
            {
                pageBody.Attributes["oncontextmenu"] = "return false";
                pageBody.Attributes["onselectstart"] = "return false";
                pageBody.Attributes["oncopy"] = "return false";
            }
            onLineCount();

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("zh-tw");
        }

        #region 線上人數統計
        private void onLineCount()
        {
            //宣告一維陣列清單會依照增加項目的多少而動態調整大小。
            ArrayList arrltTemp = new ArrayList();
            //宣告字串變數。(用戶識別碼)
            string strUserID = null;
            //宣告字串變數。(取得進入頁面後系統給予的 Session 唯一識別碼)
            string strUserSessionID = Session.SessionID;
            //宣告整數變數。(人數計數器)
            int intNum = 0;
            //宣告整數變數。(迴圈計數器)
            int i = 0;
            //宣告整數變數。(距離多少時間(秒)的「用戶最後存取時間」才算是離開)
            int intCheckTickSeconds = 60;
            //新增此 Session 變數以使 Session 物件能正常運作。(建議要加)
            Session["Flag"] = true;
            //宣告日期物件變數。(取得目前時間)
            DateTime dtDateTime = DateTime.Now;

            //清空變數值。(測試用)
            //Application.Clear();

            //鎖定全域變數存取。
            Application.Lock();

            //@操作說明：清點所有連線到此網頁的用戶名單，並判斷是否為「新加入用戶」，
            //如果是就將目前的「新加入用戶」名稱放入「舊有存在用戶」陣列的最後一筆。

            //當目前進入用戶的「UserSessionID 和最後存取時間」的
            //Application 名稱全域變數為空值時。(代表該用戶為第一次進入)
            if (Application[strUserSessionID + "LastAccessTime_A"] == null)
            {
                //當 Application 「所有人數數量」為無值時，先設定預設值。(不然存取會有錯誤)
                if (Application["TotalUsers_A"] == null)
                {
                    //指派變數名稱值為 0。(所有線上用戶數量)
                    Application.Set("TotalUsers_A", 0);
                }

                //宣告一維陣列清單會依照增加項目的多少而動態調整大小。(取得 Application 的「線上用戶名單」陣列)
                ArrayList arrltOnlineUserC = (ArrayList)Application["OnlineUser_A"];
                //設定累計數值值為 0。
                intNum = 0;

                //如果「總人數計數」大於0時。(代表線上已經有人進入，不是第一次進來的狀況)
                if ((int)Application["TotalUsers_A"] > 0)
                {
                    //當陣列內容不是無值時。(線上用戶名單)
                    if (arrltOnlineUserC != null)
                    {
                        //讀取「線上用戶名單」陣列範圍，將「新加入用戶」與 
                        //「舊有存在用戶」陣列內容做比較看是否以經存在。

                        //利用「元素長度」取得一維陣列內容值。(線上用戶名單)
                        for (i = 0; i < arrltOnlineUserC.Count; i++)
                        {
                            //取出存放在「舊有存在用戶」陣列裡面的用戶名單。
                            strUserID = arrltOnlineUserC[i].ToString();
                            //當「舊有存在用戶」不等於「新加入用戶」陣列名單時。
                            if (strUserID != strUserSessionID.ToString())
                            {
                                //將「舊有存在用戶」放入陣列項目裡面。
                                arrltTemp.Add(strUserID);
                                //累加計數值。(統計人數)
                                intNum += 1;
                            }
                        }
                    }
                }

                //將「新加入的用戶」放入「舊有存在用戶」陣列項目裡面。(此陣列位置為最後一筆)
                arrltTemp.Add(strUserSessionID);
                //將「線上所有人數」累加 1。
                Application.Set("TotalUsers_A", intNum + 1);
                //將之前清查的陣列項目值「目前用戶名單」放入 Application「線上用戶名單」值裡面。
                Application.Set("OnlineUser_A", arrltTemp);
            }

            //指派目前進入用戶的「Session 個人變數 ID 和最後存取時間」Application 名稱全域變數值。(目前存取時間)
            Application.Set(strUserSessionID + "LastAccessTime_A", DateTime.Now.ToString());

            //== 檢查所有連線到此網頁之瀏覽器的最近存取時間，重新計算線上人數名單。
            //== 如果與目前時間相差30秒以上，表示結束連線。

            //使用動態陣列讀取。(取得 Application 的「線上用戶名單」陣列)
            ArrayList arrltOnlineUserT = (ArrayList)Application["OnlineUser_A"];
            //指派累加值為0。
            intNum = 0;

            //宣告日期物件操作案例。
            DateTime dtLastAccessTime = new DateTime();

            //讀取「線上用戶名單」陣列項目。
            for (i = 0; i < arrltOnlineUserT.Count; i++)
            {
                //當 Application 全域變數「用戶最後存取時間」不是無值時。
                if (Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"] != null)
                {
                    //取得 Application 全域變數的「用戶最後存取時間」。
                    dtLastAccessTime = DateTime.Parse(Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"].ToString());
                    //宣告刻度間隔物件操作案例。(取得「目前時間」與「用戶最後存取時間」相差距的秒數)
                    TimeSpan tsTicks = new TimeSpan(dtDateTime.Ticks - dtLastAccessTime.Ticks);
                    //當相差距的秒數小於 30 秒時。
                    if (Convert.ToInt32(tsTicks.TotalSeconds) < intCheckTickSeconds)
                    {
                        //顯示目前「線上用戶名單」與「用戶最後存取時間」與「目前時間」與「用戶最後存取時間」相差距的秒數。
                        //Response.Write("全域變數用戶：" + arrltOnlineUserT[i].ToString() + " 存取時間：" + Application[arrltOnlineUserT[i].ToString() + "LastAccessTime_A"] + " 相差: " + Convert.ToInt32(tsTicks.TotalSeconds).ToString() + " 秒");
                        //累加計數值。(統計人數)
                        intNum += 1;
                    }
                    else //當相差距的秒數大於 30 秒時。
                    {
                        //設定目前用戶的「用戶最後存取時間」Application 變數為無值。(清除 Application)
                        Application.Set(arrltOnlineUserT[i].ToString() + "LastAccessTime_A", null);
                        Application.Set(arrltOnlineUserT[i].ToString(), null);
                        Application.Remove(arrltOnlineUserT[i].ToString() + "LastAccessTime_A");
                        Application.Remove(arrltOnlineUserT[i].ToString());
                    }
                }
            }

            //當上面所清查完成的「目前線上人數」與 Application「線上所有人數」不同就表示中間有人斷線。
            if (intNum != (int)Application["TotalUsers_A"])
            {
                //將陣列項目值「線上用戶名單」放入 Application["OnlineUser_A"] 全域陣列值裡面。
                Application.Set("OnlineUser_A", arrltOnlineUserT);
                //將上面所清查的「線上人數」放入 Application["TotalUsers_A"] 全域變數裡面。
                Application.Set("TotalUsers_A", intNum);
            }

            //輸出結果。
            //Response.Write("目前線上人數：" + Application["TotalUsers_A"]);

            //不鎖定全域變數存取。
            Application.UnLock();

        }
        #endregion
    }
}