﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class Document : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("Document?id=11", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);
                if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                {
                    init(Request["id"]);
                    IintDataBind();
                }
            }
        }

        private void init(string id)
        {
            if (ConfigurationManager.AppSettings["TABCFilePoolPath"] != null)
            {
                string catrgory = "";
                switch (id)
                {
                    case "11":
                        catrgory = "11公文區";
                        break;
                    case "12":
                        catrgory = "12壽險公司資源";
                        break;
                    case "13":
                        catrgory = "13產險公司資源";
                        break;
                    case "14":
                        catrgory = "14台名壹周看";
                        break;
                    case "15":
                        catrgory = "15行政表單";
                        break;
                    case "16":
                        catrgory = "16管理規則";
                        break;

                }
                ViewState["catrgory"] = catrgory;
                string sMainPth = string.Format("{0}\\{1}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), catrgory);
                if (Directory.Exists(sMainPth))
                {
                    DirectoryInfo di = new DirectoryInfo(sMainPth);
                    DirectoryInfo[] diArr = di.GetDirectories();
                    ddlSh_category.Items.Add(new ListItem("查詢全部", ""));
                    foreach (DirectoryInfo dri in diArr)
                    {
                        ddlSh_category.Items.Add(new ListItem(dri.Name.Substring(2), dri.Name));
                    }
                }
                else
                    MainControls.showMsg(this, "路徑錯誤！");
            }
            else
            {
                MainControls.showMsg(this, "尚未設定來源路徑！");
            }
        }

        private void dataBind()
        {
            string sMainPth = string.Format("{0}\\{1}\\{2}\\{3}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString(), ddlSh_category.SelectedValue, ddlSh_category_sub.SelectedValue);
            DirectoryInfo di = new DirectoryInfo(sMainPth);
            FileInfo[] fi_array = di.GetFiles();
            DataTable dt = new DataTable("TABCFiles");
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("LastWriteTime", typeof(DateTime));
            dt.Columns.Add("LastWriteDate", typeof(string));
            dt.Columns.Add("Extension", typeof(string));
            dt.Columns.Add("Length", typeof(string));
            dt.Columns.Add("FullName", typeof(string));
            dt.Columns.Add("isNew", typeof(bool));
            foreach (FileInfo fi in fi_array)
            {
                string length = "";
                if (Math.Round((double)fi.Length / 1024) > 1024)
                    length = string.Format("{0} MB", Math.Round((double)fi.Length / 1024 / 1024, 2));
                else
                    length = string.Format("{0} kB", Math.Round((double)fi.Length / 1024, 0) == 0 ? 1 : Math.Round((double)fi.Length / 1024, 0));
                if (!fi.Name.Equals("Thumbs.db"))
                {
                    dt.Rows.Add(
                        fi.Name,
                        fi.LastWriteTime,
                        fi.LastWriteTime.ToString("yyyy/MM/dd"),
                        fi.Extension.Replace(".", "").ToUpper(),
                        length,
                        fi.FullName,
                        fi.LastWriteTime > DateTime.Today.AddDays(-20)
                        );
                }
            }
            DataView dvfiles = new DataView(dt);
            ViewState["dt_file"] = dt;
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        private void IintDataBind()
        {
            DataTable dt = new DataTable();

            string sMainPth = string.Format("{0}\\{1}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString());
            ArrayList fi_array = new ArrayList();
            DirectoryInfo di = new DirectoryInfo(sMainPth);
            GetFiles(di, ref fi_array);
            dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("LastWriteTime", typeof(DateTime));
            dt.Columns.Add("LastWriteDate", typeof(string));
            dt.Columns.Add("Extension", typeof(string));
            dt.Columns.Add("Length", typeof(string));
            dt.Columns.Add("FullName", typeof(string));
            dt.Columns.Add("isNew", typeof(bool));
            foreach (FileInfo fi in fi_array)
            {
                string length = "";
                if (Math.Round((double)fi.Length / 1024) > 1024)
                    length = string.Format("{0} MB", Math.Round((double)fi.Length / 1024 / 1024, 2));
                else
                    length = string.Format("{0} kB", Math.Round((double)fi.Length / 1024, 0) == 0 ? 1 : Math.Round((double)fi.Length / 1024, 0));
                if (!fi.Name.Equals("Thumbs.db"))
                {
                    dt.Rows.Add(
                        fi.Name,
                        fi.LastWriteTime,
                        fi.LastWriteTime.ToString("yyyy/MM/dd"),
                        fi.Extension.Replace(".", "").ToUpper(),
                        length,
                        fi.FullName,
                        fi.LastWriteTime > DateTime.Today.AddDays(-20)
                        );
                }
            }
            DataView dvfiles = dt.AsDataView();
            if (txtSh_Name.Text.Trim() != "")
                dvfiles = dt.AsEnumerable().Where(x => 
                x.Field<string>("Name").IndexOf(txtSh_Name.Text.Trim()) != -1 &&
                x.Field<DateTime>("LastWriteTime").Year == DateTime.Now.Year).AsDataView();
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        private void GetFiles(DirectoryInfo di, ref ArrayList Files)
        {
            //取得檔案
            foreach (FileInfo fi in di.GetFiles())
            {
                Files.Add(fi);
            }
            
            foreach (DirectoryInfo d in di.GetDirectories())
            {
                GetFiles(d, ref Files); //遞迴方法
            }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string fileUrlPath = ((ImageButton)sender).CommandName;
            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] file = null;

            try
            {
                file = wc.DownloadData(fileUrlPath);

                HttpContext.Current.Response.Clear();
                string fileName = System.IO.Path.GetFileName(fileUrlPath);
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.BinaryWrite(file);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                MainControls.showMsg(this, ex.Message);
            }


        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            if(ddlSh_category_sub.SelectedValue != "" && ddlSh_category_sub.SelectedValue != "")
                dt = ViewState["dt_file"] as DataTable;
            else
            {
                if (ViewState["dt_file_all"] == null)
                {
                    string sMainPth = string.Format("{0}\\{1}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString());
                    ArrayList fi_array = new ArrayList();
                    DirectoryInfo di = new DirectoryInfo(sMainPth);
                    GetFiles(di, ref fi_array);
                    dt = new DataTable();
                    dt.Columns.Add("Name", typeof(string));
                    dt.Columns.Add("LastWriteTime", typeof(DateTime));
                    dt.Columns.Add("LastWriteDate", typeof(string));
                    dt.Columns.Add("Extension", typeof(string));
                    dt.Columns.Add("Length", typeof(string));
                    dt.Columns.Add("FullName", typeof(string));
                    dt.Columns.Add("isNew", typeof(bool));
                    foreach (FileInfo fi in fi_array)
                    {
                        string length = "";
                        if (Math.Round((double)fi.Length / 1024) > 1024)
                            length = string.Format("{0} MB", Math.Round((double)fi.Length / 1024 / 1024, 2));
                        else
                            length = string.Format("{0} kB", Math.Round((double)fi.Length / 1024, 0) == 0 ? 1 : Math.Round((double)fi.Length / 1024, 0));
                        if (!fi.Name.Equals("Thumbs.db"))
                        {
                            dt.Rows.Add(
                                fi.Name,
                                fi.LastWriteTime,
                                fi.LastWriteTime.ToString("yyyy/MM/dd"),
                                fi.Extension.Replace(".", "").ToUpper(),
                                length,
                                fi.FullName,
                                fi.LastWriteTime > DateTime.Today.AddDays(-20)
                                );
                        }
                    }
                    ViewState["dt_file_all"] = dt;
                }
                else
                    dt = ViewState["dt_file_all"] as DataTable;
            }
            DataView dvfiles = dt.AsDataView();
            if (txtSh_Name.Text.Trim() != "")
                dvfiles = dt.AsEnumerable().Where(x => x.Field<string>("Name").IndexOf(txtSh_Name.Text.Trim()) != -1).AsDataView();
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            txtSh_Name.Text = string.Empty;
            if (ddlSh_category.Items.Count > 1)
                ddlSh_category.SelectedIndex = 1;

            DataTable dt = ViewState["dt_file"] as DataTable;
            DataView dvfiles = dt.AsDataView();
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        protected void ddlSh_category_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSh_category_sub.Items.Clear();
            if (ddlSh_category.SelectedValue != "")
            {
                string sMainPth = string.Format("{0}\\{1}\\{2}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString(), ddlSh_category.SelectedValue);
                DirectoryInfo di = new DirectoryInfo(sMainPth);
                DirectoryInfo[] diArr = di.GetDirectories();
                
                foreach (DirectoryInfo dri in diArr)
                {
                    ddlSh_category_sub.Items.Add(new ListItem(dri.Name.Substring(2), dri.Name));
                }
            }

            dataBind();
        }

        protected void ddlSh_category_sub_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataBind();
        }
    }
}