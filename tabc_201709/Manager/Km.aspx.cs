﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Data;

namespace tabc_201709.Manager
{
    public partial class Km : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.GetFriendlyUrlSegments() != null)
            {
                if (Request.GetFriendlyUrlSegments().Count > 0)
                {
                    if (Request.GetFriendlyUrlSegments()[Request.GetFriendlyUrlSegments().Count - 1] != "")
                    {
                        foreach (DataRow dr in DataMenu.menuReader_Km(Request.GetFriendlyUrlSegments()[Request.GetFriendlyUrlSegments().Count - 1]).Rows)
                        {
                            if (!DataMenu.PageLimitCheck(dr["muHyperLink"].ToString(), Session["emNo"].ToString()) || !new SysValue(dr["muHyperLink"].ToString(), this.Session).Authority.Select)
                                Response.Redirect(" ");
                        }

                        //系統參數
                        for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                        {
                            if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                            {
                                ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                                break;
                            }
                        }
                        //資料繫結
                        dataBind(Request.GetFriendlyUrlSegments()[Request.GetFriendlyUrlSegments().Count - 1]);
                    }
                }
            }
        }

        #region 資料繫結
        private void dataBind(string id)
        {
            DataTable dt = DataSysKm.id_DataReader(id);
            if (dt.Rows.Count != 0)
            {
                lbl_PageTitle.Text = dt.Rows[0]["name"].ToString();
                lbl_emNo.Text = string.Format("編修人員：{0}", dt.Rows[0]["emName"].ToString());
                lbl_updateDate.Text = string.Format("更新日期：{0}", dt.Rows[0]["UpdateDateStr"].ToString());
                Literal_description.Text = dt.Rows[0]["description"].ToString();
            }
        }
        #endregion
    }
}