﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ETeacherTot.aspx.cs" Inherits="tabc_201709.Manager.ETeacherTot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>
<%@ Register src="ascx/selCourse.ascx" tagname="selCourse" tagprefix="uc1" %>

<%@ Register src="ascx/selTeacher.ascx" tagname="selTeacher" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelSet" runat="server" CssClass="MainHeadTdLink" OnClick="lnkExcelSet_Click"><img src="<%=ResolveUrl("images/export.png") %>" border="0" style="vertical-align:bottom" /> 匯出</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse3" runat="server" CommandName="5" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/document-settings-icon.png") %>" border="0" style="vertical-align:bottom" /> 設定</asp:LinkButton>
            <br />
            <asp:LinkButton ID="lnkBrowse0" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 總表</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse1" runat="server" CommandName="3" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 明細表</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse2" runat="server" CommandName="4" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 加權計分表</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse3" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse0" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse1" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse2" />
            <asp:PostBackTrigger ControlID="lnkExcelSet" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">開課日期：</td>
                                            <td align="center" class="tdQueryData" colspan="3">
                                                <uc2:DateRange ID="DateRangeSh_EC_SDate" runat="server" />
                                            </td>
                                            <td align="center" width="85" rowspan="4">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="4">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">年度：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EH_Year" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">月份：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EH_Month" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">公司：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Company" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">部門：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_Department" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">講師編號：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_TCode" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">講師姓名：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_ET_TName" runat="server" MaxLength="30"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataETeacherTot"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">公司</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_ET_Company" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">部門</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_ET_Department" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">授課講師</td>
                                            <td class="tdData10s">
                                                <uc1:selTeacher ID="selTeacher_ET_TName" runat="server" OnRefreshButtonClick="selTeacher_ET_TName_RefreshButtonClick" OnSearchButtonClick="selTeacher_ET_TName_SearchButtonClick" OnSelectedIndexChanged="selTeacher_ET_TName_SelectedIndexChanged" OnTextChanged="selTeacher_ET_TName_TextChanged" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">統計年月</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EH_Year_Month" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">時數</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EH_Hours" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">點數</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EH_Points" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">應付費</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EH_ShouldPay" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">實付費</td>
                                            <td class="tdData10s">
                                                <asp:Label ID="lbl_EH_RealPay" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnExcute" runat="server" CommandName="Excute" CssClass="btnExcute" OnCommand="btn_Command" Text="計算" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2" style="height: 20px">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EH_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_ET_TransNo" runat="server" />
                                            </td>
                                        </tr>
                                        </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table id="print_all" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <asp:GridView ID="GridView2" runat="server" Font-Size="16px" CellPadding="7" ForeColor="#333333" GridLines="None">
                                        <AlternatingRowStyle BackColor="White" />
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#993300" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                        <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                        <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                        <SortedDescendingHeaderStyle BackColor="#820000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <button class="btnPrint" onclick="printScreen(print_all,'');">列印</button>
                                    <asp:Button ID="btnExport" runat="server" CommandName="Export_all" CssClass="btnExport" OnCommand="btn_Command" Text="匯出" />
                                    <asp:Button ID="Button2" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table id="print_datail" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <asp:GridView ID="GridView3" runat="server" Font-Size="14px" CellPadding="7" ForeColor="#333333" GridLines="None">
                                        <AlternatingRowStyle BackColor="White" />
                                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#993300" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#FFFBD6" ForeColor="#333333" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                        <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                        <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                        <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                        <SortedDescendingHeaderStyle BackColor="#820000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <button class="btnPrint" onclick="printScreen(print_datail,'');">
                                        列印
                                    </button>
                                    <asp:Button ID="btnExport0" runat="server" CommandName="Export_datail" CssClass="btnExport" OnCommand="btn_Command" Text="匯出" />
                                    <asp:Button ID="Button3" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <table id="print_score" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="center" width="100%">
                                    <asp:GridView ID="GridView4" runat="server" Font-Size="12px" BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px" CellPadding="4">
                                        <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                                        <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#993300" HorizontalAlign="Center" />
                                        <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                                        <RowStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Center" />
                                        <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                                        <SortedAscendingCellStyle BackColor="#FEFCEB" />
                                        <SortedAscendingHeaderStyle BackColor="#AF0101" />
                                        <SortedDescendingCellStyle BackColor="#F6F0C0" />
                                        <SortedDescendingHeaderStyle BackColor="#7E0000" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" class="Noprint" width="100%">
                            <tr>
                                <td align="center" class="tdMaintainButton">
                                    <button class="btnPrint" onclick="printScreen(print_score,'');">
                                        列印
                                    </button>
                                    <asp:Button ID="btnExport1" runat="server" CommandName="Export_score" CssClass="btnExport" OnCommand="btn_Command" Text="匯出" />
                                    <asp:Button ID="Button4" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <table border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="right" class="MS_tdTitle" width="150">年度</td>
                                <td class="tdData10s">
                                    <asp:DropDownList ID="ddlSh_year" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="150">月份</td>
                                <td class="tdData10s">
                                    <asp:DropDownList ID="ddlSh_Month" runat="server">
                                        <asp:ListItem Value="1">1 月</asp:ListItem>
                                        <asp:ListItem Value="2">2 月</asp:ListItem>
                                        <asp:ListItem Value="3">3 月</asp:ListItem>
                                        <asp:ListItem Value="4">4 月</asp:ListItem>
                                        <asp:ListItem Value="5">5 月</asp:ListItem>
                                        <asp:ListItem Value="6">6 月</asp:ListItem>
                                        <asp:ListItem Value="7">7 月</asp:ListItem>
                                        <asp:ListItem Value="8">8 月</asp:ListItem>
                                        <asp:ListItem Value="9">9 月</asp:ListItem>
                                        <asp:ListItem Value="10">10 月</asp:ListItem>
                                        <asp:ListItem Value="11">11 月</asp:ListItem>
                                        <asp:ListItem Value="12">12 月</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" class="MS_tdTitle" width="150">講師：</td>
                                <td class="tdData10s">
                                    <asp:UpdatePanel ID="UpdatePanel_Sort" runat="server">
                                        <ContentTemplate>
                                            <table align="center" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">
                                                        <br />
                                                    </td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333"></td>
                                                    <td align="center" style="font-family: 微軟正黑體; font-size: medium; color: #333333; padding-top: 10px; padding-bottom: 10px;">待設定人員</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxSur" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                                    </td>
                                                    <td align="center">
                                                        <p>
                                                            <asp:ImageButton ID="ibtnRefresh" runat="server" CommandName="refresh" ImageUrl="~/Manager/images/Refresh-icon.png" OnCommand="btnApply_Command" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="btnAddAll" runat="server" CommandName="addAll" CssClass="btnAddAll" OnCommand="btnApply_Command" Text="全部加入" />
                                                            <br />
                                                            <br />
                                                            <asp:Button ID="btnRemoveAll" runat="server" CommandName="removeAll" CssClass="btnRemoveAll" OnCommand="btnApply_Command" Text="全部移除" />
                                                            <br />
                                                            <br />
                                                            <asp:ImageButton ID="ibtnRight" runat="server" CommandName="add" ImageUrl="~/Manager/images/Forward-icon.png" OnCommand="btnApply_Command" />
                                                            <br />
                                                            <br />
                                                            <asp:ImageButton ID="ibtnLeft" runat="server" CommandName="remove" ImageUrl="~/Manager/images/Back-icon.png" OnCommand="btnApply_Command" />
                                                        </p>
                                                    </td>
                                                    <td align="center">
                                                        <asp:ListBox ID="lboxObj" runat="server" Height="500px" Rows="10" Width="300px"></asp:ListBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                    <td align="center"></td>
                                                    <td align="center" style="padding-top: 10px; padding-bottom: 10px" valign="top">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="tdMaintainButton" colspan="2">
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Append" CssClass="btnEdit" OnCommand="btnApply_Command" Text="更新" />
                                    <asp:Button ID="btnCancel0" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="height: 20px">
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" style="line-height: 125%">
                                                <asp:Label ID="lbl_Msg0" runat="server" CssClass="errmsg12"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExport" />
            <asp:PostBackTrigger ControlID="btnExport0" />
            <asp:PostBackTrigger ControlID="btnExport1" />
        </Triggers>
    </asp:UpdatePanel>
    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>

</asp:Content>
