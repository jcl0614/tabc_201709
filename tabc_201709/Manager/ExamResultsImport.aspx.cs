﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ExamResultsImport : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EG_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataERegion.DataColumn_ExamResultsImport(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EG_Type"] = DataPhrase.DDL_TypeName(ddlSh_EG_Type, true, "", "144");
            ViewState["ddlSh_EP_Type"] = DataPhrase.DDL_TypeName(ddlSh_EP_Type, false, "", "144");
            //DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
            //ViewState["ddlSh_EG_Region"] = DataPhrase.DDL_TypeName(ddlSh_EG_Region, true, "", "E005");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            
            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            switch (e.CommandName)
            {
                case "Member": //名單
                    {
                        DataRow dr_region = DataERegion.DataReader_EG_TransNo(e.CommandArgument.ToString());
                        if (dr_region != null)
                        {
                            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("144", dr_region["EG_Type"].ToString());
                            if (dr_Phrase != null)
                                lbl_EI_ExamTypeName.Text = string.Format("考試類別：{0}", dr_Phrase["TypeName"].ToString());
                            DataTable dt = DataECircular.DataReader_EG_TransNo(dr_region["EG_TransNo"].ToString()).Tables[0];
                            GridView2.DataSource = dt;
                            GridView2.DataBind();

                        }
                        ChangeMultiView(1);

                    }
                    break;
                case "Notify": //通知
                    {
                        DataRow dr_region = DataERegion.DataReader_EG_TransNo(e.CommandArgument.ToString());
                        if (dr_region != null)
                        {
                            string E7_Memo = "";
                            DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("", "");
                            if (dr_E7 != null)
                            {
                                E7_Memo = dr_E7["E7_Memo"].ToString();
                            }
                            string mail_from = "", mail_from_name = "";
                            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_person != null)
                            {
                                if (dr_person["PS_EMAIL"].ToString() != "")
                                    mail_from = dr_person["PS_EMAIL"].ToString();
                                else
                                    mail_from = DataWebSet.dataReader("WebSendMail");
                                mail_from_name = dr_person["PS_NAME"].ToString();
                            }
                            else
                            {
                                mail_from = DataWebSet.dataReader("WebSendMail");
                                mail_from_name = DataWebSet.dataReader("WebTitle");

                            }
                            int count_sucess = 0;
                            int count_faile = 0;
                            int update_sucess = 0;
                            int update_faile = 0;
                            DataTable dt = DataECircular.DataReader_EG_TransNo(dr_region["EG_TransNo"].ToString()).Tables[0];
                            foreach (DataRow dr in dt.Rows)
                            {
                                Dictionary<string, string> FieldData = new Dictionary<string, string>();
                                Dictionary<string, string> QueryData = new Dictionary<string, string>();
                                QueryData.Add("EI_TransNo", dr["EI_TransNo"].ToString());
                                //Email通知
                                RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr["PS_EMAIL"].ToString(), "", "", string.Format("{0}-考試成績通知", dr["EI_ExamTypeName"].ToString()), E7_Memo, "", mail_from_name, false);
                                if (!rsp_sendMail.Result)
                                {
                                    count_faile += 1;
                                    logger.Error(string.Format("考試成績-Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                    logger_db.Error(string.Format("考試成績通知-Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                }
                                else //執行成功
                                {
                                    FieldData.Add("EI_IsEmail", "1");
                                    count_sucess += 1;
                                }
                                switch (dr["EI_ExamType"].ToString())
                                {
                                    case "E011"://1人身保險業務員資格測驗
                                        {
                                            if (dr["EI_QualifiedSF"].ToString() != "")
                                            {
                                                FieldData.Add("EI_Progress", "1");
                                            }
                                        }
                                        break;
                                    case "E012"://2投資型保險商品測驗
                                        {
                                            if (dr["EI_QualifiedSF"].ToString() != "")
                                            {
                                                FieldData.Add("EI_Progress", "1");
                                            }
                                        }
                                        break;
                                    case "E013"://3外幣收付非型保險商品測驗
                                        {
                                            if (dr["EI_QualifiedSF"].ToString() != "")
                                            {
                                                FieldData.Add("EI_Progress", "1");
                                            }
                                        }
                                        break;
                                    case "E014"://4財產保險業務員考試
                                        {
                                            if (dr["EI_QualifiedS"].ToString() != "")
                                            {
                                                FieldData.Add("EI_Progress", "1");
                                            }
                                        }
                                        break;
                                }
                                if (FieldData.Count != 0)
                                {
                                    RServiceProvider rsp_update = DataECircular.updateField(FieldData, QueryData);
                                    if (rsp_update.Result)
                                    {
                                        update_sucess += 1;
                                    }
                                    else
                                    {
                                        update_faile += 1;
                                        logger.Error(string.Format("考試成績-更新失敗[EI_TransNo:{0}]：\r\n{1}", dr["EI_TransNo"].ToString(), rsp_sendMail.ReturnMessage));
                                        logger_db.Error(string.Format("考試成績通知-更新失敗[EI_TransNo:{0}]：\r\n{1}", dr["EI_TransNo"].ToString(), rsp_sendMail.ReturnMessage));
                                    }
                                }
                            }
                            MainControls.showMsg(this, string.Format("已成功通知{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料通知失敗！", count_sucess) : ""));
                            
                        }
                    }
                    break;
                case "Import": //匯入
                    {
                        DataTable dt = null;
                        DataRow dr_region = null;
                        try
                        {
                            StringBuilder sb_valid = new StringBuilder();
                            if (selRegion_EG_TransNo.SelectedValue == "")
                                sb_valid.AppendFormat("「考試名稱」必須選取！\\n");
                            else
                                dr_region = DataERegion.DataReader_EG_TransNo(selRegion_EG_TransNo.SelectedValue);
                            if (FileUpload1.FileName == "")
                                sb_valid.AppendFormat("「匯入檔案」必須選取！\\n");
                            if (sb_valid.Length != 0)
                            {
                                MainControls.showMsg(this, sb_valid.ToString());
                                return;
                            }
                            else
                            {
                                RServiceProvider rsp_import = ExportControls.ReadExcel(FileUpload1, 0);
                                if (rsp_import.Result)
                                {
                                    StringBuilder sb_import = new StringBuilder();
                                    dt = rsp_import.ReturnData as DataTable;

                                }
                                else
                                {
                                    MainControls.showMsg(this, "檔案匯入失敗！" + rsp_import.ReturnMessage);
                                    return;
                                }
                            }
                        }
                        catch (Exception es)
                        {
                            MainControls.showMsg(this, "檔案匯入失敗！");
                            return;
                        }
                        string E7_Memo = "";
                        DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("144", "");
                        if (dr_E7 != null)
                        {
                            E7_Memo = dr_E7["E7_Memo"].ToString();
                        }
                        string mail_from = "", mail_from_name = "";
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            if (dr_person["PS_EMAIL"].ToString() != "")
                                mail_from = dr_person["PS_EMAIL"].ToString();
                            else
                                mail_from = DataWebSet.dataReader("WebSendMail");
                            mail_from_name = dr_person["PS_NAME"].ToString();
                        }
                        else
                        {
                            mail_from = DataWebSet.dataReader("WebSendMail");
                            mail_from_name = DataWebSet.dataReader("WebTitle");

                        }
                        string EP_Type = "";
                        EP_Type = (ViewState["ddlSh_EP_Type"] as DataTable).Rows[ddlSh_EP_Type.SelectedIndex]["TypeSubCode"].ToString();
                        switch (EP_Type)
                        {
                            case "E011"://1人身保險業務員資格測驗
                                {
                                    if (FileUpload1.FileName == "09壽險測驗成績--exam_signup.xls")
                                    {
                                        StringBuilder sb_import = new StringBuilder();
                                        StringBuilder sb_html = new StringBuilder();
                                        sb_html.Append("<table>");
                                        sb_html.Append("<tr>");
                                        foreach (DataColumn dc in dt.Columns)
                                            sb_html.AppendFormat("<td>{0}</td>", dc.ColumnName);
                                        sb_html.Append("</tr>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region != null && dr_region["EG_Year"].ToString() == dr[0].ToString() && DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[5].ToString(), "E011"))
                                            {
                                                sb_html.Append("<tr>");
                                            foreach (DataColumn dc in dt.Columns)
                                                sb_html.AppendFormat("<td>{0}</td>", dr[dt.Columns.IndexOf(dc)].ToString());
                                                sb_html.Append("</tr>");
                                            }
                                        }
                                        sb_html.Append("</table>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region == null || dr_region["EG_Year"].ToString() != dr[0].ToString() || !DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[5].ToString(), "E011"))
                                            {
                                                sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：{1}</span><br>", dt.Rows.IndexOf(dr) + 1, "資料比對不符！");
                                            }
                                            else
                                            {
                                                Dictionary<string, string> FieldData = new Dictionary<string, string>();
                                                FieldData.Add("EI_Pass", dr[2].ToString());
                                                FieldData.Add("EI_Name", dr[3].ToString());
                                                FieldData.Add("EI_Sex", dr[4].ToString() == "1" ? "男" : "女");
                                                FieldData.Add("EI_ID", dr[5].ToString());
                                                if (dr[6].ToString().Length == 8)
                                                    FieldData.Add("EI_Birth", string.Format("{0}/{1}/{2}", int.Parse(dr[6].ToString().Substring(0, 4)) + 1911, dr[6].ToString().Substring(4, 2), dr[6].ToString().Substring(6, 2)));
                                                FieldData.Add("EI_ScoreF", dr[7].ToString());
                                                FieldData.Add("EI_ScoreFMemo", dr[8].ToString());
                                                FieldData.Add("EI_QualifiedSF", dr[9].ToString());
                                                FieldData.Add("EI_Score1", dr[10].ToString());
                                                FieldData.Add("EI_Score1Memo", dr[11].ToString());
                                                FieldData.Add("EI_Score2", dr[12].ToString());
                                                FieldData.Add("EI_Score2Memo", dr[13].ToString());
                                                FieldData.Add("EI_QualifiedS", dr[14].ToString());
                                                Dictionary<string, string> QueryData = new Dictionary<string, string>();
                                                QueryData.Add("EG_TransNo", selRegion_EG_TransNo.SelectedValue);
                                                QueryData.Add("EI_ID", dr[5].ToString());
                                                QueryData.Add("EI_ExamType", "E011");

                                                RServiceProvider rsp = DataECircular.updateField(FieldData, QueryData);
                                                if (rsp.Result) //執行成功
                                                {
                                                    sb_import.AppendFormat("<span style=\"color:#009933;\">第{0}筆：匯入成功！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                                }
                                                else
                                                    sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：匯入失敗！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                            }

                                        }
                                        GridView1.DataBind();
                                        lbl_Msg0.Text = sb_import.ToString();
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, string.Format("匯入檔名不符！\\n必須是：{0}", "09壽險測驗成績--exam_signup.xls"));
                                        return;
                                    }
                                }
                                break;
                            case "E012"://2投資型保險商品測驗
                                {
                                    if (FileUpload1.FileName == "10投資型測驗成績--exam_signup.xls")
                                    {
                                        StringBuilder sb_import = new StringBuilder();
                                        StringBuilder sb_html = new StringBuilder();
                                        sb_html.Append("<table>");
                                        sb_html.Append("<tr>");
                                        foreach (DataColumn dc in dt.Columns)
                                            sb_html.AppendFormat("<td>{0}</td>", dc.ColumnName);
                                        sb_html.Append("</tr>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region != null && DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[2].ToString(), "E012"))
                                            {
                                                sb_html.Append("<tr>");
                                            foreach (DataColumn dc in dt.Columns)
                                                sb_html.AppendFormat("<td>{0}</td>", dr[dt.Columns.IndexOf(dc)].ToString());
                                                sb_html.Append("</tr>");
                                            }
                                        }
                                        sb_html.Append("</table>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region == null || !DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[2].ToString(), "E012"))
                                            {
                                                sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：{1}</span><br>", dt.Rows.IndexOf(dr) + 1, "資料比對不符！");
                                            }
                                            else
                                            {
                                                Dictionary<string, string> FieldData = new Dictionary<string, string>();
                                                FieldData.Add("EI_Subject", dr[0].ToString());
                                                FieldData.Add("EI_Region", dr[1].ToString());
                                                FieldData.Add("EI_ID", dr[2].ToString());
                                                FieldData.Add("EI_Name", dr[3].ToString());
                                                FieldData.Add("EI_Pass", dr[4].ToString());
                                                FieldData.Add("EI_Score1", dr[5].ToString());
                                                FieldData.Add("EI_Score2", dr[6].ToString());
                                                FieldData.Add("EI_QualifiedSF", dr[7].ToString());
                                                Dictionary<string, string> QueryData = new Dictionary<string, string>();
                                                QueryData.Add("EG_TransNo", selRegion_EG_TransNo.SelectedValue);
                                                QueryData.Add("EI_ID", dr[5].ToString());
                                                QueryData.Add("EI_ExamType", "E012");


                                                RServiceProvider rsp = DataECircular.updateField(FieldData, QueryData);
                                                if (rsp.Result) //執行成功
                                                {
                                                    sb_import.AppendFormat("<span style=\"color:#009933;\">第{0}筆：匯入成功！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                                }
                                                else
                                                    sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：匯入失敗！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                            }

                                        }
                                        GridView1.DataBind();
                                        lbl_Msg0.Text = sb_import.ToString();
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, string.Format("匯入檔名不符！\\n必須是：{0}", "10投資型測驗成績--exam_signup.xls"));
                                        return;
                                    }
                                }
                                break;
                            case "E013"://3外幣收付非型保險商品測驗
                                {
                                    if (FileUpload1.FileName == "11外幣測驗成績--exam_signup.xls")
                                    {
                                        StringBuilder sb_import = new StringBuilder();
                                        StringBuilder sb_html = new StringBuilder();
                                        sb_html.Append("<table>");
                                        sb_html.Append("<tr>");
                                        foreach (DataColumn dc in dt.Columns)
                                            sb_html.AppendFormat("<td>{0}</td>", dc.ColumnName);
                                        sb_html.Append("</tr>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region != null && dr_region["EG_Year"].ToString() == dr[0].ToString() && DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[5].ToString(), "E013"))
                                            {
                                                sb_html.Append("<tr>");
                                                foreach (DataColumn dc in dt.Columns)
                                                    sb_html.AppendFormat("<td>{0}</td>", dr[dt.Columns.IndexOf(dc)].ToString());
                                                sb_html.Append("</tr>");
                                            }
                                        }
                                        sb_html.Append("</table>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region == null || dr_region["EG_Year"].ToString() != dr[0].ToString() || !DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[5].ToString(), "E013"))
                                            {
                                                sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：{1}</span><br>", dt.Rows.IndexOf(dr) + 1, "資料比對不符！");
                                            }
                                            else
                                            {
                                                Dictionary<string, string> FieldData = new Dictionary<string, string>();
                                                FieldData.Add("EI_Pass", dr[2].ToString());
                                                FieldData.Add("EI_Name", dr[3].ToString());
                                                FieldData.Add("EI_Sex", dr[4].ToString() == "1" ? "男" : "女");
                                                FieldData.Add("EI_ID", dr[5].ToString());
                                                if (dr[6].ToString().Length == 8)
                                                    FieldData.Add("EI_Birth", string.Format("{0}/{1}/{2}", int.Parse(dr[6].ToString().Substring(0, 4)) + 1911, dr[6].ToString().Substring(4, 2), dr[6].ToString().Substring(6, 2)));
                                                FieldData.Add("EI_ScoreF", dr[7].ToString());
                                                FieldData.Add("EI_QualifiedSF", dr[8].ToString());
                                                Dictionary<string, string> QueryData = new Dictionary<string, string>();
                                                QueryData.Add("EG_TransNo", selRegion_EG_TransNo.SelectedValue);
                                                QueryData.Add("EI_ID", dr[5].ToString());
                                                QueryData.Add("EI_ExamType", "E013");


                                                RServiceProvider rsp = DataECircular.updateField(FieldData, QueryData);
                                                if (rsp.Result) //執行成功
                                                {
                                                    sb_import.AppendFormat("<span style=\"color:#009933;\">第{0}筆：匯入成功！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                                }
                                                else
                                                    sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：匯入失敗！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                            }

                                        }
                                        GridView1.DataBind();
                                        lbl_Msg0.Text = sb_import.ToString();
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, string.Format("匯入檔名不符！\\n必須是：{0}", "11外幣測驗成績--exam_signup.xls"));
                                        return;
                                    }
                                }
                                break;
                            case "E014"://4財產保險業務員考試
                                {
                                    if (FileUpload1.FileName == "12產險測驗成績--exam_signup.xls")
                                    {
                                        StringBuilder sb_import = new StringBuilder();
                                        StringBuilder sb_html = new StringBuilder();
                                        sb_html.Append("<table>");
                                        sb_html.Append("<tr>");
                                        foreach (DataColumn dc in dt.Columns)
                                            sb_html.AppendFormat("<td>{0}</td>", dc.ColumnName);
                                        sb_html.Append("</tr>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region != null && DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[3].ToString(), "E014"))
                                            {
                                                sb_html.Append("<tr>");
                                                foreach (DataColumn dc in dt.Columns)
                                                    sb_html.AppendFormat("<td>{0}</td>", dr[dt.Columns.IndexOf(dc)].ToString());
                                                sb_html.Append("</tr>");
                                            }
                                        }
                                        sb_html.Append("</table>");
                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            if (dr_region == null || !DataECircular.valid_import(selRegion_EG_TransNo.SelectedValue, dr[3].ToString(), "E014"))
                                            {
                                                sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：{1}</span><br>", dt.Rows.IndexOf(dr) + 1, "資料比對不符！");
                                            }
                                            else
                                            {
                                                Dictionary<string, string> FieldData = new Dictionary<string, string>();
                                                FieldData.Add("EI_ExamDate", dr[0].ToString());
                                                FieldData.Add("EI_Region", dr[2].ToString());
                                                FieldData.Add("EI_ID", dr[3].ToString());
                                                FieldData.Add("EI_Name", dr[4].ToString());
                                                FieldData.Add("EI_Pass", dr[5].ToString());
                                                FieldData.Add("EI_ScoreF", dr[6].ToString());
                                                FieldData.Add("EI_ScoreFMemo", dr[7].ToString());
                                                FieldData.Add("EI_QualifiedSF", dr[8].ToString());
                                                FieldData.Add("EI_Score1", dr[9].ToString());
                                                FieldData.Add("EI_Score2", dr[10].ToString());
                                                FieldData.Add("EI_Score1Memo", dr[11].ToString());
                                                FieldData.Add("EI_QualifiedS", dr[12].ToString());
                                                FieldData.Add("EI_Score3", dr[13].ToString());
                                                FieldData.Add("EI_Score4", dr[14].ToString());
                                                FieldData.Add("EI_Score3Memo", dr[15].ToString());
                                                FieldData.Add("EI_QualifiedSC", dr[16].ToString());
                                                Dictionary<string, string> QueryData = new Dictionary<string, string>();
                                                QueryData.Add("EG_TransNo", selRegion_EG_TransNo.SelectedValue);
                                                QueryData.Add("EI_ID", dr[5].ToString());
                                                QueryData.Add("EI_ExamType", "E014");


                                                RServiceProvider rsp = DataECircular.updateField(FieldData, QueryData);
                                                if (rsp.Result) //執行成功
                                                {
                                                    sb_import.AppendFormat("<span style=\"color:#009933;\">第{0}筆：匯入成功！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                                }
                                                else
                                                    sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：匯入失敗！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                            }

                                        }
                                        GridView1.DataBind();
                                        lbl_Msg0.Text = sb_import.ToString();
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, string.Format("匯入檔名不符！\\n必須是：{0}", "12產險測驗成績--exam_signup.xls"));
                                        return;
                                    }
                                }
                                break;
                        }
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();



            return hsData;
        }

        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {

        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
                case 2:
                    lnkBrowse.Visible = true;
                    lbl_Msg0.Text = string.Empty;
                    selRegion_EG_TransNo.Clear();
                    maintainButtonEnabled("Import");
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnImport.Visible = false;
            switch (commandName)
            {
                case "Import": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnImport.Visible = true & sysValue.Authority.Update;
                    break;
                default:
                    //TODO:程式非共用修改-10

                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                //{
                //    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                //    {
                //        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                //        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                //        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                //    }
                //}

                //switch (Convert.ToInt16(ViewState["RowNo"]))
                //{
                //    case 0:
                //        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                //        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                //        ViewState["RowNo"] = 1;
                //        break;
                //    case 1:
                //        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                //        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                //        ViewState["RowNo"] = 0;
                //        break;
                //}

                #region cbox,sendbtn
                e.Row.Cells[4].Controls.Clear();
                LinkButton lbtn_mamber = new LinkButton();
                lbtn_mamber.Text = "人員名單";
                lbtn_mamber.ID = string.Format("lbtn_member_{0}", e.Row.Cells[6].Text);
                lbtn_mamber.CommandName = "Member";
                lbtn_mamber.CommandArgument = e.Row.Cells[6].Text;
                lbtn_mamber.Command += btn_Command;
                lbtn_mamber.Enabled = sysValue.Authority.Select;
                lbtn_mamber.Font.Underline = false;
                e.Row.Cells[4].Controls.Add(lbtn_mamber);
                e.Row.Cells[5].Controls.Clear();
                LinkButton lbtn_notify = new LinkButton();
                lbtn_notify.Text = "EMAIL通知";
                lbtn_notify.ID = string.Format("lbtn_notify_{0}", e.Row.Cells[6].Text);
                lbtn_notify.CommandName = "Notify";
                lbtn_notify.CommandArgument = e.Row.Cells[6].Text;
                lbtn_notify.Command += btn_Command;
                lbtn_notify.OnClientClick = "return confirm('確認要通知人員嗎？');";
                lbtn_notify.Enabled = sysValue.Authority.Select;
                lbtn_notify.Font.Underline = false;
                e.Row.Cells[5].Controls.Add(lbtn_notify);
                #endregion
            }
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EG_Year")) return;
            e.InputParameters.Add("EG_Year", txtSh_EG_Year.Text.Trim());
            if (ddlSh_EG_Type.SelectedIndex == 0)
                e.InputParameters.Add("EG_Type", "");
            else
                e.InputParameters.Add("EG_Type", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            e.InputParameters.Add("EG_Subject", ddlSh_EG_Subject.SelectedValue);
            string EG_Region = "";
            if (ddlSh_EG_Region.SelectedValue != "")
                EG_Region = string.Format("{0},{1},{2},{3}",
                        ddlSh_EG_Region.SelectedItem.Selected ? "Y" : "N",
                        ddlSh_EG_Region.SelectedItem.Value,
                        (ViewState["ddlSh_EG_Region"] as DataTable).Rows[ddlSh_EG_Region.SelectedIndex - 1]["TypeSubCode"].ToString(),
                        ddlSh_EG_Region.SelectedItem.Text);
            e.InputParameters.Add("EG_Region", EG_Region);
            e.InputParameters.Add("EG_Name", txtSh_EG_Name.Text.Trim());
            e.InputParameters.Add("EG_SDate_s", DateRangeSh_EG_StartDate.Date_start);
            e.InputParameters.Add("EG_SDate_e", DateRangeSh_EG_StartDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            txtSh_EG_Year.Text = string.Empty;
            ddlSh_EG_Type.SelectedIndex = -1;
            ddlSh_EG_Region.Items.Clear();
            ddlSh_EG_Subject.SelectedIndex = -1;
            ddlSh_EG_Subject.Items.Clear();
            ddlSh_EG_Region.SelectedIndex = -1;
            txtSh_EG_Name.Text = string.Empty;
            DateRangeSh_EG_StartDate.Init();
            
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        protected void ddlSh_EP_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            string EP_Type = "";
            EP_Type = (ViewState["ddlSh_EP_Type"] as DataTable).Rows[ddlSh_EP_Type.SelectedIndex]["TypeSubCode"].ToString();
            switch (EP_Type)
            {
                case "E011"://1人身保險業務員資格測驗
                    {
                        HyperLink_xls.Text = "09壽險測驗成績--exam_signup.xls";
                        HyperLink_xls.NavigateUrl = "~/XLS/09壽險測驗成績--exam_signup.xls";
                    }
                    break;
                case "E012"://2投資型保險商品測驗
                    {
                        HyperLink_xls.Text = "10投資型測驗成績--exam_signup.xls";
                        HyperLink_xls.NavigateUrl = "~/XLS/10投資型測驗成績--exam_signup.xls";
                    }
                    break;
                case "E013"://3外幣收付非型保險商品測驗
                    {
                        HyperLink_xls.Text = "11外幣測驗成績--exam_signup.xls";
                        HyperLink_xls.NavigateUrl = "~/XLS/11外幣測驗成績--exam_signup.xls";
                    }
                    break;
                case "E014"://4財產保險業務員考試
                    {
                        HyperLink_xls.Text = "12產險測驗成績--exam_signup.xls";
                        HyperLink_xls.NavigateUrl = "~/XLS/12產險測驗成績--exam_signup.xls";
                    }
                    break;
            }
        }


        #endregion

        protected void ddlSh_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlSh_EG_Type.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "160");
                    break;
                default:
                    ddlSh_EG_Subject.Items.Clear();
                    break;
            }
            if (ddlSh_EG_Type.SelectedIndex > 0)
                ViewState["ddlSh_EG_Region"] = DataPhrase.DDL_TypeName(ddlSh_EG_Region, true, "", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                ddlSh_EG_Region.Items.Clear();
        }
    }
}