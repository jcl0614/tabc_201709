﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EQRule : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddlSh_EB_SubType);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(selExamSub_EB_TransNo);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(selExamSub_EB_TransNo);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1300);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEQRule.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EB_SubType"] = DataPhrase.DDL_TypeName(ddlSh_EB_SubType, true, "", "144");
            DataPhrase.DDL_TypeName(ddlSh_EE_Item, true, "", "163");
            DataPhrase.DDL_TypeName(ddl_EE_Item, true, "請選擇", "163");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selExamSub_EB_TransNo.SelectedValue == "")
                sbError.Append("●「報考科目」必須選取!<br>");
            if (ddl_EE_Item.SelectedValue == "")
                sbError.Append("●「報考項目」必須選取!<br>");
            if(selExamSub_EB_TransNo.SelectedValue != "" && ddl_EE_Item.SelectedValue != "")
            {
                if (commandName == "Append" && !DataEQRule.valid_EB_EE_Item(selExamSub_EB_TransNo.SelectedValue, ddl_EE_Item.SelectedValue))
                    sbError.Append("●「報考科目」及「報考項目」已存在!<br>");
            }
            if (txt_EE_TimeLength.Text.Trim() == "")
                sbError.Append("●「測驗時間」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_TimeLength.Text.Trim())))
                    sbError.Append("●「測驗時間」格式錯誤!<br>");
            }

            if (txt_EE_TrueFalse.Text.Trim() == "")
                sbError.Append("●「是非題數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_TrueFalse.Text.Trim())))
                    sbError.Append("●「是非題數」格式錯誤!<br>");
            }
            if (txt_EE_TrueFalseS.Text.Trim() == "")
                sbError.Append("●「是非題分數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_TrueFalseS.Text.Trim())))
                    sbError.Append("●「是非題分數」格式錯誤!<br>");
            }
            if (txt_EE_TrueFalseO.Text.Trim() == "")
                sbError.Append("●「是非題順序」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_TrueFalseO.Text.Trim())))
                    sbError.Append("●「是非題順序」格式錯誤!<br>");
            }

            if (txt_EE_OneChoice.Text.Trim() == "")
                sbError.Append("●「單選題數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_OneChoice.Text.Trim())))
                    sbError.Append("●「單選題數」格式錯誤!<br>");
            }
            if (txt_EE_OneChoiceS.Text.Trim() == "")
                sbError.Append("●「單選題分數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_OneChoiceS.Text.Trim())))
                    sbError.Append("●「單選題分數」格式錯誤!<br>");
            }
            if (txt_EE_OneChoiceO.Text.Trim() == "")
                sbError.Append("●「單選題順序」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_OneChoiceO.Text.Trim())))
                    sbError.Append("●「單選題順序」格式錯誤!<br>");
            }

            if (txt_EE_MultiChoice.Text.Trim() == "")
                sbError.Append("●「複選題數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_MultiChoice.Text.Trim())))
                    sbError.Append("●「複選題數」格式錯誤!<br>");
            }
            if (txt_EE_MultiChoiceS.Text.Trim() == "")
                sbError.Append("●「複選題分數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_MultiChoiceS.Text.Trim())))
                    sbError.Append("●「複選題分數」格式錯誤!<br>");
            }
            if (txt_EE_MultiChoiceO.Text.Trim() == "")
                sbError.Append("●「複選題順序」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EE_MultiChoiceO.Text.Trim())))
                    sbError.Append("●「複選題順序」格式錯誤!<br>");
            }


            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EE_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_UidNo":
                            hif_EE_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EB_TransNo":
                            hif_EB_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selExamSub_EB_TransNo.search(hif_EB_TransNo.Value);
                            selExamSub_EB_TransNo.SelectedValue = hif_EB_TransNo.Value;
                            selExamSub_EB_TransNo.SearchText = string.Empty;
                            break;
                        case "EE_Item":
                            MainControls.ddlIndexSelectValue(ddl_EE_Item, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EE_TimeLength":
                            txt_EE_TimeLength.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_TrueFalse":
                            txt_EE_TrueFalse.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_TrueFalseS":
                            txt_EE_TrueFalseS.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_TrueFalseO":
                            txt_EE_TrueFalseO.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_OneChoice":
                            txt_EE_OneChoice.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_OneChoiceS":
                            txt_EE_OneChoiceS.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_OneChoiceO":
                            txt_EE_OneChoiceO.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_MultiChoice":
                            txt_EE_MultiChoice.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_MultiChoiceS":
                            txt_EE_MultiChoiceS.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EE_MultiChoiceO":
                            txt_EE_MultiChoiceO.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selExamSub_EB_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selExamSub_EB_TransNo_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selExamSub_EB_TransNo_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selExamSub_EB_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selExamSub_EB_TransNo.search(hif_EB_TransNo.Value);
            selExamSub_EB_TransNo.SelectedValue = hif_EB_TransNo.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQRule.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEQRule.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQRule.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEQRule.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EE_UidNo.Value = string.Empty;
                        this.SetFocus(txt_EE_TimeLength);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQRule.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEQRule.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEQRule.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EE_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EE_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EE_UidNo", hif_EE_UidNo.Value);
            }
            DataRow dr_examSub = DataExamSub.DataReader_EB_TransNo(selExamSub_EB_TransNo.SelectedValue);
            if (dr_examSub != null)
            {
                hsData.Add("EB_TransNo", dr_examSub["EB_TransNo"].ToString());
                hsData.Add("EB_UidNo", dr_examSub["EB_UidNo"].ToString());
                hsData.Add("EE_ExamType", dr_examSub["EB_SubType"].ToString());
                hsData.Add("EE_Subject", dr_examSub["EB_Subject"].ToString());
            }
            hsData.Add("EE_Item", ddl_EE_Item.SelectedValue);
            hsData.Add("EE_TimeLength", txt_EE_TimeLength.Text.Trim());
            hsData.Add("EE_TrueFalse", txt_EE_TrueFalse.Text.Trim());
            hsData.Add("EE_TrueFalseS", txt_EE_TrueFalseS.Text.Trim());
            hsData.Add("EE_TrueFalseO", txt_EE_TrueFalseO.Text.Trim());
            hsData.Add("EE_OneChoice", txt_EE_OneChoice.Text.Trim());
            hsData.Add("EE_OneChoiceS", txt_EE_OneChoiceS.Text.Trim());
            hsData.Add("EE_OneChoiceO", txt_EE_OneChoiceO.Text.Trim());
            hsData.Add("EE_MultiChoice", txt_EE_MultiChoice.Text.Trim());
            hsData.Add("EE_MultiChoiceS", txt_EE_MultiChoiceS.Text.Trim());
            hsData.Add("EE_MultiChoiceO", txt_EE_MultiChoiceO.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EE_TransNo", TransNo);
            hsData.Add("EE_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EE_UidNo.Value = string.Empty;
            selExamSub_EB_TransNo.Clear();
            ddl_EE_Item.SelectedIndex = -1;
            txt_EE_TimeLength.Text = string.Empty;
            txt_EE_TrueFalse.Text = string.Empty;
            txt_EE_TrueFalseS.Text = string.Empty;
            txt_EE_TrueFalseO.Text = string.Empty;
            txt_EE_OneChoice.Text = string.Empty;
            txt_EE_OneChoiceS.Text = string.Empty;
            txt_EE_OneChoiceO.Text = string.Empty;
            txt_EE_MultiChoice.Text = string.Empty;
            txt_EE_MultiChoiceS.Text = string.Empty;
            txt_EE_MultiChoiceO.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EE_TimeLength);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EE_TimeLength);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[8].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[9].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[8].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[8].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEQRule.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[9].Text, GridView1.Rows[i].Cells[10].Text)));

                    //RServiceProvider rsp = DataEQRule.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEQRule.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EB_SubType")) return;
            if (ddlSh_EB_SubType.SelectedIndex > 0)
                e.InputParameters.Add("EB_SubType", (ViewState["ddlSh_EB_SubType"] as DataTable).Rows[ddlSh_EB_SubType.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                e.InputParameters.Add("EB_SubType", "");
            e.InputParameters.Add("EB_Subject", ddlSh_EB_Subject.SelectedValue);
            e.InputParameters.Add("EE_Item", ddlSh_EE_Item.SelectedValue);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            ddlSh_EB_SubType.SelectedIndex = -1;
            ddlSh_EB_Subject.Items.Clear();
            ddlSh_EB_Subject.SelectedIndex = -1;
            ddlSh_EE_Item.SelectedIndex = -1;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void ddlSh_EB_SubType_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlSh_EB_SubType.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddlSh_EB_Subject, true, "", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddlSh_EB_Subject, true, "", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddlSh_EB_Subject, true, "", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddlSh_EB_Subject, true, "", "160");
                    break;
                default:
                    ddlSh_EB_Subject.Items.Clear();
                    break;
            }
        }
    }
}