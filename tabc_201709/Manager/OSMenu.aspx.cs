﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class OSMenu : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (btnEdit.Visible)
            {
                this.SetFocus(ddl_muLayer);
                Panel_Form.DefaultButton = btnEdit.ID;
            }
            else
            {
                this.SetFocus(txt_muNo);
                Panel_Form.DefaultButton = btnAppend.ID;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 770, true);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //清除編輯元件內容
                clearControlContext();
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataMenu.menuDataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataMenuLayer.menuLayerReaderDropDownList(ddl_muLayer, true, "");
            DataMenuLayer.menuLayerReaderDropDownList(ddl_QmuLayer, true, "");
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "muNo":
                            txt_muNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "muLayer":
                            MainControls.ddlIndexSelectValue(ddl_muLayer, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            DataSysKm.KmDropDownList(ddl_Km, true, "", ddl_muLayer.SelectedValue);
                            break;
                        case "muName":
                            txt_muName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "muSort":
                            txt_muSort.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "muHyperLink":
                            txt_muHyperLink.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_muHyperLink.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "muTarget":
                            MainControls.ddlIndexSelectValue(ddl_muTarget, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "muEnable":
                            cbox_muEnable.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            break;
                        case "muIsSub":
                            cbox_muIsSub.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            break;
                        case "muLocation":
                            MainControls.ddlIndexSelectValue(ddl_muLocation, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "muKm":
                            MainControls.ddlIndexSelectValue(ddl_Km, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                    }
                }

            maintainButtonEnabled("Select");
        }
        #endregion

        #region 資料驗証
        private string dataVaild(Boolean b)
        {
            StringBuilder sbError = new StringBuilder();
            if (b)
            {
                if (txt_muNo.Text.Trim() == "")
                    sbError.Append("●「編號」必須輸入!<br>");
                else
                    if (txt_muNo.Text.Trim().Length > 5 || !MainControls.ValidDataType("^\\w+$", txt_muNo.Text.Trim()))
                        sbError.Append("●「編號」格式錯誤!<br>");
                    else
                        if (!DataMenu.muNoValid(txt_muNo.Text.Trim()))
                            sbError.Append("●「編號」已被使用!<br>");

                
            }
            if (txt_muHyperLink.Text.Trim() == "")
                sbError.Append("●「選單連結」必須輸入!<br>");
            else
                if (!MainControls.ValidDataType("(http(s)?://([\\w-]+\\.?)+[\\w-]+(/[\\w- ./?%&=:]*)?)|(([\\w-]+\\.?)+[\\w-]+(/[\\w- ./?%&=:]*)?)", txt_muHyperLink.Text.Trim()))
                    sbError.Append("●「選單連結」格式錯誤!<br>");
                else
                    if (!DataMenu.hyperLinkValid(txt_muHyperLink.Text.Trim()) && txt_muHyperLink.Text.Trim() != hif_muHyperLink.Value)
                        sbError.Append("●「選單連結」已被使用!<br>");
            if (ddl_muLayer.SelectedValue == "")
                sbError.Append("●「選單分類」必須選擇!<br>");
            if (txt_muSort.Text.Trim() == "")
                sbError.Append("●「顯示順序」必須輸入!<br>");
            else
                if (MainControls.ValidDataType("^\\d+$", (txt_muSort.Text.Trim())))
                {
                    if (int.Parse(txt_muSort.Text.Trim()) < 1 || int.Parse(txt_muSort.Text.Trim()) > 99)
                        sbError.Append("●「顯示順序」格式錯誤!<br>");
                }
                else
                    sbError.Append("●「顯示順序」格式錯誤!<br>");
            if (txt_muName.Text.Trim() == "")
                sbError.Append("●「選單名稱」必須輸入!<br>");
            else
                if (txt_muName.Text.Trim().Length > 50)
                    sbError.Append("●「選單名稱」長度不得大於50個字元!<br>");

            return sbError.ToString().Trim();
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            if (e.CommandName != "Cancel")
            {

            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        lbl_Msg.Text = dataVaild(true);
                        if (lbl_Msg.Text.Trim().Length > 0)
                            return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataMenu.GetSchema(), dataToHashtable()));
                        RServiceProvider rsp = DataMenu.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hif_muHyperLink.Value = txt_muHyperLink.Text.Trim();
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        lbl_Msg.Text = dataVaild(false);
                        if (lbl_Msg.Text.Trim().Length > 0)
                            return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataMenu.GetSchema(), dataToHashtable()));
                        RServiceProvider rsp = DataMenu.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        lbl_Msg.Text = dataVaild(false);
                        if (lbl_Msg.Text.Trim().Length > 0)
                            return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataMenu.GetSchema(), dataToHashtable()));
                        RServiceProvider rsp = DataMenu.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            clearControlContext();
                        }
                    }
                    break;
                case "Cancel": //取消
                    clearControlContext();
                    break;
            }
            maintainButtonEnabled(e.CommandName);

        }
        #endregion

        #region 清除編輯元件內容
        //TODO:程式非共用修改-8
        private void clearControlContext()
        {
            txt_muNo.Text = string.Empty;
            ddl_muLayer.SelectedItem.Selected = false;
            txt_muSort.Text = string.Empty;
            txt_muName.Text = string.Empty;
            txt_muHyperLink.Text = string.Empty;
            hif_muHyperLink.Value = string.Empty;
            ddl_muTarget.SelectedIndex = -1;
            cbox_muEnable.Checked = true;
            cbox_muIsSub.Checked = false;
            ddl_muLocation.SelectedIndex = -1;
            ddl_Km.SelectedIndex = -1;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnCancel.Visible = false;
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    txt_muNo.ReadOnly = true;
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_muLayer);
                    break;
                default:
                    txt_muNo.ReadOnly = false;
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    this.SetFocus(txt_muNo);
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable, 供異動資料使用
        private Hashtable dataToHashtable()
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("muNo", Lib.FdVP(txt_muNo.Text.Trim()));
            hsData.Add("muLayer", ddl_muLayer.SelectedItem.Value);
            hsData.Add("muSort", Lib.FdVP(txt_muSort.Text.Trim()));
            hsData.Add("muName", txt_muName.Text.Trim());
            hsData.Add("muHyperLink", Lib.FdVP(txt_muHyperLink.Text.Trim(), "/%&"));
            hsData.Add("muTarget", ddl_muTarget.SelectedItem.Value);
            hsData.Add("muEnable", cbox_muEnable.Checked);
            hsData.Add("muIsSub", cbox_muIsSub.Checked);
            hsData.Add("muLocation", ddl_muLocation.SelectedValue);
            if(ddl_Km.SelectedValue != "")
                hsData.Add("muKm", ddl_Km.SelectedValue);
            return hsData;
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource1_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPageCtrlPrev.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("muLayer")) return;
            e.InputParameters.Add("muLayer", ddl_QmuLayer.SelectedValue.Trim());
            e.InputParameters.Add("muEnable", ddl_QmuEnable.SelectedValue.Trim());
            e.InputParameters.Add("muLocation", ddlSh_muLocation.SelectedValue.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            ddl_QmuLayer.SelectedIndex = 0;
            ddl_QmuEnable.SelectedIndex = 0;
            ddlSh_muLocation.SelectedIndex = 0;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void ddl_muLayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSysKm.KmDropDownList(ddl_Km, true, "", ddl_muLayer.SelectedValue);
        }
    }
}