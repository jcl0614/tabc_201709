﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MyEAttendTot : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EO_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddlSh_year);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddlSh_year);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 900);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                if (Data_tabcSales.isDM(Session["AccID"].ToString()))
                {
                    txtSh_DM.Text = Session["AccID"].ToString();
                    selPerson_AccID_EO_DM.search(Session["AccID"].ToString());
                    selPerson_AccID_EO_DM.SelectedValue = Session["AccID"].ToString();
                    selPerson_AccID_EO_DM.Enabled = false;
                    selPerson_AccID_EO_DM0.search(Session["AccID"].ToString());
                    selPerson_AccID_EO_DM0.SelectedValue = Session["AccID"].ToString();
                    selPerson_AccID_EO_DM0.Enabled = false;
                    MarkSortSetInit();
                }
                else
                {
                    txtSh_EO_AccID.Text = Session["AccID"].ToString();
                    txtSh_DM.Enabled = false;
                    txtSh_EO_AccID.Enabled = false;
                }
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEAttendTot.DataColumn_my(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EO_Unit"] = DataPhrase.DDL_TypeName(ddlSh_EO_Unit, true, "", "E008");
            ViewState["ddl_EO_Unit"] = DataPhrase.DDL_TypeName(ddl_EO_Unit, true, "請選擇", "E008");
            ViewState["ddl_EO_Unit0"] = DataPhrase.DDL_TypeName(ddl_EO_Unit0, true, "請選擇", "E008");
            ddl_EO_year.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
            ddl_EO_year.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddl_EO_year.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
            ddl_EO_year.SelectedValue = DateTime.Now.Year.ToString();
            ddl_EO_Month.SelectedValue = DateTime.Now.Month.ToString();
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
            ddlSh_year.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
            ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
            ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
            Data_tabcSales.DDL_AccID_DM(ddl_EO_AccID, true, "", Session["AccID"].ToString());
            if(ddl_EO_AccID.Items.Count == 0)
            {
                DataRow dr_PS = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                if(dr_PS != null)
                {
                    ddl_EO_AccID.Items.Add(new ListItem(string.Format("{0} ({1})", dr_PS["PS_NAME"].ToString(), dr_PS["AccID"].ToString()), dr_PS["AccID"].ToString()));
                }
            }
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_EO_Unit.SelectedValue == "")
                sbError.Append("●「單位」必須選取!<br>");
            if (ddl_EO_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「人員」必須選取!<br>");
            if (selPerson_AccID_EO_DM.SelectedValue.Trim() == "")
                sbError.Append("●「處經理」必須選取!<br>");
            if (txt_EO_ShouldAtt.Text.Trim() == "")
                sbError.Append("●「應出席次數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_ShouldAtt.Text.Trim())))
                    sbError.Append("●「應出席次數」格式錯誤!<br>");
            }
            if (ddl_EO_AccID.SelectedValue.Trim() != "" && ddl_EO_year.SelectedValue != "" && ddl_EO_Month.SelectedValue == "")
                if (commandName == "Append" && !DataEAttendTot.valid_EO(ddl_EO_AccID.SelectedValue, ddl_EO_year.SelectedValue, ddl_EO_Month.SelectedValue))
                    sbError.AppendFormat("●「人員」{0}已存{1}年{2}月份資料! ", ddl_EO_AccID.SelectedItem.Text, ddl_EO_year.SelectedValue, ddl_EO_Month.SelectedValue);
            return sbError.ToString();
        }
        private string dataValid_batch()
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_EO_Unit0.SelectedValue == "")
                sbError.Append("●「單位」必須選取!<br>");
            if (selPerson_AccID_EO_DM0.SelectedValue.Trim() == "")
                sbError.Append("●「處經理」必須選取!<br>");
            if (txt_EO_ShouldAtt0.Text.Trim() == "")
                sbError.Append("●「應出席次數」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_ShouldAtt0.Text.Trim())))
                    sbError.Append("●「應出席次數」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EO_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_UidNo":
                            hif_EO_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_Unit":
                            MainControls.ddlIndexSelectAttribute(ddl_EO_Unit, "TypeSubCode", ViewState["ddl_EO_Unit"] as DataTable, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EO_Year":
                            MainControls.ddlIndexSelectValue(ddl_EO_year, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EO_Month":
                            MainControls.ddlIndexSelectValue(ddl_EO_Month, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EO_AccID":
                            MainControls.ddlIndexSelectValue(ddl_EO_AccID, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EO_DM":
                            selPerson_AccID_EO_DM.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_EO_DM.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_EO_DM.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_ShouldAtt":
                            txt_EO_ShouldAtt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selPerson_AccID_EO_DM_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_DM_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_DM_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_DM_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EO_DM.search(hif_EO_DM.Value);
            selPerson_AccID_EO_DM.SelectedValue = hif_EO_DM.Value;

        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEAttendTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEAttendTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EO_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EO_UidNo", hif_EO_UidNo.Value);
            }
            hsData.Add("EO_Year", ddl_EO_year.SelectedValue);
            hsData.Add("EO_Month", ddl_EO_Month.SelectedValue);
            hsData.Add("EO_Unit", (ViewState["ddl_EO_Unit"] as DataTable).Rows[ddl_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString());
            hsData.Add("EO_AccID", ddl_EO_AccID.SelectedValue);
            hsData.Add("EO_DM", selPerson_AccID_EO_DM.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(ddl_EO_AccID.SelectedValue);
            if (dr_sales != null)
            {
                hsData.Add("EO_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("EO_ShouldAtt", txt_EO_ShouldAtt.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", TransNo);
            hsData.Add("EO_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_batch(string EO_AccID)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("EO_UidNo", Session["UidNo"].ToString());
            hsData.Add("EO_Year", ddlSh_year.SelectedValue);
            hsData.Add("EO_Month", ddlSh_Month.SelectedValue);
            hsData.Add("EO_Unit", (ViewState["ddl_EO_Unit0"] as DataTable).Rows[ddl_EO_Unit0.SelectedIndex - 1]["TypeSubCode"].ToString());
            hsData.Add("EO_AccID", EO_AccID);
            hsData.Add("EO_DM", selPerson_AccID_EO_DM0.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(EO_AccID);
            if (dr_sales != null)
            {
                hsData.Add("EO_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("EO_ShouldAtt", txt_EO_ShouldAtt0.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_EC_batch(DataRow dr_sales, string EC_TransNo, string ED_Date)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EC_TransNo", EC_TransNo);
            hsData.Add("EC_UidNo", Session["UidNo"].ToString());
            hsData.Add("EC_Year", ddlSh_year.SelectedValue);
            hsData.Add("EC_Category", "2");
            hsData.Add("EC_Mode", "1");
            hsData.Add("EC_Code", string.Format("{0}{1}", Session["AccID"].ToString(), DateTime.Parse(ED_Date).ToString("yyyyMMdd")));
            hsData.Add("EC_CName", string.Format("{0}{1}早會", ddlSh_EO_Unit.SelectedItem.Text, DateTime.Parse(ED_Date).ToString("yyyy年MM月dd日")));
            hsData.Add("EC_SDate", string.Format("{0} 08:00:00", ED_Date));
            hsData.Add("EC_EDate", string.Format("{0} 09:00:00", ED_Date));
            hsData.Add("EC_STime", "08:00");
            hsData.Add("EC_ETime", "09:00");
            hsData.Add("EC_SDate1", DateTime.Now.ToString("yyyy/MM/dd"));
            hsData.Add("EC_EDate1", DateTime.Now.ToString("yyyy/MM/dd"));
            hsData.Add("EC_Unit", dr_sales["Zone"].ToString());
            hsData.Add("EC_Area", dr_sales["cZON_TYPE"].ToString());
            hsData.Add("EC_PNo", "0");
            hsData.Add("EC_Hours", "1");
            hsData.Add("EC_IsAtt", "Y");
            hsData.Add("EC_IsHours", "N");
            hsData.Add("EC_DM", Session["AccID"].ToString());
            hsData.Add("EC_DMName", dr_sales["Name"].ToString());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_ED_batch(DataRow dr_sales, string EC_TransNo, string ED_Date)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(EC_TransNo);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                hsData.Add("ED_Code", dr_course["EC_Code"].ToString());
                hsData.Add("ED_CName", dr_course["EC_CName"].ToString());
                hsData.Add("ED_Mode", dr_course["EC_Mode"].ToString());
                hsData.Add("ED_Category", dr_course["EC_Category"].ToString());
                hsData.Add("ED_AppointDate", dr_course["EC_SDate"].ToString());
                hsData.Add("ED_AttendDate", dr_course["EC_SDate"].ToString());
                hsData.Add("ED_ClassHours", dr_course["EC_Hours"].ToString());
            }
            hsData.Add("ED_AccID", dr_sales["cSAL_FK"].ToString());
            hsData.Add("ED_ID", dr_sales["cSAL_ID_S"].ToString());
            hsData.Add("ED_Name", dr_sales["NAME"].ToString());
            hsData.Add("ED_Date", DateTime.Now.ToString("yyyy/MM/dd"));
            hsData.Add("ED_IsSelf", "N");
            hsData.Add("ED_Type", "2");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EO_UidNo.Value = string.Empty;
            hif_EO_AccID.Value = string.Empty;
            hif_EO_DM.Value = string.Empty;
            ddl_EO_Unit.SelectedIndex = -1;
            ddl_EO_year.SelectedIndex = 0;
            ddl_EO_Month.SelectedIndex = 0;
            txt_EO_ShouldAtt.Text = string.Empty;
            ddl_EO_AccID.SelectedIndex = -1;
            //selPerson_AccID_EO_DM.search(Session["AccID"].ToString());
            //selPerson_AccID_EO_DM.SelectedValue = Session["AccID"].ToString();
            //selPerson_AccID_EO_DM.Enabled = false;
            lbl_Msg.Text = string.Empty;

            ddl_EO_Unit0.SelectedIndex = -1;
            ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
            ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
            txt_EO_ShouldAtt0.Text = string.Empty;
            //selPerson_AccID_EO_DM0.search(Session["AccID"].ToString());
            //selPerson_AccID_EO_DM0.SelectedValue = Session["AccID"].ToString();
            //selPerson_AccID_EO_DM0.Enabled = false;
            lbl_Msg0.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
            switch (e.CommandName)
            {
                case "3":
                    DataRow dr_EO = DataEAttendTot.DataReader_EO_TransNo(e.CommandArgument.ToString());
                    if (dr_EO != null)
                    {
                        string DM_Name = "";
                        DataRow dr_DM = Data_tabcSales.DataReader_AccID(dr_EO["EO_DM"].ToString());
                        if (dr_DM != null)
                            DM_Name = dr_DM["Name"].ToString();
                        lbl_mTitle.Text = string.Format("{0}年{1}月　單位：{2}　處經理：{3}", dr_EO["EO_Year"].ToString(), dr_EO["EO_Month"].ToString(), dr_EO["EO_Unit"].ToString(), DM_Name);
                        DataTable dt = DataEAttendTot.DataReader_EO_DM(dr_EO["EO_DM"].ToString(), dr_EO["EO_Year"].ToString(), dr_EO["EO_Month"].ToString()).Tables[0];
                        DataTable dt_export = new DataTable();
                        
                        dt_export.Columns.Add("姓名／日期", typeof(string));
                        
                        int days = DateTime.DaysInMonth(int.Parse(dr_EO["EO_Year"].ToString()), int.Parse(dr_EO["EO_Month"].ToString()));
                        for (int i = 1; i <= days; i++)
                        {
                            string DayName = GetDayName(DateTime.Parse(string.Format("{0}/{1}/{2}", dr_EO["EO_Year"].ToString(), dr_EO["EO_Month"].ToString(), i)));
                            dt_export.Columns.Add(string.Format("{0}/{1}({2})", dr_EO["EO_Month"].ToString(), i.ToString("00"), DayName), typeof(string));
                            
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            List<string> status = new List<string>();
                            status.Add(dr["EO_Name"].ToString());
                            for (int i = 1; i <= days; i++)
                            {
                                DateTime today = DateTime.Parse(string.Format("{0}/{1}/{2}", dr_EO["EO_Year"].ToString(), dr_EO["EO_Month"].ToString(), i));
                                if (DataEAttend.DataReader_ED_AccID_ED_SignDate(dr["EO_AccID"].ToString(), "1", today.ToString("yyyy/MM/dd")) != null)
                                {
                                    status.Add("出席");
                                }
                                else
                                {
                                    if (today.DayOfWeek == DayOfWeek.Saturday || today.DayOfWeek == DayOfWeek.Sunday)
                                        status.Add("公休");
                                    else
                                        status.Add("缺席");
                                }
                            }
                            dt_export.Rows.Add(status.ToArray());
                        }
                        GridView2.DataSource = dt_export;
                        GridView2.DataBind();
                    }
                    break;
            }
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            lnkMaintain0.Visible = false;
            lnkExcelSet.Visible = false;
            bool isDM = Data_tabcSales.isDM(Session["AccID"].ToString());
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append & isDM;
                    lnkMaintain0.Visible = (ViewState["SysValue"] as SysValue).Authority.Append & isDM;
                    lnkExcelSet.Visible = true;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;

            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            bool isDM = Data_tabcSales.isDM(Session["AccID"].ToString());
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update & isDM;
                    btnDelete.Visible = true & sysValue.Authority.Delete & isDM;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EO_year);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append & isDM;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EO_year);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[8].Controls.Clear();
                ImageButton ibtn = new ImageButton();
                ibtn.ID = string.Format("ibtn_{0}", e.Row.Cells[10].Text);
                ibtn.CommandName = "3";
                ibtn.CommandArgument = e.Row.Cells[10].Text;
                ibtn.Command += lnk_Command;
                ibtn.ImageUrl = "~/images/signin.jpg";
                ibtn.Enabled = sysValue.Authority.Append;
                e.Row.Cells[8].Controls.Add(ibtn);

                e.Row.Cells[9].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[10].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[9].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[9].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[10].Text, GridView1.Rows[i].Cells[11].Text)));

                    //RServiceProvider rsp = DataEAttendTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EO_Year")) return;
            e.InputParameters.Add("EO_Year", txtSh_EO_Year.Text.Trim());
            e.InputParameters.Add("EO_Month", txtSh_EO_Month.Text.Trim());
            if (ddlSh_EO_Unit.SelectedIndex > 0)
                e.InputParameters.Add("EO_Unit", (ViewState["ddlSh_EO_Unit"] as DataTable).Rows[ddlSh_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                e.InputParameters.Add("EO_Unit", "");
            e.InputParameters.Add("EO_AccID", txtSh_EO_AccID.Text.Trim());
            e.InputParameters.Add("DM", txtSh_DM.Text.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EO_Unit.SelectedIndex = -1;
            txtSh_EO_Year.Text = string.Empty;
            txtSh_EO_Month.Text = string.Empty;

            if (Data_tabcSales.isDM(Session["AccID"].ToString()))
            {
                txtSh_DM.Text = Session["AccID"].ToString();
                txtSh_EO_AccID.Text = string.Empty;
            }
            else
            {
                txtSh_DM.Text = string.Empty;
                txtSh_EO_AccID.Text = Session["AccID"].ToString();
            }
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            string Sh_EO_Unit = "";
            if (ddlSh_EO_Unit.SelectedIndex > 0)
                Sh_EO_Unit = (ViewState["ddlSh_EO_Unit"] as DataTable).Rows[ddlSh_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString();
            DataTable dt = DataEAttendTot.DataReader_my(
                Lib.FdVP(txtSh_EO_Year.Text).Trim(),
                Lib.FdVP(txtSh_EO_Month.Text).Trim(),
                Sh_EO_Unit,
                Lib.FdVP(txtSh_EO_AccID.Text).Trim(),
                txtSh_DM.Text.Trim(),
                0, 99999
                ).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("年度", typeof(string));
                dt_export.Columns.Add("月份", typeof(string));
                dt_export.Columns.Add("單位", typeof(string));
                dt_export.Columns.Add("業務員編號", typeof(string));
                dt_export.Columns.Add("業務員姓名", typeof(string));
                dt_export.Columns.Add("處經理編號", typeof(string));
                dt_export.Columns.Add("處經理姓名", typeof(string));
                dt_export.Columns.Add("應出席次數", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    string EO_Unit = "";
                    string DM = "";
                    string DM_NAME = "";
                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr["EO_Unit"].ToString());
                    if (dr_Phrase != null)
                        EO_Unit = dr_Phrase["TypeName"].ToString();
                    DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EO_AccID"].ToString());
                    if (dr_sales != null)
                    {
                        DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                        if (dr_sales2 != null)
                        {
                            DM = dr_sales["DM"].ToString();
                            DM_NAME = dr_sales2["NAME"].ToString();
                        }
                    }
                    dt_export.Rows.Add(
                        dr["EO_Year"].ToString(),
                        dr["EO_Month"].ToString(),
                        EO_Unit,
                        dr["EO_AccID"].ToString(),
                        dr["EO_Name"].ToString(),
                        DM,
                        DM_NAME,
                        dr["EO_ShouldAtt"].ToString()
                        );
                }

                ExportControls.CreateCSVFile(Page, dt_export, string.Format("早會應出席資料_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
            }
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }



        protected void btnApply_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Append"://儲存
                    lbl_Msg0.Text = dataValid_batch(); //資料格式驗証
                    if (lbl_Msg0.Text.Trim().Length > 0) return;
                    DataRow dr_sales = Data_tabcSales.DataReader_Full_AccID(Session["AccID"].ToString());
                    if (dr_sales != null)
                    {
                        if (lboxObj.GetSelectedIndices().Count() != 0 && lboxSur.GetSelectedIndices().Count() != 0)
                        {
                            SysValue sysValue = ViewState["SysValue"] as SysValue;
                            StringBuilder sb_msg = new StringBuilder();
                            foreach (ListItem item in lboxObj.Items)
                            {
                                if (item.Selected)
                                {
                                    DataRow dt_EC = DataECourse.valid_EC(
                                        string.Format("{0}{1}", Session["AccID"].ToString(), DateTime.Parse(item.Value).ToString("yyyyMMdd")),
                                        string.Format("{0}{1}早會", ddlSh_EO_Unit.SelectedItem.Text, DateTime.Parse(item.Value).ToString("yyyy年MM月dd日")),
                                        string.Format("{0} 08:00:00", item.Value)
                                        );
                                    if (dt_EC == null)
                                    {
                                        DataSet ds_EC = new DataSet();
                                        //元件資料轉成Hashtable
                                        string EC_TransNo = System.Guid.NewGuid().ToString("N");
                                        ds_EC.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable_EC_batch(dr_sales, EC_TransNo, item.Value)));

                                        RServiceProvider rsp_EC = DataECourse.Append(sysValue.emNo, sysValue.ProgId, ds_EC);
                                        if(rsp_EC.Result)
                                        {
                                            dt_EC = DataECourse.DataReader_EC_TransNo(EC_TransNo);
                                        }

                                    }
                                    foreach (ListItem m_item in lboxSur.Items)
                                    {
                                        if (m_item.Selected)
                                        {
                                            DataRow dr_sales_m = Data_tabcSales.DataReader_Full_AccID(m_item.Value);
                                            if (dr_sales_m != null)
                                            {
                                                if (DataEAttend.valid_ED(dt_EC["EC_TransNo"].ToString(), m_item.Value))
                                                {
                                                    DataSet ds_ED = new DataSet();
                                                    //元件資料轉成Hashtable
                                                    ds_ED.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED_batch(dr_sales_m, dt_EC["EC_TransNo"].ToString(), item.Value)));

                                                    RServiceProvider rsp_ED = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds_ED);
                                                    if (!rsp_ED.Result)
                                                    {
                                                        logger.Error(string.Format("我的學習記錄>出席率查詢：DataEAttend[新增]失敗：\r\n", rsp_ED.ReturnMessage));
                                                        logger_db.Error(string.Format("我的學習記錄>出席率查詢：DataEAttend[新增]失敗：\r\n", rsp_ED.ReturnMessage));
                                                    }
                                                    else
                                                    {
                                                        
                                                    }
                                                }
                                                if (DataEAttendTot.valid_EO(m_item.Value, ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue))
                                                {
                                                    DataSet ds = new DataSet();
                                                    //元件資料轉成Hashtable
                                                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable_batch(m_item.Value)));

                                                    RServiceProvider rsp = DataEAttendTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                                                    if (!rsp.Result)
                                                    {
                                                        logger.Error(string.Format("我的學習記錄>出席率查詢：DataEAttend[新增]失敗：\r\n", rsp.ReturnMessage));
                                                        logger_db.Error(string.Format("我的學習記錄>出席率查詢：DataEAttend[新增]失敗：\r\n", rsp.ReturnMessage));
                                                        //sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}({1})：新增失敗！</span><br>", m_item.Text, m_item.Value);

                                                    }
                                                    //else
                                                    //    sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：新增成功！</span><br>", m_item.Text, m_item.Value);
                                                }
                                                
                                            }
                                        }
                                    }
                                    sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}：設定完成！</span><br>", item.Text);

                                }
                                
                            }
                            GridView1.DataBind();
                            lbl_Msg0.Text = sb_msg.ToString();
                        }
                        else
                        {
                            MainControls.showMsg(this, "請選擇「早會日期」及「應出席人員」！\\n(使用鍵盤Ctrl或Shift+滑鼠進行選取或取消)");
                        }
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    lbl_Msg0.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }

        protected void MarkSortSetInit()
        {
            DataTable dt = Data_tabcSales.DataReader_DM(Session["AccID"].ToString()).Tables[0];
            lboxSur.Items.Clear();
            foreach (DataRow dw in dt.Rows)
            {
                ListItem li = new ListItem();
                li.Text = dw["NAME"].ToString().Trim();
                li.Value = dw["cSAL_FK"].ToString().Trim();
                lboxSur.Items.Add(li);
            }

            initDays();
        }

        private void initDays()
        {
            lboxObj.Items.Clear();
            int days = DateTime.DaysInMonth(int.Parse(ddlSh_year.SelectedValue), int.Parse(ddlSh_Month.SelectedValue));
            for (int i = 1; i <= days; i++)
            {
                string DayName = GetDayName(DateTime.Parse(string.Format("{0}/{1}/{2}", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, i)));
                ListItem li = new ListItem();
                li.Text = string.Format("{0}/{1}/{2}({3})", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, i.ToString("00"), DayName);
                li.Value = string.Format("{0}/{1}/{2}", ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue, i.ToString("00"));
                lboxObj.Items.Add(li);
            }
        }

        private string GetDayName(DateTime today)
        {
            string result = "";

            if (today.DayOfWeek == DayOfWeek.Monday)
            {
                result = "一";
            }
            else if (today.DayOfWeek == DayOfWeek.Tuesday)
            {
                result = "二";
            }
            else if (today.DayOfWeek == DayOfWeek.Wednesday)
            {
                result = "三";
            }
            else if (today.DayOfWeek == DayOfWeek.Thursday)
            {
                result = "四";
            }
            else if (today.DayOfWeek == DayOfWeek.Friday)
            {
                result = "五";
            }
            else if (today.DayOfWeek == DayOfWeek.Saturday)
            {
                result = "六";
            }
            else if (today.DayOfWeek == DayOfWeek.Sunday)
            {
                result = "日";
            }

            return result;
        }

        protected void ddlSh_year_SelectedIndexChanged(object sender, EventArgs e)
        {
            initDays();
        }

        protected void ddlSh_Month_SelectedIndexChanged(object sender, EventArgs e)
        {
            initDays();
        }

    }
}