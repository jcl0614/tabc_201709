﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class selExamSub : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                ddlSetting();
        }

        private void ddlSetting()
        {
            if (Lib.FdVP(txtSh_NO.Text.Trim()) != "")
            {
                DataExamSub.DDL_EB_TransNo(ddl_NO, true, "請選擇報考科目", Lib.FdVP(txtSh_NO.Text.Trim()));
                MainControls.ddlIndexSelectValue(ddl_NO, txtSh_NO.Text.Trim());
            }
            else
                ddl_NO.Items.Clear();

            if (ddl_NO.Items.Count != 0)
                ddl_NO.Enabled = true;
            else
            {
                ddl_NO.Items.Add(new ListItem("請輸入報考科目關鍵字", ""));
                ddl_NO.Enabled = false;
            }
        }
        public void search(string key)
        {
            txtSh_NO.Text = key;
            ddlSetting();
        }
        public void Clear()
        {
            txtSh_NO.Text = string.Empty;
            ddl_NO.Items.Clear();

            if (ddl_NO.Items.Count != 0)
                ddl_NO.Enabled = true;
            else
            {
                ddl_NO.Items.Add(new ListItem("請輸入報考科目關鍵字", ""));
                ddl_NO.Enabled = false;
            }
        }
        public string SelectedValue
        {
            get
            {
                return ddl_NO.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_NO, value);
            }
        }
        public string SelectedText
        {
            get
            {
                return ddl_NO.SelectedItem.Text;
            }
            set
            {
                ddl_NO.SelectedItem.Text = value;
                MainControls.ddlIndexSelectText(ddl_NO, value);
            }
        }
        public string SearchText
        {
            get
            {
                return txtSh_NO.Text.Trim();
            }
            set
            {
                txtSh_NO.Text = value.Trim();
            }
        }

        public delegate void PatientsChangedEventHandler(object sender, EventArgs e);

        public event PatientsChangedEventHandler SelectedIndexChanged;
        protected void ddl_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
                this.SelectedIndexChanged(sender, e);
        }

        public event PatientsChangedEventHandler TextChanged;
        protected void txtSh_NO_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null)
                this.TextChanged(sender, e);
            ddlSetting();
        }

        public event PatientsChangedEventHandler SearchButtonClick;
        protected void ibtn_uSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (this.SearchButtonClick != null)
                this.SearchButtonClick(sender, e);
            ddlSetting();
        }

        public event PatientsChangedEventHandler RefreshButtonClick;
        protected void ibtn_uRefresh_Click(object sender, ImageClickEventArgs e)
        {
            if (this.RefreshButtonClick != null)
                this.RefreshButtonClick(sender, e);
        }

    }
}