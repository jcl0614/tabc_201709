﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class selcolor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static void m(string s)
        {
            StringBuilder sb = new StringBuilder();
            string[] color = s.Split('#');
            for (int i = 1; i < 16; i++)
                sb.AppendFormat("#{0},", color[i]);
            RServiceProvider rsp = new RServiceProvider();
            List<SqlParameter> lSqlParas = new List<SqlParameter>();
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@value", sb.ToString()));
            lSqlParas.Add(SqlAccess.CreateSqlParameter("@name", "SelColor"));
            rsp = SqlAccess.SqlcommandNonQueryExecute("0", " UPDATE WebSet SET value=@value WHERE name=@name ", lSqlParas.ToArray());
        }
    }
}