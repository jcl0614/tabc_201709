﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="selTeacher.ascx.cs" Inherits="tabc_201709.Manager.ascx.selTeacher" %>
<asp:UpdatePanel ID="UpdatePanel_selTeacher" runat="server">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:DropDownList ID="ddl_NO" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_NO_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="right" width="110">
                    <asp:TextBox ID="txtSh_NO" runat="server" 
                        BackColor="#DFDFDF" ontextchanged="txtSh_NO_TextChanged" Width="100px"></asp:TextBox>
                </td>
                <td align="right" width="20">
                    <asp:ImageButton ID="ibtn_uSearch" runat="server" ImageUrl="~/Manager/images/Search-icon.png" ToolTip="查詢" />
                </td>
                <td align="right" width="20">
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Manager/images/refresh.png" OnClick="ibtn_uRefresh_Click" ToolTip="復原初始值" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>