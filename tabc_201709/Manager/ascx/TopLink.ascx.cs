﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class TopLink : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LinkStr();
                
            }
            else
                btnInit(((Button)this.FindControl(string.Format("btn_v{0}", MultiView1.ActiveViewIndex))));
            
        }

        #region 連結文字
        private void LinkStr()
        {

        }
        #endregion

        protected void UpdatePanel_TopLink_Load(object sender, EventArgs e)
        {
            LinkStr();
        }

        protected void btn_Command(object sender, CommandEventArgs e)
        {
            MultiView1.ActiveViewIndex = int.Parse(e.CommandName);

            btnInit((Button)sender);
        }

        private void btnInit(Button btn)
        {
            btn_v0.BorderColor = System.Drawing.Color.FromName("#ffffff");
            btn_v1.BorderColor = System.Drawing.Color.FromName("#ffffff");
            btn_v2.BorderColor = System.Drawing.Color.FromName("#ffffff");
            btn_v0.ForeColor = System.Drawing.Color.FromName("#808080");
            btn_v1.ForeColor = System.Drawing.Color.FromName("#808080");
            btn_v2.ForeColor = System.Drawing.Color.FromName("#808080");

            btn.BorderColor = System.Drawing.Color.FromName("#edcd66");
            btn.ForeColor = System.Drawing.Color.FromName("#666666");
        }

        protected void Calendar_top_SelectionChanged(object sender, EventArgs e)
        {
            MainControls.WindowOpen(UpdatePanel_TopLink, "Schedule", true, true, true, 935, 665, "images/demo/open_schedule.jpg");
        }




    }
}