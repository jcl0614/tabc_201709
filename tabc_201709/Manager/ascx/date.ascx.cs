﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class date : System.Web.UI.UserControl
    {
        private bool Years_18 = true;

        protected void Page_Init(object sender, EventArgs e)
        {
            MainControls.yearDDL(ddl_year, IsYears_18);
            MainControls.momthDDL(ddl_momth);
        }
        protected void ddl_momth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddl_year.SelectedValue != "" && ddl_momth.SelectedValue != "")
                MainControls.dayDDL(ddl_day, int.Parse(ddl_year.SelectedValue), int.Parse(ddl_momth.SelectedValue));
            else
                ddl_day.Items.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsYears_18
        {
            get
            {
                return Years_18;
            }
            set
            {
                MainControls.yearDDL(ddl_year, value);
                Years_18 = value;
            }
        }

        /// <summary>
        /// 年
        /// </summary>
        public string Year
        {
            get
            {
                return ddl_year.SelectedValue;
            }
            set
            {
                ddl_year.SelectedValue = value;
            }
        }

        /// <summary>
        /// 月
        /// </summary>
        public string Month
        {
            get
            {
                return ddl_momth.SelectedValue;
            }
            set
            {
                ddl_momth.SelectedValue = value;
            }
        }

        /// <summary>
        /// 日
        /// </summary>
        public string day
        {
            get
            {
                return ddl_day.SelectedValue;
            }
            set
            {
                ddl_day.SelectedValue = value;
            }
        }
    }
}