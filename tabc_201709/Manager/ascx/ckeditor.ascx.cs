﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class ckeditor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        public string Html
        {
            get
            {
                return txt_ckeditor.Text;
            }
            set
            {
                txt_ckeditor.Text = value;
            }
        }
        public Unit Width
        {
            get
            {
                return txt_ckeditor.Width;
            }
            set
            {
                txt_ckeditor.Width = value;
            }
        }
        public Unit Height
        {
            get
            {
                return txt_ckeditor.Height;
            }
            set
            {
                txt_ckeditor.Height = value;
            }
        }
    }
}