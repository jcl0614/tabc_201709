﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class logout : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }

        #region 登出按鈕
        protected void img_signoff_Click(object sender, ImageClickEventArgs e)
        {
            Session["emUser"] = "";
            Session["emID"] = "";
            Session["emNo"] = "";
            Session["emOS"] = "0";
            Session["emDefOM"] = "";
            Session["emLimit"] = "";
            Session["userMenu"] = "";

            Response.Redirect("Login");
        }
        #endregion
    }
}