﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class TimeRange : System.Web.UI.UserControl
    {
        private string fieldName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Init()
        {
            ddl_sTime.SelectedIndex = -1;
            ddl_eTime.SelectedIndex = -1;
        }
        public string valid
        {
            get
            {
                StringBuilder sbError = new StringBuilder();
                if (DateTime.Compare(DateTime.Parse(string.Format("2017/09/01 {0}", ddl_eTime.SelectedValue)), DateTime.Parse(string.Format("2017/09/01 {0}", ddl_sTime.SelectedValue))) < 0)
                    sbError.AppendFormat("●「{0}」結束時間不可小於起始時間！", fieldName);
                return sbError.ToString();
            }
            set
            {

            }
        }
        public string FieldName
        {
            get
            {
                return fieldName;
            }
            set
            {
                fieldName = value;
            }
        }
        public string Time_start
        {
            get
            {
                return ddl_sTime.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_sTime, value);
            }
        }
        public string Time_end
        {
            get
            {
                return ddl_eTime.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_eTime, value);
            }
        }
        public bool Enabled
        {
            get
            {
                return ddl_sTime.Enabled;
            }
            set
            {
                ddl_sTime.Enabled = value;
                ddl_eTime.Enabled = value;
            }
        }
    }
}