﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class Explorer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //登入驗證
            if (DataEmployee.emNoValid(Session["emNo"].ToString().Trim()))
            {
                //關閉式窗
                ScriptManager.RegisterStartupScript(this, this.GetType(), MainControls.random("", 8), "window.opener=null; window.open('', '_self', ''); window.close();", true);
                return;
            }
        }
    }
}