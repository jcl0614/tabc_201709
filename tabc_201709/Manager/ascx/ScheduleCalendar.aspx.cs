﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class ScheduleCalendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Calendar_top_DayRender(object sender, DayRenderEventArgs e)
        {
            DataTable dt = DataScheduleM.DataReader_AccID_Date(Session["AccID"].ToString(), e.Day.Date.ToString("yyyy/MM/dd")).Tables[0];

            HyperLink btn = new HyperLink();
            btn.ID = "Date" + e.Day.Date.ToString("yyyyMMdd");
            btn.Text = e.Day.Date.Day.ToString();
            if (dt.Rows.Count != 0)
            {
                btn.NavigateUrl = string.Format("~/manager/Schedule?open=1&d={0}", Server.UrlEncode(e.Day.Date.ToString("yyyy/MM/dd")));
                btn.Target = "_blank";
                btn.Font.Bold = true;
                btn.ForeColor = ColorTranslator.FromHtml("#ff0000");
            }
            
            e.Cell.Text = "";
            e.Cell.Controls.Add(btn);
        }
    }
}