﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace tabc_201709.Manager.ascx
{
    /// <summary>
    /// ShowUploadPhoto 的摘要描述
    /// </summary>
    public class ShowUploadPhoto : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string guidname = context.Request.QueryString["Guid"];
            if (string.IsNullOrEmpty(guidname))
            {
                NoFound(context);
                return;
            }

            Guid guid = new Guid(guidname);

            string tempdir = context.Server.MapPath("~/Manager/UploaderTemp/");
            string[] files = Directory.GetFiles(tempdir, "persisted." + guid + ".*");
            if (files == null || files.Length == 0)
            {
                NoFound(context);
                return;
            }

            string filepath = files[0];

            string filename = Path.GetFileNameWithoutExtension(filepath).Remove(0, 47);

            //context.Response.Cache.SetExpires(DateTime.Now.AddDays(2));
            SampleUtil.DownloadFile(context, filepath, filename, true);

        }

        void NoFound(HttpContext context)
        {
            string imgfile = "~/Manager/images/nofound.png";

            context.Response.Redirect(imgfile);

            string filepath = context.Server.MapPath(imgfile);
            SampleUtil.DownloadFile(context, filepath, "nofound.png", true);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}