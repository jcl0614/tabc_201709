﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // 在這裡放置使用者程式碼以初始化網頁
            if (Session["emUser"].ToString().Trim() == "")
                Response.Redirect("manager.aspx");


            if (!IsPostBack)
            {
                DisplayMenu();
            }

        }

        #region 功能項目連結
        private void DisplayMenu()
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt_MenuLayer = DataMenuLayer.DataMenuLayerReader(true).Tables[0];
            if (dt_MenuLayer != null)
            {
                DataRow dr = DataEmployee.emDataReader(Session["emNo"].ToString());
                for (int i = 1; i <= dt_MenuLayer.Rows.Count; i++)
                {
                    DataTable dt_Menu = DataMenu.menuReader(dt_MenuLayer.Rows[i - 1]["id"].ToString(), dr["emNo"].ToString()).Tables[0];
                    if (!string.IsNullOrEmpty(dr["EmGroup"].ToString()))
                        dt_Menu = DataMenu.menuReader_group(dt_MenuLayer.Rows[i - 1]["id"].ToString(), dr["EmGroup"].ToString()).Tables[0];
                    if (bool.Parse(dt_MenuLayer.Rows[i - 1]["enable"].ToString()) && dt_Menu.Rows.Count != 0)
                    {
                        Boolean active = false;
                        for (int j = Request.Url.Segments.Length - 1; j >= 0; j--)
                        {
                            if (DataMenu.PageLimitCheck(Request.Url.Segments[j].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[j].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], this.Session).Authority.Select)
                            {
                                active = DataMenuLayer.DataMenuLayerActive(Request.Url.Segments[j].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], dt_MenuLayer.Rows[i - 1]["id"].ToString());
                                break;
                            }
                            if (DataMenu.PageLimitCheck(Request.Url.Segments[j].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[j].Replace("/", string.Empty), this.Session).Authority.Select)
                            {
                                active = DataMenuLayer.DataMenuLayerActive(Request.Url.Segments[j].Replace("/", string.Empty), dt_MenuLayer.Rows[i - 1]["id"].ToString());
                                break;
                            }
                        }

                        if (active)
                            sb.Append("<div class=\"menuLayer active\">"); // 開啟：menuLayer active；關閉：menuLayer
                        else
                            sb.Append("<div class=\"menuLayer\">"); // 開啟：menuLayer active；關閉：menuLayer

                        if (active)
                        {
                            sb.AppendFormat("<div id=\"menuLbn{1}\" class=\"menuLbn\" style=\"color:#FFFFFF;background-image: url('images/menu_item_active.png'),url('../images/menu_item_active.png');\">{0}</div>", dt_MenuLayer.Rows[i - 1]["name"].ToString(), i.ToString());
                            sb.AppendFormat("<div id=\"menuPan{0}\" class=\"menu_item_bk\">", i.ToString());
                        }
                        else
                        {
                            sb.AppendFormat("<div id=\"menuLbn{1}\" class=\"menuLbn\" style=\"\">{0}</div>", dt_MenuLayer.Rows[i - 1]["name"].ToString(), i.ToString());
                            sb.AppendFormat("<div id=\"menuPan{0}\" class=\"menu_item_bk\" style=\"display: none;\">", i.ToString());
                        }

                        foreach (DataRow dw in dt_Menu.Rows)
                        {
                            bool SubhlEnable = false;
                            switch (dw["muLocation"].ToString())
                            {
                                case "0":
                                    SubhlEnable = true;
                                    break;
                                case "1":
                                    SubhlEnable = (Request.Url.AbsoluteUri.IndexOf("http://192.168") != -1 || Request.Url.AbsoluteUri.IndexOf("https://192.168") != -1 || Request.Url.AbsoluteUri.IndexOf("http://localhost") != -1) == true ? true : false;
                                    break;
                                case "2":
                                    SubhlEnable = Request.Url.AbsoluteUri.Replace("https://", "http://").IndexOf(new WebSetValue().WebUrl) != -1 ? true : false;
                                    break;
                            }
                            if (SubhlEnable)
                            {
                                Boolean hover = false;
                                for (int j = Request.Url.Segments.Length - 1; j >= 0; j--)
                                {
                                    if (DataMenu.PageLimitCheck(Request.Url.Segments[j].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[j].Replace("/", string.Empty) + Request.Url.Query.Split('&')[0], this.Session).Authority.Select)
                                    {
                                        hover = Request.Url.Segments[j].Replace("/", string.Empty).ToLower() + Request.Url.Query.Split('&')[0] == dw["muHyperLink"].ToString().Trim().ToLower();
                                        break;
                                    }
                                    if (DataMenu.PageLimitCheck(Request.Url.Segments[j].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[j].Replace("/", string.Empty), this.Session).Authority.Select)
                                    {
                                        hover = Request.Url.Segments[j].Replace("/", string.Empty).ToLower() == dw["muHyperLink"].ToString().Trim().ToLower();
                                        break;
                                    }
                                }
                                if (dw["muTarget"].ToString().Trim() == "_blank" && bool.Parse(Application["WindowFull"].ToString()))
                                {
                                    if (hover)
                                        sb.AppendFormat("<a class=\"menuSubhl_hover\" href=\"javascript:WindowOpen('{0}','{1}');\">{2}</a>", ResolveUrl(dw["muHyperLink"].ToString().Trim()).Replace("/ascx", string.Empty), dw["muHyperLink"].ToString().Trim().Replace(".", string.Empty).Replace("/", string.Empty).Replace("?", "_").Replace("=", "_"), dw["muIsSub"].ToString() != "True" ? string.Format("&nbsp;&nbsp;{0} {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: -3px;\" />", ResolveUrl("images/menu_li.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()) : string.Format("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}  {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: 4px;\" />", ResolveUrl("images/point4.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()));
                                    else
                                        sb.AppendFormat("<a class=\"menuSubhl\" href=\"javascript:WindowOpen('{0}','{1}');\">{2}</a>", ResolveUrl(dw["muHyperLink"].ToString().Trim()).Replace("/ascx", string.Empty), dw["muHyperLink"].ToString().Trim().Replace(".", string.Empty).Replace("/", string.Empty).Replace("?", "_").Replace("=", "_"), dw["muIsSub"].ToString() != "True" ? string.Format("&nbsp;&nbsp;{0} {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: -3px;\" />", ResolveUrl("images/menu_li.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()) : string.Format("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}  {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: 4px;\" />", ResolveUrl("images/point4.gif").Replace("/ascx", string.Empty)), dw["muName"].ToString()));
                                }
                                else
                                {
                                    if (hover)
                                        sb.AppendFormat("<a class=\"menuSubhl_hover\" href=\"{0}\" target=\"{1}\">{2}</a>", ResolveUrl(dw["muHyperLink"].ToString().Trim()).Replace("/ascx", string.Empty), dw["muTarget"].ToString().Trim(), dw["muIsSub"].ToString() != "True" ? string.Format("&nbsp;&nbsp;{0} {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: -3px;\" />", ResolveUrl("images/menu_li.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()) : string.Format("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}  {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: 4px;\" />", ResolveUrl("images/point4.gif").Replace("/ascx", string.Empty)), dw["muName"].ToString()));
                                    else
                                        sb.AppendFormat("<a class=\"menuSubhl\" href=\"{0}\" target=\"{1}\">{2}</a>", ResolveUrl(dw["muHyperLink"].ToString().Trim()).Replace("/ascx", string.Empty), dw["muTarget"].ToString().Trim(), dw["muIsSub"].ToString() != "True" ? string.Format("&nbsp;&nbsp;{0} {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: -3px;\" />", ResolveUrl("images/menu_li.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()) : string.Format("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{0}  {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: 4px;\" />", ResolveUrl("images/point4.gif").Replace("/ascx", string.Empty)), dw["muName"].ToString()));
                                }
                            }
                            else
                                sb.AppendFormat("<a class=\"menuSubhl_off\">{0}</a>", string.Format("&nbsp;&nbsp;{0} {1}", string.Format("<img src=\"{0}\" border=\"0\" style=\"vertical-align: -3px;\" />", ResolveUrl("images/menu_stop.png").Replace("/ascx", string.Empty)), dw["muName"].ToString()));

                            sb.Append("<div style=\"height:10px;\"></div>");
                        }

                        sb.Append("</div>");

                        sb.Append("<div style=\"height:10px;\"></div>");

                        sb.Append("</div>");
                    }
                }
            }
            Literal_menu.Text = sb.ToString();
        }
        #endregion
    }
}