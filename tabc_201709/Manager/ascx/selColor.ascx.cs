﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class selColor : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txt_color.Text))
                ScriptManager.RegisterStartupScript(this, GetType(), MainControls.random("", 6), string.Format("$('#{0}').css('background', '{1}');", ShowSCB.ClientID, txt_color.Text), true);
            Literal_seltor.Text = selTor();
        }

        private string selTor()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("<div onclick=\"ShowDialog('{1}', 280, 320 , document.all.{0}, '{2}');\" style=\"cursor: pointer\">", txt_color.ClientID, ResolveUrl("selcolor.aspx"), ShowSCB.ClientID);
            sb.AppendFormat("<img src=\"{0}\" align=\"bottom\" />", ResolveUrl("images/bgcolor.gif").Replace("/ascx", string.Empty));
            sb.Append("</div>");

            return sb.ToString();
        }

        public string color
        {
            get
            {
                return txt_color.Text.Trim();
            }
            set
            {
                txt_color.Text = value;
                //if (!string.IsNullOrEmpty(txt_color.Text))
                ScriptManager.RegisterStartupScript(this, GetType(), MainControls.random("", 6), string.Format("$('#{0}').css('background', '{1}');", ShowSCB.ClientID, txt_color.Text), true);
            }
        }
    }
}