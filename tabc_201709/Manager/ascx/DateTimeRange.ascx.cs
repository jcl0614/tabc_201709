﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class DateTimeRange : System.Web.UI.UserControl
    {
        private string fieldName = "";

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void Init()
        {
            txt_sDate.Text = string.Empty;
            txt_eDate.Text = string.Empty;
            ddl_sTime.SelectedIndex = -1;
            ddl_eTime.SelectedIndex = -1;
        }
        public string valid
        {
            get
            {
                StringBuilder sbError = new StringBuilder();
                if (!string.IsNullOrEmpty(txt_sDate.Text.Trim()) && !string.IsNullOrEmpty(txt_eDate.Text.Trim()))
                {
                    if (DateTime.Compare(DateTime.Parse(txt_eDate.Text.Trim()), DateTime.Parse(txt_sDate.Text.Trim())) < 0)
                        sbError.AppendFormat("●「{0}」終止日期不可小於起始日期!", fieldName);
                    else
                    {
                        if (DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", txt_sDate.Text.Trim(), ddl_eTime.SelectedValue)), DateTime.Parse(string.Format("{0} {1}", txt_eDate.Text.Trim(), ddl_sTime.SelectedValue))) < 0)
                            sbError.AppendFormat("●「{0}」結束時間不可小於起始時間！", fieldName);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(txt_sDate.Text.Trim()) && !MainControls.ValidDate(txt_sDate.Text.Trim(), "yyyy/M/d"))
                        sbError.AppendFormat("●「{0}-起始」輸入格式錯誤!", fieldName);
                    if (!string.IsNullOrEmpty(txt_eDate.Text.Trim()) && !MainControls.ValidDate(txt_eDate.Text.Trim(), "yyyy/M/d"))
                        sbError.AppendFormat("●「{0}-終止」輸入格式錯誤!", fieldName);
                }
                
                return sbError.ToString();
            }
            set
            {

            }
        }
        public string FieldName
        {
            get
            {
                return fieldName;
            }
            set
            {
                fieldName = value;
            }
        }
        public string Date_start
        {
            get
            {
                return txt_sDate.Text;
            }
            set
            {
                txt_sDate.Text = value;
            }
        }
        public string Date_end
        {
            get
            {
                return txt_eDate.Text;
            }
            set
            {
                txt_eDate.Text = value;
            }
        }
        public string Time_start
        {
            get
            {
                return ddl_sTime.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_sTime, value);
            }
        }
        public string Time_end
        {
            get
            {
                return ddl_eTime.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_eTime, value);
            }
        }
        public string DateTime_start
        {
            get
            {
                return string.Format("{0} {1}", txt_sDate.Text, ddl_sTime.SelectedValue);
            }
            set
            {
                if (MainControls.ValidDate(value, "yyyy/MM/dd HH:mm:ss"))
                {
                    txt_sDate.Text = DateTime.Parse(value).ToString("yyyy/MM/dd");
                    MainControls.ddlIndexSelectValue(ddl_sTime, DateTime.Parse(value).ToString("HH:mm"));
                }
            }
        }
        public string DateTime_end
        {
            get
            {
                return string.Format("{0} {1}", txt_eDate.Text, ddl_eTime.SelectedValue);
            }
            set
            {
                if (MainControls.ValidDate(value, "yyyy/MM/dd HH:mm:ss"))
                {
                    txt_eDate.Text = DateTime.Parse(value).ToString("yyyy/MM/dd");
                    MainControls.ddlIndexSelectValue(ddl_eTime, DateTime.Parse(value).ToString("HH:mm"));
                }
            }
        }
    }
}