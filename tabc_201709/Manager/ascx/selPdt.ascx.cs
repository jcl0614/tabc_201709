﻿using AppCode.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager.ascx
{
    public partial class selPdt : System.Web.UI.UserControl
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Clear();
            }
        }
        
        private void ddlSetting()
        {
            ViewState["dt_Pdt"] = DDL_InsName(ddl_NO, true, "請選擇投保商品", ddl_Saled.SelectedValue, txtSh_NO.Text.Trim());

            if (ddl_NO.Items.Count != 0)
            {
                ddl_NO.Enabled = true;
                ddl_NO.SelectedIndex = 0;
            }
            else
            {
                ddl_NO.Items.Add(new ListItem("請輸入[險種代碼]或[投保商品關鍵字]", ""));
                ddl_NO.Enabled = false;
            }
        }
        private void setValue()
        {
            DataTable dt = ViewState["dt_Pdt"] as DataTable;
            if (ddl_NO.SelectedValue != "" && dt != null && dt.Rows.Count != 0)
            {
                DataRow dr = (ViewState["dt_Pdt"] as DataTable).Rows[ddl_NO.SelectedIndex - 1];
                if(dr != null)
                {
                    CompanyNo_ = dr["CompanyNo"].ToString();
                    CompanyName_ = dr["CompanyName"].ToString();
                    Mark_ = dr["Mark"].ToString();
                    DispMark_ = dr["DispMark"].ToString();
                    InsName_ = dr["InsName"].ToString();
                    DispClass_ = dr["DispClass"].ToString();
                    Unit_ = dr["Unit"].ToString();
                    Saled_ = bool.Parse(dr["Saled"].ToString());
                    Keyword_ = dr["Keyword"].ToString();
                    Master_ = bool.Parse(dr["Master"].ToString());
                    MinVal_ = int.Parse(dr["MinVal"].ToString());
                    MaxVal_ = int.Parse(dr["MaxVal"].ToString());
                    Channel_ = dr["Channel"].ToString();
                }
            }
        }
        private DataTable getAPI()
        {
            DataTable dt = new DataTable();
            string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "GetInsItemListByCmpy");
            string IP = "192.168.1.1";
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", iStatus, CompanyNo, DateTime.UtcNow.ToString("yyyyMMddHH"), "TABC", IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            string err = "";
            string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
            if (err.Length == 0)
            {
                dt = JsonConvert.DeserializeObject<DataTable>(ret);
            }
            return dt;
        }
        private DataTable DDL_InsName(DropDownList DDL, bool all, string allString, string Saled, string keyword)
        {
            DDL.Items.Clear();
            DataTable dt = getAPI();
            List<DataRow> list = new List<DataRow>();
            if (dt != null)
            {
                list = dt.AsEnumerable().ToList();
                if (Saled.Trim() != "")
                    list = dt.AsEnumerable().Where(x => x.Field<bool>("Saled") == bool.Parse(Saled)).ToList();
                if (keyword.Trim() != "")
                {
                    list = dt.Select(string.Format("InsName Like '%{0}%' OR keyword Like '%{0}%'", keyword)).ToList();
                    //list = dt.AsEnumerable().Where(x => x.Field<string>("InsName").IndexOf(keyword) != -1).ToList();
                    //if(list.Count == 0)
                    //    list = dt.AsEnumerable().Where(x => x.Field<string>("Keyword").IndexOf(keyword) != -1).ToList();
                }
                if (list != null && list.Count != 0)
                {
                    if (all)
                        DDL.Items.Add(new ListItem(allString, ""));
                    foreach (DataRow dr in list)
                    {
                        ListItem listItem = new ListItem(dr["InsName"].ToString(), dr["Mark"].ToString());
                        DDL.Items.Add(listItem);
                    }
                }
            }
            return dt;
        }

        public void search(string key)
        {
            txtSh_NO.Text = key;
            ddlSetting();
        }
        public string SearchText
        {
            get
            {
                return txtSh_NO.Text.Trim();
            }
            set
            {
                txtSh_NO.Text = value.Trim();
            }
        }
        public void Clear()
        {
            txtSh_NO.Text = string.Empty;
            ddl_NO.Items.Clear();

            if (ddl_NO.Items.Count != 0)
                ddl_NO.Enabled = true;
            else
            {
                ddl_NO.Items.Add(new ListItem("請輸入[險種代碼]或[投保商品關鍵字]", ""));
                ddl_NO.Enabled = false;
            }
        }
        public string SelectedValue
        {
            get
            {
                return ddl_NO.SelectedValue;
            }
            set
            {
                MainControls.ddlIndexSelectValue(ddl_NO, value);
            }
        }

        //0:壽險 1:產險
        protected static string iStatus_ = "";
        public string iStatus
        {
            get
            {
                return iStatus_;
            }
            set
            {
                iStatus_ = value;
            }
        }

        protected static string CompanyNo_ = "";
        public string CompanyNo
        {
            get
            {
                return CompanyNo_;
            }
            set
            {
                CompanyNo_ = value;
            }
        }
        protected static string CompanyName_ = "";
        public string CompanyName
        {
            get
            {
                return CompanyName_;
            }
            set
            {
                CompanyName_ = value;
            }
        }
        protected static string Mark_ = "";
        public string Mark
        {
            get
            {
                return Mark_;
            }
            set
            {
                Mark_ = value;
            }
        }
        protected static string DispMark_ = "";
        public string DispMark
        {
            get
            {
                return DispMark_;
            }
            set
            {
                DispMark_ = value;
            }
        }
        protected static string InsName_ = "";
        public string InsName
        {
            get
            {
                return InsName_;
            }
            set
            {
                InsName_ = value;
            }
        }
        protected static string DispClass_ = "";
        public string DispClass
        {
            get
            {
                return DispClass_;
            }
            set
            {
                DispClass_ = value;
            }
        }
        protected static string Unit_ = "";
        public string Unit
        {
            get
            {
                return Unit_;
            }
            set
            {
                Unit_ = value;
            }
        }
        protected static bool Saled_;
        public bool Saled
        {
            get
            {
                return Saled_;
            }
            set
            {
                Saled_ = value;
            }
        }
        protected static string Keyword_ = "";
        public string Keyword
        {
            get
            {
                return Keyword_;
            }
            set
            {
                Keyword_ = value;
            }
        }
        protected static bool Master_;
        public bool Master
        {
            get
            {
                return Master_;
            }
            set
            {
                Master_ = value;
            }
        }
        protected static int MinVal_ = 0;
        public int MinVal
        {
            get
            {
                return MinVal_;
            }
            set
            {
                MinVal_ = value;
            }
        }
        protected static int MaxVal_ = 0;
        public int MaxVal
        {
            get
            {
                return MaxVal_;
            }
            set
            {
                MaxVal_ = value;
            }
        }
        protected static string Channel_ = "";
        public string Channel
        {
            get
            {
                return Channel_;
            }
            set
            {
                Channel_ = value;
            }
        }


        public string SelectedText
        {
            get
            {
                return ddl_NO.SelectedItem.Text;
            }
            set
            {
                ddl_NO.SelectedItem.Text = value;
                MainControls.ddlIndexSelectText(ddl_NO, value);
            }
        }

        public bool Enabled
        {
            get
            {
                return txtSh_NO.Enabled;
            }
            set
            {
                txtSh_NO.Enabled = value;
                ddl_NO.Enabled = value;
                ibtn_uSearch.Enabled = value;
                ImageButton1.Enabled = value;
            }
        }



        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        public delegate void PatientsChangedEventHandler(object sender, EventArgs e);

        public event PatientsChangedEventHandler SelectedIndexChanged;
        protected void ddl_NO_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
                this.SelectedIndexChanged(sender, e);
            setValue();
        }

        public event PatientsChangedEventHandler TextChanged;
        protected void txtSh_NO_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null)
                this.TextChanged(sender, e);
            ddlSetting();
        }

        public event PatientsChangedEventHandler SearchButtonClick;
        protected void ibtn_uSearch_Click(object sender, ImageClickEventArgs e)
        {
            if (this.SearchButtonClick != null)
                this.SearchButtonClick(sender, e);
            ddlSetting();
        }

        public event PatientsChangedEventHandler RefreshButtonClick;
        protected void ibtn_uRefresh_Click(object sender, ImageClickEventArgs e)
        {
            if (this.RefreshButtonClick != null)
                this.RefreshButtonClick(sender, e);
        }

    }
}