﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ETeacher : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddl_ET_TeacherType);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddl_ET_TeacherType);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddl_ET_TeacherType);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataETeacher.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_ET_TeacherType, true, "", "148");
            DataPhrase.DDL_TypeName(ddl_ET_TeacherType, true, "請選擇", "148");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_ET_TeacherType.SelectedValue == "")
                sbError.Append("●「講師類型」必須選取!<br>");
            if (txt_ET_Company.Text.Trim() == "")
                sbError.Append("●「公司名稱」必須輸入!<br>");
            //if (txt_ET_Department.Text.Trim() == "")
            //    sbError.Append("●「部門名稱」必須輸入!<br>");
            if (txt_ET_JobTitle.Text.Trim() == "")
                sbError.Append("●「職稱」必須輸入!<br>");
            //if (txt_ET_TCode.Text.Trim() == "")
            //    sbError.Append("●「講師代碼」必須輸入!<br>");
            if (txt_ET_TCode.Text.Trim() != "")
                if (txt_ET_TCode.Text.Trim() != hif_ET_Code.Value.Trim() && !DataETeacher.valid_ET_TCode(txt_ET_TCode.Text.Trim()))
                    sbError.Append("●「講師代碼」已被使用!<br>");
            if (txt_ET_TName.Text.Trim() == "")
                sbError.Append("●「授課講師」必須輸入!<br>");
            //if (txt_ET_ID.Text.Trim() == "")
            //    sbError.Append("●「身份證號」必須輸入!<br>");
            //else
            //{
            //    if(!MainControls.checkSSN(txt_ET_ID.Text.Trim()))
            //        sbError.Append("●「身份證號」格式錯誤!<br>");
            //}
            //if (txt_ET_Addr.Text.Trim() == "")
            //    sbError.Append("●「地址」必須輸入!<br>");
            //if (txt_ET_Phone.Text.Trim() == "")
            //    sbError.Append("●「聯絡電話」必須輸入!<br>");
            //if (txt_ET_Specialty.Text.Trim() == "")
            //    sbError.Append("●「專長領域」必須輸入!<br>");
            if (txt_ET_Salary.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_ET_Salary.Text.Trim())))
                    sbError.Append("●「時薪」格式錯誤!<br>");
            }
            //if (txt_ET_TSummary.Text.Trim() == "")
            //    sbError.Append("●「講師簡介」必須輸入!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "ET_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_UidNo":
                            hif_ET_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_TeacherType":
                            MainControls.ddlIndexSelectText(ddl_ET_TeacherType, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "ET_Company":
                            txt_ET_Company.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Department":
                            txt_ET_Department.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_JobTitle":
                            txt_ET_JobTitle.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_TCode":
                            txt_ET_TCode.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selPerson_AccID_ET_TCode.search(txt_ET_TCode.Text);
                            selPerson_AccID_ET_TCode.SelectedValue = txt_ET_TCode.Text;
                            hif_ET_Code.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_TName":
                            txt_ET_TName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_ID":
                            txt_ET_ID.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Addr":
                            txt_ET_Addr.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Phone":
                            txt_ET_Phone.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Specialty":
                            txt_ET_Specialty.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Salary":
                            txt_ET_Salary.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_TSummary":
                            txt_ET_TSummary.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selPerson_AccID_ET_TCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = DataPerson.DataReader_AccID(selPerson_AccID_ET_TCode.SelectedValue);
            if (dr != null)
            {
                txt_ET_TCode.Text = dr["AccID"].ToString();
                txt_ET_JobTitle.Text = dr["PS_Title"].ToString();
                txt_ET_TName.Text = dr["PS_NAME"].ToString();
                txt_ET_ID.Text = dr["PS_ID"].ToString();
                txt_ET_Phone.Text = dr["PS_MOIBLE"].ToString();
            }
        }
        protected void selPerson_AccID_ET_TCode_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_ET_TCode_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_ET_TCode_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_ET_TCode.search(hif_ET_Code.Value);
            selPerson_AccID_ET_TCode.SelectedValue = hif_ET_Code.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacher.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataETeacher.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            //txt_ET_TCode.ReadOnly = true;
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacher.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataETeacher.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        //txt_ET_TCode.ReadOnly = false;
                        txt_ET_TCode.Text = string.Empty;
                        hif_ET_Code.Value = string.Empty;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_ET_UidNo.Value = string.Empty;
                        this.SetFocus(txt_ET_TCode);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請填上「txt_ET_TCode」後進行新增儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacher.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataETeacher.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataETeacher.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ET_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ET_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ET_UidNo", hif_ET_UidNo.Value);
            }
            hsData.Add("ET_TeacherType", ddl_ET_TeacherType.SelectedItem.Text.Trim());
            hsData.Add("ET_Company", txt_ET_Company.Text.Trim());
            hsData.Add("ET_Department", txt_ET_Department.Text.Trim());
            hsData.Add("ET_JobTitle", txt_ET_JobTitle.Text.Trim());
            hsData.Add("ET_TCode", txt_ET_TCode.Text.Trim());
            hsData.Add("ET_TName", txt_ET_TName.Text.Trim());
            hsData.Add("ET_ID", txt_ET_ID.Text.Trim());
            hsData.Add("ET_Addr", txt_ET_Addr.Text.Trim());
            hsData.Add("ET_Phone", txt_ET_Phone.Text.Trim());
            hsData.Add("ET_Specialty", txt_ET_Specialty.Text.Trim());
            hsData.Add("ET_Salary", txt_ET_Salary.Text.Trim());
            hsData.Add("ET_TSummary", txt_ET_TSummary.Text.Trim());
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_ET_TCode.SelectedValue);
            if (dr_sales != null)
            {
                hsData.Add("ET_Area", dr_sales["cZON_TYPE"].ToString());
                hsData.Add("ET_Unit", dr_sales["Zone"].ToString());
            }
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ET_TransNo", TransNo);
            hsData.Add("ET_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_ET_UidNo.Value = string.Empty;
            ddl_ET_TeacherType.SelectedIndex = -1;
            txt_ET_Company.Text = string.Empty;
            txt_ET_Department.Text = string.Empty;
            txt_ET_JobTitle.Text = string.Empty;
            txt_ET_TCode.Text = string.Empty;
            hif_ET_Code.Value = string.Empty;
            selPerson_AccID_ET_TCode.Clear();
            txt_ET_TName.Text = string.Empty;
            txt_ET_ID.Text = string.Empty;
            txt_ET_Addr.Text = string.Empty;
            txt_ET_Phone.Text = string.Empty;
            txt_ET_Specialty.Text = string.Empty;
            txt_ET_Salary.Text = string.Empty;
            txt_ET_TSummary.Text = string.Empty;

            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    txt_ET_TCode.ReadOnly = true;
                    this.SetFocus(ddl_ET_TeacherType);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    txt_ET_TCode.ReadOnly = false;
                    this.SetFocus(ddl_ET_TeacherType);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[6].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[7].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[6].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[6].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacher.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[7].Text, GridView1.Rows[i].Cells[8].Text)));

                    //RServiceProvider rsp = DataETeacher.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataETeacher.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("ET_Company")) return;
            e.InputParameters.Add("ET_Company", Lib.FdVP(txtSh_ET_Company.Text).Trim());
            e.InputParameters.Add("ET_TCode", Lib.FdVP(txtSh_ET_TCode.Text).Trim());
            e.InputParameters.Add("ET_TName", Lib.FdVP(txtSh_ET_TName.Text).Trim());
            e.InputParameters.Add("ET_Phone", Lib.FdVP(txtSh_ET_Phone.Text).Trim());
            e.InputParameters.Add("ET_Specialty", Lib.FdVP(txtSh_ET_Specialty.Text).Trim());
            e.InputParameters.Add("ET_TSummary", Lib.FdVP(txtSh_ET_TSummary.Text).Trim());
            e.InputParameters.Add("ET_TeacherType", ddlSh_ET_TeacherType.SelectedItem.Text.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_ET_Company.Text = string.Empty;
            txtSh_ET_TCode.Text = string.Empty;
            txtSh_ET_TName.Text = string.Empty;
            txtSh_ET_Phone.Text = string.Empty;
            txtSh_ET_Specialty.Text = string.Empty;
            txtSh_ET_TSummary.Text = string.Empty;
            ddlSh_ET_TeacherType.SelectedIndex = -1;

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion
    }
}