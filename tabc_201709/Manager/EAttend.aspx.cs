﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EAttend : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txt_EP_Bank);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_EP_Bank);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_EP_Bank);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 800);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEAttend.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddl_ED_EatHabits, true, "請選擇", "124");
            DataPhrase.DDL_TypeName(ddl_ED_Transport, true, "請選擇", "125");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selCourse_EC_TransNo.SelectedValue.Trim() == "")
                sbError.Append("●「課程名稱」必須選取!<br>");
            if (selPerson_AccID_ED_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「人員」必須選取!<br>");
            if (selCourse_EC_TransNo.SelectedValue.Trim() != "" && selPerson_AccID_ED_AccID.SelectedValue.Trim() != "")
                if (commandName == "Append" && !DataEAttend.valid_ED(selCourse_EC_TransNo.SelectedValue, selPerson_AccID_ED_AccID.SelectedValue))
                    sbError.AppendFormat("●「人員」{0}已存在對應課程資料! ", selPerson_AccID_ED_AccID.SelectedText);
            //if (txt_EP_Bank.Text.Trim() == "")
            //    sbError.Append("●「轉出銀行」必須輸入!<br>");
            if (txt_EP_Account.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\d{5}$", (txt_EP_Account.Text.Trim())))
                    sbError.Append("●「轉出帳號末5碼」格式錯誤!<br>");
            }
            if (txt_EP_TransAmt.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EP_TransAmt.Text.Trim())))
                    sbError.Append("●「轉帳金額」格式錯誤!<br>");
            }
            
            if (txt_EP_TransDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_EP_TransDate.Text.Trim()))
                    sbError.Append("●「轉帳日期」格式錯誤!<br>");
            }
            if (TimeRangeAll_EP_TransTime.Time_start != "" && TimeRangeAll_EP_TransTime.Time_end != "")
            {
                TimeRangeAll_EP_TransTime.FieldName = "轉帳時間";
                if (TimeRangeAll_EP_TransTime.valid != "")
                    sbError.Append(TimeRangeAll_EP_TransTime.valid);
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "ED_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_EP = DataEPayment.DataReader_ED_TransNo(hif_TransNo.Value);
                            if (dr_EP !=null)
                            {
                                hif_EP_TransNo.Value = dr_EP["EP_TransNo"].ToString();
                                txt_EP_Bank.Text = dr_EP["EP_Bank"].ToString();
                                txt_EP_Account.Text = dr_EP["EP_Account"].ToString();
                                txt_EP_TransAmt.Text = dr_EP["EP_TransAmt"].ToString();
                                if (dr_EP["EP_TransDate"].ToString() != "")
                                    txt_EP_TransDate.Text = DateTime.Parse(dr_EP["EP_TransDate"].ToString()).ToString("yyyy/MM/dd");
                                TimeRangeAll_EP_TransTime.Time_start = dr_EP["EP_TransTimeS"].ToString();
                                TimeRangeAll_EP_TransTime.Time_end = dr_EP["EP_TransTimeE"].ToString();
                            }
                            else
                                hif_EP_TransNo.Value = System.Guid.NewGuid().ToString("N");
                            break;
                        case "ED_UidNo":
                            hif_ED_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_SDate_Str":
                            lbl_EC_SDateTime.Text = gvr.Cells[i].Text;
                            break;
                        case "EC_TransNo":
                            hif_EC_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selCourse_EC_TransNo.EC_Category = "2";
                            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
                            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
                            selCourse_EC_TransNo.SearchText = string.Empty;
                            break;
                        case "ED_AccID":
                            selPerson_AccID_ED_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_ED_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_ED_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ED_EatHabits":
                            MainControls.ddlIndexSelectValue(ddl_ED_EatHabits, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "ED_Transport":
                            MainControls.ddlIndexSelectValue(ddl_ED_Transport, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selCourse_EC_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selCourse_EC_TransNo_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selCourse_EC_TransNo_SearchButtonClick(object sender, EventArgs e)
        {
            selCourse_EC_TransNo.EC_Category = "2";
        }
        protected void selCourse_EC_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
        }
        protected void selPerson_AccID_ED_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_ED_AccID_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_ED_AccID_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selPerson_AccID_ED_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_ED_AccID.search(hif_ED_AccID.Value);
            selPerson_AccID_ED_AccID.SelectedValue = hif_ED_AccID.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_EP = new DataSet();
            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hif_EP_TransNo.Value = System.Guid.NewGuid().ToString("N");
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                            RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                            if (rsp_EP.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "新增成功...";
                                maintainButtonEnabled("Select");

                                return;
                            }
                            else
                                lbl_Msg.Text = rsp_EP.ReturnMessage;
                            
                            
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            RServiceProvider rsp_EP = new RServiceProvider();
                            if (DataEPayment.valid_ED_TransNo(hif_TransNo.Value))
                            {
                                ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                                rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                            }
                            else
                            {
                                ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("M")));
                                rsp_EP = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds_EP);
                            }
                            if (rsp_EP.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "修改成功...";
                                return;
                            }
                            else
                                lbl_Msg.Text = rsp_EP.ReturnMessage;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEAttend.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("D")));
                            RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                            if (rsp_EP.Result) //執行成功
                            {
                                GridView1.DataBind();
                                lbl_Msg.Text = "刪除成功...";
                                maintainButtonEnabled("");
                                clearControlContext();
                                return;
                            }
                            else
                                lbl_Msg.Text = rsp_EP.ReturnMessage;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ED_UidNo", hif_ED_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                hsData.Add("ED_Code", dr_course["EC_Code"].ToString());
                hsData.Add("ED_CName", dr_course["EC_CName"].ToString());
                hsData.Add("ED_Mode", dr_course["EC_Mode"].ToString());
                hsData.Add("ED_Category", dr_course["EC_Category"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_ED_AccID.SelectedValue);
            if (dr_person != null)
            {
                hsData.Add("ED_AccID", dr_person["AccID"].ToString());
                hsData.Add("ED_ID", dr_person["PS_ID"].ToString());
                hsData.Add("ED_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("ED_EatHabits", ddl_ED_EatHabits.SelectedValue);
            hsData.Add("ED_Transport", ddl_ED_Transport.SelectedValue);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", TransNo);
            hsData.Add("ED_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EP(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", hif_EP_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EP_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EP_UidNo", hif_ED_UidNo.Value);
            }
            hsData.Add("ED_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ED_UidNo", hif_ED_UidNo.Value);
            }
            hsData.Add("ED_Category", "2");
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_ED_AccID.SelectedValue);
            if (dr_person != null)
            {
                hsData.Add("EP_AccID", dr_person["AccID"].ToString());
                hsData.Add("EP_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EP_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("EP_Bank", txt_EP_Bank.Text.Trim());
            hsData.Add("EP_Account", txt_EP_Account.Text.Trim());
            hsData.Add("EP_TransAmt", txt_EP_TransAmt.Text.Trim());
            hsData.Add("EP_TransDate", txt_EP_TransDate.Text.Trim());
            hsData.Add("EP_TransTimeS", TimeRangeAll_EP_TransTime.Time_start);
            hsData.Add("EP_TransTimeE", TimeRangeAll_EP_TransTime.Time_end);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EP_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", TransNo);
            hsData.Add("EP_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_ED_UidNo.Value = string.Empty;
            selCourse_EC_TransNo.Clear();
            selPerson_AccID_ED_AccID.Clear();
            hif_EP_TransNo.Value = string.Empty;
            txt_EP_Bank.Text = string.Empty;
            txt_EP_Account.Text = string.Empty;
            txt_EP_TransAmt.Text = string.Empty;
            txt_EP_TransDate.Text = string.Empty;
            TimeRangeAll_EP_TransTime.Init();
            hif_EC_TransNo.Value = string.Empty;
            hif_ED_AccID.Value = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            lnkExcelSet.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    lnkExcelSet.Visible = true;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EP_Bank);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EP_Bank);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[6].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[7].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[6].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[6].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[7].Text, GridView1.Rows[i].Cells[8].Text)));

                    //RServiceProvider rsp = DataEAttend.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        DataRow dr_EP = DataEPayment.DataReader_ED_TransNo(GridView1.Rows[i].Cells[7].Text);
                        if (dr_EP != null)
                        {
                            DataSet ds_EP = new DataSet();
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP_dlete(dr_EP["EP_TransNo"].ToString(), dr_EP["EP_UidNo"].ToString())));
                            RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                        }
                        count_sucess += 1;
                    }
                    else
                        count_faile += 1; ;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EC_CName")) return;
            e.InputParameters.Add("EC_CName", Lib.FdVP(txtSh_EC_CName.Text).Trim());
            e.InputParameters.Add("EC_Code", Lib.FdVP(txtSh_EC_Code.Text).Trim());
            e.InputParameters.Add("PS_NAME", Lib.FdVP(txtSh_PS_NAME.Text).Trim());
            e.InputParameters.Add("EP_Bank", Lib.FdVP(txtSh_EP_Bank.Text).Trim());
            e.InputParameters.Add("EC_SDate_s", DateRangeSh_EC_SDate.Date_start);
            e.InputParameters.Add("EC_SDate_e", DateRangeSh_EC_SDate.Date_end);
            e.InputParameters.Add("ED_Category", "2");
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EC_CName.Text = string.Empty;
            txtSh_EC_Code.Text = string.Empty;
            txtSh_PS_NAME.Text = string.Empty;
            txtSh_EP_Bank.Text = string.Empty;
            DateRangeSh_EC_SDate.Init();

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            DataTable dt = DataEAttend.DataReader(
                Lib.FdVP(txtSh_EC_CName.Text).Trim(),
                Lib.FdVP(txtSh_EC_Code.Text).Trim(),
                Lib.FdVP(txtSh_PS_NAME.Text).Trim(),
                Lib.FdVP(txtSh_EP_Bank.Text).Trim(),
                DateRangeSh_EC_SDate.Date_start,
                DateRangeSh_EC_SDate.Date_end,
                "2",
                0, 99999
                ).Tables[0];
            
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("課程代碼", typeof(string));
                dt_export.Columns.Add("課程名稱", typeof(string));
                dt_export.Columns.Add("業務員編號", typeof(string));
                dt_export.Columns.Add("姓名", typeof(string));
                dt_export.Columns.Add("飲食習慣", typeof(string));
                dt_export.Columns.Add("交通方式", typeof(string));
                dt_export.Columns.Add("歸屬地區", typeof(string));
                dt_export.Columns.Add("歸屬單位", typeof(string));
                dt_export.Columns.Add("歸屬處經理", typeof(string));

                foreach(DataRow dr in dt.Rows)
                {
                    string cZON_TYPE = "";
                    string cZON_NAME = "";
                    string DM_NAME = "";
                    DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["ED_AccID"].ToString());
                    if (dr_sales != null)
                    {
                        cZON_TYPE = dr_sales["cZON_TYPE"].ToString();
                        cZON_NAME = dr_sales["cZON_NAME"].ToString();
                        DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                        if (dr_sales2 != null)
                        {
                            DM_NAME = dr_sales2["NAME"].ToString();
                        }
                    }
                    dt_export.Rows.Add(
                        dr["ED_Code"].ToString(), 
                        dr["ED_CName"].ToString(), 
                        dr["ED_AccID"].ToString(),
                        dr["ED_Name"].ToString(),
                        dr["ED_EatHabits"].ToString(),
                        dr["ED_Transport"].ToString(),
                        cZON_TYPE,
                        cZON_NAME,
                        DM_NAME
                        );
                }

                ExportControls.CreateCSVFile(Page, dt_export, string.Format("報名資料_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
            }
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }
    }
}