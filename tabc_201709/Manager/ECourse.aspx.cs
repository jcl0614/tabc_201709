﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ECourse : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EC_CName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txtSh_EC_CName);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txtSh_EC_CName);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataECourse.DataColumn(this.GridView1, "1");
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EC_Type, true, "", "E001");
            DataPhrase.DDL_TypeName(ddl_EC_Type, true, "請選擇", "E001");
            DataPhrase.DDL_TypeName(ddlSh_EC_Rating, true, "", "E002");
            DataPhrase.DDL_TypeName(ddl_EC_Rating, true, "請選擇", "E002");
            DataPhrase.DDL_TypeName(ddl_EC_Annual, true, "請選擇", "149");
            DataPhrase.DDL_TypeName(ddl_EC_TrainType, true, "請選擇", "128");
            DataPhrase.DDL_TypeName(ddl_EC_FeeWay, true, "請選擇", "123");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            //if (txt_EC_Code.Text.Trim() == "")
            //    sbError.Append("●「課程代碼」必須輸入!<br>");
            if (txt_EC_Code.Text.Trim() != "")
                if (txt_EC_Code.Text.Trim() != hif_EC_Code.Value.Trim() && !DataECourse.valid_EC_Code(txt_EC_Code.Text.Trim()))
                    sbError.Append("●「課程代碼」已被使用!<br>");
            if (ddl_EC_Type.SelectedValue == "")
                sbError.Append("●「學院」必須選取!<br>");
            if (txt_EC_CName.Text.Trim() == "")
                sbError.Append("●「課程名稱」必須輸入!<br>");
            if (selTeacher_ET_TName.SelectedValue.Trim() == "")
                sbError.Append("●「授課講師」必須選取!<br>");
            if (txt_EC_Summary.Text.Trim() == "")
                sbError.Append("●「課程簡介」必須輸入!<br>");
            if (ddl_EC_Annual.SelectedValue == "")
                sbError.Append("●「應訓練年度」必須選取!<br>");
            if (ddl_EC_Rating.SelectedValue == "")
                sbError.Append("●「分級」必須選取!<br>");
            if (ddl_EC_TrainType.SelectedValue == "")
                sbError.Append("●「訓練通報」必須選取!<br>");
            if (ddl_EC_FeeWay.SelectedValue == "")
                sbError.Append("●「收費方式」必須選取!<br>");
            if (txt_EC_FeeAmt.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_FeeAmt.Text.Trim())))
                    sbError.Append("●「收費金額」格式錯誤!<br>");
            }
            if (txt_EC_FeePoints.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_FeePoints.Text.Trim())))
                    sbError.Append("●「收費點數」格式錯誤!<br>");
            }
            if (txt_EC_QuizNo.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_QuizNo.Text.Trim())))
                    sbError.Append("●「測驗題數」格式錯誤!<br>");
                else
                {
                    if (rbl_EC_IsExam.SelectedValue == "Y")
                    {
                        if (int.Parse(txt_EC_QuizNo.Text.Trim()) <= 0)
                            sbError.Append("●「測驗題數」必須大於0!<br>");
                        else
                        {
                            if (100 % int.Parse(txt_EC_QuizNo.Text.Trim()) != 0)
                                sbError.Append("●「測驗題數」必須可以被100整除!<br>");
                        }
                    }
                }
            }
            if (txt_EC_PassScore.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_PassScore.Text.Trim())))
                    sbError.Append("●「成績限制」格式錯誤!<br>");
            }
            if (txt_EC_ExamTime.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_ExamTime.Text.Trim())))
                    sbError.Append("●「測驗時間限制」格式錯誤!<br>");
            }
            if (txt_EC_PassHours.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_PassHours.Text.Trim())))
                    sbError.Append("●「通報時數」格式錯誤!<br>");
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EC_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_UidNo":
                            hif_EC_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_Code":
                            txt_EC_Code.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_EC_Code.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_Type":
                            MainControls.ddlIndexSelectText(ddl_EC_Type, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_CName":
                            txt_EC_CName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "tblETeacher_ET_TName":
                            hif_tblETeacher_ET_TName.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selTeacher_ET_TName.search(hif_tblETeacher_ET_TName.Value);
                            
                            break;
                        case "tblETeacher_ET_TransNo":
                            hif_tblETeacher_ET_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selTeacher_ET_TName.SelectedValue = hif_tblETeacher_ET_TransNo.Value;
                            DataRow dr = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
                            if (dr != null)
                            {
                                txt_ET_TSummary.Text = dr["ET_TSummary"].ToString();
                            }
                            break;
                        case "EC_IsExam":
                            MainControls.ddlIndexSelectValue(rbl_EC_IsExam, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_IsForce":
                            MainControls.ddlIndexSelectValue(rbl_EC_IsForce, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_Annual":
                            MainControls.ddlIndexSelectValue(ddl_EC_Annual, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_Summary":
                            txt_EC_Summary.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_Memo":
                            txt_EC_Memo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_QuizNotify":
                            txt_EC_QuizNotify.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_Rating":
                            MainControls.ddlIndexSelectText(ddl_EC_Rating, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_TrainType":
                            MainControls.ddlIndexSelectValue(ddl_EC_TrainType, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_FeeWay":
                            MainControls.ddlIndexSelectText(ddl_EC_FeeWay, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EC_FeeAmt":
                            txt_EC_FeeAmt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_FeePoints":
                            txt_EC_FeePoints.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_QuizNo":
                            txt_EC_QuizNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_PassScore":
                            txt_EC_PassScore.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_ExamTime":
                            txt_EC_ExamTime.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_PassHours":
                            txt_EC_PassHours.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_IsAtt":
                            cbox_EC_IsAtt.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "Y" ? true : false;
                            break;
                        case "EC_IsHours":
                            cbox_EC_IsHours.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "Y" ? true : false;
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selTeacher_ET_TName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
            if (dr != null)
            {
                txt_ET_TSummary.Text = dr["ET_TSummary"].ToString();
            }
        }
        protected void selTeacher_ET_TName_TextChanged(object sender, EventArgs e)
        {
            txt_ET_TSummary.Text = string.Empty;
        }
        protected void selTeacher_ET_TName_SearchButtonClick(object sender, EventArgs e)
        {
            txt_ET_TSummary.Text = string.Empty;
        }
        protected void selTeacher_ET_TName_RefreshButtonClick(object sender, EventArgs e)
        {
            selTeacher_ET_TName.search(hif_tblETeacher_ET_TName.Value);
            selTeacher_ET_TName.SelectedValue = hif_tblETeacher_ET_TransNo.Value;
            DataRow dr = DataETeacher.DataReader_ET_TransNo(hif_tblETeacher_ET_TransNo.Value);
            if (dr != null)
            {
                txt_ET_TSummary.Text = dr["ET_TSummary"].ToString();
            }
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataECourse.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            //txt_EC_Code.ReadOnly = true;
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataECourse.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        //txt_EC_Code.ReadOnly = false;
                        txt_EC_Code.Text = string.Empty;
                        hif_EC_Code.Value = string.Empty;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EC_UidNo.Value = string.Empty;
                        this.SetFocus(txt_EC_Code);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請填上「課程代碼」後進行新增儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataECourse.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataECourse.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EC_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EC_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EC_UidNo", hif_EC_UidNo.Value);
            }
            DataRow dr_teacher = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
            if (dr_teacher != null)
            {
                hsData.Add("ET_TransNo", dr_teacher["ET_TransNo"].ToString());
                hsData.Add("ET_UidNo", dr_teacher["ET_UidNo"].ToString());
                hsData.Add("E5_TransNo", dr_teacher["E5_TransNo"].ToString());
                hsData.Add("E5_UidNo", dr_teacher["E5_UidNo"].ToString());
            }
            hsData.Add("EC_Code", txt_EC_Code.Text.Trim());
            if(txt_EC_Code.Text.Trim() != "" && txt_EC_Code.Text.Trim().Length >= 4)
            {
                if (MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EC_Code.Text.Trim().Substring(0, 4))))
                {
                    hsData.Add("EC_Year", txt_EC_Code.Text.Trim().Substring(0, 4));
                }
            }
            hsData.Add("EC_Category", "1");
            hsData.Add("EC_Type", ddl_EC_Type.SelectedItem.Text.Trim());
            hsData.Add("EC_CName", txt_EC_CName.Text.Trim());
            hsData.Add("EC_IsExam", rbl_EC_IsExam.SelectedValue.Trim());
            hsData.Add("EC_IsForce", rbl_EC_IsForce.SelectedValue.Trim());
            if(rbl_EC_IsForce.SelectedValue == "Y")
                hsData.Add("EC_Mode", "2");
            else
                hsData.Add("EC_Mode", null);
            hsData.Add("EC_Annual", ddl_EC_Annual.SelectedValue.Trim());
            hsData.Add("EC_Summary", txt_EC_Summary.Text.Trim());
            hsData.Add("EC_Memo", txt_EC_Memo.Text.Trim());
            hsData.Add("EC_QuizNotify", txt_EC_QuizNotify.Text.Trim());
            hsData.Add("EC_Rating", ddl_EC_Rating.SelectedItem.Text.Trim());
            hsData.Add("EC_TrainType", ddl_EC_TrainType.SelectedValue.Trim());
            hsData.Add("EC_FeeWay", ddl_EC_FeeWay.SelectedItem.Text.Trim());
            hsData.Add("EC_FeePoints", txt_EC_FeePoints.Text.Trim());
            hsData.Add("EC_FeeAmt", txt_EC_FeeAmt.Text.Trim());
            hsData.Add("EC_QuizNo", txt_EC_QuizNo.Text.Trim());
            hsData.Add("EC_PassScore", txt_EC_PassScore.Text.Trim());
            hsData.Add("EC_ExamTime", txt_EC_ExamTime.Text.Trim());
            hsData.Add("EC_PassHours", txt_EC_PassHours.Text.Trim());
            hsData.Add("IsHours", cbox_EC_IsHours.Checked == true ? "Y" : "N");
            hsData.Add("EC_IsAtt", cbox_EC_IsAtt.Checked == true ? "Y" : "N");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EC_TransNo", TransNo);
            hsData.Add("EC_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            //txt_EC_Code.ReadOnly = false;
            hif_TransNo.Value = string.Empty;
            hif_EC_UidNo.Value = string.Empty;
            txt_EC_Code.Text = string.Empty;
            hif_EC_Code.Value = string.Empty;
            ddl_EC_Type.SelectedIndex = -1;
            txt_EC_CName.Text = string.Empty;
            selTeacher_ET_TName.Clear();
            txt_ET_TSummary.Text = string.Empty;
            MainControls.ddlIndexSelectValue(rbl_EC_IsExam, "N");
            MainControls.ddlIndexSelectValue(rbl_EC_IsForce, "N");
            ddl_EC_Annual.SelectedIndex = -1;
            txt_EC_Summary.Text = string.Empty;
            txt_EC_Memo.Text = string.Empty;
            txt_EC_QuizNotify.Text = string.Empty;
            ddl_EC_Rating.SelectedIndex = -1;
            ddl_EC_TrainType.SelectedIndex = -1;
            ddl_EC_FeeWay.SelectedIndex = -1;
            txt_EC_FeePoints.Text = string.Empty;
            txt_EC_QuizNo.Text = "20";
            txt_EC_PassScore.Text = string.Empty;
            txt_EC_ExamTime.Text = string.Empty;
            txt_EC_PassHours.Text = string.Empty;
            hif_tblETeacher_ET_TName.Value = string.Empty;
            hif_tblETeacher_ET_TransNo.Value = string.Empty;
            cbox_EC_IsAtt.Checked = false;
            cbox_EC_IsHours.Checked = false;

            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    //txt_EC_Code.ReadOnly = true;
                    this.SetFocus(txt_EC_CName);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    //txt_EC_Code.ReadOnly = false;
                    this.SetFocus(txt_EC_Code);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[7].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[8].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[7].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[7].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[7].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[7].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataECourse.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[8].Text, GridView1.Rows[i].Cells[9].Text)));

                    //RServiceProvider rsp = DataECourse.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataECourse.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1; ;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EC_CName")) return;
            e.InputParameters.Add("EC_CName", Lib.FdVP(txtSh_EC_CName.Text).Trim());
            e.InputParameters.Add("EC_Type", ddlSh_EC_Type.SelectedItem.Text);
            e.InputParameters.Add("EC_Rating", ddlSh_EC_Rating.SelectedItem.Text);
            e.InputParameters.Add("EC_Category", "1");
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EC_CName.Text = string.Empty;
            ddlSh_EC_Type.SelectedIndex = -1;
            ddlSh_EC_Rating.SelectedIndex = -1;

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        
    }
}