﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ClerkECourse : BasePage
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));
        private static string pageTitle = "";

        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
            if (Request.QueryString["c"] != null)
                QuesInit();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EC_CName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                pageTitle = lbl_PageTitle.Text;
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1100);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                if (Request.QueryString["id"] != null)
                {
                    DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        ViewState["dt_course"] = dr_course.Table;
                        if (Request.QueryString["c"] != null)
                        {
                            switch (Request.QueryString["c"])
                            {
                                case "1":

                                    ChangeMultiView("1");
                                    break;
                                case "2":

                                    ChangeMultiView("2");
                                    break;
                                case "3":

                                    ChangeMultiView("3");
                                    break;
                                case "4":

                                    ChangeMultiView("4");
                                    break;
                                case "5":

                                    ChangeMultiView("5");
                                    break;
                                case "6":

                                    ChangeMultiView("6");
                                    break;
                                case "7":

                                    ChangeMultiView("7");
                                    break;
                                case "8":

                                    ChangeMultiView("8");
                                    break;
                                case "9":

                                    ChangeMultiView("9");
                                    break;
                                default:
                                    ChangeMultiView("0");
                                    break;
                            }
                        }
                        else
                            ChangeMultiView("0");
                    }
                    else
                        MainControls.showMsg(this, "課程不存在！");
                }
                else
                    ChangeMultiView("0");
                
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataECourse.DataColumn_clerk(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EC_Type, true, "", "E001");
            DataPhrase.DDL_TypeName(ddlSh_EC_Rating, true, "", "E002");
            DataPhrase.DDL_TypeName(ddlSh_EC_Mode, true, "", "130");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            
            

            return sbError.ToString();
        }
        private string dataValid_batch()
        {
            StringBuilder sbError = new StringBuilder();



            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_EP = new DataSet();
            DataRow dr_course = null;
            switch (e.CommandName)
            {
                case "AttendPersonal":
                    {
                        lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        if (ViewState["dt_course"] != null)
                            dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                        else
                            dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                        if (dr_course != null)
                        {
                            ViewState["dt_course"] = dr_course.Table;
                            if (DataEAttend.valid_ED(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString()))
                            {
                                hif_ED_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                //元件資料轉成Hashtable
                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED("A")));

                                RServiceProvider rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds);
                                if (rsp.Result) //執行成功
                                {
                                    hif_EP_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                    ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                                    RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                                    if (rsp_EP.Result) //執行成功
                                    {
                                        MainControls.showMsg(this, "報名完成！");
                                        lbtn_DeletePersonal.Visible = true;
                                        return;
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, "報名繳費資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                        logger.Error(string.Format("實體課程-個人報名(繳費登錄資料)[新增]失敗：\r\n", rsp_EP.ReturnMessage));
                                        logger_db.Error(string.Format("實體課程-個人報名(繳費登錄資料)[新增]失敗：\r\n", rsp_EP.ReturnMessage));
                                    }
                                }
                                else
                                {
                                    MainControls.showMsg(this, "報名失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                    logger.Error(string.Format("實體課程-個人報名[新增]失敗：\r\n", rsp.ReturnMessage));
                                    logger_db.Error(string.Format("實體課程-個人報名[新增]失敗：\r\n", rsp.ReturnMessage));
                                }
                            }
                            else
                            {
                                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                                if (lbl_Msg.Text.Trim().Length > 0) return;
                                //元件資料轉成Hashtable
                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED("M")));

                                RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                                if (rsp.Result) //執行成功
                                {
                                    RServiceProvider rsp_EP = new RServiceProvider();
                                    if (DataEPayment.valid_ED_TransNo(hif_ED_TransNo.Value))
                                    {
                                        ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                                        rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                                    }
                                    else
                                    {
                                        ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("M")));
                                        rsp_EP = DataEPayment.Update(sysValue.emNo, sysValue.ProgId, ds_EP);
                                    }
                                    if (rsp_EP.Result) //執行成功
                                    {
                                        MainControls.showMsg(this, "報名完成！");
                                        return;
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, "報名繳費資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                        logger.Error(string.Format("實體課程-個人報名(繳費登錄資料)[更新]失敗：\r\n", rsp_EP.ReturnMessage));
                                        logger_db.Error(string.Format("實體課程-個人報名(繳費登錄資料)[更新]失敗：\r\n", rsp_EP.ReturnMessage));
                                    }
                                }
                                else
                                {
                                    MainControls.showMsg(this, "報名失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                    logger.Error(string.Format("實體課程-個人報名[更新]失敗：\r\n", rsp.ReturnMessage));
                                    logger_db.Error(string.Format("實體課程-個人報名[更新]失敗：\r\n", rsp.ReturnMessage));
                                }
                            }
                        }
                    }
                    break;
                case "DeletePersonal":
                    {
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED("D")));

                        //RServiceProvider rsp = DataEAttend.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("M")));
                            RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                            if (rsp_EP.Result) //執行成功
                            {
                                MainControls.showMsg(this, "報名刪除完成！");
                                rbl_ED_EatHabits.SelectedIndex = -1;
                                rbl_ED_Transport.SelectedIndex = -1;
                                txt_EP_Bank.Text = string.Empty;
                                txt_EP_Account.Text = string.Empty;
                                txt_EP_TransAmt.Text = string.Empty;
                                txt_EP_TransDate.Text = string.Empty; ;
                                TimeRangeAll_EP_TransTime.Init();
                                lbtn_DeletePersonal.Visible = false;
                                return;
                            }
                            else
                            {
                                MainControls.showMsg(this, "報名繳費資料刪除失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                logger.Error(string.Format("實體課程-個人報名(繳費登錄資料)[刪除]失敗：\r\n", rsp_EP.ReturnMessage));
                                logger_db.Error(string.Format("實體課程-個人報名(繳費登錄資料)[刪除]失敗：\r\n", rsp_EP.ReturnMessage));
                            }
                        }
                        else
                        {
                            MainControls.showMsg(this, "報名資料刪除失敗！\\n請稍後再試，或聯絡相關處理人員。");
                            logger.Error(string.Format("實體課程-個人報名[刪除]失敗：\r\n", rsp.ReturnMessage));
                            logger_db.Error(string.Format("實體課程-個人報名[刪除]失敗：\r\n", rsp.ReturnMessage));
                        }
                    }
                    break;
                case "PrintExam":
                    {

                    }
                    break;

            }

            
        }

        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable_ED(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", hif_ED_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ED_UidNo", hif_ED_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(hif_EC_TransNo.Value);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                hsData.Add("ED_Code", dr_course["EC_Code"].ToString());
                hsData.Add("ED_CName", dr_course["EC_CName"].ToString());
                hsData.Add("ED_Category", dr_course["EC_Category"].ToString());
                hsData.Add("ED_ClassHours", dr_course["EC_Hours"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if (dr_person != null)
            {
                hsData.Add("ED_AccID", dr_person["AccID"].ToString());
                hsData.Add("ED_ID", dr_person["PS_ID"].ToString());
                hsData.Add("ED_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("ED_EatHabits", rbl_ED_EatHabits.SelectedValue);
            hsData.Add("ED_Transport", rbl_ED_Transport.SelectedValue);
            hsData.Add("ED_SignDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_ED_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", TransNo);
            hsData.Add("ED_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EP(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", hif_EP_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EP_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EP_UidNo", hif_ED_UidNo.Value);
            }
            hsData.Add("ED_TransNo", hif_ED_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ED_UidNo", hif_ED_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(hif_EC_TransNo.Value);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
            if (dr_person != null)
            {
                hsData.Add("EP_AccID", dr_person["AccID"].ToString());
                hsData.Add("EP_ID", dr_person["PS_ID"].ToString());
                hsData.Add("EP_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("EP_Bank", txt_EP_Bank.Text.Trim());
            hsData.Add("EP_Account", txt_EP_Account.Text.Trim());
            hsData.Add("EP_TransAmt", txt_EP_TransAmt.Text.Trim());
            hsData.Add("EP_TransDate", txt_EP_TransDate.Text.Trim());
            hsData.Add("EP_TransTimeS", TimeRangeAll_EP_TransTime.Time_start);
            hsData.Add("EP_TransTimeE", TimeRangeAll_EP_TransTime.Time_end);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EP_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", TransNo);
            hsData.Add("EP_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_ED_batch(string ED_AccID)
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("ED_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                hsData.Add("ED_Code", dr_course["EC_Code"].ToString());
                hsData.Add("ED_CName", dr_course["EC_CName"].ToString());
                hsData.Add("ED_Category", dr_course["EC_Category"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(ED_AccID);
            if (dr_person != null)
            {
                hsData.Add("ED_AccID", dr_person["AccID"].ToString());
                hsData.Add("ED_ID", dr_person["PS_ID"].ToString());
                hsData.Add("ED_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("ED_EatHabits", rbl_ED_EatHabits0.SelectedValue);
            hsData.Add("ED_Transport", rbl_ED_Transport0.SelectedValue);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_SC_batch(string AccID, string SC_Remark, string SC_StartTime, string SC_EndTime)
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("SC_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("SC_UidNo", Session["UidNo"].ToString());
            hsData.Add("SC_Kind", "業務活動/訓練");
            hsData.Add("SC_Plan", "內部訓練");
            hsData.Add("SC_Remark", SC_Remark);
            hsData.Add("SC_StartTime", SC_StartTime);
            hsData.Add("SC_EndTime", SC_EndTime);
            hsData.Add("AccID", AccID);
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_SC_P_batch(string AccID, string SC_Address, string SC_Remark, string SC_StartTime, string SC_EndTime)
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("SC_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("SC_UidNo", Session["UidNo"].ToString());
            hsData.Add("SC_Address", SC_Address);
            hsData.Add("SC_Kind", "業務活動/訓練");
            hsData.Add("SC_Plan", "內部訓練");
            hsData.Add("SC_Remark", SC_Remark);
            if (SC_StartTime != "")
                hsData.Add("SC_StartTime", SC_StartTime);
            if (SC_EndTime != "")
                hsData.Add("SC_EndTime", SC_EndTime);
            hsData.Add("AccID", AccID);
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_E1_batch(string AccID, string E1_Subject, string E1_Contents, string E1_Sender, string E1_SenderMail, string E1_RecipientMail)
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("E1_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("E1_UidNo", Session["UidNo"].ToString());
            hsData.Add("E1_Type", "3");
            hsData.Add("E1_IsEmail", true);
            hsData.Add("E1_IsSMS", false);
            hsData.Add("E1_Sender", E1_Sender);
            hsData.Add("E1_SenderMail", E1_SenderMail);
            hsData.Add("E1_RecipientMail", E1_RecipientMail);
            hsData.Add("E1_Subject", E1_Subject);
            hsData.Add("E1_Contents", E1_Contents);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_EX(string ModiState, string EX_Score)
        {
            Hashtable hsData = new Hashtable();

            hif_EX_TransNo.Value = System.Guid.NewGuid().ToString("N");

            hsData.Add("EX_TransNo", hif_EX_TransNo.Value);
            hsData.Add("EX_UidNo", Session["UidNo"].ToString());
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }

            hsData.Add("EX_Date",  DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("EX_Score", EX_Score);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_E9(string ModiState,
            string EQ_TransNo, string EQ_UidNo,
            string E9_OrderNo, string E9_Question, string E9_AnsListItem, string E9_Answer,string E9_Score,
            string EA_TransNo1, string EA_UidNo1, string EA_TransNo2, string EA_UidNo2, string EA_TransNo3, string EA_UidNo3, string EA_TransNo4, string EA_UidNo4,
            string E9_IsSelected1, string E9_IsCorrect1, string E9_IsSelected2, string E9_IsCorrect2, string E9_IsSelected3, string E9_IsCorrect3, string E9_IsSelected4, string E9_IsCorrect4
            )
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("E9_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("E9_UidNo", Session["UidNo"].ToString());
            hsData.Add("EX_TransNo", hif_EX_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EX_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EX_UidNo", hif_ED_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }
            hsData.Add("EQ_TransNo", EQ_TransNo);
            hsData.Add("EQ_UidNo", EQ_UidNo);
            hsData.Add("E9_OrderNo", E9_OrderNo);
            hsData.Add("E9_Question", E9_Question);
            hsData.Add("E9_AnsListItem", E9_AnsListItem);
            hsData.Add("E9_Answer", E9_Answer);
            hsData.Add("E9_Score", E9_Score);
            hsData.Add("EA_TransNo1", EA_TransNo1);
            hsData.Add("EA_UidNo1", EA_UidNo1);
            hsData.Add("EA_TransNo2", EA_TransNo2);
            hsData.Add("EA_UidNo2", EA_UidNo2);
            hsData.Add("EA_TransNo3", EA_TransNo3);
            hsData.Add("EA_UidNo3", EA_UidNo3);
            hsData.Add("EA_TransNo4", EA_TransNo4);
            hsData.Add("EA_UidNo4", EA_UidNo4);
            hsData.Add("E9_IsSelected1", E9_IsSelected1 == "1" ? true: false);
            hsData.Add("E9_IsCorrect1", E9_IsCorrect1);
            hsData.Add("E9_IsSelected2", E9_IsSelected2 == "1" ? true : false);
            hsData.Add("E9_IsCorrect2", E9_IsCorrect2);
            hsData.Add("E9_IsSelected3", E9_IsSelected3 == "1" ? true : false);
            hsData.Add("E9_IsCorrect3", E9_IsCorrect3);
            hsData.Add("E9_IsSelected4", E9_IsSelected4 == "1" ? true : false);
            hsData.Add("E9_IsCorrect4", E9_IsCorrect4);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_EL(string EC_TransNo, DataRow dr_EL)
        {
            Hashtable hsData = new Hashtable();

            if (dr_EL == null)
            {
                hsData.Add("EL_TransNo", System.Guid.NewGuid().ToString("N"));
                hsData.Add("EL_UidNo", Session["UidNo"].ToString());
                hsData.Add("EL_Hits", "1");
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("EL_TransNo", dr_EL["EL_TransNo"].ToString());
                hsData.Add("EL_UidNo", dr_EL["EL_UidNo"].ToString());
                hsData.Add("EL_Hits", (int.Parse(dr_EL["EL_Hits"].ToString()) + 1).ToString());
                hsData.Add("ModiState", "M");
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(e.CommandName);
        }

        private void ChangeMultiView(string CommandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataRow dr_course = null;
            switch (CommandName)
            {
                case "1":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "課程資訊");
                    if (ViewState["dt_course"] != null)
                        dr_course = (ViewState["dt_course"] as DataTable).Rows[0];
                        
                    else
                        dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        ViewState["dt_course"] = dr_course.Table;
                        lbl_EC_CName0.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Category.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                        lbl_EC_Type.Text = dr_course["EC_Type"].ToString();
                        lbl_EC_Rating.Text = dr_course["EC_Rating"].ToString();
                        lbl_EC_CName.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Summary.Text = dr_course["EC_Summary"].ToString();
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("128", dr_course["EC_TrainType"].ToString());
                        if (dr_Phrase != null)
                            lbl_EC_TrainType.Text = dr_Phrase["TypeName"].ToString();
                        lbl_EC_PassHours.Text = dr_course["EC_PassHours"].ToString();
                        lbl_EC_FeeWay.Text = dr_course["EC_FeeWay"].ToString();
                        lbl_EC_FeePoints.Text = dr_course["EC_FeePoints"].ToString();
                        lbl_EC_FeeAmt.Text = dr_course["EC_FeeAmt"].ToString();
                        lbl_EC_IsExam.Text = dr_course["EC_IsExam"].ToString() == "Y" ? "是" : "否";
                        lbl_EC_QuizNo.Text = dr_course["EC_QuizNo"].ToString();
                        lbl_EC_PassScore.Text = dr_course["EC_PassScore"].ToString();
                        lbl_EC_Memo.Text = dr_course["EC_Memo"].ToString();
                        DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr_course["ET_TransNo"].ToString());
                        if (dr_ET != null)
                        {
                            lbl_ET_TName.Text = dr_ET["ET_TName"].ToString();
                            lbl_ET_TSummary.Text = dr_ET["ET_TSummary"].ToString();
                        }
                        DataRow dr_Exam = DataETm.DataReader_EC_TransNo(dr_course["EC_TransNo"].ToString());
                        if (dr_Exam != null)
                        {
                            lbl_EM_Type.Text = dr_Exam["EM_Type"].ToString();
                            lbl_EM_FileSize.Text = dr_Exam["EM_FileSize"].ToString();
                            lbl_EM_FileTime.Text = dr_Exam["EM_FileTime"].ToString();
                        }
                    }
                    break;
                case "2":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "課程資訊");
                    if (ViewState["dt_course"] != null)
                        dr_course = (ViewState["dt_course"] as DataTable).Rows[0];
                        
                    else
                        dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        lbl_EC_CName1.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Category0.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                        lbl_EC_Type0.Text = dr_course["EC_Type"].ToString();
                        lbl_EC_Rating0.Text = dr_course["EC_Rating"].ToString();
                        lbl_EC_CName2.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Summary0.Text = dr_course["EC_Summary"].ToString();
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("128", dr_course["EC_TrainType"].ToString());
                        if (dr_Phrase != null)
                            lbl_EC_TrainType0.Text = dr_Phrase["TypeName"].ToString();
                        lbl_EC_PassHours0.Text = dr_course["EC_PassHours"].ToString();
                        lbl_EC_FeeWay0.Text = dr_course["EC_FeeWay"].ToString();
                        lbl_EC_FeePoints0.Text = dr_course["EC_FeePoints"].ToString();
                        lbl_EC_FeeAmt0.Text = dr_course["EC_FeeAmt"].ToString();
                        lbl_EC_IsForce.Text = dr_course["EC_IsForce"].ToString() == "Y" ? "是" : "否";
                        lbl_EC_QuizNo0.Text = dr_course["EC_QuizNo"].ToString();
                        lbl_EC_PassScore0.Text = dr_course["EC_PassScore"].ToString();
                        dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("149", dr_course["EC_Annual"].ToString());
                        if (dr_Phrase != null)
                            lbl_EC_Annual.Text = dr_Phrase["TypeName"].ToString();
                        DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_course["EC_Unit"].ToString());
                        if (dr_Unit != null)
                            lbl_EC_Unit.Text = string.Format("{0}({1})", dr_Unit["cZON_NAME"].ToString(), dr_Unit["cZON_PK"].ToString());
                        lbl_EC_SDate.Text = DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd");
                        lbl_EC_Time.Text = string.Format("{0} ~ {1}", dr_course["EC_STime"].ToString(), dr_course["EC_ETime"].ToString());
                        lbl_EC_Place.Text = dr_course["EC_Place"].ToString();
                        DataRow dr_EC_Moderator = DataPerson.DataReader_AccID(dr_course["EC_Moderator"].ToString());
                        if (dr_EC_Moderator != null)
                            lbl_EC_Moderator.Text = dr_EC_Moderator["PS_NAME"].ToString();
                        lbl_EC_Hours.Text = dr_course["EC_Hours"].ToString();
                        lbl_EC_PNo.Text = dr_course["EC_PNo"].ToString();
                        lbl_EC_Memo0.Text = dr_course["EC_Memo"].ToString();
                        string[] EC_EatHabits = dr_course["EC_EatHabits"].ToString().Split(';');
                        StringBuilder sb_EC_EatHabits = new StringBuilder();
                        foreach (string eatHabits in EC_EatHabits)
                        {
                            if (!string.IsNullOrEmpty(eatHabits))
                            {
                                dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("124", eatHabits);
                                if (dr_Phrase != null)
                                    sb_EC_EatHabits.AppendFormat("{0}；", dr_Phrase["TypeName"].ToString());
                            }
                        }
                        lbl_EC_EatHabits.Text = sb_EC_EatHabits.ToString().TrimEnd('；');
                        string[] EC_Transport = dr_course["EC_Transport"].ToString().Split(';');
                        StringBuilder sb_EC_Transport = new StringBuilder();
                        foreach (string transport in EC_Transport)
                        {
                            if (!string.IsNullOrEmpty(transport))
                            {
                                dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("125", transport);
                                if (dr_Phrase != null)
                                    sb_EC_Transport.AppendFormat("{0}；", dr_Phrase["TypeName"].ToString());
                            }
                        }
                        lbl_EC_Transport.Text = sb_EC_Transport.ToString().TrimEnd('；');
                        DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(dr_course["ET_TransNo"].ToString());
                        if (dr_ET != null)
                        {
                            lbl_ET_TName0.Text = dr_ET["ET_TName"].ToString();
                            lbl_ET_TSummary0.Text = dr_ET["ET_TSummary"].ToString();
                        }
                        DataRow dr_Exam = DataETm.DataReader_EC_TransNo(dr_course["EC_TransNo"].ToString());
                        if (dr_Exam != null)
                        {
                            DataTable dt_Examatt = DataETmatt.dataReader_EM_TransNo(true, dr_Exam["EM_TransNo"].ToString()).Tables[0];
                            StringBuilder sb_EM_File = new StringBuilder();
                            foreach (DataRow dr_file in dt_Examatt.Rows)
                            {
                                sb_EM_File.AppendFormat("{0}<a href=\"/uploads/ETmatt/{1}\" target=\"_blank\">{1}</a><br>", dt_Examatt.Rows.IndexOf(dr_file) + 1, dr_file["EA_File"].ToString());
                            }
                            Literal_EM_File.Text = sb_EM_File.ToString();
                        }
                    }
                    break;
                case "3":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "進入課程");
                        if (ViewState["dt_course"] != null)
                            dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                        else
                            dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                        if (dr_course != null)
                        {
                            ViewState["dt_course"] = dr_course.Table;
                            hif_EC_TransNo.Value = dr_course["EC_TransNo"].ToString();
                            
                            if (DataEAttend.valid_ED(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString()))
                            {
                                hif_ED_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                DataSet ds_3 = new DataSet();
                                DataSet ds_EP_3 = new DataSet();
                                //元件資料轉成Hashtable
                                ds_3.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED("A")));

                                RServiceProvider rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds_3);
                                if (rsp.Result) //執行成功
                                {
                                    ds_EP_3.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP("A")));
                                    RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP_3);
                                    if (rsp_EP.Result) //執行成功
                                    {

                                    }
                                }
                            }
                            //更新EAttend資料
                            DataRow dr_ED = DataEAttend.DataReader_EC_TransNo_ED_AccID(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString());
                            if (dr_ED != null)
                            {
                                if (dr_course["EC_IsExam"].ToString() == "N")
                                {
                                    DataEAttend.updateED_ED_Type(dr_ED["ED_TransNo"].ToString(), "1");

                                }
                            }
                            lbl_EC_CName5.Text = dr_course["EC_CName"].ToString();
                            lbl_EC_Category3.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_person != null)
                            {
                                lbl_PS_ID.Text = dr_person["PS_ID"].ToString();
                                lbl_PS_NAME.Text = dr_person["PS_NAME"].ToString();

                            }
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_sales != null)
                            {
                                lbl_PS_Unit.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    lbl_DM_Name.Text = dr_sales2["NAME"].ToString();
                                }
                            }
                            lbl_EC_CName10.Text = dr_course["EC_CName"].ToString();
                            lbl_EC_SDate0.Text = DateTime.Now.ToString("yyyy/MM/dd");
                            lbl_EC_PassHours1.Text = dr_course["EC_PassHours"].ToString();
                            lbl_EC_FeePoints1.Text = dr_course["EC_FeePoints"].ToString();
                            lbl_EC_FeeAmt1.Text = dr_course["EC_FeeAmt"].ToString();
                        }
                    }
                    break;
                case "4":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "個人報名");
                        if (ViewState["dt_course"] != null)
                            dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                        else
                            dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                        if (dr_course != null)
                        {
                            ViewState["dt_course"] = dr_course.Table;
                            lbl_EC_CName4.Text = dr_course["EC_CName"].ToString();
                            lbl_EC_Category2.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                            DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_person != null)
                            {
                                lbl_PS_ID1.Text = dr_person["PS_ID"].ToString();
                                lbl_PS_NAME1.Text = dr_person["PS_NAME"].ToString();

                            }
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                            if (dr_sales != null)
                            {
                                lbl_PS_Unit1.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    lbl_DM_Name1.Text = dr_sales2["NAME"].ToString();
                                }
                            }
                            lbl_EC_CName13.Text = dr_course["EC_CName"].ToString();
                            if (!string.IsNullOrEmpty(dr_course["EC_SDate"].ToString()))
                                lbl_EC_SDate2.Text = DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd");
                            lbl_EC_PassHours2.Text = dr_course["EC_PassHours"].ToString();
                            lbl_EC_FeePoints2.Text = dr_course["EC_FeePoints"].ToString();
                            lbl_EC_FeeAmt2.Text = dr_course["EC_FeeAmt"].ToString();
                            lbl_EC_IsForce0.Text = dr_course["EC_IsForce"].ToString() == "Y" ? "是" : "否";
                            DataPhrase.CBL_TypeName(rbl_ED_EatHabits, "124", dr_course["EC_EatHabits"].ToString().Replace(";", ",").TrimEnd(','));
                            DataPhrase.CBL_TypeName(rbl_ED_Transport, "125", dr_course["EC_Transport"].ToString().Replace(";", ",").TrimEnd(','));
                            hif_EC_TransNo.Value = dr_course["EC_TransNo"].ToString();
                            DataRow dr_append = DataEAttend.DataReader_EC_TransNo_ED_AccID(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString());
                            if (dr_append != null)
                            {
                                hif_ED_TransNo.Value = dr_append["ED_TransNo"].ToString();
                                hif_ED_UidNo.Value = dr_append["ED_UidNo"].ToString();
                                hif_EC_TransNo.Value = dr_append["EC_TransNo"].ToString();
                                MainControls.ddlIndexSelectValue(rbl_ED_EatHabits, dr_append["ED_EatHabits"].ToString());
                                MainControls.ddlIndexSelectValue(rbl_ED_Transport, dr_append["ED_Transport"].ToString());
                                DataRow dr_EP = DataEPayment.DataReader_ED_TransNo(dr_append["ED_TransNo"].ToString());
                                if (dr_EP != null)
                                {
                                    hif_EP_TransNo.Value = dr_EP["EP_TransNo"].ToString();
                                    txt_EP_Bank.Text = dr_EP["EP_Bank"].ToString();
                                    txt_EP_Account.Text = dr_EP["EP_Account"].ToString();
                                    txt_EP_TransAmt.Text = dr_EP["EP_TransAmt"].ToString();
                                    if (dr_EP["EP_TransDate"].ToString() != "")
                                        txt_EP_TransDate.Text = DateTime.Parse(dr_EP["EP_TransDate"].ToString()).ToString("yyyy/MM/dd");
                                    TimeRangeAll_EP_TransTime.Time_start = dr_EP["EP_TransTimeS"].ToString();
                                    TimeRangeAll_EP_TransTime.Time_end = dr_EP["EP_TransTimeE"].ToString();
                                }
                                else
                                    hif_EP_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                lbtn_DeletePersonal.Visible = true;
                            }
                            else
                            {
                                hif_ED_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                lbtn_DeletePersonal.Visible = false;
                            }
                            if ((dr_course["EC_FeeAmt"].ToString() == "" || dr_course["EC_FeeAmt"].ToString() == "0") &&
                                (dr_course["EC_FeePoints"].ToString() == "" || dr_course["EC_FeePoints"].ToString() == "0"))
                                tb_Pay.Visible = false;
                            else
                                tb_Pay.Visible = true;
                        }
                    }
                    break;
                case "5":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "團體報名");
                    if (ViewState["dt_course"] != null)
                        dr_course = (ViewState["dt_course"] as DataTable).Rows[0];
                        
                    else
                        dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        ViewState["dt_course"] = dr_course.Table;
                        lbl_EC_CName3.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Category1.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_Title.Text = dr_person["PS_Title"].ToString();

                        }
                        DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit2.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                        }
                        DataTable dt_sales = Data_tabcSales.DataReader_DM(Session["AccID"].ToString()).Tables[0];
                        lboxSur.Items.Clear();
                        foreach (DataRow dw in dt_sales.Rows)
                        {
                            dr_person = DataPerson.DataReader_AccID(dw["cSAL_FK"].ToString().Trim());
                            if (dr_person != null)
                            {
                                ListItem li = new ListItem();
                                li.Text = dw["NAME"].ToString().Trim();
                                li.Value = dw["cSAL_FK"].ToString().Trim();
                                lboxSur.Items.Add(li);
                            }
                        }
                        DataPhrase.CBL_TypeName(rbl_ED_EatHabits0, "124", dr_course["EC_EatHabits"].ToString().Replace(";", ",").TrimEnd(','));
                        DataPhrase.CBL_TypeName(rbl_ED_Transport0, "125", dr_course["EC_Transport"].ToString().Replace(";", ",").TrimEnd(','));
                        if (dr_course["EC_Category"].ToString() == "1")
                            tb_Group.Visible = false;
                        else
                            tb_Group.Visible = true;

                    }
                    break;
                case "6":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "數位課程上課");
                        if (ViewState["dt_course"] != null)
                            dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                        else
                            dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                        if (dr_course != null)
                        {
                            ViewState["dt_course"] = dr_course.Table;
                            lbl_EC_CName6.Text = dr_course["EC_CName"].ToString();
                            lbl_EC_Category4.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                            lbl_EC_CName11.Text = dr_course["EC_CName"].ToString();
                            lbl_EC_SDate1.Text = DateTime.Now.ToString("yyyy/MM/dd");
                            hif_sDatetime.Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
                            Timer1.Enabled = true;
                            DataRow dr_Exam = DataETm.DataReader_EC_TransNo(dr_course["EC_TransNo"].ToString());
                            if (dr_Exam != null)
                            {
                                if (dr_Exam["EM_Type"].ToString() == "一般影音檔")
                                {
                                    if (!string.IsNullOrEmpty(dr_Exam["EM_URL"].ToString()))
                                        Literal_EM_File0.Text = string.Format("<iframe src=\"{0}\" width=\"100%\" height=\"600\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>", dr_Exam["EM_URL"].ToString().Replace("https://vimeo.com/", "https://player.vimeo.com/video/"));
                                    else
                                        Literal_EM_File0.Text = string.Format("<video controls controlsList=\"nodownload\"oncontextmenu=\"return false;\" width=\"100%\" height=\"600\"><source src=\"/uploads/ETM/{0}\" type=\"video/mp4\"></video>", dr_Exam["EM_File"].ToString());
                                }
                                else
                                    Literal_EM_File0.Text = string.Format("<iframe src=\"/uploads/ETM/{0}\" width=\"100%\" height=\"600\"></iframe>", dr_Exam["EM_File"].ToString());
                                DataTable dt_Examatt = DataETmatt.dataReader_EM_TransNo(true, dr_Exam["EM_TransNo"].ToString()).Tables[0];
                                GridView_EA_File.DataSource = dt_Examatt;
                                GridView_EA_File.DataBind();
                            }
                        }
                        DataRow dr_EL = DataEClick.DataReader_EC_TransNo_AccID(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString());
                        DataSet ds = new DataSet();
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEClick.GetSchema(), dataToHashtable_EL(dr_course["EC_TransNo"].ToString(), dr_EL)));
                        RServiceProvider rsp = new RServiceProvider();
                        if(dr_EL == null)
                            rsp = DataEClick.Append(sysValue.emNo, sysValue.ProgId, ds);
                        else
                            rsp = DataEClick.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {

                        }
                    }
                    break;
                case "7":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "進入測驗");
                    Timer1.Enabled = false;
                    if (ViewState["dt_course"] != null)
                        dr_course = (ViewState["dt_course"] as DataTable).Rows[0];
                        
                    else
                        dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        ViewState["dt_course"] = dr_course.Table;
                        lbl_EC_CName7.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Category5.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_ID0.Text = dr_person["PS_ID"].ToString();
                            lbl_PS_NAME0.Text = dr_person["PS_NAME"].ToString();
                        }
                        DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit0.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                            DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                            if (dr_sales2 != null)
                            {
                                lbl_DM_Name0.Text = dr_sales2["NAME"].ToString();
                            }
                        }
                        lbl_EC_CName12.Text = dr_course["EC_CName"].ToString();
                        lbl_ED_ExamDate.Text = DateTime.Now.ToString("yyyy/MM/dd");
                        lbl_EC_QuizNotify.Text = dr_course["EC_QuizNotify"].ToString();
                    }
                    break;
                case "8":
                    {
                        lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "開始測驗");
                        Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                        if (!cbox_agree.Checked)
                        {
                            MainControls.showMsg(this, "請確認資訊並勾選「我知道了」。");
                            return;
                        }
                        else if (ques_list.Count == 0)
                        {
                            MainControls.showMsg(this, "測驗題庫尚未建立，請聯絡相關人員確認。");
                            return;
                        }
                        else
                        {
                            if (ViewState["dt_course"] != null)
                                dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                            else
                                dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                            if (dr_course != null)
                            {
                                ViewState["dt_course"] = dr_course.Table;
                                lbl_EC_CName8.Text = dr_course["EC_CName"].ToString();
                                lbl_EC_Category6.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";

                                lbl_EX_STime.Text = DateTime.Now.ToString("HH:mm");
                                int EC_ExamTime = 0;
                                int.TryParse(dr_course["EC_ExamTime"].ToString(), out EC_ExamTime);
                                Session["eTime"] = DateTime.Now.AddMinutes(EC_ExamTime);
                                Timer2.Enabled = true;
                            }
                        }
                    }
                    break;
                case "9":
                    lbl_PageTitle.Text = string.Format("{0}-{1}", pageTitle, "完成測驗");
                    Timer2.Enabled = false;
                    if (ViewState["dt_course"] != null)
                        dr_course = (ViewState["dt_course"] as DataTable).Rows[0];
                        
                    else
                        dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                    if (dr_course != null)
                    {
                        ViewState["dt_course"] = dr_course.Table;
                        lbl_EC_CName9.Text = dr_course["EC_CName"].ToString();
                        lbl_EC_Category7.Text = dr_course["EC_Category"].ToString() == "1" ? "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">數</span>" : "<span style=\"font-size: 14px; color: #FFFFFF; background-color: #FF6820; padding: 5px;\">實</span>";
                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            lbl_PS_ID2.Text = dr_person["PS_ID"].ToString();
                            lbl_PS_NAME2.Text = dr_person["PS_NAME"].ToString();
                        }
                        DataRow dr_sales = Data_tabcSales.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_sales != null)
                        {
                            lbl_PS_Unit2.Text = string.Format("{0}{1}", dr_sales["Zone"].ToString(), dr_sales["cZON_NAME"].ToString());
                            DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                            if (dr_sales2 != null)
                            {
                                lbl_DM_Name2.Text = dr_sales2["NAME"].ToString();
                            }
                        }
                        lbl_EC_CName14.Text = dr_course["EC_CName"].ToString();
                        lbl_EX_STime0.Text = lbl_EX_STime.Text;
                        lbl_EX_ETime0.Text = lbl_EX_ETime.Text;

                        Panel_EA_ListItem2.Controls.Clear();
                        int score = 0;
                        int EX_Score = 0;
                        int EC_QuizNo = 0;
                        int.TryParse(dr_course["EC_QuizNo"].ToString(), out EC_QuizNo);
                        if (EC_QuizNo != 0)
                            score = 100 / EC_QuizNo;
                        int NO = 1;
                        DataSet ds_EX = new DataSet();

                        #region 寫入交易 測驗結果 tblExam
                        RServiceProvider rsp = new RServiceProvider();
                        //if (!this.IsRefresh)
                        //{
                            ds_EX.Tables.Add(MainControls.UpLoadToDataTable(DataExam.GetSchema(), dataToHashtable_EX("A", EX_Score.ToString())));
                            rsp = DataExam.AppendTransaction(sysValue.emNo, sysValue.ProgId, ds_EX);
                            if (rsp.Result) //執行成功
                            {

                            }
                            else
                            {
                                MainControls.showMsg(this, "數位課程-測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                logger.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                logger_db.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                            }
                        //}
                        #endregion
                            Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                        foreach (var ques in ques_list)
                        {
                            DataRow dr = DataEQues.DataReader_EQ_TransNo(ques.Key);
                            if (dr != null)
                            {
                                string EQ_TransNo = "", EQ_UidNo = "",
                                    E9_OrderNo = "", E9_Question = "", E9_AnsListItem = "", E9_Answer = "", E9_Score = "",
                                    EA_TransNo1 = "", EA_UidNo1 = "", EA_TransNo2 = "", EA_UidNo2 = "", EA_TransNo3 = "", EA_UidNo3 = "", EA_TransNo4 = "", EA_UidNo4 = "",
                                    E9_IsSelected1 = "", E9_IsCorrect1 = "", E9_IsSelected2 = "", E9_IsCorrect2 = "", E9_IsSelected3 = "", E9_IsCorrect3 = "", E9_IsSelected4 = "", E9_IsCorrect4 = "";
                                EQ_TransNo = dr["EQ_TransNo"].ToString();
                                EQ_UidNo = dr["EQ_UidNo"].ToString();
                                E9_OrderNo = NO.ToString();
                                E9_Question = dr["EQ_Question"].ToString();
                                switch (dr["EQ_Type"].ToString())
                                {
                                    case "是非題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                            Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                            bool ansResult = false;
                                            StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                RadioButton rbtn = ((RadioButton)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                isChecked.Add(ans.Key, rbtn.Checked);
                                                if (dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked)
                                                    ansResult = true;
                                                if (rbtn.Checked)
                                                    E9_Answer = string.Format("{0}.{1}", ans.Key + 1, rbtn.Text);
                                                sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, rbtn.Text);
                                                switch(ans.Key)
                                                {
                                                    case 0:
                                                        EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected1 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 1:
                                                        EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected2 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                }
                                            }
                                            E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                            MainControls.CreateRadioButton(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "rb", 2, id, text, isChecked);
                                            Label lbl_score = new Label();
                                            lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_score.Text = string.Format("得分：{0}", ansResult ? score : 0);
                                            Panel_EA_ListItem2.Controls.Add(lbl_score);
                                            E9_Score = ansResult ? score.ToString() : "0";
                                            if (ansResult)
                                                EX_Score += score;
                                        }
                                        break;
                                    case "單選題":
                                        {

                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                            Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                            bool ansResult = false;
                                            StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                RadioButton rbtn = ((RadioButton)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                isChecked.Add(ans.Key, rbtn.Checked);
                                                if (dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked)
                                                    ansResult = true;
                                                if (rbtn.Checked)
                                                    E9_Answer = string.Format("{0}.{1}", ans.Key + 1, rbtn.Text);
                                                sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, rbtn.Text);
                                                switch (ans.Key)
                                                {
                                                    case 0:
                                                        EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected1 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 1:
                                                        EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected2 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 2:
                                                        EA_TransNo3 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo3 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected3 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect3 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 3:
                                                        EA_TransNo4 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo4 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected4 = rbtn.Checked ? "1" : "0";
                                                        E9_IsCorrect4 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && rbtn.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !rbtn.Checked)) ? "Y" : "N";
                                                        break;
                                                }
                                            }
                                            E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                            MainControls.CreateRadioButton(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "rb", 4, id, text, isChecked);
                                            Label lbl_score = new Label();
                                            lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_score.Text = string.Format("得分：{0}", ansResult ? score : 0);
                                            Panel_EA_ListItem2.Controls.Add(lbl_score);
                                            E9_Score = ansResult ? score.ToString() : "0";
                                            if (ansResult)
                                                EX_Score += score;
                                        }
                                        break;
                                    case "複選題":
                                        {
                                            Label lbl_ques = new Label();
                                            lbl_ques.ID = string.Format("lbl_EQ_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "<br>" : "<br>");
                                            Panel_EA_ListItem2.Controls.Add(lbl_ques);
                                            SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                            SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                            SortedDictionary<int, bool> isChecked = new SortedDictionary<int, bool>();
                                            bool ansResult = true;
                                            StringBuilder sb_E9_Answer = new StringBuilder();
                                            StringBuilder sb_E9_AnsListItem = new StringBuilder();
                                            foreach (var ans in ques.Value)
                                            {
                                                DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                                text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                                CheckBox cbox = ((CheckBox)Panel_EA_ListItem1.FindControl(string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString())));
                                                isChecked.Add(ans.Key, cbox.Checked);
                                                if ((dr_ans["EA_IsCorrect"].ToString() == "Y" && !cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && cbox.Checked))
                                                    ansResult = false;
                                                if (cbox.Checked)
                                                    sb_E9_Answer.AppendFormat("{0}.{1};", ans.Key + 1, cbox.Text);
                                                sb_E9_AnsListItem.AppendFormat("{0}.{1}(;)", ans.Key + 1, cbox.Text);
                                                switch (ans.Key)
                                                {
                                                    case 0:
                                                        EA_TransNo1 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo1 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected1 = cbox.Checked ? "1" : "0";
                                                        E9_IsCorrect1 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 1:
                                                        EA_TransNo2 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo2 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected2 = cbox.Checked ? "1" : "0";
                                                        E9_IsCorrect2 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 2:
                                                        EA_TransNo3 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo3 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected3 = cbox.Checked ? "1" : "0";
                                                        E9_IsCorrect3 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                        break;
                                                    case 3:
                                                        EA_TransNo4 = dr_ans["EA_TransNo"].ToString();
                                                        EA_UidNo4 = dr_ans["EA_UidNo"].ToString();
                                                        E9_IsSelected4 = cbox.Checked ? "1" : "0";
                                                        E9_IsCorrect4 = ((dr_ans["EA_IsCorrect"].ToString() == "Y" && cbox.Checked) || (dr_ans["EA_IsCorrect"].ToString() == "N" && !cbox.Checked)) ? "Y" : "N";
                                                        break;
                                                }
                                            }
                                            E9_Answer = sb_E9_Answer.ToString().TrimEnd(';');
                                            E9_AnsListItem = sb_E9_AnsListItem.ToString();
                                            MainControls.CreateCheckBox(Panel_EA_ListItem2, string.Format("EQ_{0}2", dr["EQ_TransNo"].ToString()), "cb", 4, id, text, isChecked);
                                            Label lbl_score = new Label();
                                            lbl_score.ID = string.Format("lbl_score_{0}2", dr["EQ_TransNo"].ToString());
                                            lbl_score.Text = string.Format("得分：{0}", ansResult ? score : 0);
                                            Panel_EA_ListItem2.Controls.Add(lbl_score);
                                            E9_Score = ansResult ? score.ToString() : "0";
                                            if (ansResult)
                                                EX_Score += score;
                                        }
                                        break;
                                }
                                
                                NO += 1;

                                #region 寫入交易 測驗結果 tblExamD
                                //if (!this.IsRefresh)
                                //{
                                    if (rsp.Result) //執行成功
                                    {
                                        DataSet ds_E9 = new DataSet();
                                        ds_E9.Tables.Add(MainControls.UpLoadToDataTable(DataExamD.GetSchema(),
                                            dataToHashtable_E9("A", EQ_TransNo, EQ_UidNo,
                                            E9_OrderNo, E9_Question, E9_AnsListItem, E9_Answer, E9_Score,
                                            EA_TransNo1, EA_UidNo1, EA_TransNo2, EA_UidNo2, EA_TransNo3, EA_UidNo3, EA_TransNo4, EA_UidNo4,
                                            E9_IsSelected1, E9_IsCorrect1, E9_IsSelected2, E9_IsCorrect2, E9_IsSelected3, E9_IsCorrect3, E9_IsSelected4, E9_IsCorrect4)
                                            ));
                                        RServiceProvider rsp_E9 = DataExamD.AppendTransaction(sysValue.emNo, sysValue.ProgId, ds_E9, rsp.ReturnSqlTransaction);
                                        if (rsp_E9.Result) //執行成功
                                        {

                                        }
                                        else
                                        {
                                            //取消SQL交易，不寫入DB
                                            SqlAccess.SqlTransactionRollback(rsp.ReturnSqlTransaction);

                                            MainControls.showMsg(this, "數位課程-測驗結果資料登錄！\\n請稍後再試，或聯絡相關處理人員。");
                                            logger.Error(string.Format("數位課程-測驗結果資料登錄(答案明細)[新增]失敗：\r\n", rsp_E9.ReturnMessage));
                                            logger_db.Error(string.Format("數位課程-測驗結果資料登錄(答案明細)[新增]失敗：\r\n", rsp_E9.ReturnMessage));

                                            return;
                                        }
                                    }
                                    else
                                    {
                                        MainControls.showMsg(this, "數位課程-測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                                        logger.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                        logger_db.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", rsp.ReturnMessage));
                                    }
                                //}
                                #endregion
                            }
                            

                            
                        }

                        //執行SQL交易，正式寫入DB
                        try
                        {
                            SqlAccess.SqlTransactionCommit(rsp.ReturnSqlTransaction);
                            //更新測驗總分
                            RServiceProvider rsp_EX_Score = DataExam.updateEX_Score(hif_EX_TransNo.Value, EX_Score.ToString());
                            //更新EAttend資料
                            DataRow dr_ED = DataEAttend.DataReader_EC_TransNo_ED_AccID(dr_course["EC_TransNo"].ToString(), Session["AccID"].ToString());
                            if(dr_ED != null)
                            {
                                if (dr_course["EC_PassScore"].ToString() != "")
                                {
                                    if (EX_Score >= int.Parse(dr_course["EC_PassScore"].ToString()))
                                    {
                                        if(dr_ED["ED_Score"].ToString() == "")
                                            DataEAttend.updateED_AttHours_ED_Score(dr_ED["ED_TransNo"].ToString(), hif_EX_TransNo.Value, Session["UidNo"].ToString(), dr_course["EC_PassHours"].ToString(), EX_Score.ToString());
                                        else
                                        {
                                            if (EX_Score >= int.Parse(dr_ED["ED_Score"].ToString()))
                                                DataEAttend.updateED_AttHours_ED_Score(dr_ED["ED_TransNo"].ToString(), hif_EX_TransNo.Value, Session["UidNo"].ToString(), dr_course["EC_PassHours"].ToString(), EX_Score.ToString());
                                        }
                                    }

                                }
                            }
                        }
                        catch (Exception ex) //發生例外錯誤事件
                        {
                            //取消SQL交易，不寫入DB
                            SqlAccess.SqlTransactionRollback(rsp.ReturnSqlTransaction);
                            MainControls.showMsg(this, "數位課程-測驗結果資料登錄失敗！\\n請稍後再試，或聯絡相關處理人員。");
                            logger.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", ex));
                            logger_db.Error(string.Format("數位課程-測驗結果資料登錄[新增]失敗：\r\n", ex));
                        }
                        lbl_EX_Score.Text = EX_Score.ToString();
                    }
                    break;
                default:
                    if (!IsPostBack)
                        dataBind();
                    else
                        Response.Redirect("~/manager/ClerkECourse");
                    break;
            }
            MultiView1.ActiveViewIndex = Convert.ToInt16(CommandName);
            //this.IsRefresh = true;
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            TimeSpan tSpan = DateTime.Now.Subtract(DateTime.Parse(hif_sDatetime.Value));
            lbl_EC_STime.Text = string.Format("{0}:{1}:{2}", tSpan.Hours.ToString("00"), tSpan.Minutes.ToString("00"), tSpan.Seconds.ToString("00"));
        }
        protected void Timer2_Tick(object sender, EventArgs e)
        {
            lbl_EX_ETime.Text = DateTime.Now.ToString("HH:mm");
            TimeSpan tSpan = ((DateTime)Session["eTime"]).Subtract(DateTime.Now);
            if ((int)tSpan.TotalSeconds >= 0)
            {
                lbl_RemainingTime.Text = string.Format("{0}:{1}:{2}", tSpan.Hours.ToString("00"), tSpan.Minutes.ToString("00"), tSpan.Seconds.ToString("00"));

                if ((int)tSpan.TotalSeconds <= 0)
                {
                    Timer2.Enabled = false;
                    ScriptManager.RegisterClientScriptBlock(
                        this, HttpContext.Current.GetType(), "Alert",
                        string.Format("alert('{0}');location.href='/manager/ClerkECourse?id={1}&c=9&open=1'", "測驗時間結束！", Request.QueryString["id"]),
                        true);

                }

            }
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

            }

        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EC_CName")) return;
            e.InputParameters.Add("EC_CName", Lib.FdVP(txtSh_EC_CName.Text).Trim());
            e.InputParameters.Add("ET_Name", Lib.FdVP(txtSh_ET_Name.Text).Trim());
            e.InputParameters.Add("EC_Place", Lib.FdVP(txtSh_EC_Place.Text).Trim());
            e.InputParameters.Add("EC_SDate_s", DateRangeSh_EC_SDate.Date_start);
            e.InputParameters.Add("EC_SDate_e", DateRangeSh_EC_SDate.Date_end);
            e.InputParameters.Add("EC_Type", ddlSh_EC_Type.SelectedItem.Text);
            e.InputParameters.Add("EC_Rating", ddlSh_EC_Rating.SelectedItem.Text);
            e.InputParameters.Add("EC_Category", ddlSh_EC_Category.SelectedValue);
            e.InputParameters.Add("EC_Mode", ddlSh_EC_Mode.SelectedItem.Value);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EC_CName.Text = string.Empty;
            txtSh_ET_Name.Text = string.Empty;
            txtSh_EC_Place.Text = string.Empty;
            DateRangeSh_EC_SDate.Init();
            ddlSh_EC_Type.SelectedIndex = -1;
            ddlSh_EC_Rating.SelectedIndex = -1;
            ddlSh_EC_Category.SelectedIndex = -1;
            ddlSh_EC_Mode.SelectedIndex = -1;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion


        protected void btnApply_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "search"://
                    DataTable dt_sales = Data_tabcSales.DataReader_DM(Session["AccID"].ToString(), txtSh_AccID.Text.Trim()).Tables[0];
                    lboxSur.Items.Clear();
                    foreach (DataRow dw in dt_sales.Rows)
                    {
                        DataRow dr_person = DataPerson.DataReader_AccID(dw["cSAL_FK"].ToString().Trim());
                        if (dr_person != null)
                        {
                            ListItem li = new ListItem();
                            li.Text = dw["NAME"].ToString().Trim();
                            li.Value = dw["cSAL_FK"].ToString().Trim();
                            lboxSur.Items.Add(li);
                        }
                    }
                    break;
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) == -1)
                        {
                            lboxObj.Items.Add(m_item);
                        }
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            if (lboxObj.Items.IndexOf(itemSur) == -1)
                            {
                                lboxObj.Items.Add(itemSur);
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                                lboxSur.Items.Remove(itemSur);
                                if (lboxSur.Items.Count > siSur)
                                    lboxSur.SelectedIndex = siSur;
                                else
                                    lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            }
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "Append"://儲存新顯示順序設定
                    lbl_Msg0.Text = dataValid_batch(); //資料格式驗証
                    if (lbl_Msg0.Text.Trim().Length > 0) return;
                    if (lboxObj.Items.Count != 0)
                    {
                        string E7_Memo = "";
                        DataRow dr_course = null;
                        if (ViewState["dt_course"] != null)
                            dr_course = (ViewState["dt_course"] as DataTable).Rows[0];

                        else
                            dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
                        if (dr_course != null)
                        {
                            ViewState["dt_course"] = dr_course.Table;
                            DataRow dr_E7 = DataEMailCan.DataReader_E7_TypeCode("E017");
                            if(dr_E7 != null)
                            {
                                E7_Memo = dr_E7["E7_Memo"].ToString().Replace("{EC_CName}", dr_course["EC_CName"].ToString());
                            }
                        }

                        SysValue sysValue = ViewState["SysValue"] as SysValue;
                        StringBuilder sb_msg = new StringBuilder();
                        string mail_from = "", mail_from_name = "";

                        DataRow dr_person = DataPerson.DataReader_AccID(Session["AccID"].ToString());
                        if (dr_person != null)
                        {
                            if (dr_person["PS_EMAIL"].ToString() != "")
                                mail_from = dr_person["PS_EMAIL"].ToString();
                            else
                                mail_from = DataWebSet.dataReader("WebSendMail");
                            mail_from_name = dr_person["PS_NAME"].ToString();
                        }
                        else
                        {
                            mail_from = DataWebSet.dataReader("WebSendMail");
                            mail_from_name = DataWebSet.dataReader("WebTitle");

                        }
                        foreach (ListItem m_item in lboxObj.Items)
                        {
                            if (DataEAttend.valid_ED(dr_course["EC_TransNo"].ToString(), m_item.Value))
                            {
                                DataSet ds = new DataSet();
                                //元件資料轉成Hashtable
                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_ED_batch(m_item.Value)));

                                RServiceProvider rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds);
                                if (rsp.Result)
                                {
                                    sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：新增成功！</span><br>", m_item.Text, m_item.Value);
                                    DataSet ds_SC = new DataSet();
                                    if (!tb_Group.Visible)//數位課程
                                        ds_SC.Tables.Add(MainControls.UpLoadToDataTable(DataScheduleM.GetSchema(), dataToHashtable_SC_batch(m_item.Value, E7_Memo, DateTime.Now.ToString("yyyy/MM/dd HH:mm:00"), string.Format("{0} {1}:{2}:00", DateTime.Now.ToString("yyyy/MM/dd"), (DateTime.Now.Hour + 1).ToString("00"), DateTime.Now.Minute.ToString("00")))));
                                    else//實體課程
                                    {
                                        DateTime sDate;
                                        string sDateTime = "", eDateTime = "";
                                        if (!string.IsNullOrEmpty(dr_course["EC_SDate"].ToString()))
                                        {
                                            sDateTime = string.Format("{0} {1}", DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_course["EC_STime"].ToString());
                                            eDateTime = string.Format("{0} {1}", DateTime.Parse(dr_course["EC_SDate"].ToString()).ToString("yyyy/MM/dd"), dr_course["EC_ETime"].ToString());
                                        }
                                        DateTime.TryParse(dr_course["EC_SDate"].ToString(), out sDate);
                                        ds_SC.Tables.Add(MainControls.UpLoadToDataTable(DataScheduleM.GetSchema(), dataToHashtable_SC_P_batch(m_item.Value, dr_course["EC_Place"].ToString(), E7_Memo, sDateTime, eDateTime)));
                                    }
                                    RServiceProvider rsp_SC = DataScheduleM.Append(sysValue.emNo, sysValue.ProgId, ds_SC);
                                    if (!rsp_SC.Result)
                                    {
                                        logger.Error(string.Format("數位課程-團體報名行程資料-[新增]失敗：\r\n{0}", rsp.ReturnMessage));
                                        logger_db.Error(string.Format("數位課程-團體報名行程資料[新增]失敗：\r\n{0}", rsp.ReturnMessage));
                                    }

                                    DataRow dr_person2 = DataPerson.DataReader_AccID(m_item.Value);
                                    if (dr_person2 != null)
                                    {
                                        DataSet ds_E1 = new DataSet();
                                        ds_E1.Tables.Add(MainControls.UpLoadToDataTable(DataEmailSMS.GetSchema(), dataToHashtable_E1_batch(m_item.Value, string.Format("{0}-團體報名", dr_course["EC_CName"].ToString()), E7_Memo, mail_from_name, mail_from, dr_person2["PS_EMAIL"].ToString())));
                                        RServiceProvider rsp_E1 = DataEmailSMS.Append(sysValue.emNo, sysValue.ProgId, ds_E1);
                                        if (!rsp_E1.Result)
                                        {
                                            logger.Error(string.Format("數位課程-團體報名Email資料-[新增]失敗：\r\n{0}", rsp.ReturnMessage));
                                            logger_db.Error(string.Format("數位課程-團體報名Email資料[新增]失敗：\r\n{0}", rsp.ReturnMessage));
                                        }
                                        //Email通知
                                        RServiceProvider rsp_sendMail = MainControls.sendMail(mail_from, dr_person2["PS_EMAIL"].ToString(), "", "", string.Format("{0}-團體報名", dr_course["EC_CName"].ToString()), E7_Memo, "", mail_from_name, false);
                                        if (!rsp_sendMail.Result)
                                        {
                                            logger.Error(string.Format("數位課程-團體報名Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                            logger_db.Error(string.Format("數位課程-團體報名Email發信通知失敗：\r\n{0}", rsp_sendMail.ReturnMessage));
                                        }
                                    }


                                }
                                else
                                    sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}({1})：新增失敗！</span><br>", m_item.Text, m_item.Value);
                            }
                            else
                                sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：已新增成功！</span><br>", m_item.Text, m_item.Value);
                        }
                        GridView1.DataBind();
                        lbl_Msg0.Text = sb_msg.ToString();
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入人員!");
                    }
                    break;

            }
        }

        protected void MarkSortSetInit()
        {
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            txtSh_AccID.Text = string.Empty;
        }

        private void QuesInit()
        {
            Panel_EA_ListItem1.Controls.Clear();
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(Request.QueryString["id"]);
            if (dr_course != null && dr_course["EC_Category"].ToString() == "1")
            {
                if (!IsPostBack)
                {
                    DataTable dt_ques = DataEQues.DataReader_EC_TransNo_random(dr_course["EC_TransNo"].ToString(), dr_course["EC_QuizNo"].ToString()).Tables[0];
                    Dictionary<string, Dictionary<int, string>> ques_list = new Dictionary<string, Dictionary<int, string>>();
                    foreach (DataRow dr in dt_ques.Rows)
                    {
                        Dictionary<int, string> ans = new Dictionary<int, string>();
                        switch (dr["EQ_Type"].ToString())
                        {
                            case "是非題":
                                {
                                    Label lbl_ques = new Label();
                                    lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                    lbl_ques.Text = string.Format("{2}{0}. {1}<br>", dt_ques.Rows.IndexOf(dr) + 1, dr["EQ_Question"].ToString(), dt_ques.Rows.IndexOf(dr) == 0 ? "" : "<br>");
                                    Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                    SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                    SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                    DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                    foreach (DataRow dr_ans in dt_ans.Rows)
                                    {
                                        ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                        id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                        text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                    }
                                    MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "rb", 2, id, text);
                                }
                                break;
                            case "單選題":
                                {

                                    Label lbl_ques = new Label();
                                    lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                    lbl_ques.Text = string.Format("{2}{0}. {1}<br>", dt_ques.Rows.IndexOf(dr) + 1, dr["EQ_Question"].ToString(), dt_ques.Rows.IndexOf(dr) == 0 ? "" : "<br>");
                                    Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                    SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                    SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                    DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                    foreach (DataRow dr_ans in dt_ans.Rows)
                                    {
                                        ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                        id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                        text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                    }
                                    MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "rb", 4, id, text);
                                }
                                break;
                            case "複選題":
                                {
                                    Label lbl_ques = new Label();
                                    lbl_ques.ID = string.Format("lbl_EQ_{0}", dr["EQ_TransNo"].ToString());
                                    lbl_ques.Text = string.Format("{2}{0}. {1}<br>", dt_ques.Rows.IndexOf(dr) + 1, dr["EQ_Question"].ToString(), dt_ques.Rows.IndexOf(dr) == 0 ? "" : "<br>");
                                    Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                    SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                    SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                    DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, dr["EQ_TransNo"].ToString()).Tables[0];
                                    foreach (DataRow dr_ans in dt_ans.Rows)
                                    {
                                        ans.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_TransNo"].ToString());
                                        id.Add(dt_ans.Rows.IndexOf(dr_ans), string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                        text.Add(dt_ans.Rows.IndexOf(dr_ans), dr_ans["EA_ListItem"].ToString());
                                    }
                                    MainControls.CreateCheckBox(Panel_EA_ListItem1, string.Format("EQ_{0}", dr["EQ_TransNo"].ToString()), "cb", 4, id, text);
                                }
                                break;
                        }
                        ques_list.Add(dr["EQ_TransNo"].ToString(), ans);

                    }
                    Session["ques_list"] = ques_list;
                }
                else
                {
                    int NO = 1;
                    Dictionary<string, Dictionary<int, string>> ques_list = Session["ques_list"] as Dictionary<string, Dictionary<int, string>>;
                    foreach (var ques in ques_list)
                    {
                        DataRow dr = DataEQues.DataReader_EQ_TransNo(ques.Key);
                        if (dr != null)
                        {
                            switch (dr["EQ_Type"].ToString())
                            {
                                case "是非題":
                                    {
                                        Label lbl_ques = new Label();
                                        lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                        lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                        Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                        SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                        SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                        DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                        foreach (var ans in ques.Value)
                                        {
                                            DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                            id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                            text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                        }
                                        MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "rb", 2, id, text);
                                    }
                                    break;
                                case "單選題":
                                    {

                                        Label lbl_ques = new Label();
                                        lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                        lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                        Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                        SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                        SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                        DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                        foreach (var ans in ques.Value)
                                        {
                                            DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                            id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                            text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                        }
                                        MainControls.CreateRadioButton(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "rb", 4, id, text);
                                    }
                                    break;
                                case "複選題":
                                    {
                                        Label lbl_ques = new Label();
                                        lbl_ques.ID = string.Format("lbl_EQ_{0}", ques.Key);
                                        lbl_ques.Text = string.Format("{2}{0}. {1}<br>", NO, dr["EQ_Question"].ToString(), NO == 1 ? "" : "<br>");
                                        Panel_EA_ListItem1.Controls.Add(lbl_ques);
                                        SortedDictionary<int, string> id = new SortedDictionary<int, string>();
                                        SortedDictionary<int, string> text = new SortedDictionary<int, string>();
                                        DataTable dt_ans = DataEQans.dataReader_EQ_TransNo_random(true, ques.Key).Tables[0];
                                        foreach (var ans in ques.Value)
                                        {
                                            DataRow dr_ans = DataEQans.DataReader_EA_TransNo(ans.Value);
                                            id.Add(ans.Key, string.Format("EA_{0}", dr_ans["EA_TransNo"].ToString()));
                                            text.Add(ans.Key, dr_ans["EA_ListItem"].ToString());
                                        }
                                        MainControls.CreateCheckBox(Panel_EA_ListItem1, string.Format("EQ_{0}", ques.Key), "cb", 4, id, text);
                                    }
                                    break;
                            }
                        }
                        NO += 1;
                    }
                }
            }

        }
    }

    



}



