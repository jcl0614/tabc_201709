﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="ExamTest.aspx.cs" Inherits="tabc_201709.Manager.ExamTest" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnNew" runat="server" BackColor="#7DC44E" CommandName="1" CssClass="btnStyle" OnCommand="lnk_Command" Text="新增測驗" Visible="False" />
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <script>
        $(function () {
            window.scrollTo = function () { }
        });
    </script>
    <asp:Timer ID="Timer2" runat="server" Enabled="False" Interval="1000" OnTick="Timer2_Tick"></asp:Timer>

            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">測驗日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EX_Date" runat="server" />
                                            </td>
                                            <td align="center" class="tdQueryHead">證照關鍵字：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EG_Name" runat="server" MaxLength="100" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" BackColor="#7DC44E" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" BackColor="#7B7BC0" CssClass="btnStyle" OnClick="btnRestQuery_Click" Text="條件清除" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">證照分類：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EG_Type" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_EG_Type_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">證照科目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EG_Subject" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_test" SelectMethod="DataReader_test"
                                        TypeName="DataExamSub"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E">
                                                <table cellpadding="0" width="100%">
                                                    <tr>
                                                        <td align="left" style="padding: 20px 10px 20px 10px">
                                                            <asp:Label ID="lbl_EB_SubTypeName" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                                            </td>
                                                        <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                                <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">學員證號</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_ID0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">姓名</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_NAME0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">單位</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_PS_Unit0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">歸屬處經理</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_DM_Name0" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">測驗分類</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Type" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">考試科目</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EG_Subject" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">測驗項目</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EE_ItemName" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">測驗注意事項</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:Label ID="lbl_EB_Warnings" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" class="MS_tdTitle" width="120">&nbsp;</td>
                                                        <td class="tdData10s" style="background-color: #F8F8C8">
                                                            <asp:CheckBox ID="cbox_agree" runat="server" Text="我知道了" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                                <asp:LinkButton ID="lbtn_start" runat="server" BackColor="#EB8484" CommandName="2" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">開始測驗</asp:LinkButton>
                                                &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back6" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EB_SubTypeName0" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td class="MS_tdTitle">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name1" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗科目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EG_Subject1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗項目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EE_ItemName0" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_ExamDate1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗開始時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_STime" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗結束時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_EX_ETime" runat="server"></asp:Label>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="MS_tdTitle">剩餘時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lbl_RemainingTime" runat="server"></asp:Label>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="Timer2" EventName="Tick" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel_EA_ListItem1" runat="server">
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_finish" runat="server" BackColor="#EB8484" CommandName="3" CssClass="btnStyle" Font-Underline="False" OnClientClick="return confirm(&quot;確定要送出測驗資料嗎?&quot;);" OnCommand="lnk_Command" style="">完成測驗</asp:LinkButton>
                                    &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back9" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table id="print_exam" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EB_SubTypeName1" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td class="MS_tdTitle">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name2" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗科目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EG_Subject2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗項目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EE_ItemName1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_ExamDate2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗開始時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_STime0" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗結束時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_ETime0" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">總分數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_Score" runat="server" Font-Names="Arial" Font-Size="Medium"></asp:Label>
                                                <asp:HiddenField ID="hif_EX_TransNo" runat="server" />
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel_EA_ListItem2" runat="server" Enabled="False">
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px"><%--                                    <asp:LinkButton ID="lbtn_start2" runat="server" BackColor="#EB8484" CommandName="PrintExam" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" style="">列印測驗卷</asp:LinkButton>--%><a class="btnStyle" onclick="printScreen(print_exam,'');" style="background: #EB8484;">列印測驗卷</a> &nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lbtn_Back10" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <table id="print_exam0" align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="left" style="padding: 20px 10px 20px 10px">
                                                <asp:Label ID="lbl_EB_SubTypeName2" runat="server" Font-Bold="True" Font-Size="16px" ForeColor="#5E273C"></asp:Label>
                                            </td>
                                            <td align="right" style="padding: 20px 10px 20px 10px">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td class="MS_tdTitle">學員證號</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_ID3" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">姓名</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_NAME3" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">單位</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_PS_Unit3" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">歸屬處經理</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_DM_Name3" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗科目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EG_Subject3" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗項目</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EE_ItemName2" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_ExamDate3" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="MS_tdTitle">測驗開始時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_STime1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">測驗結束時間</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_ETime1" runat="server"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">總分數</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EX_Score0" runat="server" Font-Names="Arial" Font-Size="Medium"></asp:Label>
                                            </td>
                                            <td class="MS_tdTitle">&nbsp;</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel_EA_ListItem3" runat="server" Enabled="False">
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_Back11" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>


    <script>
        function printScreen(printlist, title) {
            var value = printlist.outerHTML;
            var printPage = window.open("", title, "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>
</asp:Content>
