﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ETeacherFee_print.aspx.cs" Inherits="tabc_201709.Manager.ETeacherFee_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
.main {
    border: 1px solid #000;
}

.sub {
    border: 0px;
}

</style>
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <table align="center" cellpadding="0" cellspacing="0" style="font-family: 微軟正黑體; font-size: 16px;">
            <tr>
                <td align="center" height="35" style="font-size: 18px; font-weight: bold; text-decoration: underline;">
    
                    台名保險經紀人股份有限公司</td>
            </tr>
            <tr>
                <td align="center" height="35" style="font-size: 18px; font-weight: bold; text-decoration: underline;">
    
                    給付收據</td>
            </tr>
            <tr>
                <td>
    
        <table class="main" align="center" cellpadding="0" cellspacing="0" width="800" style="font-size: 14px; font-family: 微軟正黑體;">
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">領&nbsp; 款&nbsp; 人<br />
                    姓　　名</td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px">
                    <table class="sub" cellpadding="0" cellspacing="0" style="height: 50px" width="700">
                        <tr>
                            <td rowspan="2" width="200" style="border-right-style: solid; border-right-width: 1px; padding-left: 10px; font-size: 20px; letter-spacing: 10px;"><%= ET_TNam %></td>
                            <td align="center" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px;">公司名稱</td>
                            <td style="border-bottom-style: solid; border-bottom-width: 1px; padding-left: 10px;"><%= ET_Company %></td>
                        </tr>
                        <tr>
                            <td align="center" width="100" style="border-right-style: solid; border-right-width: 1px;">電　　話</td>
                            <td style="padding-left: 10px"><%= ET_Phone %></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">課程名稱</td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px; padding-left: 10px; font-size: 16px;"><%= EC_CName %></td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">費用別</td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px; padding-left: 10px; font-size: 16px;"><%= E6_Type_str %></td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">應付金額</td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px">
                    <table class="sub" cellpadding="0" cellspacing="0" style="height: 50px" width="700">
                        <tr>
                            <td rowspan="2" width="500" style="border-right-style: solid; border-right-width: 1px; padding-left: 10px; font-size: 18px;"><%= E6_ShouldPay_str %></td>
                            <td style="border-bottom-style: solid; border-bottom-width: 1px" align="center" width="200">應 扣 繳 所 得 稅</td>
                        </tr>
                        <tr>
                            <td align="center" width="200"><%= E6_Tax_str %></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">實付金額<br />
                    <span style="font-size: 12px">(含補克保費)</span></td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px; padding-left: 10px; font-size: 18px;"><%= E6_RealPay_str %></td>
            </tr>
            <tr>
                <td align="center" height="50" style="border-bottom-style: solid; border-bottom-width: 1px" colspan="2">
                    <table class="sub" cellpadding="0" cellspacing="0" style="height: 50px" width="100%">
                        <tr>
                            <td width="300" style="border-right-style: solid; border-right-width: 1px;" align="center">上列款項已由 (全銜)<br />匯至　　　　帳戶　　　　如數領訖</td>
                            <td align="center" width="100" style="border-right-style: solid; border-right-width: 1px; ">領&nbsp; 款&nbsp; 人<br />
                                簽　　章</td>
                            <td width="400">&nbsp;</td>
                        </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td align="center" height="25" style="border-bottom-style: solid; border-bottom-width: 1px" colspan="2">
                    <table class="sub" cellpadding="0" cellspacing="0" style="height: 25px" width="100%">
                        <tr>
                            <td width="300" style="border-right-style: solid; border-right-width: 1px;" align="center">國　民　身　分　證　統　一　編　號</td>
                            <td align="left" width="490" style="font-size: 18px; letter-spacing: 10px; padding-left: 10px; font-weight: bold; font-family: Arial, Helvetica, sans-serif;"><%= ET_ID %></td>
                        </tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px; border-bottom-style: solid; border-bottom-width: 1px">地　　址</td>
                <td align="left" style="border-bottom-style: solid; border-bottom-width: 1px; padding-left: 10px;"><%= ET_Addr %></td>
            </tr>
            <tr>
                <td align="center" height="50" width="100" style="border-right-style: solid; border-right-width: 1px;">備　　考</td>
                <td align="left">
                    <table class="sub" cellpadding="0" cellspacing="0" style="height: 50px" width="700">
                        <tr>
                            <td width="300" style="border-right-style: solid; border-right-width: 1px; padding-left: 10px;" align="left"><%# Eval("E6_Remark") %></td>
                            <td align="center" width="100" style="border-right-style: solid; border-right-width: 1px; ">匯　　款<br />
                                日　　期</td>
                            <td width="300" align="center" style="font-size: 16px"><%= E6_TransDate_str %></td>
                        </tr>
                        </table>
                </td>
            </tr>
        </table>
    
                </td>
            </tr>
            <tr>
                <td align="right" height="70" style="padding-right: 50px">承辦人&nbsp;<span style="text-decoration: underline; font-size: 18px;">　　　<%= E6_PIC_Name %>　　　</span>&nbsp;</td>
            </tr>
            <tr>
                <td align="center" style="font-size: 12px">====================　身　分　證　影　本　黏　貼　區　====================</td>
            </tr>
            <tr>
                <td align="center" height="300" style="font-size: 12px">《 請用浮貼方式黏貼 》</td>
            </tr>
            <tr>
                <td align="right" style="font-size: 12px" valign="bottom" height="250">20170920 版<br />
                    業務支援處製表</td>
            </tr>
        </table>
            </ItemTemplate>
        </asp:Repeater>
    
        
    
    </form>
</body>
</html>
