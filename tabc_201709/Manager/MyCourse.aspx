﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="MyCourse.aspx.cs" Inherits="tabc_201709.Manager.MyCourse" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc2" %>

<%@ Register src="ascx/TimeRangeAll.ascx" tagname="TimeRangeAll" tagprefix="uc1" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc3" %>
<%@ Register src="ascx/selCourse.ascx" tagname="selCourse" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">

    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
    <style>
        .btnStyle {
            cursor: pointer;
            font-size: 15px;
            font-family: 微軟正黑體;
            padding: 5px 13px 5px 13px;
            margin: 2px 5px 2px 5px;
            color: #ffffff;
            border-radius: 4px;
            border-width: 0px;
        }

            .btnStyle:hover {
                -moz-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                -webkit-box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
                box-shadow: 2px 2px 2px rgba(20%,20%,40%,0.6),4px 4px 6px rgba(20%,20%,40%,0.4),6px 6px 12px rgba(20%,20%,40%,0.4);
            }
            .tb {
    border: 1px solid #000; border-collapse: collapse;
}
.tb tr, .tb td {
    border: 1px solid #000;
}
    </style>
    <style media="print">
        .Noprint {
            display: none;
            margin: 0px;
            padding: 0px;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="left" style="padding-top: 10px; padding-bottom: 10px; border-bottom-style: solid; border-bottom-width: 1px; font-size: 16px;">
                            <asp:Literal ID="Literal4" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding-top: 10px">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                    </td>
                                    <td width="50">&nbsp;</td>
                                    <td>
                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal>
                                    </td>
                                    <td width="50">&nbsp;</td>
                                    <td>
                                        <asp:Literal ID="Literal3" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            &nbsp;</td>
                    </tr>
                </table>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" style="background-color: #FBEAD4" width="100%">
                                        <tr>
                                            <td align="center" class="tdQueryHead">學院：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">分級：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EC_Rating" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">上課地點：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_Place" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" rowspan="3">
                                                <asp:Button ID="btnSearch0" runat="server" CssClass="btnStyle" Text="轄下人員選取" BackColor="#EB8484" CommandName="2" OnCommand="lnk_Command" />
                                                <br />
                                                <asp:Button ID="btnSearch" runat="server" BackColor="#7DC44E" CssClass="btnStyle" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="3">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnStyle" OnClick="btnRestQuery_Click" BackColor="#7B7BC0" Text="條件清除" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">課程/講師關鍵字：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EC_CName" runat="server" Width="100px"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">數位/實體：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_ED_Category" runat="server">
                                                    <asp:ListItem Value=""></asp:ListItem>
                                                    <asp:ListItem Value="1">數位</asp:ListItem>
                                                    <asp:ListItem Value="2">實體</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">開課日期：</td>
                                            <td align="center" class="tdQueryData">
                                                <uc2:DateRange ID="DateRangeSh_EC_SDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" class="tdQueryHead">狀態：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_ED_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount_my" SelectMethod="DataReader_my"
                                        TypeName="DataEAttend"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="center" style="background-color: #FFBD6E; padding-top: 20px; padding-bottom: 20px; font-weight: bold; color: #6F3512; font-size: 18px;">上課人員名單</td>
                                        </tr>
                                    </table>
                                </td>
                                </tr>
                            <tr>
                                <td align="left" style="padding: 10px; background-color: #F8F8C8;">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="120">開課日期</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_SDate" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label>
                                            </td>
                                            <td align="right" class="MS_tdTitle" width="120">課程名稱</td>
                                            <td class="tdData10s" style="background-color: #F8F8C8">
                                                <asp:Label ID="lbl_EC_CName" runat="server" Font-Bold="True" Font-Size="16px"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel1" runat="server">
                                        <div id="print_all">
                                            <asp:GridView ID="GridView2" runat="server" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="5" Font-Size="14px" ForeColor="Black" GridLines="None" AutoGenerateColumns="False">
                                                <AlternatingRowStyle BackColor="#F4F9FD" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="單位">
                                                        <ItemTemplate>
                                                            <%# Eval("cZON_PK_NAME") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="受訓人員編號">
                                                        <ItemTemplate>
                                                            <%# Eval("ED_AccID") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="受訓人員姓名">
                                                        <ItemTemplate>
                                                            <%# Eval("ED_Name") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="學習時間">
                                                        <ItemTemplate>
                                                            <%# Eval("ED_SignDate") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="測驗時間">
                                                        <ItemTemplate>
                                                            <%# Eval("ED_ExamDate") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="測驗結果(分數)">
                                                        <ItemTemplate>
                                                            <%# Eval("ED_Score") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="Tan" />
                                                <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" HorizontalAlign="Center" ForeColor="#4B0F00" Height="40px" />
                                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                <RowStyle HorizontalAlign="Center" BackColor="#FCF3F3" />
                                                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                            </table>
                        <table align="center" border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px"><asp:LinkButton ID="lbtn_Back10" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <table align="center" border="0" cellpadding="2" cellspacing="1" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E">
                                    <table cellpadding="0" width="100%">
                                        <tr>
                                            <td align="center" style="background-color: #FFBD6E; padding-top: 20px; padding-bottom: 20px; font-weight: bold; color: #6F3512; font-size: 18px;">上課人員名單</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding: 10px; background-color: #F8F8C8;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="left" style="background-color: #F8F8C8; padding: 5px 10px 20px 10px">
                                    <asp:Panel ID="Panel2" runat="server">
                                        <div id="print_all0">
                                            <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="5" Font-Size="14px" ForeColor="Black" GridLines="None">
                                                <AlternatingRowStyle BackColor="#F4F9FD" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="選取">
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="CheckBox1"  ToolTip='<%# Eval("cSAL_FK") %>' runat="server" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="單位">
                                                        <ItemTemplate>
                                                            <%# Eval("cZON_TYPE") %><%# Eval("cZON_Name") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="職級">
                                                        <ItemTemplate>
                                                            <%# Eval("PS_Title") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="業務員編號">
                                                        <ItemTemplate>
                                                            <%# Eval("cSAL_FK") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="業務員姓名">
                                                        <ItemTemplate>
                                                            <%# Eval("Name") %>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="Tan" />
                                                <HeaderStyle BackColor="#FFBD6E" Font-Bold="True" ForeColor="#4B0F00" Height="40px" HorizontalAlign="Center" />
                                                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                                                <RowStyle BackColor="#FCF3F3" HorizontalAlign="Center" />
                                                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                                                <SortedAscendingCellStyle BackColor="#FAFAE7" />
                                                <SortedAscendingHeaderStyle BackColor="#DAC09E" />
                                                <SortedDescendingCellStyle BackColor="#E1DB9C" />
                                                <SortedDescendingHeaderStyle BackColor="#C2A47B" />
                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table align="center" border="0" cellpadding="2" cellspacing="1" class="Noprint" width="100%">
                            <tr>
                                <td align="left" style="background-color: #FFBD6E; padding: 15px">
                                    <asp:LinkButton ID="lbtn_Back12" runat="server" BackColor="#EB8484" CommandName="SearchMember" CssClass="btnStyle" Font-Underline="False" OnCommand="btn_Command" Style="">確認</asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="lbtn_Back11" runat="server" BackColor="#7B7BC0" CommandName="0" CssClass="btnStyle" Font-Underline="False" OnCommand="lnk_Command" Style="">回查詢清單</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
