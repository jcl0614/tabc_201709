﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace tabc_201709.Manager
{
    public partial class PdCompare : System.Web.UI.Page
    {
        protected StringBuilder sbCompare = new StringBuilder("");
        protected DataSet CompItemds = new DataSet();

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage10);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    //      this.SetFocus(txtSh_title);
                    //      Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    //if (btnEdit.Visible)
                    //{
                    //    this.SetFocus(txt_title);
                    //    Panel_Form.DefaultButton = btnEdit.ID;
                    //}
                    //else
                    //{
                    //    this.SetFocus(txt_title);
                    //    Panel_Form.DefaultButton = btnAppend.ID;
                    //}
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNO"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();

                DataPdCompare.CompanyDropDownList(ddlSh_Company, ddlSh_ComKind.SelectedValue);
                DataPdCompare.PhraseRadioButtonList(rblSh_Type, "061", "TypeSubCode");

                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 850);
                //預設：只出現新增按鈕
                //   this.maintainButtonEnabled("");
                //預設：瀏覽
                //  ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataPdCompare.DataColumn(this.GridView1);
            DataPdCompare.DataColumnPDT(this.GridView2);
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            //if (ddl_class.SelectedValue == "")
            //    sbError.Append("●「分類」必須選取!<br>");
            //if (txt_title.Text.Trim() == "")
            //    sbError.Append("●「標題文字」必須輸入!<br>");
            //if (MainControls.htmlText(ckeditor1.Html).Trim() == "")
            //    sbError.Append("●「內容」必須輸入!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // \"Unit\":\"萬元\",\"MinVal\":\"25\",\"MaxVal\":\"300\"
            string Unit = "";
            string MinVal = "";
            string MaxVal = "";
            // clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "Mark":
                            HF_Mark.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        //    case "class":
                        //        MainControls.ddlIndexSelectValue(ddl_class, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                        //        break;
                        case "DispClass":
                            HF_DispClass.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "InsName":
                            HF_InsName.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "DispMark":
                            HF_DispMark.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CompanyNo":
                            HF_CompanyNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        //case "description":
                        //    ckeditor1.Html = Server.HtmlDecode(gvr.Cells[i].Text);
                        //    break;
                        //case "addDate":
                        //    lbl_addDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                        //    break;
                        //case "emName":
                        //    lbl_name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                        //    break;
                        //case "emNo":
                        //    hf_emNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                        //    break;
                        case "Unit":
                            Unit = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "MinVal":
                            MinVal = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "MaxVal":
                            MaxVal = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }

            // \"Unit\":\"萬元\",\"MinVal\":\"25\",\"MaxVal\":\"300\"

            InsLimit.Text = "";

            DataTable YearList = DataPdCompare.Itemds.Tables["YearList"];
            DataPdCompare.RowsRadioButtonList(rblYear_list, YearList, HF_Mark.Value);
            //UnitList保額清單(同一筆險種的UnitList回傳有2筆以上者且ItemList.Unit not like '%元%'，則「保額欄位」的輸入，改為清單列示UnitList的項目)
            ddl_Amtinput.Items.Clear();
            txt_Amtinput.Text = MinVal;
            DataTable UnitList = DataPdCompare.Itemds.Tables["UnitList"];
            DataRow[] drs = UnitList.Select("Mark='" + HF_Mark.Value + "'");
            if (drs.Length > 0)
            {
                if (drs.Length == 1)
                {
                    txt_Amtinput.Text = drs[0]["Val"].ToString();
                    ddl_Amtinput.Visible = false;
                }
                else if (Unit.IndexOf("元") < 0)
                {

                    foreach (DataRow dr in drs)
                    {
                        ddl_Amtinput.Items.Add(new ListItem(dr["Val"].ToString(), dr["Val"].ToString()));
                    }
                    if (ddl_Amtinput.Items.Count > 0)
                        ddl_Amtinput.Items[0].Selected = true;
                    ddl_Amtinput.Visible = true;
                    txt_Amtinput.Visible = false;

                }
            }

            pdtUnit.Text = Unit;
            InsLimit.Text = MinVal + Unit + "~" + MaxVal + Unit;
            HF_MinVal.Value = MinVal;
            HF_MaxVal.Value = MaxVal;
            lbl_Msg.Text = "";

            CalcItemPremium();

            //切換至維護畫面
            //ChangeMultiView(1);
            //  maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel")
            {
                //lbl_Msg.Text = dataValid(); //資料格式驗証
                //if (lbl_Msg.Text.Trim().Length > 0) return;
                //元件資料轉成Hashtable
                ds.Tables.Add(MainControls.UpLoadToDataTable(DataPdCompare.GetSchema(), dataToHashtable()));
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        RServiceProvider rsp = DataPdCompare.Append(sysValue.emNo, sysValue.ProgId, ds);
                        //if (rsp.Result) //執行成功
                        //{
                        //    hf_id.Value = rsp.ReturnData.ToString();
                        //    hf_emNo.Value = Session["emNo"].ToString();
                        //    GridView1.DataBind();
                        //    lbl_Msg.Text = "新增成功!";
                        //    maintainButtonEnabled("Select");
                        //    return;
                        //}
                        //else
                        //    lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        RServiceProvider rsp = DataPdCompare.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            //    lbl_Msg.Text = "修改成功!";
                            return;
                        }
                        //else
                        //    lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        RServiceProvider rsp = DataPdCompare.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            //    lbl_Msg.Text = "刪除成功!";
                            //    maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        //  else
                        //       lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable()
        {
            Hashtable hsData = new Hashtable();

            //hsData.Add("id", hf_id.Value);
            //hsData.Add("class", ddl_class.SelectedValue);
            //hsData.Add("title", Lib.FdVP(txt_title.Text.Trim()));
            //hsData.Add("description", Lib.HtmlTags(ckeditor1.Html.Trim()));
            hsData.Add("emNo", Session["emNo"].ToString());

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            //hf_id.Value = string.Empty;
            //ddl_class.SelectedIndex = 0;
            //txt_title.Text = string.Empty;
            //ckeditor1.Html = string.Empty;
            //lbl_addDate.Text = string.Empty;
            //lbl_name.Text = string.Empty;
            //lbl_Msg.Text = string.Empty;
            //ddl_class.Enabled = true;
            //txt_title.ReadOnly = false;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            //   lnkBrowse.Visible = false;
            //   lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    MultiView2.ActiveViewIndex = -1;
                    MultiView3.ActiveViewIndex = -1;
                    if (DataPdCompare.dtCompare != null)
                        DataPdCompare.dtCompare.Clear();
                    CompItemds = new DataSet();
              //      Session["PdComparePrn"] = null;
                    ViewState["PdComparePrn"] = null;
                    //    clearControlContext();
                    //    maintainButtonEnabled("Append");
                    //    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    DataPdCompare.CompanyDropDownList(ddlSh_Company, ddlSh_ComKind.SelectedValue);
                    DataPdCompare.PhraseRadioButtonList(rblSh_Type, "061", "TypeSubCode");
                    ddl_Amtinput.Visible = false;
                    txt_Amtinput.Visible = true;
                    txt_Amtinput.Text = "1";
                    txt_ModePrem.Text = "0";
                    pdtUnit.Text = "";
                    InsLimit.Text = "";
                    this.GridView1.DataBind();
                    this.GridView2.DataBind();

                    //GridView1顯示控制
                    // MainControls.GridViewStyleCtrl(GridView1, 850);
                    //預設：只出現新增按鈕
                    //   this.maintainButtonEnabled("");
                    //預設：瀏覽
                    //  ChangeMultiView(0);
                    //資料繫結
                    // dataBind();

                    break;
                case 1:
                    //    lnkBrowse.Visible = true;
                    break;
            }
        }
        #endregion


        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                if (e.Row.Cells[0].Controls.Count > 0 && e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                    else if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Delete")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnDelete";
                        //   ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        //   ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }
                else if (e.Row.Cells[6].Controls.Count > 0 && e.Row.Cells[6].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[6].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[6].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[6].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[6].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                    else if (((Button)e.Row.Cells[6].Controls[0]).CommandName == "Delete")
                    {
                        ((Button)e.Row.Cells[6].Controls[0]).CssClass = "btnDelete";
                        //   ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        //   ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            // string iStatus, string CompanyNo, string Saled, string Channel, string DispMark, string Keyword
            //無搜尋者刪除
            if (e.InputParameters.Contains("iStatus")) return;
            e.InputParameters.Add("iStatus", ddlSh_ComKind.SelectedValue.Trim());
            e.InputParameters.Add("CompanyNo", ddlSh_Company.SelectedValue.Trim());
            e.InputParameters.Add("Saled", (chk_stop.Checked ? "2" : "0"));//// status 0:現售  1:停售  2:全部
            e.InputParameters.Add("Channel", "TABC");//tabcSale.Checked ? "TABC" : "null");
            e.InputParameters.Add("DispMark", txtSh_PdtNo.Text.Trim());
            //     e.InputParameters.Add("Mark", rblSh_Type.SelectedValue.Trim());
            //    e.InputParameters.Add("InsName", txtSh_PdtName.Text.Trim());
            e.InputParameters.Add("Keyword", txtSh_KeyWord.Text.Trim());            //
            e.InputParameters.Add("DispClass", rblSh_Type.SelectedValue.Trim());            //
            DataPdCompare.CompanyName = ddlSh_Company.SelectedItem.Text;
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            rblYear_list.Items.Clear();
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            //  ddlSh_class.SelectedIndex = 0;
            txtSh_PdtNo.Text = string.Empty;
            txtSh_KeyWord.Text = string.Empty;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        protected void ddlSh_ComKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataPdInfo.CompanyDropDownList(ddlSh_Company, ddlSh_ComKind.SelectedValue);

        }

        #endregion

        protected void tab1_Click(object sender, EventArgs e)
        {
            ChangeMultiView(0);
        }

        protected void btn_ok_Click(object sender, EventArgs e)
        {
            MultiView2.ActiveViewIndex = 0;
            if (DataPdCompare.dtCompare == null)
            {
                DataPdCompare.dtCompare = new DataTable();
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("GID", System.Type.GetType("System.String")));//公司
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("CompanyNo", System.Type.GetType("System.String")));//公司
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("CompanyName", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));//
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("DispMark", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));//險種名稱
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("DispClass", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("Amtinput", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("ModePrem", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("YName", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("Yearly", System.Type.GetType("System.String")));
                DataPdCompare.dtCompare.Columns.Add(new DataColumn("Unit", System.Type.GetType("System.String")));
            }
            lbl_Msg.Text = "";

            if (DataPdCompare.dtCompare.Rows.Count < 4)
            {

                if (DataPdCompare.Cint(HF_MinVal.Value) <= DataPdCompare.Cint(txt_Amtinput.Text) && DataPdCompare.Cint(HF_MaxVal.Value) >= DataPdCompare.Cint(txt_Amtinput.Text))
                {
                    EX_DataAPI ed = new EX_DataAPI();
                    //  DataTable dt = new DataTable();
                    //   Itemds = new DataSet();

                    int InsAge = DataPdCompare.InsAge(txt_birth.Text);
                    // itmInfo:{"DispGrade":"J","CmpyNo":"PT","IMark":"HN","DispMark":"HN","Yearly":"1","Target":1,"Amount":"30","Premium":0,"InsName":"住院醫療健康保險附約（丙型）","AnnuitySetting":null,"Payment":"0","CstmName":"","Sex":"0","Age":"30","CLevel":"1","CmpyName":"保德信","YName":"1年期","TagName":1,"AmountString":"計劃30","PremiumString":0,"MinVal":5,"MaxVal":60,"SDate":"2017/11/28"}
                    string Json = "{\"DispGrade\":\"" + HF_DispClass.Value + "\",\"CmpyNo\":\"" + HF_CompanyNo.Value + "\",\"IMark\":\"" + HF_Mark.Value + "\",\"Yearly\":\"" + rblYear_list.SelectedValue + "\",\"Amount\":\"" + txt_Amtinput.Text + "\",\"Payment\":\"0\",\"Sex\":\"" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + "\",\"Age\":\"" + InsAge.ToString() + "\",\"CLevel\":\"" + ddljob1.SelectedItem + "\",\"SDate\":\"" + DateTime.Now.ToString("yyyy/MM/dd") + "\"}";
                    string InsItem = ed.CalcItemPremium((rbl_sex.SelectedValue == "男" ? "0" : "1"), InsAge.ToString(), Json);
                    if (InsItem != "[]" && InsItem.Length > 0)// && DataPdCompare.Cint(txt_ModePrem.Text) == 0)
                    {
                        //{"DispGrade":"A","Payment":0,"CmpyNo":"ML","IMark":"INJ","Yearly":6,"Target":0,"Amount":10,"Premium":0,"AnnuitySetting":null,"SDate":"2017/11/30","CstmName":"要保人","Age":46,"Sex":0,"CLevel":1,"CmpyName":null,"DispMark":null,"InsName":null,"YName":null,"AmountString":"100,000","PremiumString":"0","statusInfo":null}
                        JObject itmInfo = JObject.Parse(InsItem);
                        txt_ModePrem.Text = itmInfo["Premium"].ToString();
                    }
                    if (DataPdCompare.Cint(txt_ModePrem.Text) > 0)
                    {
                        DataRow dr = DataPdCompare.dtCompare.NewRow();
                        dr["GID"] = System.Guid.NewGuid().ToString();
                        dr["CompanyNo"] = HF_CompanyNo.Value;//公司
                        dr["CompanyName"] = DataPdCompare.CompanyName;
                        dr["Mark"] = HF_Mark.Value;//
                        dr["DispMark"] = HF_DispMark.Value;
                        dr["InsName"] = HF_InsName.Value;//險種名稱
                        dr["DispClass"] = HF_DispClass.Value;
                        dr["Amtinput"] = txt_Amtinput.Text;
                        dr["ModePrem"] = txt_ModePrem.Text;
                        dr["YName"] = rblYear_list.SelectedItem.Value;
                        dr["Yearly"] = rblYear_list.SelectedValue;
                        dr["Unit"] = pdtUnit.Text;
                        dr.EndEdit();
                        DataPdCompare.dtCompare.Rows.Add(dr);
                    }
                    else
                        lbl_Msg.Text = "請輸入保費";

                }
                else
                    lbl_Msg.Text = "保額不符合投保限制！";// "請輸入保額:" + InsLimit.Text;
            }
            else
                lbl_Msg.Text = "比較險種僅限4筆！";//"請輸入保額:" + InsLimit.Text;

            // CompareTable();
            this.GridView2.DataSource = DataPdCompare.dtCompare;
            this.GridView2.DataBind();

        }
        protected void CompareTable()
        {
            sbCompare = new StringBuilder("");
            for (int i = 0; i < DataPdCompare.dtCompare.Rows.Count; i++)
            {
                //公司 險種代碼 險種名稱 年期 保額 保費 刪除
                sbCompare.Append("<tr>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["CompanyName"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["DispMark"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["InsName"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["YName"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["Amtinput"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append(DataPdCompare.dtCompare.Rows[i]["ModePrem"].ToString()).Append("</td>");
                sbCompare.Append("<td>").Append("<asp:Button ID='btn_del" + i.ToString()).Append("' runat='server' Text='完成查詢'class='btn btn-sm  btn-danger' onclick='btn_del" + i.ToString()).Append("_Click' />").Append("</td>");
                sbCompare.Append("</tr>");
            }
        }
        protected void btn_close_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = -1;
            MultiView2.ActiveViewIndex = 0;

        }

        protected void ddl_Amtinput_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_Amtinput.Text = ddl_Amtinput.SelectedValue;
            CalcItemPremium();

        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataPdCompare.dtCompare.Rows[GridView2.SelectedIndex].Delete();
        }

        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            DataPdCompare.dtCompare.Rows[e.RowIndex].Delete();
            this.GridView2.DataSource = DataPdCompare.dtCompare;
            this.GridView2.DataBind();
        }
        protected void CalcItemPremium()
        {
            EX_DataAPI ed = new EX_DataAPI();
            int InsAge = DataPdCompare.InsAge(txt_birth.Text);
            // itmInfo:{"DispGrade":"J","CmpyNo":"PT","IMark":"HN","DispMark":"HN","Yearly":"1","Target":1,"Amount":"30","Premium":0,"InsName":"住院醫療健康保險附約（丙型）","AnnuitySetting":null,"Payment":"0","CstmName":"","Sex":"0","Age":"30","CLevel":"1","CmpyName":"保德信","YName":"1年期","TagName":1,"AmountString":"計劃30","PremiumString":0,"MinVal":5,"MaxVal":60,"SDate":"2017/11/28"}
            string Json = "{\"DispGrade\":\"" + HF_DispClass.Value + "\",\"CmpyNo\":\"" + HF_CompanyNo.Value + "\",\"IMark\":\"" + HF_Mark.Value + "\",\"Yearly\":\"" + rblYear_list.SelectedValue + "\",\"Amount\":\"" + txt_Amtinput.Text + "\",\"Payment\":\"0\",\"Sex\":\"" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + "\",\"Age\":\"" + InsAge.ToString() + "\",\"CLevel\":\"" + ddljob1.SelectedItem + "\",\"SDate\":\"" + DateTime.Now.ToString("yyyy/MM/dd") + "\"}";
            string InsItem = ed.CalcItemPremium((rbl_sex.SelectedValue == "男" ? "0" : "1"), InsAge.ToString(), Json);
            if (InsItem != "[]" && InsItem.Length > 0)
            {
                //{"DispGrade":"A","Payment":0,"CmpyNo":"ML","IMark":"INJ","Yearly":6,"Target":0,"Amount":10,"Premium":0,"AnnuitySetting":null,"SDate":"2017/11/30","CstmName":"要保人","Age":46,"Sex":0,"CLevel":1,"CmpyName":null,"DispMark":null,"InsName":null,"YName":null,"AmountString":"100,000","PremiumString":"0","statusInfo":null}
                //DataTable dt = JsonConvert.DeserializeObject<DataTable>(InsItem);
                //dt.TableName = "itmInfo";
                //txt_ModePrem.Text = dt.Rows[0]["Premium"].ToString();
                JObject itmInfo = JObject.Parse(InsItem);
                txt_ModePrem.Text = itmInfo["Premium"].ToString();

            }
            //    CompareTable();
            this.GridView2.DataSource = DataPdCompare.dtCompare;
            this.GridView2.DataBind();
        }

        protected string TdVal(string tdstr)
        {
            string ret = tdstr;

            return (ret =="null" ? "&nbsp;" : ret);
        }
        protected void tab2_Click(object sender, EventArgs e)
        {


            if (DataPdCompare.dtCompare != null)
            {
                //DataPdCompare.dtCompare
                //                            dr["GID"] = System.Guid.NewGuid().ToString();
                //            dr["CompanyNo"] = HF_CompanyNo.Value;//公司
                //            dr["CompanyName"] = DataPdCompare.CompanyName;
                //            dr["Mark"] = HF_Mark.Value;//
                //            dr["DispMark"] = HF_DispMark.Value;
                //            dr["InsName"] = HF_InsName.Value;//險種名稱
                //            dr["DispClass"] = HF_DispClass.Value;
                //            dr["Amtinput"] = txt_Amtinput.Text;
                //            dr["ModePrem"] = txt_ModePrem.Text;
                //            dr["YName"] = rblYear_list.SelectedItem.Value;
                //            dr["Yearly"] = rblYear_list.SelectedValue;
                //           dr["Unit"] = pdtUnit.Text;
                // itmInfo:{"DispGrade":"J","CmpyNo":"PT","IMark":"HN","DispMark":"HN","Yearly":"1","Target":1,"Amount":"30","Premium":0,"InsName":"住院醫療健康保險附約（丙型）","AnnuitySetting":null,"Payment":"0","CstmName":"","Sex":"0","Age":"30","CLevel":"1","CmpyName":"保德信","YName":"1年期","TagName":1,"AmountString":"計劃30","PremiumString":0,"MinVal":5,"MaxVal":60,"SDate":"2017/11/28"}
                //string Json = "{\"DispGrade\":\"" + HF_DispClass.Value + "\",\"CmpyNo\":\"" + HF_CompanyNo.Value + "\",\"IMark\":\"" + HF_Mark.Value + "\",\"Yearly\":\"" + rblYear_list.SelectedValue + "\",\"Amount\":\"" + txt_Amtinput.Text + "\",\"Payment\":\"0\",\"Sex\":\"" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + "\",\"Age\":\"" + InsAge.ToString() + "\",\"CLevel\":\"" + ddljob1.SelectedItem + "\",\"SDate\":\"" + DateTime.Now.ToString("yyyy/MM/dd") + "\"}";
                int InsAge = DataPdCompare.InsAge(txt_birth.Text);
                string Json = "{\"Payment\":0,\"SetDate\":\"\",\"CmpyNo\":\"\",\"PPLNo\":\"\",\"PPLGuid\":\"" + System.Guid.NewGuid().ToString() + "\",";
                Json = Json + "\"PMans\":[{\"Identity\":null,\"PKID\":\"\",\"CName\":\"\",\"Sex\":\"" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + "\",\"Birthday\":\"" + txt_birth.Text + "\",\"Age\":\"" + InsAge.ToString() + "\",\"CLevel\":\"" + ddljob1.SelectedItem + "\"}],";
                //    Json = Json + "\"insList\":[{\"DispGrade\":\"A\",\"CmpyNo\":\"PT\",\"IMark\":\"10F55A\",\"DispMark\":\"10F55A\",\"Yearly\":\"10\",\"Target\":1,\"Amount\":20,\"Premium\":0,\"InsName\":\"家用保障定期保險(繳費10年保障至55歲)\",\"AnnuitySetting\":null,\"CmpyName\":\"保德信\",\"YName\":\"10年期\",\"TagName\":1,\"AmountString\":\"20萬元\",\"PremiumString\":0,\"MinVal\":18,\"MaxVal\":500,\"SDate\":\"\"},";
                sbCompare = new StringBuilder("\"insList\":[");
                for (int i = 0; i < DataPdCompare.dtCompare.Rows.Count; i++)
                {
                    //公司 險種代碼 險種名稱 年期 保額 保費 刪除
                    sbCompare.Append("{");
                    sbCompare.Append("\"DispGrade\":\"").Append(DataPdCompare.dtCompare.Rows[i]["DispClass"].ToString()).Append("\",");
                    sbCompare.Append("\"CmpyNo\":\"").Append(DataPdCompare.dtCompare.Rows[i]["CompanyNo"].ToString()).Append("\",");
                    sbCompare.Append("\"IMark\":\"").Append(DataPdCompare.dtCompare.Rows[i]["Mark"].ToString()).Append("\",");
                    sbCompare.Append("\"DispMark\":\"").Append(DataPdCompare.dtCompare.Rows[i]["DispMark"].ToString()).Append("\",");
                    sbCompare.Append("\"Yearly\":\"").Append(DataPdCompare.dtCompare.Rows[i]["Yearly"].ToString()).Append("\",");
                    sbCompare.Append("\"Target\":1,");
                    sbCompare.Append("\"Amount\":").Append(DataPdCompare.dtCompare.Rows[i]["Amtinput"].ToString()).Append(",");
                    sbCompare.Append("\"Premium\":").Append(DataPdCompare.dtCompare.Rows[i]["ModePrem"].ToString()).Append(",");
                    sbCompare.Append("\"InsName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["InsName"].ToString()).Append("\",");
                    sbCompare.Append("\"CmpyName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["CompanyName"].ToString()).Append("\",");
                    sbCompare.Append("\"YName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["YName"].ToString()).Append("\",");
                    //  sbCompare.Append("\"MinVal\":").Append(DataPdCompare.dtCompare.Rows[i]["MinVal"].ToString()).Append(",");
                    //   sbCompare.Append("\"MaxVal\":").Append(DataPdCompare.dtCompare.Rows[i]["MaxVal"].ToString()).Append(",");
                    sbCompare.Append("\"SDate\":\"").Append(DateTime.Now.ToString("yyyy/MM/dd")).Append("\"");
                    sbCompare.Append("},");
                }
                Json = Json + sbCompare.ToString().Substring(0, sbCompare.Length - 1) + "]}";
                EX_DataAPI ed = new EX_DataAPI();
                DataTable dt = new DataTable();
                CompItemds = new DataSet();
                string InsItem = ed.CompareInsItem(DataPdCompare.dtCompare.Rows.Count.ToString(), "0", Json);
                if (InsItem != "{}" && InsItem.Length > 0)
                {
                    JObject jo = JObject.Parse(InsItem);
                    //  Descript":"保險公司","DLVL":"B","Item1":"ING安泰","Item2":null,"Item3":null,"Item4"
                    #region GTxtList
                    string GTxtList = jo["GTxtList"].ToString();
                    if (GTxtList != "[]" && GTxtList.Length > 0)
                        dt = JsonConvert.DeserializeObject<DataTable>(GTxtList);
                    else
                    {
                        dt.Columns.Add(new DataColumn("Descript", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("DLVL", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Item1", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Item2", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Item3", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Item4", System.Type.GetType("System.String")));
                    }
                    dt.TableName = "GTxtList";
                    CompItemds.Tables.Add(dt);
                    #endregion
                    //"ItemYValue":[{"Year":1,"Life1":135000.0,"Mbck1":0.0,"Life2":0.0,"Mbck2":0.0,"Life3":0.0,"Mbck3":0.0,"Life4":0.0,"Mbck4":0.0,"Flag":null}
                    #region ItemYValue
                    dt = new DataTable();
                    string ItemYValue = jo["ItemYValue"].ToString();
                    if (ItemYValue != "[]" && ItemYValue.Length > 0)
                        dt = JsonConvert.DeserializeObject<DataTable>(ItemYValue);
                    else
                    {
                        dt.Columns.Add(new DataColumn("Year", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Life1", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Mbck1", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Life2", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Mbck2", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Life3", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Mbck3", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Life4", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Mbck4", System.Type.GetType("System.Int32")));
                        dt.Columns.Add(new DataColumn("Flag", System.Type.GetType("System.Int32")));
                    }
                    dt.TableName = "ItemYValue";
                    CompItemds.Tables.Add(dt);
                    #endregion
                    //"PrftList":[{"Profit1":"全福儲蓄壽險","Value1":"","Lvl1":"A","GP1":null,"Profit2":null,"Value2":null,"Lvl2":null,"GP2":null,"Profit3":null,"Value3":null,"Lvl3":null,"GP3":null,"Profit4":null,"Value4":null,"Lvl4":null,"GP4":null}
                    #region PrftList
                    dt = new DataTable();
                    string PrftList = jo["PrftList"].ToString();
                    if (PrftList != "[]" && PrftList.Length > 0)
                        dt = JsonConvert.DeserializeObject<DataTable>(PrftList);
                    else
                    {
                        dt.Columns.Add(new DataColumn("Profit1", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Value1", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Lvl1", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("GP1", System.Type.GetType("System.String")));

                        dt.Columns.Add(new DataColumn("Profit2", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Value2", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Lvl2", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("GP2", System.Type.GetType("System.String")));

                        dt.Columns.Add(new DataColumn("Profit3", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Value3", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Lvl3", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("GP3", System.Type.GetType("System.String")));

                        dt.Columns.Add(new DataColumn("Profit4", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Value4", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Lvl4", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("GP4", System.Type.GetType("System.String")));

                    }
                    dt.TableName = "PrftList";
                    CompItemds.Tables.Add(dt);
                    #endregion
                    //"ItemList":[{"SNo":1,"Mark":"6EC","InsName":"ING安泰全福儲蓄壽險","YName":"6年期","Amount":"100,000","Premium":25640.0,"Target":null,"CmpyName":"ING安泰","Discount":0.0}
                    #region ItemList
                    dt = new DataTable();
                    string ItemList = jo["ItemList"].ToString();
                    if (ItemList != "[]" && ItemList.Length > 0)
                        dt = JsonConvert.DeserializeObject<DataTable>(ItemList);
                    else
                    {
                        dt.Columns.Add(new DataColumn("SNo", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Mark", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("InsName", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("YName", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Amount", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Premium", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Target", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("CmpyName", System.Type.GetType("System.String")));
                        dt.Columns.Add(new DataColumn("Discount", System.Type.GetType("System.String")));
                    }
                    dt.TableName = "ItemList";
                    CompItemds.Tables.Add(dt);
                    #endregion
                    // "NoPVItems":[{"Names":"ING安泰-6EC"}
                    #region NoPVItems
                    dt = new DataTable();
                    string NoPVItems = "";
                    if (jo["NoPVItems"] != null)
                        NoPVItems = jo["NoPVItems"].ToString();
                    if (NoPVItems != "[]" && NoPVItems.Length > 0)
                        dt = JsonConvert.DeserializeObject<DataTable>(NoPVItems);
                    else
                    {
                        dt.Columns.Add(new DataColumn("Names", System.Type.GetType("System.String")));
                    }
                    dt.TableName = "NoPVItems";
                    CompItemds.Tables.Add(dt);
                    #endregion

                    MultiView2.ActiveViewIndex = 1;
                    MultiView3.ActiveViewIndex = 0;

                    Ltl_CompareResult.Text = "";
                    #region sbCompareResult
                    StringBuilder sbCompareResult = new StringBuilder("");
                    sbCompareResult.Append("                            <table id=\"tbl_CompareResult\" style=\"display: table;border-spacing: 2px;border: 1px solid #000; width: 100%; max-width: 930px; height: auto; background-color: #cccccc;\" rules=\"all\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    sbCompareResult.Append("                                <tbody>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_1\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 228, 202);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[0]["Descript"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[0]["Item1"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[0]["Item2"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[0]["Item3"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[0]["Item4"].ToString()) + "</td>");
                    sbCompareResult.Append("                                    </tr>");
                    for (int i = 1; i < CompItemds.Tables["GTxtList"].Rows.Count; i++)
                    {
                        if (i <= 4)
                            sbCompareResult.Append("                                    <tr id=\"CompareRow_" + (i + 1).ToString() + "\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(197, 244, 226);\">");
                        else
                            sbCompareResult.Append("                                    <tr id=\"CompareRow_" + (i + 1).ToString() + "\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");

                        sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[i]["Descript"].ToString()) + "</td>");
                        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[i]["Item1"].ToString()) + "</td>");
                        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[i]["Item2"].ToString()) + "</td>");
                        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[i]["Item3"].ToString()) + "</td>");
                        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[i]["Item4"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    </tr>");
                    }
                    /*
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_3\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(197, 244, 226);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[2]["Descript"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[2]["Item1"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[2]["Item2"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[2]["Item3"].ToString()) + "</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">" + TdVal(CompItemds.Tables["GTxtList"].Rows[2]["Item4"].ToString()) + "</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_4\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(197, 244, 226);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保額/單位</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬元</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬元</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1仟元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_5\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(197, 244, 226);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">繳費年期</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">15年期</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">15年期</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">10年期</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_6\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(197, 244, 226);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">應繳保費</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">(男) 53,380元<br>");
                    sbCompareResult.Append("                                            (女) 56,240元</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">(男) 51,000元<br>");
                    sbCompareResult.Append("                                            (女) 51,600元</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">(男) 1,762元<br>");
                    sbCompareResult.Append("                                            (女) 2,366元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_7\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 保障儲蓄內容</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_8\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保障-一般身故(全殘)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1,868元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_9\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保障-意外身故(全殘)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1,868元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_10\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保障-癌症身故(全殘)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1,868元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_11\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保障-一般身故終身保障</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">100萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1.87萬</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_12\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">保障-投資型保障</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_13\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">儲蓄-生存金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_14\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">儲蓄-滿期/祝壽金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_15\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">儲蓄-年金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_16\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">重大疾病保險金(一次)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_17\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">重大傷病保險金(一次)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_18\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">特定傷病保險金(一次)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_19\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">特定傷病保險金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_20\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">看護/照顧保險金(一次)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">20萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">30萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1萬</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_21\" class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">看護/照顧扶助金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">24萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">20萬</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_22\" class=\"table_list01\" style=\"vertical-align: middle; border: 1px; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">看護/照顧扶助金(每月)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\">1,000元</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_23\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">全殘扶助金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_24\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 疾病醫療定額</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_25\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-住院日額(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_26\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-加護/燒燙傷(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_27\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-加護病房(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_28\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-住院前後門診</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_29\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-急診保險金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_30\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-緊急轉送保險金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_31\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-重大器官移植</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_32\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-出院/住院療養金(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_33\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-2~6(11)級殘廢保險金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_34\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-1~6(11)級殘廢扶助金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_35\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病-失能保險金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_36\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 意外醫療定額</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_37\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-住院日額(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_38\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-骨折/骨折未住院</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_39\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-加護/燒燙傷(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_40\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-加護病房(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_41\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-燒燙傷病房(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_42\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-重大燒燙傷</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_43\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-水陸航空身故保險金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_44\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-2~6(11)級殘廢保險金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_45\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-1~6(11)級殘廢扶助金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_46\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-失能保險金(每年)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_47\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 醫療限額(疾病/意外)</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_48\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外醫療限額</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_49\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">住院醫療限額(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_50\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">醫院雜費限額</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_51\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">住院手術限額</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_52\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">門診手術限額</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_53\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">加護/燒燙傷病房(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_54\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">燒燙傷病房(每日加計)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_55\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">可轉換日額(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_56\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 手術定額(疾病/意外)</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_57\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">住院手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_58\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">門診手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_59\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">特定/重大手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_60\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">疾病手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_61\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-住院手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_62\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">意外-門診手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_63\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">手術看護金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_64\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">手術療養金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_65\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">抗排斥藥物</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_66\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(231, 252, 255);\">");
                    sbCompareResult.Append("                                        <td colspan=\"31\" class=\"text02\" style=\"text-align: left; color: navy; font-size: 14px; font-weight: bold;\">&nbsp;※ 癌症醫療</td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_67\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-初次罹患癌症</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_68\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-原位/低侵襲性癌症</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_69\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-住院日額(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_70\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-門診醫療金(每次)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_71\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-出院療養金(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_72\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-住院收入補償金</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_73\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-化學/放射線治療</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_74\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-化學治療</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_75\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-放射線治療</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_76\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-外科手術</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_77\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-手術後住院(每日)</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_78\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-骨髓移植</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_79\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-義乳重建</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_80\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-義肢裝設</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                    <tr id=\"CompareRow_81\" class=\"table_list01\" style=\"vertical-align: middle; display: none; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td class=\"text02\" style=\"text-align: left; color: rgb(0, 0, 0); width: 230px;\">癌症-義齒裝設</td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 170px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    */
                    sbCompareResult.Append("                                </tbody>");
                    sbCompareResult.Append("                            </table>");
                    Ltl_CompareResult.Text = sbCompareResult.ToString();
                    #endregion
                    Ltl_graph.Text = "";
                    #region graph
                    sbCompareResult = new StringBuilder("");


                    sbCompareResult.Append("                    <div id=\"div_pdtlist\" style=\"width: 100%;\">");
                    sbCompareResult.Append("                        <table id=\"tbl_pdtList\" style=\"display: table;border: 1px solid #000;width: 100%; height: auto;\"  rules=\"all\"  border=\"0\" cellspacing=\"2\" cellpadding=\"2\">");
                    sbCompareResult.Append("                            <tbody>");
                    sbCompareResult.Append("                                <tr style=\"background-color: rgb(188, 233, 245); height: 30px;\">");
                    sbCompareResult.Append("                                    <td rowspan=\"2\"  style=\"width: 80px; text-align: center;\" class=\"text03\">年度</td>");
                    for (int i = 0; i < 4; i++)
                    {
                        if (CompItemds.Tables["ItemList"].Rows.Count > i)
                            sbCompareResult.Append("                                    <td colspan=\"2\" style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemList"].Rows[i]["InsName"].ToString()) + "</td>");
                        else
                            sbCompareResult.Append("                                    <td colspan=\"2\" style=\"width: 120px; text-align: center;\" class=\"text03\">&nbsp;</td>");
                    }
                    sbCompareResult.Append("                                </tr>");
                    sbCompareResult.Append("                                <tr style=\"background-color: rgb(188, 233, 245); height: 30px;\">");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度保障</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度領回</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度保障</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度領回</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度保障</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度領回</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度保障</td>");
                    sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">年度領回</td>");
                    sbCompareResult.Append("                                </tr>");
                    for (int j = 0; j < CompItemds.Tables["ItemYValue"].Rows.Count; j++)
                    {
                        sbCompareResult.Append("<tr>");
                        sbCompareResult.Append("                                    <td style=\"width: 80px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Year"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Life1"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Mbck1"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Life2"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Mbck2"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Life3"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Mbck3"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Life4"].ToString()) + "</td>");
                        sbCompareResult.Append("                                    <td style=\"width: 120px; text-align: center;\" class=\"text03\">" + TdVal(CompItemds.Tables["ItemYValue"].Rows[j]["Mbck4"].ToString()) + "</td>");
                        sbCompareResult.Append("                                </tr>");

                    }
                    sbCompareResult.Append("                            </tbody>");
                    sbCompareResult.Append("                        </table>");
                    sbCompareResult.Append("                    </div>");



                    //sbCompareResult.Append("                            <table id=\"tbl_graph\" style=\"display: table; width: 100%; height: auto;\" cellspacing=\"0\" cellpadding=\"0\">");
                    //sbCompareResult.Append("                                <tbody>");

                    //for (int i = 0; i < CompItemds.Tables["ItemList"].Rows.Count; i++)
                    //{

                    //        sbCompareResult.Append("                                    <tr id=\"graphrrow_0\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        sbCompareResult.Append("                                        <td colspan=\"2\" class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">" + TdVal(CompItemds.Tables["ItemList"].Rows[i]["InsName"].ToString()) + "</span></td>");
                    //        sbCompareResult.Append("                                    </tr>");
                    //        sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(204, 204, 204);\">");
                    //        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                    //        sbCompareResult.Append("                                            <div id=\"div_graph_0\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                    //        sbCompareResult.Append("                                                <table id=\"tbl_graph_" + (i + 1).ToString() + "\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    //        sbCompareResult.Append("                                                    <tbody>");
                    //        sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); vertical-align: top; width: 50%;\">");
                    //        sbCompareResult.Append("                                                                <img id=\"img_compare_81\" width=\"450\" height=\"300\" src=\"./resources/img/demo1.jpg\"></td>");
                    //        sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); width: 50%; vertical-align: top;\">");
                    //        sbCompareResult.Append("                                                                <div id=\"div_desc_" + (i + 1).ToString() + "\" style=\"width: 100%; height: 300px; overflow: auto;\">");
                    //        sbCompareResult.Append("                                                                    <table id=\"tbl_desc_" + (i + 1).ToString() + "\" style=\"width: auto; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    //        sbCompareResult.Append("                                                                        <tbody>");
                    //    for (int j = 0; j < CompItemds.Tables["PrftList"].Rows.Count; j++)
                    //    {

                    //        sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">" + TdVal(CompItemds.Tables["PrftList"].Rows[j]["Profit" + (i + 1).ToString()].ToString()) + TdVal(CompItemds.Tables["PrftList"].Rows[j]["Value" + (i + 1).ToString()].ToString()) + "</td>");
                    //        sbCompareResult.Append("                                                                            </tr>");
                    //        //sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        //sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">2. 祝壽金：滿96歲給付保額1倍</td>");
                    //        //sbCompareResult.Append("                                                                            </tr>");
                    //        //sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        //sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">3. 長期看護療養金：保額20%</td>");
                    //        //sbCompareResult.Append("                                                                            </tr>");
                    //        //sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        //sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">4. 長期看護保險金：保額12%</td>");
                    //        //sbCompareResult.Append("                                                                            </tr>");
                    //        //sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        //sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">5. 豁免保費：符合「長期看護狀態」</td>");
                    //        //sbCompareResult.Append("                                                                            </tr>");
                    //        //sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        //sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">◎新重大燒燙傷保險給付附加條款：保額25%</td>");
                    //        //sbCompareResult.Append("                                                                            </tr>");
                    //    }
                    //        sbCompareResult.Append("                                                                        </tbody>");
                    //        sbCompareResult.Append("                                                                    </table>");
                    //        sbCompareResult.Append("                                                                </div>");
                    //        sbCompareResult.Append("                                                            </td>");
                    //        sbCompareResult.Append("                                                        </tr>");
                    //        sbCompareResult.Append("                                                    </tbody>");
                    //        sbCompareResult.Append("                                                </table>");
                    //        sbCompareResult.Append("                                            </div>");
                    //        sbCompareResult.Append("                                        </td>");
                    //        sbCompareResult.Append("                                    </tr>");
                    //        sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //        sbCompareResult.Append("                                        <td colspan=\"2\" style=\"text-align: left; color: rgb(0, 0, 0); height: 25px;\"></td>");
                    //        sbCompareResult.Append("                                    </tr>");
                    //}
                    ////sbCompareResult.Append("                                    <tr id=\"graphrrow_1\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                        <td colspan=\"2\" class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">國泰  K4  新松柏長期看護終身壽險</span></td>");
                    ////sbCompareResult.Append("                                    </tr>");
                    ////sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(204, 204, 204);\">");
                    ////sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                    ////sbCompareResult.Append("                                            <div id=\"div_graph_1\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                    ////sbCompareResult.Append("                                                <table id=\"tbl_graph_1\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    ////sbCompareResult.Append("                                                    <tbody>");
                    ////sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); vertical-align: top; width: 50%;\">");
                    ////sbCompareResult.Append("                                                                <img id=\"img1\" width=\"450\" height=\"300\" src=\"./resources/img/demo2.jpg\" alt=\"\"></td>");
                    ////sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); width: 50%; vertical-align: top;\">");
                    ////sbCompareResult.Append("                                                                <div id=\"div_desc_1\" style=\"width: 100%; height: 300px; overflow: auto;\">");
                    ////sbCompareResult.Append("                                                                    <table id=\"tbl_desc_1\" style=\"width: auto; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    ////sbCompareResult.Append("                                                                        <tbody>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">1. 身故或喪葬費用、第一級殘廢保險金：保額1倍</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">2. 長期看護復健保險金：保額30%</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">3. 長期看護保險金：保額10%</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">4. 退還未到期保費：按日數比例計算當期已繳付</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">5. 豁免保費：長期看護狀態</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                        </tbody>");
                    ////sbCompareResult.Append("                                                                    </table>");
                    ////sbCompareResult.Append("                                                                </div>");
                    ////sbCompareResult.Append("                                                            </td>");
                    ////sbCompareResult.Append("                                                        </tr>");
                    ////sbCompareResult.Append("                                                    </tbody>");
                    ////sbCompareResult.Append("                                                </table>");
                    ////sbCompareResult.Append("                                            </div>");
                    ////sbCompareResult.Append("                                        </td>");
                    ////sbCompareResult.Append("                                    </tr>");
                    ////sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                        <td colspan=\"2\" style=\"text-align: left; color: rgb(0, 0, 0); height: 25px;\"></td>");
                    ////sbCompareResult.Append("                                    </tr>");
                    ////sbCompareResult.Append("                                    <tr id=\"graphrrow_2\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                        <td colspan=\"2\" class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">臺銀  EF  樂活人生長期看護終身保險</span></td>");
                    ////sbCompareResult.Append("                                    </tr>");
                    ////sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(204, 204, 204);\">");
                    ////sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                    ////sbCompareResult.Append("                                            <div id=\"div_graph_2\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                    ////sbCompareResult.Append("                                                <table id=\"tbl_graph_2\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    ////sbCompareResult.Append("                                                    <tbody>");
                    ////sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); vertical-align: top; width: 50%;\">");
                    ////sbCompareResult.Append("                                                                <img id=\"img2\" width=\"450\" height=\"300\" src=\"./resources/img/demo3.jpg\"></td>");
                    ////sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: center; color: rgb(0, 0, 0); width: 50%; vertical-align: top;\">");
                    ////sbCompareResult.Append("                                                                <div id=\"div_desc_2\" style=\"width: 100%; height: 300px; overflow: auto;\">");
                    ////sbCompareResult.Append("                                                                    <table id=\"tbl_desc_2\" style=\"width: auto; height: auto;\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
                    ////sbCompareResult.Append("                                                                        <tbody>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">1. 身故保險金或喪葬費用保險金：</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">年繳保費總和1.06倍</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">扣除已給付之長期看護關懷保險金及長期看護保險金之餘額</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">2. 祝壽保險金(達110歲)：</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">年繳保費總和1.06倍</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">扣除已給付之長期看護關懷保險金及長期看護保險金之餘額</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">3. 長期看護關懷保險金(以一次為限)：</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">持續符合長期看護狀態者，</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">保額10倍</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">4. 長期看護保險金(每月)(以180次為限)：</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">持續符合長期看護狀態者，</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">保額1倍</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">5. 豁免保險費：</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">(1) 符合長期看護狀態者</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">(2) 致成第1~3殘廢等級者</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                            <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                                                                <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0);\">免繳爾後保費</td>");
                    ////sbCompareResult.Append("                                                                            </tr>");
                    ////sbCompareResult.Append("                                                                        </tbody>");
                    ////sbCompareResult.Append("                                                                    </table>");
                    ////sbCompareResult.Append("                                                                </div>");
                    ////sbCompareResult.Append("                                                            </td>");
                    ////sbCompareResult.Append("                                                        </tr>");
                    ////sbCompareResult.Append("                                                    </tbody>");
                    ////sbCompareResult.Append("                                                </table>");
                    ////sbCompareResult.Append("                                            </div>");
                    ////sbCompareResult.Append("                                        </td>");
                    ////sbCompareResult.Append("                                    </tr>");

                    ////sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    ////sbCompareResult.Append("                                        <td colspan=\"2\" style=\"text-align: left; color: rgb(0, 0, 0); height: 25px;\"></td>");
                    ////sbCompareResult.Append("                                    </tr>");

                    //sbCompareResult.Append("                                </tbody>");
                    //sbCompareResult.Append("                            </table>");

                    Ltl_graph.Text = sbCompareResult.ToString();
                    #endregion
                    Ltl_rider.Text = "";
                    #region rider
                    sbCompareResult = new StringBuilder("");
                    sbCompareResult.Append("                            <table id=\"tbl_rider\" style=\"display: table;border: 1px solid #000; width: 100%; max-width: 600px; height: auto;\" cellspacing=\"0\" cellpadding=\"0\">");
                    sbCompareResult.Append("                                <tbody>");
                    for (int i = 0; i < CompItemds.Tables["ItemList"].Rows.Count; i++)
                    {

                        sbCompareResult.Append("                                    <tr id=\"riderrow_" + (i + 1).ToString() + "\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        sbCompareResult.Append("                                        <td class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">" + TdVal(CompItemds.Tables["ItemList"].Rows[i]["InsName"].ToString()) + "</span></td>");
                        sbCompareResult.Append("                                    </tr>");
                        sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                        sbCompareResult.Append("                                            <div id=\"div_rider_" + (i + 1).ToString() + "\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                        sbCompareResult.Append("                                                <table id=\"tbl_rider_" + (i + 1).ToString() + "\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"2px\" cellpadding=\"2px\">");
                        sbCompareResult.Append("                                                    <tbody>");
                        for (int j = 0; j < CompItemds.Tables["PrftList"].Rows.Count; j++)
                        {

                            //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                            //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">" + TdVal(CompItemds.Tables["PrftList"].Rows[j]["Profit" + (i + 1).ToString()].ToString()) +"</td>");
                            //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">本人</td>");
                            //sbCompareResult.Append("                                                        </tr>");
                            if (CompItemds.Tables["PrftList"].Rows[j]["Profit" + (i + 1).ToString()].ToString().Length > 0)
                            {
                                sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                                sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">" + TdVal(CompItemds.Tables["PrftList"].Rows[j]["Profit" + (i + 1).ToString()].ToString()) + "</td>");
                                sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">" + TdVal(CompItemds.Tables["PrftList"].Rows[j]["Value" + (i + 1).ToString()].ToString()) + "</td>");
                                sbCompareResult.Append("                                                        </tr>");
                            }
                        }
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">本公司按保險金額之20%給付長期看護療養金。</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">20萬</td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">．長期看護保險金</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">本公司按保險金額之12%每半年給付一次長期看護保險金。</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">12萬</td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">．豁免保費</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">本公司豁免本契約於「長期看護狀態期間」之保險費。</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">．祝壽保險金</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">至96歲給付保額</td>");
                        //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                        //sbCompareResult.Append("                                                        </tr>");
                        sbCompareResult.Append("                                                    </tbody>");
                        sbCompareResult.Append("                                                </table>");
                        sbCompareResult.Append("                                            </div>");
                        sbCompareResult.Append("                                        </td>");
                        sbCompareResult.Append("                                    </tr>");
                    }
                    //sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"></td>");
                    //sbCompareResult.Append("                                    </tr>");
                    //sbCompareResult.Append("                                    <tr id=\"riderrow_1\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">國泰  K4  新松柏長期看護終身壽險</span></td>");
                    //sbCompareResult.Append("                                    </tr>");
                    //sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                    //sbCompareResult.Append("                                            <div id=\"div_rider_1\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                    //sbCompareResult.Append("                                                <table id=\"tbl_rider_1\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"2px\" cellpadding=\"2px\">");
                    //sbCompareResult.Append("                                                    <tbody>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">【國泰人壽松柏長期看護終身壽險】</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">本人</td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎長期看護復健保險金(終身限1次)</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">30萬</td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">若符合長期看護狀態，則於「免責期間」終了翌日按保險金額</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">30%給付。</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎長期看護保險金</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">領取「長期看護復健保險金」後，每滿半年給付保險金額10%</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">10萬</td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎退還未到期保費</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">繳費期間身故或殘廢退還未到期保費</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                    </tbody>");
                    //sbCompareResult.Append("                                                </table>");
                    //sbCompareResult.Append("                                            </div>");
                    //sbCompareResult.Append("                                        </td>");
                    //sbCompareResult.Append("                                    </tr>");
                    //sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"></td>");
                    //sbCompareResult.Append("                                    </tr>");
                    //sbCompareResult.Append("                                    <tr id=\"riderrow_2\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td class=\"table01\" style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"><span class=\"tittle01\">臺銀  EF  樂活人生長期看護終身保險</span></td>");
                    //sbCompareResult.Append("                                    </tr>");
                    //sbCompareResult.Append("                                    <tr class=\"table_list01\" style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                        <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: auto; height: auto;\">");
                    //sbCompareResult.Append("                                            <div id=\"div_rider_2\" style=\"text-align: center; width: 100%; height: auto; overflow: auto; background-color: #EEEEEE;\">");
                    //sbCompareResult.Append("                                                <table id=\"tbl_rider_2\" style=\"width: 100%; height: auto;\" border=\"0\" cellspacing=\"2px\" cellpadding=\"2px\">");
                    //sbCompareResult.Append("                                                    <tbody>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">【臺銀人壽樂活人生長期看護終身保險】(EF)</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎身故保險金或喪葬費用保險金</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">年繳保費總和1.06倍，扣除已給付之</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">長期看護關懷保險金及長期看護保險金之餘額</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎祝壽保險金(達110歲)</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">年繳保費總和1.06倍，扣除已給付之</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">長期看護關懷保險金及長期看護保險金之餘額</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎長期看護關懷保險金(以一次為限)</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">持續符合長期看護狀態者，</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">保額10倍</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">1萬</td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎長期看護保險金(每月)(以180次為限)</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">持續符合長期看護狀態者，</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">保額1倍</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\">1,000元</td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">◎豁免保險費</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">(1) 符合長期看護狀態者</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">(2) 致成第1~3殘廢等級者</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                        <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: left; color: rgb(0, 0, 0); width: 550px; border: 1px solid rgb(204, 202, 202);\">免繳爾後保費</td>");
                    //sbCompareResult.Append("                                                            <td class=\"text03\" style=\"text-align: right; color: rgb(0, 0, 0); width: 200px; border: 1px solid rgb(188, 233, 245);\"></td>");
                    //sbCompareResult.Append("                                                        </tr>");
                    //sbCompareResult.Append("                                                    </tbody>");
                    //sbCompareResult.Append("                                                </table>");
                    //sbCompareResult.Append("                                            </div>");
                    //sbCompareResult.Append("                                        </td>");
                    //sbCompareResult.Append("                                    </tr>");

                    sbCompareResult.Append("                                    <tr style=\"vertical-align: middle; background-color: rgb(255, 255, 255);\">");
                    sbCompareResult.Append("                                        <td style=\"text-align: left; color: rgb(0, 0, 0); height: 20px;\"></td>");
                    sbCompareResult.Append("                                    </tr>");
                    sbCompareResult.Append("                                </tbody>");
                    sbCompareResult.Append("                            </table>");


                    Ltl_rider.Text = sbCompareResult.ToString();

                    #endregion
                    sbCompare = new StringBuilder("");
                    //          //   sbCompare = new StringBuilder("<HTML><head>");
                    //             sbCompare.Append("        <script type='text/javascript'>   ");
                    //             sbCompare.Append("            function doPrint() {    ");
                    //             sbCompare.Append("                bdhtml=window.document.body.innerHTML;    ");
                    //             sbCompare.Append("                sprnstr='<!--startprint-->';    ");
                    //             sbCompare.Append("                eprnstr='<!--endprint-->';    ");
                    //             sbCompare.Append("                prnhtml=bdhtml.substr(bdhtml.indexOf(sprnstr)+17);    ");
                    //             sbCompare.Append("                prnhtml=prnhtml.substring(0,prnhtml.indexOf(eprnstr));    ");
                    //             sbCompare.Append("                console.log(prnhtml); ");
                    //             sbCompare.Append("                window.document.body.innerHTML=prnhtml; ");
                    //             sbCompare.Append("                window.print();  ");
                    ////             sbCompare.Append("                window.document.body.innerHTML='';");
                    //             sbCompare.Append("                window.close();");
                    //             sbCompare.Append("        }    ");
                    //             sbCompare.Append("        </script>");
                    //            // sbCompare.Append("</head><BODY onload='window.print();window.close()'><PRE><!--startprint-->");
                    //             sbCompare.Append("</head><BODY onload='doPrint()'><PRE><!--startprint-->");
                    //sbCompare.Append("</head><BODY><PRE><!--startprint-->");

                    sbCompare.Append("<div STYLE=\"page-break-after: always;\">");
                    sbCompare.Append("<table  style=\"width:713px;border: 0px none;\" cellpadding=\"2\" id=\"ReportHeader0\"><tbody><tr><td style=\"text-align:center;height:60px;\"><div style=\"position:relative;width:100%;height:100%;\"><div style=\"position:relative;text-align:center;top:25px;\"><span class=\"p_caption\">商品分析比較表</span></div></div></td></tr></tbody></table>");
                    sbCompare.Append(Ltl_CompareResult.Text);
                    sbCompare.Append("</div>");
                    sbCompare.Append("<div STYLE=\"page-break-after: always;\">");
                    sbCompare.Append("<table  style=\"width:713px;border: 0px none;\" cellpadding=\"2\" id=\"ReportHeader1\"><tbody><tr><td style=\"text-align:center;height:60px;\"><div style=\"position:relative;width:100%;height:100%;\"><div style=\"position:relative;text-align:center;top:25px;\"><span class=\"p_caption\">保障明細比較表</span></div></div></td></tr></tbody></table>");
                    sbCompare.Append(Ltl_graph.Text);
                    sbCompare.Append("</div>");
                    sbCompare.Append("<div STYLE=\"page-break-after: always;\">");
                    sbCompare.Append("<table  style=\"width:713px;border: 0px none;\" cellpadding=\"2\" id=\"ReportHeader2\"><tbody><tr><td style=\"text-align:center;height:60px;\"><div style=\"position:relative;width:100%;height:100%;\"><div style=\"position:relative;text-align:center;top:25px;\"><span class=\"p_caption\">投保利益比較表</span></div></div></td></tr></tbody></table>");
                    sbCompare.Append(Ltl_rider.Text);
                    sbCompare.Append("</div>");
                    //sbCompare.Append("<!--endprint-->");
                    //sbCompare.Append("</PRE></BODY>");
                    ////sbCompare.Append("        <script type=\"text/javascript\">   ");
                    ////sbCompare.Append("          setTimeout(doPrint(), 1000);");
                    ////sbCompare.Append("        </script>");
                    //sbCompare.Append("</HTML>");

                    //   Session["PdComparePrn"] = sbCompare.ToString();
                    ViewState["PdComparePrn"] = sbCompare.ToString();
                    //   tab3.OnClientClick = "window.open('PdComparePrn.aspx','_blank','menubar=no,status=no,scrollbars=yes,top=100,left=200,toolbar=no,width=450,height=300');";
                    //   tab3.OnClientClick = "window.open('PdComparePrn.aspx','_blank','');";
                    //"var printPage = window.open('','printPage','');"+
                    //"printPage.document.open();"+
                    //"printPage.document.write('" + sbCompare.ToString() + "');";

                    //"printPage.document.write('<HTML><head></head><BODY onload=window.print();window.close() >');"+
                    //"printPage.document.write('<PRE>');"+
                    //"printPage.document.write(value);"+
                    //"printPage.document.write('</PRE>');"+
                    //"printPage.document.close('</BODY></HTML>');";

                }
            }
        }

        protected void tab3_Click(object sender, EventArgs e)
        {
            int InsAge = DataPdCompare.InsAge(txt_birth.Text);

            string Json = "{\"FamilyFeeTo\":1,\"ComunityName\":\"\",\"PPLMan\":[{\"PKID\":\"" + System.Guid.NewGuid().ToString() + "\",\"Relation\":null,\"Name\":\"本人\",\"BirthDay\":\"" + txt_birth.Text + "T00:00:00\",\"RAge\":0,\"Age\":" + InsAge.ToString() + ",\"Sex\":" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + ",\"Level\":" + ddljob1.SelectedItem + ",\"CareerCode\":null,\"AddCost\":0.0,\"CurrentAge\":0},";
             Json = Json + "{\"PKID\":\"00000000-0000-0000-0000-000000000000\",\"Relation\":null,\"Name\":\"陳志豪\",\"BirthDay\":\"1981-10-16T00:00:00\",\"RAge\":0,\"Age\":36,\"Sex\":0,\"Level\":1,\"CareerCode\":null,\"AddCost\":0.0,\"CurrentAge\":0}],";
             Json = Json + "\"UserBoxList\":[{\"CstmLifeID\":\"Z123456789\",";
             Json = Json + "\"PolBox\":[{\"BfyNames\":\"陳志豪;直系血親尊親屬\",";
             Json = Json + "\"Policy\":{\"Guid\":\"00000000-0000-0000-0000-000000000000\",\"InsType\":0,\"PolicyID\":\"IG2500\",\"CSTMGuid\":\"00000000-0000-0000-0000-000000000000\",\"SNO\":null,\"INRCMPY\":\"保德信\",\"Insurer\":\"陳志豪\",\"ApplicantGuid\":null,\"Applicant\":\"陳爸爸\",\"PolicyDate\":null,\"Status\":\"有效保單\",\"Payment\":\"年繳\",\"Age\":28,\"Charge\":null,\"Amount\":null,\"Premiums\":null,\"Discount\":null,\"Address\":null,\"CalYear\":null,\"Benefit\":null,\"CDate\":null,\"SelfDefined\":null,\"UserDate1\":null,\"UserDate2\":null,\"AdjedAmount\":null,\"ETIDate\":null,\"ETIMoney\":null,\"PolicyID2\":null,\"InsrTarget\":null,\"CarGuid\":null,\"mortgage\":null,\"Remark\":null,\"EditID\":null,\"SetID\":null,\"ModifyDate\":null,\"PolicyFeeRecords\":[],\"PolicyServices\":[],\"PolicyItems\":[],\"Beneficiaries\":[],\"Selected\":false},";
             Json = Json + "\"PolicyItem\":[{\"Guid\":\"00000000-0000-0000-0000-000000000000\",\"PolicyGuid\":\"00000000-0000-0000-0000-000000000000\",\"InrCmpy\":\"保德信\",\"SNO\":null,\"Mark\":\"10F55A\",\"ItemName\":\"家用保障定期保險(繳費15年保障至65歲)\",\"YName\":\"15年期\",\"Target\":null,\"Amount\":\"180000\",\"Premium\":0.0,\"SDate\":null,\"SPEC\":null,\"VULPARM\":null,\"Status\":null,\"AdjedAmount\":null,\"AdjDate\":null,\"ETIDate\":null,\"EtiMoney\":null,\"ModifyDate\":null,\"Policy\":null}]}]}]}";
             Json = Json + "\"PMans\":[{\"Identity\":null,\"PKID\":\"\",\"CName\":\"\",\"Sex\":\"" + (rbl_sex.SelectedValue == "男" ? "0" : "1") + "\",\"Birthday\":\"" + txt_birth.Text + "\",\"Age\":\"" + InsAge.ToString() + "\",\"CLevel\":\"" + ddljob1.SelectedItem + "\"}],";
            sbCompare = new StringBuilder("\"insList\":[");
            for (int i = 0; i < DataPdCompare.dtCompare.Rows.Count; i++)
            {
                //公司 險種代碼 險種名稱 年期 保額 保費 刪除
                sbCompare.Append("{");
                sbCompare.Append("\"DispGrade\":\"").Append(DataPdCompare.dtCompare.Rows[i]["DispClass"].ToString()).Append("\",");
                sbCompare.Append("\"CmpyNo\":\"").Append(DataPdCompare.dtCompare.Rows[i]["CompanyNo"].ToString()).Append("\",");
                sbCompare.Append("\"IMark\":\"").Append(DataPdCompare.dtCompare.Rows[i]["Mark"].ToString()).Append("\",");
                sbCompare.Append("\"DispMark\":\"").Append(DataPdCompare.dtCompare.Rows[i]["DispMark"].ToString()).Append("\",");
                sbCompare.Append("\"Yearly\":\"").Append(DataPdCompare.dtCompare.Rows[i]["Yearly"].ToString()).Append("\",");
                sbCompare.Append("\"Target\":1,");
                sbCompare.Append("\"Amount\":").Append(DataPdCompare.dtCompare.Rows[i]["Amtinput"].ToString()).Append(",");
                sbCompare.Append("\"Premium\":").Append(DataPdCompare.dtCompare.Rows[i]["ModePrem"].ToString()).Append(",");
                sbCompare.Append("\"InsName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["InsName"].ToString()).Append("\",");
                sbCompare.Append("\"CmpyName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["CompanyName"].ToString()).Append("\",");
                sbCompare.Append("\"YName\":\"").Append(DataPdCompare.dtCompare.Rows[i]["YName"].ToString()).Append("\",");
                //  sbCompare.Append("\"MinVal\":").Append(DataPdCompare.dtCompare.Rows[i]["MinVal"].ToString()).Append(",");
                //   sbCompare.Append("\"MaxVal\":").Append(DataPdCompare.dtCompare.Rows[i]["MaxVal"].ToString()).Append(",");
                sbCompare.Append("\"SDate\":\"").Append(DateTime.Now.ToString("yyyy/MM/dd")).Append("\"");
                sbCompare.Append("},");
            }
            Json = Json + sbCompare.ToString().Substring(0, sbCompare.Length - 1) + "]}";
            EX_DataAPI ed = new EX_DataAPI();
            DataTable dt = new DataTable();
            DataSet CompItemds = new DataSet();
            string InsItem = ed.CompareInsItem(DataPdCompare.dtCompare.Rows.Count.ToString(),"0", Json);
            if (InsItem != "{}" && InsItem.Length > 0)
            {
                JObject jo = JObject.Parse(InsItem);
                //  Descript":"保險公司","DLVL":"B","Item1":"ING安泰","Item2":null,"Item3":null,"Item4"
                string GTxtList = jo["GTxtList"].ToString();
                if (GTxtList != "[]" && GTxtList.Length > 0)
                    dt = JsonConvert.DeserializeObject<DataTable>(GTxtList);
                else
                {
                    dt.Columns.Add(new DataColumn("Descript", System.Type.GetType("System.String")));
                    dt.Columns.Add(new DataColumn("DLVL", System.Type.GetType("System.String")));
                    dt.Columns.Add(new DataColumn("Item1", System.Type.GetType("System.String")));
                    dt.Columns.Add(new DataColumn("Item2", System.Type.GetType("System.String")));
                    dt.Columns.Add(new DataColumn("Item3", System.Type.GetType("System.String")));
                    dt.Columns.Add(new DataColumn("Item4", System.Type.GetType("System.String")));
                }
                dt.TableName = "GTxtList";
                CompItemds.Tables.Add(dt);
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 0;
            Button1.Focus();
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 1;
            Button2.Focus();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            MultiView3.ActiveViewIndex = 2;
            Button3.Focus();

        }

        protected void tab3_Click1(object sender, EventArgs e)
        {
            if (ViewState["PdComparePrn"] != null)
            {
                ScriptManager.RegisterClientScriptBlock(
                this, HttpContext.Current.GetType(), "Print",
                string.Format("printScreen('{0}','');", ViewState["PdComparePrn"].ToString()),
                true); 
                //ScriptManager.RegisterClientScriptBlock(
                //this, HttpContext.Current.GetType(), "Print",
                //string.Format("printScreen('{0}','');", Session["PdComparePrn"].ToString()),
                //true);
            }
        }


    }
}