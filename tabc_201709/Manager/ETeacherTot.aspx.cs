﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ETeacherTot : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EH_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(selTeacher_ET_TName);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(selTeacher_ET_TName);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataETeacherTot.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year - 1).ToString(), (DateTime.Now.Year - 1).ToString()));
            ddlSh_year.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlSh_year.Items.Add(new ListItem((DateTime.Now.Year + 1).ToString(), (DateTime.Now.Year + 1).ToString()));
            ddlSh_year.SelectedValue = DateTime.Now.Year.ToString();
            ddlSh_Month.SelectedValue = DateTime.Now.Month.ToString();
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selTeacher_ET_TName.SelectedValue.Trim() == "")
                sbError.Append("●「授課講師」必須選取!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EH_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EH_UidNo":
                            hif_EH_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Company":
                            lbl_ET_Company.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_Department":
                            lbl_ET_Department.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ET_TransNo":
                            hif_ET_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(hif_ET_TransNo.Value);
                            if (dr_ET != null)
                            {
                                selTeacher_ET_TName.search(dr_ET["ET_TName"].ToString());
                                selTeacher_ET_TName.SelectedValue = hif_ET_TransNo.Value;
                            }
                            break;
                        case "YearMonth":
                            lbl_EH_Year_Month.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EH_Hours":
                            lbl_EH_Hours.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EH_Points":
                            lbl_EH_Points.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EH_ShouldPay":
                            lbl_EH_ShouldPay.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EH_RealPay":
                            lbl_EH_RealPay.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;


                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selTeacher_ET_TName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
            if (dr != null)
            {
                lbl_ET_Company.Text = dr["ET_Company"].ToString();
                lbl_ET_Department.Text = dr["ET_Department"].ToString();
            }
        }
        protected void selTeacher_ET_TName_TextChanged(object sender, EventArgs e)
        {
            
        }
        protected void selTeacher_ET_TName_SearchButtonClick(object sender, EventArgs e)
        {
            
        }
        protected void selTeacher_ET_TName_RefreshButtonClick(object sender, EventArgs e)
        {
            DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(hif_ET_TransNo.Value);
            if (dr_ET != null)
            {
                selTeacher_ET_TName.search(hif_ET_TransNo.Value);
                selTeacher_ET_TName.SelectedValue = hif_ET_TransNo.Value;

            }
        }

        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EH_UidNo.Value = Session["UidNo"].ToString();
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataETeacherTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataETeacherTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Excute": //計算
                    {
                        int weight = 1;
                        double EH_Hours_count = 0;
                        DataTable dt_ec = DataECourse.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue, "2").Tables[0];
                        List<DataRow> list_ec = dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[0] && r.Field<DateTime>("EC_SDate").Month.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[1]).ToList();
                        foreach(DataRow dr_ec in list_ec)
                        {
                            string EW_CUnit = "", EW_TUnit = "";
                            DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr_ec["EC_Unit"].ToString());
                            if (dr_p != null)
                                EW_CUnit = dr_p["TypeName"].ToString();
                            DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
                            if (dr_ET != null)
                            {
                                dr_p = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr_ET["ET_Unit"].ToString());
                                if (dr_p != null)
                                    EW_TUnit = dr_p["TypeName"].ToString();
                            }
                            DataRow dr_weight = DataEWeightM.DataReader_EW_WeightM(EW_CUnit, EW_TUnit);
                            if(dr_weight != null)
                            {
                                weight = int.Parse(dr_weight["EW_WeightM"].ToString());
                            }
                        }
                        EH_Hours_count = dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[0] && r.Field<DateTime>("EC_SDate").Month.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[1]).Sum(r => r.Field<double>("EC_Hours"));
                        lbl_EH_Hours.Text = EH_Hours_count.ToString();
                        lbl_EH_Points.Text = (EH_Hours_count * weight).ToString();
                        lbl_EH_ShouldPay.Text = Math.Round(dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[0] && r.Field<DateTime>("EC_SDate").Month.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[1]).Sum(r => r.Field<double>("EC_Hours") * r.Field<int>("EC_TeacherCost"))).ToString();
                        DataTable dt_e6 = DataETeacherFee.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue).Tables[0];
                        lbl_EH_RealPay.Text = dt_e6.AsEnumerable().Where(r => r.Field<DateTime>("E6_TransDate").Year.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[0] && r.Field<DateTime>("E6_TransDate").Month.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[1]).Sum(r => r.Field<int>("E6_RealPay")).ToString();
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataETeacherTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataETeacherTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
                case "Export_all": //總表
                    ExportControls.CreateCSVFile(Page, ViewState["dt_export"] as DataTable, string.Format("講師授課時數總表_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
                    break;
                case "Export_datail": //明細表
                    ExportControls.CreateCSVFile(Page, ViewState["dt_export"] as DataTable, string.Format("講師授課時數明細表_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
                    break;
                case "Export_score": //加權計分表
                    ExportControls.CreateCSVFile(Page, ViewState["dt_export"] as DataTable, string.Format("講師授課時數加權計分表_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EH_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EH_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EH_UidNo", hif_EH_UidNo.Value);
            }
            DataRow dr_teacher = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
            if (dr_teacher != null)
            {
                hsData.Add("ET_TransNo", dr_teacher["ET_TransNo"].ToString());
                hsData.Add("ET_UidNo", dr_teacher["ET_UidNo"].ToString());
            }
            hsData.Add("EH_Year", DateTime.Now.Year.ToString("00"));
            hsData.Add("EH_Month", DateTime.Now.Month.ToString("00"));
            hsData.Add("EH_Hours", lbl_EH_Hours.Text);
            hsData.Add("EH_Points", lbl_EH_Points.Text);
            hsData.Add("EH_ShouldPay", lbl_EH_ShouldPay.Text);
            hsData.Add("EH_RealPay", lbl_EH_RealPay.Text);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_batch(string ModiState, string ET_TransNo, string EH_Year, string EH_Month, string EH_TransNo, string EH_UidNo,
            string EH_Hours, string EH_Points, string EH_ShouldPay, string EH_RealPay)
        {
            Hashtable hsData = new Hashtable();

            if (ModiState == "A")
            {
                hsData.Add("EH_TransNo", System.Guid.NewGuid().ToString("N"));
                hsData.Add("EH_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EH_TransNo", EH_TransNo);
                hsData.Add("EH_UidNo", EH_UidNo);
            }
            DataRow dr_teacher = DataETeacher.DataReader_ET_TransNo(ET_TransNo);
            if (dr_teacher != null)
            {
                hsData.Add("ET_TransNo", dr_teacher["ET_TransNo"].ToString());
                hsData.Add("ET_UidNo", dr_teacher["ET_UidNo"].ToString());
            }
            hsData.Add("EH_Year", EH_Year);
            hsData.Add("EH_Month", EH_Month);
            hsData.Add("EH_Hours", EH_Hours);
            hsData.Add("EH_Points", EH_Points);
            hsData.Add("EH_ShouldPay", EH_ShouldPay);
            hsData.Add("EH_RealPay", EH_RealPay);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EH_TransNo", TransNo);
            hsData.Add("EH_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EH_UidNo.Value = string.Empty;
            selTeacher_ET_TName.Clear();
            lbl_EH_Hours.Text = string.Empty;
            lbl_EH_Points.Text = string.Empty;
            lbl_EH_RealPay.Text = string.Empty;
            lbl_EH_ShouldPay.Text = string.Empty;
            lbl_EH_Year_Month.Text = string.Format("{0}/{1}", DateTime.Now.Year, DateTime.Now.Month.ToString("00"));
            lbl_ET_Company.Text = string.Empty;
            lbl_ET_Department.Text = string.Empty;
            hif_ET_TransNo.Value = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
                case 2:
                    {
                        DataTable dt = DataETeacherTot.DataReader(
                                        Lib.FdVP(txtSh_EH_Year.Text).Trim(),
                                        Lib.FdVP(txtSh_EH_Month.Text).Trim(),
                                        Lib.FdVP(txtSh_ET_Company.Text).Trim(),
                                        Lib.FdVP(txtSh_ET_Department.Text).Trim(),
                                        Lib.FdVP(txtSh_ET_TCode.Text).Trim(),
                                        Lib.FdVP(txtSh_ET_TName.Text).Trim(),
                                        DateRangeSh_EC_SDate.Date_start,
                                        DateRangeSh_EC_SDate.Date_end,
                                        0, 99999
                                        ).Tables[0];
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            DataTable dt_export = new DataTable();
                            dt_export.Columns.Add("序號", typeof(string));
                            dt_export.Columns.Add("查詢區間日期", typeof(string));
                            dt_export.Columns.Add("講師所屬單位", typeof(string));
                            dt_export.Columns.Add("講師姓名", typeof(string));
                            dt_export.Columns.Add("授課時數(小時)", typeof(string));
                            dt_export.Columns.Add("授課點數(計分)", typeof(string));

                            foreach (DataRow dr in dt.Rows)
                            {
                                dt_export.Rows.Add(
                                    dr["SerialNo"].ToString(),
                                    (DateRangeSh_EC_SDate.Date_start == "" && DateRangeSh_EC_SDate.Date_end == "") ? "" : string.Format("{0}~{1}", DateRangeSh_EC_SDate.Date_start, DateRangeSh_EC_SDate.Date_end),
                                    dr["ET_Unit"].ToString(),
                                    dr["ET_TName"].ToString(),
                                    dr["EH_Hours"].ToString(),
                                    dr["EH_Points"].ToString()
                                    );
                            }
                            GridView2.DataSource = dt_export;
                            GridView2.DataBind();
                            ViewState["dt_export"] = dt_export;
                            //ExportControls.CreateCSVFile(Page, dt_export, string.Format("講師授課時數_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
                        }

                        lnkBrowse.Visible = true;
                    }
                    break;
                case 3:
                    {
                        DataTable dt = DataETeacherTot.DataReader2(
                                       Lib.FdVP(txtSh_EH_Year.Text).Trim(),
                                       Lib.FdVP(txtSh_EH_Month.Text).Trim(),
                                       Lib.FdVP(txtSh_ET_Company.Text).Trim(),
                                       Lib.FdVP(txtSh_ET_Department.Text).Trim(),
                                       Lib.FdVP(txtSh_ET_TCode.Text).Trim(),
                                       Lib.FdVP(txtSh_ET_TName.Text).Trim(),
                                       DateRangeSh_EC_SDate.Date_start,
                                       DateRangeSh_EC_SDate.Date_end,
                                       0, 99999
                                       ).Tables[0];
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            DataTable dt_export = new DataTable();
                            dt_export.Columns.Add("序號", typeof(string));
                            dt_export.Columns.Add("上課日期", typeof(string));
                            dt_export.Columns.Add("課程類別", typeof(string));
                            dt_export.Columns.Add("開課地區", typeof(string));
                            dt_export.Columns.Add("開課單位", typeof(string));
                            dt_export.Columns.Add("課程地點", typeof(string));
                            dt_export.Columns.Add("講師所屬地區", typeof(string));
                            dt_export.Columns.Add("講師所屬單位", typeof(string));
                            dt_export.Columns.Add("講師業務編號", typeof(string));
                            dt_export.Columns.Add("講師姓名", typeof(string));
                            dt_export.Columns.Add("課程名稱", typeof(string));
                            dt_export.Columns.Add("授課時數(小時)", typeof(string));
                            dt_export.Columns.Add("授課點數(計分)", typeof(string));

                            foreach (DataRow dr in dt.Rows)
                            {
                                dt_export.Rows.Add(
                                    dr["SerialNo"].ToString(),
                                    dr["EC_SDate"].ToString(),
                                    dr["EC_ModeName"].ToString(),
                                    dr["EC_Area"].ToString(),
                                    dr["EC_UnitName"].ToString(),
                                    dr["EC_Place"].ToString(),
                                    dr["ET_Area"].ToString(),
                                    dr["ET_UnitName"].ToString() == "" ? dr["ET_Unit"].ToString() : dr["ET_UnitName"].ToString(),
                                    dr["ET_TCode"].ToString(),
                                    dr["ET_TName"].ToString(),
                                    dr["EC_CName"].ToString(),
                                    dr["EH_Hours"].ToString(),
                                    dr["EH_Points"].ToString()
                                    );
                            }
                            GridView3.DataSource = dt_export;
                            GridView3.DataBind();
                            ViewState["dt_export"] = dt_export;
                            //ExportControls.CreateCSVFile(Page, dt_export, string.Format("講師授課時數_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
                        }

                        lnkBrowse.Visible = true;
                    }
                    break;
                case 4:
                    {
                        DataTable dt_unit = DataPhrase.DataReader_TypeCode("E008").Tables[0];
                        DataTable dt_export = new DataTable();
                        dt_export.Columns.Add("→邀請單位\n↓講師單位", typeof(string));

                        foreach (DataRow dr_unit in dt_unit.Rows)
                        {
                            dt_export.Columns.Add(dr_unit["TypeName"].ToString(), typeof(string));

                        }
                        DataTable dt = DataETeacherTot.DataReader2(
                                               "",
                                               "",
                                               "",
                                               "",
                                               "",
                                               "",
                                               "",
                                               "",
                                               0, 99999
                                               ).Tables[0];
                        var dt_unit_t = dt.AsEnumerable().OrderBy(o => o.Field<string>("ET_Unit")).GroupBy(r => new { ET_UnitName = r["ET_UnitName"] }).ToList();
                        foreach (var data in dt_unit_t)
                        {
                            List<string> list = new List<string>();
                            list.Add(data.Key.ET_UnitName.ToString());
                            foreach (DataRow dr_unit in dt_unit.Rows)
                            {
                                var amt = dt.Compute("sum(EH_Points)", string.Format("ET_UnitName='{0}' and EC_UnitName='{1}'", data.Key.ET_UnitName.ToString(), dr_unit["TypeName"].ToString())).ToString();
                                list.Add(amt == "" ? "0" : amt);
                            }
                            dt_export.Rows.Add(list.ToArray());
                        }
                        GridView4.DataSource = dt_export;
                        GridView4.DataBind();
                        ViewState["dt_export"] = dt_export;
                        GridView4.HeaderRow.Cells[0].Text = "→邀請單位<br>↓講師單位";

                        lnkBrowse.Visible = true;
                    }
                    break;
                case 5:
                    MarkSortSetInit();
                    lbl_Msg0.Text = string.Empty;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(selTeacher_ET_TName);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(selTeacher_ET_TName);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[9].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[10].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[9].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[9].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[10].Text, GridView1.Rows[i].Cells[11].Text)));

                    //RServiceProvider rsp = DataETeacherTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataETeacherTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EH_Year")) return;
            e.InputParameters.Add("EH_Year", Lib.FdVP(txtSh_EH_Year.Text).Trim());
            e.InputParameters.Add("EH_Month", Lib.FdVP(txtSh_EH_Month.Text).Trim());
            e.InputParameters.Add("ET_Company", Lib.FdVP(txtSh_ET_Company.Text).Trim());
            e.InputParameters.Add("ET_Department", Lib.FdVP(txtSh_ET_Department.Text).Trim());
            e.InputParameters.Add("ET_TCode", Lib.FdVP(txtSh_ET_TCode.Text).Trim());
            e.InputParameters.Add("ET_TName", Lib.FdVP(txtSh_ET_TName.Text).Trim());
            e.InputParameters.Add("EC_SDate_s", DateRangeSh_EC_SDate.Date_start);
            e.InputParameters.Add("EC_SDate_e", DateRangeSh_EC_SDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EH_Year.Text = string.Empty;
            txtSh_EH_Month.Text = string.Empty;
            txtSh_ET_Company.Text = string.Empty;
            txtSh_ET_Department.Text = string.Empty;
            txtSh_ET_TCode.Text = string.Empty;
            txtSh_ET_TName.Text = string.Empty;
            DateRangeSh_EC_SDate.Init();

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            DataTable dt = DataETeacherTot.DataReader(
                Lib.FdVP(txtSh_EH_Year.Text).Trim(),
                Lib.FdVP(txtSh_EH_Month.Text).Trim(),
                Lib.FdVP(txtSh_ET_Company.Text).Trim(),
                Lib.FdVP(txtSh_ET_Department.Text).Trim(),
                Lib.FdVP(txtSh_ET_TCode.Text).Trim(),
                Lib.FdVP(txtSh_ET_TName.Text).Trim(),
                DateRangeSh_EC_SDate.Date_start,
                DateRangeSh_EC_SDate.Date_end,
                0, 99999
                ).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("公司名稱", typeof(string));
                dt_export.Columns.Add("部門名稱", typeof(string));
                dt_export.Columns.Add("講師編號", typeof(string));
                dt_export.Columns.Add("講師姓名", typeof(string));
                dt_export.Columns.Add("統計年月", typeof(string));
                dt_export.Columns.Add("點數", typeof(string));
                dt_export.Columns.Add("應付費", typeof(string));
                dt_export.Columns.Add("實付費", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    dt_export.Rows.Add(
                        dr["ET_Company"].ToString(),
                        dr["ET_Department"].ToString(),
                        dr["ET_TCode"].ToString(),
                        dr["ET_TName"].ToString(),
                        dr["YearMonth"].ToString(),
                        dr["EH_Points"].ToString(),
                        dr["EH_ShouldPay"].ToString(),
                        dr["EH_RealPay"].ToString()
                        );
                }

                ExportControls.CreateCSVFile(Page, dt_export, string.Format("講師授課時數_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
            }
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }

        protected void btnApply_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "search"://
                    
                    break;
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) == -1)
                        {
                            lboxObj.Items.Add(m_item);
                        }
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            if (lboxObj.Items.IndexOf(itemSur) == -1)
                            {
                                lboxObj.Items.Add(itemSur);
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                                lboxSur.Items.Remove(itemSur);
                                if (lboxSur.Items.Count > siSur)
                                    lboxSur.SelectedIndex = siSur;
                                else
                                    lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            }
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "Append"://儲存新顯示順序設定
                    if (lboxObj.Items.Count != 0)
                    {
                        SysValue sysValue = ViewState["SysValue"] as SysValue;
                        StringBuilder sb_msg = new StringBuilder();
                        foreach (ListItem m_item in lboxObj.Items)
                        {
                            int weight = 1;
                            double EH_Hours_count = 0;
                            string EH_Hours = "", EH_Points = "", EH_ShouldPay = "", EH_RealPay = "";
                            DataTable dt_ec = DataECourse.DataReader_ET_TransNo(m_item.Value, "2").Tables[0];
                            List<DataRow> list_ec = dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[0] && r.Field<DateTime>("EC_SDate").Month.ToString("00") == lbl_EH_Year_Month.Text.Split('/')[1]).ToList();
                            foreach (DataRow dr_ec in list_ec)
                            {
                                string EW_CUnit = "", EW_TUnit = "";
                                DataRow dr_p = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr_ec["EC_Unit"].ToString());
                                if (dr_p != null)
                                    EW_CUnit = dr_p["TypeName"].ToString();
                                DataRow dr_ET = DataETeacher.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue);
                                if (dr_ET != null)
                                {
                                    dr_p = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr_ET["ET_Unit"].ToString());
                                    if (dr_p != null)
                                        EW_TUnit = dr_p["TypeName"].ToString();
                                }
                                DataRow dr_weight = DataEWeightM.DataReader_EW_WeightM(EW_CUnit, EW_TUnit);
                                if (dr_weight != null)
                                {
                                    weight = int.Parse(dr_weight["EW_WeightM"].ToString());
                                }
                            }
                            EH_Hours_count = dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == ddlSh_year.SelectedValue && r.Field<DateTime>("EC_SDate").Month.ToString("00") == ddlSh_Month.SelectedValue).Sum(r => r.Field<double>("EC_Hours"));
                            EH_Hours = EH_Hours_count.ToString();
                            EH_Points = (EH_Hours_count * weight).ToString(); //加權
                            EH_ShouldPay = Math.Round(dt_ec.AsEnumerable().Where(r => r.Field<DateTime>("EC_SDate").Year.ToString("00") == ddlSh_year.SelectedValue && r.Field<DateTime>("EC_SDate").Month.ToString("00") == ddlSh_Month.SelectedValue).Sum(r => r.Field<double>("EC_Hours") * r.Field<int>("EC_TeacherCost"))).ToString();
                            DataTable dt_e6 = DataETeacherFee.DataReader_ET_TransNo(selTeacher_ET_TName.SelectedValue).Tables[0];
                            EH_RealPay = dt_e6.AsEnumerable().Where(r => r.Field<DateTime>("E6_TransDate").Year.ToString("00") == ddlSh_year.SelectedValue && r.Field<DateTime>("E6_TransDate").Month.ToString("00") == ddlSh_Month.SelectedValue).Sum(r => r.Field<int>("E6_RealPay")).ToString();
                            
                            DataRow dr = DataETeacherTot.DataReader_ET_TransNo(m_item.Value, ddlSh_year.SelectedValue, ddlSh_Month.SelectedValue);
                            if(dr != null)
                            {
                                DataSet ds = new DataSet();
                                //元件資料轉成Hashtable
                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(),
                                    dataToHashtable_batch("M", m_item.Value, ddlSh_year.SelectedValue, int.Parse(ddlSh_Month.SelectedValue).ToString("00"), dr["EH_TransNo"].ToString(), dr["EH_UidNo"].ToString(),
                                    EH_Hours, EH_Points, EH_ShouldPay, EH_RealPay)));

                                RServiceProvider rsp = DataETeacherTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                                if (rsp.Result)
                                {
                                    sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}：更新成功！</span><br>", m_item.Text);
                                }
                                else
                                    sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}：更新失敗！</span><br>", m_item.Text);
                            }
                            else
                            {
                                DataSet ds = new DataSet();
                                //元件資料轉成Hashtable
                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataETeacherTot.GetSchema(),
                                    dataToHashtable_batch("A", m_item.Value, ddlSh_year.SelectedValue, int.Parse(ddlSh_Month.SelectedValue).ToString("00"), "", "",
                                    EH_Hours, EH_Points, EH_ShouldPay, EH_RealPay)));

                                RServiceProvider rsp = DataETeacherTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                                if (rsp.Result)
                                {
                                    sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：更新成功！</span><br>", m_item.Text, m_item.Value);
                                }
                                else
                                    sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}({1})：更新失敗！</span><br>", m_item.Text, m_item.Value);
                            }

                        }
                        GridView1.DataBind();
                        lbl_Msg0.Text = sb_msg.ToString();
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入人員!");
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    lbl_Msg0.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }

        protected void MarkSortSetInit()
        {
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            DataTable dt = DataETeacher.baseDataReader(true, "").Tables[0];
            lboxSur.Items.Clear();
            foreach (DataRow dw in dt.Rows)
            {
                ListItem li = new ListItem();
                li.Text = string.Format("{0}({1}){2}", dw["ET_TName"].ToString().Trim(), dw["ET_TCode"].ToString().Trim(), dw["ET_Company"].ToString());
                li.Value = dw["ET_TransNo"].ToString().Trim();
                lboxSur.Items.Add(li);
            }
        }
    }
}