﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="EQues.aspx.cs" Inherits="tabc_201709.Manager.EQues" %>

<%@ Register src="ascx/selPerson_AccID.ascx" tagname="selPerson_AccID" tagprefix="uc4" %>

<%@ Register src="ascx/selCourse.ascx" tagname="selCourse" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0"
                CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>
            </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkMaintain" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="Server">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr>
                                            <td align="center" class="tdQueryHead">題型：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:DropDownList ID="ddlSh_EQ_Type" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" class="tdQueryHead">&nbsp;</td>
                                            <td align="center" class="tdQueryData">
                                                &nbsp;</td>
                                            <td align="center" width="85" rowspan="2">
                                                <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                            </td>
                                            <td align="center" width="35" rowspan="2">
                                                <asp:Button ID="btnRestQuery" runat="server" CssClass="btnRefresh" OnClick="btnRestQuery_Click" Width="30px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" class="tdQueryHead">題目：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EQ_Question" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">答案：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EA_ListItem" runat="server"></asp:TextBox>
                                            </td>
                                            <td align="center" class="tdQueryHead">測驗代碼：</td>
                                            <td align="center" class="tdQueryData">
                                                <asp:TextBox ID="txtSh_EQ_QCode" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True"
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount"
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First"
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev"
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next"
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last"
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server"
                                        AutoGenerateColumns="False" CssClass="GridViewTable"
                                        DataSourceID="ObjectDataSource1" EmptyDataText="查無符合資料!!!"
                                        OnSelectedIndexChanged="GridView1_SelectedIndexChanged"
                                        OnLoad="GridView1_Load"
                                        EnableModelValidation="True">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True"
                                        SelectCountMethod="GetCount" SelectMethod="DataReader"
                                        TypeName="DataEQues"></asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table  align="center" border="0" cellpadding="2" cellspacing="1"
                            width="100%">
                            <tr>
                                <td width="100%">
                                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">題型</td>
                                            <td class="tdData10s">
                                                <asp:DropDownList ID="ddl_EQ_Type" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_EQ_Type_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">相關課程</td>
                                            <td class="tdData10s">
                                                <uc1:selCourse ID="selCourse_EC_TransNo" runat="server" OnSelectedIndexChanged="selCourse_EC_TransNo_SelectedIndexChanged" OnTextChanged="selCourse_EC_TransNo_TextChanged" OnSearchButtonClick="selCourse_EC_TransNo_SearchButtonClick" OnRefreshButtonClick="selCourse_EC_TransNo_RefreshButtonClick" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">題目</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EQ_Question" runat="server" MaxLength="300" Width="98%" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">答案</td>
                                            <td class="tdData10s">
                                                <asp:Panel ID="Panel_EA_ListItem1" runat="server" Visible="False"></asp:Panel>
                                                <asp:Panel ID="Panel_EA_ListItem2" runat="server" Visible="False"></asp:Panel>
                                                <asp:Panel ID="Panel_EA_ListItem3" runat="server" Visible="False"></asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="MS_tdTitle" width="100">測驗代碼</td>
                                            <td class="tdData10s">
                                                <asp:TextBox ID="txt_EQ_QCode" runat="server" MaxLength="30" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="center" class="tdMaintainButton" colspan="2">
                                                <asp:Button ID="btnAppend" runat="server" CommandName="Append" CssClass="btnAppend" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnEdit" runat="server" CommandName="Edit" CssClass="btnSave" OnCommand="btn_Command" Text="存檔" />
                                                <asp:Button ID="btnCopy" runat="server" CommandName="Copy" CssClass="btnCopy" OnClientClick="return confirm(&quot;確定要複製嗎?&quot;);" OnCommand="btn_Command" Text="複製" />
                                                <asp:Button ID="btnDelete" runat="server" CommandName="Delete" CssClass="btnDelete" OnClientClick="return confirm(&quot;確定要刪除嗎?&quot;);" OnCommand="btn_Command" Text="刪除" />
                                                <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" CssClass="btnCancel" OnCommand="btn_Command" Text="取消" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="height: 20px" colspan="2">
                                                <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="line-height: 125%">
                                                        <asp:Label ID="lbl_Msg" runat="server" CssClass="errmsg12"></asp:Label>
                                                    </td>
                                                </tr>
                                                </table>
                                                <asp:HiddenField ID="hif_TransNo" runat="server" />
                                                <asp:HiddenField ID="hif_EQ_UidNo" runat="server" />
                                                <asp:HiddenField ID="hif_EC_TransNo" runat="server" />
                                            </td>
                                        </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>
            </ContentTemplate>
    </asp:UpdatePanel>

    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(EndRequestHandler);
        function EndRequestHandler() {
            $(function () {

                selectChangeConfirm('<%= ddl_EQ_Type.ClientID %>', "將清除目前答案項目的設定值！\n確定要切換類型嗎？");
                });
            }
            function selectChangeConfirm(controlID, message) {
                var s = $('#' + controlID).val();
                $('#' + controlID).removeAttr('onchange');
                $('#' + controlID).change(function (e) {
                    e.preventDefault();
                    if (confirm(message)) {
                        setTimeout("__doPostBack('" + controlID + "','')", 0);
                        s = $('#' + controlID).val();
                    }
                    else {
                        $('#' + controlID).val(s);
                    }
                });
            }
    </script>
</asp:Content>
