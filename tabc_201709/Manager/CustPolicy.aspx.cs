﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class CustPolicy : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_CU_CustName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                //case 1:
                //    if (btnEdit.Visible)
                //    {
                //        this.SetFocus(txt_PO_PolNo);
                //        Panel_Form.DefaultButton = btnEdit.ID;
                //    }
                //    else
                //    {
                //        this.SetFocus(txt_PO_PolNo);
                //        Panel_Form.DefaultButton = btnAppend.ID;
                //    }
                //    break;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("CustPolicy", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1200);
                MainControls.GridViewStyleCtrl(GridView2, 950);
                MainControls.GridViewStyleCtrl(GridView3, 700);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();

                if (Request.QueryString["id"] != null)
                    QueryMainTable.Visible = false;
                else
                    QueryMainTable.Visible = true;
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataCustPolicy.DataColumn(this.GridView1);
            DataCustPdt.DataColumn(this.GridView2);
            DataCustBenf.DataColumn(this.GridView3);
        }
        private void dataBind_GV2()
        {
            //投保商品
            DataSet ds = DataCustPdt.DataReader_PO_TransNo(hif_TransNo.Value);
            DataCustPdt.DataProcess(ds);
            GridView2.DataSource = ds.Tables[0];
            GridView2.DataBind();
        }
        private void dataBind_GV3()
        {
            //保單受益人
            DataSet ds = DataCustBenf.DataReader_PO_TransNo(hif_TransNo.Value);
            DataCustBenf.DataProcess(ds);
            GridView3.DataSource = ds.Tables[0];
            GridView3.DataBind();
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_CU_CustSource, true, "", "099");
            DataPhrase.DDL_TypeName(ddlSh_CU_Sex, true, "", "002");
            DataPhrase.DDL_TypeName(ddlSh_CU_ContactMode, true, "", "021");
            DataPhrase.DDL_TypeName(ddlSh_CU_Marry, true, "", "022");
            DataPhrase.DDL_TypeName(ddlSh_CU_CustType, true, "", "023");
            DataPhrase.DDL_TypeName(ddl_PO_Mode, true, "", "028");
            DataPhrase.DDL_TypeName(ddl_PO_PaidMode, true, "", "032");
            DataPhrase.DDL_TypeName(ddl_PO_PolState, true, "", "033");
            DataPhrase.DDL_TypeName(ddl_PO_Dividend, true, "", "034");
            DataTABC_COMPANY_MAP.DDL_TCM_ComCode(ddl_PO_ComSName, true, "");

            ViewState["ddl_PD_Kind"] = DataPhrase.DDL_TypeName(ddl_PD_Kind, true, "", "061");
            DataPhrase.DDL_TypeName(ddl_PD_Relation, true, "", "036");
            DataPhrase.DDL_TypeName(ddl_PD_PdtCodeState, true, "", "033");

            DataPhrase.DDL_TypeName(ddl_BE_Kind, true, "", "103");
            DataPhrase.DDL_TypeName(ddl_BE_Relation, true, "", "019");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_PO_PolNo.Text.Trim() == "")
                sbError.Append("●「保單號碼」必須輸入!<br>");
            if (ddl_PO_ComSName.SelectedValue == "")
                sbError.Append("●「投保公司」必須選取!<br>");
            if (txt_PO_AccureDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_PO_AccureDate.Text.Trim()))
                    sbError.Append("●「投保日期」格式錯誤!<br>");
            }
            if (txt_PO_PaidToDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_PO_PaidToDate.Text.Trim()))
                    sbError.Append("●「繳費日期」格式錯誤!<br>");
            }
            if (txt_PO_InsureAge.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_PO_InsureAge.Text.Trim())))
                    sbError.Append("●「投保年齡」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        private string dataValid_PD(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_PD_Kind.SelectedValue == "")
                sbError.Append("●「約別」必須選取!<br>");

            if (ddl_PD_Relation.SelectedValue == "")
                sbError.Append("●「投保對象」必須選取!<br>");
            if (selCustomer01.SelectedValue == "")
                sbError.Append("●「被保險人」必須選取!<br>");
            if (txt_PD_AccureDate.Text.Trim() == "")
                sbError.Append("●「投保日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_PD_AccureDate.Text.Trim()))
                    sbError.Append("●「投保日期」格式錯誤!<br>");
            }
            if (txt_PD_PdtYear.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_PD_PdtYear.Text.Trim())))
                    sbError.Append("●「年期」格式錯誤!<br>");
            }
            if (txt_PD_Amtinput.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_PD_Amtinput.Text.Trim())))
                    sbError.Append("●「台幣保額」格式錯誤!<br>");
            }
            if (txt_PD_ModePrem.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_PD_ModePrem.Text.Trim())))
                    sbError.Append("●「應繳保費」格式錯誤!<br>");
            }


            return sbError.ToString();
        }
        private string dataValid_BE(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_BE_Kind.SelectedValue == "")
                sbError.Append("●「種類」必須選取!<br>");
            if (selCustomer3.SelectedValue == "")
                sbError.Append("●「受益人」必須選取!<br>");
            if (ddl_BE_Relation.SelectedValue == "")
                sbError.Append("●「關係」必須選取!<br>");
            if (txt_BE_BenefSuccession.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_BE_BenefSuccession.Text.Trim())))
                    sbError.Append("●「順位」格式錯誤!<br>");
            }
            if (txt_BE_BenefPercent.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_BE_BenefPercent.Text.Trim())))
                    sbError.Append("●「比例」格式錯誤!<br>");
            }
            if (txt_BE_Age.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_BE_Age.Text.Trim())))
                    sbError.Append("●「年齡」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "PO_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            dataBind_GV2();
                            dataBind_GV3();
                            break;
                        case "PO_UidNo":
                            hif_PO_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolNo":
                            txt_PO_PolNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_ComSName":
                            MainControls.ddlIndexSelectText(ddl_PO_ComSName, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            DataRow dr_TCM = DataTABC_COMPANY_MAP.DataReader_TCM_ComCode(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            if (dr_TCM != null)
                            {
                                selPdt.iStatus = "1";//壽險
                                selPdt.CompanyNo = dr_TCM["ComCode_GT"].ToString();
                            }
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
                            if (dr_customer != null)
                            {
                                selCustomer1.search(dr_customer["CU_CustName"].ToString());
                                selCustomer1.SelectedValue = hif_CU_TransNo.Value;
                            }
                            break;
                        case "PO_AccureDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_PO_AccureDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "PO_InsureAge":
                            txt_PO_InsureAge.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolState":
                            MainControls.ddlIndexSelectText(ddl_PO_PolState, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PO_PaidToDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_PO_PaidToDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "PO_Mode":
                            MainControls.ddlIndexSelectValue(ddl_PO_Mode, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PaidMode":
                            MainControls.ddlIndexSelectText(ddl_PO_PaidMode, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PO_ModePrem":
                            txt_PO_ModePrem.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolmCurrency":
                            MainControls.ddlIndexSelectText(ddl_PO_PolmCurrency, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            MainControls.ddlIndexSelectText(ddl_PD_PolmCurrency, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PO_PolicyAmt":
                            txt_PO_PolicyAmt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolicyPrem":
                            txt_PO_PolicyPrem.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_Dividend":
                            MainControls.ddlIndexSelectText(ddl_PO_Dividend, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PO_Predecessor":
                            txt_PO_Predecessor.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_POLBELONGAGENT":
                            txt_PO_POLBELONGAGENT.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolZip":
                            txt_PO_PolZip.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolAddr":
                            txt_PO_PolAddr.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PolMemo":
                            txt_PO_PolMemo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PO_PKSKey":
                            string PO_PKSKey = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            if (PO_PKSKey != "")//是匯入
                            {
                                txt_PO_PolNo.ReadOnly = true;
                                ddl_PO_ComSName.Enabled = false;
                                selCustomer1.Enabled = false;
                                txt_PO_AccureDate.ReadOnly = true;
                            }
                            else
                            {
                                txt_PO_PolNo.ReadOnly = false;
                                ddl_PO_ComSName.Enabled = true;
                                selCustomer1.Enabled = true;
                                txt_PO_AccureDate.ReadOnly = false;
                            }
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext_PD();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView2.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView2.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView2.Columns[i] is BoundField)
                {
                    BoundField bf = GridView2.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "PD_TransNo":
                            hif_PD_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PD_UidNo":
                            hif_PD_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo1.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo1.Value);
                            if (dr_customer != null)
                            {
                                selCustomer01.CU_IsShow = ddl_PD_Relation.SelectedItem.Text == "本人" ? "1" : "0";
                                selCustomer01.search(dr_customer["CU_CustName"].ToString());
                                selCustomer01.SelectedValue = hif_CU_TransNo1.Value;
                            }
                            break;
                        case "CU_UidNo":
                            hif_CU_UidNo1.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PD_Kind":
                            MainControls.ddlIndexSelectAttribute(ddl_PD_Kind, "TypeSubCode", ViewState["ddl_PD_Kind"] as DataTable, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PD_PdtName":
                            txt_PD_PdtName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PD_PKSKey":
                            string PD_PKSKey = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            if(PD_PKSKey != "")//是匯入
                            {
                                selPdt.Visible = false;
                                txt_PD_PdtName.Visible = true;
                                ddl_PD_Kind.Enabled = false;
                                selCustomer01.Enabled = false;
                            }
                            else
                            {
                                selPdt.Visible = true;
                                txt_PD_PdtName.Visible = false;
                                ddl_PD_Kind.Enabled = true;
                                selCustomer01.Enabled = true;
                                selPdt.search(txt_PD_PdtName.Text);
                                selPdt.SelectedText = txt_PD_PdtName.Text;
                            }
                            break;
                        case "PD_Relation":
                            MainControls.ddlIndexSelectValue(ddl_PD_Relation, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PD_PolmCurrency":
                            MainControls.ddlIndexSelectValue(ddl_PD_PolmCurrency, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PD_PdtYear":
                            txt_PD_PdtYear.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PD_PdtCodeState":
                            MainControls.ddlIndexSelectText(ddl_PD_PdtCodeState, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "PD_AccureDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_PD_AccureDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "PD_Amtinput":
                            txt_PD_Amtinput.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "PD_ModePrem":
                            txt_PD_ModePrem.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;


                    }
                }
            //切換至維護畫面
            show_div("div_PD_edit");
            maintainButtonEnabled("Select");
        }
        protected void selPdt_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_div("div_PD_edit");
        }
        protected void selPdt_SearchButtonClick(object sender, EventArgs e)
        {
            show_div("div_PD_edit");
        }
        protected void selPdt_RefreshButtonClick(object sender, EventArgs e)
        {
            show_div("div_PD_edit");
        }
        protected void selCustomer01_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_div("div_PD_edit");
        }
        protected void selCustomer01_SearchButtonClick(object sender, EventArgs e)
        {
            show_div("div_PD_edit");
        }
        protected void selCustomer01_RefreshButtonClick(object sender, EventArgs e)
        {
            selCustomer01.search(hif_CU_TransNo1.Value);
            selCustomer01.SelectedValue = hif_CU_TransNo1.Value;
            show_div("div_PD_edit");
        }
        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext_BE();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView3.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView3.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView3.Columns[i] is BoundField)
                {
                    BoundField bf = GridView3.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "BE_TransNo":
                            hif_BE_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "BE_UidNo":
                            hif_BE_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo0.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo0.Value);
                            if (dr_customer != null)
                            {
                                selCustomer3.search(dr_customer["CU_CustName"].ToString());
                                selCustomer3.SelectedValue = hif_CU_TransNo0.Value;
                            }
                            break;
                        case "CU_UidNo":
                            hif_CU_UidNo0.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "BE_Kind":
                            MainControls.ddlIndexSelectText(ddl_BE_Kind, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "BE_Relation":
                            MainControls.ddlIndexSelectText(ddl_BE_Relation, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "BE_BenefSuccession":
                            txt_BE_BenefSuccession.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "BE_BenefPercent":
                            txt_BE_BenefPercent.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "BE_BenefID":
                            txt_BE_BenefID.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "BE_Age":
                            txt_BE_Age.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            show_div("div_BE_edit");
            maintainButtonEnabled("Select");
        }
        protected void selCustomer3_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_div("div_BE_edit");
        }
        protected void selCustomer3_SearchButtonClick(object sender, EventArgs e)
        {
            show_div("div_BE_edit");
        }
        protected void selCustomer3_RefreshButtonClick(object sender, EventArgs e)
        {
            selCustomer3.search(hif_CU_TransNo0.Value);
            selCustomer3.SelectedValue = hif_CU_TransNo0.Value;
            show_div("div_BE_edit");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPolicy.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataCustPolicy.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPolicy.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataCustPolicy.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPolicy.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataCustPolicy.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataCustPolicy.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    GridView1.DataBind();
                    break;
            }
        }
        protected void btn_Command_PD(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg0.Text = dataValid_PD(e.CommandName); //資料格式驗証
                show_div("div_PD_edit");

            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        hif_PD_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPdt.GetSchema(), dataToHashtable_PD("A")));

                        RServiceProvider rsp = DataCustPdt.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPdt.GetSchema(), dataToHashtable_PD("M")));

                        RServiceProvider rsp = DataCustPdt.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPdt.GetSchema(), dataToHashtable_PD("D")));

                        //RServiceProvider rsp = DataCustPdt.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataCustPdt.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext_PD();
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    //ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('div_CM_edit').unblock();", true);
                    clearControlContext_PD();
                    GridView2.DataBind();
                    break;
            }

            
        }
        protected void btn_Command_BE(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg1.Text = dataValid_BE(e.CommandName); //資料格式驗証
                show_div("div_BE_edit");
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        hif_BE_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustBenf.GetSchema(), dataToHashtable_BE("A")));

                        RServiceProvider rsp = DataCustBenf.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV3();
                            lbl_Msg1.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustBenf.GetSchema(), dataToHashtable_BE("M")));

                        RServiceProvider rsp = DataCustBenf.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV3();
                            lbl_Msg1.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustBenf.GetSchema(), dataToHashtable_BE("D")));

                        RServiceProvider rsp = DataCustBenf.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV3();
                            lbl_Msg1.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext_BE();
                            return;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    //ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('div_FA_edit').unblock();", true);
                    clearControlContext_BE();
                    GridView3.DataBind();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PO_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("PO_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("PO_UidNo", hif_PO_UidNo.Value);
            }
            hsData.Add("PO_PolNo", txt_PO_PolNo.Text.Trim());
            hsData.Add("PO_ComSName", ddl_PO_ComSName.SelectedItem.Text.Trim());
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(selCustomer1.SelectedValue);
            if (dr_customer != null)
            {
                hsData.Add("CU_TransNo", dr_customer["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_customer["CU_UidNo"].ToString());
                hsData.Add("PO_OwnerName", dr_customer["CU_CustName"].ToString());
            }
            hsData.Add("PO_AccureDate", txt_PO_AccureDate.Text.Trim());
            hsData.Add("PO_InsureAge", txt_PO_InsureAge.Text.Trim());
            hsData.Add("PO_PolState", ddl_PO_PolState.SelectedItem.Text.Trim());
            hsData.Add("PO_PaidToDate", txt_PO_PaidToDate.Text.Trim());
            hsData.Add("PO_Mode", ddl_PO_Mode.SelectedItem.Value.Trim());
            hsData.Add("PO_PaidMode", ddl_PO_PaidMode.SelectedItem.Text.Trim());
            hsData.Add("PO_ModePrem", txt_PO_ModePrem.Text.Trim());
            hsData.Add("PO_PolmCurrency", ddl_PO_PolmCurrency.SelectedItem.Text.Trim());
            hsData.Add("PO_PolicyAmt", txt_PO_PolicyAmt.Text.Trim());
            hsData.Add("PO_PolicyPrem", txt_PO_PolicyPrem.Text.Trim());
            hsData.Add("PO_Dividend", ddl_PO_Dividend.SelectedItem.Text.Trim());
            hsData.Add("PO_Predecessor", txt_PO_Predecessor.Text.Trim());
            hsData.Add("PO_POLBELONGAGENT", txt_PO_POLBELONGAGENT.Text.Trim());
            hsData.Add("PO_PolZip", txt_PO_PolZip.Text.Trim());
            hsData.Add("PO_PolAddr", txt_PO_PolAddr.Text.Trim());
            hsData.Add("PO_PolMemo", txt_PO_PolMemo.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PO_TransNo", TransNo);
            hsData.Add("PO_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_PD(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PD_TransNo", hif_PD_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("PD_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("PD_UidNo", Session["UidNo"].ToString());
            }
            hsData.Add("PD_PolNo", txt_PO_PolNo.Text.Trim());
            hsData.Add("PD_Kind", (ViewState["ddl_PD_Kind"] as DataTable).Rows[ddl_PD_Kind.SelectedIndex - 1]["TypeSubCode"].ToString());
            if(selPdt.Visible)
            {
                hsData.Add("PD_PdtNo", selPdt.Mark);
                hsData.Add("PD_PdtName", selPdt.InsName);
                hsData.Add("PD_Unit", selPdt.Unit);
            }
            hsData.Add("PD_AccureDate", txt_PD_AccureDate.Text.Trim());
            hsData.Add("PD_Relation", ddl_PD_Relation.SelectedValue);
            hsData.Add("PD_PolmCurrency", ddl_PD_PolmCurrency.SelectedValue);
            hsData.Add("PD_PdtYear", txt_PD_PdtYear.Text.Trim());
            hsData.Add("PD_AmtInput", txt_PD_Amtinput.Text.Trim());
            hsData.Add("PD_PD_ModePrem", txt_PD_ModePrem.Text.Trim());
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(selCustomer01.SelectedValue);
            if (dr_customer != null)
            {
                hsData.Add("CU_TransNo", dr_customer["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_customer["CU_UidNo"].ToString());
            }
            hsData.Add("PD_PdtCodeState", ddl_PD_PdtCodeState.SelectedItem.Text);
            hsData.Add("PO_TransNo", hif_TransNo.Value);
            hsData.Add("PO_UidNo", hif_PO_UidNo.Value);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete_PD(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PD_TransNo", TransNo);
            hsData.Add("PD_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_BE(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("BE_TransNo", hif_BE_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("BE_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("BE_UidNo", hif_BE_UidNo.Value);
            }
            hsData.Add("BE_Kind", ddl_BE_Kind.SelectedItem.Text);
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(selCustomer3.SelectedValue);
            if (dr_customer != null)
            {
                hsData.Add("CU_TransNo", dr_customer["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_customer["CU_UidNo"].ToString());
            }
            hsData.Add("BE_Relation", ddl_BE_Relation.SelectedItem.Text);

            hsData.Add("BE_BenefSuccession", txt_BE_BenefSuccession.Text.Trim());
            hsData.Add("BE_BenefPercent", txt_BE_BenefPercent.Text.Trim());
            hsData.Add("BE_BenefID", txt_BE_BenefID.Text.Trim());
            hsData.Add("BE_Age", txt_BE_Age.Text.Trim());
            hsData.Add("PO_TransNo", hif_TransNo.Value);
            hsData.Add("PO_UidNo", hif_PO_UidNo.Value);

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete_BE(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("BE_TransNo", TransNo);
            hsData.Add("BE_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_PO_UidNo.Value = string.Empty;
            hif_CU_TransNo.Value = string.Empty;
            hif_CU_UidNo.Value = string.Empty;
            txt_PO_PolNo.Text = string.Empty;
            ddl_PO_ComSName.SelectedIndex = -1;
            selCustomer1.Clear();
            txt_PO_AccureDate.Text = string.Empty;
            txt_PO_InsureAge.Text = string.Empty;
            ddl_PO_PolState.SelectedIndex = -1;
            txt_PO_PaidToDate.Text = string.Empty;
            ddl_PO_Mode.SelectedIndex = -1;
            ddl_PO_PaidMode.SelectedIndex = -1;
            txt_PO_ModePrem.Text = string.Empty;
            ddl_PO_PolmCurrency.SelectedIndex = -1;
            txt_PO_PolicyAmt.Text = string.Empty;
            txt_PO_PolicyPrem.Text = string.Empty;
            ddl_PO_Dividend.SelectedIndex = -1;
            txt_PO_Predecessor.Text = string.Empty;
            txt_PO_POLBELONGAGENT.Text = string.Empty;
            txt_PO_PolZip.Text = string.Empty;
            txt_PO_PolAddr.Text = string.Empty;
            txt_PO_PolMemo.Text = string.Empty;

            txt_PO_PolNo.ReadOnly = false;
            ddl_PO_ComSName.Enabled = true;
            selCustomer1.Enabled = true;
            txt_PO_AccureDate.ReadOnly = false;

            GridView2.DataSource = null;
            GridView2.DataBind();
            GridView3.DataSource = null;
            GridView3.DataBind();

            lbl_Msg.Text = string.Empty;
        }
        private void clearControlContext_PD()
        {
            hif_PD_TransNo.Value = string.Empty;
            hif_PD_UidNo.Value = string.Empty;
            ddl_PD_Kind.SelectedIndex = -1;
            txt_PD_PdtName.Text = string.Empty;
            selPdt.Visible = true;
            txt_PD_PdtName.Visible = false;
            ddl_PD_Kind.Enabled = true;
            selPdt.Clear();
            selCustomer01.Enabled = true;
            selCustomer01.Clear();
            ddl_PD_Relation.SelectedIndex = -1;
            txt_PD_AccureDate.Text = string.Empty;
            ddl_PD_PolmCurrency.SelectedIndex = -1;
            txt_PD_PdtYear.Text = string.Empty;
            txt_PD_Amtinput.Text = string.Empty;
            txt_PD_ModePrem.Text = string.Empty;
            ddl_PD_PdtCodeState.SelectedIndex = -1;

            lbl_Msg0.Text = string.Empty;
        }
        private void clearControlContext_BE()
        {
            hif_BE_TransNo.Value = string.Empty;
            hif_BE_UidNo.Value = string.Empty;
            hif_CU_TransNo0.Value = string.Empty;
            hif_CU_UidNo0.Value = string.Empty;
            ddl_BE_Kind.SelectedIndex = -1;
            selCustomer3.Clear();
            ddl_BE_Relation.SelectedIndex = -1;
            txt_BE_BenefSuccession.Text = string.Empty;
            txt_BE_BenefPercent.Text = string.Empty;
            txt_BE_BenefID.Text = string.Empty;
            txt_BE_Age.Text = string.Empty;

            lbl_Msg1.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
            //新增預設帶入'要保人'='客戶'
            if (e.CommandName == "1" && Request.QueryString["id"] != null)
            {
                DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(Request.QueryString["id"]);
                if (dr_customer != null)
                {
                    selCustomer1.search(dr_customer["CU_CustName"].ToString());
                    selCustomer1.SelectedValue = Request.QueryString["id"];
                }
            }
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            btnAppend0.Visible = false;
            btnEdit0.Visible = false;
            btnCancel0.Visible = false;
            btnDelete0.Visible = false;
            btnAppend1.Visible = false;
            btnEdit1.Visible = false;
            btnCancel1.Visible = false;
            btnDelete1.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    btnEdit0.Visible = true & sysValue.Authority.Update;
                    btnDelete0.Visible = true & sysValue.Authority.Delete;
                    btnCancel0.Visible = true;
                    btnEdit1.Visible = true & sysValue.Authority.Update;
                    btnDelete1.Visible = true & sysValue.Authority.Delete;
                    btnCancel1.Visible = true;
                    if (ddl_BE_Kind.SelectedItem.Text == "本人")
                    {
                        btnEdit1.Visible = false;
                        btnDelete1.Visible = false;
                    }
                    else
                    {
                        btnEdit1.Visible = true;
                        btnDelete1.Visible = true;
                    }
                    this.SetFocus(txt_PO_PolNo);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    btnAppend0.Visible = true & sysValue.Authority.Append;
                    btnCancel0.Visible = true;
                    btnAppend1.Visible = true & sysValue.Authority.Append;
                    btnCancel1.Visible = true;
                    this.SetFocus(txt_PO_PolNo);
                    break;
            }
        }
        private void maintainButtonEnabled_PD(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend0.Visible = false;
            btnEdit0.Visible = false;
            btnCancel0.Visible = false;
            btnDelete0.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit0.Visible = true & sysValue.Authority.Update;
                    btnDelete0.Visible = true & sysValue.Authority.Delete;
                    btnCancel0.Visible = true;

                    this.SetFocus(ddl_PD_Kind);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend0.Visible = true & sysValue.Authority.Append;
                    btnCancel0.Visible = true;
                    this.SetFocus(ddl_PD_Kind);
                    break;
            }
        }
        private void maintainButtonEnabled_BE(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend1.Visible = false;
            btnEdit1.Visible = false;
            btnCancel1.Visible = false;
            btnDelete1.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit1.Visible = true & sysValue.Authority.Update;
                    btnDelete1.Visible = true & sysValue.Authority.Delete;
                    btnCancel1.Visible = true;
                    if (ddl_BE_Kind.SelectedItem.Text == "本人")
                    {
                        btnEdit1.Visible = false;
                        btnDelete1.Visible = false;
                    }
                    else
                    {
                        btnEdit1.Visible = true;
                        btnDelete1.Visible = true;
                    }
                    this.SetFocus(ddl_BE_Kind);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend1.Visible = true & sysValue.Authority.Append;
                    btnCancel1.Visible = true;
                    this.SetFocus(ddl_BE_Kind);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[10].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[11].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[10].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(btn);
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[11].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[12].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[11].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[11].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_PD_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[11].Controls.Add(btn);
            }
        }
        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
                #region cbox,sendbtn
                e.Row.Cells[8].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[9].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(cboxi);
                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[8].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_BE_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[10].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPolicy.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[11].Text, GridView1.Rows[i].Cells[12].Text)));

                    //RServiceProvider rsp = DataCustPolicy.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustPolicy.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        protected void checksend_PD_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView2.Rows.Count; i++)
            {
                if (((CheckBox)GridView2.Rows[i].Cells[11].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPdt.GetSchema(), dataToHashtable_dlete_PD(GridView2.Rows[i].Cells[12].Text, GridView2.Rows[i].Cells[13].Text)));

                    //RServiceProvider rsp = DataCustPolicy.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustPdt.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            dataBind_GV2();
        }
        protected void checksend_BE_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView3.Rows.Count; i++)
            {
                if (GridView3.Rows[i].Cells[8].Controls.Count != 0 && ((CheckBox)GridView3.Rows[i].Cells[8].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustBenf.GetSchema(), dataToHashtable_dlete_BE(GridView3.Rows[i].Cells[9].Text, GridView3.Rows[i].Cells[10].Text)));

                    //RServiceProvider rsp = DataCustPolicy.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustBenf.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            dataBind_GV3();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }
        protected void GridView2_Load(object sender, EventArgs e)
        {
            if (hif_TransNo.Value != "")
            {
                dataBind_GV2();
            }
        }
        protected void GridView3_Load(object sender, EventArgs e)
        {
            if (hif_TransNo.Value != "")
            {
                dataBind_GV3();
            }
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("CU_TransNo")) return;
            if(Request.QueryString["id"] != null)
                e.InputParameters.Add("CU_TransNo", Request.QueryString["id"]);
            else
                e.InputParameters.Add("CU_TransNo", "");
            e.InputParameters.Add("CU_CustName", Lib.FdVP(txtSh_CU_CustName.Text).Trim());
            e.InputParameters.Add("CU_ID", Lib.FdVP(txtSh_CU_ID.Text).Trim());
            e.InputParameters.Add("CU_Sex", ddlSh_CU_Sex.SelectedItem.Text);
            e.InputParameters.Add("CU_Age", Lib.FdVP(txtSh_CU_Age.Text).Trim());
            e.InputParameters.Add("CU_Marry", ddlSh_CU_Marry.SelectedItem.Text);
            e.InputParameters.Add("CU_CustType", ddlSh_CU_CustType.SelectedItem.Text);
            e.InputParameters.Add("CU_CustSource", ddlSh_CU_CustSource.SelectedItem.Text);
            e.InputParameters.Add("CU_ContactMode", ddlSh_CU_ContactMode.SelectedItem.Text);
            e.InputParameters.Add("CU_IsShow", "1");
            e.InputParameters.Add("Mobile", Lib.FdVP(txtSh_Mobile.Text).Trim());
            e.InputParameters.Add("Address", Lib.FdVP(txtSh_Address.Text).Trim());
            e.InputParameters.Add("PO_ComSName", Lib.FdVP(txtSh_PO_ComSName.Text).Trim());
            e.InputParameters.Add("PO_PolNo", Lib.FdVP(txtSh_PO_PolNo.Text).Trim());
            e.InputParameters.Add("PO_AccureDate_s", DateRange_PO_AccureDate.Date_start);
            e.InputParameters.Add("PO_AccureDate_e", DateRange_PO_AccureDate.Date_end);
            e.InputParameters.Add("PO_Polmmemo", Lib.FdVP(txtSh_PO_Polmmemo.Text).Trim());
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_CU_CustName.Text = string.Empty;
            txtSh_CU_ID.Text = string.Empty;
            ddlSh_CU_Sex.SelectedIndex = -1;
            txtSh_CU_Age.Text = string.Empty;
            ddlSh_CU_Marry.SelectedIndex = -1;
            ddlSh_CU_CustType.SelectedIndex = -1;
            ddlSh_CU_CustSource.SelectedIndex = -1;
            ddlSh_CU_ContactMode.SelectedIndex = -1;
            txtSh_Mobile.Text = string.Empty;
            txtSh_Address.Text = string.Empty;
            txtSh_PO_ComSName.Text = string.Empty;
            txtSh_PO_PolNo.Text = string.Empty;
            DateRange_PO_AccureDate.Init();
            txtSh_PO_Polmmemo.Text = string.Empty;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void lnk_CommandAdd(object sender, CommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "1":
                    if(!btnEdit.Visible)
                    {
                        MainControls.showMsg(this, "請先新增保單資料！");
                        return;
                    }
                    DataRow dr_TCM = DataTABC_COMPANY_MAP.DataReader_TCM_ComCode(ddl_PO_ComSName.SelectedItem.Text);
                    if (dr_TCM != null)
                    {
                        selPdt.iStatus = "1";//壽險
                        selPdt.CompanyNo = dr_TCM["ComCode_GT"].ToString();
                    }
                    show_div("div_PD_edit");
                    //預設：只出現新增按鈕
                    this.maintainButtonEnabled_PD("");
                    break;
                case "2":
                    if (!btnEdit.Visible)
                    {
                        MainControls.showMsg(this, "請先新增保單資料！");
                        return;
                    }
                    show_div("div_BE_edit");
                    //預設：只出現新增按鈕
                    this.maintainButtonEnabled_BE("");
                    break;
            }
           
        }

        private void show_div(string clientID)
        {
            ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('#" + UpdatePanel1.ClientID + "').block({ message: $('#"+ clientID + "'), centerY: false , css: { width: '1000px', cursor: null, top: '20px' }, overlayCSS: {cursor: null}  }); ", true);
        }

        private void hide_div(string id)
        {

        }
    }
}