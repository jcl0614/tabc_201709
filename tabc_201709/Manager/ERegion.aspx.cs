﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ERegion : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EG_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddl_EG_Year);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddl_EG_Year);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1200);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataERegion.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EG_Type"] = DataPhrase.DDL_TypeName(ddlSh_EG_Type, true, "", "144");
            ViewState["ddl_EG_Type"] = DataPhrase.DDL_TypeName(ddl_EG_Type, true, "請選擇", "144");
            //DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
            //DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "138");
            //ViewState["ddlSh_EG_Region"] = DataPhrase.DDL_TypeName(ddlSh_EG_Region, true, "", "E005");
            //ViewState["cbl_EG_Region"] = DataPhrase.CBL_TypeName(cbl_EG_Region, "E005");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_EG_Year.SelectedValue == "")
                sbError.Append("●「年度」必須選取!<br>");
            if (ddl_EG_Type.SelectedValue == "")
                sbError.Append("●「考試類別」必須選取!<br>");
            if (txt_EG_Name.Text.Trim() == "")
                sbError.Append("●「考試名稱」必須輸入!<br>");
            if (txt_EG_ExamDate.Text.Trim() == "")
                sbError.Append("●「考試日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_ExamDate.Text.Trim()))
                    sbError.Append("●「考試日期」格式錯誤!<br>");
            }
            if (ddl_EG_Subject.SelectedValue == "")
                sbError.Append("●「考試科目」必須選取!<br>");
            if (txt_EG_RegistFee.Text.Trim() == "")
                sbError.Append("●「報名費用」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EG_RegistFee.Text.Trim())))
                    sbError.Append("●「報名費用」格式錯誤!<br>");
            }
            if (txt_EG_StartDate.Text.Trim() == "")
                sbError.Append("●「報名開始日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_StartDate.Text.Trim()))
                    sbError.Append("●「報名開始日期」格式錯誤!<br>");
            }
            if (txt_EG_Deadline.Text.Trim() == "")
                sbError.Append("●「報名截止日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_Deadline.Text.Trim()))
                    sbError.Append("●「報名截止日期」格式錯誤!<br>");
            }
            if (txt_EG_RegistDate.Text.Trim() == "")
                sbError.Append("●「公會報名日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_RegistDate.Text.Trim()))
                    sbError.Append("●「公會報名日期」格式錯誤!<br>");
            }
            if (txt_EG_Training.Text.Trim() == "")
                sbError.Append("●「訓練日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_Training.Text.Trim()))
                    sbError.Append("●「訓練日期」格式錯誤!<br>");
            }
            if (txt_EG_ReScore.Text.Trim() == "")
                sbError.Append("●「成績複查截止日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EG_ReScore.Text.Trim()))
                    sbError.Append("●「成績複查截止日期」格式錯誤!<br>");
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EG_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_UidNo":
                            hif_EG_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_Year":
                            string EG_Year = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            ddl_EG_Year.Items.Clear();
                            ddl_EG_Year.Items.Add(new ListItem("請選擇", ""));
                            if (int.Parse(EG_Year) <= DateTime.Now.Year)
                            {
                                for (int year = int.Parse(EG_Year); year <= DateTime.Now.Year + 1; year++)
                                    ddl_EG_Year.Items.Add(new ListItem(year.ToString(), year.ToString()));
                            }
                            else
                            {
                                for (int year = DateTime.Now.Year; year <= int.Parse(EG_Year); year++)
                                    ddl_EG_Year.Items.Add(new ListItem(year.ToString(), year.ToString()));
                            }
                            MainControls.ddlIndexSelectValue(ddl_EG_Year, EG_Year);
                            break;
                        case "EG_Type":
                            MainControls.ddlIndexSelectAttribute(ddl_EG_Type, "TypeSubCode", ViewState["ddl_EG_Type"] as DataTable, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            switch (ddl_EG_Type.SelectedValue)
                            {
                                case "1":
                                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "138");
                                    break;
                                case "2":
                                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "158");
                                    break;
                                case "3":
                                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "159");
                                    break;
                                case "4":
                                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "160");
                                    break;
                                default:
                                    ddl_EG_Subject.Items.Clear();
                                    break;
                            }
                            MainControls.ddlIndexSelectValue(ddl_EG_Subject, ViewState["EG_Subject"].ToString());
                            ViewState["cbl_EG_Region"] = DataPhrase.CBL_TypeName(cbl_EG_Region, (ViewState["ddl_EG_Type"] as DataTable).Rows[ddl_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
                            break;
                        case "EG_Name":
                            txt_EG_Name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_ExamDate":
                            txt_EG_ExamDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_Region":
                            string EG_Region = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            if (EG_Region.IndexOf(";") != -1)
                            {
                                string[] EG_Region_item = EG_Region.Split(';');
                                foreach (string item in EG_Region_item)
                                {
                                    if (!string.IsNullOrEmpty(item))
                                    {
                                        string[] value = item.Split(',');
                                        ListItem li = cbl_EG_Region.Items.FindByValue(value[1]);
                                        if(li != null)
                                            li.Selected = value[0] == "Y";
                                    }
                                }
                            }
                            break;
                        case "EG_Subject":
                            ViewState["EG_Subject"] = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            
                            break;
                        case "EG_RegistFee":
                            txt_EG_RegistFee.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EG_StartDate":
                            txt_EG_StartDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_Deadline":
                            txt_EG_Deadline.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_RegistDate":
                            txt_EG_RegistDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_Training":
                            txt_EG_Training.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EG_ReScore":
                            txt_EG_ReScore.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }

        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataERegion.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataERegion.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataERegion.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataERegion.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EG_UidNo.Value = string.Empty;
                        this.SetFocus(ddl_EG_Year);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataERegion.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataERegion.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataERegion.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EG_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EG_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EG_UidNo", hif_EG_UidNo.Value);
            }
            hsData.Add("EG_Year", ddl_EG_Year.SelectedItem.Value.Trim());
            hsData.Add("EG_Type", (ViewState["ddl_EG_Type"] as DataTable).Rows[ddl_EG_Type.SelectedIndex-1]["TypeSubCode"].ToString());
            hsData.Add("EG_Name", txt_EG_Name.Text.Trim());
            hsData.Add("EG_ExamDate", txt_EG_ExamDate.Text.Trim());
            StringBuilder sb_EG_Region = new StringBuilder();
            foreach (ListItem item in cbl_EG_Region.Items)
            {
                sb_EG_Region.AppendFormat("{0},{1},{2},{3};",
                        item.Selected ? "Y" : "N",
                        item.Value,
                        (ViewState["cbl_EG_Region"] as DataTable).Rows[cbl_EG_Region.Items.IndexOf(item)]["TypeSubCode"].ToString(),
                        item.Text);
            }
            hsData.Add("EG_Region", sb_EG_Region.ToString().TrimEnd(';'));
            hsData.Add("EG_Subject", ddl_EG_Subject.SelectedItem.Value.Trim());
            hsData.Add("EG_RegistFee", txt_EG_RegistFee.Text.Trim());
            hsData.Add("EG_StartDate", txt_EG_StartDate.Text.Trim());
            hsData.Add("EG_Deadline", txt_EG_Deadline.Text.Trim());
            hsData.Add("EG_RegistDate", txt_EG_RegistDate.Text.Trim());
            hsData.Add("EG_Training", txt_EG_Training.Text.Trim());
            hsData.Add("EG_ReScore", txt_EG_ReScore.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EG_TransNo", TransNo);
            hsData.Add("EG_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EG_UidNo.Value = string.Empty;
            ddl_EG_Year.Items.Clear();
            ddl_EG_Year.Items.Add(new ListItem("請選擇", ""));
            ddl_EG_Year.Items.Add(new ListItem(DateTime.Now.Year.ToString()));
            ddl_EG_Year.Items.Add(new ListItem((DateTime.Now.Year+1).ToString()));
            ddl_EG_Year.SelectedIndex = -1;
            ddl_EG_Type.SelectedIndex = -1;
            cbl_EG_Region.Items.Clear();
            txt_EG_Name.Text = string.Empty;
            txt_EG_ExamDate.Text = string.Empty;
            ddl_EG_Subject.SelectedIndex = -1;
            txt_EG_RegistFee.Text = string.Empty;
            txt_EG_StartDate.Text = string.Empty;
            txt_EG_Deadline.Text = string.Empty;
            txt_EG_RegistDate.Text = string.Empty;
            txt_EG_Training.Text = string.Empty;
            txt_EG_ReScore.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EG_Year);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EG_Year);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[9].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[10].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[9].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[9].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataERegion.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[10].Text, GridView1.Rows[i].Cells[11].Text)));

                    //RServiceProvider rsp = DataERegion.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataERegion.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EG_Year")) return;
            e.InputParameters.Add("EG_Year", txtSh_EG_Year.Text.Trim());
            if (ddlSh_EG_Type.SelectedIndex == 0)
                e.InputParameters.Add("EG_Type", "");
            else
                e.InputParameters.Add("EG_Type", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            e.InputParameters.Add("EG_Subject", ddlSh_EG_Subject.SelectedValue);
            string EG_Region = "";
            if (ddlSh_EG_Region.SelectedValue != "")
                EG_Region = string.Format("{0},{1},{2},{3}",
                        ddlSh_EG_Region.SelectedItem.Selected ? "Y" : "N",
                        ddlSh_EG_Region.SelectedItem.Value,
                        (ViewState["ddlSh_EG_Region"] as DataTable).Rows[ddlSh_EG_Region.SelectedIndex - 1]["TypeSubCode"].ToString(),
                        ddlSh_EG_Region.SelectedItem.Text);
            e.InputParameters.Add("EG_Region", EG_Region);
            e.InputParameters.Add("EG_Name", txtSh_EG_Name.Text.Trim());
            e.InputParameters.Add("EG_SDate_s", DateRangeSh_EG_StartDate.Date_start);
            e.InputParameters.Add("EG_SDate_e", DateRangeSh_EG_StartDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            txtSh_EG_Year.Text = string.Empty;
            ddlSh_EG_Type.SelectedIndex = -1;
            ddlSh_EG_Subject.SelectedIndex = -1;
            ddlSh_EG_Subject.Items.Clear();
            ddlSh_EG_Region.SelectedIndex = -1;
            ddlSh_EG_Region.Items.Clear();
            txtSh_EG_Name.Text = string.Empty;
            DateRangeSh_EG_StartDate.Init();
            
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void ddl_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddl_EG_Type.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddl_EG_Subject, true, "請選擇", "160");
                    break;
                default:
                    ddl_EG_Subject.Items.Clear();
                    break;
            }
            if (ddl_EG_Type.SelectedIndex > 0)
                ViewState["cbl_EG_Region"] = DataPhrase.CBL_TypeName(cbl_EG_Region, (ViewState["ddl_EG_Type"] as DataTable).Rows[ddl_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                cbl_EG_Region.Items.Clear();
        }

        protected void ddlSh_EG_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (ddlSh_EG_Type.SelectedValue)
            {
                case "1":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "138");
                    break;
                case "2":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "158");
                    break;
                case "3":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "159");
                    break;
                case "4":
                    DataPhrase.DDL_TypeName(ddlSh_EG_Subject, true, "", "160");
                    break;
                default:
                    ddlSh_EG_Subject.Items.Clear();
                    break;
            }
            if(ddlSh_EG_Type.SelectedIndex > 0)
                ViewState["ddlSh_EG_Region"] = DataPhrase.DDL_TypeName(ddlSh_EG_Region, true, "", (ViewState["ddlSh_EG_Type"] as DataTable).Rows[ddlSh_EG_Type.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                ddlSh_EG_Region.Items.Clear();
        }


    }
}