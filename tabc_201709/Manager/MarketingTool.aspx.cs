﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MarketingTool : System.Web.UI.Page
    {
        protected string btnactive1 = "active";
        protected string btnactive2 = "";
        protected string btnactive3 = "";
        protected string btnactive4 = "";
        protected string btnactive5 = "";
        protected string btnactive6 = "";


        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage10);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                //    this.SetFocus(txtSh_title);
                //    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    //if (btnEdit.Visible)
                    //{
                    //    this.SetFocus(txt_title);
                    //    Panel_Form.DefaultButton = btnEdit.ID;
                    //}
                    //else
                    //{
                    //    this.SetFocus(txt_title);
                    //    Panel_Form.DefaultButton = btnAppend.ID;
                    //}
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNO"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 850);
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataMarketingTool.DataColumn(this.GridView1);
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            //if (ddl_class.SelectedValue == "")
            //    sbError.Append("●「分類」必須選取!<br>");
            //if (txt_title.Text.Trim() == "")
            //    sbError.Append("●「標題文字」必須輸入!<br>");
            //if (MainControls.htmlText(ckeditor1.Html).Trim() == "")
            //    sbError.Append("●「內容」必須輸入!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    //switch (bf.DataField)
                    //{
                    //    case "id":
                    //        hf_id.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                    //        break;
                    //    case "class":
                    //        MainControls.ddlIndexSelectValue(ddl_class, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                    //        break;
                    //    case "title":
                    //        txt_title.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                    //        break;
                    //    case "description":
                    //        ckeditor1.Html = Server.HtmlDecode(gvr.Cells[i].Text);
                    //        break;
                    //    case "addDate":
                    //        lbl_addDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                    //        break;
                    //    case "emName":
                    //        lbl_name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                    //        break;
                    //    case "emNo":
                    //        hf_emNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                    //        break;
                    //}
                }
            //切換至維護畫面
            ChangeMultiView(1);
         //   maintainButtonEnabled("Select");
        }
        #endregion


        #region 資料轉至hashtable
        private Hashtable dataToHashtable()
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("kind", HiddenField1.Value);

            return hsData;
        }
        #endregion


        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
              //      lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
            //        lnkBrowse.Visible = true;
                    break;
            }
        }
        #endregion

 
        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls.Count >0 && e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("kind")) return;
            e.InputParameters.Add("kind", HiddenField1.Value);
            //
        }
        #endregion

        #region 執行搜尋
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            btnactive1 = "";
            btnactive2 = "";
            btnactive3 = "";
            btnactive4 = "";
            btnactive5 = "";
            btnactive6 = "";
        //    ImageButton1.CssClass = ImageButton1.CssClass.Replace("active","");
        //    ((ImageButton)sender).CssClass = ((ImageButton)sender).CssClass + " active";
            if (((ImageButton)sender).ID == "ImageButton1")
            {
                HiddenField1.Value = "1101";
                btnactive1 = "active";
                Literal1.Text = "退休行銷展示資料";
            }
            else if (((ImageButton)sender).ID == "ImageButton2")
            {
                HiddenField1.Value = "1102";
                btnactive2 = "active";
                Literal1.Text = "長照行銷展示資料";
            }
            else if (((ImageButton)sender).ID == "ImageButton3")
            {
                HiddenField1.Value = "1103";
                 btnactive3 = "active";
                 Literal1.Text = "醫療行銷展示資料";
            }
            else if (((ImageButton)sender).ID == "ImageButton4")
            {
                HiddenField1.Value = "1105";
                 btnactive4 = "active";
                 Literal1.Text = "台名制度手冊";
            }
            else if (((ImageButton)sender).ID == "ImageButton5")
            {
                HiddenField1.Value = "1106";
                 btnactive5 = "active";
                 Literal1.Text = "增員手冊";
            }
            else if (((ImageButton)sender).ID == "ImageButton6")
            {
                HiddenField1.Value = "1108";
                 btnactive6 = "active";
                 Literal1.Text = "商品類型彙總表";
            }
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();

        }

        #endregion


        #endregion
    }
}