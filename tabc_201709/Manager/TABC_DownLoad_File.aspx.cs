﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class TABC_DownLoad_File : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sFileFullPath = Request["FileFullPath"] ?? "";
            string localpath = Server.MapPath("");
            if (sFileFullPath.Length > 0)
            {

                string fileUrlPath = localpath+"\\"+sFileFullPath.Replace(';', '\\');
                //用戶端的物件
                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] file = null;

                try
                {
                    //用戶端下載檔案到byte陣列
                    file = wc.DownloadData(fileUrlPath);
                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write("下載的檔案不存在，請聯絡相關人員");
                    // HttpContext.Current.Response.Write("ASP.net禁止下載此敏感檔案(通常為：.cs、.vb、微軟資料庫mdb、mdf和config組態檔等)。<br/>檔案路徑：" + fileUrlPath + "<br/>錯誤訊息：" + ex.ToString());
                    return;
                }

                HttpContext.Current.Response.Clear();
                string fileName = System.IO.Path.GetFileName(fileUrlPath);
                //跳出視窗，讓用戶端選擇要儲存的地方                         //使用Server.UrlEncode()編碼中文字才不會下載時，檔名為亂碼
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                //設定MIME類型為二進位檔案
                HttpContext.Current.Response.ContentType = "application/octet-stream";

                try
                {
                    //檔案有各式各樣，所以用BinaryWrite
                    HttpContext.Current.Response.BinaryWrite(file);

                }
                catch (Exception ex)
                {
                    HttpContext.Current.Response.Write("下載的檔案不存在，請聯絡相關人員");
                    //   HttpContext.Current.Response.Write("檔案輸出有誤，您可以在瀏覽器的URL網址貼上以下路徑嘗試看看。<br/>檔案路徑：" + fileUrlPath + "<br/>錯誤訊息：" + ex.ToString());
                    return;
                }

                //這是專門寫文字的
                //HttpContext.Current.Response.Write();

            }
            else
                HttpContext.Current.Response.Write("下載的檔案不存在，請聯絡相關人員");
            HttpContext.Current.Response.End();

        }
    }
}