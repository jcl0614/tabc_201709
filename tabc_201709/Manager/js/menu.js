$(document).ready(function(){
    $('.menuLbn').bind('click', _openTextEvent);
    $('#menu_open').bind('click', _openAll);
    $('#menu_close').bind('click', _closeAll);
});

function _openTextEvent(){
    if ($(this).parent().attr('class') == 'menuLayer active') {
        $(this).parent().removeClass('active');
        $(this).parent().find('.menu_item_bk').not(':animated').slideUp(500);
        $(this).css("background-image", "url('images/menu_item.png'),url('../images/menu_item.png')");
	}
	else {
		$(this).parent().addClass('active');
		$(this).parent().find('.menu_item_bk').not(':animated').slideDown(500);
		$(this).css("background-image", "url('images/menu_item_active.png'),url('../images/menu_item_active.png')");
	}
}

function _openAll() {
    if ($('.menuLayer').attr('class') != 'menuLayer active') {

    }
    $('.menuLayer').addClass('active');
    $('.menuLayer').find('.menu_item_bk').not(':animated').slideDown(500);
    $('.menuLbn').css("background-image", "url('images/menu_item_active.png'),url('../images/menu_item_active.png')");
}

function _closeAll() {
    if ($('.menuLayer').attr('class') == 'menuLayer active') {

    }
    $('.menuLayer').removeClass('active');
    $('.menuLayer').find('.menu_item_bk').not(':animated').slideUp(500);
    $('.menuLbn').css("background-image", "url('images/menu_item.png'),url('../images/menu_item.png')");
}