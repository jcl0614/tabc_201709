/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 config.language = 'zh-tw';
    //config.uiColor = '#AADC6E';

	 config.toolbar =
 [
     { name: 'document', items: ['Source', '-', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates'] },
     { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
     { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt'] },
     //{
     //    name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
     //      'HiddenField']
     //},
     '/',
     { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
     {
         name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
         '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
     },
     { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
     { name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe', 'Slideshow', 'cssanim'] },
     '/',
     { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
     { name: 'colors', items: ['TextColor', 'BGColor'] },
     { name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'A11ychecker'] }
 ];

	// config.toolbarGroups = [
    // { name: 'clipboard', groups: ['clipboard', 'undo'] },
    // { name: 'editing', groups: ['find', 'selection', 'spellchecker'] },
    // { name: 'links' },
    // { name: 'insert' },
    // { name: 'forms' },
    // { name: 'tools' },
    // { name: 'document', groups: ['mode', 'document', 'doctools'] },
    // { name: 'others' },
    // '/',
    // { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    // { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'] },
    // { name: 'styles' },
    // { name: 'colors' },
    // //{ name: 'about' }
	// ],
    //config.removeButtons= '�x�s'
    //;
};
