﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace wowbowow.js.ckeditor.plugins.ckfinder.core.connector.aspx
{
    public partial class connector_ : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));

        protected void Page_Load(object sender, EventArgs e)
        {
            //登入驗證
            //if (DataEmployee.emNoValid(Session["emNo"].ToString().Trim()))
            //{
            //    //關閉式窗
            //    //ScriptManager.RegisterStartupScript(this, this.GetType(), MainControls.random("", 8), "window.opener=null; window.open('', '_self', ''); window.close();", true);
            //    Response.Write("<html><body style=\"display:none;\"><script>alert('請先登入!');</script></body></html>");
            //    return;

            //}
            
            //if (!IsPostBack)
            //{
            //    if (Request.UrlReferrer.AbsoluteUri.IndexOf("/Article") != -1)
            //    {
            //        //管理權限
            //        //系統參數
            //        for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
            //        {
            //            if (DataMenu.PageLimitCheck("Article", Session["emNo"].ToString()) && new SysValue("Article", this.Session).Authority.Select)
            //            {
            //                ViewState["SysValue"] = new SysValue("Article", this.Session);
            //                break;
            //            }
            //        }
            //        if (ViewState["SysValue"] != null)
            //        {
            //            SysValue sysValue = ViewState["SysValue"] as SysValue;
            //            if (!sysValue.Authority.Select || !sysValue.Authority.Append)
            //                Response.Write("<html><body style=\"display:none;\"><script>alert('沒有權限!');</script></body></html>");
            //            else
            //            {
            //                if (Session["_ArticleId"] == null)
            //                {
            //                    Response.Write("<html><body style=\"display:none;\"><script>alert('請先新增資料再上傳圖片!');</script></body></html>");
            //                }
            //                else
            //                {
            //                    try
            //                    {
            //                        var upload = Request.Files["upload"];
            //                        string CKEditorFuncNum = Request["CKEditorFuncNum"];
            //                        string CKEditor = Request["CKEditor"];
            //                        string langCode = Request["langCode"];

            //                        string fileExtension;
            //                        switch (upload.ContentType)
            //                        {
            //                            case "image/jpg": fileExtension = "jpg"; break;
            //                            case "image/jpeg": fileExtension = "jpg"; break;
            //                            case "image/png": fileExtension = "png"; break;
            //                            case "image/gif": fileExtension = "jpg"; break;
            //                            case "image/bmp": fileExtension = "bmp"; break;
            //                            default: fileExtension = ""; break;
            //                        }
            //                        if (fileExtension == "")
            //                            Response.Write("<html><body style=\"display:none;\"><script>alert('必須是圖檔格式!');</script></body></html>");

            //                        string path = Server.MapPath(string.Format(@"~/uploads/article-content/{0}", Session["_ArticleId"].ToString()));
            //                        if (!Directory.Exists(path))
            //                        {
            //                            //新增資料夾
            //                            Directory.CreateDirectory(path);

            //                        }

            //                        string fileName = string.Format("{0}.{1}", MainControls.GetDateStr(), fileExtension);
            //                        upload.SaveAs(Server.MapPath(string.Format("~/uploads/article-content/{0}/{1}", Session["_ArticleId"].ToString(), fileName)));
            //                        var imageUrl = ResolveUrl(string.Format("~/uploads/article-content/{0}/{1}", Session["_ArticleId"].ToString(), fileName));
            //                        var vMessage = string.Empty;
            //                        Response.Write("<html><body><script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + imageUrl + "\", \"" + vMessage + "\");</script></body></html>");
            //                    }
            //                    catch (Exception ex)
            //                    {
            //                        logger.Error(string.Format("來源頁面(ckeditor)：{0}\r\n{1}", Request.Path, ex.ToString()));
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            Response.Write("<html><body style=\"display:none;\"><script>alert('沒有權限!');</script></body></html>");
            //        }
                    
            //    }
            //}


            
        }
    }
}