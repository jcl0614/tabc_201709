﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" validateRequest="false" CodeBehind="MarketingTool.aspx.cs" Inherits="tabc_201709.Manager.MarketingTool" %>


<%@ Register src="ascx/ckeditor.ascx" tagname="ckeditor" tagprefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">

            <asp:LinkButton ID="lnkMaintain" runat="server" CommandName="1" 
                CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/add.png") %>" border="0" style="vertical-align:bottom" /> 新增</asp:LinkButton>
            &nbsp;&nbsp;&nbsp;
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0" 
                CssClass="MainHeadTdLink" oncommand="lnk_Command" CausesValidation="False"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 瀏覽</asp:LinkButton>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <style>
        .imgItem{
            width: 120px;
            height: 80px;
        }
        ul{
    display: block;
    list-style-type: disc;
    -webkit-margin-before: 1em;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 40px;
       }
       ul, ol {
    margin-top: 0;
    margin-bottom: 10px;
}
.btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}
.btn.active {
    background-image: none;
    outline: 0;
    -webkit-box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
    box-shadow: inset 0 3px 5px rgba(0, 0, 0, .125);
}
.btn-default {
    color: #333;
    background-color: #fff;
    border-color: #ccc;
}
.btn-warning {
    color: #fff;
    background-color: #f0ad4e;
    border-color: #eea236;
}
.btn-xs, .btn-group-xs > .btn {
    padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5;
    /* border-radius: 3px; */
}
    </style>
    <script>

    function chgTab(mNo, mId) {
        $("#nav1 li").removeClass("active");
        $("#" + mId).addClass("active");
    }

    function getcycloByItem1(sFullPath) {
     //   var row = $('body').find('#' + rowid);
    //    var sFullPath = row.data('data').FullPath;
        var param_ = "width=300,height=" + (screen.availHeight - 50) + ",resizable=yes,menubar=no,directories=no,help=no,toolbar=no,location=no,status=no,scrollbars=yes,top=0,left=10";
     //   var param_ = "width=300,height=" + (screen.availHeight - 50) + ",resizable=1,menubar=0,directories=0,help=0,toolbar=0,location=0,status=0,scrollbars=1,top=0,left=10";
        //  var param_ = "height=200,width=200";
        if (sFullPath != "") {
            var murl = "TABC_DownLoad_File.aspx?FileFullPath=" + encodeURI(sFullPath);
            var cyclofrm = window.open(murl, 'cyclofrm', param_);
            cyclofrm.focus();
        }
    }
    </script>
            <asp:Panel ID="Panel_Form" runat="server">
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table align="center" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center" height="3">
                                    <table cellpadding="0" cellspacing="0" class="QueryMainTable">
                                        <tr align="center" style="height: 120px">

                                            <td align="center" class="tdQueryData" style="width: 100%">
                                        <ul class="thumbnails" id="nav1">
            <li class="btn btn-default <%=btnactive1 %>" id="nav1_0" onclick="chgTab(0,this.id);">退休行銷展示資料<br>
                                                <asp:ImageButton ID="ImageButton1" runat="server" 
                                                    ImageUrl="~/Manager/images/退休行銷公版.jpg" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>
            <li class="btn btn-default <%=btnactive2 %>" id="nav1_1" onclick="chgTab(1,this.id);">長照行銷展示資料<br>
                                                <asp:ImageButton ID="ImageButton2" runat="server" 
                                                    ImageUrl="~/Manager/images/長照行銷公版.jpg" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>
            <li class="btn btn-default <%=btnactive3 %>" id="nav1_2" onclick="chgTab(2,this.id);">醫療行銷展示資料<br>
                                                 <asp:ImageButton ID="ImageButton3" runat="server" 
                                                    ImageUrl="~/Manager/images/醫療行銷公版.jpg" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>
            <li class="btn btn-default <%=btnactive4 %>" id="nav1_4" onclick="chgTab(4,this.id);">台名制度手冊<br>
                                                <asp:ImageButton ID="ImageButton4" runat="server" 
                                                    ImageUrl="~/Manager/images/台名制度手冊.jpg" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>
            <li class="btn btn-default <%=btnactive5 %>" id="nav1_5" onclick="chgTab(5,this.id);">增員手冊<br>
                                                 <asp:ImageButton ID="ImageButton5" runat="server" 
                                                     ImageUrl="~/Manager/images/增員手冊.jpg" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>
            <li class="btn btn-default <%=btnactive6 %>" id="nav1_7" onclick="chgTab(7,this.id);">商品類型彙總表<br>
                                                <asp:ImageButton ID="ImageButton6" runat="server" 
                                                    ImageUrl="~/Manager/images/商品類型彙總表.gif" 
                    class="btn img-rounded imgItem" onclick="ImageButton1_Click" />
                </li>

        </ul>
                                            </td>
                                            <td align="center" width="85">
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value="1101" />
                                            </td>
                                        </tr>
                                    </table>

                                      <table border="0" cellpadding="5" cellspacing="0"  
                                        width="100%" bgcolor="#F8F3E5">
                                        <tr bgcolor="#F8F3E5">
                                            <td height="60px" style="font-size: 24px">
                                                <asp:Literal ID="Literal1" runat="server" Text="退休行銷展示資料"></asp:Literal>
                                            </td>
                                        </tr>
                                    </table>
                                      <table border="0" cellpadding="5" cellspacing="0" class="tablePageCtrl" 
                                        width="100%">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlPageRow" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                                
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlPageChange" runat="server" AutoPostBack="True" 
                                                    CssClass="ddlPage">
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Label ID="labGridViewRows" runat="server" CssClass="GridPageCount" 
                                                    Width="120px"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlFirst" runat="server" CommandName="First" 
                                                    CssClass="btnPage_first" Width="50px">第一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlPrev" runat="server" CommandName="Prev" 
                                                    CssClass="btnPage_pre" Width="50px">上一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlNext" runat="server" CommandName="Next" 
                                                    CssClass="btnPage_next" Width="50px">下一頁</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lbtnPageCtrlLast" runat="server" CommandName="Last" 
                                                    CssClass="btnPage_final" Width="50px">最後頁</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                        CssClass="GridViewTable" DataSourceID="ObjectDataSource1" 
                                        EmptyDataText="查無符合資料!!!" 
                                        onselectedindexchanged="GridView1_SelectedIndexChanged">
                                        <PagerSettings Visible="False" />
                                    </asp:GridView>
                                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" EnablePaging="True" 
                                        SelectCountMethod="GetCount" SelectMethod="DataReader" TypeName="DataMarketingTool">
                                    </asp:ObjectDataSource>
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </asp:Panel>

</asp:Content>
