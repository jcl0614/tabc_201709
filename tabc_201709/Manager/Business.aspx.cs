﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class Business : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                if (dr_person != null)
                {
                    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                    Session["AccID"] = dr_person["AccID"].ToString();
                    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                }
                DataRow dr_em = DataEmployee.emDataReader("11526");
                Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                Session["emNo"] = dr_em["emNo"].ToString().Trim();
                Session["emID"] = dr_em["emID"].ToString().Trim();
                Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                ViewState["SysValue"] = new SysValue("Business", this.Session);

                //系統參數
                //for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                //{
                //    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                //    {
                //        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                //        break;
                //    }
                //}
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1200);
                //下拉選單設定
                setddlChoices();

            }

        }

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddl_SC_Kind"] = DataPhrase.DataReader_TypeCode("107").Tables[0];
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = DataScheduleM.DataReader_AccID("TB0232").Tables[0];
            GridView1.DataSource = dt;
            GridView1.DataBind(); ;
        }

        protected void btnRestQuery_Click(object sender, EventArgs e)
        {

        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddl_SC_Kind = (DropDownList)e.Row.Cells[2].FindControl("ddl_SC_Kind");
                DropDownList ddl_SC_Plan = (DropDownList)e.Row.Cells[2].FindControl("ddl_SC_Plan");
                DataPhrase.DDL_TypeName(ddl_SC_Plan, true, "", (ViewState["ddl_SC_Kind"] as DataTable).Rows[ddl_SC_Kind.SelectedIndex]["TypeSubCode"].ToString());
            }
        }

        protected void ddl_SC_Kind_SelectedIndexChanged(object sender, EventArgs e)
        {
            int rowIndex = int.Parse(((DropDownList)sender).ToolTip);
            DropDownList ddl_SC_Plan = (DropDownList)GridView1.Rows[rowIndex].Cells[2].FindControl("ddl_SC_Plan");
            DataPhrase.DDL_TypeName(ddl_SC_Plan, true, "", (ViewState["ddl_SC_Kind"] as DataTable).Rows[((DropDownList)sender).SelectedIndex]["TypeSubCode"].ToString());
        }

        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("TypeCode")) return;
            e.InputParameters.Add("TypeCode", "107");
            //
        }

        protected void btnAppend_Click(object sender, EventArgs e)
        {

        }
    }
}