﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EAttendFinish : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddl_ED_Type);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddl_ED_Type);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddl_ED_Type);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 900);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEAttend.DataColumn_AttHours(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_ED_Type, true, "", "131");
            ddlSh_ED_Type.SelectedIndex = 1;
            DataPhrase.DDL_TypeName(ddl_ED_Type, true, "請選擇", "131");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (selCourse_EC_TransNo.SelectedValue.Trim() == "")
                sbError.Append("●「課程名稱」必須選取!<br>");
            if (selPerson_AccID_ED_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「人員」必須選取!<br>");
            if (ddl_ED_Type.SelectedValue == "")
                sbError.Append("●「上課狀況」必須選取!<br>");
            else
            {
                DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
                if (dr_course != null)
                {

                    switch (ddl_ED_Type.SelectedValue)
                    {
                        case "1":
                            if (TimeRangeAll_ED_SignTime.Time_start == "" || TimeRangeAll_ED_SignTime.Time_end == "")
                                sbError.Append("●「上課簽名時間」必須選取!<br>");
                            else
                            {
                                TimeRangeAll_ED_SignTime.FieldName = "上課簽名時間";
                                if (TimeRangeAll_ED_SignTime.valid != "")
                                    sbError.Append(TimeRangeAll_ED_SignTime.valid);
                                else
                                {
                                    if (txt_ED_SignDate.Text.Trim() == "")
                                        sbError.Append("●「上課簽名日期」必須輸入!<br>");
                                    else
                                    {
                                        if (!MainControls.ValidDate(txt_ED_SignDate.Text.Trim()))
                                            sbError.Append("●「上課簽名日期」格式錯誤!<br>");
                                        else
                                        {
                                            if (DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", txt_ED_SignDate.Text.Trim(), TimeRangeAll_ED_SignTime.Time_start)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SigninTime"].ToString()))) > 0 ||
                                                DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", txt_ED_SignDate.Text.Trim(), TimeRangeAll_ED_SignTime.Time_end)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SignoffTime"].ToString()))) < 0)
                                                sbError.AppendFormat("●「上課狀況」與「上課簽名時間」({0}~{1})不符!<br>", dr_course["EC_SigninTime"].ToString(), dr_course["EC_SignoffTime"].ToString());
                                        }
                                    }

                                }
                            }

                            break;
                        case "2":
                            if (TimeRangeAll_ED_SignTime.Time_start != "" || TimeRangeAll_ED_SignTime.Time_end != "")
                                sbError.AppendFormat("●「上課狀況」與「上課簽名時間」({0})不符!<br>", "不必選取");
                            break;
                        case "3":
                            if (TimeRangeAll_ED_SignTime.Time_start == "" || TimeRangeAll_ED_SignTime.Time_end == "")
                                sbError.Append("●「上課簽名時間」必須選取!<br>");
                            else
                            {
                                TimeRangeAll_ED_SignTime.FieldName = "上課簽名時間";
                                if (TimeRangeAll_ED_SignTime.valid != "")
                                    sbError.Append(TimeRangeAll_ED_SignTime.valid);
                                else
                                {
                                    if (txt_ED_SignDate.Text.Trim() == "")
                                        sbError.Append("●「上課簽名日期」必須輸入!<br>");
                                    else
                                    {
                                        if (!MainControls.ValidDate(txt_ED_SignDate.Text.Trim()))
                                            sbError.Append("●「上課簽名日期」格式錯誤!<br>");
                                        else
                                        {
                                            if (DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", txt_ED_SignDate.Text.Trim(), TimeRangeAll_ED_SignTime.Time_start)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SigninTime"].ToString()))) <= 0 &&
                                                DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", txt_ED_SignDate.Text.Trim(), TimeRangeAll_ED_SignTime.Time_end)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SignoffTime"].ToString()))) > 0)
                                                sbError.AppendFormat("●「上課狀況」與「上課簽名時間」({0}~{1})不符!<br>", dr_course["EC_SigninTime"].ToString(), dr_course["EC_SignoffTime"].ToString());
                                        }
                                    }

                                }
                            }
                            break;
                    }
                }
            }
            if (txt_ED_AttHours.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_ED_AttHours.Text.Trim())))
                    sbError.Append("●「訓練時數」格式錯誤!<br>");
            }


            return sbError.ToString();
        }
        private string dataValid_import(string EC_Year, string EC_TrainType, string EC_Type, string EC_Mode, string EC_Kind, string EC_CName, string EC_SDate, string ED_AccID, string ED_Type, string ED_AttHours)
        {
            //string ED_SigninTime = "09:00";
            //string ED_SignoffTime = "09:00";
            StringBuilder sbError = new StringBuilder();

            if (EC_Year == "")
                sbError.Append("●「年度」必須輸入! ");
            if (EC_TrainType == "")
                sbError.Append("●「類別」必須輸入! ");
            if (EC_Type == "")
                sbError.Append("●「課程分類」必須輸入! ");
            if (EC_Kind == "")
                sbError.Append("●「課程種類」必須輸入! ");
            if (EC_CName == "")
                sbError.Append("●「課程名稱」必須輸入! ");
            if (EC_SDate == "")
                sbError.Append("●「開課日期」必須輸入! ");
            if (ED_AccID == "")
                sbError.Append("●「業務員編號」必須輸入! ");
            if (ED_AttHours == "")
                sbError.Append("●「上課時數」必須輸入! ");
            else
            {
                if (!MainControls.ValidDataType("^[0-9]+(.[0-9]{1,1})?$", ED_AttHours))
                    sbError.Append("●「上課時數」格式錯誤! ");
                //else
                //{
                //    double mm = (9 + double.Parse(ED_AttHours)) * 60;
                //    ED_SignoffTime = string.Format("{0}:{1}", (mm/60).ToString("00"), (mm % 60).ToString("00"));
                //}
            }
            if (ED_Type == "")
                sbError.Append("●「上課狀況」必須輸入! ");
            else
            {
                DataRow dr_course = DataECourse.valid_EC2(EC_Year, EC_TrainType, EC_Type, EC_Mode, EC_Kind, EC_CName, EC_SDate);
                if (dr_course != null)
                {
                    //if (!DataEAttend.valid_ED(dr_course["EC_TransNo"].ToString(), ED_AccID))
                    //    sbError.AppendFormat("●「業務員編號」({0})已匯入對應課程資料! ", ED_AccID);
                    //else
                    //{
                    //    switch (ED_Type)
                    //    {
                    //        case "1":
                    //            if (ED_SigninTime == "" || ED_SignoffTime == "")
                    //                sbError.Append("●「上課簽到時間」必須選取! ");
                    //            else
                    //            {
                    //                if (EC_SDate == "")
                    //                    sbError.Append("●「開課日期」必須輸入! ");
                    //                else
                    //                {
                    //                    if (!MainControls.ValidDate(EC_SDate))
                    //                        sbError.Append("●「開課日期」格式錯誤! ");
                    //                    else
                    //                    {
                    //                        if (DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", EC_SDate, ED_SigninTime)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SigninTime"].ToString()))) > 0 ||
                    //                            DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", EC_SDate, ED_SignoffTime)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SignoffTime"].ToString()))) < 0)
                    //                            sbError.AppendFormat("●「上課狀況」與「上課簽到時間」({0}~{1})不符! ", dr_course["EC_SigninTime"].ToString(), dr_course["EC_SignoffTime"].ToString());
                    //                    }
                    //                }
                    //            }

                    //            break;
                    //        case "2":
                    //            if (ED_SigninTime != "" || ED_SignoffTime != "")
                    //                sbError.AppendFormat("●「上課狀況」與「上課簽到時間」({0})不符! ", "不必輸入");
                    //            break;
                    //        case "3":
                    //            if (ED_SigninTime == "" || ED_SignoffTime == "")
                    //                sbError.Append("●「上課簽到時間」必須輸入! ");
                    //            else
                    //            {
                    //                if (EC_SDate == "")
                    //                    sbError.Append("●「開課日期」必須輸入! ");
                    //                else
                    //                {
                    //                    if (!MainControls.ValidDate(EC_SDate))
                    //                        sbError.Append("●「開課日期」格式錯誤! ");
                    //                    else
                    //                    {
                    //                        if (DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", EC_SDate, ED_SigninTime)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SigninTime"].ToString()))) <= 0 &&
                    //                            DateTime.Compare(DateTime.Parse(string.Format("{0} {1}", EC_SDate, ED_SignoffTime)), DateTime.Parse(string.Format("{0} {1}", dr_course["EC_SDate"].ToString(), dr_course["EC_SignoffTime"].ToString()))) > 0)
                    //                            sbError.AppendFormat("●「上課狀況」與「上課簽到時間」({0}~{1})不符! ", dr_course["EC_SigninTime"].ToString(), dr_course["EC_SignoffTime"].ToString());
                    //                    }
                    //                }
                    //            }
                    //            break;
                    //        default:
                    //            sbError.Append("●「上課狀況」格式錯誤! ");
                    //            break;
                    //    }
                    //}
                }
                else
                    sbError.Append("●課程不存在，請先建立該課程! ");
            }
            if(DataPerson.DataReader_AccID(ED_AccID) == null)
                sbError.Append("●「業務員編號」不存在! ");


            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "ED_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ED_UidNo":
                            hif_ED_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EC_SDate_Str":
                            lbl_EC_SDateTime.Text = gvr.Cells[i].Text;
                            break;
                        case "EC_TransNo":
                            selCourse_EC_TransNo.EC_Category = "2";
                            hif_EC_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
                            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
                            selCourse_EC_TransNo.SearchText = string.Empty;
                            break;
                        case "ED_AccID":
                            selPerson_AccID_ED_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_ED_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_ED_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_person = DataPerson.DataReader_AccID(hif_ED_AccID.Value);
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(hif_ED_AccID.Value);
                            if (dr_person != null)
                            {
                                txt_PS_Title.Text = dr_person["PS_Title"].ToString();
                            }
                            if (dr_sales != null)
                            {
                                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["cZON_PK"].ToString());
                                if (dr_Unit != null)
                                {
                                    txt_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                                    txt_cZON_NAME.Text = string.Format("{0} {1}", dr_Unit["cZON_NAME"].ToString(), dr_Unit["cZON_PK"].ToString());
                                }
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    txt_DM_NAME.Text = dr_sales2["NAME"].ToString();
                                }
                            }
                            break;
                        case "ED_Type":
                            MainControls.ddlIndexSelectValue(ddl_ED_Type, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "ED_AttHours":
                            txt_ED_AttHours.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ED_SignDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_ED_SignDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "ED_SigninTime":
                            TimeRangeAll_ED_SignTime.Time_start = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ED_SignoffTime":
                            TimeRangeAll_ED_SignTime.Time_end = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;


                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selCourse_EC_TransNo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selCourse_EC_TransNo_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selCourse_EC_TransNo_SearchButtonClick(object sender, EventArgs e)
        {
            selCourse_EC_TransNo.EC_Category = "2";
        }
        protected void selCourse_EC_TransNo_RefreshButtonClick(object sender, EventArgs e)
        {
            selCourse_EC_TransNo.search(hif_EC_TransNo.Value);
            selCourse_EC_TransNo.SelectedValue = hif_EC_TransNo.Value;
        }
        protected void selPerson_AccID_ED_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_ED_AccID.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_ED_AccID.SelectedValue);
            if (dr_person != null)
            {
                txt_PS_Title.Text = dr_person["PS_Title"].ToString();
            }
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["cZON_PK"].ToString());
                if (dr_Unit != null)
                {
                    txt_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                    txt_cZON_NAME.Text = string.Format("{0} {1}", dr_Unit["cZON_NAME"].ToString(), dr_Unit["cZON_PK"].ToString());
                }
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    txt_DM_NAME.Text = dr_sales2["NAME"].ToString();
                }
            }
        }
        protected void selPerson_AccID_ED_AccID_TextChanged(object sender, EventArgs e)
        {
            txt_cZON_TYPE.Text = string.Empty;
            txt_cZON_NAME.Text = string.Empty;
            txt_DM_NAME.Text = string.Empty;
            txt_PS_Title.Text = string.Empty;
        }
        protected void selPerson_AccID_ED_AccID_SearchButtonClick(object sender, EventArgs e)
        {
            txt_cZON_TYPE.Text = string.Empty;
            txt_cZON_NAME.Text = string.Empty;
            txt_DM_NAME.Text = string.Empty;
            txt_PS_Title.Text = string.Empty;
        }
        protected void selPerson_AccID_ED_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_ED_AccID.search(hif_ED_AccID.Value);
            selPerson_AccID_ED_AccID.SelectedValue = hif_ED_AccID.Value;
            DataRow dr_person = DataPerson.DataReader_AccID(hif_ED_AccID.Value);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(hif_ED_AccID.Value);
            if (dr_person != null)
            {
                txt_PS_Title.Text = dr_person["PS_Title"].ToString();
            }
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["cZON_PK"].ToString());
                if (dr_Unit != null)
                {
                    txt_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                    txt_cZON_NAME.Text = string.Format("{0} {1}", dr_Unit["cZON_NAME"].ToString(), dr_Unit["cZON_PK"].ToString());
                }
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    txt_DM_NAME.Text = dr_sales2["NAME"].ToString();
                }
            }
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEAttend.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Import": //匯入
                    {
                        try
                        {
                            RServiceProvider rsp_import = ExportControls.ReadExcel(FileUpload1, 0);
                            if (rsp_import.Result)
                            {
                                StringBuilder sb_import = new StringBuilder();
                                DataTable dt = rsp_import.ReturnData as DataTable;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    string valid_error = dataValid_import(
                                            dr["年度"].ToString(), dr["類別"].ToString(), dr["課程分類"].ToString(), "2", dr["課程種類"].ToString(), dr["課程名稱"].ToString(), dr["上課日期"].ToString(),
                                            dr["業務員編號"].ToString(), "1", dr["上課時數"].ToString()
                                            );
                                    if (valid_error != "")
                                        sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：{1}</span><br>", dt.Rows.IndexOf(dr) + 1, valid_error);
                                    else
                                    {
                                        ds = new DataSet();
                                        RServiceProvider rsp = new RServiceProvider();
                                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                                        //元件資料轉成Hashtable
                                        DataRow dr_course = DataECourse.valid_EC2(dr["年度"].ToString(), dr["類別"].ToString(), dr["課程分類"].ToString(), "2", dr["課程種類"].ToString(), dr["課程名稱"].ToString(), dr["上課日期"].ToString());
                                        if (dr_course != null)
                                        {
                                            if (DataEAttend.valid_ED(dr_course["EC_TransNo"].ToString(), dr["業務員編號"].ToString()))
                                            {
                                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(),
                                                dataToHashtable_import("A", dr["年度"].ToString(), dr["類別"].ToString(), dr["課程分類"].ToString(), "2", dr["課程種類"].ToString(), dr["課程名稱"].ToString(), dr["上課日期"].ToString(),
                                                dr["業務員編號"].ToString(), "1", dr["上課時數"].ToString()))
                                                );

                                                rsp = DataEAttend.Append(sysValue.emNo, sysValue.ProgId, ds);
                                            }
                                            else
                                            {
                                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(),
                                                dataToHashtable_import("M", dr["年度"].ToString(), dr["類別"].ToString(), dr["課程分類"].ToString(), "2", dr["課程種類"].ToString(), dr["課程名稱"].ToString(), dr["上課日期"].ToString(),
                                                dr["業務員編號"].ToString(), "1", dr["上課時數"].ToString()))
                                                );

                                                rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                                            }
                                        }
                                        if (rsp.Result) //執行成功
                                        {
                                            sb_import.AppendFormat("<span style=\"color:#009933;\">第{0}筆：匯入成功！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                        }
                                        else
                                            sb_import.AppendFormat("<span style=\"color:#ff0000;\">第{0}筆：匯入失敗！</span><br>", dt.Rows.IndexOf(dr) + 1);
                                    }

                                }
                                GridView1.DataBind();
                                lbl_Msg0.Text = sb_import.ToString();
                            }
                            else
                                MainControls.showMsg(this, "檔案匯入失敗！" + rsp_import.ReturnMessage);
                        }
                        catch (Exception es)
                        {
                            MainControls.showMsg(this, "檔案匯入失敗！");
                        }

                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("ED_UidNo", hif_ED_UidNo.Value);
            }
            DataRow dr_course = DataECourse.DataReader_EC_TransNo(selCourse_EC_TransNo.SelectedValue);
            if (dr_course != null)
            {
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_ED_AccID.SelectedValue);
            if (dr_person != null)
            {
                hsData.Add("ED_AccID", dr_person["AccID"].ToString());
                hsData.Add("ED_ID", dr_person["PS_ID"].ToString());
                hsData.Add("ED_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("ED_Type", ddl_ED_Type.SelectedValue);
            hsData.Add("ED_AttHours", txt_ED_AttHours.Text.Trim());
            hsData.Add("ED_SignDate", txt_ED_SignDate.Text.Trim());
            hsData.Add("ED_SigninTime", TimeRangeAll_ED_SignTime.Time_start);
            hsData.Add("ED_SignoffTime", TimeRangeAll_ED_SignTime.Time_end);
            hsData.Add("ED_Category", "2");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_import(string ModiState, string EC_Year, string EC_TrainType, string EC_Type, string EC_Mode, string EC_Kind, string EC_CName, string EC_SDate, string ED_AccID, string ED_Type, string ED_AttHours)
        {
            Hashtable hsData = new Hashtable();
            if (ModiState == "A")
            {
                hsData.Add("ED_TransNo", System.Guid.NewGuid().ToString("N"));
                hsData.Add("ED_UidNo", Session["UidNo"].ToString());
            }

            DataRow dr_course = DataECourse.valid_EC2(EC_Year, EC_TrainType, EC_Type, EC_Mode, EC_Kind, EC_CName, EC_SDate);
            if (dr_course != null)
            {
                if (ModiState == "M")
                {
                    DataRow dr_ED = DataEAttend.DataReader_EC_TransNo_ED_AccID(dr_course["EC_TransNo"].ToString(), ED_AccID);
                    if (dr_ED != null)
                    {
                        hsData.Add("ED_TransNo", dr_ED["ED_TransNo"].ToString());
                        hsData.Add("ED_UidNo", dr_ED["ED_UidNo"].ToString());
                    }
                }
                hsData.Add("EC_TransNo", dr_course["EC_TransNo"].ToString());
                hsData.Add("EC_UidNo", dr_course["EC_UidNo"].ToString());
                hsData.Add("ED_SigninTime", dr_course["EC_SigninTime"].ToString());
                hsData.Add("ED_SignoffTime", dr_course["EC_SignoffTime"].ToString());
            }
            DataRow dr_person = DataPerson.DataReader_AccID(ED_AccID);
            if (dr_person != null)
            {
                hsData.Add("ED_AccID", dr_person["AccID"].ToString());
                hsData.Add("ED_ID", dr_person["PS_ID"].ToString());
                hsData.Add("ED_Name", dr_person["PS_NAME"].ToString());
            }
            hsData.Add("ED_Type", ED_Type);
            hsData.Add("ED_SignDate", EC_SDate);
            //hsData.Add("ED_SigninTime", ED_SigninTime);
            //hsData.Add("ED_SignoffTime", ED_SignoffTime);
            hsData.Add("ED_AttHours", ED_AttHours);
            hsData.Add("ED_Category", "2");
            hsData.Add("ED_IsImport", "True");
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("ED_TransNo", TransNo);
            hsData.Add("ED_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_EP_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EP_TransNo", TransNo);
            hsData.Add("EP_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_ED_UidNo.Value = string.Empty;
            selCourse_EC_TransNo.Clear();
            selPerson_AccID_ED_AccID.Clear();
            txt_ED_AttHours.Text = string.Empty;
            txt_ED_SignDate.Text = string.Empty;
            TimeRangeAll_ED_SignTime.Init();
            txt_cZON_TYPE.Text = string.Empty;
            txt_cZON_NAME.Text = string.Empty;
            txt_DM_NAME.Text = string.Empty;
            txt_PS_Title.Text = string.Empty;
            hif_EC_TransNo.Value = string.Empty;
            hif_ED_AccID.Value = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
                case 2:
                    lnkBrowse.Visible = true;
                    lbl_Msg0.Text = string.Empty;
                    maintainButtonEnabled("Import");
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_ED_Type);
                    break;
                case "Import": //
                    btnImport.Visible = true & sysValue.Authority.Append;
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_ED_Type);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[6].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[7].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[6].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[6].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[6].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttend.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[7].Text, GridView1.Rows[i].Cells[8].Text)));

                    //RServiceProvider rsp = DataEAttend.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEAttend.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        DataRow dr_EP = DataEPayment.DataReader_ED_TransNo(GridView1.Rows[i].Cells[7].Text);
                        if (dr_EP != null)
                        {
                            DataSet ds_EP = new DataSet();
                            ds_EP.Tables.Add(MainControls.UpLoadToDataTable(DataEPayment.GetSchema(), dataToHashtable_EP_dlete(dr_EP["EP_TransNo"].ToString(), dr_EP["EP_UidNo"].ToString())));
                            RServiceProvider rsp_EP = DataEPayment.Append(sysValue.emNo, sysValue.ProgId, ds_EP);
                        }
                        count_sucess += 1;
                    }
                    else
                        count_faile += 1; ;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EC_CName")) return;
            e.InputParameters.Add("EC_CName", Lib.FdVP(txtSh_EC_CName.Text).Trim());
            e.InputParameters.Add("EC_Code", Lib.FdVP(txtSh_EC_Code.Text).Trim());
            e.InputParameters.Add("PS_NAME", Lib.FdVP(txtSh_PS_NAME.Text).Trim());
            e.InputParameters.Add("ED_Type", ddlSh_ED_Type.SelectedValue);
            e.InputParameters.Add("EC_SDate_s", DateRangeSh_EC_SDate.Date_start);
            e.InputParameters.Add("EC_SDate_e", DateRangeSh_EC_SDate.Date_end);
            e.InputParameters.Add("ED_Category", "2");
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_EC_CName.Text = string.Empty;
            txtSh_EC_Code.Text = string.Empty;
            txtSh_PS_NAME.Text = string.Empty;
            ddlSh_ED_Type.SelectedIndex = 1;
            DateRangeSh_EC_SDate.Init();

            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion
    }
}