﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ChangePWD : System.Web.UI.Page
    {
        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.SetFocus(txt_emPwd);
            Panel_Form.DefaultButton = btnEdit.ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //預設更新按鈕
                btnEdit.Enabled = true & (ViewState["SysValue"] as SysValue).Authority.Update;
                //資料繫結
                dataBind(Session["emID"].ToString());
            }
        }

        #region 載入姓名
        private void dataBind(string emID)
        {
            lab_emID.Text = emID;
            lab_emName.Text = DataEmployee.baseDataReader(emID).Tables[0].Rows[0]["emName"].ToString();
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();
            if (txt_emPwd.Text.Length == 0)
                sbError.Append("●「原密碼」必須輸入!<br>");
            else
                if (Lib.GetMD5(txt_emPwd.Text) != DataEmployee.baseDataReader(Session["emID"].ToString()).Tables[0].Rows[0]["emPWD"].ToString())
                    sbError.Append("●「原密碼」輸入錯誤!<br>");

            if (txt_emNewPwd1.Text.Length == 0)
                sbError.Append("●「新密碼」必須輸入!<br>");
            else
                if (txt_emNewPwd1.Text.Trim().Length < 6 || !MainControls.ValidDataType("^([\\w- ./?%&=~!@#$^*]+)+$", txt_emNewPwd1.Text.Trim()))
                    sbError.Append("●「新密碼」輸入無效字元或字元長度小於6!<br>");

            if (txt_emNewPwd1.Text != txt_emNewPwd2.Text)
                sbError.Append("●「新密碼」確認錯誤!<br>");

            if (txt_emPwd.Text == txt_emNewPwd1.Text && txt_emPwd.Text.Length != 0)
                sbError.Append("●「新密碼」相同!<br>");

            return sbError.ToString().Trim();
        }
        #endregion

        #region 變更密碼
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            lbl_Msg.Text = "";
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            switch (e.CommandName)
            {
                case "RunChange":
                    //資料驗証
                    lbl_Msg.Text = dataValid();
                    if (lbl_Msg.Text.Trim().Length > 0)
                        return;

                    RServiceProvider rsp = DataEmployee.changePwd(Session["emID"].ToString(), Lib.GetMD5(txt_emPwd.Text.Trim()), Lib.GetMD5(txt_emNewPwd1.Text.Trim()));
                    if (rsp.Result)
                    {
                        lbl_Msg.Text = "密碼變更成功，請使用新密碼重新登入!";

                        Session["emUser"] = "";
                        Session["emID"] = "";
                        Session["emNo"] = "";
                        Session["emOS"] = "0";
                        Session["emDefOM"] = "";
                        Session["emLimit"] = "";
                        Session["userMenu"] = "";

                    }
                    else
                        lbl_Msg.Text = rsp.ReturnMessage;

                    break;
            }
        }
        #endregion
    }
}