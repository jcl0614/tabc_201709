﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="personData.aspx.cs" Inherits="tabc_201709.Manager.personData" %>
<%@ Register src="ascx/DateRange.ascx" tagname="DateRange" tagprefix="uc1" %>
<%@ Register Src="~/Manager/ascx/UpdateProgress.ascx" TagPrefix="uc1" TagName="UpdateProgress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:LinkButton ID="lnkBrowse" runat="server" CommandName="0" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 基本資料</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="1" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 個人佣金</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="2" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 公積金</asp:LinkButton>
            &nbsp;&nbsp;
            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="3" CssClass="MainHeadTdLink" OnCommand="lnk_Command"><img src="<%=ResolveUrl("images/view.png") %>" border="0" style="vertical-align:bottom" /> 出席率</asp:LinkButton>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:AsyncPostBackTrigger ControlID="LinkButton1" />
            <asp:AsyncPostBackTrigger ControlID="LinkButton2" />
            <asp:AsyncPostBackTrigger ControlID="LinkButton3" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            
            <uc1:UpdateProgress runat="server" ID="UpdateProgress" />
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <table border="0" cellpadding="2" cellspacing="1" width="100%">
                        <tr>
                            <td align="center" colspan="2" style="height: 50px; background-color: #F2DE97; color: #333333; font-weight: bold; font-family: 微軟正黑體; font-size: 18px;">基本資料</td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">
                                <div style="padding: 10px; border: 1px dashed #666666; width: 100px; height: 100px; float:right;"><asp:Literal ID="Literal_photo" runat="server"></asp:Literal></div>
                                
                            </td>
                            <td class="tdData10s">
                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                <asp:Button ID="btnUplod" runat="server" CommandName="Upload" CssClass="btnSave" OnClick="btnUplod_Click" Text="上傳照片" OnClientClick="return confirm(&quot;確定要上傳更新嗎？&quot;);" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">服務公司</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_CP_ComyName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">姓名</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_NAME" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">職級</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_Title" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">業務員編號</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_AccID" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">手機號碼</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_MOIBLE" runat="server"></asp:Label>
                            </td>
                        </tr>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">電子信箱</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_EMAIL" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                                            <td align="center" colspan="2" style="height: 50px; background-color: #F2DE97; color: #333333; font-weight: bold; font-family: 微軟正黑體; font-size: 18px;">訓練資料</td>
                                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">洗錢防制通報</td>
                            <td class="tdData10s">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[人身]登錄證號</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_LifeNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[人身]換證日期</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_FirstDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[人身]應訓時數</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_ER_Hours" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[人身]已訓時數</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_ED_AttHours" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[人身]招攬資格</td>
                            <td class="tdData10s">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[產險]登錄證號</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_ProductNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[產險]換證日期</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_PS_StartDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[產險]應訓時數</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_ER_Hours1" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="MS_tdTitle" width="150">[產險]已訓時數</td>
                            <td class="tdData10s">
                                <asp:Label ID="lbl_ED_AttHours1" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">薪資年份：<asp:DropDownList ID="ddlSh_year" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSh_year_SelectedIndexChanged">
                                </asp:DropDownList>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="薪資類別">
                                            <ItemTemplate>
                                                <%# Eval("薪資類別") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="期數">
                                            <ItemTemplate>
                                                <%# Eval("期數") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="承攬所得">
                                            <ItemTemplate>
                                                <%# Eval("承攬所得") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="營運金">
                                            <ItemTemplate>
                                                <%# Eval("營運金") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="其他所得">
                                            <ItemTemplate>
                                                <%# Eval("其他所得") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="所得扣款">
                                            <ItemTemplate>
                                                <%# Eval("所得扣款") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="應稅所得">
                                            <ItemTemplate>
                                                <%# Eval("應稅所得") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="所得稅">
                                            <ItemTemplate>
                                                <%# Eval("所得稅") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="本期應付">
                                            <ItemTemplate>
                                                <%# Eval("本期應付") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="本期應收">
                                            <ItemTemplate>
                                                <%# Eval("本期應收") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="上期應收不足">
                                            <ItemTemplate>
                                                <%# Eval("上期應收不足") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="公積金">
                                            <ItemTemplate>
                                                <%# Eval("公積金") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="合計">
                                            <ItemTemplate>
                                                <%# Eval("合計") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False">
                        <Columns>
                            <asp:TemplateField HeaderText="年度">
                                <ItemTemplate>
                                    <%# Eval("年度") %>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="金額">
                                <ItemTemplate>
                                    <%# Eval("金額") %>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="累計金額">
                                <ItemTemplate>
                                    <%# Eval("累計金額") %>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:View>
                <asp:View ID="View4" runat="server">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>日期：</td>
                                        <td>
                                            <uc1:DateRange ID="DateRange1" runat="server" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnSearch" runat="server" CssClass="btnSearch" OnClick="btnSearch_Click" Text="查詢" />
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="業務員">
                                            <ItemTemplate>
                                                <%# Eval("業務員") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="150px" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="月份">
                                            <ItemTemplate>
                                                <%# Eval("月份") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="應出席次數">
                                            <ItemTemplate>
                                                <%# Eval("應出席次數") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="實際出席次數">
                                            <ItemTemplate>
                                                <%# Eval("實際出席次數") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="當月出席率">
                                            <ItemTemplate>
                                                <%# Eval("應出席次數") %>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUplod" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
