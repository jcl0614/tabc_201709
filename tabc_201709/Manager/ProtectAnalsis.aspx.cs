﻿using AppCode.Lib;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ProtectAnalsis : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }

                //資料繫結
                dataBind();

            }
        }

        #region 資料繫結
        private void dataBind()
        {
            if (Request.QueryString["id"] != null)
            {
                DataRow dr_CU = DataCustomer.DataReader_CU_TransNo(Request.QueryString["id"]);
                if (dr_CU != null)
                {
                    string json = getJson(dr_CU);
                    if (json != "")
                    {
                        if (json.IndexOf("FamilyFeeTo") != -1)
                        {
                            List<List<DataTable>> list = getAPI_GetInsItemList(string.Format("{0}一家", dr_CU["CU_CustName"].ToString()), "1", json);

                            StringBuilder sb_html = new StringBuilder();
                            sb_html.Append("<style>");
                            sb_html.Append("table ");
                            sb_html.Append("{");
                            sb_html.Append("border: 1px solid #000; ");
                            sb_html.Append("border - collapse: collapse;");
                            sb_html.Append("}");
                            sb_html.Append("tr, td ");
                            sb_html.Append("{");
                            sb_html.Append("border: 1px solid #000; ");
                            sb_html.Append("}");
                            sb_html.Append("</style>");
                            foreach (var item in list)
                            {
                                switch (list.IndexOf(item))
                                {
                                    case 0:
                                        {
                                            #region 投保項目總覽
                                            DataTable ItemList = item.Where(x => x.TableName == "ItemList").SingleOrDefault();
                                            if (ItemList != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保險公司");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "被保人");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "投保年齡");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "險種代號");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "險種名稱");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "投保年期");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保額");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保費");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "投保日期");
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in ItemList.Rows)
                                                {
                                                    if (dr["Mark"].ToString() == "")
                                                    {
                                                        if (dr["Age"].ToString() != "")
                                                        {
                                                            sb_html.Append("<tr style=\"background-color: #f5f091;\">");
                                                            sb_html.AppendFormat("<td colspan=\"9\" align=\"left\" style=\"font-weight:bold;\">{0}　{1}　{2}　{3}　{4}　{5}</td>",
                                                                string.Format("投保公司：{0}", dr["Cmpy"].ToString()),
                                                                string.Format("{0}", dr["Ain"].ToString()),
                                                                string.Format("保單號碼：{0}", dr["Age"].ToString()),
                                                                string.Format("投保日期：{0}", dr["Idate"].ToString()),
                                                                string.Format("繳別：{0}", dr["ContactYear"].ToString()),
                                                                string.Format("保費：{0}", dr["Premium"].ToString())
                                                                );
                                                            sb_html.Append("</tr>");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["Cmpy"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["Ain"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Age"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["Mark"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["ItemName"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["ContactYear"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Amount"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Premium"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["Idate"].ToString());
                                                        sb_html.Append("</tr>");
                                                    }
                                                }
                                                sb_html.Append("</table>");
                                                Literal_ItemList.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 每月保費
                                            DataTable dtPerMonthFee = item.Where(x => x.TableName == "dtPerMonthFee").SingleOrDefault();
                                            if (dtPerMonthFee != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保險公司");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保單號碼");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "被保人");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "一月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "二月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "三月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "四月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "五月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "六月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "七月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "八月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "九月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "十月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "十一月");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "十二月");
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in dtPerMonthFee.Rows)
                                                {
                                                    if (dr["ShowType"].ToString() == "1")
                                                    {
                                                        sb_html.Append("<tr style=\"background-color: #f5f091;\">");
                                                        sb_html.AppendFormat("<td colspan=\"15\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            string.Format("被保人：{0}", dr["CMPY"].ToString())
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["CMPY"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["INSRNO"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["PerFeeAin"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Jau"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Feb"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Mar"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Apr"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["May"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Jun"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Jul"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Aug"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Sep"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Oct"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Nov"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["Dec"].ToString());
                                                        sb_html.Append("</tr>");
                                                    }
                                                }
                                                sb_html.Append("</table>");
                                                Literal_dtPerMonthFee.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 每年度保費
                                            DataTable dtFee = item.Where(x => x.TableName == "dtFee").SingleOrDefault();
                                            if (dtFee != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                foreach (DataColumn dc in dtFee.Columns)
                                                {
                                                    if (dc.ColumnName == "Year")
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", "年度");
                                                    else if (dc.ColumnName == "Summary")
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", "合計");
                                                    else
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", dc.ColumnName);
                                                }
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in dtFee.Rows)
                                                {
                                                    sb_html.Append("<tr>");
                                                    foreach (DataColumn dc in dtFee.Columns)
                                                    {
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr[dc.ColumnName].ToString());
                                                    }
                                                    sb_html.Append("</tr>");
                                                }
                                                sb_html.Append("</table>");
                                                Literal_dtFee.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 生存領回
                                            DataTable dtBack = item.Where(x => x.TableName == "dtBack").SingleOrDefault();
                                            if (dtBack != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                foreach (DataColumn dc in dtBack.Columns)
                                                {
                                                    if (dc.ColumnName == "Year")
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", "年度");
                                                    else if (dc.ColumnName == "Summary")
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", "合計");
                                                    else
                                                        sb_html.AppendFormat("<td align=\"center\">{0}</td>", dc.ColumnName);
                                                }
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in dtBack.Rows)
                                                {
                                                    sb_html.Append("<tr>");
                                                    foreach (DataColumn dc in dtBack.Columns)
                                                    {
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr[dc.ColumnName].ToString());
                                                    }
                                                    sb_html.Append("</tr>");
                                                }
                                                sb_html.Append("</table>");
                                                Literal_dtBack.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 保障分析
                                            DataTable dtProtectAnalsis = item.Where(x => x.TableName == "dtProtectAnalsis").SingleOrDefault();
                                            if (dtProtectAnalsis != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                foreach (DataRow dr in dtProtectAnalsis.Rows)
                                                {
                                                    if (dr["Type"].ToString() == "M")
                                                    {
                                                        sb_html.Append("<tr style=\"background-color: #f5f091;\">");
                                                        sb_html.AppendFormat("<td colspan=\"3\" align=\"left\" style=\"\">{0}</td>",
                                                            string.Format("被保人：{0}", dr["Item1"].ToString())
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else if (dr["Type"].ToString() == "H")
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td colspan=\"3\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            dr["Item1"].ToString()
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        sb_html.Append("<tr>");
                                                        string Item = "";
                                                        if (dr["Item1"].ToString().IndexOf("●") == 0)
                                                            Item = dr["Item1"].ToString().Replace("●", "▣");
                                                        else if (dr["Item1"].ToString().IndexOf("　") == 0)
                                                            Item = "☐" + dr["Item1"].ToString().Substring(1, dr["Item1"].ToString().Length - 1);
                                                        else
                                                            Item = "☐" + dr["Item1"].ToString();
                                                        if (Item.Length == 1)
                                                            Item = "";
                                                        sb_html.AppendFormat("<td>{0}</td>", Item);
                                                        if (dr["Item2"].ToString().IndexOf("●") == 0)
                                                            Item = dr["Item2"].ToString().Replace("●", "▣");
                                                        else if (dr["Item2"].ToString().IndexOf("　") == 0)
                                                            Item = "☐" + dr["Item2"].ToString().Substring(1, dr["Item2"].ToString().Length - 1);
                                                        else
                                                            Item = "☐" + dr["Item2"].ToString();
                                                        if (Item.Length == 1)
                                                            Item = "";
                                                        sb_html.AppendFormat("<td>{0}</td>", Item);
                                                        if (dr["Item3"].ToString().IndexOf("●") == 0)
                                                            Item = dr["Item3"].ToString().Replace("●", "▣");
                                                        else if (dr["Item3"].ToString().IndexOf("　") == 0)
                                                            Item = "☐" + dr["Item3"].ToString().Substring(1, dr["Item3"].ToString().Length - 1);
                                                        else
                                                            Item = "☐" + dr["Item3"].ToString();
                                                        if (Item.Length == 1)
                                                            Item = "";
                                                        sb_html.AppendFormat("<td>{0}</td>", Item);
                                                        sb_html.Append("</tr>");
                                                    }
                                                }
                                                sb_html.Append("</table>");
                                                Literal_dtProtectAnalsis.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 綜合投保利益
                                            DataTable dtProfit = item.Where(x => x.TableName == "dtProfit").SingleOrDefault();
                                            if (dtProfit != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                int colspan = 0;
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                foreach (DataColumn dc in dtProfit.Columns)
                                                {
                                                    if (dc.ColumnName != "GroupID" &&
                                                        dc.ColumnName != "Type" &&
                                                        dc.ColumnName != "SayPay" &&
                                                        dc.ColumnName != "Sysshow" &&
                                                        dc.ColumnName != "DefaultValue" &&
                                                        dc.ColumnName != "IsHaveDoc"
                                                        )
                                                    {
                                                        colspan += 1;
                                                        if (dc.ColumnName == "Item")
                                                            sb_html.Append("<td></td>");
                                                        else
                                                            sb_html.AppendFormat("<td align=\"center\">{0}</td>", dc.ColumnName);

                                                    }
                                                }
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in dtProfit.Rows)
                                                {
                                                    if (dr["Type"].ToString() == "H")
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td colspan=\"{1}\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            dr["Item"].ToString(), colspan
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        sb_html.Append("<tr>");
                                                        foreach (DataColumn dc in dtProfit.Columns)
                                                        {
                                                            if (dc.ColumnName != "GroupID" &&
                                                                dc.ColumnName != "Type" &&
                                                                dc.ColumnName != "SayPay" &&
                                                                dc.ColumnName != "Sysshow" &&
                                                                dc.ColumnName != "DefaultValue" &&
                                                                dc.ColumnName != "IsHaveDoc"
                                                                )
                                                            {
                                                                sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr[dc.ColumnName].ToString());
                                                            }
                                                        }
                                                        sb_html.Append("</tr>");
                                                    }
                                                }
                                                sb_html.Append("</table>");
                                                Literal_dtProfit.Text = sb_html.ToString();
                                            }
                                            #endregion
                                            #region 醫療明細
                                            DataTable tbHealthy = item.Where(x => x.TableName == "tbHealthy").SingleOrDefault();
                                            if (tbHealthy != null)
                                            {
                                                sb_html = new StringBuilder();
                                                sb_html.Append("<table>");
                                                sb_html.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保險公司");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保單號碼");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "險種代號");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "險種名稱");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "最低給付");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "最高給付");
                                                sb_html.AppendFormat("<td align=\"center\">{0}</td>", "保障年期");
                                                sb_html.Append("</tr>");
                                                foreach (DataRow dr in tbHealthy.Rows)
                                                {
                                                    if (dr["Type"].ToString() == "M")
                                                    {
                                                        sb_html.Append("<tr style=\"background-color: #f5f091;\">");
                                                        sb_html.AppendFormat("<td colspan=\"7\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            string.Format("被保人：{0}", dr["Ain"].ToString())
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else if (dr["Type"].ToString() == "H")
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td colspan=\"7\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            dr["PPLNO"].ToString()
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else if (dr["Type"].ToString() == "T")
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td colspan=\"7\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            string.Format("　{0}", dr["PPLNO"].ToString())
                                                            );
                                                        sb_html.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        sb_html.Append("<tr>");
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["CMPY"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["PPLNO"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["Mark"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["InsItemName"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["MinV"].ToString());
                                                        sb_html.AppendFormat("<td align=\"right\">{0}</td>", dr["MaxV"].ToString());
                                                        sb_html.AppendFormat("<td>{0}</td>", dr["ProtectYear"].ToString());
                                                        sb_html.Append("</tr>");
                                                    }
                                                }
                                                sb_html.Append("</table>");
                                                Literal_tbHealthy.Text = sb_html.ToString();
                                            }
                                            #endregion
                                        }
                                        break;
                                    case 1:
                                        {
                                            #region 保障明細表
                                            StringBuilder sb_html_tabs = new StringBuilder();
                                            StringBuilder sb_html_conyent = new StringBuilder();
                                            sb_html = new StringBuilder();
                                            foreach (DataTable dt in item)
                                            {
                                                sb_html_tabs.AppendFormat("<li><a href=\"#tabs_sub1-{0}\">{1}</a></li>", item.IndexOf(dt) + 1, dt.TableName);
                                                sb_html_conyent.AppendFormat("<div id=\"tabs_sub1-{0}\">", item.IndexOf(dt) + 1);
                                                sb_html_conyent.Append("<table>");
                                                sb_html_conyent.Append("<tr style=\"background-color: #f5f091;\">");
                                                sb_html_conyent.AppendFormat("<td colspan=\"4\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                    string.Format("被保人：{0}", dt.TableName)
                                                    );
                                                sb_html_conyent.Append("</tr>");
                                                sb_html_conyent.Append("<tr style=\"font-weight:bold;background-color: #edcd66;\">");
                                                sb_html_conyent.AppendFormat("<td align=\"center\">{0}</td>", "年度");
                                                sb_html_conyent.AppendFormat("<td align=\"center\">{0}</td>", "一般身故");
                                                sb_html_conyent.AppendFormat("<td align=\"center\">{0}</td>", "意外保障");
                                                sb_html_conyent.AppendFormat("<td align=\"center\">{0}</td>", "癌症保障");
                                                sb_html_conyent.Append("</tr>");
                                                foreach (DataRow dr in dt.Rows)
                                                {
                                                    sb_html_conyent.Append("<tr>");
                                                    sb_html_conyent.AppendFormat("<td>{0}</td>", dr["Year"].ToString());
                                                    sb_html_conyent.AppendFormat("<td align=\"right\">{0}</td>", dr["Life"].ToString());
                                                    sb_html_conyent.AppendFormat("<td align=\"right\">{0}</td>", dr["ACC"].ToString());
                                                    sb_html_conyent.AppendFormat("<td align=\"right\">{0}</td>", dr["CR"].ToString());
                                                    sb_html_conyent.Append("</tr>");
                                                }
                                                sb_html_conyent.Append("</table>");
                                                sb_html_conyent.Append("<br /><br />");
                                                sb_html_conyent.Append("</div>");
                                            }
                                            sb_html.Append("<div id=\"tabs_sub1\">");
                                            sb_html.AppendFormat("<ul>{0}</ul>", sb_html_tabs);
                                            sb_html.Append(sb_html_conyent);
                                            sb_html.Append("</div>");
                                            Literal_FamilyDetail.Text = sb_html.ToString();
                                            #endregion
                                        }
                                        break;
                                    case 2:
                                        {
                                            #region 綜合保障明細
                                            StringBuilder sb_html_tabs = new StringBuilder();
                                            StringBuilder sb_html_conyent = new StringBuilder();
                                            sb_html = new StringBuilder();
                                            foreach (DataTable dt in item)
                                            {
                                                sb_html_tabs.AppendFormat("<li><a href=\"#tabs_sub2-{0}\">{1}</a></li>", item.IndexOf(dt) + 1, dt.TableName);
                                                sb_html_conyent.AppendFormat("<div id=\"tabs_sub2-{0}\">", item.IndexOf(dt) + 1);
                                                sb_html_conyent.Append("<table>");
                                                int colspan = 0;
                                                foreach (DataColumn dc in dt.Columns)
                                                {
                                                    if (dc.ColumnName != "Type" &&
                                                        dc.ColumnName != "GroupNo"
                                                        )
                                                    {
                                                        colspan += 1;
                                                    }
                                                }
                                                sb_html_conyent.Append("<tr style=\"background-color: #f5f091;\">");
                                                sb_html_conyent.AppendFormat("<td colspan=\"{1}\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                    string.Format("被保人：{0}", dt.TableName), colspan
                                                    );
                                                sb_html_conyent.Append("</tr>");
                                                foreach (DataRow dr in dt.Rows)
                                                {
                                                    if (dr["Type"].ToString() == "H")
                                                    {
                                                        sb_html_conyent.Append("<tr>");
                                                        sb_html_conyent.AppendFormat("<td colspan=\"{1}\" align=\"left\" style=\"font-weight:bold;\">{0}</td>",
                                                            dr["ItemShow"].ToString(), colspan
                                                            );
                                                        sb_html_conyent.Append("</tr>");
                                                    }
                                                    else
                                                    {
                                                        sb_html_conyent.Append("<tr>");
                                                        foreach (DataColumn dc in dt.Columns)
                                                        {
                                                            if (dc.ColumnName != "Type" && dc.ColumnName != "GroupNo")
                                                            {
                                                                if (dc.ColumnName == "ItemShow")
                                                                    sb_html_conyent.AppendFormat("<td style=\"font-weight:bold;background-color: #edcd66;\">{0}</td>", dr[dc.ColumnName].ToString());
                                                                else
                                                                    sb_html_conyent.AppendFormat("<td>{0}</td>", dr[dc.ColumnName].ToString());
                                                            }
                                                        }
                                                        sb_html_conyent.Append("</tr>");
                                                    }
                                                }
                                                sb_html_conyent.Append("</table>");
                                                sb_html_conyent.Append("<br /><br />");
                                                sb_html_conyent.Append("</div>");
                                            }
                                            sb_html.Append("<div id=\"tabs_sub2\">");
                                            sb_html.AppendFormat("<ul>{0}</ul>", sb_html_tabs);
                                            sb_html.Append(sb_html_conyent);
                                            sb_html.Append("</div>");
                                            Literal_FamilyAll.Text = sb_html.ToString();
                                            #endregion
                                        }
                                        break;
                                }

                            }

                            //HtmlToPdf converter = new HtmlToPdf();
                            //converter.Options.MarginTop = 10;
                            //converter.Options.PdfPageSize = PdfPageSize.A4;
                            //PdfDocument doc = converter.ConvertHtmlString(sb_html.ToString());
                            //doc.Save(Response, false, "Sample.pdf");

                        }
                        else
                            MainControls.showMsg(this, json);
                    }
                }
            }
            else
                MainControls.showMsg(this, "客戶不存在！");
        }
        #endregion

        private dynamic getAPI_GetInsItemList(string ComunityName, string FamilyFeeBy, string Json)
        {
            List<DataTable> list_Policy = new List<DataTable>();
            List<DataTable> list_PolicyDetail = new List<DataTable>();
            List<DataTable> list_PolicyDetail_other = new List<DataTable>();

            List<List<DataTable>> list = new List<List<DataTable>>();
            try
            {
                string ApiURL = string.Format("{0}/{1}", System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_URL"], "ShowInsProfit");
                string Channel = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_Channel"];
                string IP = System.Web.Configuration.WebConfigurationManager.AppSettings["gouptech_InsAPI_IP"];
                string no = string.Format("{0}|{1}|{2}|{3}|{4}|{5}", ComunityName, FamilyFeeBy, DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP, Json);
                no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
                string err = "";
                string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
                if (err.Length == 0 && ret.IndexOf("[Message]") == -1)
                {
                    JArray dynObj = null;
                    dynObj = JsonConvert.DeserializeObject(ret) as JArray;
                    foreach (var item0 in dynObj)
                    {
                        switch (dynObj.IndexOf(item0))
                        {
                            case 0:
                                {
                                    foreach (var item1 in item0.ToList())
                                    {
                                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(item1.ToList()[0].ToString());
                                        dt.TableName = ((JProperty)item1).Name;
                                        list_Policy.Add(dt);
                                    }
                                }
                                break;
                            case 1:
                                {
                                    foreach (var item1 in item0.ToList())
                                    {
                                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(item1.ToList()[0].ToString());
                                        dt.TableName = ((JProperty)item1).Name;
                                        list_PolicyDetail.Add(dt);
                                    }
                                }
                                break;
                            case 2:
                                {
                                    foreach (var item1 in item0.ToList())
                                    {
                                        DataTable dt = JsonConvert.DeserializeObject<DataTable>(item1.ToList()[0].ToString());
                                        dt.TableName = ((JProperty)item1).Name;
                                        list_PolicyDetail_other.Add(dt);
                                    }
                                }
                                break;
                        }

                    }
                    list.Add(list_Policy);
                    list.Add(list_PolicyDetail);
                    list.Add(list_PolicyDetail_other);
                }
                else
                {
                    MainControls.showMsg(this, "查無符合資料！");
                    if(err.Length > 0)
                        Literal_error.Text = err;
                    else if (ret.Length > 0)
                        Literal_error.Text = ret;
                }
            }
            catch (Exception ex)
            {
                Literal_error.Text = ex.Message + "<br>" + ex.StackTrace;
            }
            return list;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }

        private string getJson(DataRow dr_CU)
        {
            if (dr_CU != null)
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("{");
                sb.AppendFormat("\"FamilyFeeTo\": 1,");
                sb.AppendFormat("\"ComunityName\":\"{0}一家\",", dr_CU["CU_CustName"].ToString());
                sb.AppendFormat("\"PPLMan\": [");
                //客戶本人資訊
                sb.Append("{");
                sb.AppendFormat("\"PKID\":\"{0}\",", new Guid(dr_CU["CU_TransNo"].ToString()).ToString());
                sb.AppendFormat("\"Relation\": \"本人\",");
                sb.AppendFormat("\"Name\":\"{0}\",", dr_CU["CU_CustName"].ToString());
                if (dr_CU["CU_Birth"].ToString().Trim().Length > 0)
                {
                    if (!Convert.ToDateTime(dr_CU["CU_Birth"].ToString()).ToString("yyyy/MM/dd").Equals("1900/01/01"))
                    {
                        int CU_Age = DateTime.Now.Year - DateTime.Parse(dr_CU["CU_Birth"].ToString()).Year;
                        if (DateTime.Now.Month - DateTime.Parse(dr_CU["CU_Birth"].ToString()).Month >= 6)
                            CU_Age += 1;
                        int CU_AgeFull = DateTime.Now.Year - DateTime.Parse(dr_CU["CU_Birth"].ToString()).Year;
                        sb.AppendFormat("\"BirthDay\":\"{0}\",", string.Format("{0:s}", DateTime.Parse(dr_CU["CU_Birth"].ToString())));
                        sb.AppendFormat("\"RAge\": {0},", CU_AgeFull.ToString());
                        sb.AppendFormat("\"Age\": {0},", CU_Age.ToString());
                    }
                }
                else
                {
                    return "請輸入[客戶生日]已進行分析保障！";
                }
                sb.AppendFormat("\"Sex\": {0},", dr_CU["CU_Sex"].ToString() == "男" ? "0" : "1");
                sb.AppendFormat("\"Level\": {0},", dr_CU["CU_JobPA"].ToString());
                sb.AppendFormat("\"CareerCode\": {0},", dr_CU["CU_JobCode"].ToString() == "" ? "0" : dr_CU["CU_JobCode"].ToString());
                sb.AppendFormat("\"AddCost\": {0},", dr_CU["CU_JobLife"].ToString() == "" ? "0" : dr_CU["CU_JobLife"].ToString());
                sb.AppendFormat("\"CurrentAge\": {0}", dr_CU["CU_Age"].ToString());
                sb.Append("},");

                DataTable dt_PO = DataCustPolicy.DataReader_CU_TransNo(dr_CU["CU_TransNo"].ToString()).Tables[0];
                //主被保人資訊
                List<string> FA_CU = new List<string>();
                foreach (DataRow dr_PO in dt_PO.Rows)
                {
                    if (dr_PO["PO_PolState"].ToString() == "1-有效" ||
                        dr_PO["PO_PolState"].ToString() == "6-減額繳清" ||
                        dr_PO["PO_PolState"].ToString() == "7-展期")
                    {
                        if (!FA_CU.Contains(dr_PO["PO_OwnCUTransNo"].ToString()))
                        {
                            DataRow dr_PO_CU = DataCustomer.DataReader_CU_TransNo(dr_PO["PO_OwnCUTransNo"].ToString());
                            if (dr_PO_CU != null)
                            {
                                FA_CU.Add(dr_PO["PO_OwnCUTransNo"].ToString());
                                string Relation = "";
                                DataRow dr_FA = DataFamilyRela.DataReader_CU_TransNo_FA_CUTransNo(dr_CU["CU_TransNo"].ToString(), dr_PO_CU["CU_TransNo"].ToString());
                                if (dr_FA != null)
                                    Relation = dr_FA["FA_RelaName"].ToString();
                                sb.Append("{");
                                sb.AppendFormat("\"PKID\":\"{0}\",", new Guid(dr_PO_CU["CU_TransNo"].ToString()).ToString());
                                sb.AppendFormat("\"Relation\": \"{0}\",", Relation);
                                sb.AppendFormat("\"Name\":\"{0}\",", dr_PO_CU["CU_CustName"].ToString());
                                if (dr_PO_CU["CU_Birth"].ToString().Trim().Length > 0)
                                {
                                    if (!Convert.ToDateTime(dr_PO_CU["CU_Birth"].ToString()).ToString("yyyy/MM/dd").Equals("1900/01/01"))
                                    {
                                        int CU_Age = DateTime.Now.Year - DateTime.Parse(dr_PO_CU["CU_Birth"].ToString()).Year;
                                        if (DateTime.Now.Month - DateTime.Parse(dr_PO_CU["CU_Birth"].ToString()).Month >= 6)
                                            CU_Age += 1;
                                        int CU_AgeFull = DateTime.Now.Year - DateTime.Parse(dr_PO_CU["CU_Birth"].ToString()).Year;
                                        sb.AppendFormat("\"BirthDay\":\"{0}\",", string.Format("{0:s}", DateTime.Parse(dr_PO_CU["CU_Birth"].ToString())));
                                        sb.AppendFormat("\"RAge\": {0},", CU_AgeFull.ToString());
                                        sb.AppendFormat("\"Age\": {0},", CU_Age.ToString());
                                    }
                                }
                                else
                                {
                                    return "請輸入[客戶生日]已進行分析保障！";
                                }
                                sb.AppendFormat("\"Sex\": {0},", dr_PO_CU["CU_Sex"].ToString() == "男" ? "0" : "1");
                                sb.AppendFormat("\"Level\": {0},", dr_PO_CU["CU_JobPA"].ToString());
                                sb.AppendFormat("\"CareerCode\": {0},", dr_PO_CU["CU_JobCode"].ToString() == "" ? "0" : dr_PO_CU["CU_JobCode"].ToString());
                                sb.AppendFormat("\"AddCost\": {0},", dr_PO_CU["CU_JobLife"].ToString() == "" ? "0" : dr_PO_CU["CU_JobLife"].ToString());
                                sb.AppendFormat("\"CurrentAge\": {0}", dr_PO_CU["CU_Age"].ToString());
                                sb.Append("},");
                            }
                        }
                    }
                }
                //客戶家屬資訊
                //DataTable dt_FA = DataFamilyRela.DataReader_CU_TransNo(dr_CU["CU_TransNo"].ToString()).Tables[0];
                //foreach (DataRow dr_FA in dt_FA.Rows)
                //{
                //    //if (dr_FA["FA_RelaName"].ToString() != "本人")
                //    //{
                //        DataRow dr_FA_CU = DataCustomer.DataReader_CU_TransNo(dr_FA["FA_CUTransNo"].ToString());
                //        if (dr_FA_CU != null)
                //        {
                //            sb.Append("{");
                //            sb.AppendFormat("\"PKID\":\"{0}\",", new Guid(dr_FA_CU["CU_TransNo"].ToString()).ToString());
                //            sb.AppendFormat("\"Relation\": \"{0}\",", dr_FA["FA_RelaName"].ToString());
                //            sb.AppendFormat("\"Name\":\"{0}\",", dr_FA_CU["CU_CustName"].ToString());
                //            if (dr_FA_CU["CU_Birth"].ToString().Trim().Length > 0)
                //            {
                //                if (!Convert.ToDateTime(dr_FA_CU["CU_Birth"].ToString()).ToString("yyyy/MM/dd").Equals("1900/01/01"))
                //                {
                //                    int CU_Age = DateTime.Now.Year - DateTime.Parse(dr_FA_CU["CU_Birth"].ToString()).Year;
                //                    if (DateTime.Now.Month - DateTime.Parse(dr_FA_CU["CU_Birth"].ToString()).Month >= 6)
                //                        CU_Age += 1;
                //                    int CU_AgeFull = DateTime.Now.Year - DateTime.Parse(dr_FA_CU["CU_Birth"].ToString()).Year;
                //                    sb.AppendFormat("\"BirthDay\":\"{0}\",", string.Format("{0:s}", DateTime.Parse(dr_FA_CU["CU_Birth"].ToString())));
                //                    sb.AppendFormat("\"RAge\": {0},", CU_AgeFull.ToString());
                //                    sb.AppendFormat("\"Age\": {0},", CU_Age.ToString());
                //                }
                //            }
                //            else
                //            {
                //                return "請輸入[客戶生日]已進行分析保障！";
                //            }
                //            sb.AppendFormat("\"Sex\": {0},", dr_FA_CU["CU_Sex"].ToString() == "男" ? "0" : "1");
                //            sb.AppendFormat("\"Level\": {0},", dr_FA_CU["CU_JobPA"].ToString());
                //            sb.AppendFormat("\"CareerCode\": {0},", dr_FA_CU["CU_JobCode"].ToString() == "" ? "0" : dr_FA_CU["CU_JobCode"].ToString());
                //            sb.AppendFormat("\"AddCost\": {0},", dr_FA_CU["CU_JobLife"].ToString() == "" ? "0" : dr_FA_CU["CU_JobLife"].ToString());
                //            sb.AppendFormat("\"CurrentAge\": {0}", dr_FA_CU["CU_Age"].ToString());
                //            sb.Append("},");
                //        }
                //    //}
                //}

                sb.Append("],");
                sb.AppendFormat("\"UserBoxList\": [");
                //保單資訊
                foreach (DataRow dr_PO in dt_PO.Rows)
                {
                    if (dr_PO["PO_PolState"].ToString() == "1-有效" || 
                        dr_PO["PO_PolState"].ToString() == "6-減額繳清" || 
                        dr_PO["PO_PolState"].ToString() == "7-展期")
                    {
                        //主被保人
                        DataRow dr_PO_CU = DataCustomer.DataReader_CU_TransNo(dr_PO["PO_OwnCUTransNo"].ToString());
                        sb.Append("{");
                        string CstmLifeID = "";
                        if (dr_PO_CU != null)
                            CstmLifeID = new Guid(dr_PO_CU["CU_TransNo"].ToString()).ToString();
                        sb.AppendFormat("\"CstmLifeID\":\"{0}\",", CstmLifeID);
                        //sb.AppendFormat("\"CstmLifeID\":\"{0}\",", dr_PO["PO_OwnerID"].ToString().Replace("*", "") + dt_PO.Rows.IndexOf(dr_PO).ToString("00000"));
                        sb.AppendFormat("\"PolBox\": [");

                        sb.Append("{");
                        //受益人
                        StringBuilder BfyNames = new StringBuilder();
                        DataTable dt_BE = DataCustBenf.DataReader_PO_TransNo(dr_PO["PO_TransNo"].ToString()).Tables[0];
                        foreach (DataRow dr_BE in dt_BE.Rows)
                        {
                            BfyNames.AppendFormat("{0};", dr_BE["BE_Name"].ToString());
                        }
                        sb.AppendFormat("\"BfyNames\":\"{0}\",", BfyNames.ToString().TrimEnd(';'));
                        sb.AppendFormat("\"Policy\": ");
                        sb.Append("{");
                        sb.AppendFormat("\"Guid\":\"{0}\",", new Guid(dr_PO["PO_TransNo"].ToString()).ToString());
                        string InsType = "0";
                        string INRCMPY = "";
                        DataRow dr_TCM = DataTABC_COMPANY_MAP.DataReader_TCM_ComCode(dr_PO["PO_ComSName"].ToString());
                        if (dr_TCM != null)
                        {
                            InsType = dr_TCM["ComKind_GT"].ToString();
                            INRCMPY = dr_TCM["ComSName_GT"].ToString();
                        }
                        sb.AppendFormat("\"InsType\": {0},", InsType);
                        sb.AppendFormat("\"PolicyID\":\"{0}\",", dr_PO["PO_PolNo"].ToString());
                        sb.AppendFormat("\"CSTMGuid\":\"{0}\",", new Guid(dr_PO["PO_OwnCUTransNo"].ToString()).ToString());
                        sb.AppendFormat("\"SNO\": null,");
                        sb.AppendFormat("\"INRCMPY\":\"{0}\",", INRCMPY);
                        //主被保人
                        string Insurer = "";
                        if (dr_PO_CU != null)
                            Insurer = dr_PO_CU["CU_CustName"].ToString();
                        sb.AppendFormat("\"Insurer\":\"{0}\",", Insurer);
                        sb.AppendFormat("\"ApplicantGuid\": \"{0}\",", new Guid(dr_CU["CU_TransNo"].ToString()).ToString());
                        sb.AppendFormat("\"Applicant\":\"{0}\",", dr_PO["PO_OwnerName"].ToString());
                        sb.AppendFormat("\"PolicyDate\": \"{0}\",", dr_PO["PO_AccureDate"].ToString() == "" ? "" : string.Format("{0:s}", DateTime.Parse(dr_PO["PO_AccureDate"].ToString())));
                        string Status = "";
                        switch(dr_PO["PO_PolState"].ToString())
                        {
                            case "1-有效":
                                Status = "有效保單";
                                break;
                            case "6-減額繳清":
                                Status = "繳清";
                                break;
                            case "7-展期":
                                Status = "展期";
                                break;
                        }
                        sb.AppendFormat("\"Status\":\"{0}\",", Status);
                        string Payment = "";
                        DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("028", dr_PO["PO_Mode"].ToString());
                        if (dr_Phrase != null)
                        {
                            Payment = dr_Phrase["TypeName"].ToString();
                        }
                        sb.AppendFormat("\"Payment\":\"{0}\",", Payment);
                        sb.AppendFormat("\"Age\": {0},", dr_PO["PO_InsureAge"].ToString());
                        sb.AppendFormat("\"Charge\": null,");
                        sb.AppendFormat("\"Amount\": null,");
                        sb.AppendFormat("\"Premiums\": {0},", dr_PO["PO_ModePrem"].ToString());
                        sb.AppendFormat("\"Discount\": null,");
                        sb.AppendFormat("\"Address\": null,");
                        sb.AppendFormat("\"CalYear\": null,");
                        sb.AppendFormat("\"Benefit\": null,");
                        sb.AppendFormat("\"CDate\": null,");
                        sb.AppendFormat("\"SelfDefined\": null,");
                        sb.AppendFormat("\"UserDate1\": null,");
                        sb.AppendFormat("\"UserDate2\": null,");
                        sb.AppendFormat("\"AdjedAmount\": null,");
                        sb.AppendFormat("\"ETIDate\": null,");
                        sb.AppendFormat("\"ETIMoney\": null,");
                        sb.AppendFormat("\"PolicyID2\": null,");
                        sb.AppendFormat("\"InsrTarget\": null,");
                        sb.AppendFormat("\"CarGuid\": null,");
                        sb.AppendFormat("\"mortgage\": null,");
                        sb.AppendFormat("\"Remark\": null,");
                        sb.AppendFormat("\"EditID\": null,");
                        sb.AppendFormat("\"SetID\": null,");
                        sb.AppendFormat("\"ModifyDate\": null,");
                        sb.AppendFormat("\"PolicyFeeRecords\": [],");
                        sb.AppendFormat("\"PolicyServices\": [],");
                        sb.AppendFormat("\"PolicyItems\": [],");
                        sb.AppendFormat("\"Beneficiaries\": [],");
                        sb.AppendFormat("\"Selected\": false");
                        sb.Append("},");

                        sb.AppendFormat("\"PolicyItem\": [");
                        //保單商品資訊
                        int SNO = 1;
                        DataTable dt_PD = DataCustPdt.DataReader_PO_TransNo(dr_PO["PO_TransNo"].ToString()).Tables[0];
                        foreach (DataRow dr_PD in dt_PD.Rows)
                        {
                            sb.Append("{");
                            sb.AppendFormat("\"Guid\":\"{0}\",", new Guid(dr_PD["PD_TransNo"].ToString()).ToString());
                            sb.AppendFormat("\"PolicyGuid\":\"{0}\",", new Guid(dr_PO["PO_TransNo"].ToString()).ToString());
                            string InrCmpy = "";
                            if (dr_TCM != null)
                            {
                                InrCmpy = dr_TCM["ComSName_GT"].ToString();
                            }
                            sb.AppendFormat("\"InrCmpy\":\"{0}\",", InrCmpy);
                            sb.AppendFormat("\"SNO\": {0},", SNO);
                            //險種代號
                            string Mark = "";
                            if (dr_PD["PD_PdtCode"].ToString() != "")
                            {
                                DataRow dr_PdtCode = DataPlanCodeMap.DataReader_PlanMark(dr_PD["PD_PdtCode"].ToString());
                                if (dr_PdtCode != null)
                                {
                                    Mark = dr_PdtCode["GoupMark"].ToString();
                                }
                            }
                            sb.AppendFormat("\"Mark\":\"{0}\",", Mark);
                            sb.AppendFormat("\"ItemName\":\"{0}\",", dr_PD["PD_PdtName"].ToString());
                            sb.AppendFormat("\"YName\":\"{0}年期\",", dr_PD["PD_PdtYear"].ToString());
                            sb.AppendFormat("\"Target\": \"{0}\",", Insurer);
                            string Amount = "0";
                            switch (dr_PD["PD_Unit"].ToString())
                            {
                                case "計劃":
                                    Amount = string.Format("{0}{1}", dr_PD["PD_Unit"].ToString(), dr_PD["PD_AmtInput"].ToString());
                                    break;
                                case "單位":
                                    Amount = string.Format("{0}{1}", dr_PD["PD_AmtInput"].ToString(), dr_PD["PD_Unit"].ToString());
                                    break;
                                case "萬元":
                                    if (dr_PD["PD_AmtInput"].ToString() != "")
                                        Amount = (int.Parse(dr_PD["PD_AmtInput"].ToString()) * 10000).ToString();
                                    break;
                                case "仟元":
                                    if (dr_PD["PD_AmtInput"].ToString() != "")
                                        Amount = (int.Parse(dr_PD["PD_AmtInput"].ToString()) * 1000).ToString();
                                    break;
                                case "百元":
                                    if (dr_PD["PD_AmtInput"].ToString() != "")
                                        Amount = (int.Parse(dr_PD["PD_AmtInput"].ToString()) * 100).ToString();
                                    break;
                                case "元":
                                    Amount = dr_PD["PD_AmtInput"].ToString();
                                    break;
                                default:
                                    Amount = dr_PD["PD_AmtInput"].ToString();
                                    break;
                            }
                            sb.AppendFormat("\"Amount\":\"{0}\",", Amount);
                            sb.AppendFormat("\"Premium\": \"{0}\",", dr_PD["PD_Premium"].ToString());
                            sb.AppendFormat("\"SDate\": null,");
                            sb.AppendFormat("\"SPEC\": null,");
                            sb.AppendFormat("\"VULPARM\": null,");
                            sb.AppendFormat("\"Status\":\"{0}\",", Status);
                            sb.AppendFormat("\"AdjedAmount\": null,");
                            sb.AppendFormat("\"AdjDate\": null,");
                            sb.AppendFormat("\"ETIDate\": null,");
                            sb.AppendFormat("\"EtiMoney\": null,");
                            sb.AppendFormat("\"ModifyDate\": null,");
                            sb.AppendFormat("\"Policy\": null");
                            sb.Append("},");
                            SNO += 1;
                        }
                        sb.Append("]");

                        sb.Append("},");


                        sb.Append("]");
                        sb.Append("},");
                    }
                }
                sb.Append("]");
                sb.Append("}");

                return sb.ToString();
            }
            else
                return "客戶不存在！";


        }

        private string showTable(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("<span style=\"color:#0000ff\">{0}</span><br />", dt.TableName);
            sb.Append("<table>");
            sb.Append("<tr>");
            foreach (DataColumn dc in dt.Columns)
            {
                sb.AppendFormat("<td style=\"font-weight:bold\">{0}</td>", dc.ColumnName);
            }
            sb.Append("</tr>");
            foreach (DataRow dr in dt.Rows)
            {
                sb.Append("<tr>");
                foreach (DataColumn dc in dt.Columns)
                {
                    sb.AppendFormat("<td>{0}</td>", dr[dc.ColumnName].ToString());
                }
            }
            sb.Append("</table>");
            sb.Append("<br />");

            return sb.ToString();
        }
    }
}