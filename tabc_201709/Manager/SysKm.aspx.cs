﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class SysKm : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_name);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_name);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_name);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 800);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataSysKm.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataMenuLayer.menuLayerReaderDropDownList(ddl_mulayer, true, "");
            ddl_mulayer.Items.Add(new ListItem("其他", "99"));
            DataMenuLayer.menuLayerReaderDropDownList(ddlSh_mulayer, true, "");
            ddlSh_mulayer.Items.Add(new ListItem("其他", "99"));
            DataMenuLayer.menuLayerReaderDropDownList(ddlSort_Class, true, "");
            ddlSort_Class.Items.Add(new ListItem("其他", "99"));
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_mulayer.SelectedValue == "")
                sbError.Append("●「類別」必須選取!<br>");
            if (txt_sort.Text.Trim() == "")
                sbError.Append("●「排列順序」必須輸入!<br>");
            else
                if (MainControls.ValidDataType("^\\d+$", (txt_sort.Text.Trim())))
                {
                    if (int.Parse(txt_sort.Text.Trim()) < 1 || int.Parse(txt_sort.Text.Trim()) > 99)
                        sbError.Append("●「排列順序」格式錯誤!<br>");
                }
                else
                    sbError.Append("●「排列順序」格式錯誤!<br>");
            if (txt_name.Text.Trim() == "")
                sbError.Append("●「功能選單名稱」必須輸入!<br>");


            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "id":
                            hf_id.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "mulayer":
                            MainControls.ddlIndexSelectValue(ddl_mulayer, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "name":
                            txt_name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "description":
                            ckeditor1.Html = Server.HtmlDecode(gvr.Cells[i].Text);
                            break;
                        case "sort":
                            txt_sort.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "enable":
                            cbox_enable.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            break;
                        case "emName":
                            lbl_name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "updateDate":
                            lbl_updateDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                if (lbl_Msg.Text.Trim().Length > 0) return;
                //元件資料轉成Hashtable
                ds.Tables.Add(MainControls.UpLoadToDataTable(DataSysKm.GetSchema(), dataToHashtable()));
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        RServiceProvider rsp = DataSysKm.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hf_id.Value = rsp.ReturnData.ToString();
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        RServiceProvider rsp = DataSysKm.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        RServiceProvider rsp = DataSysKm.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable()
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("id", hf_id.Value);
            hsData.Add("mulayer", ddl_mulayer.SelectedValue);
            hsData.Add("name", Lib.FdVP(txt_name.Text.Trim(), "()"));
            hsData.Add("description", Lib.HtmlTags(ckeditor1.Html.Trim()));
            hsData.Add("sort", Lib.FdVP(txt_sort.Text.Trim()));
            hsData.Add("enable", cbox_enable.Checked);
            hsData.Add("emNo", Session["emNo"].ToString());
            hsData.Add("updateDate", MainControls.GetDate(""));
            lbl_updateDate.Text = MainControls.GetDate("");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hf_id.Value = string.Empty;
            txt_name.Text = string.Empty;
            ckeditor1.Html = string.Empty;
            ddl_mulayer.SelectedIndex = 0;
            txt_sort.Text = string.Empty;
            cbox_enable.Checked = true;
            lbl_name.Text = string.Empty;
            lbl_updateDate.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            lnkSortSet.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    lnkSortSet.Visible = (ViewState["SysValue"] as SysValue).Authority.Update;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    lnkSortSet.Visible = (ViewState["SysValue"] as SysValue).Authority.Update;
                    break;
                case 2:
                    maintainButtonEnabled("");
                    lnkBrowse.Visible = true;
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    MarkSortSetInit();
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_name);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    btnSaveSort.Enabled = true & sysValue.Authority.Update;
                    this.SetFocus(txt_name);
                    break;
            }
        }
        #endregion

        #region 資料順序設定模組

        #region 順序調整初始化
        protected void MarkSortSetInit()
        {
            ddlSort_Class.SelectedIndex = -1;
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            DataSet ds = DataSysKm.baseDataReader(ddlSort_Class.SelectedValue);
            if (ds != null)
            {
                foreach (DataRow dw in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem();
                    li.Text = dw["name"].ToString().Trim();
                    li.Value = dw["id"].ToString().Trim();
                    lboxSur.Items.Add(li);
                }
            }
        }
        protected void ddlSort_Class_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet ds = DataSysKm.baseDataReader(ddlSort_Class.SelectedValue);
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dw in ds.Tables[0].Rows)
                {
                    ListItem li = new ListItem();
                    li.Text = dw["name"].ToString().Trim();
                    li.Value = dw["id"].ToString().Trim();
                    lboxSur.Items.Add(li);
                }
            }
        }
        #endregion

        #region 模組按鈕觸發處理
        protected void btnSort_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) < 0)
                            lboxObj.Items.Add(m_item);
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            lboxObj.Items.Add(itemSur);
                            lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                            lboxSur.Items.Remove(itemSur);
                            if (lboxSur.Items.Count > siSur)
                                lboxSur.SelectedIndex = siSur;
                            else
                                lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "up"://上移按鈕
                    int intUp = lboxObj.SelectedIndex;
                    ListItem itemUp = lboxObj.SelectedItem;
                    if (intUp > 0)
                    {
                        lboxObj.Items.Remove(itemUp);
                        lboxObj.Items.Insert(intUp - 1, itemUp);
                        lboxObj.SelectedIndex = intUp - 1;
                    }
                    break;
                case "down"://下移按鈕
                    int intDown = lboxObj.SelectedIndex;
                    ListItem itemDown = lboxObj.SelectedItem;
                    if (intDown < lboxObj.Items.Count - 1)
                    {
                        lboxObj.Items.Remove(itemDown);
                        lboxObj.Items.Insert(intDown + 1, itemDown);
                        lboxObj.SelectedIndex = intDown + 1;
                    }
                    break;
                case "save"://儲存新顯示順序設定
                    if (lboxObj.Items.Count != 0)
                    {
                        RServiceProvider rsp = DataMenuLayer.SortUpdate(lboxObj);
                        if (rsp.Result)
                        {
                            MainControls.showMsg(this, "儲存完成!");
                            GridView1.DataBind();
                        }
                        else
                            MainControls.showMsg(this, "儲存失敗!");
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入項目!");
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    labSortMsg.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }
        #endregion

        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("name")) return;
            e.InputParameters.Add("name", Lib.FdVP(txtSh_name.Text).Trim());
            e.InputParameters.Add("mulayer", ddlSh_mulayer.SelectedValue);
            e.InputParameters.Add("enable", ddlSh_enable.SelectedValue.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_name.Text = string.Empty;
            ddlSh_mulayer.SelectedIndex = -1;
            ddlSh_enable.SelectedIndex = 1;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion
    }
}