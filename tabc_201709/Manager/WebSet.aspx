﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="WebSet.aspx.cs" Inherits="tabc_201709.Manager.WebSet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" Runat="Server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
                <table ID="Table1" align="center" border="0" cellpadding="2" cellspacing="1" 
                    width="100%">
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            網站標題</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebTitle" runat="server" MaxLength="50" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">最多輸入50字元</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            網站網址</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebUrl" runat="server" MaxLength="50" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">含「 http:// 」格式</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            Facebook 網址</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_fbUrl" runat="server" MaxLength="100" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font class="\" style="color: #D663A5">含「 http:// 」格式</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            Twitter 網址</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_twitterUrl" runat="server" MaxLength="50" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">含「 http:// 」格式</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            網站關鍵字</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebKeywords" runat="server" MaxLength="100" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">最多輸入100字元</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            網站描述文字</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebDescription" runat="server" MaxLength="500" Rows="10" 
                                TextMode="MultiLine" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">最多輸入500字元</font></td>
                    </tr>
                    <tr>
                        <td class="MS_tdTitle" width="150">
                            版權宣告文字</td>
                        <td class="tdData10s">
                            <asp:TextBox ID="txt_WebCopyright" runat="server" MaxLength="100" Width="500px"></asp:TextBox>
                            <img alt="" src="<%=ResolveUrl("images/help.gif") %>" />
                            <font style="color: #D663A5">最多輸入100字元</font></td>
                    </tr>
                    <tr>
                        <td align="center" class="tdMaintainButton" colspan="2">
                            <asp:Button ID="btnUppend" runat="server" CssClass="btnSave" 
                                onclick="btnUppend_Click" Text="更新資料" OnClientClick="return confirm(&quot;確定要更新嗎?&quot;);" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="left" style="line-height: 125%">
                                        <asp:Label ID="labMsg" runat="server" CssClass="errmsg12"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>