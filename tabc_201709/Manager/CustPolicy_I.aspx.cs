﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class CustPolicy_I : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            //判斷QueryString
            if (Request.QueryString["open"] != null && Request.QueryString["open"] == "1")
                MasterPageFile = "~/Manager/MasterPage_Open.master";
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_CU_CustName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                //case 1:
                //    if (btnEdit.Visible)
                //    {
                //        this.SetFocus(txt_PO_PolNo);
                //        Panel_Form.DefaultButton = btnEdit.ID;
                //    }
                //    else
                //    {
                //        this.SetFocus(txt_PO_PolNo);
                //        Panel_Form.DefaultButton = btnAppend.ID;
                //    }
                //    break;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                if (dr_person != null)
                {
                    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                    Session["AccID"] = dr_person["AccID"].ToString();
                    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                }
                DataRow dr_em = DataEmployee.emDataReader("11526");
                Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                Session["emNo"] = dr_em["emNo"].ToString().Trim();
                Session["emID"] = dr_em["emID"].ToString().Trim();
                Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                ViewState["SysValue"] = new SysValue("CustPolicy_I", this.Session);

                //系統參數
                //for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                //{
                //    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                //    {
                //        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                //        break;
                //    }
                //}
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);

                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();

                if (Request.QueryString["id"] != null)
                    QueryMainTable.Visible = false;
                else
                    QueryMainTable.Visible = true;
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataCustIndustPolm.DataColumn(this.GridView1);

        }

        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_CU_CustSource, true, "", "099");
            DataPhrase.DDL_TypeName(ddlSh_CU_Sex, true, "", "002");
            DataPhrase.DDL_TypeName(ddlSh_CU_ContactMode, true, "", "021");
            DataPhrase.DDL_TypeName(ddlSh_CU_Marry, true, "", "022");
            DataPhrase.DDL_TypeName(ddlSh_CU_CustType, true, "", "023");

            DataPhrase.DDL_TypeName(ddl_IP_ProdKind, true, "", "080");
            DataPhrase.DDL_TypeName(ddl_IP_Insuterm, true, "", "082");
            DataPhrase.DDL_TypeName(ddl_IP_Insnature, true, "", "083");
            DataPhrase.DDL_TypeName(ddl_IP_Cartype, true, "", "084");
            DataPhrase.DDL_TypeName(ddl_IP_Millnam, true, "", "085");
            DataPhrase.DDL_TypeName(ddl_IP_Carcc, true, "", "087");
            DataPhrase.DDL_TypeName(ddl_IP_Buildconst, true, "", "088");
            DataPhrase.DDL_TypeName(ddl_IP_Banktype, true, "", "089");
            DataPhrase.DDL_TypeName(ddl_IP_Banktype2, true, "", "090");
            DataTABC_COMPANY_MAP.DDL_TCM_ComCode(ddl_IP_ComSName, true, "");

        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_IP_Polno.Text.Trim() == "")
                sbError.Append("●「保單號碼」必須輸入!<br>");
            if (ddl_IP_ComSName.SelectedValue == "")
                sbError.Append("●「投保公司」必須選取!<br>");
            if (txt_IP_StartDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_IP_StartDate.Text.Trim()))
                    sbError.Append("●「投保日期」格式錯誤!<br>");
            }
            if (txt_IP_EndDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_IP_EndDate.Text.Trim()))
                    sbError.Append("●「到期日期」格式錯誤!<br>");
            }
            if (txt_IP_IssueDate.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_IP_IssueDate.Text.Trim()))
                    sbError.Append("●「原始發照日」格式錯誤!<br>");
            }
            if (txt_IP_Modeprem.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_Modeprem.Text.Trim())))
                    sbError.Append("●「保險費」格式錯誤!<br>");
            }

            if (txt_IP_LoadLimit.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_LoadLimit.Text.Trim())))
                    sbError.Append("●「乘載限制」格式錯誤!<br>");
            }
            if (txt_IP_CarPrice.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_CarPrice.Text.Trim())))
                    sbError.Append("●「購買車價(萬)」格式錯誤!<br>");
            }

            if (txt_IP_Archar.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_Archar.Text.Trim())))
                    sbError.Append("●「權狀面積(坪)」格式錯誤!<br>");
            }
            if (txt_IP_Totfloor.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_Totfloor.Text.Trim())))
                    sbError.Append("●「總樓層數」格式錯誤!<br>");
            }
            if (txt_IP_Decoration.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_IP_Decoration.Text.Trim())))
                    sbError.Append("●「裝潢總價(萬)」格式錯誤!<br>");
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "IP_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_UidNo":
                            hif_IP_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_PolNo":
                            txt_IP_Polno.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_ComSName":
                            MainControls.ddlIndexSelectText(ddl_IP_ComSName, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
                            if (dr_customer != null)
                            {
                                selCustomer1.search(dr_customer["CU_CustName"].ToString());
                                selCustomer1.SelectedValue = hif_CU_TransNo.Value;
                            }
                            break;
                        case "IP_StartDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_IP_StartDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "IP_EndDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_IP_EndDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "IP_Insuterm":
                            MainControls.ddlIndexSelectValue(ddl_IP_Insuterm, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_Modeprem":
                            txt_IP_Modeprem.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_PdtCoactive":
                            cbox_IP_PdtCoactive.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "有";
                            break;
                        case "IP_PdtArbitrariness":
                            cbox_IP_PdtArbitrariness.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "有";
                            break;
                        case "IP_ProdKind":
                            MainControls.ddlIndexSelectText(ddl_IP_ProdKind, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            showField(ddl_IP_ProdKind.SelectedItem.Text);
                            break;
                        //汽機車險
                        case "IP_Insnature":
                            MainControls.ddlIndexSelectValue(ddl_IP_Insnature, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_IssueDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_IP_IssueDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "IP_ProductDate":
                            txt_IP_ProductDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_CarOwner":
                            //車主
                            txt_IP_CarOwner.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            //屋主
                            txt_IP_CarOwner2.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_CarModel":
                            txt_IP_CarModel.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Carcc":
                            MainControls.ddlIndexSelectValue(ddl_IP_Carcc, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_Cartype":
                            MainControls.ddlIndexSelectValue(ddl_IP_Cartype, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_PlateNo":
                            txt_IP_PlateNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_LoadLimit":
                            txt_IP_LoadLimit.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Millnam":
                            MainControls.ddlIndexSelectText(ddl_IP_Millnam, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_EngineNo":
                            txt_IP_EngineNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_CarPrice":
                            txt_IP_CarPrice.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        //房屋險
                        case "IP_Archar":
                            txt_IP_Archar.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Borrower":
                            txt_IP_Borrower.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Buildconst":
                            MainControls.ddlIndexSelectText(ddl_IP_Buildconst, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_Totfloor":
                            txt_IP_Totfloor.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Banktype":
                            MainControls.ddlIndexSelectValue(ddl_IP_Banktype, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_Archyy":
                            txt_IP_Archyy.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Decoration":
                            txt_IP_Decoration.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Banktype2":
                            MainControls.ddlIndexSelectValue(ddl_IP_Banktype2, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "IP_Targetzip":
                            txt_IP_Targetzip.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Targetaddr":
                            txt_IP_Targetaddr.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_Memo":
                            txt_IP_Memo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "IP_PKSKey":
                            string PO_PKSKey = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            if (PO_PKSKey != "")//是匯入
                            {
                                ddl_IP_ProdKind.Enabled = false;
                                txt_IP_Polno.ReadOnly = true;
                                ddl_IP_ComSName.Enabled = false;
                                selCustomer1.Enabled = false;
                                txt_IP_StartDate.ReadOnly = true;
                            }
                            else
                            {
                                ddl_IP_ProdKind.Enabled = true;
                                txt_IP_Polno.ReadOnly = false;
                                ddl_IP_ComSName.Enabled = true;
                                selCustomer1.Enabled = true;
                                txt_IP_StartDate.ReadOnly = false;
                            }
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustIndustPolm.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataCustIndustPolm.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustIndustPolm.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataCustIndustPolm.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustIndustPolm.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataCustIndustPolm.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataCustIndustPolm.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    GridView1.DataBind();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("IP_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("IP_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("IP_UidNo", hif_IP_UidNo.Value);
            }
            hsData.Add("IP_Polno", txt_IP_Polno.Text.Trim());
            hsData.Add("IP_ComSName", ddl_IP_ComSName.SelectedItem.Text.Trim());
            DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(selCustomer1.SelectedValue);
            if (dr_customer != null)
            {
                hsData.Add("CU_TransNo", dr_customer["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_customer["CU_UidNo"].ToString());
                hsData.Add("PO_OwnerName", dr_customer["CU_CustName"].ToString());
            }
            hsData.Add("IP_StartDate", txt_IP_StartDate.Text.Trim());
            hsData.Add("IP_EndDate", txt_IP_EndDate.Text.Trim());
            hsData.Add("IP_Insuterm", ddl_IP_Insuterm.SelectedValue);
            hsData.Add("IP_Modeprem", txt_IP_Modeprem.Text.Trim());
            hsData.Add("IP_PdtCoactive", cbox_IP_PdtCoactive.Checked ? "有" : null);
            hsData.Add("IP_PdtArbitrariness", cbox_IP_PdtArbitrariness.Checked ? "有" : null);
            hsData.Add("IP_ProdKind", ddl_IP_ProdKind.SelectedItem.Text);
            //汽機車險
            hsData.Add("IP_Insnature", ddl_IP_Insnature.SelectedValue);
            hsData.Add("IP_IssueDate", txt_IP_IssueDate.Text.Trim());
            hsData.Add("IP_ProductDate", txt_IP_ProductDate.Text.Trim());
            if(ddl_IP_ProdKind.SelectedItem.Text == "")
                hsData.Add("IP_CarOwner", null);
            else
            {
                if (ddl_IP_ProdKind.SelectedItem.Text.IndexOf("車") != -1)
                    hsData.Add("IP_CarOwner", txt_IP_CarOwner.Text.Trim());//車主
                else
                    hsData.Add("IP_CarOwner", txt_IP_CarOwner2.Text.Trim());//屋主
            }
            hsData.Add("IP_CarModel", txt_IP_CarModel.Text.Trim());
            hsData.Add("IP_Carcc", ddl_IP_Carcc.SelectedValue);
            hsData.Add("IP_Cartype", ddl_IP_Cartype.SelectedValue);
            hsData.Add("IP_PlateNo", txt_IP_PlateNo.Text.Trim());
            hsData.Add("IP_LoadLimit", txt_IP_LoadLimit.Text.Trim());
            hsData.Add("IP_Millnam", ddl_IP_Millnam.SelectedItem.Text);
            hsData.Add("IP_EngineNo", txt_IP_EngineNo.Text.Trim());
            hsData.Add("IP_CarPrice", txt_IP_CarPrice.Text.Trim());
            //房屋險
            hsData.Add("IP_Archar", txt_IP_Archar.Text.Trim());
            hsData.Add("IP_Borrower", txt_IP_Borrower.Text.Trim());
            hsData.Add("IP_Buildconst", ddl_IP_Buildconst.SelectedItem.Text);
            hsData.Add("IP_Totfloor", txt_IP_Totfloor.Text.Trim());
            hsData.Add("IP_Banktype", ddl_IP_Banktype.SelectedValue);
            hsData.Add("IP_Archyy", txt_IP_Archyy.Text.Trim());
            hsData.Add("IP_Decoration", txt_IP_Decoration.Text.Trim());
            hsData.Add("IP_Banktype2", ddl_IP_Banktype2.SelectedValue);
            hsData.Add("PO_PolZip", txt_IP_Targetzip.Text.Trim());
            hsData.Add("PO_PolAddr", txt_IP_Targetaddr.Text.Trim());
            hsData.Add("PO_PolMemo", txt_IP_Memo.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("PO_TransNo", TransNo);
            hsData.Add("PO_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_IP_UidNo.Value = string.Empty;
            hif_CU_TransNo.Value = string.Empty;
            hif_CU_UidNo.Value = string.Empty;
            txt_IP_Polno.Text = string.Empty;
            ddl_IP_ComSName.SelectedIndex = -1;
            selCustomer1.Clear();
            txt_IP_StartDate.Text = string.Empty;
            txt_IP_EndDate.Text = string.Empty;
            ddl_IP_Insnature.SelectedIndex = -1;
            txt_IP_IssueDate.Text = string.Empty;
            txt_IP_Modeprem.Text = string.Empty;
            cbox_IP_PdtCoactive.Checked = false;
            cbox_IP_PdtArbitrariness.Checked = false;
            ddl_IP_ProdKind.SelectedIndex = -1;
            ddl_IP_ProdKind.Enabled = true;
            showField("");
            ddl_IP_Insnature.SelectedIndex = -1;
            txt_IP_IssueDate.Text = string.Empty;
            txt_IP_ProductDate.Text = string.Empty;
            txt_IP_CarOwner.Text = string.Empty;
            txt_IP_CarOwner2.Text = string.Empty;
            txt_IP_CarModel.Text = string.Empty;
            ddl_IP_Carcc.SelectedIndex = -1;
            ddl_IP_Cartype.SelectedIndex = -1;
            txt_IP_PlateNo.Text = string.Empty;
            txt_IP_LoadLimit.Text = string.Empty;
            ddl_IP_Millnam.SelectedIndex = -1;
            txt_IP_EngineNo.Text = string.Empty;
            txt_IP_CarPrice.Text = string.Empty;
            txt_IP_Archar.Text = string.Empty;
            txt_IP_Borrower.Text = string.Empty;
            ddl_IP_Buildconst.SelectedIndex = -1;
            txt_IP_Totfloor.Text = string.Empty;
            ddl_IP_Banktype.SelectedIndex = -1;
            txt_IP_Archyy.Text = string.Empty;
            txt_IP_Decoration.Text = string.Empty;
            ddl_IP_Banktype2.SelectedIndex = -1;
            txt_IP_Targetzip.Text = string.Empty;
            txt_IP_Targetaddr.Text = string.Empty;
            txt_IP_Memo.Text = string.Empty;

            txt_IP_Polno.ReadOnly = false;
            ddl_IP_ComSName.Enabled = true;
            selCustomer1.Enabled = true;
            txt_IP_StartDate.ReadOnly = false;

            

            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
            //新增預設帶入'要保人'='客戶'
            if (e.CommandName == "1" && Request.QueryString["id"] != null)
            {
                DataRow dr_customer = DataCustomer.DataReader_CU_TransNo(Request.QueryString["id"]);
                if (dr_customer != null)
                {
                    selCustomer1.search(dr_customer["CU_CustName"].ToString());
                    selCustomer1.SelectedValue = Request.QueryString["id"];
                }
            }
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_IP_Polno);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_IP_Polno);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[9].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[10].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[9].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[9].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[9].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustIndustPolm.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[10].Text, GridView1.Rows[i].Cells[11].Text)));

                    //RServiceProvider rsp = DataCustIndustPolm.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustIndustPolm.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }


        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("CU_TransNo")) return;
            if(Request.QueryString["id"] != null)
                e.InputParameters.Add("CU_TransNo", Request.QueryString["id"]);
            else
                e.InputParameters.Add("CU_TransNo", "");
            e.InputParameters.Add("CU_CustName", Lib.FdVP(txtSh_CU_CustName.Text).Trim());
            e.InputParameters.Add("CU_ID", Lib.FdVP(txtSh_CU_ID.Text).Trim());
            e.InputParameters.Add("CU_Sex", ddlSh_CU_Sex.SelectedItem.Text);
            e.InputParameters.Add("CU_Age", Lib.FdVP(txtSh_CU_Age.Text).Trim());
            e.InputParameters.Add("CU_Marry", ddlSh_CU_Marry.SelectedItem.Text);
            e.InputParameters.Add("CU_CustType", ddlSh_CU_CustType.SelectedItem.Text);
            e.InputParameters.Add("CU_CustSource", ddlSh_CU_CustSource.SelectedItem.Text);
            e.InputParameters.Add("CU_ContactMode", ddlSh_CU_ContactMode.SelectedItem.Text);
            e.InputParameters.Add("CU_IsShow", "1");
            e.InputParameters.Add("Mobile", Lib.FdVP(txtSh_Mobile.Text).Trim());
            e.InputParameters.Add("Address", Lib.FdVP(txtSh_Address.Text).Trim());
            e.InputParameters.Add("IP_ComSName", Lib.FdVP(txtSh_PO_ComSName.Text).Trim());
            e.InputParameters.Add("IP_Polno", Lib.FdVP(txtSh_PO_PolNo.Text).Trim());
            e.InputParameters.Add("IP_StartDate_s", DateRange_PO_AccureDate.Date_start);
            e.InputParameters.Add("IP_StartDate_e", DateRange_PO_AccureDate.Date_end);
            e.InputParameters.Add("IP_Memo", Lib.FdVP(txtSh_PO_Polmmemo.Text).Trim());
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_CU_CustName.Text = string.Empty;
            txtSh_CU_ID.Text = string.Empty;
            ddlSh_CU_Sex.SelectedIndex = -1;
            txtSh_CU_Age.Text = string.Empty;
            ddlSh_CU_Marry.SelectedIndex = -1;
            ddlSh_CU_CustType.SelectedIndex = -1;
            ddlSh_CU_CustSource.SelectedIndex = -1;
            ddlSh_CU_ContactMode.SelectedIndex = -1;
            txtSh_Mobile.Text = string.Empty;
            txtSh_Address.Text = string.Empty;
            txtSh_PO_ComSName.Text = string.Empty;
            txtSh_PO_PolNo.Text = string.Empty;
            DateRange_PO_AccureDate.Init();
            txtSh_PO_Polmmemo.Text = string.Empty;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        private void show_div(string clientID)
        {
            ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('#" + UpdatePanel1.ClientID + "').block({ message: $('#"+ clientID + "'), centerY: false , css: { width: '1000px', cursor: null, top: '20px' }, overlayCSS: {cursor: null}  }); ", true);
        }

        private void hide_div(string id)
        {

        }

        protected void ddl_IP_ProdKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            showField(ddl_IP_ProdKind.SelectedItem.Text);
        }

        private void showField(string IP_ProdKind)
        {
            if (IP_ProdKind == "")
            {
                tr_car_1.Visible = false;
                tr_car_2.Visible = false;
                tr_car_3.Visible = false;
                tr_car_4.Visible = false;
                tr_house_1.Visible = false;
                tr_house_2.Visible = false;
                tr_house_3.Visible = false;
                tr_house_4.Visible = false;
            }
            else
            {
                if (IP_ProdKind.IndexOf("車") != -1)
                {
                    tr_car_1.Visible = true;
                    tr_car_2.Visible = true;
                    tr_car_3.Visible = true;
                    tr_car_4.Visible = true;
                }
                else
                {
                    tr_house_1.Visible = true;
                    tr_house_2.Visible = true;
                    tr_house_3.Visible = true;
                    tr_house_4.Visible = true;
                }
            }
        }
    }
}