﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Reflection;
using Newtonsoft.Json;
using NLog;
using System.Net;

namespace tabc_201709.Manager
{
    public partial class Login : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Login"));

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (txtEm_id.Text.Trim() == "")
                this.SetFocus(txtEm_id);
            else if (txtEm_Pwd.Text.Trim() == "")
                this.SetFocus(txtEm_Pwd);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.UrlReferrer == null && (Session["Request.Url.AbsoluteUri"] == null || Session["Request.Url.AbsoluteUri"].ToString() != Request.Url.AbsoluteUri.Replace(".aspx", string.Empty)))
                {
                    if (Request.Url.AbsoluteUri.LastIndexOf(".aspx") == -1)
                        Session["Request.Url.AbsoluteUri"] = Request.Url.AbsoluteUri.Replace(".aspx", string.Empty);
                    if (bool.Parse(Application["WindowFull"].ToString()))
                        LoginOpen();
                }
                else
                {
                    Session.Remove("Request.Url.AbsoluteUri");
                }
                Page.Form.DefaultButton = btn_login.UniqueID;

                HttpCookie MS_UserID = Request.Cookies["MS_UserID"];
                txtEm_id.Text = MS_UserID == null ? "" : MS_UserID.Value;
                HttpCookie MS_Register = Request.Cookies["MS_Register"];
                cbox_register.Checked = bool.Parse(MS_Register == null ? "true" : MS_Register.Value);
                //一般圖文驗證
                if (DataWebSet.dataReader("SysLoginRecaptcha") == "1")
                    tr_code.Visible = true;
                //google reCAPTCHA 
                if (DataWebSet.dataReader("SysLoginRecaptcha") == "2")
                    Literal_recaptcha.Text = string.Format("<div class=\"g-recaptcha\" data-sitekey=\"{0}\"></div>", Lib.PuchAesChinese(DataWebSet.dataReader("gCAPTCHA_SiteKey"), DataSysValue.Password));
            }
        }

        #region 開啟登入畫面
        private void LoginOpen()
        {
            //設定開啟新視窗，及視窗屬性設定
            string script = "if(navigator.userAgent.toLowerCase().match('msie')){ window.open('/Manager/Login', '', 'resizable=yes,fullscreen=yes,scrollbars=yes'); window.open('', '_self', ''); window.close(); } else { window.open('', '_self', ''); window.close(); }";
            ScriptManager.RegisterStartupScript(this, this.GetType(), MainControls.random("", 8), script, true);
        }
        #endregion

        #region 登入驗證
        private void loginValid(int mode)
        {
            labMsg.Text = string.Empty;

            if (DataWebSet.dataReader("SysLoginRecaptcha") == "1")
            {
                if (!txtEm_SN.Text.ToLower().Equals(Session["F1BC2AA3-7622-41E1-92C8-0452A26E1793"].ToString().ToLower()))
                {
                    labMsg.Text += "「驗證碼」輸入錯誤 ";
                    txtEm_SN.Text = string.Empty;
                    this.SetFocus(txtEm_Pwd);
                    SN1.Reload();
                    pnl_msg.Visible = labMsg.Text == "" ? false : true;
                    return;
                }
            }

            if (DataWebSet.dataReader("SysLoginRecaptcha") == "2")
            {
                if (!ValidCAPTCHA())
                {
                    labMsg.Text += "「驗證碼」輸入錯誤 ";

                    pnl_msg.Visible = labMsg.Text == "" ? false : true;
                    return;
                }
            }

            if (txtEm_id.Text.Trim() == "" || txtEm_Pwd.Text.Trim() == "")
            {
                if (txtEm_id.Text.Trim() == "")
                {
                    labMsg.Text += "「使用者帳號」必須輸入 ";
                    this.SetFocus(txtEm_id);
                }
                else if (txtEm_Pwd.Text.Trim() == "")
                {
                    labMsg.Text += "「密碼」必須輸入 ";
                    this.SetFocus(txtEm_Pwd);
                }
            }
            else
            {
                DataSet emds = new DataSet();
                switch (mode)
                {
                    case 0:
                        emds = DataEmployee.employeeLogin(txtEm_id.Text.Trim(), Lib.GetMD5(txtEm_Pwd.Text.Trim()), MainControls.GetUserIPAddress(), mode);
                        
                        break;
                    default:

                        break;
                }
                if (emds == null || emds.Tables[0].Rows.Count == 0)
                {
                    labMsg.Text = "登入失敗!";
                    if (DataWebSet.dataReader("SysLoginRecaptcha") == "1")
                    {
                        txtEm_SN.Text = string.Empty;
                        SN1.Reload();
                    }
                    this.SetFocus(txtEm_Pwd);
                    logger.Info(string.Format("{2} 帳號：{0}；IP：{1}", txtEm_id.Text.Trim(), MainControls.GetUserIPAddress(), "登入失敗！"));
                }
                else
                {
                    DataRow dr_person = DataPerson.DataReader_AccID(emds.Tables[0].Rows[0]["AccID"].ToString().Trim());
                    if (dr_person != null)
                    {
                        Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                        Session["AccID"] = dr_person["AccID"].ToString();
                        Session["ComyCode"] = dr_person["ComyCode"].ToString();
                    }


                    Session["emUser"] = string.Format("{0}({1})", emds.Tables[0].Rows[0]["emName"].ToString().Trim(), emds.Tables[0].Rows[0]["emID"].ToString().Trim());
                    Session["emNo"] = emds.Tables[0].Rows[0]["emNo"].ToString().Trim();
                    Session["emID"] = emds.Tables[0].Rows[0]["emID"].ToString().Trim();
                    Session["emLimit"] = emds.Tables[0].Rows[0]["specialLimit"].ToString().Trim();

                    HttpCookie MS_UserID = new HttpCookie("MS_UserID");
                    HttpCookie MS_Register = new HttpCookie("MS_Register");
                    if (cbox_register.Checked)
                    {
                        MS_UserID.Value = txtEm_id.Text.Trim();
                        MS_Register.Value = cbox_register.Checked.ToString();
                        Response.Cookies.Add(MS_UserID);
                        Response.Cookies.Add(MS_Register);
                    }

                    logger.Info(string.Format("{2} 帳號：{0}；IP：{1}", txtEm_id.Text.Trim(), MainControls.GetUserIPAddress(), "登入成功！"));
                    Response.Redirect("index");
                }
            }
            pnl_msg.Visible = labMsg.Text == "" ? false : true;
        }
        #endregion

        #region google reCAPTCHA
        private bool ValidCAPTCHA()
        {
            var response = Request["g-recaptcha-response"];
            string secret = Lib.PuchAesChinese(DataWebSet.dataReader("gCAPTCHA_Secretkey"), DataSysValue.Password);

            var client = new WebClient();
            var jsonString =
                client.DownloadString(
                    string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}&remoteip={2}", secret, response, MainControls.GetUserIPAddress()));

            var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(jsonString);

            if (captchaResponse.Success)
            {
                return true;
            }
            else
            {
                return false;
                //var error = captchaResponse.ErrorCodes[0].ToLower();
                //switch (error)
                //{
                //    case ("missing-input-secret"):
                //        MainControls.showMsg(this, "The secret parameter is missing.");
                //        break;
                //    case ("invalid-input-secret"):
                //        MainControls.showMsg(this, "The secret parameter is invalid or malformed.");
                //        break;
                //    case ("missing-input-response"):
                //        MainControls.showMsg(this, "The response parameter is missing.");
                //        break;
                //    case ("invalid-input-response"):
                //        MainControls.showMsg(this, "The response parameter is invalid or malformed.");
                //        break;
                //    default:
                //        MainControls.showMsg(this, "Error occured. Please try again");
                //        break;
                //}
            }
        }
        private class CaptchaResponse
        {
            [JsonProperty("success")]
            public bool Success { get; set; }

            [JsonProperty("error-codes")]
            public List<string> ErrorCodes { get; set; }
        }
        #endregion

        protected void btn_login_Click(object sender, EventArgs e)
        {
            txtEm_id.Text = Lib.FdVP(txtEm_id.Text);
            txtEm_Pwd.Text = Lib.FdVP(txtEm_Pwd.Text);

            loginValid(0);
        }
    }
}