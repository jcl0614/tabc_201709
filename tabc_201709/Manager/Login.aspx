﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="tabc_201709.Manager.Login" %>
<%@ Register src="~/UserControl/SN.ascx" tagname="SN" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="css/Ms.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" Runat="Server">
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="100">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding: 5px; border-radius: 5px;" bgcolor="White">
                                    <table cellpadding="0" cellspacing="0" 
                                        style="border: 2px solid #f07b00; border-radius: 5px;" bgcolor="White">
                                        <tr>
                                            <td style="padding: 20px 50px 20px 50px">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td align="right" 
                                                            style="height: 35px; font-family: Arial, Helvetica, sans-serif; color: #565759; font-size: medium;" 
                                                            width="100">
                                                            帳號</td>
                                                        <td width="20">
                                                            &nbsp;</td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtEm_id" runat="server" Font-Names="Arial" Width="155px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" 
                                                            style="height: 35px; font-family: Arial, Helvetica, sans-serif; color: #565759; font-size: medium;" 
                                                            width="100">
                                                            密碼</td>
                                                        <td width="20">
                                                            &nbsp;</td>
                                                        <td align="left">
                                                            <asp:TextBox ID="txtEm_Pwd" runat="server" Font-Names="Arial" TextMode="Password" Width="155px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="tr_code" runat="server" visible="false">
                                                        <td align="right" 
                                                            style="height: 35px; font-family: Arial, Helvetica, sans-serif; color: #565759; font-size: medium;" 
                                                            width="100">
                                                            驗證碼</td>
                                                        <td width="20">
                                                            &nbsp;</td>
                                                        <td align="left">
                                                            <table cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEm_SN" runat="server" Font-Names="Arial" MaxLength="5" Width="50px"></asp:TextBox>
                                                                    </td>
                                                                    <td width="5">&nbsp;</td>
                                                                    <td>
                                                                        <uc1:SN ID="SN1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right" 
                                                            style="height: 35px; font-family: Arial, Helvetica, sans-serif; color: #565759; font-size: medium;" 
                                                            width="100">
                                                            &nbsp;</td>
                                                        <td width="20">
                                                            &nbsp;</td>
                                                        <td align="left">
                                                            <asp:CheckBox ID="cbox_register" runat="server" Font-Names="Arial" 
                                                                ForeColor="#565759" Text="記住帳號" Font-Size="Small" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-bottom: 20px">
                                                <asp:Literal ID="Literal_recaptcha" runat="server"></asp:Literal>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding-bottom: 20px">
                                                <table cellpadding="0" cellspacing="0" style="border-top-style: dotted; border-top-width: 1px; border-top-color: #f07b00" width="80%">
                                                    <tr>
                                                        <td align="center" style="padding-top: 20px">
                                                            <asp:Button ID="btn_login" runat="server" CssClass="button_default" onclick="btn_login_Click" Text="登　入" Width="100px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 35px" valign="top">
                                    <asp:Panel ID="pnl_msg" runat="server" Visible="False">
                                        <table cellpadding="0" cellspacing="0" 
                                            style="height: 30px; background-color: #CC0000; margin-top: 5px;border-radius: 5px;" width="100%">
                                            <tr>
                                                <td align="center" style="color: #FFFFFF; font-size: 0px;" width="30">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Manager/images/info.png" />
                                                </td>
                                                <td align="left" style="color: #FFFFFF">
                                                    <asp:Label ID="labMsg" runat="server" Font-Size="Small"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td height="100">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

