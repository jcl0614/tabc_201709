﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class ECircular : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(ddlSh_EI_Mode);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(ddl_EI_Mode);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(ddl_EI_Mode);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataECircular.DataColumn(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_EI_Mode, true, "", "136");
            DataPhrase.DDL_TypeName(ddl_EI_Mode, true, "請選擇", "136");
            DataPhrase.DDL_TypeName(ddl_EI_Mode0, true, "請選擇", "136");
            DataPhrase.DDL_TypeName(ddl_EI_Title, true, "請選擇", "E010");
            DataPhrase.DDL_TypeName(ddl_EI_Title0, true, "請選擇", "E010");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();
            
            if (ddl_EI_Mode.SelectedValue == "")
                sbError.Append("●「教育訓練類型」必須選取!<br>");
            if (selPerson_AccID_EI_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「業務員」必須選取!<br>");
            if (txt_EI_AppDate.Text.Trim() == "")
                sbError.Append("●「申請日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EI_AppDate.Text.Trim()))
                    sbError.Append("●「申請日期」格式錯誤!<br>");
            }
            if (ddl_EI_Title.SelectedValue == "")
                sbError.Append("●「職稱」必須選取!<br>");
            if (txt_EI_CompDate.Text.Trim() == "")
                sbError.Append("●「完訓日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EI_CompDate.Text.Trim()))
                    sbError.Append("●「完訓日期」格式錯誤!<br>");
            }
            if (txt_EI_Score.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EI_Score.Text.Trim())))
                    sbError.Append("●「分數」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        private string dataValid_batch()
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_EI_Mode0.SelectedValue == "")
                sbError.Append("●「教育訓練類型」必須選取!<br>");
            if (txt_EI_AppDate0.Text.Trim() == "")
                sbError.Append("●「申請日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EI_AppDate0.Text.Trim()))
                    sbError.Append("●「申請日期」格式錯誤!<br>");
            }
            if (ddl_EI_Title0.SelectedValue == "")
                sbError.Append("●「職稱」必須選取!<br>");
            if (txt_EI_CompDate0.Text.Trim() == "")
                sbError.Append("●「完訓日期」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDate(txt_EI_CompDate0.Text.Trim()))
                    sbError.Append("●「完訓日期」格式錯誤!<br>");
            }
            if (txt_EI_Score0.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", (txt_EI_Score0.Text.Trim())))
                    sbError.Append("●「分數」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EI_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EI_UidNo":
                            hif_EI_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EI_Mode":
                            MainControls.ddlIndexSelectValue(ddl_EI_Mode, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EI_Title":
                            MainControls.ddlIndexSelectValue(ddl_EI_Title, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EI_AppDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_EI_AppDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EI_CompDate":
                            if (MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_EI_CompDate.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "EI_AccID":
                            selPerson_AccID_EI_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_EI_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_EI_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
                            if (dr_person != null)
                                lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
                            else
                                lbl_PS_Title.Text = string.Empty;
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
                            if (dr_sales != null)
                            {
                                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                                if (dr_Unit != null)
                                {
                                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                                }
                                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                                if (dr_sales2 != null)
                                {
                                    lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                                }
                            }
                            break;
                        case "EI_Score":
                            txt_EI_Score.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selPerson_AccID_EI_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow dr_person = DataPerson.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_person != null)
                lbl_PS_Title.Text = dr_person["PS_Title"].ToString();
            else
                lbl_PS_Title.Text = string.Empty;
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_sales != null)
            {
                DataRow dr_Unit = DataUnit.dataReader_cZON_PK(dr_sales["Zone"].ToString());
                if (dr_Unit != null)
                {
                    lbl_cZON_NAME.Text = dr_Unit["cZON_NAME"].ToString();
                    lbl_cZON_TYPE.Text = dr_Unit["cZON_TYPE"].ToString();
                }
                DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                if (dr_sales2 != null)
                {
                    lbl_DN_Name.Text = dr_sales2["NAME"].ToString();
                }
            }
            else
            {
                lbl_cZON_NAME.Text = string.Empty;
                lbl_cZON_TYPE.Text = string.Empty;
                lbl_DN_Name.Text = string.Empty;
            }
        }
        protected void selPerson_AccID_EI_AccID_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EI_AccID_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EI_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EI_AccID.search(hif_EI_AccID.Value);
            selPerson_AccID_EI_AccID.SelectedValue = hif_EI_AccID.Value;
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataECircular.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EI_UidNo.Value = string.Empty;
                        this.SetFocus(txt_EI_AppDate);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataECircular.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EI_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EI_UidNo", hif_EI_UidNo.Value);
            }
            hsData.Add("EI_Type", "1");
            hsData.Add("EI_Mode", ddl_EI_Mode.SelectedValue);
            hsData.Add("EI_Title", ddl_EI_Title.SelectedValue);
            hsData.Add("EI_AppDate", txt_EI_AppDate.Text.Trim());
            hsData.Add("EI_CompDate", txt_EI_CompDate.Text.Trim());
            hsData.Add("EI_Score", txt_EI_Score.Text.Trim());
            hsData.Add("EI_AccID", selPerson_AccID_EI_AccID.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EI_AccID.SelectedValue);
            if (dr_sales != null)
            {
                hsData.Add("EI_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_batch(string EI_AccID, string EI_Mode, string EI_AppDate, string EI_CompDate, string EI_Score, string EI_Title)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("EI_UidNo", Session["UidNo"].ToString());
            hsData.Add("EI_Type", "1");
            hsData.Add("EI_Mode", EI_Mode);
            hsData.Add("EI_AppDate", EI_AppDate);
            hsData.Add("EI_CompDate", EI_CompDate);
            hsData.Add("EI_Score", EI_Score);
            hsData.Add("EI_Title", EI_Title);
            hsData.Add("EI_AccID", EI_AccID);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(EI_AccID);
            if (dr_sales != null)
            {
                hsData.Add("EI_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EI_TransNo", TransNo);
            hsData.Add("EI_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EI_UidNo.Value = string.Empty;
            ddl_EI_Mode.SelectedIndex = -1;
            ddl_EI_Title.SelectedIndex = -1;
            txt_EI_AppDate.Text = string.Empty;
            txt_EI_CompDate.Text = string.Empty;
            txt_EI_Score.Text = string.Empty;
            lbl_PS_Title.Text = string.Empty;
            lbl_cZON_TYPE.Text = string.Empty;
            lbl_cZON_NAME.Text = string.Empty;
            lbl_DN_Name.Text = string.Empty;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EI_Mode);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(ddl_EI_Mode);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[8].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[9].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[8].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[8].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[9].Text, GridView1.Rows[i].Cells[10].Text)));

                    //RServiceProvider rsp = DataECircular.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataECircular.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EI_Type")) return;
            e.InputParameters.Add("EI_Type", "1");
            e.InputParameters.Add("EI_Mode", ddlSh_EI_Mode.Text.Trim());
            e.InputParameters.Add("EI_Title", txtSh_EI_Title.Text.Trim());
            e.InputParameters.Add("EI_Name", txtSh_EI_Name.Text.Trim());
            e.InputParameters.Add("EI_AppDate_s", DateRangeSh_EI_AppDate.Date_start);
            e.InputParameters.Add("EI_AppDate_e", DateRangeSh_EI_AppDate.Date_end);
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EI_Mode.SelectedIndex = -1;
            txtSh_EI_Title.Text = string.Empty;
            txtSh_EI_Name.Text = string.Empty;
            DateRangeSh_EI_AppDate.Init();
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion


        protected void btnApply_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "search"://
                    DataTable dt = DataPerson.baseDataReader(true, txtSh_AccID.Text.Trim()).Tables[0];
                    lboxSur.Items.Clear();
                    foreach (DataRow dw in dt.Rows)
                    {
                        ListItem li = new ListItem();
                        li.Text = dw["PS_NAME"].ToString().Trim();
                        li.Value = dw["AccID"].ToString().Trim();
                        lboxSur.Items.Add(li);
                    }
                    break;
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) == -1)
                        {
                            lboxObj.Items.Add(m_item);
                        }
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            if (lboxObj.Items.IndexOf(itemSur) == -1)
                            {
                                lboxObj.Items.Add(itemSur);
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                                lboxSur.Items.Remove(itemSur);
                                if (lboxSur.Items.Count > siSur)
                                    lboxSur.SelectedIndex = siSur;
                                else
                                    lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            }
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "Append"://儲存新顯示順序設定
                    lbl_Msg0.Text = dataValid_batch(); //資料格式驗証
                    if (lbl_Msg0.Text.Trim().Length > 0) return;
                    if (lboxObj.Items.Count != 0)
                    {
                        SysValue sysValue = ViewState["SysValue"] as SysValue;
                        StringBuilder sb_msg = new StringBuilder();
                        foreach (ListItem m_item in lboxObj.Items)
                        {
                            DataSet ds = new DataSet();
                            //元件資料轉成Hashtable
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataECircular.GetSchema(), 
                                dataToHashtable_batch(m_item.Value, ddl_EI_Mode0.SelectedValue, txt_EI_AppDate0.Text.Trim(), txt_EI_CompDate0.Text.Trim(), txt_EI_Score0.Text.Trim(), ddl_EI_Title0.SelectedValue)));

                            RServiceProvider rsp = DataECircular.Append(sysValue.emNo, sysValue.ProgId, ds);
                            if (rsp.Result)
                            {
                                sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：新增成功！</span><br>", m_item.Text, m_item.Value);
                            }
                            else
                                sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}({1})：新增失敗！</span><br>", m_item.Text, m_item.Value);
                        }
                        GridView1.DataBind();
                        lbl_Msg0.Text = sb_msg.ToString();
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入人員!");
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    lbl_Msg0.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }

        protected void MarkSortSetInit()
        {
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            txtSh_AccID.Text = string.Empty;
        }

        protected void export_Command(object sender, CommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "1"://1年度在職教育訓練
                    {
                        HSSFWorkbook workbook;
                        using (FileStream fs = new FileStream(Server.MapPath("~/XLS/14教育訓練&年金通報申請表--A29-ls_01060622_309901220.xls"), FileMode.Open, FileAccess.Read))
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        MemoryStream ms = new MemoryStream();
                        ISheet sheet = workbook.GetSheetAt(0);
                        DataTable dt = DataECircular.DataReader("1", "1", txtSh_EI_Title.Text.Trim(), txtSh_EI_Name.Text.Trim(), DateRangeSh_EI_AppDate.Date_start, DateRangeSh_EI_AppDate.Date_end, 0, 99999).Tables[0];
                        int rowIndex = 20;
                        Int32 count = (Int32)dt.Compute("count(EI_AccID)", "");
                        foreach (DataRow dr in dt.Rows)
                        {
                            string ID = "", Name = "", Birth = "";

                            DataRow dr_person = DataPerson.DataReader_AccID(dr["EI_AccID"].ToString());
                            if (dr_person != null)
                            {
                               ID = dr_person["PS_ID"].ToString();
                               Name = dr_person["PS_NAME"].ToString();
                            }
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EI_AccID"].ToString());
                            if (dr_sales != null)
                            {
                                Birth = dr_sales["Birth"].ToString();
                            }
                            for (int i = 0; i <= 10;i++ )
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i+1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue(Name);
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue("1");
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(3);
                                            if(dr["EI_AppDate"].ToString() != "")
                                            {
                                                DateTime EI_AppDate = DateTime.Parse(dr["EI_AppDate"].ToString());
                                                cell.SetCellValue(string.Format("{0}{1}{2}", EI_AppDate.Year.ToString("0000"), EI_AppDate.Month.ToString("00"), EI_AppDate.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue((i+1).ToString("00000"));
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(ID);
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(7);
                                            if (Birth != "")
                                            {
                                                DateTime BirthDay = DateTime.Parse(Birth);
                                                cell.SetCellValue(string.Format("{0}{1}{2}", BirthDay.Year.ToString("0000"), BirthDay.Month.ToString("00"), BirthDay.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(8);
                                            if (dr["EI_CompDate"].ToString() != "")
                                            {
                                                DateTime EI_CompDate = DateTime.Parse(dr["EI_CompDate"].ToString());
                                                cell.SetCellValue((EI_CompDate.Year - 1911 - 100).ToString("00"));
                                            }
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(9);
                                            if (dr["EI_CompDate"].ToString() != "")
                                            {
                                                DateTime EI_AppDate = DateTime.Parse(dr["EI_CompDate"].ToString());
                                                cell.SetCellValue(string.Format("{0}{1}{2}", EI_AppDate.Year.ToString("0000"), EI_AppDate.Month.ToString("00"), EI_AppDate.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 10:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(10);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(10);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        DateTime EX_Date = DateTime.Now;
                        sheet.GetRow(16).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (EX_Date.Year - 1911).ToString("0000"), EX_Date.Month.ToString("00"), EX_Date.Day.ToString("00")));
                        sheet.GetRow(16).GetCell(6).SetCellValue(count);

                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("年度在職教育訓練_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
                case "2"://2年金銷售資格
                    {
                        HSSFWorkbook workbook;
                        using (FileStream fs = new FileStream(Server.MapPath("~/XLS/14教育訓練&年金通報申請表--A29-ls_01060622_309901220.xls"), FileMode.Open, FileAccess.Read))
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        MemoryStream ms = new MemoryStream();
                        ISheet sheet = workbook.GetSheetAt(1);
                        DataTable dt = DataECircular.DataReader("1", "2", txtSh_EI_Title.Text.Trim(), txtSh_EI_Name.Text.Trim(), DateRangeSh_EI_AppDate.Date_start, DateRangeSh_EI_AppDate.Date_end, 0, 99999).Tables[0];
                        int rowIndex = 20;
                        Int32 count = (Int32)dt.Compute("count(EI_AccID)", "");
                        foreach (DataRow dr in dt.Rows)
                        {
                            string ID = "", Name = "", Birth = "";

                            DataRow dr_person = DataPerson.DataReader_AccID(dr["EI_AccID"].ToString());
                            if (dr_person != null)
                            {
                                ID = dr_person["PS_ID"].ToString();
                                Name = dr_person["PS_NAME"].ToString();
                            }
                            DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EI_AccID"].ToString());
                            if (dr_sales != null)
                            {
                                Birth = dr_sales["Birth"].ToString();
                            }
                            for (int i = 0; i <= 10; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(0);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(0);
                                            cell.SetCellValue(i + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(1);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(1);
                                            cell.SetCellValue(Name);
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(2);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(2);
                                            cell.SetCellValue("1");
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(3);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(3);
                                            if (dr["EI_AppDate"].ToString() != "")
                                            {
                                                DateTime EI_AppDate = DateTime.Parse(dr["EI_AppDate"].ToString());
                                                cell.SetCellValue(string.Format("{0}{1}{2}", EI_AppDate.Year.ToString("0000"), EI_AppDate.Month.ToString("00"), EI_AppDate.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(4);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(4);
                                            cell.SetCellValue((i + 1).ToString("00000"));
                                        }
                                        break;
                                    case 5:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(5);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(5);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                    case 6:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(6);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(6);
                                            cell.SetCellValue(ID);
                                        }
                                        break;
                                    case 7:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(7);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(7);
                                            if (Birth != "")
                                            {
                                                DateTime BirthDay = DateTime.Parse(Birth);
                                                cell.SetCellValue(string.Format("{0}{1}{2}", BirthDay.Year.ToString("0000"), BirthDay.Month.ToString("00"), BirthDay.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 8:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(8);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(8);
                                            if (dr["EI_CompDate"].ToString() != "")
                                            {
                                                DateTime EI_CompDate = DateTime.Parse(dr["EI_CompDate"].ToString());
                                                cell.SetCellValue((EI_CompDate.Year - 1911 - 100).ToString("00"));
                                            }
                                        }
                                        break;
                                    case 9:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(9);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(9);
                                            if (dr["EI_CompDate"].ToString() != "")
                                            {
                                                DateTime EI_AppDate = DateTime.Parse(dr["EI_CompDate"].ToString());
                                                cell.SetCellValue(string.Format("{0}{1}{2}", EI_AppDate.Year.ToString("0000"), EI_AppDate.Month.ToString("00"), EI_AppDate.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 10:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(10);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(10);
                                            if (dr["EI_Score"].ToString() != "")
                                            {
                                                cell.SetCellValue(int.Parse(dr["EI_Score"].ToString()).ToString("000"));
                                            }
                                        }
                                        break;
                                }
                            }
                            rowIndex += 1;
                        }
                        DateTime EX_Date = DateTime.Now;
                        sheet.GetRow(16).GetCell(2).SetCellValue(string.Format("{0}{1}{2}", (EX_Date.Year - 1911).ToString("0000"), EX_Date.Month.ToString("00"), EX_Date.Day.ToString("00")));
                        sheet.GetRow(16).GetCell(6).SetCellValue(count);

                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("年金銷售資格_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
                case "3"://3年度防制洗錢教育訓練
                    {
                        HSSFWorkbook workbook;
                        using (FileStream fs = new FileStream(Server.MapPath("~/XLS/15防制洗錢通報--105下半年度防制洗錢教育訓練通報表.xls"), FileMode.Open, FileAccess.Read))
                        {
                            workbook = new HSSFWorkbook(fs);
                        }
                        MemoryStream ms = new MemoryStream();
                        ISheet sheet = workbook.GetSheetAt(0);
                        DataTable dt = DataECircular.DataReader("1", "1", txtSh_EI_Title.Text.Trim(), txtSh_EI_Name.Text.Trim(), DateRangeSh_EI_AppDate.Date_start, DateRangeSh_EI_AppDate.Date_end, 0, 99999).Tables[0];
                        int rowIndex = 10;
                        Int32 count = (Int32)dt.Compute("count(EI_AccID)", "");
                        foreach (DataRow dr in dt.Rows)
                        {
                            string ID = "", Name = "", EI_Title = "";

                            DataRow dr_person = DataPerson.DataReader_AccID(dr["EI_AccID"].ToString());
                            if (dr_person != null)
                            {
                                ID = dr_person["PS_ID"].ToString();
                                Name = dr_person["PS_NAME"].ToString();
                            }
                            DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeNo("E010", dr["EI_Title"].ToString());
                            if (dr_Phrase != null)
                            {
                                EI_Title = dr_Phrase["TypeName"].ToString();
                            }
                            int indexCell = dt.Rows.IndexOf(dr) % 2 == 0 ? 0 : 5;
                            for (int i = 0; i <= 4; i++)
                            {
                                switch (i)
                                {
                                    case 0:
                                        {
                                            
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(0 + indexCell);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(0 + indexCell);
                                            cell.SetCellValue(dt.Rows.IndexOf(dr) + 1);
                                        }
                                        break;
                                    case 1:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(1 + indexCell);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(1 + indexCell);
                                            cell.SetCellValue(EI_Title);
                                        }
                                        break;
                                    case 2:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(2 + indexCell);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(2 + indexCell);
                                            cell.SetCellValue(Name);
                                        }
                                        break;
                                    case 3:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(3 + indexCell);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(3 + indexCell);
                                            if (dr["EI_AppDate"].ToString() != "")
                                            {
                                                DateTime EI_AppDate = DateTime.Parse(dr["EI_AppDate"].ToString());
                                                cell.SetCellValue(string.Format("{0}{1}{2}", EI_AppDate.Year.ToString("0000"), EI_AppDate.Month.ToString("00"), EI_AppDate.Day.ToString("00")));
                                            }
                                        }
                                        break;
                                    case 4:
                                        {
                                            ICell cell = sheet.GetRow(rowIndex).GetCell(4 + indexCell);
                                            if (cell == null)
                                                cell = sheet.GetRow(rowIndex).CreateCell(4 + indexCell);
                                            cell.SetCellValue("");
                                        }
                                        break;
                                }
                            }
                            if(dt.Rows.IndexOf(dr) % 2 == 1)
                                rowIndex += 1;
                        }

                        workbook.Write(ms);
                        ms.Flush();
                        ms.Position = 0;

                        HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(string.Format("年度防制洗錢教育訓練_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss")))));
                        HttpContext.Current.Response.Charset = "big5";
                        HttpContext.Current.Response.BinaryWrite(ms.ToArray());
                        ms.Close();
                        ms.Dispose();
                    }
                    break;
            }
        }




    }
}