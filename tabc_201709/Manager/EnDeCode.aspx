﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EnDeCode.aspx.cs" Inherits="tabc_201709.Manager.EnDeCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel1" runat="server">
                <table align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lab_enAEStitle" runat="server" Font-Italic="False" 
                                            Font-Names="微軟正黑體" ForeColor="#333333" Text="《AES加密》"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_type" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="1">密碼</asp:ListItem>
                                            <asp:ListItem Value="2">連線</asp:ListItem>
                                            <asp:ListItem Value="3">信箱</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="5">
                                        &nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_enAES" runat="server" onclick="btn_enAES_Click" Text="加密" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_enAES" runat="server" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_enAESr" runat="server" ReadOnly="True" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lab_deAEStitle" runat="server" Font-Italic="False" 
                                            Font-Names="微軟正黑體" ForeColor="#333333" Text="《AES解密》"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_type0" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="1">密碼</asp:ListItem>
                                            <asp:ListItem Value="2">連線</asp:ListItem>
                                            <asp:ListItem Value="3">信箱</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="5">
                                        &nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_deAES" runat="server" onclick="btn_deAES_Click" Text="解密" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_deAES" runat="server" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_deAESr" runat="server" ReadOnly="True" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <table align="center" cellpadding="5" cellspacing="0">
                    <tr>
                        <td align="left">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:Label ID="lab_enMD5" runat="server" Font-Italic="False" Font-Names="微軟正黑體" 
                                            ForeColor="#333333" Text="《MD5加密》"></asp:Label>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Button ID="btn_enAES0" runat="server" onclick="btn_enAES0_Click" 
                                            Text="加密" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_deMD5" runat="server" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_enMD5" runat="server" ReadOnly="True" Width="1000px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server">
                <table align="center" cellpadding="50" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox ID="txt_PWD" runat="server" AutoPostBack="True" 
                                ontextchanged="txt_PWD_TextChanged" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    </form>
</body>
</html>
