using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace tabc_201709.Manager
{
    public partial class PdInfo : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage10);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            this.SetFocus(txtSh_PdtNo);
            Panel_Form.DefaultButton = btnSearch.ID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1350, true);
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }

        #region 資料繫結
        private void dataBind()
        {
            DataPdInfo.PdInfoDataColumn(this.GridView1);
        }
        #endregion

        #region 資料驗証
        private string dataVaild()
        {
            StringBuilder sbError = new StringBuilder();

            //if (txt_edDate_BEG_q.Text.Trim().Length > 0)
            //{
            //    if (!MainControls.ValidDate(txt_edDate_BEG_q.Text.Trim()))
            //        sbError.Append("●「維護日期起」輸入格式錯誤!\n");
            //}
            //if (txt_edDate_END_q.Text.Trim().Length > 0)
            //{
            //    if (!MainControls.ValidDate(txt_edDate_END_q.Text.Trim()))
            //        sbError.Append("●「維護日期訖」輸入格式錯誤!\n");
            //}
            return sbError.ToString().Trim();
        }
        #endregion

        #region 設定下拉選單的內容
        private void setddlChoices()
        {
            //  DataPdInfo.CompanyDropDownList(ddlSh_ComKind,"0");
            DataPdInfo.PhraseDropDownList(ddlSh_kind, "201","TypeNo");
          //  TypeNo,TypeSubCode
          //  DataPdInfo.DispClassDropDownList(ddlSh_Type);
            // DataPdInfo.PhraseDropDownList(ddlSh_Type, "060");
             DataPdInfo.PhraseDropDownList(ddlSh_Type, "061","TypeSubCode");
            DataPdInfo.CompanyDropDownList(ddlSh_Company, ddlSh_ComKind.SelectedValue);
        }
        #endregion

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
          //  lnkBrowse.Visible = false;
          //  lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
            //        clearControlContext();
            //        maintainButtonEnabled("Append");
           //         lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
            //        lnkBrowse.Visible = true;
                    ChangeMultiView3(0);
                    break;
            }
        }
        private void ChangeMultiView3(int index)
        {
            MultiView3.ActiveViewIndex = index;
            //  lnkBrowse.Visible = false;
            //  lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    ChangeMultiView2(0);
                    break;
            }
        }

        private void ChangeMultiView2(int index)
        {
            MultiView2.ActiveViewIndex = index;
            //  lnkBrowse.Visible = false;
            //  lnkMaintain.Visible = false;
            ImageButton1.ImageUrl = "~/Manager/images/p11_1.gif";
            ImageButton2.ImageUrl = "~/Manager/images/p12_1.gif";
            ImageButton3.ImageUrl = "~/Manager/images/p13_1.gif";
            ImageButton4.ImageUrl = "~/Manager/images/p14_1.gif";
            switch (index)
            {
                case 0:
                    ImageButton1.ImageUrl = "~/Manager/images/p11_2.gif";
                   break;
                case 1:
                   ImageButton2.ImageUrl = "~/Manager/images/p12_2.gif";
                   break;
                case 2:
                   ImageButton3.ImageUrl = "~/Manager/images/p13_2.gif";
                   break;
                case 3:
                   ImageButton4.ImageUrl = "~/Manager/images/p14_2.gif";
                   break;
            }
        }


        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            GridViewPagers(pnlPagers, mGV.PageCount, 1, mGV.PageIndex);
            PageChoiceSet(mGV);
        }
        #endregion

        #region 控制 GridView 數字分頁
        private void GridViewPagers(Panel pnl, int PageCounts, int StartValue, int pageIndex)
        {
            pnl.Controls.Clear();
            Panel pnlH = new Panel();
            pnlH.Height = 5;
            pnl.Controls.Add(pnlH);

            LinkButton lbtnPre = new LinkButton();
            lbtnPre.ID = "lbtnN_pre";
            lbtnPre.Text = "上一頁";
            lbtnPre.CommandName = "Prev";
            lbtnPre.Command += lbtnPageCtrl_Command;
            pagerLbtnStyle(lbtnPre);
            if (pageIndex == 0)
            {
                lbtnPre.Visible = false;
            }

            pnl.Controls.Add(lbtnPre);

            Label lbl_ = new Label();
            lbl_.Text = "";
            lbl_.Width = 7;
            pnl.Controls.Add(lbl_);

            if (pageIndex < 6 && (PageCounts - 1 - pageIndex) < 6)
            {
                createLbtn(0, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex >= 6 && (PageCounts - 1 - pageIndex) >= 6)
            {
                createLbtn(0, 1, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(pageIndex - 3, pageIndex + 3, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(PageCounts - 2, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex >= 6 && (PageCounts - 1 - pageIndex) < 6)
            {
                createLbtn(0, 1, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(pageIndex - 3, PageCounts - 1, pageIndex, pnl);
            }
            if (pageIndex < 6 && (PageCounts - 1 - pageIndex) >= 6)
            {
                createLbtn(0, pageIndex + 3, pageIndex, pnl);
                createLbl(pnl);
                createLbtn(PageCounts - 2, PageCounts - 1, pageIndex, pnl);
            }

            LinkButton lbtnNext = new LinkButton();
            lbtnNext.ID = "lbtnN_Next";
            lbtnNext.Text = "下一頁";
            lbtnNext.CommandName = "Next";
            lbtnNext.Command += lbtnPageCtrl_Command;
            pagerLbtnStyle(lbtnNext);
            if (pageIndex == PageCounts - 1)
            {
                lbtnNext.Visible = false;
            }
            pnl.Controls.Add(lbtnNext);
        }
        private void createLbtn(int s, int e, int pageIndex, Panel pnl)
        {
            for (int ls = s; ls <= e; ls++)
            {
                LinkButton lbtn = new LinkButton();
                lbtn.ID = string.Format("lbtn_p{0}", ls.ToString());
                lbtn.Text = (ls + 1).ToString();
                pagerLbtnStyle(lbtn);
                if (ls == pageIndex)
                {
                    lbtn.Enabled = false;
                    lbtn.Font.Underline = false;
                    lbtn.ForeColor = System.Drawing.Color.FromName("#FF6699");
                }
                else
                {
                    lbtn.Font.Underline = true;
                    lbtn.Click += lbtnPagers_Click;
                }
                pnl.Controls.Add(lbtn);

                Label lbl = new Label();
                lbl.Text = "";
                lbl.Width = 7;
                pnl.Controls.Add(lbl);
            }
        }
        private void createLbl(Panel pnl)
        {
            Label lbl_piont = new Label();
            lbl_piont.Text = "…";
            lbl_piont.ForeColor = System.Drawing.Color.FromName("#333333");
            lbl_piont.Font.Size = 11;
            lbl_piont.Font.Name = "Arial";
            pnl.Controls.Add(lbl_piont);

            Label lbl = new Label();
            lbl.Text = "";
            lbl.Width = 7;
            pnl.Controls.Add(lbl);
        }
        private LinkButton pagerLbtnStyle(LinkButton lbtn)
        {
            //lbtn.Width = 20;
            //lbtn.BackColor = System.Drawing.Color.FromName("#C8DAEC");
            //lbtn.BorderColor = System.Drawing.Color.FromName("#CCCCCC");
            lbtn.ForeColor = System.Drawing.Color.FromName("#336699");
            //lbtn.BorderWidth = 1;
            lbtn.Font.Size = 11;
            //lbtn.Font.Bold = true;
            lbtn.Font.Name = "Arial";

            return lbtn;
        }
        protected void GridView_Load(object sender, EventArgs e)
        {
            GridViewPagers(pnlPagers, ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageCount, 1, ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex);
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ViewState["dataCount"] = count;
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        //下拉是分頁
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        //數字分頁
        protected void lbtnPagers_Click(object sender, EventArgs e)
        {
            ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = int.Parse(((LinkButton)sender).ID.Replace("lbtn_p", ""));
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = int.Parse(((LinkButton)sender).ID.Replace("lbtn_p", ""));
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //string iStatus, string CompanyNo, string Master, string DispClass, string Saled, string Channel, string DispMark, string InsName, string Keyword, string sortExpression, int startRowIndex, int maximumRows
            //無搜尋者刪除
            if (e.InputParameters.Contains("iStatus")) return;
        //    e.InputParameters.Add("iStatus", ddlSh_ComKind.SelectedValue.Trim());
            e.InputParameters.Add("iStatus", ddlSh_stop.SelectedValue.Trim());
            e.InputParameters.Add("CompanyNo", ddlSh_Company.SelectedValue.Trim());
            e.InputParameters.Add("Master", ddlSh_kind.SelectedValue.Trim());
            e.InputParameters.Add("DispClass", ddlSh_Type.SelectedValue.Trim());
            e.InputParameters.Add("Saled", tabcSale.Checked ? "TABC" : "null");
            e.InputParameters.Add("Channel", tabcSale.Checked ? "TABC" : "");
            e.InputParameters.Add("DispMark", txtSh_PdtNo.Text.Trim());
            e.InputParameters.Add("InsName", txtSh_PdtName.Text.Trim());
            e.InputParameters.Add("Keyword", txtSh_KeyWord.Text.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (dataVaild() != "")
            {
                MainControls.showMsg(this, dataVaild());
            }
            else
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
            }
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_PdtNo.Text = string.Empty;
            txtSh_PdtName.Text = string.Empty;
            txtSh_KeyWord.Text = string.Empty;
         //   txt_emNo_q.Text = string.Empty;
         //   chkHIS.Checked = false;
         //   GridView1.Sort("edDate", SortDirection.Descending);
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        #region 結轉超過30天以上員工維護記錄至暫存
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            RServiceProvider rsp = DataPdInfo.PdInfoDelete();
            if (rsp.Result)
                MainControls.showMsg(this, "結轉成功!!!");
            else
                MainControls.showMsg(this, rsp.ReturnMessage);
            GridView1.DataBind();
        }
        #endregion

        #region 匯出EXCEL
        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            DataTable dt = DataPdInfo.PdInfoReader(ddlSh_ComKind.SelectedValue.Trim(), ddlSh_Company.SelectedValue.Trim(), ddlSh_kind.SelectedValue.Trim(), ddlSh_Type.SelectedValue.Trim(), ddlSh_stop.SelectedValue.Trim(), tabcSale.Checked ? "TABC" : "null", txtSh_PdtNo.Text.Trim(), txtSh_PdtName.Text.Trim(), txtSh_KeyWord.Text.Trim(), (string)ViewState["sortExpression"], 0, (int)ViewState["dataCount"]).Tables[0];
            if (dt.Rows.Count > 0)
                ExportControls.DataTableToExcelbyNPOI(dt, string.Format("資料異動紀錄_{0}", DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString()));
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }
        #endregion

        protected void ddlSh_ComKind_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataPdInfo.CompanyDropDownList(ddlSh_Company, ddlSh_ComKind.SelectedValue);

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
                        //填入各欄位資料至欄位元件
            String CompanyNo = "";
            String Mark = "";
            String Channel = "";
            String CompanyName = "";
            String MasterName = "";
            String DispMark = "";
            String InsName = "";
            
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "CompanyNo":
                            CompanyNo = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                   //     case "class":
                   //         MainControls.ddlIndexSelectValue(ddl_class, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                   //         break;
                        case "Mark":
                            Mark = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "Channel":
                            Channel = Server.HtmlDecode(gvr.Cells[i].Text);
                            break;
                        case "CompanyName":
                            CompanyName = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "MasterName":
                            MasterName = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "DispMark":
                            DispMark = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "InsName":
                            InsName = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            ComSname1.Text = CompanyName;
            kindname.Text = MasterName;
            PdtCode.Text = DispMark;
            PdtName.Text = InsName;
            ChangeMultiView(1);
            EX_DataAPI ed = new EX_DataAPI();
            DataTable dt = new DataTable();
            dt.TableName = "InsFullInfo";

            string InsItem = ed.GetInsFullInfo(CompanyNo, Mark);
            //iStatus|CmpyNo|yyyyMMddHH|Channel|IP
            InsItem = "[" + InsItem + "]";
            if (InsItem != "[]" && InsItem.Length >0)
            {
                //    {
                //"UrlCmpy": "TW",
                //"UrlMark": "050",
                //"Intrude": "<p class=\"Title\">* 停售 *<br>台壽國民儲蓄保險</p>\n\n<span class=\"IntrudeItem\">●商品特色</span>\n<p></p>",
                //"Premiums": null,
                //"ItemUnit": "萬元",
                //"isPDFLaw": false,
                //"PDFUrl": null,
                //"KeyTitles": "在特定期間或期滿且被保險人仍生存時，可領取生存保險金。可領取的金額與時間因保單而異。",
                //"LawDisplay": "    \ufeff\n<p class=\"Title\">台灣人壽十年滿期國民儲蓄保險保險單條款\n</p>",
                //"keyword": "儲蓄/養老/還本",
                //"FacebookTitle": "* 停售 *<br>台壽國民儲蓄保險",
                //"FacebookDescription": "    * 停售 *台壽國民儲蓄保險  ●商品特色"
                //}
                dt = JsonConvert.DeserializeObject<DataTable>(InsItem);
                dt.TableName = "InsFullInfo";
                Literal1.Text = dt.Rows[0]["Premiums"].ToString();
                Literal2.Text = dt.Rows[0]["KeyTitles"].ToString().Replace(";","</br>");//特色
                Literal3.Text = dt.Rows[0]["Intrude"].ToString();//簡介
                if (!Convert.ToBoolean(dt.Rows[0]["isPDFLaw"]))
                    Literal4.Text = dt.Rows[0]["LawDisplay"].ToString();//條款
                else
                {
                    string LawPdfurl = ed.GetLawPdfApi(dt.Rows[0]["LawDisplay"].ToString());
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<iframe src=" + LawPdfurl + " ");
                    //fix height and width
                    sb.Append("width=800px height=500px");
                    sb.Append("<View PDF: <a href=" + LawPdfurl + "</a></p> ");
                    sb.Append("</iframe>");
                    Literal4.Text = sb.ToString();
                }

                List<string> sqllst = new List<string>();
                sqllst.Add("1=1");
                //sqllst.Add("TABCProjCode = '0' ");
                //  sqllst.Add("ComSName='" + company + "'");
                sqllst.Add("TABC_sno='" + DispMark + "'");
                sqllst.Add("TABC_FILE_KIND in ('1','2','3','4') ");
                string mSql = "";
                mSql = "SELECT Top 100 TABC_PRODUCT_DOC.TABC_sno, TABC_FILE_No, TABC_FILE_KIND, TABC_FILE_PATH, TABC_FILE_NAME, TABCPdtName, TABCPdtKind, TABCPdtCode, TABCPdtYear, ComSName_GT, ComCode_GT, ComKind_GT, PlanCmpy, PlanMark, GoupCmpy, GoupMark "+
" FROM TABC_PRODUCT_DOC inner join TABC_PRODUCT_LIST on TABC_PRODUCT_DOC.TABC_sno=TABC_PRODUCT_LIST.TABC_sno "+
" inner join  TABC_COMPANY_MAP on TABC_PRODUCT_LIST.TABCComSName=TABC_COMPANY_MAP.TCM_ComCode "+
" inner join  PlanCodeMap on TABC_PRODUCT_LIST.TABCPdtCode=PlanCodeMap.PlanMark "+
                 "WHERE 1=1 and GoupCmpy='" + CompanyNo + "' and GoupMark='" + DispMark + "' and TABC_FILE_KIND in ('1','2','3','4') " +
" ORDER BY TABC_PRODUCT_DOC.TABC_sno desc, TABC_FILE_KIND";
              //  mSql = "select top 100 * from TABC_PRODUCT_DOC "; // where {0} order by TABCPdtCode", string.Join(" and ", sqllst.ToArray()));
              //  mSql = string.Format(mSql + " where {0} order by TABC_FILE_KIND ", string.Join(" and ", sqllst.ToArray()));
                DataSet pdt_tabcPdtDoc = new DataSet();

                pdt_tabcPdtDoc = SqlAccess.SqlcommandExecute("0", mSql);
                pdt_tabcPdtDoc.Tables[0].TableName = "pdt_tabcPdtDoc";
              //  string Pdfurl = ed.GetLawPdfApi(dt.Rows[0]["PDFUrl"].ToString());
                if (pdt_tabcPdtDoc.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drs = pdt_tabcPdtDoc.Tables[0].Select("TABC_FILE_KIND='1'");
                    if (drs.Length>0)
                        ImageButton5.Attributes.Add("onclick", "script:window.open('" + drs[0]["TABC_FILE_PATH"].ToString() + "');");
                    drs = pdt_tabcPdtDoc.Tables[0].Select("TABC_FILE_KIND='2'");
                    if (drs.Length > 0)
                        ImageButton6.Attributes.Add("onclick", "script:window.open('" + drs[0]["TABC_FILE_PATH"].ToString() + "');");
                    drs = pdt_tabcPdtDoc.Tables[0].Select("TABC_FILE_KIND='3'");
                    if (drs.Length > 0)
                        ImageButton7.Attributes.Add("onclick", "script:window.open('" + drs[0]["TABC_FILE_PATH"].ToString() + "');");
                    drs = pdt_tabcPdtDoc.Tables[0].Select("TABC_FILE_KIND='4'");
                    if (drs.Length > 0)
                        ImageButton8.Attributes.Add("onclick", "script:window.open('" + drs[0]["TABC_FILE_PATH"].ToString() + "');");
                }
                string filter = "TABC_sno ='" + DispMark + "' and  PremiumType <> '2'";                    
                DataTable dtCPdtList = DataPdInfo.GetTable(string.Format("select * from HH_CommissionRate where {0} order by RecNo", filter));
                StringBuilder sbCRate1 = new StringBuilder("");
                StringBuilder sbCRate2 = new StringBuilder("");
                StringBuilder sbCRate3 = new StringBuilder("");
                for (int i = 0; i < dtCPdtList.Rows.Count; i++)
                {
                sbCRate1.AppendLine("<tr class='table_list01'  style='background-color:#FFFFFF;height:25px'>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+(i + 1).ToString()+"</td>");
                sbCRate1.AppendLine("<td class='text02' style='border: 1px solid; border-right: 1px solid; text-align: left;color:#000000'>"+dtCPdtList.Rows[i]["PremiumType"].ToString()+"</td>");
                sbCRate1.AppendLine("<td class='text02' style='border: 1px solid; border-right: 1px solid; text-align: left;color:#000000'>"+dtCPdtList.Rows[i]["Age"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["Year"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY1"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY2"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY3"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY4"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY5"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY6"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY7"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY8"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY9"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CY10"].ToString()+"</td>");
                sbCRate1.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["CSupply"].ToString()+"</td>");
                sbCRate1.AppendLine("</tr>");

                sbCRate2.AppendLine("<tr class='table_list01'  style='background-color:#FFFFFF;height:25px'>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+(i + 1).ToString()+"</td>");
                sbCRate2.AppendLine("<td class='text02' style='border: 1px solid; border-right: 1px solid; text-align: left;color:#000000'>"+dtCPdtList.Rows[i]["PremiumType"].ToString()+"</td>");
                sbCRate2.AppendLine("<td class='text02' style='border: 1px solid; border-right: 1px solid; text-align: left;color:#000000'>"+dtCPdtList.Rows[i]["Age"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["Year"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY1"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY2"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY3"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY4"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY5"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY6"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY7"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY8"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY9"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SY10"].ToString()+"</td>");
                sbCRate2.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["SSupply"].ToString()+"</td>");
                sbCRate2.AppendLine("</tr>");

                sbCRate3.AppendLine("<tr class='even'>");
                sbCRate3.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["Percentage"].ToString()+"</td>");//計績比例
                sbCRate3.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["YearEnd_bonuses"].ToString()+"</td>");//年終獎金
                sbCRate3.AppendLine("<td style='border: 1px solid; border-right: 1px solid; text-align: center;color:#000000'>"+dtCPdtList.Rows[i]["Continue_bonuses"].ToString()+"</td>");//繼續率獎金
                sbCRate3.AppendLine("</tr>");

                }
                StringBuilder sbCRate = new StringBuilder("");
                sbCRate.AppendLine("<div class='tab-pane fade active in' id='tab1'>");
                sbCRate.AppendLine("                    <div class='searchform'>");
                sbCRate.AppendLine("                        <table style='width: 100%;'>");
                sbCRate.AppendLine("                            <tbody><tr>");
                sbCRate.AppendLine("                                <td><span>保險公司:</span><label id='ComSname2'>" + CompanyName + "</label>&nbsp;</td>");
                sbCRate.AppendLine("                                <td><span>約別:</span><label id='kindname2'>" + MasterName + "</label>&nbsp;</td>");
                sbCRate.AppendLine("                                <td><span>險種代號:</span><label id='PdtCode2'>" + DispMark + "</label>");
                sbCRate.AppendLine("                                </td>");
                sbCRate.AppendLine("                                <td><span>險種名稱:</span><label id='PdtName2'>" + InsName + "</label>");
                sbCRate.AppendLine("                                </td>");
                sbCRate.AppendLine("");
                sbCRate.AppendLine("                            </tr>");
                sbCRate.AppendLine("                        </tbody></table>                    ");
                sbCRate.AppendLine("                    </div>");
                sbCRate.AppendLine("                    ");
                sbCRate.AppendLine("                    <div class='alert '>");
                sbCRate.AppendLine("                        <table style='width: 100%;'>");
                sbCRate.AppendLine("                            <tbody><tr>");
                sbCRate.AppendLine("                                <td><strong><span class='text02' style='color: Red;'>1.本系統揭露的商品佣金率、特別津貼、獎勵計績比例、年終獎金與繼續率獎金僅供參考，實際發放以正式公文為準。</span></strong></td>");
                sbCRate.AppendLine("                            </tr>");
                sbCRate.AppendLine("                            <tr>");
                sbCRate.AppendLine("                                <td><strong><span class='text02' style='color: Red;'>2.部分商品之特別津貼(僅提供前10年之資訊)、年終獎金與繼續率獎金，民國100年3月以前因系統未建檔，故無法提供查詢，實際發放以正式公文為準。</span></strong></td>");
                sbCRate.AppendLine("                            </tr>                            ");
                sbCRate.AppendLine("                        </tbody></table>");
                sbCRate.AppendLine("                        <table id='rate1' style='width: 100%; height: 100px; font-size: 14px;' class='table table-bordered'>");
                sbCRate.AppendLine("                            <thead>");
                sbCRate.AppendLine("                                <tr class='tr_title success' style='height: 40px;'>");
                sbCRate.AppendLine("                                    <th class='locked' style='background-color: #dff0d8;width: 100%; text-align: center;' colspan='15'><span style='font-size: medium;'>佣金率</span>  保單年度</th>");
                sbCRate.AppendLine("                                </tr>");
                sbCRate.AppendLine("                                <tr class='tr_title' style='height: 40px;'>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>No</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>保費類別</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>年齡</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>繳費年期</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>FY</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>2Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>3Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>4Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>5Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>6Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>7Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>8Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>9Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>10Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>補充說明</th>");
                sbCRate.AppendLine("                                </tr>");
                sbCRate.AppendLine("                            </thead>");
                sbCRate.AppendLine("                            <tbody>");
                sbCRate.AppendLine(sbCRate1.ToString());
                sbCRate.AppendLine("                            </tbody>");
                sbCRate.AppendLine("                        </table>");
                sbCRate.AppendLine("                        <hr>");
                sbCRate.AppendLine("                        <table id='rate2' style='width: 100%; height: 100px; font-size: 14px;' class='table table-bordered'>");
                sbCRate.AppendLine("                            <thead>");
                sbCRate.AppendLine("                                <tr class='tr_title info' style='height: 40px;'>");
                sbCRate.AppendLine("                                    <th class='locked' style='background-color: #d9edf7;width: 100%; text-align: center;' colspan='15'><span style='font-size: medium;'>特別津貼</span>  保單年度</th>");
                sbCRate.AppendLine("                                </tr>");
                sbCRate.AppendLine("                                <tr class='tr_title' style='height: 40px;'>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>No</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>保費類別</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>年齡</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>繳費年期</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>FY</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>2Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>3Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>4Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>5Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>6Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>7Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>8Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>9Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>10Y</th>");
                sbCRate.AppendLine("                                    <th class='locked' style='text-align: center;'>補充說明</th>");
                sbCRate.AppendLine("                                </tr>");
                sbCRate.AppendLine("                            </thead>");
                sbCRate.AppendLine("                            <tbody>");
                sbCRate.AppendLine(sbCRate2.ToString());
                sbCRate.AppendLine("                            </tbody>");
                sbCRate.AppendLine("                        </table>");
                sbCRate.AppendLine("                        <hr>");
                sbCRate.AppendLine("                        <table id='rate3' class='rounded-corner' style='font-size: 14px; width: 300px; text-align: center;'>");
                sbCRate.AppendLine("                            <thead>");
                sbCRate.AppendLine("                                <tr>");
                sbCRate.AppendLine("                                    <th>獎勵計績比例</th>");
                sbCRate.AppendLine("                                    <th>年終獎金</th>");
                sbCRate.AppendLine("                                    <th>繼續率獎金</th>");
                sbCRate.AppendLine("                                </tr>");
                sbCRate.AppendLine("                            </thead>");
                sbCRate.AppendLine("                            <tbody>");
                sbCRate.AppendLine(sbCRate3.ToString());
                sbCRate.AppendLine("                            </tbody>");
                sbCRate.AppendLine("                        </table>");
                sbCRate.AppendLine("                    </div>");
                sbCRate.AppendLine("                </div>");
                Literal5.Text = sbCRate.ToString();

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            ChangeMultiView(0);
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ChangeMultiView2(0);

        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            ChangeMultiView2(1);

        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            ChangeMultiView2(2);

        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            ChangeMultiView2(3);

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            ChangeMultiView3(2);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ChangeMultiView3(0);

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            ChangeMultiView3(1);

        }



    }
}