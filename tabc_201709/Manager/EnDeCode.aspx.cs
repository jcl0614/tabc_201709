﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EnDeCode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //預設顯示
                Panel1.Visible = false;
                Panel2.Visible = true;
            }
        }
        protected void btn_enAES_Click(object sender, EventArgs e)
        {
            if (txt_enAES.Text.Trim() == "")
                MainControls.showMsg(this, "必須輸入!");
            else
            {
                switch (ddl_type.SelectedValue)
                {
                    case "1":
                        txt_enAESr.Text = Lib.GetAesChinese(txt_enAES.Text.Trim(), DataSysValue.Password);
                        break;
                    case "2":
                        txt_enAESr.Text = Lib.GetAesChinese(txt_enAES.Text.Trim(), DataSysValue.Connection);
                        break;
                    case "3":
                        txt_enAESr.Text = Lib.GetAesChinese(txt_enAES.Text.Trim(), DataSysValue.eMail);
                        break;
                    default:
                        MainControls.showMsg(this, "請選擇加密方式!");
                        break;
                }
            }
        }
        protected void btn_deAES_Click(object sender, EventArgs e)
        {
            try
            {
                if (txt_deAES.Text.Trim() == "")
                    MainControls.showMsg(this, "必須輸入!");
                else
                {
                    switch (ddl_type0.SelectedValue)
                    {
                        case "1":
                            txt_deAESr.Text = Lib.PuchAesChinese(txt_deAES.Text.Trim(), DataSysValue.Password);
                            break;
                        case "2":
                            txt_deAESr.Text = Lib.PuchAesChinese(txt_deAES.Text.Trim(), DataSysValue.Connection);
                            break;
                        case "3":
                            txt_deAESr.Text = Lib.PuchAesChinese(txt_deAES.Text.Trim(), DataSysValue.eMail);
                            break;
                        default:
                            MainControls.showMsg(this, "請選擇加密方式!");
                            break;
                    }
                }
            }
            catch
            {
                MainControls.showMsg(this, "解密失敗!");
            }
        }
        protected void btn_enAES0_Click(object sender, EventArgs e)
        {
            if (txt_deMD5.Text.Trim() == "")
                MainControls.showMsg(this, "必須輸入!");
            else
                txt_enMD5.Text = Lib.GetMD5(txt_deMD5.Text.Trim());
        }

        protected void txt_PWD_TextChanged(object sender, EventArgs e)
        {
            if (Lib.GetMD5(txt_PWD.Text) != DataEmployee.baseDataReader(Session["emID"].ToString()).Tables[0].Rows[0]["emPWD"].ToString())
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
            }
            else
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
            }
        }
    }
}