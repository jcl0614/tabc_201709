﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EmGroup : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
            PagerModul2(GridView2, ObjectDataSource2, ddlPageRow1, ddlPageChange1, labGridViewRows1, lbtnPageCtrlFirst1, lbtnPageCtrlPrev1, lbtnPageCtrlNext1, lbtnPageCtrlLast1, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_name);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    this.SetFocus(txt_name);
                    Panel_Form.DefaultButton = btnEdit.ID;
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 760);
                MainControls.GridViewStyleCtrl(GridView2, 660);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }
        #region 資料繫結
        private void dataBind()
        {
            DataEmGroup.DataColumn(this.GridView1);
            DataEmGroupLimit.EmGroupLimitDataColumn(this.GridView2);
        }
        #endregion

        #region 下拉選單設定
        private void setddlChoices()
        {
            ddlSh_enable.SelectedIndex = 0;
            //權限
            DataEmGroupLimit.FunctionReaderCheckBoxList(this.cbl_Function);
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_name.Text.Trim() == "")
                sbError.Append("●「群組名稱」必須輸入!<br>");
            else
                if (txt_name.Text.Trim().Length > 10)
                    sbError.Append("●「群組名稱」長度不得大於10個字元!<br>");

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "id":
                            hif_id.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "name":
                            txt_name.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "enable":
                            cbox_enable.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            break;
                        case "ipLimit":
                            txt_ipLimit.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                if (lbl_Msg.Text.Trim().Length > 0) return;
                //元件資料轉成Hashtable
                ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmGroup.GetSchema(), dataToHashtable()));
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        RServiceProvider rsp = DataEmGroup.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            hif_id.Value = DataEmGroup.MAX_id();
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        RServiceProvider rsp = DataEmGroup.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        RServiceProvider rsp = DataEmGroup.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp1 = DataEmGroupLimit.gpIdEmGroupLimitDelete(hif_id.Value);
                        if (rsp.Result && rsp1.Result) //執行成功
                        {
                            GridView1.DataBind();
                            maintainButtonEnabled("");
                            clearControlContext();
                            lbl_Msg.Text = "刪除成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable, 供異動資料使用
        private Hashtable dataToHashtable()
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("id", hif_id.Value);
            hsData.Add("name", Lib.FdVP(txt_name.Text.Trim()));
            hsData.Add("enable", cbox_enable.Checked ? "true" : "false");
            hsData.Add("ipLimit", Lib.FdVP(txt_ipLimit.Text.Trim()));

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_id.Value = string.Empty;
            txt_name.Text = string.Empty;
            txt_ipLimit.Text = string.Empty;
            cbox_enable.Checked = true;
            lbl_Msg.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    //選單主分類
                    DataMenuLayer.menuLayerReaderDropDownList(ddl_muLayer, true, "全部");
                    //選單子分類
                    DataMenu.menuReaderDropDownList(ddl_muNo, false, ddl_muLayer.SelectedValue);
                    //重載員工權限資料
                    GridView2.DataBind();
                    lbl_Msg1.Text = string.Empty;
                    break;
            }
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            btnInsert.Enabled = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    btnInsert.Enabled = true & sysValue.Authority.Append;
                    this.SetFocus(txt_name);
                    break;
                default:
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    btnInsert.Enabled = false;
                    this.SetFocus(txt_name);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource1_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if(((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        protected void GridView_RowDataBound2(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "編輯")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).CssClass = "btnEdit";
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[7].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }
                if (((Button)e.Row.Cells[7].Controls[2]).Text == "刪除")
                {
                    ((Button)e.Row.Cells[7].Controls[2]).CssClass = "btnDelete2";
                    ((Button)e.Row.Cells[7].Controls[2]).Enabled = sysValue.Authority.Delete;
                    ((Button)e.Row.Cells[7].Controls[2]).Style.Add("cursor", sysValue.Authority.Delete ? "pointer" : "not-allowed");
                }
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "更新")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).CssClass = "btnSave2";
                    ((Button)e.Row.Cells[7].Controls[2]).CssClass = "btnCancel2";
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[7].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("name")) return;
            e.InputParameters.Add("name", Lib.FdVP(txtSh_name.Text).Trim());
            e.InputParameters.Add("enable", ddlSh_enable.SelectedValue.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_name.Text = string.Empty;
            ddlSh_enable.SelectedIndex = 0;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        #region 分頁模組

        private void PagerModul2(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls2"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound2;
            grv.DataBound += GridView2_DataBound;
            objDS.Selected += ObjectDataSource2_Selected;
            objDS.Selecting += ObjectDataSource2_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows1_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex1_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl1_Command;
            lbtnPagePre.Command += lbtnPageCtrl1_Command;
            lbtnPageNext.Command += lbtnPageCtrl1_Command;
            lbtnPageLast.Command += lbtnPageCtrl1_Command;
        }

        bool chagePageRow1 = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls2"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageIndex = 0;

            chagePageRow1 = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices1(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet1(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource2_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //預防無資料時分頁控制按鈕預設會啟用
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls2"])[0]), (DropDownList)((object[])ViewState["PagerModulControls2"])[3]);

            if (e.ReturnValue is DataSet)
                ViewState["table1"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls2"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow1)
                {
                    chagePageRow1 = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls2"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls2"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls2"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex;
                PageChoiceSet1(((GridView)((object[])ViewState["PagerModulControls2"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl1_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls2"])[0]), e.CommandName);
            PageChoiceSet1(((GridView)((object[])ViewState["PagerModulControls2"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet1(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls2"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Style.Add("cursor", "default");

                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");

                }
                if (((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls2"])[0]), (DropDownList)((object[])ViewState["PagerModulControls2"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource2_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (e.InputParameters.Contains("gpId")) return;
            e.InputParameters.Add("gpId", hif_id.Value);
            e.InputParameters.Add("muLayer", ddl_muLayer.SelectedValue);
        }
        #endregion

        #endregion

        #region 權限異動按鈕
        protected void btnInsert_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (ddl_muNo.SelectedIndex == -1)
            {
                lbl_Msg1.Text = "「系統功能選單」必須選取!";
                return;
            }
            else
                if (!DataEmGroupLimit.gpIdLimitValid(hif_id.Value, ddl_muNo.SelectedValue))
                {
                    lbl_Msg1.Text = "此「系統功能選單」權限已加入!";
                    return;
                }
            switch (e.CommandName)
            {
                case "Insert": //新增
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmGroupLimit.GetSchema(), dataToHashtableForLimit()));
                    RServiceProvider rsp = DataEmGroupLimit.EmGroupLimitAppend(sysValue.emNo, sysValue.ProgId, ds);
                    //員工維護系統資料記錄
                    DataemDaily.emDailyInsert("0", sysValue.emNo, sysValue.ProgId, ds, DataemDaily.MaintainStatus.Insert);
                    if (rsp.Result) //執行成功
                    {
                        GridView2.DataBind();
                        lbl_Msg1.Text = "新增權限成功!";
                    }
                    else
                        lbl_Msg1.Text = rsp.ReturnMessage;
                    break;
            }
        }
        private Hashtable dataToHashtableForLimit()
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("id", "");
            hsData.Add("gpId", hif_id.Value);
            hsData.Add("muNo", ddl_muNo.SelectedItem.Value);
            hsData.Add("gpInsert", cbl_Function.Items.FindByValue("gpInsert").Selected);
            hsData.Add("gpUpdate", cbl_Function.Items.FindByValue("gpUpdate").Selected);
            hsData.Add("gpSelect", cbl_Function.Items.FindByValue("gpSelect").Selected);
            hsData.Add("gpDelete", cbl_Function.Items.FindByValue("gpDelete").Selected);
            return hsData;
        }
        #endregion

        #region 選單分類變更事件
        protected void ddl_muLayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //選單子分類
            DataMenu.menuReaderDropDownList(ddl_muNo, false, ddl_muLayer.SelectedValue);
            GridView2.DataBind();
        }
        #endregion

        #region 更新權限資料
        protected void ObjectDataSource2_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            string muNo = GridView2.SelectedRow.Cells[1].Text;
            string id = GridView2.SelectedRow.Cells[0].Text;
            if (e.InputParameters.Contains("id")) return;
            {
                e.InputParameters.Insert(0, "id", id);
                e.InputParameters.Insert(1, "emNo", sysValue.emNo);
                e.InputParameters.Insert(2, "progId", sysValue.ProgId);
                e.InputParameters.Insert(3, "muNo", muNo);
            }
        }
        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView2.SelectedIndex = e.RowIndex;
        }
        #endregion

        #region 刪除權限資料
        protected void ObjectDataSource2_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            string muNo = GridView2.SelectedRow.Cells[1].Text;
            string id = GridView2.SelectedRow.Cells[0].Text;
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.InputParameters.Contains("id")) return;
            {
                e.InputParameters.Add("id", id);
                e.InputParameters.Add("emNo", sysValue.emNo);
                e.InputParameters.Add("progId", sysValue.ProgId);
                e.InputParameters.Add("muNo", muNo);
            }

        }
        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView2.SelectedIndex = e.RowIndex;
        }
        #endregion

        #region command field及button field加確認屬性的方法 && GridView 編輯時，調整欄寬
        protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (e.Row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)
                //    || e.Row.RowState == DataControlRowState.Edit
                //    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit)
                //    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit | DataControlRowState.Alternate)
                //    )
                //{
                //    //Width屬性是Unit類型，不能直接給整數值
                //    //Unit.Pixel():依像表設定絕對寬度；Unit.Percent():依百分比設定寬度
                //    ((TextBox)e.Row.Cells[0].Controls[0]).Width = Unit.Pixel(140); //預刊日期
                //    ((TextBox)e.Row.Cells[1].Controls[0]).Width = Unit.Pixel(20); //刊登天數
                //}

                if (((Button)e.Row.Cells[7].Controls[2]).Text == "刪除")
                {
                    ((Button)e.Row.Cells[7].Controls[2]).Attributes.Add("onclick", "if(!window.confirm('確定要刪除嗎？')) return;");
                    ((Button)e.Row.Cells[7].Controls[2]).Enabled = sysValue.Authority.Delete;
                }
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "編輯")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                }
            }
        }
        #endregion

        #region 系統功能選單
        protected void ddl_muNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_Msg1.Text = string.Empty;
        }
        #endregion

    }
}