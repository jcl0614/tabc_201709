﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class EAttendTotAttRate : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EO_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_EO_Year);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_EO_Year);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataEAttendTot.DataColumn_AttRate(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EO_Unit"] = DataPhrase.DDL_TypeName(ddlSh_EO_Unit, true, "", "E008");
            ViewState["ddl_EO_Unit"] = DataPhrase.DDL_TypeName(ddl_EO_Unit, true, "請選擇", "E008");
            ViewState["ddl_EO_Unit0"] = DataPhrase.DDL_TypeName(ddl_EO_Unit0, true, "請選擇", "E008");
        }
        #endregion

        #region 資料驗証
        private string dataValid()
        {
            StringBuilder sbError = new StringBuilder();
            
            if (txt_EO_Year.Text.Trim() == "")
                sbError.Append("●「年度」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", txt_EO_Year.Text.Trim()))
                    sbError.Append("●「年度」格式錯誤!<br>");
                else
                {
                    if(int.Parse(txt_EO_Year.Text.Trim()) < 1911)
                        sbError.Append("●「年度」格式錯誤!<br>");
                }
            }
            if (txt_EO_Month.Text.Trim() == "")
                sbError.Append("●「月份」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^(0[1-9]|[1]?[0-2])$", txt_EO_Month.Text.Trim()))
                    sbError.Append("●「月份」格式錯誤!<br>");

            }
            if (ddl_EO_Unit.SelectedValue == "")
                sbError.Append("●「單位」必須選取!<br>");
            if (selPerson_AccID_EO_AccID.SelectedValue.Trim() == "")
                sbError.Append("●「人員」必須選取!<br>");
            if (txt_EO_ShouldAtt.Text.Trim() == "")
                sbError.Append("●「應出席」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_ShouldAtt.Text.Trim())))
                    sbError.Append("●「應出席」格式錯誤!<br>");
            }
            if (txt_EO_RealAtt.Text.Trim() == "")
                sbError.Append("●「實出席」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_RealAtt.Text.Trim())))
                    sbError.Append("●「實出席」格式錯誤!<br>");
            }
            if (txt_EO_AttRate.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_EO_AttRate.Text.Trim())))
                    sbError.Append("●「出席率」格式錯誤!<br>");
            }
            if (txt_EO_AttBonus.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_EO_AttBonus.Text.Trim())))
                    sbError.Append("●「加值」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        private string dataValid_batch()
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_EO_Year0.Text.Trim() == "")
                sbError.Append("●「年度」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[0-9]*$", txt_EO_Year0.Text.Trim()))
                    sbError.Append("●「年度」格式錯誤!<br>");
                else
                {
                    if (int.Parse(txt_EO_Year0.Text.Trim()) < 1911)
                        sbError.Append("●「年度」格式錯誤!<br>");
                }
            }
            if (txt_EO_Month0.Text.Trim() == "")
                sbError.Append("●「月份」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^(0[1-9]|[1]?[0-2])$", txt_EO_Month0.Text.Trim()))
                    sbError.Append("●「月份」格式錯誤!<br>");
            }
            if (ddl_EO_Unit0.SelectedValue == "")
                sbError.Append("●「單位」必須選取!<br>");
            if (txt_EO_ShouldAtt0.Text.Trim() == "")
                sbError.Append("●「應出席」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_ShouldAtt0.Text.Trim())))
                    sbError.Append("●「應出席」格式錯誤!<br>");
            }
            if (txt_EO_RealAtt0.Text.Trim() == "")
                sbError.Append("●「實出席」必須輸入!<br>");
            else
            {
                if (!MainControls.ValidDataType("^\\+?[1-9][0-9]*$", (txt_EO_RealAtt0.Text.Trim())))
                    sbError.Append("●「實出席」格式錯誤!<br>");
            }
            if (txt_EO_AttRate0.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_EO_AttRate0.Text.Trim())))
                    sbError.Append("●「出席率」格式錯誤!<br>");
            }
            if (txt_EO_AttBonus0.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_EO_AttBonus0.Text.Trim())))
                    sbError.Append("●「加值」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EO_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_UidNo":
                            hif_EO_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_Unit":
                            MainControls.ddlIndexSelectAttribute(ddl_EO_Unit, "TypeSubCode", ViewState["ddl_EO_Unit"] as DataTable, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "EO_Year":
                            txt_EO_Year.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_Month":
                            txt_EO_Month.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_AccID":
                            selPerson_AccID_EO_AccID.search(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            selPerson_AccID_EO_AccID.SelectedValue = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            hif_EO_AccID.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_ShouldAtt":
                            txt_EO_ShouldAtt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_RealAtt":
                            txt_EO_RealAtt.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_AttRate":
                            txt_EO_AttRate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EO_AttBonus":
                            txt_EO_AttBonus.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void selPerson_AccID_EO_AccID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_AccID_TextChanged(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_AccID_SearchButtonClick(object sender, EventArgs e)
        {

        }
        protected void selPerson_AccID_EO_AccID_RefreshButtonClick(object sender, EventArgs e)
        {
            selPerson_AccID_EO_AccID.search(hif_EO_AccID.Value);
            selPerson_AccID_EO_AccID.SelectedValue = hif_EO_AccID.Value;

        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataEAttendTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Copy": //複製
                    {
                        btnEdit.Visible = false;
                        btnCopy.Visible = false;
                        btnDelete.Visible = false;
                        btnAppend.Visible = true & sysValue.Authority.Append;
                        btnCancel.Visible = true;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        hif_EO_UidNo.Value = string.Empty;
                        this.SetFocus(txt_EO_Year);
                        lbl_Msg.Text = string.Empty;
                        MainControls.showMsg(this, "已複製欄位資料，請點選存檔進行儲存。");
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataEAttendTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("EO_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EO_UidNo", hif_EO_UidNo.Value);
            }
            hsData.Add("EO_Year", txt_EO_Year.Text.Trim());
            hsData.Add("EO_Month", txt_EO_Month.Text.Trim());
            hsData.Add("EO_Unit", (ViewState["ddl_EO_Unit"] as DataTable).Rows[ddl_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString());
            hsData.Add("EO_AccID", selPerson_AccID_EO_AccID.SelectedValue);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(selPerson_AccID_EO_AccID.SelectedValue);
            if (dr_sales != null)
            {
                hsData.Add("EO_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("EO_ShouldAtt", txt_EO_ShouldAtt.Text.Trim());
            hsData.Add("EO_RealAtt", txt_EO_RealAtt.Text.Trim());
            hsData.Add("EO_AttRate", txt_EO_AttRate.Text.Trim());
            hsData.Add("EO_AttBonus", txt_EO_AttBonus.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_batch(string EO_AccID)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", System.Guid.NewGuid().ToString("N"));
            hsData.Add("EO_UidNo", Session["UidNo"].ToString());
            hsData.Add("EO_Year", txt_EO_Year0.Text.Trim());
            hsData.Add("EO_Month", txt_EO_Month0.Text.Trim());
            hsData.Add("EO_Unit", (ViewState["ddl_EO_Unit0"] as DataTable).Rows[ddl_EO_Unit0.SelectedIndex - 1]["TypeSubCode"].ToString());
            hsData.Add("EO_AccID", EO_AccID);
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(EO_AccID);
            if (dr_sales != null)
            {
                hsData.Add("EO_Name", dr_sales["NAME"].ToString());
            }
            hsData.Add("EO_ShouldAtt", txt_EO_ShouldAtt0.Text.Trim());
            hsData.Add("EO_RealAtt", txt_EO_RealAtt0.Text.Trim());
            hsData.Add("EO_AttRate", txt_EO_AttRate0.Text.Trim());
            hsData.Add("EO_AttBonus", txt_EO_AttBonus0.Text.Trim());
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "A");

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("EO_TransNo", TransNo);
            hsData.Add("EO_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_EO_UidNo.Value = string.Empty;
            ddl_EO_Unit.SelectedIndex = -1;
            txt_EO_Year.Text = string.Empty;
            txt_EO_Month.Text = string.Empty;
            txt_EO_ShouldAtt.Text = string.Empty;
            txt_EO_RealAtt.Text = string.Empty;
            txt_EO_AttRate.Text = string.Empty;
            txt_EO_AttBonus.Text = string.Empty;
            selPerson_AccID_EO_AccID.Clear();
            lbl_Msg.Text = string.Empty;

            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            txtSh_AccID.Text = string.Empty;
            ddl_EO_Unit0.SelectedIndex = -1;
            txt_EO_Year0.Text = string.Empty;
            txt_EO_Month0.Text = string.Empty;
            txt_EO_ShouldAtt0.Text = string.Empty;
            txt_EO_RealAtt0.Text = string.Empty;
            txt_EO_AttRate0.Text = string.Empty;
            txt_EO_AttBonus0.Text = string.Empty;
            lbl_Msg0.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCopy.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnCopy.Visible = true & sysValue.Authority.Append;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EO_Year);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    this.SetFocus(txt_EO_Year);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[10].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[11].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[10].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[10].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[11].Text, GridView1.Rows[i].Cells[12].Text)));

                    //RServiceProvider rsp = DataEAttendTot.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataEAttendTot.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EO_Year")) return;
            e.InputParameters.Add("EO_Year", txtSh_EO_Year.Text.Trim());
            e.InputParameters.Add("EO_Month", txtSh_EO_Month.Text.Trim());
            if (ddlSh_EO_Unit.SelectedIndex > 0)
                e.InputParameters.Add("EO_Unit", (ViewState["ddlSh_EO_Unit"] as DataTable).Rows[ddlSh_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                e.InputParameters.Add("EO_Unit", "");
            e.InputParameters.Add("EO_AccID", txtSh_EO_AccID.Text.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EO_Unit.SelectedIndex = -1;
            txtSh_EO_Year.Text = string.Empty;
            txtSh_EO_Month.Text = string.Empty;
            txtSh_EO_AccID.Text = string.Empty;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion

        protected void lnkExcelSet_Click(object sender, EventArgs e)
        {
            string Sh_EO_Unit = "";
            if (ddlSh_EO_Unit.SelectedIndex > 0)
                Sh_EO_Unit = (ViewState["ddlSh_EO_Unit"] as DataTable).Rows[ddlSh_EO_Unit.SelectedIndex - 1]["TypeSubCode"].ToString();
            DataTable dt = DataEAttendTot.DataReader(
                Lib.FdVP(txtSh_EO_Year.Text).Trim(),
                Lib.FdVP(txtSh_EO_Month.Text).Trim(),
                Sh_EO_Unit,
                Lib.FdVP(txtSh_EO_AccID.Text).Trim(),
                0, 99999
                ).Tables[0];

            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("年度", typeof(string));
                dt_export.Columns.Add("月份", typeof(string));
                dt_export.Columns.Add("單位", typeof(string));
                dt_export.Columns.Add("業務員編號", typeof(string));
                dt_export.Columns.Add("業務員姓名", typeof(string));
                dt_export.Columns.Add("應出席", typeof(string));
                dt_export.Columns.Add("實出席", typeof(string));
                dt_export.Columns.Add("出席率(%)", typeof(string));
                dt_export.Columns.Add("加值(%)", typeof(string));
                dt_export.Columns.Add("歸屬處經理", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    string EO_Unit = "";
                    string DM_NAME = "";
                    DataRow dr_Phrase = DataPhrase.dataReader_TypeCode_TypeSubCode("E008", dr["EO_Unit"].ToString());
                    if (dr_Phrase != null)
                        EO_Unit = dr_Phrase["TypeName"].ToString();
                    DataRow dr_sales = Data_tabcSales.DataReader_AccID(dr["EO_AccID"].ToString());
                    if (dr_sales != null)
                    {
                        DataRow dr_sales2 = Data_tabcSales.DataReader_AccID(dr_sales["DM"].ToString());
                        if (dr_sales2 != null)
                        {
                            DM_NAME = dr_sales2["NAME"].ToString();
                        }
                    }
                    dt_export.Rows.Add(
                        dr["EO_Year"].ToString(),
                        dr["EO_Month"].ToString(),
                        EO_Unit,
                        dr["EO_AccID"].ToString(),
                        dr["EO_Name"].ToString(),
                        dr["EO_ShouldAtt"].ToString(),
                        dr["EO_RealAtt"].ToString(),
                        dr["EO_AttRate"].ToString(),
                        dr["EO_AttBonus"].ToString(),
                        DM_NAME
                        );
                }

                ExportControls.CreateCSVFile(Page, dt_export, string.Format("早會應出席資料_{0}.csv", DateTime.Now.ToString("yyyyMMddhhmmss")));
            }
            else
                MainControls.showMsg(this, "未查詢任何記錄!");
        }



        protected void btnApply_Command(object sender, CommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "search"://
                    DataTable dt = DataPerson.baseDataReader(true, txtSh_AccID.Text.Trim()).Tables[0];
                    lboxSur.Items.Clear();
                    foreach (DataRow dw in dt.Rows)
                    {
                        ListItem li = new ListItem();
                        li.Text = dw["PS_NAME"].ToString().Trim();
                        li.Value = dw["AccID"].ToString().Trim();
                        lboxSur.Items.Add(li);
                    }
                    break;
                case "refresh"://重新載入
                    MarkSortSetInit();
                    break;
                case "addAll"://全部加入功能按鈕
                    foreach (ListItem m_item in lboxSur.Items)
                    {
                        if (lboxObj.Items.IndexOf(m_item) == -1)
                        {
                            lboxObj.Items.Add(m_item);
                        }
                    }
                    lboxSur.Items.Clear();
                    if (lboxObj.Items.Count != 0)
                        lboxObj.SelectedIndex = 0;
                    break;
                case "removeAll"://全部移除功能按鈕
                    foreach (ListItem m_item in lboxObj.Items)
                    {
                        if (lboxSur.Items.IndexOf(m_item) < 0)
                            lboxSur.Items.Add(m_item);
                    }
                    lboxObj.Items.Clear();
                    if (lboxSur.Items.Count != 0)
                        lboxSur.SelectedIndex = 0;
                    break;
                case "add"://加入一個項目
                    int siSur = lboxSur.SelectedIndex;
                    ListItem itemSur = lboxSur.SelectedItem;
                    if (lboxSur.Items.Count > 0 && lboxSur.SelectedIndex != -1)
                    {
                        if (itemSur.Text.Trim() != "")
                        {
                            if (lboxObj.Items.IndexOf(itemSur) == -1)
                            {
                                lboxObj.Items.Add(itemSur);
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                                lboxSur.Items.Remove(itemSur);
                                if (lboxSur.Items.Count > siSur)
                                    lboxSur.SelectedIndex = siSur;
                                else
                                    lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            }
                        }
                    }
                    break;
                case "remove"://移除一個項目
                    int siObj = lboxObj.SelectedIndex;
                    ListItem itemObj = lboxObj.SelectedItem;
                    if (lboxObj.Items.Count > 0 && lboxObj.SelectedIndex != -1)
                    {
                        if (itemObj.Text.Trim() != "")
                        {
                            lboxSur.Items.Add(itemObj);
                            lboxSur.SelectedIndex = lboxSur.Items.Count - 1;
                            lboxObj.Items.Remove(itemObj);
                            if (lboxObj.Items.Count > siObj)
                                lboxObj.SelectedIndex = siObj;
                            else
                                lboxObj.SelectedIndex = lboxObj.Items.Count - 1;
                        }
                    }
                    break;
                case "Append"://儲存新顯示順序設定
                    lbl_Msg0.Text = dataValid_batch(); //資料格式驗証
                    if (lbl_Msg0.Text.Trim().Length > 0) return;
                    if (lboxObj.Items.Count != 0)
                    {
                        SysValue sysValue = ViewState["SysValue"] as SysValue;
                        StringBuilder sb_msg = new StringBuilder();
                        foreach (ListItem m_item in lboxObj.Items)
                        {
                            DataSet ds = new DataSet();
                            //元件資料轉成Hashtable
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataEAttendTot.GetSchema(), dataToHashtable_batch(m_item.Value)));

                            RServiceProvider rsp = DataEAttendTot.Append(sysValue.emNo, sysValue.ProgId, ds);
                            if (rsp.Result)
                            {
                                sb_msg.AppendFormat("<span style=\"color:#009933;\">{0}({1})：新增成功！</span><br>", m_item.Text, m_item.Value);
                            }
                            else
                                sb_msg.AppendFormat("<span style=\"color:#ff0000;\">{0}({1})：新增失敗！</span><br>", m_item.Text, m_item.Value);
                        }
                        GridView1.DataBind();
                        lbl_Msg0.Text = sb_msg.ToString();
                    }
                    else
                    {
                        MainControls.showMsg(this, "必須加入人員!");
                    }
                    break;
                case "cancel"://取消顯示設定功能
                    lbl_Msg0.Text = "";
                    ChangeMultiView(0);
                    break;
            }
        }

        protected void MarkSortSetInit()
        {
            lboxSur.Items.Clear();
            lboxObj.Items.Clear();
            txtSh_AccID.Text = string.Empty;
        }

    }
}