﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class Employee : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
            PagerModul2(GridView2, ObjectDataSource2, ddlPageRow1, ddlPageChange1, labGridViewRows1, lbtnPageCtrlFirst1, lbtnPageCtrlPrev1, lbtnPageCtrlNext1, lbtnPageCtrlLast1, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_title);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_emName);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_emNo);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty), Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty), this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //下拉選單設定
                setddlChoices();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 840);
                MainControls.GridViewStyleCtrl(GridView2, 660);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
            }
        }
        #region 資料繫結
        private void dataBind()
        {
            DataEmployee.DataColumn(this.GridView1);
            DataEmployeeLimit.EmployeeLimitDataColumn(this.GridView2);
        }
        #endregion

        #region 下拉選單設定
        private void setddlChoices()
        {
            DataEmGroup.DropDownList(ddl_EmGpLimit, true, "");
            DataEmGroup.DropDownList(ddl_EmGpLimit_copy, true, "");
            //權限
            DataEmployeeLimit.FunctionReaderCheckBoxList(this.cbl_Function);
        }
        #endregion

        #region 資料驗証
        private string dataValid(Boolean b)
        {
            StringBuilder sbError = new StringBuilder();

            if (b)
            {
                if (txt_emNo.Text.Trim() == "")
                    sbError.Append("●「代碼」必須輸入!<br>");
                else
                    if (txt_emNo.Text.Trim().Length > 15 || !MainControls.ValidDataType("^\\d+$", txt_emNo.Text.Trim()))
                        sbError.Append("●「代碼」格式錯誤!<br>");
                    else
                        if (!DataEmployee.emNoValid(txt_emNo.Text.Trim()))
                            sbError.Append("●「代碼」已被使用!<br>");
                if (txt_emID.Text.Trim() == "")
                    sbError.Append("●「帳號」必須輸入!<br>");
                else
                    if (txt_emID.Text.Trim().Length > 15 || !MainControls.ValidDataType("^\\w+$", txt_emID.Text.Trim()))
                        sbError.Append("●「帳號」格式錯誤!<br>");
                    else
                        if (!DataEmployee.emIDValid(txt_emID.Text.Trim()))
                            sbError.Append("●「帳號」已被使用!<br>");
                if (txt_emPWD.Text.Trim() == "")
                    sbError.Append("●「密碼」必須輸入!<br>");
                else
                    if (txt_emPWD.Text.Trim().Length < 6 || !MainControls.ValidDataType("^([\\w- ./?%&=~!@#$^*]+)+$", txt_emPWD.Text.Trim()))
                        sbError.Append("●「密碼」格式錯誤!<br>");
                    else
                        if (txt_emPWDre.Text.Trim() != txt_emPWD.Text.Trim())
                            sbError.Append("●「密碼確認」錯誤!<br>");
                if (txt_emName.Text.Trim() == "")
                    sbError.Append("●「姓名」必須輸入!<br>");

                //if (rbtnl_emSex.SelectedIndex == -1)
                //    sbError.Append("●「性別」必須選擇!<br>");
                //if (txt_emMail.Text.Trim() == "")
                //    sbError.Append("●「信箱」必須輸入!<br>");
                if (txt_emMail.Text.Trim() != "" && !MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$", txt_emMail.Text.Trim()))
                    sbError.Append("●「信箱」格式錯誤!<br>");
                //if (txt_emPhone.Text.Trim() == "")
                //    sbError.Append("●「電話」必須輸入!<br>");
                if (txt_emPhone.Text.Trim() != "")
                    if (!MainControls.ValidDataType("(0\\d-\\d{7,8}#\\d{1,})|(0\\d-\\d{7,8})|(09\\d{8,13})", txt_emPhone.Text.Trim()))
                        sbError.Append("●「電話」格式錯誤!<br>");
            }
            else
            {
                if (txt_emNo.Text.Trim() == "")
                    sbError.Append("●「代碼」必須輸入!<br>");
                if (txt_emNo.Text.Trim().Length > 15 && !MainControls.ValidDataType("^\\d+$", txt_emNo.Text.Trim()))
                    sbError.Append("●「代碼」格式錯誤!<br>");
                if (txt_emID.Text.Trim() == "")
                    sbError.Append("●「帳號」必須輸入!<br>");
                if (txt_emID.Text.Trim().Length > 15 && !MainControls.ValidDataType("^\\w+$", txt_emID.Text.Trim()))
                    sbError.Append("●「帳號」格式錯誤!<br>");
                if (txt_emPWDre.Text.Trim() != txt_emPWD.Text.Trim())
                    sbError.Append("●「密碼確認」錯誤!<br>");
                if (txt_emName.Text.Trim() == "")
                    sbError.Append("●「姓名」必須輸入!<br>");
                if (txt_emName.Text.Trim().Length > 10)
                    sbError.Append("●「姓名」格式錯誤!<br>");
                //if (rbtnl_emSex.SelectedIndex == -1)
                //    sbError.Append("●「性別」必須選擇!<br>");
                //if (txt_emMail.Text.Trim() == "")
                //    sbError.Append("●「信箱」必須輸入!<br>");
                if (txt_emMail.Text.Trim() != "" && !MainControls.ValidDataType("^[\\w-]+(\\.[\\w-]+)*@[\\w-]+(\\.[\\w-]+)+$", txt_emMail.Text.Trim()))
                    sbError.Append("●「信箱」格式錯誤!<br>");
                if (txt_emPhone.Text.Trim() != "")
                    if (!MainControls.ValidDataType("(0\\d-\\d{7,8}#\\d{1,})|(0\\d-\\d{7,8})|(09\\d{8,13})", txt_emPhone.Text.Trim()))
                        sbError.Append("●「電話」格式錯誤!<br>");
            }

            return sbError.ToString();
        }

        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext(false);
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "emNo":
                            txt_emNo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "emID":
                            txt_emID.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "emName":
                            txt_emName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "emSex":
                            MainControls.ddlIndexSelectValue(rbtnl_emSex, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "emMail":
                            txt_emMail.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "emPhone":
                            txt_emPhone.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "enable":
                            cbox_emEnable.Checked = MainControls.ReplaceSpace(gvr.Cells[i].Text) == "True" ? true : false;
                            break;
                        case "emDate":
                            lbl_emDate.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "ipLimit":
                            txt_ipLimit.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EmGroup":
                            MainControls.ddlIndexSelectValue(ddl_EmGpLimit, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel")
            {

            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        lbl_Msg.Text = dataValid(true); //資料格式驗証
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema(true), dataToHashtable(true)));
                        RServiceProvider rsp = DataEmployee.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            txt_emNo.ReadOnly = true;
                            txt_emID.ReadOnly = true;
                            btn_emIDvalid.Visible = false;
                            tr_emPWD.Visible = false;
                            tr_emPWD_set.Visible = true;
                            tr_emPWDre.Visible = false;
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        lbl_Msg.Text = dataValid(false); //資料格式驗証
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema(false), dataToHashtable(false)));
                        RServiceProvider rsp = DataEmployee.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        lbl_Msg.Text = dataValid(false); //資料格式驗証
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployee.GetSchema(false), dataToHashtable(false)));
                        RServiceProvider rsp = DataEmployee.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp1 = DataEmployeeLimit.emNoEmployeeLimitDelete(txt_emNo.Text.Trim());
                        if (rsp.Result && rsp1.Result) //執行成功
                        {
                            GridView1.DataBind();
                            GridView2.DataBind();
                            maintainButtonEnabled("");
                            clearControlContext(true);
                            lbl_Msg.Text = "刪除成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext(true);
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable, 供異動資料使用
        private Hashtable dataToHashtable(Boolean b)
        {
            Hashtable hsData = new Hashtable();

            if (b)
            {
                hsData.Add("emNo", Lib.FdVP(txt_emNo.Text.Trim()));
                hsData.Add("emID", Lib.FdVP(txt_emID.Text.Trim()));
                hsData.Add("emPWD", Lib.GetMD5(txt_emPWD.Text.Trim()));
                hsData.Add("emName", Lib.FdVP(txt_emName.Text.Trim()));
                hsData.Add("emSex", rbtnl_emSex.SelectedValue);
                hsData.Add("emMail", Lib.FdVP(txt_emMail.Text));
                hsData.Add("emPhone", Lib.FdVP(txt_emPhone.Text.Trim()));
                hsData.Add("emEnable", cbox_emEnable.Checked ? "true" : "false");
                hsData.Add("ipLimit", Lib.FdVP(txt_ipLimit.Text.Trim()));
                hsData.Add("EmGroup", string.IsNullOrEmpty(ddl_EmGpLimit.SelectedValue) ? null : ddl_EmGpLimit.SelectedValue);
            }
            else
            {
                hsData.Add("emNo", txt_emNo.Text.Trim());
                hsData.Add("emID", Lib.FdVP(txt_emID.Text));
                hsData.Add("emName", Lib.FdVP(txt_emName.Text.Trim()));
                hsData.Add("emSex", rbtnl_emSex.SelectedValue);
                hsData.Add("emMail", Lib.FdVP(txt_emMail.Text));
                hsData.Add("emPhone", Lib.FdVP(txt_emPhone.Text.Trim()));
                hsData.Add("emEnable", cbox_emEnable.Checked ? "true" : "false");
                hsData.Add("ipLimit", Lib.FdVP(txt_ipLimit.Text.Trim()));
                hsData.Add("EmGroup", string.IsNullOrEmpty(ddl_EmGpLimit.SelectedValue) ? null : ddl_EmGpLimit.SelectedValue);
            }

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext(Boolean b)
        {
            if (b)
            {
                txt_emNo.ReadOnly = false;
                txt_emNo.Text = string.Empty;
                txt_emName.Text = string.Empty;
                txt_emID.ReadOnly = false;
                txt_emID.Text = string.Empty;
                btn_emIDvalid.Visible = true;
                tr_emPWD.Visible = true;
                txt_emPWD.Text = string.Empty;
                tr_emPWD_set.Visible = false;
                tr_emPWDre.Visible = true;
                txt_emPWDre.Text = string.Empty;
                txt_emMail.Text = string.Empty;
                txt_emPhone.Text = string.Empty;
                rbtnl_emSex.SelectedIndex = -1;
                cbox_emEnable.Checked = true;
                lbl_emDate.Text = string.Empty;
                txt_ipLimit.Text = string.Empty;
                ddl_EmGpLimit.SelectedIndex = -1;
                ddl_EmGpLimit_copy.SelectedIndex = -1;
                lbl_Msg.Text = string.Empty;
            }
            else
            {
                txt_emNo.ReadOnly = true;
                txt_emNo.Text = string.Empty;
                txt_emName.Text = string.Empty;
                txt_emID.ReadOnly = true;
                txt_emID.Text = string.Empty;
                btn_emIDvalid.Visible = false;
                tr_emPWD.Visible = false;
                txt_emPWD.Text = string.Empty;
                tr_emPWD_set.Visible = true;
                tr_emPWDre.Visible = false;
                txt_emPWDre.Text = string.Empty;
                txt_emMail.Text = string.Empty;
                txt_emPhone.Text = string.Empty;
                rbtnl_emSex.SelectedIndex = -1;
                cbox_emEnable.Checked = true;
                lbl_emDate.Text = string.Empty;
                txt_ipLimit.Text = string.Empty;
                ddl_EmGpLimit.SelectedIndex = -1;
                ddl_EmGpLimit_copy.SelectedIndex = -1;
                lbl_Msg.Text = string.Empty;
            }
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext(true);
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    //選單主分類
                    DataMenuLayer.menuLayerReaderDropDownList(ddl_muLayer, true, "全部");
                    //選單子分類
                    DataMenu.menuReaderDropDownList(ddl_muNo, false, ddl_muLayer.SelectedValue);
                    //重載員工權限資料
                    GridView2.DataBind();
                    lbl_Msg1.Text = string.Empty;
                    break;
            }
        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            btnInsert.Enabled = false;
            btnCopy.Enabled = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    btnInsert.Enabled = true & sysValue.Authority.Append;
                    btnCopy.Enabled = true & sysValue.Authority.Append;
                    btn_resetPWD.Enabled = Session["emNo"].ToString() == "00000000";
                    this.SetFocus(txt_emName);
                    break;
                default:
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    btnInsert.Enabled = false;
                    btnCopy.Enabled = false;
                    this.SetFocus(txt_emName);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource1_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        protected void GridView_RowDataBound2(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "編輯")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).CssClass = "btnEdit";
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[7].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }
                if (((Button)e.Row.Cells[7].Controls[2]).Text == "刪除")
                {
                    ((Button)e.Row.Cells[7].Controls[2]).CssClass = "btnDelete2";
                    ((Button)e.Row.Cells[7].Controls[2]).Enabled = sysValue.Authority.Delete;
                    ((Button)e.Row.Cells[7].Controls[2]).Style.Add("cursor", sysValue.Authority.Delete ? "pointer" : "not-allowed");
                }
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "更新")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).CssClass = "btnSave2";
                    ((Button)e.Row.Cells[7].Controls[2]).CssClass = "btnCancel2";
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                    ((Button)e.Row.Cells[7].Controls[0]).Style.Add("cursor", sysValue.Authority.Update ? "pointer" : "not-allowed");
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("emNoName")) return;
            e.InputParameters.Add("emNoName", Lib.FdVP(txtSh_title.Text).Trim());
            e.InputParameters.Add("emEnable", ddlSh_enable.SelectedValue.Trim());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_title.Text = string.Empty;
            ddlSh_enable.SelectedIndex = 0;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        #region 分頁模組

        private void PagerModul2(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = new object[] { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls2"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound2;
            grv.DataBound += GridView2_DataBound;
            objDS.Selected += ObjectDataSource2_Selected;
            objDS.Selecting += ObjectDataSource2_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows1_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex1_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl1_Command;
            lbtnPagePre.Command += lbtnPageCtrl1_Command;
            lbtnPageNext.Command += lbtnPageCtrl1_Command;
            lbtnPageLast.Command += lbtnPageCtrl1_Command;
        }

        bool chagePageRow1 = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls2"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageIndex = 0;

            chagePageRow1 = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices1(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource2_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            //預防無資料時分頁控制按鈕預設會啟用
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls2"])[0]), (DropDownList)((object[])ViewState["PagerModulControls2"])[3]);

            if (e.ReturnValue is DataSet)
                ViewState["table1"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls2"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow1)
                {
                    chagePageRow1 = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls2"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls2"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == (DropDownList)((object[])ViewState["PagerModulControls2"])[3])
            {
                ((GridView)((object[])ViewState["PagerModulControls2"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex;
                PageChoiceSet1(((GridView)((object[])ViewState["PagerModulControls2"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl1_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls2"])[0]), e.CommandName);
            PageChoiceSet1(((GridView)((object[])ViewState["PagerModulControls2"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet1(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls2"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[6]).Style.Add("cursor", "default");

                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                        ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[7]).Style.Add("cursor", "pointer");
                    ((LinkButton)((object[])ViewState["PagerModulControls2"])[8]).Style.Add("cursor", "pointer");

                }
                if (((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls2"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView2_DataBound(object sender, EventArgs e)
        {
            PageChoices1(((GridView)((object[])ViewState["PagerModulControls2"])[0]), (DropDownList)((object[])ViewState["PagerModulControls2"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource2_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            if (e.InputParameters.Contains("emNo")) return;
            e.InputParameters.Add("emNo", txt_emNo.Text);
            e.InputParameters.Add("muLayer", ddl_muLayer.SelectedValue);
        }
        #endregion

        #endregion

        #region 權限異動按鈕
        protected void btnInsert_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            
            switch (e.CommandName)
            {
                case "Insert": //新增權限
                    if (ddl_muNo.SelectedIndex == -1)
                    {
                        lbl_Msg1.Text = "「系統功能選單」必須選取!";
                        return;
                    }
                    else
                        if (!DataEmployeeLimit.emNoLimitValid(txt_emNo.Text.Trim(), ddl_muNo.SelectedValue))
                        {
                            lbl_Msg1.Text = "此「系統功能選單」權限已加入!";
                            return;
                        }
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataEmployeeLimit.GetSchema(), dataToHashtableForLimit()));
                    RServiceProvider rsp = DataEmployeeLimit.EmployeeLimitAppend(sysValue.emNo, sysValue.ProgId, ds);
                    //員工維護系統資料記錄
                    DataemDaily.emDailyInsert("0", sysValue.emNo, sysValue.ProgId, ds, DataemDaily.MaintainStatus.Insert);
                    if (rsp.Result) //執行成功
                    {
                        GridView2.DataBind();
                        lbl_Msg1.Text = "新增權限成功!";
                    }
                    else
                        lbl_Msg1.Text = rsp.ReturnMessage;
                    break;
                case "Copy": //複製群組權限
                    if (string.IsNullOrEmpty(ddl_EmGpLimit_copy.SelectedValue))
                    {
                        MainControls.showMsg(this, "請選擇群組!");
                        return;
                    }
                    else
                    {
                        RServiceProvider rsp_copy = DataEmployeeLimit.CopyEmGroupLimit(txt_emNo.Text.Trim(), ddl_EmGpLimit_copy.SelectedValue);
                        if (rsp_copy.Result) //執行成功
                        {
                            GridView2.DataBind();
                            lbl_Msg1.Text = "複製群組權限成功!";
                        }
                        else
                            MainControls.showMsg(this, rsp_copy.ReturnMessage);
                    }
                    break;
            }
        }
        private Hashtable dataToHashtableForLimit()
        {
            Hashtable hsData = new Hashtable();
            hsData.Add("id", "");
            hsData.Add("emNo", txt_emNo.Text);
            hsData.Add("muNo", ddl_muNo.SelectedItem.Value);
            hsData.Add("emInsert", cbl_Function.Items.FindByValue("emInsert").Selected);
            hsData.Add("emUpdate", cbl_Function.Items.FindByValue("emUpdate").Selected);
            hsData.Add("emSelect", cbl_Function.Items.FindByValue("emSelect").Selected);
            hsData.Add("emDelete", cbl_Function.Items.FindByValue("emDelete").Selected);
            return hsData;
        }
        #endregion

        #region 選單分類變更事件
        protected void ddl_muLayer_SelectedIndexChanged(object sender, EventArgs e)
        {
            //選單子分類
            DataMenu.menuReaderDropDownList(ddl_muNo, false, ddl_muLayer.SelectedValue);
            GridView2.DataBind();
        }
        #endregion

        #region 更新權限資料
        protected void ObjectDataSource2_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            string muNo = GridView2.SelectedRow.Cells[1].Text;
            string id = GridView2.SelectedRow.Cells[0].Text;
            if (e.InputParameters.Contains("id")) return;
            {
                e.InputParameters.Insert(0, "id", id);
                e.InputParameters.Insert(1, "emNo", sysValue.emNo);
                e.InputParameters.Insert(2, "progId", sysValue.ProgId);
                e.InputParameters.Insert(3, "muNo", muNo);
            }
        }
        protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView2.SelectedIndex = e.RowIndex;
        }
        #endregion

        #region 刪除權限資料
        protected void ObjectDataSource2_Deleting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            string muNo = GridView2.SelectedRow.Cells[1].Text;
            string id = GridView2.SelectedRow.Cells[0].Text;
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.InputParameters.Contains("id")) return;
            {
                e.InputParameters.Add("id", id);
                e.InputParameters.Add("emNo", sysValue.emNo);
                e.InputParameters.Add("progId", sysValue.ProgId);
                e.InputParameters.Add("muNo", muNo);
            }

        }
        protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView2.SelectedIndex = e.RowIndex;
        }
        #endregion

        #region command field及button field加確認屬性的方法 && GridView 編輯時，調整欄寬
        protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (e.Row.RowState == (DataControlRowState.Edit | DataControlRowState.Alternate)
                //    || e.Row.RowState == DataControlRowState.Edit
                //    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit)
                //    || e.Row.RowState == (DataControlRowState.Selected | DataControlRowState.Edit | DataControlRowState.Alternate)
                //    )
                //{
                //    //Width屬性是Unit類型，不能直接給整數值
                //    //Unit.Pixel():依像表設定絕對寬度；Unit.Percent():依百分比設定寬度
                //    ((TextBox)e.Row.Cells[0].Controls[0]).Width = Unit.Pixel(140); //預刊日期
                //    ((TextBox)e.Row.Cells[1].Controls[0]).Width = Unit.Pixel(20); //刊登天數
                //}

                if (((Button)e.Row.Cells[7].Controls[2]).Text == "刪除")
                {
                    ((Button)e.Row.Cells[7].Controls[2]).Attributes.Add("onclick", "if(!window.confirm('確定要刪除嗎？')) return;");
                    ((Button)e.Row.Cells[7].Controls[2]).Enabled = sysValue.Authority.Delete;
                }
                if (((Button)e.Row.Cells[7].Controls[0]).Text == "編輯")
                {
                    ((Button)e.Row.Cells[7].Controls[0]).Enabled = sysValue.Authority.Update;
                }
            }
        }
        #endregion

        #region 帳號檢核
        protected void btn_emIDvalid_Click(object sender, EventArgs e)
        {
            if (txt_emID.Text.Trim() == "")
                MainControls.showMsg(this, "請輸入帳號!");
            else
            {
                if (!DataEmployee.emIDValid(txt_emID.Text.Trim()))
                    MainControls.showMsg(this, "無法使用，已被使用!");
                else
                    MainControls.showMsg(this, "可以使用，未被使用!");
            }
        }
        #endregion

        #region 系統功能選單
        protected void ddl_muNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_Msg1.Text = string.Empty;
        }
        #endregion

        #region 變更密碼
        protected void btn_resetPWD_Click(object sender, EventArgs e)
        {
            string newPWD = MainControls.random("4", 8);
            DataRow dr = DataEmployee.baseDataReader(txt_emID.Text.Trim()).Tables[0].Rows[0];
            RServiceProvider rsp1 = DataEmployee.changePwd(dr["emID"].ToString(), dr["emPWD"].ToString(), Lib.GetMD5(newPWD));
            if (rsp1.Result)
                MainControls.showMsg(this, string.Format("密碼變更完成!\\n新密碼：{0}", newPWD));
            else
                MainControls.showMsg(this, string.Format("密碼變更失敗!\\n{0}", rsp1.ReturnMessage));
        }
        #endregion
    }
}