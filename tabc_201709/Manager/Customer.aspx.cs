﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class Customer : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_CU_CustName);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:
                    if (btnEdit.Visible)
                    {
                        this.SetFocus(txt_CU_CustName);
                        Panel_Form.DefaultButton = btnEdit.ID;
                    }
                    else
                    {
                        this.SetFocus(txt_CU_CustName);
                        Panel_Form.DefaultButton = btnAppend.ID;
                    }
                    break;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //測試用登入參數
                //DataRow dr_person = DataPerson.DataReader_AccID("TB0232");
                //if (dr_person != null)
                //{
                //    Session["UidNo"] = dr_person["PS_UidNo"].ToString();
                //    Session["AccID"] = dr_person["AccID"].ToString();
                //    Session["ComyCode"] = dr_person["ComyCode"].ToString();
                //}
                //DataRow dr_em = DataEmployee.emDataReader("11526");
                //Session["emUser"] = string.Format("{0}　NO.{1}", dr_em["emName"].ToString().Trim(), dr_em["emNo"].ToString().Trim());
                //Session["emNo"] = dr_em["emNo"].ToString().Trim();
                //Session["emID"] = dr_em["emID"].ToString().Trim();
                //Session["emLimit"] = dr_em["specialLimit"].ToString().Trim();
                //ViewState["SysValue"] = new SysValue("Customer", this.Session);

                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 1000);
                MainControls.GridViewStyleCtrl(GridView2, 750);
                MainControls.GridViewStyleCtrl(GridView3, 800);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();

            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataCustomer.DataColumn(this.GridView1);
            DataCommunication.DataColumn(this.GridView2);
            DataFamilyRela.DataColumn(this.GridView3);
        }
        private void dataBind_GV2()
        {
            //聯絡資訊
            DataSet ds = DataCommunication.DataReader_CU_TransNo(hif_TransNo.Value);
            DataCommunication.DataProcess(ds);
            GridView2.DataSource = ds.Tables[0];
            GridView2.DataBind();
        }
        private void dataBind_GV3()
        {
            //家庭關係
            DataSet ds = DataFamilyRela.DataReader_CU_TransNo(hif_TransNo.Value);
            DataFamilyRela.DataProcess(ds);
            GridView3.DataSource = ds.Tables[0];
            GridView3.DataBind();
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            DataPhrase.DDL_TypeName(ddlSh_CU_CustSource, true, "", "099");
            DataPhrase.DDL_TypeName(ddl_CU_CustSource, true, "", "099");
            DataPhrase.DDL_TypeName(ddlSh_CU_Sex, true, "", "002");
            DataPhrase.DDL_TypeName(ddl_CU_Sex, true, "", "002");
            DataPhrase.DDL_TypeName(ddl_CU_JobPA, true, "", "003");
            DataPhrase.DDL_TypeName(ddl_CU_CustClass, true, "", "020");
            DataPhrase.DDL_TypeName(ddlSh_CU_ContactMode, true, "", "021");
            DataPhrase.DDL_TypeName(ddl_CU_ContactMode, true, "", "021");
            DataPhrase.DDL_TypeName(ddlSh_CU_Marry, true, "", "022");
            DataPhrase.DDL_TypeName(ddl_CU_Marry, true, "", "022");
            DataPhrase.DDL_TypeName(ddlSh_CU_CustType, true, "", "023");
            DataPhrase.DDL_TypeName(ddl_CU_CustType, true, "", "023");
            DataPhrase.DDL_TypeName(ddl_CU_Category, true, "", "024");
            DataPhrase.DDL_TypeName(ddl_CU_Category1, true, "", "025");
            DataPhrase.DDL_TypeName(ddl_CU_Category2, true, "", "026");

            DataPhrase.DDL_TypeName(ddl_CM_CLAS, true, "", "040");

            DataPhrase.DDL_TypeName(ddl_FA_RelaName, true, "", "019");
            ddl_FA_RelaName.Items.RemoveAt(1);
            DataPhrase.DDL_TypeName(ddl_CU_Sex0, true, "", "002");
            DataPhrase.DDL_TypeName(ddl_CU_JobPA0, true, "", "003");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_CU_CustName.Text.Trim() == "")
                sbError.Append("●「客戶名稱/姓名」必須輸入!<br>");
            if (ddl_CU_Sex.SelectedValue == "")
                sbError.Append("●「性別」必須選取!<br>");
            if (txt_CU_Birth.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_CU_Birth.Text.Trim()))
                    sbError.Append("●「出生日期」格式錯誤!<br>");
            }
            if (txt_CU_Age.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_CU_Age.Text.Trim())))
                    sbError.Append("●「投保年齡」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        private string dataValid_CM(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (ddl_CM_CLAS.SelectedValue == "")
                sbError.Append("●「聯絡類別」必須選取!<br>");
            if (ddl_CM_TYPE.SelectedValue == "")
                sbError.Append("●「聯絡次類別」必須選取!<br>");
            if (txt_CM_DESC.Text.Trim() == "")
                sbError.Append("●「聯絡內容」必須輸入!<br>");
            

            return sbError.ToString();
        }
        private string dataValid_FA(string commandName)
        {
            StringBuilder sbError = new StringBuilder();

            if (txt_CU_CustName0.Text.Trim() == "")
                sbError.Append("●「客戶名稱/姓名」必須輸入!<br>");
            if (ddl_CU_Sex0.SelectedValue == "")
                sbError.Append("●「性別」必須選取!<br>");
            if (txt_CU_Birth0.Text.Trim() != "")
            {
                if (!MainControls.ValidDate(txt_CU_Birth0.Text.Trim()))
                    sbError.Append("●「出生日期」格式錯誤!<br>");
            }
            if (txt_CU_Age.Text.Trim() != "")
            {
                if (!MainControls.ValidDataType("^(100|[1-9]?[0-9])$", (txt_CU_Age0.Text.Trim())))
                    sbError.Append("●「投保年齡」格式錯誤!<br>");
            }

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "CU_TransNo":
                            hif_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            dataBind_GV2();
                            dataBind_GV3();
                            break;
                        case "CU_UidNo":
                            hif_CU_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_CustName":
                            txt_CU_CustName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_ID":
                            txt_CU_ID.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_Sex":
                            MainControls.ddlIndexSelectText(ddl_CU_Sex, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_Birth":
                            if(MainControls.ReplaceSpace(gvr.Cells[i].Text) != "")
                                txt_CU_Birth.Text = DateTime.Parse(MainControls.ReplaceSpace(gvr.Cells[i].Text)).ToString("yyyy/MM/dd");
                            break;
                        case "CU_Age":
                            txt_CU_Age.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_Marry":
                            MainControls.ddlIndexSelectText(ddl_CU_Marry, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_CustType":
                            MainControls.ddlIndexSelectText(ddl_CU_CustType, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_CustClass":
                            MainControls.ddlIndexSelectText(ddl_CU_CustClass, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_CustSource":
                            MainControls.ddlIndexSelectText(ddl_CU_CustSource, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_Category":
                            MainControls.ddlIndexSelectText(ddl_CU_Category, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_Category1":
                            MainControls.ddlIndexSelectText(ddl_CU_Category1, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_Category2":
                            MainControls.ddlIndexSelectText(ddl_CU_Category2, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_CompName":
                            txt_CU_CompName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_DepartName":
                            txt_CU_DepartName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_CustTitle":
                            txt_CU_CustTitle.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_JobName":
                            txt_CU_JobName.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_JobPA":
                            MainControls.ddlIndexSelectText(ddl_CU_JobPA, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_ContactMode":
                            MainControls.ddlIndexSelectText(ddl_CU_ContactMode, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CU_CustMemo":
                            txt_CU_CustMemo.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;

                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext_CM();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView2.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView2.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView2.Columns[i] is BoundField)
                {
                    BoundField bf = GridView2.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "CM_TransNo":
                            hif_CM_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CM_UidNo":
                            hif_CM_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CM_CLAS":
                            MainControls.ddlIndexSelectText(ddl_CM_CLAS, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            switch (ddl_CM_CLAS.SelectedItem.Text)
                            {
                                case "地址":
                                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "041");
                                    tr_CM_ZIP.Visible = true;
                                    break;
                                case "電話":
                                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "042");
                                    tr_CM_ZIP.Visible = false;
                                    break;
                                case "其他":
                                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "043");
                                    tr_CM_ZIP.Visible = false;
                                    break;
                            }
                            break;
                        case "CM_TYPE":
                            MainControls.ddlIndexSelectText(ddl_CM_TYPE, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            break;
                        case "CM_ZIP":
                            txt_CM_ZIP.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CM_DESC":
                            txt_CM_DESC.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;


                    }
                }
            //切換至維護畫面
            show_div("div_CM_edit");
            maintainButtonEnabled("Select");
        }
        protected void GridView3_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext_FA();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView3.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView3.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView3.Columns[i] is BoundField)
                {
                    BoundField bf = GridView3.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "FA_TransNo":
                            hif_FA_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "FA_UidNo":
                            hif_FA_UidNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "CU_TransNo":
                            hif_CU_TransNo.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            DataRow dr_CU = DataCustomer.DataReader_CU_TransNo(hif_CU_TransNo.Value);
                            if(dr_CU != null)
                            {
                                txt_CU_CustName0.Text = dr_CU["CU_CustName"].ToString();
                                txt_CU_ID0.Text = dr_CU["CU_ID"].ToString();
                                MainControls.ddlIndexSelectText(ddl_CU_Sex0, dr_CU["CU_Sex"].ToString());
                                if (dr_CU["CU_Birth"].ToString() != "")
                                    txt_CU_Birth0.Text = DateTime.Parse(dr_CU["CU_Birth"].ToString()).ToString("yyyy/MM/dd");
                                MainControls.ddlIndexSelectText(ddl_CU_JobPA0, dr_CU["CU_JobPA"].ToString());
                                txt_CU_Age0.Text = dr_CU["CU_Age"].ToString();
                            }
                            break;
                        case "CU_UidNo":
                            hif_CU_UidNo0.Value = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "FA_RelaName":
                            MainControls.ddlIndexSelectText(ddl_FA_RelaName, MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            
                            break;

                    }
                }
            //切換至維護畫面
            show_div("div_FA_edit");
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        hif_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable("A")));

                        RServiceProvider rsp = DataCustomer.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable("M")));

                        RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable("D")));

                        //RServiceProvider rsp = DataCustomer.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            GridView1.DataBind();
                            lbl_Msg.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext();
                            return;
                        }
                        else
                            lbl_Msg.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    GridView1.DataBind();
                    break;
            }
        }
        protected void btn_Command_CM(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();

            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg0.Text = dataValid_CM(e.CommandName); //資料格式驗証
                show_div("div_CM_edit");

            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        hif_CM_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(), dataToHashtable_CM("A")));

                        RServiceProvider rsp = DataCommunication.Append(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "新增成功...";
                            maintainButtonEnabled("Select");
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(), dataToHashtable_CM("M")));

                        RServiceProvider rsp = DataCommunication.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "修改成功...";
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg0.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(), dataToHashtable_CM("D")));

                        //RServiceProvider rsp = DataCommunication.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        RServiceProvider rsp = DataCommunication.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                        {
                            dataBind_GV2();
                            lbl_Msg0.Text = "刪除成功...";
                            maintainButtonEnabled("");
                            clearControlContext_CM();
                            return;
                        }
                        else
                            lbl_Msg0.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    //ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('div_CM_edit').unblock();", true);
                    clearControlContext_CM();
                    GridView2.DataBind();
                    break;
            }

            
        }
        protected void btn_Command_FA(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            DataSet ds = new DataSet();
            DataSet ds_CU = new DataSet();
            if (e.CommandName != "Cancel" && e.CommandName != "Delete")
            {
                lbl_Msg1.Text = dataValid_FA(e.CommandName); //資料格式驗証
                show_div("div_FA_edit");
            }

            switch (e.CommandName)
            {
                case "Append": //新增
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        hif_CU_TransNo.Value = System.Guid.NewGuid().ToString("N");
                        //元件資料轉成Hashtable
                        ds_CU.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable_CU("A")));

                        RServiceProvider rsp = DataCustomer.Append(sysValue.emNo, sysValue.ProgId, ds_CU);
                        if (rsp.Result) //執行成功
                        {
                            hif_FA_TransNo.Value = System.Guid.NewGuid().ToString("N");
                            //元件資料轉成Hashtable
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_FA("A")));

                            rsp = DataFamilyRela.Append(sysValue.emNo, sysValue.ProgId, ds);
                            if (rsp.Result) //執行成功
                            {
                                dataBind_GV3();
                                lbl_Msg1.Text = "新增成功...";
                                maintainButtonEnabled("Select");
                                return;
                            }
                            else
                                lbl_Msg1.Text = rsp.ReturnMessage;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Edit": //修改
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds_CU.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable_CU("M")));

                        RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds_CU);
                        if (rsp.Result) //執行成功
                        {
                            //元件資料轉成Hashtable
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_FA("M")));

                            rsp = DataFamilyRela.Update(sysValue.emNo, sysValue.ProgId, ds);
                            if (rsp.Result) //執行成功
                            {
                                dataBind_GV3();
                                lbl_Msg1.Text = "修改成功...";
                                return;
                            }
                            else
                                lbl_Msg1.Text = rsp.ReturnMessage;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Delete": //刪除
                    {
                        if (lbl_Msg1.Text.Trim().Length > 0) return;
                        //元件資料轉成Hashtable
                        ds_CU.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable_CU("D")));

                        RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds_CU);
                        if (rsp.Result) //執行成功
                        {
                            //元件資料轉成Hashtable
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_FA("D")));

                            //RServiceProvider rsp = DataFamilyRela.Delete(sysValue.emNo, sysValue.ProgId, ds);
                            rsp = DataFamilyRela.Update(sysValue.emNo, sysValue.ProgId, ds);
                            if (rsp.Result) //執行成功
                            {
                                dataBind_GV3();
                                lbl_Msg1.Text = "刪除成功...";
                                maintainButtonEnabled("");
                                clearControlContext_FA();
                                return;
                            }
                            else
                                lbl_Msg1.Text = rsp.ReturnMessage;
                        }
                        else
                            lbl_Msg1.Text = rsp.ReturnMessage;
                    }
                    break;
                case "Cancel": //取消
                    //瀏覽
                    //ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('div_FA_edit').unblock();", true);
                    clearControlContext_FA();
                    GridView3.DataBind();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CU_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("CU_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("CU_UidNo", hif_CU_UidNo.Value);
            }
            hsData.Add("CU_CustName", txt_CU_CustName.Text.Trim());
            hsData.Add("CU_ID", txt_CU_ID.Text.Trim());
            hsData.Add("CU_Sex", ddl_CU_Sex.SelectedItem.Text.Trim());
            if(txt_CU_Birth.Text.Trim() != "")
                hsData.Add("CU_Birth", txt_CU_Birth.Text.Trim());
            else
                hsData.Add("CU_Birth", null);
            if (txt_CU_Age.Text.Trim() != "")
                hsData.Add("CU_Age", txt_CU_Age.Text.Trim());
            else
                hsData.Add("CU_Age", null);
            hsData.Add("CU_Marry", ddl_CU_Marry.SelectedItem.Text.Trim());
            hsData.Add("CU_CustType", ddl_CU_CustType.SelectedItem.Text.Trim());
            hsData.Add("CU_CustClass", ddl_CU_CustClass.SelectedItem.Text.Trim());
            hsData.Add("CU_CustSource", ddl_CU_CustSource.SelectedItem.Text.Trim());
            hsData.Add("CU_Category", ddl_CU_Category.SelectedItem.Text.Trim());
            hsData.Add("CU_Category1", ddl_CU_Category1.SelectedItem.Text.Trim());
            hsData.Add("CU_Category2", ddl_CU_Category2.SelectedItem.Text.Trim());
            hsData.Add("CU_CompName", txt_CU_CompName.Text.Trim());
            hsData.Add("CU_DepartName", txt_CU_DepartName.Text.Trim());
            hsData.Add("CU_CustTitle", txt_CU_CustTitle.Text.Trim());
            hsData.Add("CU_JobName", txt_CU_JobName.Text.Trim());
            hsData.Add("CU_JobPA", ddl_CU_JobPA.SelectedItem.Text.Trim());
            hsData.Add("CU_ContactMode", ddl_CU_ContactMode.SelectedItem.Text.Trim());
            hsData.Add("CU_CustMemo", txt_CU_CustMemo.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CU_TransNo", TransNo);
            hsData.Add("CU_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_CM(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CM_TransNo", hif_CM_TransNo.Value);
            hsData.Add("CU_TransNo", hif_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("CM_UidNo", Session["UidNo"].ToString());
                hsData.Add("CU_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("CM_UidNo", hif_CM_UidNo.Value);
                hsData.Add("CU_UidNo", hif_CU_UidNo.Value);
            }
            
            hsData.Add("CM_CLAS", ddl_CM_CLAS.SelectedItem.Text);
            hsData.Add("CM_TYPE", ddl_CM_TYPE.SelectedItem.Text);
            hsData.Add("CM_ZIP", txt_CM_ZIP.Text.Trim());
            hsData.Add("CM_DESC", txt_CM_DESC.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete_CM(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CM_TransNo", TransNo);
            hsData.Add("CM_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_CU(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CU_TransNo", hif_CU_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("CU_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("CU_UidNo", hif_CU_UidNo0.Value);
            }

            hsData.Add("CU_CustName", txt_CU_CustName0.Text.Trim());
            hsData.Add("CU_ID", txt_CU_ID0.Text.Trim());
            hsData.Add("CU_Sex", ddl_CU_Sex0.SelectedItem.Text.Trim());
            hsData.Add("CU_Birth", txt_CU_Birth0.Text.Trim());
            hsData.Add("CU_Age", txt_CU_Age0.Text.Trim());
            hsData.Add("CU_JobPA", ddl_CU_JobPA0.SelectedItem.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete_CU(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("CU_TransNo", TransNo);
            hsData.Add("CU_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        private Hashtable dataToHashtable_FA(string ModiState)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("FA_TransNo", hif_FA_TransNo.Value);
            hsData.Add("CU_TransNo", hif_TransNo.Value);
            hsData.Add("FA_CUTransNo", hif_CU_TransNo.Value);
            if (ModiState == "A")
            {
                hsData.Add("FA_UidNo", Session["UidNo"].ToString());
                hsData.Add("CU_UidNo", Session["UidNo"].ToString());
                hsData.Add("FA_CUUidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("FA_UidNo", hif_FA_UidNo.Value);
                hsData.Add("CU_UidNo", hif_CU_UidNo.Value);
                hsData.Add("FA_CUUidNo", hif_CU_UidNo.Value);
            }
            
            
            hsData.Add("FA_RelaName", ddl_FA_RelaName.SelectedItem.Text);
            hsData.Add("FA_RelaNo", ddl_FA_RelaName.SelectedValue);

            hsData.Add("CU_CustName", txt_CU_CustName0.Text.Trim());
            hsData.Add("CU_ID", txt_CU_ID0.Text.Trim());
            hsData.Add("CU_Sex", ddl_CU_Sex0.SelectedItem.Text.Trim());
            hsData.Add("CU_Birth", txt_CU_Birth0.Text.Trim());
            hsData.Add("CU_Age", txt_CU_Age0.Text.Trim());
            hsData.Add("CU_JobPA", ddl_CU_JobPA0.SelectedItem.Text.Trim());

            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }
        private Hashtable dataToHashtable_dlete_FA(string TransNo, string UidNo)
        {
            Hashtable hsData = new Hashtable();

            hsData.Add("FA_TransNo", TransNo);
            hsData.Add("FA_UidNo", UidNo);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", "D");

            return hsData;
        }
        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            hif_TransNo.Value = string.Empty;
            hif_CU_UidNo.Value = string.Empty;
            txt_CU_CustName.Text = string.Empty;
            txt_CU_ID.Text = string.Empty;
            ddl_CU_Sex.SelectedIndex = -1;
            txt_CU_Birth.Text = string.Empty;
            txt_CU_Age.Text = string.Empty;
            ddl_CU_Marry.SelectedIndex = -1;
            ddl_CU_CustType.SelectedIndex = -1;
            ddl_CU_CustClass.SelectedIndex = -1;
            ddl_CU_CustSource.SelectedIndex = -1;
            ddl_CU_Category.SelectedIndex = -1;
            ddl_CU_Category1.SelectedIndex = -1;
            ddl_CU_Category2.SelectedIndex = -1;
            txt_CU_CompName.Text = string.Empty;
            txt_CU_DepartName.Text = string.Empty;
            txt_CU_CustTitle.Text = string.Empty;
            txt_CU_JobName.Text = string.Empty;
            ddl_CU_JobPA.SelectedIndex = -1;
            ddl_CU_ContactMode.SelectedIndex = -1;
            txt_CU_CustMemo.Text = string.Empty;

            lbl_Msg.Text = string.Empty;
        }
        private void clearControlContext_CM()
        {
            hif_CM_TransNo.Value = string.Empty;
            hif_CM_UidNo.Value = string.Empty;
            ddl_CM_CLAS.SelectedIndex = -1;
            ddl_CM_TYPE.Items.Clear();
            ddl_CM_TYPE.SelectedIndex = -1;
            txt_CM_ZIP.Text = string.Empty;
            tr_CM_ZIP.Visible = true;
            txt_CM_DESC.Text = string.Empty;

            lbl_Msg0.Text = string.Empty;
        }
        private void clearControlContext_FA()
        {
            hif_FA_TransNo.Value = string.Empty;
            hif_FA_UidNo.Value = string.Empty;
            hif_CU_TransNo.Value = string.Empty;
            hif_CU_UidNo0.Value = string.Empty;
            ddl_FA_RelaName.SelectedIndex = -1;
            txt_CU_CustName0.Text = string.Empty;
            txt_CU_ID0.Text = string.Empty;
            ddl_CU_Sex0.SelectedIndex = -1;
            txt_CU_Birth0.Text = string.Empty;
            txt_CU_Age0.Text = string.Empty;
            ddl_CU_JobPA0.SelectedIndex = -1;

            lbl_Msg1.Text = string.Empty;
        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain.Visible = false;
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain.Visible = (ViewState["SysValue"] as SysValue).Authority.Append;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnAppend.Visible = false;
            btnEdit.Visible = false;
            btnCancel.Visible = false;
            btnDelete.Visible = false;
            btnAppend0.Visible = false;
            btnEdit0.Visible = false;
            btnCancel0.Visible = false;
            btnDelete0.Visible = false;
            btnAppend1.Visible = false;
            btnEdit1.Visible = false;
            btnCancel1.Visible = false;
            btnDelete1.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnEdit.Visible = true & sysValue.Authority.Update;
                    btnDelete.Visible = true & sysValue.Authority.Delete;
                    btnCancel.Visible = true;
                    btnEdit0.Visible = true & sysValue.Authority.Update;
                    btnDelete0.Visible = true & sysValue.Authority.Delete;
                    btnCancel0.Visible = true;
                    btnEdit1.Visible = true & sysValue.Authority.Update;
                    btnDelete1.Visible = true & sysValue.Authority.Delete;
                    btnCancel1.Visible = true;
                    if (ddl_FA_RelaName.SelectedItem.Text == "本人")
                    {
                        btnEdit1.Visible = false;
                        btnDelete1.Visible = false;
                    }
                    else
                    {
                        btnEdit1.Visible = true;
                        btnDelete1.Visible = true;
                    }
                    this.SetFocus(txt_CU_CustName);
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnAppend.Visible = true & sysValue.Authority.Append;
                    btnCancel.Visible = true;
                    btnAppend0.Visible = true & sysValue.Authority.Append;
                    btnCancel0.Visible = true;
                    btnAppend1.Visible = true & sysValue.Authority.Append;
                    btnCancel1.Visible = true;
                    this.SetFocus(txt_CU_CustName);
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                int total = DataCustomer.GetCount(
                    Lib.FdVP(txtSh_CU_CustName.Text).Trim(),
                    Lib.FdVP(txtSh_CU_ID.Text).Trim(),
                    ddlSh_CU_Sex.SelectedItem.Text,
                    Lib.FdVP(txtSh_CU_Age.Text).Trim(),
                    ddlSh_CU_Marry.SelectedItem.Text,
                    ddlSh_CU_CustType.SelectedItem.Text,
                    ddlSh_CU_CustSource.SelectedItem.Text,
                    ddlSh_CU_ContactMode.SelectedItem.Text,
                    "",
                    Lib.FdVP(txtSh_Mobile.Text).Trim(),
                    Lib.FdVP(txtSh_Address.Text).Trim(),
                    Lib.FdVP(txtSh_PO_ComSName.Text).Trim(),
                    Lib.FdVP(txtSh_PO_PolNo.Text).Trim(),
                    DateRange_PO_AccureDate.Date_start,
                    DateRange_PO_AccureDate.Date_end,
                    Lib.FdVP(txtSh_PO_Polmmemo.Text).Trim(),
                    Session["AccID"].ToString()
                    );
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = string.Format("客戶總數：<font class='GridPageCountInt'>{0}</font>／客戶及家屬總數：<font class='GridPageCountInt'>{1}</font>", count, total);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[10].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[11].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[10].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[10].Controls.Add(btn);
            }
        }
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                #region cbox,sendbtn
                e.Row.Cells[5].Controls.Clear();
                CheckBox cboxi = new CheckBox();
                cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[6].Text);
                cboxi.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(cboxi);

                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[5].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_CM_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[5].Controls.Add(btn);
            }
        }
        protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

                if (MainControls.ReplaceSpace(e.Row.Cells[1].Text) != "本人")
                {
                    #region cbox,sendbtn
                    e.Row.Cells[8].Controls.Clear();
                    CheckBox cboxi = new CheckBox();
                    cboxi.ID = string.Format("cbox_{0}", e.Row.Cells[9].Text);
                    cboxi.Enabled = sysValue.Authority.Delete;
                    e.Row.Cells[8].Controls.Add(cboxi);
                }
                else
                {
                    e.Row.Cells[0].Controls.Clear();
                }
                #endregion
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //head btn控制項
                e.Row.Cells[8].Controls.Clear();
                Button btn = new Button();
                btn.Click += checksend_FA_Click;
                btn.Text = "刪除";
                btn.CssClass = "btnDelete2";
                btn.Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                btn.OnClientClick = "return confirm('確定要刪除已勾選資料嗎?');";
                btn.Enabled = sysValue.Authority.Delete;
                e.Row.Cells[8].Controls.Add(btn);
            }
        }
        #endregion

        #region 批次刪除
        protected void checksend_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                if (((CheckBox)GridView1.Rows[i].Cells[10].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable_dlete(GridView1.Rows[i].Cells[11].Text, GridView1.Rows[i].Cells[12].Text)));

                    //RServiceProvider rsp = DataCustomer.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            GridView1.DataBind();
        }
        protected void checksend_CM_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView2.Rows.Count; i++)
            {
                if (((CheckBox)GridView2.Rows[i].Cells[5].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(), dataToHashtable_dlete_CM(GridView2.Rows[i].Cells[6].Text, GridView2.Rows[i].Cells[7].Text)));

                    //RServiceProvider rsp = DataCustomer.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCommunication.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                        count_sucess += 1;
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            dataBind_GV2();
        }
        protected void checksend_FA_Click(object sender, EventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            int count_sucess = 0;
            int count_faile = 0;
            for (int i = 0; i < GridView3.Rows.Count; i++)
            {
                if (GridView3.Rows[i].Cells[8].Controls.Count != 0 && ((CheckBox)GridView3.Rows[i].Cells[8].Controls[0]).Checked)
                {
                    DataSet ds = new DataSet();
                    DataSet ds_CU = new DataSet();
                    //元件資料轉成Hashtable
                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), dataToHashtable_dlete_CU(GridView3.Rows[i].Cells[11].Text, GridView3.Rows[i].Cells[12].Text)));

                    //RServiceProvider rsp = DataCustomer.Delete(sysValue.emNo, sysValue.ProgId, ds);
                    RServiceProvider rsp = DataCustomer.Update(sysValue.emNo, sysValue.ProgId, ds);
                    if (rsp.Result) //執行成功
                    {
                        //元件資料轉成Hashtable
                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_dlete_FA(GridView3.Rows[i].Cells[9].Text, GridView3.Rows[i].Cells[10].Text)));

                        //RServiceProvider rsp = DataCustomer.Delete(sysValue.emNo, sysValue.ProgId, ds);
                        rsp = DataFamilyRela.Update(sysValue.emNo, sysValue.ProgId, ds);
                        if (rsp.Result) //執行成功
                            count_sucess += 1;
                        else
                            count_faile += 1;
                    }
                    else
                        count_faile += 1;
                }
            }
            MainControls.showMsg(this, string.Format("已成功刪除{0}筆資料！", count_sucess) + (count_faile != 0 ? string.Format("\\n{0}筆資料刪除失敗！", count_sucess) : ""));
            dataBind_GV3();
        }
        #endregion

        protected void GridView1_Load(object sender, EventArgs e)
        {
            GridView1.DataBind();
        }
        protected void GridView2_Load(object sender, EventArgs e)
        {
            if (hif_TransNo.Value != "")
            {
                dataBind_GV2();
            }
        }
        protected void GridView3_Load(object sender, EventArgs e)
        {
            if (hif_TransNo.Value != "")
            {
                dataBind_GV3();
            }
        }

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("CU_CustName")) return;
            e.InputParameters.Add("CU_CustName", Lib.FdVP(txtSh_CU_CustName.Text).Trim());
            e.InputParameters.Add("CU_ID", Lib.FdVP(txtSh_CU_ID.Text).Trim());
            e.InputParameters.Add("CU_Sex", ddlSh_CU_Sex.SelectedItem.Text);
            e.InputParameters.Add("CU_Age", Lib.FdVP(txtSh_CU_Age.Text).Trim());
            e.InputParameters.Add("CU_Marry", ddlSh_CU_Marry.SelectedItem.Text);
            e.InputParameters.Add("CU_CustType", ddlSh_CU_CustType.SelectedItem.Text);
            e.InputParameters.Add("CU_CustSource", ddlSh_CU_CustSource.SelectedItem.Text);
            e.InputParameters.Add("CU_ContactMode", ddlSh_CU_ContactMode.SelectedItem.Text);
            e.InputParameters.Add("CU_IsShow", "1");
            e.InputParameters.Add("Mobile", Lib.FdVP(txtSh_Mobile.Text).Trim());
            e.InputParameters.Add("Address", Lib.FdVP(txtSh_Address.Text).Trim());
            e.InputParameters.Add("PO_ComSName", Lib.FdVP(txtSh_PO_ComSName.Text).Trim());
            e.InputParameters.Add("PO_PolNo", Lib.FdVP(txtSh_PO_PolNo.Text).Trim());
            e.InputParameters.Add("PO_AccureDate_s", DateRange_PO_AccureDate.Date_start);
            e.InputParameters.Add("PO_AccureDate_e", DateRange_PO_AccureDate.Date_end);
            e.InputParameters.Add("PO_Polmmemo", Lib.FdVP(txtSh_PO_Polmmemo.Text).Trim());
            e.InputParameters.Add("AccID", Session["AccID"].ToString());
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除
            txtSh_CU_CustName.Text = string.Empty;
            txtSh_CU_ID.Text = string.Empty;
            ddlSh_CU_Sex.SelectedIndex = -1;
            txtSh_CU_Age.Text = string.Empty;
            ddlSh_CU_Marry.SelectedIndex = -1;
            ddlSh_CU_CustType.SelectedIndex = -1;
            ddlSh_CU_CustSource.SelectedIndex = -1;
            ddlSh_CU_ContactMode.SelectedIndex = -1;
            txtSh_Mobile.Text = string.Empty;
            txtSh_Address.Text = string.Empty;
            txtSh_PO_ComSName.Text = string.Empty;
            txtSh_PO_PolNo.Text = string.Empty;
            DateRange_PO_AccureDate.Init();
            txtSh_PO_Polmmemo.Text = string.Empty;
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #endregion

        protected void lnk_CommandAdd(object sender, CommandEventArgs e)
        {
            switch(e.CommandName)
            {
                case "1":
                    show_div("div_CM_edit");
                    break;
                case "2":
                    show_div("div_FA_edit");
                    break;
            }
            //預設：只出現新增按鈕
            this.maintainButtonEnabled("");
        }

        protected void ddl_CM_CLAS_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(ddl_CM_CLAS.SelectedItem.Text)
            {
                case "地址":
                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "041");
                    tr_CM_ZIP.Visible = true;
                    break;
                case "電話":
                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "042");
                    tr_CM_ZIP.Visible = false;
                    break;
                case "其他":
                    DataPhrase.DDL_TypeName(ddl_CM_TYPE, true, "", "043");
                    tr_CM_ZIP.Visible = false;
                    break;
            }

            show_div("div_CM_edit");
        }

        private void show_div(string clientID)
        {
            ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "POP", "$('#" + UpdatePanel1.ClientID + "').block({ message: $('#"+ clientID + "'), centerY: false , css: { width: '800px', cursor: null, top: '20px' }, overlayCSS: {cursor: null}  }); ", true);
        }

        private void hide_div(string id)
        {

        }
    }
}