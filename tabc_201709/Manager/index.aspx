﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Manager/MasterPage_LeftBar.master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="tabc_201709.Manager.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Label ID="lbl_PageTitle" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder4" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <triggers>
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse1" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse2" />
            <asp:AsyncPostBackTrigger ControlID="lnkBrowse3" />
        </triggers>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Panel ID="Panel_Form" runat="server">
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
