﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.Manager
{
    public partial class MyTrainYear : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Operation"));
        private static Logger logger_db = LogManager.GetLogger(string.Format("{0} (LogType:{1})", MethodInfo.GetCurrentMethod().ReflectedType.Name, "Error_FULL"));

        protected void Page_Init(object sender, EventArgs e)
        {
            //分頁模組
            PagerModul(GridView1, ObjectDataSource1, ddlPageRow, ddlPageChange, labGridViewRows, lbtnPageCtrlFirst, lbtnPageCtrlPrev, lbtnPageCtrlNext, lbtnPageCtrlLast, MainControls.GridViewPage30);
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            switch (MultiView1.ActiveViewIndex)
            {
                case 0:
                    this.SetFocus(txtSh_EY_Year);
                    Panel_Form.DefaultButton = btnSearch.ID;
                    break;
                case 1:

                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //系統參數
                for (int i = Request.Url.Segments.Length - 1; i >= 0; i--)
                {
                    if (DataMenu.PageLimitCheck(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, Session["emNo"].ToString()) && new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session).Authority.Select)
                    {
                        ViewState["SysValue"] = new SysValue(Request.Url.Segments[i].Replace("/", string.Empty) + Request.Url.Query, this.Session);
                        break;
                    }
                }
                //頁面Title文字
                lbl_PageTitle.Text = DataMenu.menuReader((ViewState["SysValue"] as SysValue).ProgId).Rows[0]["muName"].ToString();
                //GridView1顯示控制
                MainControls.GridViewStyleCtrl(GridView1, 700);
                //預設：只出現新增按鈕
                this.maintainButtonEnabled("");
                //下拉選單設定
                setddlChoices();
                //預設：瀏覽
                ChangeMultiView(0);
                //資料繫結
                dataBind();
                if (Data_tabcSales.isDM(Session["AccID"].ToString()))
                {
                    txtSh_DM.Text = Session["AccID"].ToString();
                }
                else
                {
                    txtSh_EY_AccID.Text = Session["AccID"].ToString();
                    txtSh_DM.Enabled = false;
                    txtSh_EY_AccID.Enabled = false;
                }
            }

        }

        #region 資料繫結
        private void dataBind()
        {
            DataETrainYear.DataColumn_my(this.GridView1);
        }
        #endregion

        #region 設定平台下拉選單的內容
        private void setddlChoices()
        {
            ViewState["ddlSh_EY_Unit"] = DataPhrase.DDL_TypeName(ddlSh_EY_Unit, true, "", "E008");
        }
        #endregion

        #region 資料驗証
        private string dataValid(string commandName)
        {
            StringBuilder sbError = new StringBuilder();
            

            return sbError.ToString();
        }
        private string dataValid_batch()
        {
            StringBuilder sbError = new StringBuilder();

            

            return sbError.ToString();
        }
        #endregion

        #region 選取資料事件
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearControlContext();
            //取得目前所選取的資料列
            GridViewRow gvr = GridView1.SelectedRow;
            //填入各欄位資料至欄位元件
            for (int i = 0; i < GridView1.Columns.Count; i++)
                // Type is BoundField of Column of GridView
                if (GridView1.Columns[i] is BoundField)
                {
                    BoundField bf = GridView1.Columns[i] as BoundField;
                    switch (bf.DataField)
                    {
                        case "EY_Year":
                            lbl_EY_Year.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EY_Unit":
                            DataRow dr_Unit = DataUnit.dataReader_cZON_PK(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            lbl_EY_Unit.Text = dr_Unit["cZON_PK"].ToString() + dr_Unit["cZON_NAME"].ToString();
                            break;
                        case "EY_AccID":
                            DataRow dr_sales = Data_tabcSales.DataReader_Full_AccID(MainControls.ReplaceSpace(gvr.Cells[i].Text));
                            if (dr_sales != null)
                            {
                                lbl_DM_AccID.Text = dr_sales["DM"].ToString();
                                lbl_DM_Name.Text = dr_sales["DM_Name"].ToString();
                                lbl_EY_AccID.Text = dr_sales["cSAL_FK"].ToString();
                                lbl_EY_Name.Text = dr_sales["NAME"].ToString();
                            }
                            break;
                        case "EY_Hours":
                            lbl_EY_Hours.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "EY_AttHours":
                            lbl_EY_AttHours.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                        case "LackHour":
                            lbl_LackHour.Text = MainControls.ReplaceSpace(gvr.Cells[i].Text);
                            break;
                    }
                }
            //切換至維護畫面
            ChangeMultiView(1);
            maintainButtonEnabled("Select");
        }
        #endregion

        #region 維護按鈕事件
        protected void btn_Command(object sender, CommandEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            

            if (e.CommandName != "Cancel" && e.CommandName != "Delete" && e.CommandName != "Copy")
            {
                lbl_Msg.Text = dataValid(e.CommandName); //資料格式驗証
                
            }

            switch (e.CommandName)
            {
                case "Apply": //
                    DataTable dt = Data_tabcSales.DataReader_DM(Session["AccID"].ToString()).Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow dr_EY = DataETrainYear.DataReader_EY_AccID_EY_Year(dr["cSAL_FK"].ToString(), MainControls.ServerDateTime().Year.ToString());
                        if (dr_EY == null)
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataETrainYear.GetSchema(), dataToHashtable_batch("A", "", "", dr["cSAL_FK"].ToString())));

                            RServiceProvider rsp = DataETrainYear.Append(sysValue.emNo, sysValue.ProgId, ds);
                            if (!rsp.Result)
                            {
                                logger.Error(string.Format("我的學習記錄>年度訓練時數查詢：DataETrainYear[新增]失敗：\r\n", rsp.ReturnMessage));
                                logger_db.Error(string.Format("我的學習記錄>年度訓練時數查詢：DataETrainYear[新增]失敗：\r\n", rsp.ReturnMessage));
                            }
                        }
                        else
                        {
                            DataSet ds = new DataSet();
                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataETrainYear.GetSchema(), dataToHashtable_batch("M", dr_EY["EY_TransNo"].ToString(), dr_EY["EY_UidNo"].ToString(), dr["cSAL_FK"].ToString())));

                            RServiceProvider rsp = DataETrainYear.Update(sysValue.emNo, sysValue.ProgId, ds);
                            if (!rsp.Result)
                            {
                                logger.Error(string.Format("我的學習記錄>年度訓練時數查詢：DataETrainYear[更新]失敗：\r\n", rsp.ReturnMessage));
                                logger_db.Error(string.Format("我的學習記錄>年度訓練時數查詢：DataETrainYear[更新]失敗：\r\n", rsp.ReturnMessage));
                            }
                        }
                    }
                    MainControls.showMsg(this, "執行完成！");
                    GridView1.DataBind();
                    break;
                case "Cancel": //取消
                    //瀏覽
                    ChangeMultiView(0);
                    clearControlContext();
                    break;
            }
        }
        #endregion

        #region 資料轉至hashtable
        private Hashtable dataToHashtable_batch(string ModiState, string EY_TransNo, string EY_UidNo, string EY_AccID)
        {
            Hashtable hsData = new Hashtable();

            if (ModiState == "A")
            {
                hsData.Add("EY_TransNo", System.Guid.NewGuid().ToString("N"));
                hsData.Add("EY_UidNo", Session["UidNo"].ToString());
            }
            else
            {
                hsData.Add("EY_TransNo", EY_TransNo);
                hsData.Add("EY_UidNo", EY_UidNo);
            }
            hsData.Add("EY_AccID", EY_AccID);
            DateTime finalDay = DateTime.Parse(string.Format("{0}/12/31", DateTime.Now.Year));
            DataRow dr_sales = Data_tabcSales.DataReader_AccID(EY_AccID);
            if (dr_sales != null)
            {
                hsData.Add("EY_ID", dr_sales["cSAL_ID_S"].ToString());
                hsData.Add("EY_Unit", dr_sales["Zone"].ToString());
                int iYear = DateTime.Now.Year - DateTime.Parse(dr_sales["Startdate"].ToString()).Year + 1;
                hsData.Add("EY_Times", iYear);
                DataTable dt_attend = DataEAttend.DataReader_finish(finalDay.ToString("yyyy/MM/dd"), dr_sales["cSAL_FK"].ToString()).Tables[0];
                DataRow dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "1");//壽險
                if (dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("1").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                {
                    hsData.Add("EY_Hours1", dr_erule["ER_Hours"].ToString());
                    double ED_AttHours = 0;
                    if (dt_attend != null)
                    {
                        var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                        double.TryParse(hours, out ED_AttHours);
                    }
                    hsData.Add("EY_AttHours1", ED_AttHours.ToString());
                }
                else
                    hsData.Add("EY_Hours1", "0");
                if (iYear % 5 == 0)
                    iYear = 5;
                else
                    iYear = iYear % 5;
                dr_erule = DataERule.DataReader_ER_YearNo_ER_Type(iYear.ToString(), "2");//產險
                if(dr_erule == null)
                {
                    DataTable dt_ER = DataERule.DataReader_ER_Type("2").Tables[0];
                    if (dt_ER.Rows.Count != 0)
                        dr_erule = dt_ER.Rows[0];
                }
                if (dr_erule != null)
                {
                    hsData.Add("EY_Hours2", dr_erule["ER_Hours"].ToString());
                    double ED_AttHours = 0;
                    if (dt_attend != null)
                    {
                        var hours = dt_attend.Compute("sum(ED_AttHours)", string.Format("EC_TrainType='{0}' or EC_TrainType='3'", dr_erule["ER_Type"].ToString())).ToString();
                        double.TryParse(hours, out ED_AttHours);
                    }
                    hsData.Add("EY_AttHours2", ED_AttHours.ToString());
                }
                else
                    hsData.Add("EY_Hours2", "0");
                
            }
            hsData.Add("EY_Year", MainControls.ServerDateTime().Year);
            hsData.Add("AccID", Session["AccID"].ToString());
            hsData.Add("ComyCode", Session["ComyCode"].ToString());
            hsData.Add("ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            //hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            hsData.Add("ModiState", ModiState);

            return hsData;
        }

        #endregion

        #region 清除編輯元件內容
        private void clearControlContext()
        {
            lbl_EY_Year.Text = string.Empty;
            lbl_EY_Unit.Text = string.Empty;
            lbl_DM_AccID.Text = string.Empty;
            lbl_DM_Name.Text = string.Empty;
            lbl_EY_AccID.Text = string.Empty;
            lbl_EY_Name.Text = string.Empty;
            lbl_EY_Hours.Text = string.Empty;
            lbl_EY_AttHours.Text = string.Empty;
            lbl_LackHour.Text = string.Empty;
            lbl_Msg.Text = string.Empty;


        }
        #endregion

        #region 切換維護及瀏覽
        protected void lnk_Command(object sender, CommandEventArgs e)
        {
            ChangeMultiView(Convert.ToInt16(e.CommandName));
        }

        private void ChangeMultiView(int index)
        {
            MultiView1.ActiveViewIndex = index;
            lnkBrowse.Visible = false;
            lnkMaintain0.Visible = false;
            bool isDM = Data_tabcSales.isDM(Session["AccID"].ToString());
            switch (index)
            {
                case 0:
                    clearControlContext();
                    maintainButtonEnabled("Append");
                    lnkMaintain0.Visible = (ViewState["SysValue"] as SysValue).Authority.Append & isDM;
                    break;
                case 1:
                    lnkBrowse.Visible = true;
                    break;
            }

        }
        #endregion

        #region 維護按鈕停用/啟用
        private void maintainButtonEnabled(string commandName)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            btnCancel.Visible = false;
            switch (commandName)
            {
                case "Select": //選取按鈕
                    //TODO:程式非共用修改-9
                    btnCancel.Visible = true;
                    break;
                default:
                    //TODO:程式非共用修改-10
                    btnCancel.Visible = true;
                    break;
            }
        }
        #endregion

        #region 分頁模組

        private void PagerModul(GridView grv, ObjectDataSource objDS, DropDownList ddlPageRows, DropDownList ddlPageIndex, Label labPageCount, LinkButton lbtnPageFirst, LinkButton lbtnPagePre, LinkButton lbtnPageNext, LinkButton lbtnPageLast, int pageAutoCount)
        {
            object[] controls = { grv, objDS, ddlPageRows, ddlPageIndex, labPageCount, lbtnPageFirst, lbtnPagePre, lbtnPageNext, lbtnPageLast, pageAutoCount };
            ViewState["PagerModulControls"] = controls;
            if (!IsPostBack)
            {
                ViewState["RowNo"] = 0;
                MainControls.GridViewPageSet(ddlPageRows, pageAutoCount);
                grv.AllowPaging = true;
                grv.PagerSettings.Visible = false;
                grv.PageSize = pageAutoCount;
            }
            grv.RowDataBound += GridView_RowDataBound;
            grv.DataBound += GridView_DataBound;
            objDS.Selected += ObjectDataSource_Selected;
            objDS.Selecting += ObjectDataSource_Selecting;
            ddlPageRows.SelectedIndexChanged += ddlPageRows_SelectedIndexChanged;
            ddlPageIndex.SelectedIndexChanged += ddlPageIndex_SelectedIndexChanged;
            lbtnPageFirst.Command += lbtnPageCtrl_Command;
            lbtnPagePre.Command += lbtnPageCtrl_Command;
            lbtnPageNext.Command += lbtnPageCtrl_Command;
            lbtnPageLast.Command += lbtnPageCtrl_Command;
        }

        bool chagePageRow = false;
        #region 設定每頁筆數事件
        protected void ddlPageRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageSize = Convert.ToInt16(((DropDownList)((object[])ViewState["PagerModulControls"])[2]).SelectedValue);
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;

            chagePageRow = true;
        }
        #endregion

        #region 設定共有幾頁
        private void PageChoices(GridView mGV, DropDownList PageChange)
        {
            int SelectedIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
            MainControls.GridViewPageChoices(PageChange, mGV.PageCount, 1);
            PageChoiceSet(mGV);
            if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count > SelectedIndex)
                ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = SelectedIndex;
        }
        #endregion

        #region 取得資料總筆數
        protected void ObjectDataSource_Selected(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.ReturnValue is DataSet)
                ViewState["table"] = (e.ReturnValue as DataSet).Tables[0];
            else
            {
                int count = Convert.ToInt32(e.ReturnValue);
                ((Label)((object[])ViewState["PagerModulControls"])[4]).Text = MainControls.GridViewPageCountDisplay(count);
                if (!IsPostBack || chagePageRow)
                {
                    chagePageRow = false;
                    MainControls.GridViewPageChoices((DropDownList)((object[])ViewState["PagerModulControls"])[3], MainControls.GridViewPageCount(count, Convert.ToInt16(((object[])ViewState["PagerModulControls"])[9].ToString())), 1);
                }
            }
        }
        #endregion

        #region 指定切換至 n 頁事件
        protected void ddlPageIndex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == ddlPageChange)
            {
                ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex;
                PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
            }
        }
        #endregion

        #region 換頁事件觸發處理 first.prev.next.last 等
        protected void lbtnPageCtrl_Command(object sender, CommandEventArgs e)
        {
            MainControls.GridViewChangePage(((GridView)((object[])ViewState["PagerModulControls"])[0]), e.CommandName);
            PageChoiceSet(((GridView)((object[])ViewState["PagerModulControls"])[0]));
        }
        #endregion

        #region 設定換頁功能鍵事件  PageChoiceSet(GridView mGV)
        private void PageChoiceSet(GridView mGV)
        {
            if (mGV == ((GridView)((object[])ViewState["PagerModulControls"])[0]))
            {
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = true;
                ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "pointer");
                ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "pointer");

                if (mGV.PageIndex == 0)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[5]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[6]).Style.Add("cursor", "default");
                    if (mGV.PageCount == 0)
                    {
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                        ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");
                    }
                }
                if (mGV.PageIndex == mGV.PageCount - 1)
                {
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Enabled = false;
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[7]).Style.Add("cursor", "default");
                    ((LinkButton)((object[])ViewState["PagerModulControls"])[8]).Style.Add("cursor", "default");


                }
                if (((DropDownList)((object[])ViewState["PagerModulControls"])[3]).Items.Count == 0)
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = -1;
                else
                    ((DropDownList)((object[])ViewState["PagerModulControls"])[3]).SelectedIndex = mGV.PageIndex;
            }
        }
        #endregion

        #region 設定GridView資料列顏色
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            SysValue sysValue = ViewState["SysValue"] as SysValue;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.Cells[0].Controls[0].GetType().Name == "DataControlButton")
                {
                    if (((Button)e.Row.Cells[0].Controls[0]).CommandName == "Select")
                    {
                        ((Button)e.Row.Cells[0].Controls[0]).CssClass = "btnEdit";
                        ((Button)e.Row.Cells[0].Controls[0]).Enabled = sysValue.Authority.Select;
                        ((Button)e.Row.Cells[0].Controls[0]).Style.Add("cursor", sysValue.Authority.Select ? "pointer" : "not-allowed");
                    }
                }

                switch (Convert.ToInt16(ViewState["RowNo"]))
                {
                    case 0:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVItemStyleOMout(2));
                        ViewState["RowNo"] = 1;
                        break;
                    case 1:
                        e.Row.Attributes.Add("onmouseout", MainControls.GVAlterItemStyleOMout(1));
                        e.Row.Attributes.Add("onmouseover", MainControls.GVAlterItemStyleOMout(2));
                        ViewState["RowNo"] = 0;
                        break;
                }

            }

        }
        #endregion

        #region 控制資料的分頁數
        protected void GridView_DataBound(object sender, EventArgs e)
        {
            PageChoices(((GridView)((object[])ViewState["PagerModulControls"])[0]), (DropDownList)((object[])ViewState["PagerModulControls"])[3]);
        }
        #endregion

        #region ObjectDataSource設定
        protected void ObjectDataSource_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            //無搜尋者刪除
            if (e.InputParameters.Contains("EY_Year")) return;
            e.InputParameters.Add("EY_Year", txtSh_EY_Year.Text.Trim());
            if (ddlSh_EY_Unit.SelectedIndex > 0)
                e.InputParameters.Add("EY_Unit", (ViewState["ddlSh_EY_Unit"] as DataTable).Rows[ddlSh_EY_Unit.SelectedIndex - 1]["TypeSubCode"].ToString());
            else
                e.InputParameters.Add("EY_Unit", "");
            e.InputParameters.Add("EY_AccID", txtSh_EY_AccID.Text.Trim());
            e.InputParameters.Add("DM", txtSh_DM.Text.Trim());
            
            //
        }
        #endregion

        #region 執行搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        #region 清除查詢內容
        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            //無搜尋者刪除

            ddlSh_EY_Unit.SelectedIndex = -1;
            txtSh_EY_Year.Text = string.Empty;
            txtSh_EY_AccID.Text = string.Empty;
            txtSh_DM.Text = Session["AccID"].ToString();
            //
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).PageIndex = 0;
            ((GridView)((object[])ViewState["PagerModulControls"])[0]).DataBind();
        }
        #endregion

        

        #endregion






    }
}