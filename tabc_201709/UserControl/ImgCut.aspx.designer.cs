﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------

namespace tabc_201709.UserControl {
    
    
    public partial class ImgCut {
        
        /// <summary>
        /// Head1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlHead Head1;
        
        /// <summary>
        /// Link_imgareaselect 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlLink Link_imgareaselect;
        
        /// <summary>
        /// form1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Literal_script 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal_script;
        
        /// <summary>
        /// UploadPhoto 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::CuteWebUI.UploadPersistedFile UploadPhoto;
        
        /// <summary>
        /// LabelPhotoError 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label LabelPhotoError;
        
        /// <summary>
        /// btn_cutImg 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btn_cutImg;
        
        /// <summary>
        /// Literal_ViewImg 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal Literal_ViewImg;
        
        /// <summary>
        /// hif_x 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hif_x;
        
        /// <summary>
        /// hif_y 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hif_y;
        
        /// <summary>
        /// hif_width 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hif_width;
        
        /// <summary>
        /// hif_height 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hif_height;
    }
}
