﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.UserControl
{
    public partial class SN : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Reload();
        }

        public void Reload()
        {
            img_CAPTCHA.ImageUrl = string.Format("~/UserControl/CAPTCHA.ashx?{0}", MainControls.GetDateStr());
        }
    }
}