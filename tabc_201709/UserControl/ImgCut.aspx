﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImgCut.aspx.cs" Inherits="tabc_201709.UserControl.ImgCut" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="<%: ResolveUrl("~/jQuery/jquery-2.0.3.min.js") %>" type="text/javascript"></script>
    <script src="<%: ResolveUrl("~/jQuery/jquery.imgareaselect.pack.js") %>" type="text/javascript"></script>
    <link id="Link_imgareaselect" runat="server" rel="stylesheet" type="text/css" />

</head>
<body oncontextmenu="return false">
    <form id="form1" runat="server">
<%--        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
                <asp:Literal ID="Literal_script" runat="server"></asp:Literal>
                <table cellpadding="5" cellspacing="0">
                    <tr>
                        <td>
                            <CuteWebUI:UploadPersistedFile ID="UploadPhoto" runat="server"
                        AutoUseSystemTempFolder="false" CancelText="取消" DirtyText=""
                        InsertText="選擇圖檔" ItemTextTemplate=""
                        OnFileChanged="UploadPhoto_FileChanged"
                        TempDirectory="~/UserControl/UploaderTemp" FileTypeNotSupportMsg="必須是.jpg格式">
                                <ItemLabelStyle Font-Size="Small" />
                                <ProgressLabelStyle Font-Size="Small" />
                                <ValidateOption AllowedFileExtensions="jpg" />
                            </CuteWebUI:UploadPersistedFile>
                            <asp:Label ID="LabelPhotoError" runat="server"
                        ForeColor="Red" Font-Size="Small"></asp:Label>
                        </td>
                        <td>
                            <asp:Button ID="btn_cutImg" runat="server" Text="另存影像" OnClick="btn_cutImg_Click" Font-Names="微軟正黑體" Font-Size="Large" Visible="False" />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:Literal ID="Literal_ViewImg" runat="server"></asp:Literal>
                <asp:HiddenField ID="hif_x" runat="server" />
                <asp:HiddenField ID="hif_y" runat="server" />
                <asp:HiddenField ID="hif_width" runat="server" />
                <asp:HiddenField ID="hif_height" runat="server" />
<%--            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btn_cutImg" />
            </Triggers>
        </asp:UpdatePanel>--%>

    </form>
</body>
</html>