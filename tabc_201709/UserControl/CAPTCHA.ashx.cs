﻿using System;
using System.Web;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web.SessionState;

namespace tabc_201709.UserControl
{
    /// <summary>
    /// CAPTCHA 的摘要描述
    /// </summary>
    public class CAPTCHA : IHttpHandler, IReadOnlySessionState
    {

        // 列舉 Windows XP/Server 2003 繁體中文版所內建的字型清單。
        // （使用分號分隔這些字型）。
        string _fontList = "標楷體;新細明體;細明體;SimHei;SimSum";

        //' 列舉 Windows XP/Server 2003 簡體中文版所內建的字型清單。
        //' （使用分號分隔這些字型）。
        //Dim _fontList As String = _
        //    "楷体;仿宋体;宋体;黑体"

        // 指定各別預設的參數。
        // ===========================
        // 驗證碼圖片的高度。建議不要小於 30 個像素。
        int _height = 27;
        // 驗證碼圖片的寬度。建議一個字以 40 個像素為計算的基準。
        int _width = 100;
        // 亂數產生器。
        private static Random _rand = new Random();
        // 驗證碼的文字長度。
        private static int _randomTextLength = 5;
        // 呼叫自訂的 GenerateRandomText() 函式來產生驗證碼的文字字串。
        string _randomText = GenerateRandomText();
        // 扭曲字型的程度。
        FontTwistLevel _fontTwist = FontTwistLevel.Extreme;
        // 背景雜點的程度。
        BackgroundNoiseLevel _backgroundNoise = BackgroundNoiseLevel.Extreme;
        // 線條雜訊的程度。
        LineNoiseLevel _lineNoise = LineNoiseLevel.Extreme;
        // 宣告列舉型別並定義其成員值。
        // ===========================
        // 列舉扭曲字型的程度。
        public enum FontTwistLevel
        {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        // 列舉背景雜點的程度。
        public enum BackgroundNoiseLevel
        {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        // 列舉線條雜訊的程度。
        public enum LineNoiseLevel
        {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        // 宣告屬性。
        // ===========================
        // 驗證碼圖形的高度（以像素為單位）。
        private int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        // 驗證碼圖形的寬度屬性（以像素為單位）。
        private int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        // 扭曲字型程度的屬性。
        private FontTwistLevel FontTwist
        {
            get { return _fontTwist; }
            set { _fontTwist = value; }
        }

        // 背景雜點程度的屬性。
        private BackgroundNoiseLevel BackgroundNoise
        {
            get { return _backgroundNoise; }
            set { _backgroundNoise = value; }
        }

        // 線條雜訊程度的屬性。
        private LineNoiseLevel LineNoise
        {
            get { return _lineNoise; }
            set { _lineNoise = value; }
        }


        // 宣告函式。
        // ===========================
        // 隨機取得字型清單中的某個字型。
        string[] static_GetRandomFontFamily_fontFamily;
        private string GetRandomFontFamily()
        {
            // 檢查陣列 fontFamily 是否已經存在。
            // 若不存在，則將對字型清單進行剖析。
            // 若存在，則無須重新剖析，以便增加執行的速度。     
            if (static_GetRandomFontFamily_fontFamily == null)
            {
                static_GetRandomFontFamily_fontFamily = _fontList.Split(';');
            }
            return static_GetRandomFontFamily_fontFamily[_rand.Next(0, static_GetRandomFontFamily_fontFamily.Length)];
        }

        // 隨機產生驗證碼所需的文字字串。
        private static string GenerateRandomText()
        {
            StringBuilder sb = new StringBuilder(_randomTextLength);
            // 驗證碼所需的文字字串庫。
            string randomTextPool = "";
            int maxLength = 0;

            // 從資料庫中，取得文字字串。
            // 取得 web.config 中的資料庫連線字串設定來建立 SQL 連線物件。
            randomTextPool = "0123456789";
            //randomTextPool = "0123456789";
            maxLength = randomTextPool.Length;

            // 隨機取得所需的文字字串。
            for (int i = 0; i <= _randomTextLength - 1; i++)
            {
                sb.Append(randomTextPool.Substring(_rand.Next(maxLength), 1));
            }
            return sb.ToString();
        }

        // 建立兩個名稱為 RandomPoint 的多載函式，會回傳 PointF 資料型別。
        // 在所指定的範圍內，隨機產生 X 軸與 Y 軸的值。
        private PointF RandomPoint(int xMin, int xMax, ref int yMin, ref int yMax)
        {
            return new PointF(_rand.Next(xMin, xMax), _rand.Next(yMin, yMax));
        }

        // 在所指定的矩型範圍內，隨機產生 X 軸與 Y 軸的值。
        private PointF RandomPoint(Rectangle rect)
        {
            int tect_top = rect.Top;
            int tect_bottom = rect.Bottom;
            return RandomPoint(rect.Left, rect.Width, ref tect_top, ref tect_bottom);
        }

        // 建立內含所指定之文字字串與字型的 GraphicsPath 類別，
        // 用來傳回一系列連接的直線和曲線。
        private GraphicsPath TextPath(string str, Font fnt, Rectangle rect)
        {
            StringFormat sf = new StringFormat();
            // 指定文字字串對齊方式。
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Near;

            GraphicsPath gp = new GraphicsPath();
            gp.AddString(str, fnt.FontFamily, Convert.ToInt32(fnt.Style), fnt.Size, rect, sf);
            return gp;
        }

        // 指定文字字串之字型、大小與樣式。
        private Font GetFontStyle()
        {
            float fontSize = 0;
            string fontName = GetRandomFontFamily();

            // 根據扭曲字型的程度來指定字型大小。
            switch (this.FontTwist)
            {
                case FontTwistLevel.None:
                    fontSize = Convert.ToInt32(_height * 0.7);
                    break;
                case FontTwistLevel.Low:
                    fontSize = Convert.ToInt32(_height * 0.75);
                    break;
                case FontTwistLevel.Medium:
                    fontSize = Convert.ToInt32(_height * 0.8);
                    break;
                case FontTwistLevel.High:
                    fontSize = Convert.ToInt32(_height * 0.85);
                    break;
                case FontTwistLevel.Extreme:
                    fontSize = Convert.ToInt32(_height * 0.9);
                    break;
            }

            FontStyle fontStyle = default(FontStyle);
            // 隨機指定字型的樣式。
            switch (_rand.Next(5))
            {
                case 0:
                    fontStyle = FontStyle.Bold;
                    break;
                case 1:
                    fontStyle = FontStyle.Italic;
                    break;
                case 2:
                    fontStyle = FontStyle.Regular;
                    break;
                case 3:
                    fontStyle = FontStyle.Strikeout;
                    break;
                case 4:
                    fontStyle = FontStyle.Underline;
                    break;
            }

            return new Font(fontName, fontSize, fontStyle);
        }

        //Private Function SetSession(ByVal context As HttpContext) As Boolean
        //    With context
        //        .Session("CAPTCHA") = _randomText
        //        If .Session("CAPTCHA").ToString <> _randomText Then
        //            Return True
        //        Else
        //            Return False
        //        End If
        //    End With
        //End Function


        // 扭曲文字。
        private void TwistText(GraphicsPath textPath, Rectangle rect)
        {
            // 扭曲文字的大小差異數。
            float twistDivisor = 0;
            // 扭曲文字的範圍變異數。
            float rangeModifier = 0;

            switch (this.FontTwist)
            {
                case FontTwistLevel.None:
                    return;
                case FontTwistLevel.Low:
                    twistDivisor = 6;
                    rangeModifier = (float)1.1;
                    break;
                case FontTwistLevel.Medium:
                    twistDivisor = (float)5.5;
                    rangeModifier = (float)1.2;
                    break;
                case FontTwistLevel.High:
                    twistDivisor = 5;
                    rangeModifier = (float)1.3;
                    break;
                case FontTwistLevel.Extreme:
                    twistDivisor = (float)4.5;
                    rangeModifier = (float)1.4;
                    break;
            }

            // 使用四個為一組的浮點數值 (Floating-Point Number)，用來表示矩形的位置和大小。
            RectangleF rectF = new RectangleF(Convert.ToSingle(rect.Left), 0, Convert.ToSingle(rect.Width), rect.Height);

            // 計算扭曲文字所需的位置大小。
            int hRange = Convert.ToInt32(rect.Height / twistDivisor);
            int wRange = Convert.ToInt32(rect.Width / twistDivisor);
            int left = rect.Left - Convert.ToInt32(wRange * rangeModifier);
            int top = rect.Top - Convert.ToInt32(hRange * rangeModifier);
            int width = rect.Left + rect.Width + Convert.ToInt32(wRange * rangeModifier);
            int height = rect.Top + rect.Height + Convert.ToInt32(hRange * rangeModifier);

            // 確保扭曲後的字不會偏離驗證碼所在的底圖。
            if (left < 0)
                left = 0;
            if (top < 0)
                top = 0;
            if (width > this.Width)
                width = this.Width;
            if (height > this.Height)
                height = this.Height;

            // 隨機指定扭曲後的字所在的位置。
            int leftTop_yMax = top + hRange;
            int rightTop_yMax = top + hRange;
            int leftBottom_yMin = height - hRange;
            int rightBottom_yMiin = height - hRange;
            PointF leftTop = RandomPoint(left, left + wRange, ref top, ref leftTop_yMax);
            PointF rightTop = RandomPoint(width - wRange, width, ref top, ref rightTop_yMax);
            PointF leftBottom = RandomPoint(left, left + wRange, ref leftBottom_yMin, ref height);
            PointF rightBottom = RandomPoint(width - wRange, width, ref rightBottom_yMiin, ref height);
            // 定義一個 PointF 結構的陣列。
            PointF[] pF = new PointF[] {
			leftTop,
			rightTop,
			leftBottom,
			rightBottom
		};
            // 建立 3 x 3 仿射矩陣。
            Matrix mtrx = new Matrix();
            // 以 0 來轉換這個 Matrix 的 X 與 Y 值。
            mtrx.Translate(0f, 0f);
            // 使用平行四邊形和矩形所定義的透視點彎曲，來轉換這個 GraphicsPath，
            // 以便建立奇特的形狀，達到扭曲文字的目的。
            textPath.Warp(pF, rectF, mtrx, WarpMode.Perspective, 0.5f);
        }

        // 加入背景雜點。
        private void AddNoise(Graphics graph, Rectangle rect)
        {
            int density = 0;
            int size = 0;

            switch (this.BackgroundNoise)
            {
                case BackgroundNoiseLevel.None:
                    return;
                case BackgroundNoiseLevel.Low:
                    density = 100;
                    size = 60;
                    break;
                case BackgroundNoiseLevel.Medium:
                    density = 75;
                    size = 50;
                    break;
                case BackgroundNoiseLevel.High:
                    density = 50;
                    size = 45;
                    break;
                case BackgroundNoiseLevel.Extreme:
                    density = 30;
                    size = 45;
                    break;
            }

            // 使用複合模式控制 Alpha 混色。
            SolidBrush colorBrush = new SolidBrush(Color.Aqua);

            // 取得所傳入的矩型之寬度與高度最大者。
            int max = Convert.ToInt32(Math.Max(rect.Width, rect.Height) / size);

            for (int i = 0; i <= Convert.ToInt32((rect.Width * rect.Height) / density); i++)
            {
                // 自動指定筆刷顏色。
                colorBrush.Color = Color.FromArgb(_rand.Next(255), _rand.Next(255), _rand.Next(255));
                // 填滿由座標對、寬度與高度指定的邊框所定義的橢圓形內部，來產生雜點。
                graph.FillEllipse(colorBrush, _rand.Next(rect.Width), _rand.Next(rect.Height), _rand.Next(max), _rand.Next(max));
            }
            colorBrush.Dispose();
        }

        // 加入線條雜訊。
        private void AddLine(Graphics graph, Rectangle rect)
        {
            int length = 0;
            int totalLines = 0;
            float penWidth = 0;

            switch (this.LineNoise)
            {
                case LineNoiseLevel.None:
                    return;
                case LineNoiseLevel.Low:
                    length = 3;
                    totalLines = 1;
                    penWidth = Convert.ToSingle(_height / 35);
                    break;
                case LineNoiseLevel.Medium:
                    length = 4;
                    totalLines = 2;
                    penWidth = Convert.ToSingle(_height / 30);
                    break;
                case LineNoiseLevel.High:
                    length = 5;
                    totalLines = 3;
                    penWidth = Convert.ToSingle(_height / 25);
                    break;
                case LineNoiseLevel.Extreme:
                    length = 6;
                    totalLines = 3;
                    penWidth = Convert.ToSingle(_height / 20);
                    break;
            }

            Color penColor = default(Color);
            PointF[] pF = new PointF[length + 1];
            Pen randomColorPen = null;

            for (int i = 1; i <= totalLines; i++)
            {
                for (int intLoop = 0; intLoop <= length; intLoop++)
                {
                    pF[intLoop] = RandomPoint(rect);
                }
                // 自動指定筆的顏色。
                penColor = Color.FromArgb(_rand.Next(255), _rand.Next(255), _rand.Next(255));
                randomColorPen = new Pen(penColor, penWidth);
                // 指定曲線的張力為 1.2F，來繪製曲線。
                graph.DrawCurve(randomColorPen, pF, 1.2f);
            }

            randomColorPen.Dispose();
        }

        // 繪製驗證碼的主程式。
        // ===========================
        public void ProcessRequest(HttpContext context)
        {
            // 建立 Bitmap。
            using (Bitmap objBitmap = new Bitmap(_width, _height, PixelFormat.Format32bppArgb))
            {
                // 建立畫布。
                using (Graphics objGraphics = Graphics.FromImage(objBitmap))
                {
                    // 指定使用平滑化處理（又稱為反鋸齒功能）。
                    objGraphics.SmoothingMode = SmoothingMode.AntiAlias;

                    // 定義一個矩型作為顯示驗證碼之用。
                    Rectangle rect = new Rectangle(0, 0, _width, _height);

                    // 指定從頂點到底點往右斜的斜線花紋、
                    // 前景顏色為黃色、背景顏色為白色的筆刷。
                    HatchBrush hBr = new HatchBrush(HatchStyle.Weave, Color.FromArgb(225, 225, 225), Color.White);
                    // 建立一個矩型底圖。
                    objGraphics.FillRectangle(hBr, rect);
                    hBr.Dispose();

                    // 文字字串偏移值。
                    int charOffset = 0;
                    // 每個文字的寬度。
                    double charWidth = _width / _randomTextLength;
                    // 將每個文字視為一個矩型，便於扭曲該字。
                    Rectangle rectChar = default(Rectangle);
                    // 所欲使用的字型樣式。
                    Font fnt = null;

                    //使用黑色筆刷來將文字繪製於矩型中，然後扭曲該矩型，
                    //最後把該矩型填入原本的底圖之中。
                    using (Brush br = new SolidBrush(Color.FromArgb(0, 0, 0)))
                    {
                        foreach (char ch in _randomText)
                        {
                            string textStr = ch.ToString();
                            fnt = GetFontStyle();
                            //FontStyle fntStyle = default(FontStyle);
                            //fntStyle = FontStyle.Bold;
                            //fnt = new Font("Gungsuh", 20, fntStyle);
                            rectChar = new Rectangle(Convert.ToInt32(charOffset * charWidth), 2, Convert.ToInt32(charWidth), _height);

                            GraphicsPath gp = TextPath(textStr, fnt, rectChar);
                            //TwistText(gp, rectChar)
                            objGraphics.FillPath(br, gp);
                            charOffset += 1;
                        }
                    }

                    // 加上背景雜點與線條雜訊
                    AddNoise(objGraphics, rect);
                    // 加上背景線條雜訊
                    AddLine(objGraphics, rect);


                    // 設定傳回的網頁型態，並指定當圖形繪製完成之後，再將結果傳回到前端網頁。
                    var _with1 = context.Response;
                    _with1.ContentType = "Image/Jpeg";
                    _with1.Clear();
                    // 設定 Cache-Control: no-cache 標頭。
                    _with1.Cache.SetCacheability(HttpCacheability.NoCache);
                    _with1.BufferOutput = true;
                    _with1.Flush();
                    objBitmap.Save(_with1.OutputStream, ImageFormat.Jpeg);

                    HttpApplication app = context.ApplicationInstance;

                    // 取得獨一的 GUID 值，用來快取所產生的驗證碼。
                    //Dim guid As String = app.Request.QueryString("guid")
                    //Guid guid = Guid.NewGuid();
                    //Dim strGuid As String = guid.ToString()
                    //string strGuid = "F1BC2AA3-7622-41E1-92C8-0452A26E1793";

                    //if (!string.IsNullOrEmpty(strGuid))
                    //{
                    //    HttpRuntime.Cache.Insert(strGuid, _randomText);
                    //}

                    context.Session["F1BC2AA3-7622-41E1-92C8-0452A26E1793"] = _randomText;

                    objGraphics.Dispose();
                }
                //objGraphics
                objBitmap.Dispose();
            }
            //objBitmap
        }

        public bool IsReusable
        {
            get { return false; }
        }

    }
}