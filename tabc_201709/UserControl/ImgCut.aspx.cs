﻿using CuteWebUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709.UserControl
{
    public partial class ImgCut : System.Web.UI.Page
    {
        private int cutWidth, cutHeight;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Link_imgareaselect.Attributes.Add("href", ResolveUrl("~/css/imgareaselect-default.css"));

                //登入驗證
                if (DataEmployee.emNoValid(Session["emNo"].ToString().Trim()))
                {
                    //關閉式窗
                    ScriptManager.RegisterStartupScript(this, this.GetType(), MainControls.random("", 8), "window.opener=null; window.open('', '_self', ''); window.close();", true);
                    return;
                }

                if (string.IsNullOrEmpty(Request.QueryString["w"]) || string.IsNullOrEmpty(Request.QueryString["h"]))
                {
                    UploadPhoto.Visible = false;
                    return;
                }
                else
                {
                    if (int.TryParse(Request.QueryString["w"].ToString(), out cutWidth) && int.TryParse(Request.QueryString["h"].ToString(), out cutHeight))
                    {
                        hif_x.Value = "0";
                        hif_y.Value = "0";
                        hif_width.Value = cutWidth.ToString();
                        hif_height.Value = cutHeight.ToString();

                        UploadPhoto.Visible = true;
                        Literal_script.Text = ScriptInit();
                    }
                    else
                    {
                        UploadPhoto.Visible = false;
                        return;
                    }
                }
            }
        }

        private string ScriptInit()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<script type=\"text/javascript\">");
            sb.Append("$(function () {");
            sb.Append("var ias = $('#photo').imgAreaSelect({");
            sb.Append("persistent: true,");
            sb.Append("show: true,");
            sb.Append("handles: true,");
            sb.AppendFormat("aspectRatio: '{0}:{1}',", cutWidth, cutHeight);
            sb.AppendFormat("x1: 0, y1: 0, x2: {0}, y2: {1},", cutWidth, cutHeight);
            sb.AppendFormat("minWidth: {0},", cutWidth);
            sb.AppendFormat("minHeight: {0},", cutHeight);
            sb.Append("fadeSpeed: 200,");
            sb.Append("onSelectEnd: function (img, selection) {");
            sb.AppendFormat("$('#hif_x').val(selection.x1);", hif_x.ClientID);
            sb.AppendFormat("$('#hif_y').val(selection.y1);", hif_y.ClientID);
            sb.AppendFormat("$('#hif_width').val(selection.width);", hif_width.ClientID);
            sb.AppendFormat("$('#hif_height').val(selection.height);", hif_height.ClientID);
            sb.Append("}");
            sb.Append("});");
            sb.Append("});");
            sb.Append("</script>");

            return sb.ToString();
        }

        protected void UploadPhoto_FileChanged(object sender, PersistedFileEventArgs args)
        {
            if (int.TryParse(Request.QueryString["w"].ToString(), out cutWidth) && int.TryParse(Request.QueryString["h"].ToString(), out cutHeight))
            {
                System.IO.Stream fs = args.File.OpenStream();
                System.Drawing.Image Image = System.Drawing.Image.FromStream(fs);
                int width = Image.Width;
                int height = Image.Height;
                fs.Close();
                if (width < cutWidth || height < cutHeight)
                {
                    MainControls.showMsg(this, string.Format("圖檔必須是 {0} * {1} px 以上", cutWidth, cutHeight));
                    LabelPhotoError.Text = string.Empty;
                    Literal_ViewImg.Text = string.Empty;
                    btn_cutImg.Visible = false;

                }
                else
                {
                    if (Math.Max(width, height) < 800)
                        LabelPhotoError.Text = Lib.ReSize(args, 800, 800);
                    else
                        LabelPhotoError.Text = Lib.ReSize(args, Math.Max(width, height), Math.Max(width, height));

                    Literal_ViewImg.Text = string.Format("圈選影像範圍：<br><img id=\"photo\" src=\"{0}\" />", ResolveUrl(string.Format("ShowUploadPhoto.ashx?Guid={0}", args.FileGuid)));
                    btn_cutImg.Visible = true;
                }
            }
        }

        protected void btn_cutImg_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hif_x.Value) || string.IsNullOrEmpty(hif_y.Value) || string.IsNullOrEmpty(hif_width.Value) || string.IsNullOrEmpty(hif_height.Value))
            {
                MainControls.showMsg(this, "請圈選影像範圍");
                return;
            }
            int x = Convert.ToInt32(hif_x.Value);
            int y = Convert.ToInt32(hif_y.Value);
            int width = Convert.ToInt32(hif_width.Value);
            int height = Convert.ToInt32(hif_height.Value);
            //儲存路徑
            //string strSavePicUrl = Server.MapPath("../Web/images/page_bg_cut.jpg");
            System.IO.Stream fs = UploadPhoto.File.OpenStream();
            System.Drawing.Image newImage = System.Drawing.Image.FromStream(fs);
            Rectangle fromR = new Rectangle(x, y, width, height);
            Rectangle toR = new Rectangle(0, 0, width, height);
            GraphicsUnit units = GraphicsUnit.Pixel;
            System.Drawing.Image pickedImage = new System.Drawing.Bitmap(toR.Width, toR.Height);
            System.Drawing.Graphics g = Graphics.FromImage(pickedImage);
            g.Clear(Color.White);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            g.DrawImage(newImage, toR, fromR, units);
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = default(ImageCodecInfo);
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType == "image/jpeg")
                {
                    ici = codec;
                }
            }
            EncoderParameters ep = new EncoderParameters();
            ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, Convert.ToInt64(100));

            MemoryStream ms = new MemoryStream();
            pickedImage.Save(ms, ImageFormat.Jpeg);

            //另存
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpContext.Current.Server.UrlPathEncode(UploadPhoto.File.FileName)));
            HttpContext.Current.Response.BinaryWrite(ms.ToArray());
            ms.Close();
            ms.Dispose();
            //存目錄
            //pickedImage.Save(strSavePicUrl, ici, ep);
            //輸出
            //pickedImage.Save(Response.OutputStream, ici, ep);
            //Response.OutputStream.Close();
            //Response.Flush();


            pickedImage.Dispose();
            g.Dispose();
            newImage.Dispose();
            fs.Close();
        }
    }
}