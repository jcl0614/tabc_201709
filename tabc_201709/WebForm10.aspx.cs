﻿using AppCode.Lib;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace tabc_201709
{
    public partial class WebForm10 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["ImportMsg"] = "";
        }
        protected static StringBuilder ImportMsg = new StringBuilder();
        protected void Button4_Click(object sender, EventArgs e)
        {
            
            MainControls.showMsg(this, "執行完成！");
        }

        //[System.Web.Services.WebMethod]
        //public static void Import()
        //{
        //    HttpContext.Current.Session["ImportMsg"] = "";
        //    ImportMsg.Append("執行中．．．");
        //    HttpContext.Current.Session["UidNo"] = "11526";
        //    HttpContext.Current.Session["AccID"] = "TB0232";
        //    for (int i = 1; i <= 3; i++)
        //    {
        //        DataTable dt_Policy = getAPI_Policy(i.ToString(), HttpContext.Current.Session["AccID"].ToString());
        //        foreach (DataRow dr_Policy in dt_Policy.Rows)
        //        {
        //            List<DataTable> dtList_PolicyDetail = getAPI_PolicyDetail(i.ToString(), dr_Policy["保單代號"].ToString(), HttpContext.Current.Session["AccID"].ToString());
        //            if (dtList_PolicyDetail.Count != 0)
        //            {
        //                //保單明細
        //                DataRow dr_PolicyDetail = dtList_PolicyDetail[0].Rows[0];
        //                //保單險種
        //                DataTable dt_PolicyDetail_Pd = dtList_PolicyDetail[1];
        //                if (dr_PolicyDetail != null)
        //                {
        //                    DataSet ds = new DataSet();
        //                    RServiceProvider rsp = new RServiceProvider();
        //                    //匯入-客戶資料(要保人)
        //                    DataRow dr_CU = DataCustomer.DataReader_CU_ID_CU_CustName(dr_PolicyDetail["要保人身分證"].ToString(), dr_PolicyDetail["要保人"].ToString(), dr_PolicyDetail["要保人"].ToString() == dr_PolicyDetail["被保人"].ToString() ? "1" : "0");
        //                    Hashtable ht_CU = dataToHashtable_Customer(dr_PolicyDetail, dr_CU, true);
        //                    ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), ht_CU));
        //                    if (dr_CU == null)//新增
        //                        rsp = DataCustomer.Append("00000000", "CustomerImport", ds);
        //                    else//更新
        //                        rsp = DataCustomer.Update("00000000", "CustomerImport", ds);
        //                    if (rsp.Result) //執行成功
        //                    {
        //                        ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 客戶資料：要保人({1})[{2}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["要保人"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                        ds = new DataSet();
        //                        //匯入-家庭關係
        //                        DataRow dr_FA = DataFamilyRela.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString());
        //                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_FamilyRela(dr_PolicyDetail, ht_CU, dr_FA)));
        //                        if (dr_FA == null)//新增
        //                            rsp = DataFamilyRela.Append("00000000", "CustomerImport", ds);
        //                        else
        //                            rsp = DataFamilyRela.Update("00000000", "CustomerImport", ds);
        //                        if (rsp.Result) //執行成功
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 家庭關係：要保人({1})[{2}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["要保人"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                        }
        //                        else
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 家庭關係：要保人({1})[{2}]失敗！<br>　　　　{3}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["要保人"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                        }
        //                        ds = new DataSet();
        //                        //匯入-通訊資料(收費地址)
        //                        DataRow dr_CM = DataCommunication.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString(), "地址", "收費地址", dr_PolicyDetail["收費郵遞區號"].ToString(), dr_PolicyDetail["收費地址"].ToString());
        //                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(),
        //                            dataToHashtable_Communication(dr_PolicyDetail, ht_CU, dr_CM, "地址", "收費地址", dr_PolicyDetail["收費郵遞區號"].ToString(), dr_PolicyDetail["收費地址"].ToString())));
        //                        if (dr_CM == null)//新增
        //                            rsp = DataCommunication.Append("00000000", "CustomerImport", ds);
        //                        else//更新
        //                            rsp = DataCommunication.Update("00000000", "CustomerImport", ds);
        //                        if (rsp.Result) //執行成功
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 通訊資料-收費地址：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                        }
        //                        else
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 通訊資料-收費地址：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                        }
        //                        //匯入-通訊資料(通訊地址)
        //                        if (dtList_PolicyDetail[0].Columns.Contains("戶籍地址") && dr_PolicyDetail["戶籍地址"].ToString() != "")
        //                        {
        //                            ds = new DataSet();
        //                            dr_CM = DataCommunication.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString(), "地址", "通訊地址", dr_PolicyDetail["戶籍郵遞區號"].ToString(), dr_PolicyDetail["戶籍地址"].ToString());
        //                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(),
        //                                dataToHashtable_Communication(dr_PolicyDetail, ht_CU, dr_CM, "地址", "通訊地址", dr_PolicyDetail["戶籍郵遞區號"].ToString(), dr_PolicyDetail["戶籍地址"].ToString())));
        //                            if (dr_CM == null)//新增
        //                                rsp = DataCommunication.Append("00000000", "CustomerImport", ds);
        //                            else//更新
        //                                rsp = DataCommunication.Update("00000000", "CustomerImport", ds);
        //                            if (rsp.Result) //執行成功
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 通訊資料-通訊地址：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            }
        //                            else
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 通訊資料-通訊地址：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                            }
        //                        }
        //                        //匯入-通訊資料(聯絡電話1)
        //                        if (dr_PolicyDetail["聯絡電話一"].ToString() != "")
        //                        {
        //                            ds = new DataSet();
        //                            dr_CM = DataCommunication.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString(), "電話", "聯絡電話1", "", dr_PolicyDetail["聯絡電話一"].ToString());
        //                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(),
        //                                dataToHashtable_Communication(dr_PolicyDetail, ht_CU, dr_CM, "電話", "聯絡電話1", "", dr_PolicyDetail["聯絡電話一"].ToString())));
        //                            if (dr_CM == null)//新增
        //                                rsp = DataCommunication.Append("00000000", "CustomerImport", ds);
        //                            else//更新
        //                                rsp = DataCommunication.Update("00000000", "CustomerImport", ds);
        //                            if (rsp.Result) //執行成功
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 通訊資料-聯絡電話1：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            }
        //                            else
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 通訊資料-聯絡電話1：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                            }
        //                        }
        //                        //匯入-通訊資料(聯絡電話2)
        //                        if (dr_PolicyDetail["聯絡電話二"].ToString() != "")
        //                        {
        //                            ds = new DataSet();
        //                            dr_CM = DataCommunication.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString(), "電話", "聯絡電話2", "", dr_PolicyDetail["聯絡電話二"].ToString());
        //                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataCommunication.GetSchema(),
        //                                dataToHashtable_Communication(dr_PolicyDetail, ht_CU, dr_CM, "電話", "聯絡電話2", "", dr_PolicyDetail["聯絡電話二"].ToString())));
        //                            if (dr_CM == null)//新增
        //                                rsp = DataCommunication.Append("00000000", "CustomerImport", ds);
        //                            else//更新
        //                                rsp = DataCommunication.Update("00000000", "CustomerImport", ds);
        //                            if (rsp.Result) //執行成功
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 通訊資料-聯絡電話2：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            }
        //                            else
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 通訊資料-聯絡電話2：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                            }
        //                        }
        //                        ds = new DataSet();
        //                        //匯入-保單資料
        //                        DataRow dr_PO = DataCustPolicy.DataReader_PO_PKSKey(dr_PolicyDetail["保單代號"].ToString());
        //                        Hashtable ht_PO = dataToHashtable_CustPolicy(dr_PolicyDetail, ht_CU, dr_PO);
        //                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPolicy.GetSchema(), ht_PO));
        //                        if (dr_PO == null)//新增
        //                            rsp = DataCustPolicy.Append("00000000", "CustomerImport", ds);
        //                        else//更新
        //                            rsp = DataCustPolicy.Update("00000000", "CustomerImport", ds);
        //                        if (rsp.Result) //執行成功
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 保單資料：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            //匯入-險種資料
        //                            foreach (DataRow PolicyDetail_Pd in dt_PolicyDetail_Pd.Rows)
        //                            {
        //                                ds = new DataSet();
        //                                DataRow dr_PD = DataCustPdt.DataReader_PD_PKSKey(PolicyDetail_Pd["明細代號"].ToString());
        //                                ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustPdt.GetSchema(), dataToHashtable_CustPdt(dr_PolicyDetail, PolicyDetail_Pd, ht_CU, ht_PO, dr_PD)));
        //                                if (dr_PD == null)//新增
        //                                    rsp = DataCustPdt.Append("00000000", "CustomerImport", ds);
        //                                else//更新
        //                                    rsp = DataCustPdt.Update("00000000", "CustomerImport", ds);
        //                                if (rsp.Result) //執行成功
        //                                {
        //                                    ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 險種資料：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                                }
        //                                else
        //                                {
        //                                    ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 險種資料：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 保單資料：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                        }
        //                        if (i == 3) //產險
        //                        {
        //                            ds = new DataSet();
        //                            //匯入-產險保單資料
        //                            DataRow dr_IP = DataCustIndustPolm.DataReader_CU_TransNo_IP_Polno_IP_ComSname(ht_CU["CU_TransNo"].ToString(), dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["保險公司名稱"].ToString());
        //                            Hashtable ht_IP = dataToHashtable_CustIndustPolm(dr_PolicyDetail, dt_PolicyDetail_Pd.Rows[0], ht_CU, dr_IP);
        //                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustIndustPolm.GetSchema(), ht_IP));
        //                            if (dr_IP == null)//新增
        //                                rsp = DataCustIndustPolm.Append("00000000", "CustomerImport", ds);
        //                            else//更新
        //                                rsp = DataCustIndustPolm.Update("00000000", "CustomerImport", ds);
        //                            if (rsp.Result) //執行成功
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 產險保單資料：[{1}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            }
        //                            else
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 產險保單資料：[{1}]失敗！<br>　　　　{2}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                            }
        //                        }

        //                    }
        //                    else
        //                    {
        //                        ImportMsg.Insert(0, string.Format("<span style=\"color:#cc0000\">保單號碼({0}) -> 客戶資料：要保人({1})[{2}]失敗！<br>　　　　{3}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["要保人"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                    }
        //                    ds = new DataSet();
        //                    //匯入-客戶資料(被保人)
        //                    if (dr_PolicyDetail["要保人"].ToString() != dr_PolicyDetail["被保人"].ToString())//本人
        //                    {
        //                        dr_CU = DataCustomer.DataReader_CU_ID_CU_CustName(dr_PolicyDetail["被保人身分證"].ToString(), dr_PolicyDetail["被保人"].ToString(), "0");
        //                        ht_CU = dataToHashtable_Customer(dr_PolicyDetail, dr_CU, false);
        //                        ds.Tables.Add(MainControls.UpLoadToDataTable(DataCustomer.GetSchema(), ht_CU));
        //                        if (dr_CU == null)//新增
        //                            rsp = DataCustomer.Append("00000000", "CustomerImport", ds);
        //                        else//更新
        //                            rsp = DataCustomer.Update("00000000", "CustomerImport", ds);
        //                        if (rsp.Result) //執行成功
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 客戶資料：被保人({1})[{2}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["被保人"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            ds = new DataSet();
        //                            //匯入-家庭關係
        //                            DataRow dr_FA = DataFamilyRela.DataReader_CU_TransNo(ht_CU["CU_TransNo"].ToString());
        //                            ds.Tables.Add(MainControls.UpLoadToDataTable(DataFamilyRela.GetSchema(), dataToHashtable_FamilyRela(dr_PolicyDetail, ht_CU, dr_FA)));
        //                            if (dr_FA == null)//新增
        //                                rsp = DataFamilyRela.Append("00000000", "CustomerImport", ds);
        //                            else//更新
        //                                rsp = DataFamilyRela.Update("00000000", "CustomerImport", ds);
        //                            if (rsp.Result) //執行成功
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span style=\"color:#009933\">保單號碼({0}) -> 家庭關係：被保人({1})[{2}]完成！<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["被保人"].ToString(), dr_CU == null ? "新增" : "更新"));
        //                            }
        //                            else
        //                            {
        //                                ImportMsg.Insert(0, string.Format("<span color=\"#CC0000\">保單號碼({0}) -> 家庭關係：被保人({1})[{2}]失敗！<br>　　　　{3}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["被保人"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            ImportMsg.Insert(0, string.Format("<span color=\"#CC0000\">保單號碼({0}) -> 客戶資料：被保人({1})[{2}]失敗！<br>　　　　{3}<span><br>", dr_PolicyDetail["保單號碼"].ToString(), dr_PolicyDetail["被保人"].ToString(), dr_CU == null ? "新增" : "更新", rsp.ReturnMessage));
        //                        }
        //                    }
        //                }
        //            }
        //            HttpContext.Current.Session["ImportMsg"] = ImportMsg.ToString();
        //        }
        //    }
        //}

        protected void Button1_Click(object sender, EventArgs e)
        {
            GridView1.DataSource = getAPI_Policy("1", "TB0232");
            GridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            GridView2.DataSource = getAPI_PolicyDetail("1", "7686", "TB0232")[0];
            GridView2.DataBind();
            GridView4.DataSource = getAPI_PolicyDetail("1", "7686", "TB0232")[1];
            GridView4.DataBind();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string ApiURL = "http://w11.gouptech.com.tw/InsAPI/GetInsItemList";
            string IP = "192.168.1.1";
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", "ML", "INJ", DateTime.UtcNow.ToString("yyyyMMddHH"), "TABC", IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            string err = "";
            string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
            if (err.Length == 0)
            {
                DataTable dt = JsonConvert.DeserializeObject<DataTable>(ret);
                GridView3.DataSource = dt;
                GridView3.DataBind();
            }

        }

        /// <summary>
        /// 智宇 - TM_查詢_0003_業查_保單查詢
        /// </summary>
        /// <param name="P_iINS_TYPE">保單類別</param>
        /// <param name="AccID">業務員編號</param>
        /// <returns>
        //壽險明細
        //Link
        //功能1
        //功能2
        //產險明細
        //保單號碼
        //保單代號
        //保險公司
        //建檔員
        //受理日
        //生效日
        //要保人
        //要保人ID
        //被保人
        //被保人ID
        //業務員
        //原始經手人
        //承保
        //行政中心
        //險種分類
        //狀態
        /// </returns>
        private static DataTable getAPI_Policy(string P_iINS_TYPE, string AccID)
        {
            DataTable dtDisPlay = new DataTable();
            string setSearch = "<BIS Request=\"SWP(N)\">" +
                            "  <Requests Name=\"TM_查詢_0003_業查_保單查詢\" Text=\"Request\" RB=\"RB(N)\">" +
                            "   <Request Name=\"TM_查詢_0003_業查_保單查詢\" Program=\"TM_003_Request\" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                            "    <Parameters>" +
                            "     <Parameter Name=\"@P_cINS_NO\"       Value=\"''\"/>" +//'{TXI_保單號碼}'
                            "     <Parameter Name=\"@P_cINS_FK\"       Value=\"''\"/>" +//'{COB_壽險保險公司}'
                            "     <Parameter Name=\"@P_cINS_FK_P\"     Value=\"''\"/>" +//'{COB_產險保險公司}'
                            "     <Parameter Name=\"@P_iINS_TYPE\"     Value=\"'" + P_iINS_TYPE + "'\"/>" +//'{COB_保單類別}'
                            "     <Parameter Name=\"@P_cINS_MAN_A\"    Value=\"''\"/>" +//'{TXI_要保人}'
                            "     <Parameter Name=\"@P_cINS_MAN_ID_A\" Value=\"''\"/>" +//'{TXI_要保人ID}'
                            "     <Parameter Name=\"@P_cINS_MAN_B\"    Value=\"''\"/>" +//'{TXI_被保人}'
                            "     <Parameter Name=\"@P_cINS_MAN_ID_B\" Value=\"''\"/>" +//'{TXI_被保人ID}'
                            "     <Parameter Name=\"@P_dDATE_S\"       Value=\"'1900/01/01'\"/>" +//'{DTF_日期起}'
                            "     <Parameter Name=\"@P_dDATE_E\"       Value=\"'" + DateTime.Now.ToString("yyyy/MM/dd") + "'\"/>" +//'{DTF_日期迄}'
                            "     <Parameter Name=\"@P_cSAL_ID_S\"     Value=\"''\"/>" +//'{COB_轄下業務員}'
                            "     <Parameter Name=\"@P_iDATE_TYPE\"    Value=\"'1'\"/>" +//'{COB_日期類別}'
                            "     <Parameter Name=\"@P_cINS_STATUS\"   Value=\"''\"/>" +//'{COB_保單狀態}'
                            "     <Parameter Name=\"@P_cSAL_FK\"       Value=\"'" + AccID + "'\"/>" +//'{USR_PK}'
                            "    </Parameters>" +
                            "    <ReturnCols/>" +
                            "   </Request>" +
                            "  </Requests>" +
                            " </BIS>";
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                dtDisPlay = XMLtoDataTable(ret, "BIS/Responds/Respond/N");
            }
            return dtDisPlay;
            //壽險明細
            //Link
            //功能1
            //功能2
            //產險明細
            //保單號碼
            //保單代號
            //保險公司
            //建檔員
            //受理日
            //生效日
            //要保人
            //要保人ID
            //被保人
            //被保人ID
            //業務員
            //原始經手人
            //承保
            //行政中心
            //險種分類
            //狀態
        }

        /// <summary>
        /// 智宇 - TM_查詢_壽險保單詳細資料 or TM_查詢_產險保單詳細資料
        /// </summary>
        /// <param name="P_iINS_TYPE">保單類別</param>
        /// <param name="P_iINS_PK">保單代碼</param>
        /// <param name="AccID">業務員編號</param>
        /// <returns>
        /// [保單明細]
        //保單代號
        //保險公司代號
        //保險公司名稱
        //受理日
        //建檔人員
        //建檔日期
        //保單號碼
        //送金單號
        //繳別
        //保單狀態
        //生效日
        //停止日期
        //停效日期
        //是否承保
        //保單收件日
        //回條簽收日
        //回條收件日
        //要保人
        //要保人身分證
        //要保人生日
        //附加人數
        //被保人
        //被保人身分證
        //被保人生日
        //關係
        //收費郵遞區號
        //收費地址
        //聯絡電話一
        //聯絡電話二
        //首期繳費方式
        //首期實繳保費
        //首期集彙件
        //首期付款人
        //首期付款銀行
        //首期帳號
        //首期支票金額
        //首期繳別
        //續期繳費方式
        //續期實繳保費
        //續期集彙件
        //續期付款人
        //續期付款銀行
        //續期帳號
        //續期支票金額
        //續期繳別
        //經手人1ID
        //經手人1姓名
        //經手人1比例
        //經手人1單位
        //經手人1代號
        //經手人2ID
        //經手人2姓名
        //經手人2比例
        //經手人2單位
        //經手人2代號
        //異動人員
        //異動日期
        //原始經手人ID
        //原始經手姓名
        //原始經手單位
        //原始經手代號
        //備註
        //保單類別
        //戶籍郵遞區號
        //戶籍地址
        //業務員
        //業務員姓名
        //處經理
        //送件方式
        /// [保單險種]
        //明細代號
        //主附
        //險種號碼
        //險種代號
        //險種名稱
        //生效日
        //年期
        //保額
        //保費
        //代理費
        //類別
        //被保人
        //被保人ID
        //被保人生日
        //單位計劃數
        //外幣保費
        //幣別
        /// </returns>
        private static List<DataTable> getAPI_PolicyDetail(string P_iINS_TYPE, string P_iINS_PK, string AccID)
        {
            List<DataTable> dt_list = new List<DataTable>(); 
            string setSearch = "";
            if(P_iINS_TYPE == "1" || P_iINS_TYPE == "2")//壽險||團險
            {
                setSearch = "<BIS Request=\"SWP(N)\">" +
                                    "     <Requests Name=\"TM_查詢_壽險保單詳細資料\" Text=\"Request\" RB=\"RB(N)\">" +
                                    "      <Request Name=\"TM_查詢_壽險保單詳細資料\" Program=\" TM_EXP_901_Request \" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                                    "       <Parameters>" +
                                    "        <Parameter Name=\"@P_iINS_PK \" Value=\"'" + P_iINS_PK.Replace("iINS_PK=", "") + "'\"/>" + //保單代號
                                    "        <Parameter Name=\"@P_cSAL_FK\" Value=\"'" + AccID + "'\"/>" +
                                    "       </Parameters>" +
                                    "       <ReturnCols/>" +
                                    "      </Request>" +
                                    "     </Requests>" +
                                    "    </BIS>";
            }
            else if(P_iINS_TYPE == "3")//產產
            {
                setSearch = "<BIS Request=\"SWP(N)\">" +
                                    "     <Requests Name=\"TM_查詢_產險保單詳細資料\" Text=\"Request\" RB=\"RB(N)\">" +
                                    "      <Request Name=\"TM_查詢_產險保單詳細資料\" Program=\" TM_EXP_902_Request \" Row=\"\" MaxRows=\"0\" MaxCols=\"0\" MinLags=\"0\">" +
                                    "       <Parameters>" +
                                    "        <Parameter Name=\"@P_iINS_PK \" Value=\"'" + P_iINS_PK.Replace("iINS_PK=", "") + "'\"/>" + //保單代號
                                    "        <Parameter Name=\"@P_cSAL_FK\" Value=\"'" + AccID + "'\"/>" +
                                    "       </Parameters>" +
                                    "       <ReturnCols/>" +
                                    "      </Request>" +
                                    "     </Requests>" +
                                    "    </BIS>";
            }
            string err = "";
            string ret = WebClientPost("http://agent.tabc.com.tw/EXP/BIS_Express.exe", setSearch, "UTF-8", out err); //智宇 界接程式
            if (err.Length == 0)
            {
                //保單明細
                dt_list.Add(XMLtoDataTable(ret, "BIS/Responds/Respond/N"));
                //保單險種
                dt_list.Add(XMLtoDataTable(ret, "BIS/Responds/Respond/C"));
            }
            return dt_list;
        }

        /// <summary>
        /// 昇華 - 險種清單依指定保險公司與代碼
        /// </summary>
        /// <param name="CompanyNo">公司代號</param>
        /// <param name="Mark">險種代碼</param>
        /// <returns>
        //CompanyNo
        //CompanyName
        //Mark
        //DispMark
        //InsName
        //DispClass
        //Unit
        //Saled
        //Keyword
        //Master
        //MinVal
        //MaxVal
        //Channel
        /// </returns>
        private static DataTable getAPI_GetInsItemList(string CompanyNo, string Mark)
        {
            DataTable dt = new DataTable();
            string ApiURL = "http://w11.gouptech.com.tw/InsAPI/GetInsItemList";
            string Channel = "TABC";
            string IP = "192.168.1.1";
            string no = string.Format("{0}|{1}|{2}|{3}|{4}", CompanyNo, Mark, DateTime.UtcNow.ToString("yyyyMMddHH"), Channel, IP);
            no = TDecrypt.Encrypt(no, "de3FR$y6").Replace("+", "-").Replace("/", "_");
            string err = "";
            string ret = WebClientPost(ApiURL, string.Format("no={0}", no), "UTF-8", out err);
            if (err.Length == 0)
            {
                dt = JsonConvert.DeserializeObject<DataTable>(ret);
            }
            else
            {
                string s = err;
            }
            return dt;
        }

        private static string WebClientPost(string url, string postData, string encodeType, out string err)
        {
            try
            {
                Uri uri = new Uri(url);
                if (uri.Scheme == "https" || uri.Scheme == "http")
                {
                    ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                }
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = WebRequestMethods.Http.Post;

                string received = "";
                byte[] bs = Encoding.GetEncoding(encodeType).GetBytes(postData);
                StreamReader reader = null;
                request.ContentLength = bs.Length;
                request.ContentType = "application/x-www-form-urlencoded";
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(bs, 0, bs.Length);
                }
                using (WebResponse wr = request.GetResponse())
                {
                    reader = new StreamReader(wr.GetResponseStream());
                    received = reader.ReadToEnd();
                    err = string.Empty;
                    return received;
                }


            }
            catch (Exception ex)
            {
                err = ex.Message;
                return string.Empty;
            }
        }
        private class TrustAllCertificatePolicy : ICertificatePolicy
        {
            public void New() { }
            public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
            {
                return true;
            }
        }
        private static DataTable XMLtoDataTable(string xml, string SelectNodes)
        {
            DataTable dtDisPlay = new DataTable();
            #region 取出 智宇 保單主檔 XML 資料到 -->dtDisPlay
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml.Replace("&", "&amp;"));
            XmlNodeList NodeList = doc.SelectNodes(SelectNodes);
            foreach (XmlNode COB in NodeList)
            {
                if (dtDisPlay.Columns.Count == 0)
                {
                    foreach (XmlAttribute Attr in COB.Attributes)
                    {
                        dtDisPlay.Columns.Add(Attr.Name.ToString());
                    }
                }

                DataRow drnew = dtDisPlay.NewRow();
                foreach (XmlAttribute Attr in COB.Attributes)
                {
                    if (dtDisPlay.Columns.Contains(Attr.Name.ToString()))
                    {
                        drnew[Attr.Name.ToString()] = COB.Attributes[Attr.Name.ToString()].Value;
                    }
                }
                drnew.EndEdit();
                dtDisPlay.Rows.Add(drnew);
            }
            #endregion
            return dtDisPlay;
        }

        /// <summary>
        /// 客戶資料
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_Customer(DataRow dr, DataRow dr_CU, bool isOwner)
        {
            Hashtable hsData = new Hashtable();
            if (dr_CU == null)
            {
                hsData.Add("CU_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("CU_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("CU_TransNo", dr_CU["CU_TransNo"].ToString());
                hsData.Add("CU_UidNo", dr_CU["CU_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            if (isOwner)//要保人
            {
                hsData.Add("CU_IsShow", dr["要保人"].ToString() == dr["被保人"].ToString() ? "1" : "0");
                hsData.Add("CU_CustName", dr["要保人"].ToString());
                hsData.Add("CU_ID", dr["要保人身分證"].ToString());
                if (dr["要保人身分證"].ToString() != "")
                {
                    if (dr["要保人身分證"].ToString().Substring(1, 1).Equals("1"))
                        hsData.Add("CU_Sex", "男");
                    else
                        hsData.Add("CU_Sex", "女");
                }
                if (dr["要保人生日"].ToString().Trim().Length > 0)
                {
                    if (!Convert.ToDateTime(dr["要保人生日"].ToString()).ToString("yyyy/MM/dd").Equals("1900/01/01"))
                    {
                        int CU_Age = DateTime.Now.Year - DateTime.Parse(dr["要保人生日"].ToString()).Year;
                        if (DateTime.Now.Month - DateTime.Parse(dr["要保人生日"].ToString()).Month >= 6)
                            CU_Age += 1;
                        int CU_AgeFull = DateTime.Now.Year - DateTime.Parse(dr["要保人生日"].ToString()).Year;
                        hsData.Add("CU_Birth", Convert.ToDateTime(dr["要保人生日"].ToString()).ToString("yyyy/MM/dd"));
                        hsData.Add("CU_Age", CU_Age.ToString());
                        hsData.Add("CU_AgeFull", CU_AgeFull.ToString());
                    }
                }
            }
            else//被保人
            {
                hsData.Add("CU_IsShow", "0");
                hsData.Add("CU_CustName", dr["被保人"].ToString());
                hsData.Add("CU_ID", dr["被保人身分證"].ToString());
                if (dr["被保人身分證"].ToString() != "")
                {
                    if (dr["被保人身分證"].ToString().Substring(1, 1).Equals("1"))
                        hsData.Add("CU_Sex", "男");
                    else
                        hsData.Add("CU_Sex", "女");
                }
                if (dr["被保人生日"].ToString().Trim().Length > 0)
                {
                    if (!Convert.ToDateTime(dr["被保人生日"].ToString()).ToString("yyyy/MM/dd").Equals("1900/01/01"))
                    {
                        int CU_Age = DateTime.Now.Year - DateTime.Parse(dr["被保人生日"].ToString()).Year;
                        if (DateTime.Now.Month - DateTime.Parse(dr["被保人生日"].ToString()).Month >= 6)
                            CU_Age += 1;
                        int CU_AgeFull = DateTime.Now.Year - DateTime.Parse(dr["被保人生日"].ToString()).Year;
                        hsData.Add("CU_Birth", Convert.ToDateTime(dr["被保人生日"].ToString()).ToString("yyyy/MM/dd"));
                        hsData.Add("CU_Age", CU_Age.ToString());
                        hsData.Add("CU_AgeFull", CU_AgeFull.ToString());
                    }
                }
            }
            hsData.Add("CU_IDClass", "身份證號");
            hsData.Add("CU_CustType", "保戶");
            hsData.Add("CU_AddType", "TABC");
            hsData.Add("CU_BelongAgent", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            

            return hsData;
        }

        /// <summary>
        /// 家庭關係
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <param name="ht_CU">客戶資料</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_FamilyRela(DataRow dr, Hashtable ht_CU, DataRow dr_FA)
        {
            Hashtable hsData = new Hashtable();
            if (dr_FA == null)
            {
                hsData.Add("FA_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("FA_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("FA_TransNo", dr_FA["CU_TransNo"].ToString());
                hsData.Add("FA_UidNo", dr_FA["CU_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            hsData.Add("CU_TransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("CU_UidNo", ht_CU["CU_UidNo"].ToString());
            hsData.Add("FA_CUTransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("FA_CUUidNo", ht_CU["CU_UidNo"].ToString());
            if (dr["要保人"].ToString() == dr["被保人"].ToString())//本人
            {
                hsData.Add("FA_RelaName", "本人");
                hsData.Add("FA_RelaNo", "1");
            }
            else
            {
                string FA_RelaName = "";
                string FA_RelaNo = "";
                switch (dr["關係"].ToString())
                {
                    case "配偶":
                        FA_RelaName = "配偶";
                        FA_RelaNo = "2";
                        break;
                    case "子女":
                        FA_RelaName = "子女";
                        FA_RelaNo = "3";
                        break;
                    case "父母":
                        FA_RelaName = "父母";
                        FA_RelaNo = "4";
                        break;
                    case "其他":
                        FA_RelaNo = "12";
                        break;
                    default:
                        FA_RelaName = "其他";
                        FA_RelaNo = "12";
                        break;
                }
                hsData.Add("FA_RelaName", FA_RelaName);
                hsData.Add("FA_RelaNo", FA_RelaNo);
            }
            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
            
            return hsData;
        }

        /// <summary>
        /// 通訊資料
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <param name="ht_CU">客戶資料</param>
        /// <param name="CM_CLAS">類別</param>
        /// <param name="CM_TYPE">次類別</param>
        /// <param name="CM_ZIP">郵遞區號</param>
        /// <param name="CM_DESC">內容(地址||電話)</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_Communication(DataRow dr, Hashtable ht_CU, DataRow dr_CM, string CM_CLAS, string CM_TYPE, string CM_ZIP, string CM_DESC)
        {
            Hashtable hsData = new Hashtable();

            if (dr_CM == null)
            {
                hsData.Add("CM_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("CM_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("CM_TransNo", dr_CM["CU_TransNo"].ToString());
                hsData.Add("CM_UidNo", dr_CM["CU_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            hsData.Add("CU_TransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("CU_UidNo", ht_CU["CU_UidNo"].ToString());
            hsData.Add("CM_CLAS", CM_CLAS);
            hsData.Add("CM_TYPE", CM_TYPE);
            hsData.Add("CM_ZIP", CM_ZIP);
            hsData.Add("CM_DESC", CM_DESC);

            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            return hsData;
        }

        /// <summary>
        /// 保單資料(壽/團)
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <param name="ht_CU">客戶資料</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_CustPolicy(DataRow dr, Hashtable ht_CU, DataRow dr_PO)
        {
            Hashtable hsData = new Hashtable();

            if (dr_PO == null)
            {
                hsData.Add("PO_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("PO_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("PO_TransNo", dr_PO["PO_TransNo"].ToString());
                hsData.Add("PO_UidNo", dr_PO["PO_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            hsData.Add("PO_PKSKey", dr["保單代號"].ToString());
            hsData.Add("PO_PolNo", dr["保單號碼"].ToString());
            hsData.Add("PO_Ownername", dr["要保人"].ToString());
            hsData.Add("PO_AccureDate", dr["生效日"].ToString());
            if (dr.Table.Columns.Contains("續期繳費方式"))
                hsData.Add("PO_PaidMode", dr["續期繳費方式"].ToString());
            hsData.Add("PO_PolState", dr["保單狀態"].ToString());
            string sPO_Mode = "";
            switch (dr["繳別"].ToString())
            {
                case "1年繳":
                    sPO_Mode = "1";
                    break;
                case "2半年繳":
                    sPO_Mode = "2";
                    break;
                case "3季繳":
                    sPO_Mode = "3";
                    break;
                case "4月繳":
                    sPO_Mode = "4";
                    break;
                default:
                    sPO_Mode = "5";
                    break;
            }
            hsData.Add("PO_Mode", sPO_Mode);
            hsData.Add("PO_PaidToDate", GetPaiddate(Convert.ToDateTime(dr["生效日"]).ToString("yyyy/MM/dd"), sPO_Mode));
            hsData.Add("PO_ModePrem", dr["首期實繳保費"].ToString().Replace(",", ""));
            int PO_InsureAge = Convert.ToDateTime(dr["生效日"]).Year - DateTime.Parse(dr["被保人生日"].ToString()).Year;
            if (DateTime.Now.Month - DateTime.Parse(dr["被保人生日"].ToString()).Month >= 6)
                PO_InsureAge += 1;
            hsData.Add("PO_InsureAge", PO_InsureAge);
            hsData.Add("PO_ComSName", dr["保險公司名稱"].ToString());
            if (dr.Table.Columns.Contains("經手人1姓名"))
                hsData.Add("PO_POLBELONGAGENT", dr["經手人1姓名"].ToString());
            hsData.Add("PO_OwnerID", dr["要保人身分證"].ToString());
            hsData.Add("PO_PolZip", dr["收費郵遞區號"].ToString());
            hsData.Add("PO_PolAddr", dr["收費地址"].ToString());
            if (dr.Table.Columns.Contains("原始經手姓名"))
                hsData.Add("PO_Predecessor", dr["原始經手姓名"].ToString());
            hsData.Add("CU_TransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("CU_UidNo", ht_CU["CU_UidNo"].ToString());
            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            return hsData;
        }
        /// <summary>
        /// --依生效日, 繳別算出下次繳費日
        /// </summary>
        /// <param name="AccureDate">生效日</param>
        /// <param name="Mode">繳別</param>
        /// <returns></returns>
        private static string GetPaiddate(string AccureDate, string Mode)
        {
            Int32 monthtoinc = 0;  //
            if (Mode == "" || Mode == "5")
            {
                return AccureDate;
            }

            DateTime Enddate = Convert.ToDateTime(AccureDate);  //生效日
            //年繳：依生效日之　月日　加一年　　（每年繳１次）
            //半年繳：依生效日之　月日　加六個月　（每年繳２次）
            //季繳：依生效日之　月日　加三個月　（每年繳４次）
            //月繳：依生效日之　月日　加每個月　（每年繳１２次）
            switch (Mode)
            {
                case "1": monthtoinc = 12; break; //年繳
                case "2": monthtoinc = 6; break; ; //半年繳
                case "3": monthtoinc = 3; break; ; //季繳
                case "4": monthtoinc = 1; break; ; //月繳
            }

            Int32 accday = Enddate.Day; //生效日
            DateTime today = DateTime.Now;  //現在時間
            while (Enddate < today)
            {
                Enddate = Enddate.AddMonths(monthtoinc);  //
                if (accday >= 29)
                {
                    Int32 paidday = Enddate.Day;
                    if (paidday < accday)
                    {
                        DateTime int_d = new DateTime(Enddate.Year, Enddate.AddMonths(1).Month, 1);
                        Int32 LastdayOfMonth = int_d.Day;
                        if (accday < LastdayOfMonth)
                        {
                            Enddate = new DateTime(Enddate.Year, Enddate.Month, Enddate.Day + accday - paidday);
                        }
                    }
                }
            }

            return Enddate.ToString("yyyy/MM/dd");
        }

        /// <summary>
        /// 保單資料(產)
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <param name="dr_Pdt">險種資料</param>
        /// <param name="ht_CU">客戶資料</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_CustIndustPolm(DataRow dr, DataRow dr_Pdt, Hashtable ht_CU, DataRow dr_IP)
        {
            Hashtable hsData = new Hashtable();

            if (dr_IP == null)
            {
                hsData.Add("IP_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("IP_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("IP_TransNo", dr_IP["IP_TransNo"].ToString());
                hsData.Add("IP_UidNo", dr_IP["IP_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            hsData.Add("IP_ComSname", dr["保險公司名稱"].ToString());
            hsData.Add("IP_Polno", dr["保單號碼"].ToString());
            if (dr["到期日"].ToString() != "")
                hsData.Add("IP_EndDate", dr["到期日"].ToString());
            if (dr["車號"].ToString() != "")
                hsData.Add("IP_PlateNo", dr["車號"].ToString());
            if (dr_Pdt["生效日"].ToString() != "")
                hsData.Add("IP_StartDate", dr_Pdt["生效日"].ToString());
            if (dr_Pdt["險種名稱"].ToString().IndexOf("車") != -1)
                hsData.Add("IP_ProdKind", "汽車險");
            else if (dr_Pdt["險種名稱"].ToString().IndexOf("火險") != -1)
                hsData.Add("IP_ProdKind", "房屋險");
            hsData.Add("IP_CarOwner", dr["被保人"].ToString());
            if (dr_Pdt["保費"].ToString() != "")
                hsData.Add("IP_Modeprem", dr_Pdt["保費"].ToString().Replace(",", ""));
            else
                hsData.Add("IP_Modeprem", "0");
            if (dr_Pdt["險種名稱"].ToString().IndexOf("任意險") != -1)
                hsData.Add("IP_PdtArbitrariness", "有");
            else if (dr_Pdt["險種名稱"].ToString().IndexOf("強制險") != -1)
                hsData.Add("IP_PdtCoactive", "有");
            hsData.Add("CU_TransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("CU_UidNo", ht_CU["CU_UidNo"].ToString());

            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            return hsData;
        }

        /// <summary>
        /// 險種資料
        /// </summary>
        /// <param name="dr">保單明細</param>
        /// <param name="dr_Pdt">險種資料</param>
        /// <param name="ht_CU">客戶資料</param>
        /// <returns></returns>
        private static Hashtable dataToHashtable_CustPdt(DataRow dr, DataRow dr_Pdt, Hashtable ht_CU, Hashtable ht_PO, DataRow dr_PD)
        {
            Hashtable hsData = new Hashtable();

            if (dr_PD == null)
            {
                hsData.Add("PD_TransNo", Guid.NewGuid().ToString("N"));
                hsData.Add("PD_UidNo", HttpContext.Current.Session["UidNo"].ToString());
                hsData.Add("ModiState", "A");
            }
            else
            {
                hsData.Add("PD_TransNo", dr_PD["PD_TransNo"].ToString());
                hsData.Add("PD_UidNo", dr_PD["PD_UidNo"].ToString());
                hsData.Add("ModiState", "M");
            }
            hsData.Add("PD_PKSKey", dr_Pdt["明細代號"].ToString());
            hsData.Add("PD_PolNo", dr["保單號碼"].ToString());
            hsData.Add("PD_PdtNo", dr_Pdt["險種號碼"].ToString());
            DataRow dr_TCM = DataTABC_COMPANY_MAP.DataReader_TCM_ComCode(dr["保險公司名稱"].ToString());
            if (dr_TCM != null)
            {
                DataTable dt_PD = getAPI_GetInsItemList(dr_TCM["ComCode_GT"].ToString(), dr_Pdt["險種代號"].ToString());
                if (dt_PD != null && dt_PD.Rows.Count != 0)
                    hsData.Add("PD_Kind", dt_PD.Rows[0]["DispClass"].ToString());
            }
            string FA_RelaNo = "12";
            if (dr["要保人"].ToString() == dr["被保人"].ToString())//本人
            {
                FA_RelaNo = "1";
            }
            else
            {
                switch (dr["關係"].ToString())
                {
                    case "配偶":
                        FA_RelaNo = "2";
                        break;
                    case "子女":
                        FA_RelaNo = "3";
                        break;
                    case "父母":
                        FA_RelaNo = "4";
                        break;
                    case "其他":
                        FA_RelaNo = "12";
                        break;
                    default:
                        FA_RelaNo = "12";
                        break;
                }
            }
            hsData.Add("PD_Relation", FA_RelaNo);
            hsData.Add("PD_PdtCode", dr_Pdt["險種代號"].ToString());
            hsData.Add("PD_PdtYear", dr_Pdt["年期"].ToString());
            hsData.Add("PD_InsurYear", dr_Pdt["年期"].ToString());
            if (dr_Pdt["保額"].ToString() != "")
                hsData.Add("PD_AmtInput", dr_Pdt["保額"].ToString().Replace(",", ""));
            else
                hsData.Add("PD_AmtInput", "0");
            if (dr_Pdt["保費"].ToString() != "")
            {
                hsData.Add("PD_ModePrem", dr_Pdt["保費"].ToString().Replace(",", ""));
                hsData.Add("PD_Premium", dr_Pdt["保費"].ToString().Replace(",", ""));
            }
            else
            {
                hsData.Add("PD_ModePrem", "0");
                hsData.Add("PD_Premium", "0");
            }
            if (dr_Pdt["生效日"].ToString() != "")
                hsData.Add("PD_AccureDate", dr_Pdt["生效日"].ToString());
            if (dr_Pdt.Table.Columns.Contains("單位計劃數"))
                hsData.Add("PD_Unit", dr_Pdt["單位計劃數"].ToString());
            hsData.Add("PD_PdtName", dr_Pdt["險種名稱"].ToString());
            hsData.Add("PD_ComSName", dr["保險公司名稱"].ToString());
            hsData.Add("PD_PdtCodeState", dr["保單狀態"].ToString());
            hsData.Add("CU_TransNo", ht_CU["CU_TransNo"].ToString());
            hsData.Add("CU_UidNo", ht_CU["CU_UidNo"].ToString());
            hsData.Add("PO_TransNo", ht_PO["PO_TransNo"].ToString());
            hsData.Add("PO_UidNo", ht_PO["PO_UidNo"].ToString());
            hsData.Add("AccID", HttpContext.Current.Session["AccID"].ToString());
            hsData.Add("ComyCode", HttpContext.Current.Session["ComyCode"].ToString());
            hsData.Add("Server_ModiDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            return hsData;
        }
        [System.Web.Services.WebMethod]
        public static string ShowImportMsg()
        {
            return HttpContext.Current.Session["ImportMsg"].ToString();
            //ScriptManager.RegisterClientScriptBlock(this, HttpContext.Current.GetType(), "ImportMsg", string.Format("ImportMsg('{0}');", msg), true);
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            //if (ImportMsg != null)
            //    ShowImportMsg(ImportMsg.ToString());
            Session["ImportMsg"] = ImportMsg.ToString();
        }

        

        

        
    }
}