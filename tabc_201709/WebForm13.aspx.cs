﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm13 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MainControls.GridViewStyleCtrl(GridView1, 800);
                if (Request.QueryString["id"] != null && Request.QueryString["id"] != "")
                    init(Request["id"]);
            }
        }

        private void init(string id)
        {
            if (ConfigurationManager.AppSettings["TABCFilePoolPath"] != null)
            {
                string catrgory = "";
                switch (id)
                {
                    case "11":
                        catrgory = "11公文區";
                        break;
                    case "12":
                        catrgory = "12壽險公司資源";
                        break;
                    case "131":
                        catrgory = "13產險公司資源";
                        break;
                    case "14":
                        catrgory = "14台名壹周看";
                        break;
                    case "15":
                        catrgory = "15行政表單";
                        break;
                    case "16":
                        catrgory = "16管理規則";
                        break;

                }
                ViewState["catrgory"] = catrgory;
                string sMainPth = string.Format("{0}\\{1}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), catrgory);
                if (Directory.Exists(sMainPth))
                {
                    DirectoryInfo di = new DirectoryInfo(sMainPth);
                    DirectoryInfo[] diArr = di.GetDirectories();
                    foreach (DirectoryInfo dri in diArr)
                    {
                        ddlSh_category.Items.Add(new ListItem(dri.Name.Substring(2), dri.Name));
                    }
                    sMainPth = string.Format("{0}\\{1}\\{2}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString(), ddlSh_category.SelectedValue);
                    di = new DirectoryInfo(sMainPth);
                    diArr = di.GetDirectories();
                    foreach (DirectoryInfo dri in diArr)
                    {
                        ddlSh_category_sub.Items.Add(new ListItem(dri.Name.Substring(2), dri.Name));
                    }

                    dataBind();
                }
                else
                    MainControls.showMsg(this, "路徑錯誤！");
            }
            else
            {
                MainControls.showMsg(this, "尚未設定來源路徑！");
            }
        }

        private void dataBind()
        {
            string sMainPth = string.Format("{0}\\{1}\\{2}\\{3}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString(), ddlSh_category.SelectedValue, ddlSh_category_sub.SelectedValue);
            DirectoryInfo di = new DirectoryInfo(sMainPth);
            FileInfo[] fi_array = di.GetFiles();
            DataTable dt = new DataTable("TABCFiles");
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("LastWriteTime", typeof(DateTime));
            dt.Columns.Add("LastWriteDate", typeof(string));
            dt.Columns.Add("Extension", typeof(string));
            dt.Columns.Add("Length", typeof(string));
            dt.Columns.Add("FullName", typeof(string));
            dt.Columns.Add("isNew", typeof(bool));
            foreach (FileInfo fi in fi_array)
            {
                string fileName = fi.Name;                //檔案名稱
                DateTime fileDate = fi.LastWriteTime;   //修改日期
                if (!fileName.ToString().Equals("Thumbs.db"))
                {
                    dt.Rows.Add(
                        fi.Name,
                        fi.LastWriteTime,
                        fi.LastWriteTime.ToString("yyyy/MM/dd"),
                        fi.Extension.Replace(".", "").ToUpper(),
                        string.Format("{0} MB", Math.Round((double)fi.Length / 1024 / 1024, 1)),
                        fi.FullName,
                        fi.LastWriteTime > DateTime.Today.AddDays(-20)
                        );
                }
            }
            DataView dvfiles = new DataView(dt);
            ViewState["dt_file"] = dt;
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string fileUrlPath = ((ImageButton)sender).CommandName;
            System.Net.WebClient wc = new System.Net.WebClient();
            byte[] file = null;

            try
            {
                file = wc.DownloadData(fileUrlPath);

                HttpContext.Current.Response.Clear();
                string fileName = System.IO.Path.GetFileName(fileUrlPath);
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + HttpContext.Current.Server.UrlEncode(fileName));
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.BinaryWrite(file);
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {
                MainControls.showMsg(this, ex.Message);
            }


        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = ViewState["dt_file"] as DataTable;
            DataView dvfiles = dt.AsDataView();
            if (txtSh_Name.Text.Trim() != "")
                dvfiles = dt.AsEnumerable().Where(x => x.Field<string>("Name").IndexOf(txtSh_Name.Text.Trim()) != -1).AsDataView();
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        protected void btnRestQuery_Click(object sender, EventArgs e)
        {
            DataTable dt = ViewState["dt_file"] as DataTable;
            DataView dvfiles = dt.AsDataView();
            dvfiles.Sort = "LastWriteTime DESC";
            GridView1.DataSource = dvfiles;
            GridView1.DataBind();
        }

        protected void ddlSh_category_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sMainPth = string.Format("{0}\\{1}\\{2}", ConfigurationManager.AppSettings["TABCFilePoolPath"].ToString(), ViewState["catrgory"].ToString(), ddlSh_category.SelectedValue);
            DirectoryInfo di = new DirectoryInfo(sMainPth);
            DirectoryInfo[] diArr = di.GetDirectories();
            ddlSh_category_sub.Items.Clear();
            foreach (DirectoryInfo dri in diArr)
            {
                ddlSh_category_sub.Items.Add(new ListItem(dri.Name.Substring(2), dri.Name));
            }
            dataBind();
        }

        protected void ddlSh_category_sub_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataBind();
        }
    }
}