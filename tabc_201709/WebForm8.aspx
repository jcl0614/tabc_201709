﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm8.aspx.cs" Inherits="tabc_201709.WebForm8" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <style type="text/css" media="print">    
.Noprint {display: none; margin:0px ; padding:0px;}    
.PageNext {page-break-after: always}    
</style>  
    <script>
        function printHtml(value) {
            var printPage = window.open("", "", "");
            printPage.document.open();
            printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
            printPage.document.write("<PRE>");
            printPage.document.write(value);
            printPage.document.write("</PRE>");
            printPage.document.close("</BODY></HTML>");
        }
</script>
</head>
<body style="padding:0px;margin:0px;">
    <form id="form1" runat="server">
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
<%--    <div style="width: 220mm; height: 110mm">
        <table width="100%" >
            <tr><td style="width: 100%; height: 55mm; padding-top: 5%; padding-left: 5%;" align="left" valign="top">寄件地址<br />寄件單位</td></tr>
            <tr><td style="width: 100%; height: 55mm" align="center">收件地址<br /><br />收件人</td></tr>
        </table>
    </div>--%>
    <div style="page-break-before:always; line-height: 0px; height: 0px;"></div>
        <div style="border: 1px solid #CC0000; width: 220mm; height: 90mm"></div>
    <div style="page-break-before:always; line-height: 0px; height: 0px;"></div>
        <div style="border: 1px solid #CC0000; width: 220mm; height: 90mm"></div>
    </form>
    
</body>
</html>
