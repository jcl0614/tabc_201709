﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace tabc_201709
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable dt = DataETeacherTot.DataReader(
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    0, 99999
                                    ).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("序號", typeof(string));
                dt_export.Columns.Add("查詢區間日期", typeof(string));
                dt_export.Columns.Add("講師所屬單位", typeof(string));
                dt_export.Columns.Add("講師姓名", typeof(string));
                dt_export.Columns.Add("授課時數(小時)", typeof(string));
                dt_export.Columns.Add("授課點數(計分)", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    dt_export.Rows.Add(
                        dr["SerialNo"].ToString(),
                        string.Format("{0}~{1}", "", ""),
                        dr["ET_Unit"].ToString(),
                        dr["ET_TName"].ToString(),
                        dr["EH_Hours"].ToString(),
                        dr["EH_Points"].ToString()
                        );
                }
                GridView2.DataSource = dt_export;
                GridView2.DataBind();
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            DataTable dt = DataETeacherTot.DataReader(
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    0, 99999
                                    ).Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dt_export = new DataTable();
                dt_export.Columns.Add("序號", typeof(string));
                dt_export.Columns.Add("上課日期", typeof(string));
                dt_export.Columns.Add("課程類別", typeof(string));
                dt_export.Columns.Add("開課地區", typeof(string));
                dt_export.Columns.Add("開課單位", typeof(string));
                dt_export.Columns.Add("課程地點", typeof(string));
                dt_export.Columns.Add("講師所屬地區", typeof(string));
                dt_export.Columns.Add("講師所屬單位", typeof(string));
                dt_export.Columns.Add("講師業務編號", typeof(string));
                dt_export.Columns.Add("講師姓名", typeof(string));
                dt_export.Columns.Add("課程名稱", typeof(string));
                dt_export.Columns.Add("授課時數(小時)", typeof(string));
                dt_export.Columns.Add("授課點數(計分)", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    dt_export.Rows.Add(
                        dr["SerialNo"].ToString(),
                        dr["EC_SDate"].ToString(),
                        dr["EC_ModeName"].ToString(),
                        dr["EC_Area"].ToString(),
                        dr["EC_UnitName"].ToString(),
                        dr["EC_Place"].ToString(),
                        dr["ET_Area"].ToString(),
                        dr["ET_UnitName"].ToString() == "" ? dr["ET_Unit"].ToString() : dr["ET_UnitName"].ToString(),
                        dr["ET_TCode"].ToString(),
                        dr["ET_TName"].ToString(),
                        dr["EC_CName"].ToString(),
                        dr["EH_Hours"].ToString(),
                        dr["EH_Points"].ToString()
                        );
                }
                GridView2.DataSource = dt_export;
                GridView2.DataBind();
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            DataTable dt_unit = DataPhrase.DataReader_TypeCode("E008").Tables[0];
            DataTable dt_export = new DataTable();
            dt_export.Columns.Add("", typeof(string));
            
            foreach (DataRow dr_unit in dt_unit.Rows)
            {
                dt_export.Columns.Add(dr_unit["TypeName"].ToString(), typeof(string));
                
            }
            DataTable dt = DataETeacherTot.DataReader(
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   0, 99999
                                   ).Tables[0];
            var dt_unit_t = dt.AsEnumerable().OrderBy(o => o.Field<string>("ET_Unit")).GroupBy(r => new { ET_UnitName = r["ET_UnitName"] }).ToList();
            foreach (var data in dt_unit_t)
            {
                List<string> list = new List<string>();
                list.Add(data.Key.ET_UnitName.ToString());
                foreach (DataRow dr_unit in dt_unit.Rows)
                {
                    var amt = dt.Compute("sum(EH_Points)", string.Format("ET_UnitName='{0}' and EC_UnitName='{1}'", data.Key.ET_UnitName.ToString(), dr_unit["TypeName"].ToString())).ToString();
                    list.Add(amt == "" ? "0" : amt);
                }
                dt_export.Rows.Add(list.ToArray());
            }
            GridView2.DataSource = dt_export;
            GridView2.DataBind();
            GridView2.HeaderRow.Cells[0].Text = "→邀請單位<br>↓講師單位";
        }
    }
}